<?php

namespace App\Modules\Contractors\src\Request;

use App\Modules\Contractors\src\Models\Contract;
use App\Modules\Contractors\src\Models\Contractor;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class ComplianceContractorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'document' => 'required|numeric|digits_between:3,12|exists:mysql_contractors.contractors',
            'contract_year' => 'required|numeric|digits:4|date_format:Y',
            'contract_number' => 'required|numeric|digits:4',
            'period_month' => 'required|numeric',
            'period_year' => 'required|numeric|digits:4|date_format:Y'
        ];
    }

    public function withValidator($validator) 
    {
        $contractNumber = $this->input('contract_number');
        $contractYear = $this->input('contract_year');

        Contractor::query()
            ->where('document', $this->input('document'))
            ->whereHas('contracts', function($query) use ($contractNumber, $contractYear) {
                $query->where('contract', format_contract($contractNumber, $contractYear))
                    ->whereIn('contract_type_id', [Contract::NEW_CONTRACT, Contract::CONTRACT_ASSIGNMENT]);
            })->firstOr(function() use ($validator) {
                throw ValidationException::withMessages(array_merge($validator->errors()->toArray(), [
                    'contract' => __('contractor.not_found_contract')
                ]));
            });
    }
}
