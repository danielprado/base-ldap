<?php


namespace App\Modules\Contractors\src\Request;


use Illuminate\Foundation\Http\FormRequest;
class CertificateContractualRequest extends FormRequest{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function rules(){
        return [
            'documentType'  =>  'required|numeric',
            'document'      =>  'required|numeric',
            'year'          =>  'required|numeric|date_format:Y',
            'certificateType'   =>  'required|numeric'
        ];
    }

    public function attributes()
    {
        return [
            'documentType' => 'Tipo de documento',
            'document' => 'número de documento',
            'year'  =>  'Año del contrato',
            'certificateType'  =>  'Tipo de certificación',
        ];
    }
}
