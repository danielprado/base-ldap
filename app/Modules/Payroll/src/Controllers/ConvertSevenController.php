<?php


namespace App\Modules\Payroll\src\Controllers;


use App\Http\Controllers\Controller;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Modules\Payroll\src\Resources\ConvertSevenResource;
use App\Modules\Payroll\src\Models\ConvertSeven;
use Tightenco\Collect\Support\Collection;
use Illuminate\Support\Str;
use Exception;
use GuzzleHttp\Client;

class ConvertSevenController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        return $this->success_response(
            ConvertSevenResource::collection(ConvertSeven::all())
        );
    }
    public function getConvertSevenList(Request $request)
    {
        $data = [];
        // dd($data);
        $data = ConvertSeven::query()
                ->whereIn('TER_CODI', $request->listDocuments)
                // ->orderBy('TER_NOCO')
                ->paginate(10000);
        return  $this->success_response(
            ConvertSevenResource::collection( $data )
        );
    }

    public function getConvertSevenListByErrepe(Request $request)
    {
        $data = [];
        // dd($data);
        $data = ConvertSeven::query()
                ->whereIn('MPR_NUME', $request->listErrepes)
                ->paginate(10000);
        return  $this->success_response(
            ConvertSevenResource::collection( $data )
        );
    }
}
