<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('activity_report_id')->nullable()->comment('Identificador del informe de actividades');
            $table->unsignedBigInteger('compliance_id')->nullable()->comment('Identificador del certificado de cumplimiento');
            $table->unsignedBigInteger('supervisor_id')->nullable()->comment('Identificador del Usuario LDAP');
            $table->unsignedBigInteger('contract_id')->nullable()->comment('Identificador del contrato');
            $table->unsignedBigInteger('payroll_id')->nullable()->comment('Identificador de la planilla');
            $table->enum('status', ['PENDING', 'APPROVED', 'PAYROLL', 'BINNACLE', 'INVOICING', 'PAYMENT'])->default('PENDING')->comment('[PENDING] creado automaticamente, [APPROVED] aprobado por un supervisor, [PAYROLL] Incluído en una planilla [BINNACLE] aplicada la bitacoras, [INVOICING] aplicada la causación, [PAYMENT]  aplicado el pago');
            $table->string('contract', 191)->comment('Número del contrato');
            $table->integer('plan_number')->comment('Número del plan de pago');
            $table->string('filed_number', 255)->nullable()->comment('Número de radicado para (SECOP API)');
            $table->integer('year')->comment('Año del plan de pago');
            $table->integer('month')->comment('Mes del plan de pago');
            $table->integer('amount_day_pay')->comment('Días a pagar');
            $table->integer('start_day')->comment('Día inicial');
            $table->integer('final_day')->comment('Día final');
            $table->integer('total_payment')->comment('Pago mensual');
            $table->dateTime('approval_date')->nullable()->comment('Fecha de aprobación');
            $table->timestamps();
            $table->softDeletes();

            $ldap_database = env('DB_LDAP_DATABASE');
            $table->foreign('activity_report_id')
                ->references('id')
                ->on('activity_reports')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');

            $table->foreign('compliance_id')
                ->references('id')
                ->on('compliance')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');

            $table->foreign('supervisor_id')
                ->references('id')
                ->on("{$ldap_database}.users")
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');

            $table->foreign('contract_id')
                ->references('id')
                ->on('contracts')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');

            $table->foreign('payroll_id')
                ->references('id')
                ->on('payrolls')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('plans');
    }
}
