<?php

namespace App\Modules\Contractors\src\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Security\Subdirectorate;


class Payroll extends Model
{
    use SoftDeletes;

    const STATUS_PAYROLL = 'PAYROLL';

    protected $connection = 'mysql_contractors';

    protected $table = 'payrolls';

    protected $fillable = [
        'subdirection_id',
        'subdirector_id',
        'status',
        'year',
        'month',
        'totalPaymentPayroll',
        'paymentPlanCount',
        'reserve'
    ];

    protected $dates = ['deleted_at'];

    protected $casts = [
        'reserve' => 'bool',
    ];


    //relations

    public function subdirectorate()
    {
        return $this->hasOne(Subdirectorate::class, 'id', 'subdirection_id');
    }

    public function plans()
    {
        return $this->hasMany(PlanPayment::class, 'payroll_id', 'id');
    }

    public function getTotalPlansPaymentAttribute()
    {
        return $this->plans->sum('total_payment');
    }
}
