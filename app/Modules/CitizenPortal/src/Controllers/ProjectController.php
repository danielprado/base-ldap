<?php


namespace App\Modules\CitizenPortal\src\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\CitizenPortal\src\Constants\Roles;
use App\Modules\CitizenPortal\src\Models\Project;
use App\Modules\CitizenPortal\src\Resources\ProjectResource;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ProjectController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * @return JsonResponse
     */
    public function index()
    {
        $projects = Project::all();
        return $this->success_response(
            ProjectResource::collection($projects),
            Response::HTTP_OK
        );        
    }
}
