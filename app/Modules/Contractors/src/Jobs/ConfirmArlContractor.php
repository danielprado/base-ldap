<?php


namespace App\Modules\Contractors\src\Jobs;


use App\Modules\Contractors\src\Mail\ContractorLegalMail;
use App\Modules\Contractors\src\Mail\ContractorMail;
use App\Modules\Contractors\src\Mail\ContractorUpdateMail;
use App\Modules\Contractors\src\Models\Contract;
use App\Modules\Contractors\src\Models\Contractor;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use SendGrid\Mail\Mail;

class ConfirmArlContractor implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Contractor
     */
    private $user;

    /**
     * @var Contract
     */
    private $contract;

    /**
     * Create a new job instance.
     *
     * @param Contractor $user
     * @param Contract $contract
     */
    public function __construct(Contractor $user, Contract $contract)
    {
        $this->user = $user;
        $this->contract = $contract;
    }

    /**
     * Execute the job.
     *
     * @param Mail $mailer
     * @return void
     */
    public function handle(Mail $mailer)
    {
        if ( config('mail.mails.legal.email')  && filter_var( config('mail.mails.legal.email'), FILTER_VALIDATE_EMAIL) ) {
            $template = new ContractorLegalMail( $this->user, $this->contract );
            $mailer->setFrom(
                env('MAIL_CONTRACTS_FROM', 'portal.contratista@idrd.gov.co'),
                env('MAIL_CONTRACTS_FROM_NAME', 'Portal Contratista')
            );
            $mailer->setSubject("Certificado ARL {$this->user->getFullNameAttribute()} / {$this->contract->contract_type->name}");
            $mailer->addTo(config('mail.mails.legal.email'));
            $render = $template->render();
            $mailer->addContent(
                "text/html", $render
            );
            $sendgrid = new \SendGrid(config('mail.sendgrid.api'));
            try {
                $response = $sendgrid->send($mailer);
                sendgird_logger(
                    config('mail.mails.legal.email'),
                    "Certificado ARL {$this->user->getFullNameAttribute()} / {$this->contract->contract_type->name}",
                    $render,
                    [],
                    $response->statusCode(),
                    $response->body()
                );
            } catch (\Exception $e) {
                sendgird_logger(
                    config('mail.mails.legal.email'),
                    "[EXCEPTION] Certificado ARL {$this->user->getFullNameAttribute()} / {$this->contract->contract_type->name}",
                    $render,
                    [],
                    $e->getCode(),
                    $e->getMessage()
                );
            }
        }
    }
}
