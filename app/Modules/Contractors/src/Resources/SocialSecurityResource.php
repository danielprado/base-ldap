<?php


namespace App\Modules\Contractors\src\Resources;


use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;

class SocialSecurityResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Collection $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'                              => $this->resource['name'] ?? null,
            'payment_date'                      => isset($this->resource['payment_date']) ? Carbon::createFromFormat('Y-m-d', $this->resource['payment_date']) : null,
            'type_contributor'                  => $this->resource['type_contributor'] ?? null,
            'subtype_contributor'               => $this->resource['subtype_contributor'] ?? null,
            'operator'                          => $this->resource['operator'] ?? null,
            'period'                            => $this->resource['period'] ?? null,
            'ibc'                               => isset($this->resource['ibc']) ? floatval(str_replace(',', '', $this->resource['ibc'])) : null,
            'eps_value'                         => isset($this->resource['eps_value']) ? floatval(str_replace(',', '', $this->resource['eps_value'])) : null,
            'afp_value'                         => isset($this->resource['afp_value']) ? floatval(str_replace(',', '', $this->resource['afp_value'])) : null,
            'arl_value'                         => isset($this->resource['arl_value']) ? floatval(str_replace(',', '', $this->resource['arl_value'])) : null,
            'contribution_solidarity_value'     => isset($this->resource['contribution_solidarity_value']) ? floatval(str_replace(',', '', $this->resource['contribution_solidarity_value'])) : null,
            'contribution_subsistence_value'    => isset($this->resource['contribution_subsistence_value']) ? floatval(str_replace(',', '', $this->resource['contribution_subsistence_value'])) : null,
            'total'                             => $this->getTotal(),
            'status'                            => $this->resource['status'] ?? null,
            'status_color'                      => $this->getColor($this->resource['status']),
        ];
    }

    public function getColor($color): string
    {
        if ($color == 'PAGADO') {
            return 'success';
        } else {
            return 'error';
        }
    }

    public function getTotal()
    {
        $epsValue = isset($this->resource['eps_value']) ? floatval(str_replace(',', '', $this->resource['eps_value'])) : 0;
        $afpValue = isset($this->resource['afp_value']) ? floatval(str_replace(',', '', $this->resource['afp_value'])) : 0;
        $arlValue = isset($this->resource['arl_value']) ? floatval(str_replace(',', '', $this->resource['arl_value'])) : 0;
        return $epsValue + $afpValue + $arlValue;
    }
}
