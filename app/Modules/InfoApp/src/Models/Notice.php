<?php

namespace App\Modules\InfoApp\src\Models;

use Illuminate\Database\Eloquent\Model;


class Notice extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'mysql_infoapp';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'notices';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'image1',
        'image2',
        'nombre',
        'resumen',
        'descripcion',
        'categoria_id',
        'estado',
        'fechaSubida',
        'destacado',
    ];
}
