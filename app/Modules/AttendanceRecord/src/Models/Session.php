<?php

namespace App\Modules\AttendanceRecord\src\Models;

use Illuminate\Database\Eloquent\Model;

class Session extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_recreovia';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Sesiones';
    protected $primaryKey = 'Id';


    public function recreopersonas()
    {
        return $this->belongsTo(Recreopersonas::class, 'Id_Recreopersona');
    }

    public function cronograma()
    {
        return $this->belongsTo(Cronograma::class,'Id_Cronograma');
    }
}
