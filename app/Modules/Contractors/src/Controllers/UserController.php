<?php


namespace App\Modules\Contractors\src\Controllers;


use Adldap\AdldapInterface;
use Adldap\Auth\BindException;
use App\Exceptions\PasswordExpiredException;
use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Controller;
use App\Models\Security\User;
use App\Modules\Contractors\src\Constants\Roles;
use App\Modules\Contractors\src\Models\WareHouse;
use App\Modules\Contractors\src\Request\LoginRequest;
use App\Modules\Contractors\src\Resources\WareHouseResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Laravel\Passport\Http\Controllers\AccessTokenController;
use Silber\Bouncer\Database\Role;
use Symfony\Bridge\PsrHttpMessage\Factory\DiactorosFactory;

class UserController extends LoginController
{
    /**
     * Initialise common request params
     *
     * @param AdldapInterface $ldap
     */
    public function __construct(AdldapInterface $ldap)
    {
        parent::__construct($ldap);
    }


    public function getToken(Request $request)
    {
        $user = User::where('username', $request->get('username'))->first();
        if (is_null($user)) {
            return $this->error_response(
                __('auth.failed'),
                Response::HTTP_UNAUTHORIZED
            );
        } else if ($this->validatePermissions($user) ) {
            $request->request->add([
                'client_id'     =>  config('app.passport.client_id'),
                'client_secret' =>  config('app.passport.client_secret'),
                'grant_type'    =>  config('app.passport.grant_type'),
            ]);
            $data = (new DiactorosFactory)->createRequest( $request );
            return app( AccessTokenController::class )->issueToken($data);
        } else {
            return $this->error_response(
                __('validation.handler.unauthorized'),
                Response::HTTP_UNPROCESSABLE_ENTITY
            );
        }
    }

    /**
     * Attempt to log the user into the application.
     *
     * @param Request $request
     * @return bool|JsonResponse
     * @throws PasswordExpiredException
     */
    protected function attemptLogin(Request $request)
    {
        try {
            return auth()->attempt($this->credentials($request), $request->get('remember'));
        } catch (BindException $e) {
            $user = User::active()->where('username', $request->get( $this->username() ))->first();
            if (is_null($user)) {
                return false;
            } else if ( $this->validatePermissions($user) ) {
                if ( $user->is_locked ) {
                    throw new PasswordExpiredException(trans('passwords.inactive'));
                }
                if ( $user->password_expired ) {
                    throw new PasswordExpiredException(trans('passwords.expired'));
                }
                return Hash::check($request->get('password'), $user->password);
            }
            return false;
        }
    }

    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        return $request->only($this->username(), 'password');
    }

    public function validatePermissions(User $user)
    {
        return $user->isA(...Roles::all());
    }

    /**
     * @return JsonResponse
     */
    public function drawer()
    {
        $user = auth('api')->user();
        $menu = $user->isAn(Roles::ROLE_CONTRACTOR) && $user->isAn(...Roles::contractorAndOther()) ?
            collect([
                [
                    'icon'  =>  'mdi-view-dashboard',
                    'title' =>  'Dashboard',
                    'to'    =>  [ 'name' => 'index' ],
                    'exact' =>  true,
                    'can'   =>  $user->isAn(...Roles::all())
                ],
                [
                    'icon'  =>  $user->isAn(Roles::ROLE_ADMIN) ? 'mdi-shield-account' : 'mdi-file-sign',
                    'title' =>  $user->isAn(Roles::ROLE_ADMIN) ? 'Administrador' : 'Contratos',
                    'exact' =>  false,
                    'can'   =>  $user->isAn(...Roles::withoutConsultingAndContractor()),
                    'children' => array_values(collect([
                        [
                            'icon'  =>  'mdi-account-multiple-plus',
                            'title' =>  'Usuarios',
                            'to'    =>  [ 'name' => 'user-admin' ],
                            'exact' =>  true,
                            'can'   =>  $user->isAn(Roles::ROLE_ADMIN)
                        ],
                        [
                            'icon'  =>  'mdi-account-check',
                            'title' =>  'Contratistas',
                            'to'    =>  [ 'name' => 'contractors' ],
                            'exact' =>  true,
                            'can'   =>  $user->isAn(...Roles::withoutConsultingAndContractor())
                        ],
                        [
                            'icon'  =>  'mdi-file',
                            'title' =>  'Reportes',
                            'to'    =>  [ 'name' => 'reports' ],
                            'exact' =>  true,
                            'can'   =>  $user->isAn(...Roles::withoutConsultingAndContractor())
                        ],
                    ])->where('can', true)->toArray())
                ],
                [
                    'icon'  =>  'mdi-account-hard-hat',
                    'title' =>  'Contratista',
                    'exact' =>  false,
                    'can'   =>  $user->isAn(Roles::ROLE_CONTRACTOR),
                    'children' => array_values(collect([
                        [
                            'icon'  =>  'mdi-database-edit',
                            'title' =>  'Actualización de datos',
                            'to'    =>  [ 'name' => 'data-update' ],
                            'exact' =>  true,
                            'can'   =>  $user->isAn(Roles::ROLE_CONTRACTOR),
                        ],
                        [
                            'title' => 'Mis informes',
                            'icon'  =>  'mdi-file-document-multiple',
                            'to'    =>  ['name' => 'activity-report'],
                            'exact' =>  true,
                            'can' =>  $user->isAn(Roles::ROLE_CONTRACTOR),
                        ],
                        [
                            'title' => 'Cumplimiento Tributario',
                            'icon'  =>  'mdi-file-document-multiple',
                            'to'    =>  [ 'name' => 'payments' ],
                            'exact' =>  true,
                            'can' =>  $user->isAn(Roles::ROLE_CONTRACTOR),
                        ],
                        [
                            'icon'  =>  'mdi-certificate',
                            'title' =>  'Certificaciones',
                            'to'    =>  [ 'name' => 'certificates' ],
                            'exact' =>  true,
                            'can'   =>  $user->isAn(Roles::ROLE_CONTRACTOR)
                        ],
                        [
                            'icon'  =>  'mdi-account-cash',
                            'title' =>  'Consultar Estado de Pago',
                            'to'    =>  [ 'name' => 'paymentstatus' ],
                            'exact' =>  true,
                            'can'   =>  $user->isAn(Roles::ROLE_CONTRACTOR)
                        ],
                        [
                            'icon'  =>  'mdi-book-open',
                            'title' =>  'Instructivo',
                            'to'    =>  [ 'name' => 'instructive' ],
                            'exact' =>  true,
                            'can'   =>  $user->isAn(Roles::ROLE_CONTRACTOR)
                        ],
                        [
                            'icon'  =>  'mdi-card-text',
                            'title' =>  'Carnet Institucional',
                            'to'    =>  [ 'name' => 'businnesscard' ],
                            'exact' =>  true,
                            'can'   =>  $user->isAn(Roles::ROLE_CONTRACTOR)
                        ]
                    ])->where('can', true)->toArray())
                ],
                [
                    'icon'  =>  'mdi-account-supervisor',
                    'title' =>  $user->isAn(Roles::ROLE_SUPERVISOR_SUPPORT) ? 'Supervisor (Apoyo)' : 'Supervisor',
                    'exact' =>  false,
                    'can'   =>  $user->isAn(Roles::ROLE_SUPERVISOR, Roles::ROLE_SUPERVISOR_SUPPORT),
                    'children' => array_values(collect([
                        [
                            'icon'  =>  'mdi-file-sign',
                            'title' =>  'Firma',
                            'to'    =>  [ 'name' => 'signature' ],
                            'exact' =>  true,
                            'can'   =>  $user->isAn(Roles::ROLE_SUPERVISOR)
                        ],
                        [
                            'icon'  =>  'mdi-file-document-arrow-right',
                            'title' =>  'Actas de Inicio',
                            'to'    =>  [ 'name' => 'documents-init' ],
                            'exact' =>  true,
                            'can'   =>  $user->isAn(Roles::ROLE_ADMIN, Roles::ROLE_SUPERVISOR, Roles::ROLE_SUPERVISOR_SUPPORT)
                        ],
                        [
                            'icon'  =>  'mdi-file-document-multiple',
                            'title' =>  'Tramite de pagos',
                            'to'    =>  [ 'name' => 'review-activity-report' ],
                            'exact' =>  true,
                            'can'   =>  $user->isAn(Roles::ROLE_ADMIN, Roles::ROLE_SUPERVISOR, Roles::ROLE_SUPERVISOR_SUPPORT)
                        ],
                        [
                            'icon'  =>  'mdi-cash-multiple',
                            'title' =>  'Plan de pagos',
                            'to'    =>  [ 'name' => 'compliances' ],
                            'exact' =>  true,
                            'can'   =>  $user->isAn(Roles::ROLE_ADMIN, Roles::ROLE_SUPERVISOR)
                        ],
                    ])->where('can', true)->toArray())
                ],
                [
                    'icon'  =>  'mdi-account-tie',
                    'title' =>  'Apoyo Técnico',
                    'exact' =>  false,
                    'can'   =>  $user->isAn(Roles::ROLE_SUPERVISOR_TECHNICAL_SUPPORT),
                    'children' => array_values(collect([
                        [
                            'icon'  =>  'mdi-file-document-multiple',
                            'title' =>  'Tramite de pagos',
                            'to'    =>  [ 'name' => 'review-activity-report' ],
                            'exact' =>  true,
                            'can'   =>  $user->isAn(Roles::ROLE_ADMIN, Roles::ROLE_SUPERVISOR_TECHNICAL_SUPPORT)
                        ]
                    ])->where('can', true)->toArray())
                ],
                [
                    'icon'  =>  'mdi-cash-multiple',
                    'title' =>  'Orden de pago',
                    'to'    =>  [ 'name' => 'payrolls' ],
                    'exact' =>  true,
                    'can'   =>  $user->isAn(Roles::ROLE_AUTHORIZING_OFFICER)
                ],
                [
                    'icon'  =>  'mdi-bank-circle',
                    'title' =>  'Planillas',
                    'to'    =>  [ 'name' => 'payrolls' ],
                    'exact' =>  true,
                    'can'   =>  $user->isAn(Roles::ROLE_OBSERVER_PAC)
                ],
                [
                    'icon'  =>  $user->isAn(Roles::ROLE_ACCOUNTING) ? 'mdi-bank-circle-outline' : 'mdi-hand-coin-outline',
                    'title' =>  $user->isAn(Roles::ROLE_ACCOUNTING) ? 'Contabilidad' : 'Tesorería',
                    'exact' =>  false,
                    'can'   =>  $user->isAn(Roles::ROLE_ACCOUNTING, Roles::ROLE_TREASURY),
                    'children' => array_values(collect([
                        [
                            'icon'  =>  'mdi-bank-circle',
                            'title' =>  'Planillas',
                            'to'    =>  [ 'name' => 'payrolls' ],
                            'exact' =>  true,
                            'can'   =>  $user->isAn(Roles::ROLE_ACCOUNTING, Roles::ROLE_TREASURY)
                        ],
                        [
                            'icon'  =>  'mdi-book',
                            'title' =>  'Bitácora',
                            'to'    =>  [ 'name' => 'binnacle' ],
                            'exact' =>  true,
                            'can'   =>  $user->isAn(Roles::ROLE_ACCOUNTING)
                        ],
                        [
                            'icon'  =>  'mdi-calendar-check',
                            'title' =>  'Causación',
                            'to'    =>  [ 'name' => 'invoicing' ],
                            'exact' =>  true,
                            'can'   =>  $user->isAn(Roles::ROLE_ACCOUNTING)
                        ],
                        [
                            'icon'  =>  'mdi-gavel',
                            'title' =>  'Embargos',
                            'to'    =>  [ 'name' => 'contractors' ],
                            'exact' =>  true,
                            'can'   =>  $user->isAn(Roles::ROLE_TREASURY)
                        ],
                    ])->where('can', true)->toArray())
                ],
                [
                    'icon'  =>  'mdi-invoice-list',
                    'title' =>  'Jornadas Contratos',
                    'to'    =>  [ 'name' => 'sessions' ],
                    'exact' =>  true,
                    'can'   =>  $user->isAn(Roles::ROLE_ADMIN, Roles::ROLE_CYCLE_PATH_GUARDIAN)
                ]
            ]) : collect([
                [
                    'icon'  =>  'mdi-account-multiple-plus',
                    'title' =>  'Usuarios',
                    'to'    =>  [ 'name' => 'user-admin' ],
                    'exact' =>  true,
                    'can'   =>  $user->isAn(Roles::ROLE_ADMIN)
                ],
                [
                    'icon'  =>  'mdi-view-dashboard',
                    'title' =>  'Dashboard',
                    'to'    =>  [ 'name' => 'index' ],
                    'exact' =>  true,
                    'can'   =>  $user->isAn(...Roles::all())
                ],
                [
                    'icon'  =>  'mdi-account-check',
                    'title' =>  'Contratistas',
                    'to'    =>  [ 'name' => 'contractors' ],
                    'exact' =>  true,
                    'can'   =>  $user->isAn(...Roles::withoutConsultingAndContractor())
                ],
                [
                    'icon'  =>  'mdi-file',
                    'title' =>  'Reportes',
                    'to'    =>  [ 'name' => 'reports' ],
                    'exact' =>  true,
                    'can'   =>  $user->isAn(...Roles::withoutConsultingAndContractor())
                ],
                [
                    'icon'  =>  'mdi-account-tie',
                    'title' =>  $user->isAn(Roles::ROLE_SUPERVISOR_SUPPORT) ? 'Supervisor (Apoyo)' : 'Supervisor',
                    'exact' =>  false,
                    'can'   =>  $user->isAn(Roles::ROLE_SUPERVISOR, Roles::ROLE_SUPERVISOR_SUPPORT),
                    'children' => array_values(collect([
                        [
                            'icon'  =>  'mdi-file-sign',
                            'title' =>  'Firma',
                            'to'    =>  [ 'name' => 'signature' ],
                            'exact' =>  true,
                            'can'   =>  $user->isAn(Roles::ROLE_SUPERVISOR)
                        ],
                        [
                            'icon'  =>  'mdi-file-document-arrow-right',
                            'title' =>  'Actas de Inicio',
                            'to'    =>  [ 'name' => 'documents-init' ],
                            'exact' =>  true,
                            'can'   =>  $user->isAn(Roles::ROLE_ADMIN, Roles::ROLE_SUPERVISOR, Roles::ROLE_SUPERVISOR_SUPPORT)
                        ],
                        [
                            'icon'  =>  'mdi-file-document-multiple',
                            'title' =>  'Tramite de pagos',
                            'to'    =>  [ 'name' => 'review-activity-report' ],
                            'exact' =>  true,
                            'can'   =>  $user->isAn(Roles::ROLE_ADMIN, Roles::ROLE_SUPERVISOR, Roles::ROLE_SUPERVISOR_SUPPORT)
                        ],
                        [
                            'icon'  =>  'mdi-cash-multiple',
                            'title' =>  'Plan de pagos',
                            'to'    =>  [ 'name' => 'compliances' ],
                            'exact' =>  true,
                            'can'   =>  $user->isAn(Roles::ROLE_ADMIN, Roles::ROLE_SUPERVISOR)
                        ],
                    ])->where('can', true)->toArray())
                ],
                [
                    'icon'  =>  'mdi-account-tie',
                    'title' =>  'Apoyo Técnico',
                    'exact' =>  false,
                    'can'   =>  $user->isAn(Roles::ROLE_SUPERVISOR_TECHNICAL_SUPPORT),
                    'children' => array_values(collect([
                        [
                            'icon'  =>  'mdi-file-document-multiple',
                            'title' =>  'Tramite de pagos',
                            'to'    =>  [ 'name' => 'review-activity-report' ],
                            'exact' =>  true,
                            'can'   =>  $user->isAn(Roles::ROLE_ADMIN, Roles::ROLE_SUPERVISOR_TECHNICAL_SUPPORT)
                        ]
                    ])->where('can', true)->toArray())
                ],
                [
                    'icon'  =>  'mdi-database-edit',
                    'title' =>  'Actualización de datos',
                    'to'    =>  [ 'name' => 'data-update' ],
                    'exact' =>  true,
                    'can'   =>  $user->isAn(Roles::ROLE_CONTRACTOR)
                ],
                [
                    'icon'  =>  'mdi-file-document-multiple',
                    'title' =>  'Informe de actividades',
                    'exact' =>  true,
                    'can'   =>  $user->isAn(Roles::ROLE_CONTRACTOR),
                    'children' => array_values(collect([
                        [
                            'title' => 'Mis informes',
                            'icon'  =>  'mdi-file-document-multiple',
                            'to'    =>  ['name' => 'activity-report'],
                            'exact' =>  true,
                            'can' =>  $user->isAn(Roles::ROLE_CONTRACTOR),
                        ],
                        [
                            'title' => 'Cumplimiento Tributario',
                            'icon'  =>  'mdi-file-document-multiple',
                            'to'    =>  [ 'name' => 'payments' ],
                            'exact' =>  true,
                            'can' =>  $user->isAn(Roles::ROLE_CONTRACTOR),
                        ],
                    ])->where('can', true)->toArray())
                ],
                [
                    'icon'  =>  'mdi-certificate',
                    'title' =>  'Certificaciones',
                    'to'    =>  [ 'name' => 'certificates' ],
                    'exact' =>  true,
                    'can'   =>  $user->isAn(Roles::ROLE_CONTRACTOR)
                ],
                [
                    'icon'  =>  'mdi-account-cash',
                    'title' =>  'Consultar Estado de Pago',
                    'to'    =>  [ 'name' => 'paymentstatus' ],
                    'exact' =>  true,
                    'can'   =>  $user->isAn(Roles::ROLE_CONTRACTOR)
                ],
                [
                    'icon'  =>  'mdi-book-open',
                    'title' =>  'Instructivo',
                    'to'    =>  [ 'name' => 'instructive' ],
                    'exact' =>  true,
                    'can'   =>  $user->isAn(Roles::ROLE_CONTRACTOR)
                ],
                [
                    'icon'  =>  'mdi-cash-multiple',
                    'title' =>  'Orden de pago',
                    'to'    =>  [ 'name' => 'payrolls' ],
                    'exact' =>  true,
                    'can'   =>  $user->isAn(Roles::ROLE_AUTHORIZING_OFFICER)
                ],
                [
                    'icon'  =>  'mdi-bank-circle',
                    'title' =>  $user->isAn(Roles::ROLE_ACCOUNTING) ? 'Contabilidad' : 'Tesorería',
                    'exact' =>  false,
                    'can'   =>  $user->isAn(Roles::ROLE_ACCOUNTING, Roles::ROLE_TREASURY),
                    'children' => array_values(collect([
                        [
                            'icon'  =>  'mdi-bank-circle',
                            'title' =>  'Planillas',
                            'to'    =>  [ 'name' => 'payrolls' ],
                            'exact' =>  true,
                            'can'   =>  $user->isAn(Roles::ROLE_ACCOUNTING, Roles::ROLE_TREASURY)
                        ],
                        [
                            'icon'  =>  'mdi-book',
                            'title' =>  'Bitácora',
                            'to'    =>  [ 'name' => 'binnacle' ],
                            'exact' =>  true,
                            'can'   =>  $user->isAn(Roles::ROLE_ACCOUNTING)
                        ],
                        [
                            'icon'  =>  'mdi-calendar-check',
                            'title' =>  'Causación',
                            'to'    =>  [ 'name' => 'invoicing' ],
                            'exact' =>  true,
                            'can'   =>  $user->isAn(Roles::ROLE_ACCOUNTING)
                        ],
                        [
                            'icon'  =>  'mdi-gavel',
                            'title' =>  'Embargos',
                            'to'    =>  [ 'name' => 'contractors' ],
                            'exact' =>  true,
                            'can'   =>  $user->isAn(Roles::ROLE_TREASURY)
                        ],
                    ])->where('can', true)->toArray())
                ],
                [
                    'icon'  =>  'mdi-card-text',
                    'title' =>  'Carnet Institucional',
                    'to'    =>  [ 'name' => 'businnesscard' ],
                    'exact' =>  true,
                    'can'   =>  $user->isAn(Roles::ROLE_CONTRACTOR)
                ],
                [
                    'icon'  =>  'mdi-invoice-list',
                    'title' =>  'Jornadas Contratos',
                    'to'    =>  [ 'name' => 'sessions' ],
                    'exact' =>  true,
                    'can'   =>  $user->isAn(Roles::ROLE_ADMIN, Roles::ROLE_CYCLE_PATH_GUARDIAN)
                ]
            ]);
        return $this->success_message( array_values( $menu->where('can', true)->toArray() ) );
    }

    /**
     * @return JsonResponse
     */
    public function permissions()
    {
        return $this->success_message([
            ['name'  =>  'contractors-portal-super-admin', 'can' => auth('api')->user()->isA(Roles::ROLE_ADMIN)],
            ['name'  =>  'contractors-portal-arl'        , 'can' => auth('api')->user()->isA(Roles::ROLE_ARL)],
            ['name'  =>  'contractors-portal-hiring'     , 'can' => auth('api')->user()->isA(Roles::ROLE_HIRING)],
            ['name'  =>  'contractors-portal-legal'      , 'can' => auth('api')->user()->isA(Roles::ROLE_LEGAL)],
            ['name'  =>  'contractors-portal-rp'         , 'can' => auth('api')->user()->isA(Roles::ROLE_RP)],
            ['name'  =>  'contractors-portal-third-party', 'can' => auth('api')->user()->isA(Roles::ROLE_THIRD_PARTY)],
            ['name'  =>  'contractors-portal-observer'   , 'can' => auth('api')->user()->isA(Roles::ROLE_OBSERVER)],
            ['name'  =>  'contractors-portal-consultation' , 'can' => auth('api')->user()->isA(Roles::ROLE_CONSULTATION)],
            ['name'  =>  'contractors-portal-supervisor' , 'can' => auth('api')->user()->isA(Roles::ROLE_SUPERVISOR)],
            ['name'  =>  'contractors-portal-contractor' , 'can' => auth('api')->user()->isA(Roles::ROLE_CONTRACTOR)],
            ['name'  =>  'contractors-portal-supervisor-support' , 'can' => auth('api')->user()->isA(Roles::ROLE_SUPERVISOR_SUPPORT)],
            ['name'  =>  'contractors-accounting' , 'can' => auth('api')->user()->isA(Roles::ROLE_ACCOUNTING)],
            ['name'  =>  'contractors-treasury' , 'can' => auth('api')->user()->isA(Roles::ROLE_TREASURY)],
            ['name'  =>  'contractors-binnacle' , 'can' => auth('api')->user()->isA(Roles::ROLE_BINNACLE)],
            ['name'  =>  'contractors-portal-authorizing-officer' , 'can' => auth('api')->user()->isA(Roles::ROLE_AUTHORIZING_OFFICER)],
            ['name'  =>  'contractors-portal-supervisor-technical-support' , 'can' => auth('api')->user()->isA(Roles::ROLE_SUPERVISOR_TECHNICAL_SUPPORT)],
            ['name'  =>  'contractors-portal-cycle-path-guardian' , 'can' => auth('api')->user()->isA(Roles::ROLE_CYCLE_PATH_GUARDIAN)]
        ]);
    }

    public function oracle(Request $request)
    {
        $data = WareHouse::query()
            ->when($request->has('document'), function ($query) use ($request) {
                return $query->where('ter_carg', $request->get('document'));
            })->where('act_acti', '=', 'A')
            ->paginate($this->per_page);
        return  $this->success_response(
            WareHouseResource::collection( $data ),
            Response::HTTP_OK,
            [
                'headers'   =>  WareHouseResource::headers(),
            ]
        );
    }

    public function countOracle(Request $request)
    {
        $count = WareHouse::query()
            ->where('ter_carg', $request->get('document'))
            ->where('act_acti', '=', 'A')
            ->count();
        return  $this->success_message($count);
    }

    public function excelOracle(Request $request)
    {
        $data = WareHouse::query()
            ->where('ter_carg', $request->get('document'))
            ->where('act_acti', '=', 'A')
            ->get();
        return  $this->success_response(
            WareHouseResource::collection( $data )
        );
    }
}
