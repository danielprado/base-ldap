<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldsToTblActivityReports extends Migration
{

     /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'mysql_contractors';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->table('activity_reports', function (Blueprint $table) {
            $table->integer('sessionsCompleted')->nullable()->after('name_reviewer_technical');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->table('activity_reports', function (Blueprint $table) {
            $table->dropColumn('sessionsCompleted');
        });
    }
}
