<?php

namespace App\Modules\Certifications\src\Exports;

use App\Models\Security\DocumentType;
use App\Models\Security\User;
use App\Modules\Certifications\src\Models\ContractSupport;
use App\Traits\AppendHeaderToExcel;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Builder;
use Imtigger\LaravelJobStatus\JobStatus;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SupportExport implements FromQuery, WithTitle, WithEvents, WithHeadings, WithMapping, WithColumnFormatting, ShouldQueue, ShouldAutoSize
{
    use Exportable, AppendHeaderToExcel;

    /**
     * @var array
     */
    private $request;

    /**
     * @var int
     */
    private $rowNumb = 2;

    /**
     * @var User
     */
    private $user;

    public function __construct(array $request, $job, $user)
    {
        $this->user = $user;
        $this->request = $request;
        update_status_job($job, JobStatus::STATUS_EXECUTING, 'excel-certifications-module');
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return ContractSupport::query()->whereKey($this->request['supports'])->orderBy('created_at', 'desc');
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $sheet) {
                $this->setHeader($sheet->sheet, 'SOPORTES - CERTIFICACION DE CONTRATOS', 'A1:L1', 'L');
                $high = $sheet->getSheet()->getDelegate()->getHighestRow();
                $sheet->getSheet()->getDelegate()
                    ->getStyle("I8:I$high")->getAlignment()
                    ->setWrapText(true)
                    ->setVertical(Alignment::VERTICAL_CENTER)
                    ->setHorizontal(Alignment::HORIZONTAL_LEFT);
                $sheet->getSheet()->getDelegate()
                    ->getStyle("J8:J$high")->getAlignment()
                    ->setWrapText(true)
                    ->setVertical(Alignment::VERTICAL_CENTER)
                    ->setHorizontal(Alignment::HORIZONTAL_LEFT);
            }
        ];
    }

    public function headings(): array
    {
        return [
            'DOCUMENTO SOLICITANTE',
            'TIPO DE DOCUMENTO',
            'NOMBRE',
            'CORREO',
            'TELÉFONO',
            'NÚMERO CONTRATO',
            'AÑO CONTRATO',
            'ESTADO SOLICITUD',
            'DESCRIPCIÓN',
            'SOLUCIÓN',
            'FECHA DE CREACIÓN',
            'FECHA DE MODIFICACIÓN',
        ];
    }

    public function title(): string
    {
        return 'SOPORTES';
    }

    public function columnFormats(): array
    {
        return [
            'I' => NumberFormat::FORMAT_TEXT,
            'J' => NumberFormat::FORMAT_TEXT,
            'K' => NumberFormat::FORMAT_DATE_YYYYMMDD2.' h'.NumberFormat::FORMAT_DATE_TIME4,
            'L' => NumberFormat::FORMAT_DATE_YYYYMMDD2.' h'.NumberFormat::FORMAT_DATE_TIME4,
        ];
    }

    public function map($row): array
    {
        return [
            'document'          => $row['Documento_Solicitante'] ?? null,
            'document_type'     => $this->getDocumentType($row['Tipo_Documento_Solicitante_Id']),
            'name'              => isset($row['Nombre_Solicitante']) ? ucwords($row['Nombre_Solicitante']) : null,
            'email'             => $row['Correo_Solicitante'] ?? null,
            'phone'             => $row['Telefono_Solicitante'] ?? null,
            'contract_number'   => $row['Numero_Contrato'] ?? null,
            'contract_year'     => $row['Anio_Contrato'] ?? null,
            'state'             => $this->getState($row),
            'description'       => $row['Descripcion_Solicitud'] ?? null,
            'solution'          => $row['Solucion'] ?? null,
            'created_at'        =>  isset($row['created_at']) ? date_time_to_excel(Carbon::parse($row['created_at'])) : null,
            'updated_at'        =>  isset($row['updated_at']) ? date_time_to_excel(Carbon::parse($row['updated_at'])) : null,
        ];
    }

    public function getState($row): string
    {
        $state = isset($row['Estado']) ? (int) $row['Estado'] : null;
        return !is_null($state) && $state != 1 ? 'SOLUCIONADO' : 'SIN SOLUCIÓN';
    }

    public function getDocumentType($id)
    {
        $document_type_id = isset($id) ? (int) $id : null;
        if ($document_type_id) {
            $document_type = DocumentType::query()->where('Id_TipoDocumento', $document_type_id)->pluck('Descripcion_TipoDocumento');
            return $document_type[0];
        }
        return  $document_type_id;
    }
}
