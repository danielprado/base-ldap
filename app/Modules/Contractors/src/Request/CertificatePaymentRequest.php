<?php


namespace App\Modules\Contractors\src\Request;


use Illuminate\Foundation\Http\FormRequest;
class CertificatePaymentRequest extends FormRequest{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function rules(){
        return [
            'document'  => 'required|numeric',
            'birthdate' => 'required|date|date_format:Y-m-d',
            'contract'  => 'required|numeric|digits:4',
            'year'      => 'required|numeric|date_format:Y',
            'code'      => 'nullable|numeric'
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'document'      => 'Número de documento',
            'birthdate'     => 'Fecha de nacimiento',
            'contract'      => 'Número de contrato',
            'year'          => 'Año del contrato',
            'code'          => 'Código de verificación',
        ];
    }
}
