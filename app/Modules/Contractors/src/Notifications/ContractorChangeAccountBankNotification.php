<?php


namespace App\Modules\Contractors\src\Notifications;


use App\Modules\Contractors\src\Models\Contract;
use App\Modules\Contractors\src\Models\Contractor;
use App\Modules\Contractors\src\Models\ContractorBank;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class ContractorChangeAccountBankNotification extends Notification
{
    use Queueable;

    /**
     * @var Contractor
     */
    private $contractor;

    /**
     * @var Contract
     */
    private $contract;

    /**
     * Create a new notification instance.
     *
     * @param Contractor $contractor
     * @param Contract $contract
     */
    public function __construct(Contractor $contractor, Contract $contract)
    {
        $this->contractor = $contractor;
        $this->contract = $contract;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $first = isset( $this->contractor->name ) ? $this->contractor->name : '';
        $second = isset( $this->contractor->surname ) ? $this->contractor->surname : '';
        $number = $this->contract->contract ?? '000';
        $type = $this->contract->contract_type->name ?? '';
        $name = "$first $second";
        $subject = "{$name} / {$type} / {$number}";
        return [
            'title'         => 'Actualización de cuenta bancaria',
            'subject'       => $subject,
            'user'          => 'SYSTEM',
            'created_at'    => now()->format('Y-m-d H:i:s'),
        ];
    }
}
