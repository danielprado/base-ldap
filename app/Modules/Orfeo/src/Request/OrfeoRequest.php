<?php

namespace App\Modules\Orfeo\src\Request;

use Illuminate\Foundation\Http\FormRequest;

/**
 * @bodyParam name string required Nombre de la localidad máximo 50 caracteres. Example: USAQUÉN
 */
class OrfeoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'code'  => 'required|string|max:50',
        ];
    }
}
