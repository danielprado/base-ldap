<?php


namespace App\Modules\Contractors\src\Controllers;


use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Modules\Contractors\src\Request\ConsultaRequest;
use App\Modules\Contractors\src\Request\ValidacionUsuarioRequest;
use App\Modules\Contractors\src\Models\Contractor;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Modules\Contractors\src\Models\Certification;
use App\Modules\Contractors\src\Jobs\VerificationCodeTributario;
use App\Modules\Contractors\src\Request\ValidacionRequest;
use App\Modules\Contractors\src\Request\IVAICACONRequest;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use App\Helpers\FPDF;
use LaravelQRCode\Facades\QRCode;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Storage;
use Luecano\NumeroALetras\NumeroALetras;

class Certificados_TributariosController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function index(ValidacionUsuarioRequest $request){
        try {
            $contractor = Contractor::query()
            ->where('document', $request->get('document'))
            ->where('birthdate', $request->get('birthdate'))
            ->firstOrFail();

            if ($request->get('type') == 'TRB' && $request->get('year') == '2024') {
                return $this->error_response(
                    'El certificado de ingresos y retenciones para el año seleccionado no se encuentra disponible, Por favor inténtelo más tarde.',
                    422
                );
            }

            $certification = Certification::firstOrCreate([
                "document"=>$request->get('document'),
                "year"=>$request->get('year'),
                "type"=>"TRB"
            ],[
                "name"  => $contractor->full_name,
                "document" => $request->get('document'),
                "contractor_id" => $contractor->id,
                "year" => $request->get('year'),
                "type"=>"TRB"
            ]);
            $this->dispatch(new VerificationCodeTributario($contractor, $certification));
            $email=mask_email($contractor->email);
            return $this->success_message("Hemos enviado un código de verificación al correo $email.");

        } catch (\Exception $exception) {
            if ($exception instanceof ModelNotFoundException) {
                return $this->error_response(
                    'Los datos ingresados no son correctos. Verifíquelos e ingréselos nuevamente.',
                    422
                );
            }

            return $this->error_response(
                'No podemos realizar la consulta en este momento. Por favor inténtelo más tarde.',
                422,
                $exception->getMessage()
            );
        }

    }

    public function validarUsuario(ValidacionRequest $request){
        try {
            $certification=Certification::where("document", $request->get("document"))
                ->where("code", $request->get("code"))
                ->where("type", "TRB")
                ->firstOrFail();
            return $this->conexionSeven($request, $certification);
        } catch (\Exception $exception) {
            if ($exception instanceof ModelNotFoundException) {
                return $this->error_response(
                    'El código ingresado no coincide. Verifíquelo nuevamente.',
                    422
                );
            }

            return $this->error_response(
                'No podemos realizar la consulta en este momento. Por favor inténtelo más tarde.',
                422,
                $exception->getMessage()
            );
        }

    }

    public function conexionSeven(ValidacionRequest $request, Certification $certification){
        $http = new Client([
            'base_uri' => env('URL_DOCKER_SERVER'),
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ],
        ]);
        $response=$http->post("/api/contractors-portal/certificado-tributario/oracle", [
            "json" => $request->all(),
        ]);

        $data=json_decode($response->getBody()->getContents(), true);

        if (!isset($data["data"][0])){
            return $this->error_response(
                "NO SE ENCONTRÓ INFORMACIÓN TRIBUTARIA del año {$request->get('year')} para el contratista identificado con el número de documento {$request->get('document')}"
            );
        }
        if (in_array($request->get("year"), [now()->year])){
            return $this->error_response(
                "LA CERTIFICACIÓN TRIBUTARIA SOLICITADA PARA EL AÑO " . now()->year . " AÚN NO SE ENCUENTRA DISPONIBLE"
            );
        }
        $Periodo=null;
        if (in_array($request->get("type"), ["IVA","ICA","CON"])){
            $Periodo=$this->getMes($request->get("mesini"))."-".$this->getMes($request->get("mesfin"));
        }
        $pdf=$this->createPDF($data["data"], $certification, $Periodo, $request->get("type"));
        return $this->success_message([
            "file_name"=>"Ingresos_Retenciones.pdf",
            "file"=>"data:application/pdf;base64,".base64_encode($pdf)
        ]);
    }

    public function consultaSV(ConsultaRequest $request){
        if (in_array($request->get("type"), ["ICA", "IVA", "CON"])) {
            $query = DB::connection("oracle_ws")
                ->table('SEVEN.PO_FACTU AS F')
                ->selectRaw('F.PVD_CODI, F.FAC_ANOP, P.PVR_NOCO, LIQ_NOMB')
                ->selectRaw('(SELECT SUM(A1.DFA_VALO) FROM SEVEN.PO_DFACT A1 WHERE A1.PVD_CODI = F.PVD_CODI AND A1.DFA_ANOP = F.FAC_ANOP) AS VAL_BRUT')
                ->selectRaw('SUM(LIQ_BASE) AS VAL_BASE')
                ->selectRaw("CASE WHEN LIQ_NOMB = 'TOTAL' THEN 0 ELSE SUM(LIQ_VALO) * -1 END AS VAL_RETE")
                ->join('SEVEN.PO_DVFAC AS D', function($join) {
                    $join->on('F.FAC_CONT', '=', 'D.FAC_CONT')
                        ->on('F.EMP_CODI', '=', 'D.EMP_CODI');
                })
                ->join('SEVEN.PO_PVDOR AS P', function($join) {
                    $join->on('P.EMP_CODI', '=', 'F.EMP_CODI')
                        ->on('P.PVD_CODI', '=', 'F.PVD_CODI');
                })
                ->where('F.PVD_CODI', $request->get('document'))
                ->where('F.FAC_ANOP', $request->get('year'));

            $query->whereBetween('F.FAC_MESP', [$request->get('mesini'), $request->get('mesfin')]);

            if ($request->get("type") == "ICA") {
                $query->whereIn('LIQ_CODI', ['TOTAL', 'ICA', 'RETEICA', 'ICAINDEPE']);
            } elseif ($request->get("type") == "IVA") {
                $query->whereIn('LIQ_CODI', ['TOTAL', 'IVA', 'RETEIVA']);
            } elseif ($request->get("type") == "CON") {
                $query->whereIn('LIQ_CODI', ['TOTAL', 'ESTAMPROC', 'ESTAMADUL', 'ESTUNDISTI', 'UNPEDAGOGI']);
            }

            $query->where('LIQ_VALO', '<>', 0)
                ->where('F.FAC_ESTA', '=', 'A')
                ->groupBy('F.PVD_CODI', 'FAC_ANOP', 'P.PVR_NOCO', 'LIQ_NOMB');

            $data = $query->get();
        } else {
            $query = DB::connection("oracle_ws")->table('SEVEN.CN_VCERT')
                ->selectRaw('CN_VCERT.TER_CODI AS PVD_CODI, GN_TERCE.TER_NOCO AS PVR_NOCO, CUE_NOMB, SAL_ANOP AS FAC_ANOP, CUE_PORB')
                ->selectRaw('SUM(val_ingr) AS val_ingr, SUM(sal_vaba) AS val_vaba')
                ->selectRaw('SUM(CN_VCERT.SCE_VACR - CN_VCERT.SCE_VADB) AS VAL_TOTA')
                ->join('SEVEN.GN_TERCE', 'CN_VCERT.TER_CODI', '=', 'GN_TERCE.TER_CODI')
                ->where('CN_VCERT.TER_CODI', $request->get('document'))
                ->where('SAL_ANOP', $request->get('year'))
                ->where('GN_TERCE.EMP_CODI', 2)
                ->groupBy('CUE_CODI', 'CN_VCERT.TER_CODI', 'GN_TERCE.TER_NOCO', 'CUE_NOMB', 'SAL_ANOP', 'CUE_PORB');

            $data = $query->get();
        }

        return $this->success_message($data);
    }

    public function createPDF($data, Certification $certification, $periodo = null, $type = null)
    {
        $collection = collect($data ?? []);
        $pdf = new FPDF("L", "mm", "Letter");
        $pdf->AddPage();
        $pdf->setSourceFile(storage_path("app/templates/Certificado_Tributario.pdf"));
        $template = $pdf->importPage(1);
        $pdf->useTemplate($template, 0, 0, null, null, true);
        $pdf->SetFont("Courier");
        $pdf->SetFontSize(9);
        $pdf->SetTextColor(0, 0, 1);
        $name = null;
        $document = null;
        $year = null;
        $i = 0;
        $retef = [];
        $tot_base = [];

        // Variables para los totales de TRB
        $totalBruto = 0;
        $totalBase = 0;
        $totalRetenido = 0;
        $devolucion = 0;

        if ($type === 'TRB') {
            $orderedCollection = $collection->sortBy(function ($item) {
                if (stripos($item["cue_nomb"], 'retefuente servicios ley 1607/2012 et 19.00%') !== false) {
                    return 1;
                } elseif (stripos($item["cue_nomb"], 'retefuente servicios ley 1607/2012 et 28.00%') !== false) {
                    return 2;
                } elseif (stripos($item["cue_nomb"], 'devolucion retencion en la fuente retenidas') !== false) {
                    return 3;
                } else {
                    return 4;
                }
            });
        } else {
            $orderedCollection = $collection;
        }

        $orderedCollection->map(function ($collect) use (&$name, &$document, &$year, &$i, &$pdf, &$retef, &$periodo, &$type, &$tot_base, &$totalBruto, &$totalBase, &$totalRetenido, &$devolucion) {
            if ($i == 0) {
                $name = $collect["pvr_noco"] ?? "";
                $document = $collect["pvd_codi"] ?? "";
                $year = $collect["fac_anop"] ?? "";
            }

            // Diferenciar entre TRB y los otros tipos
            if ($type === "TRB") {

                $concepto = strtolower($collect["cue_nomb"] ?? "");
                $valbruto = "$ " . number_format($collect["val_ingr"] ?? 0, 2, ".", ",");
                $valbase = "$ " . number_format(abs($collect["val_vaba"] ?? 0), 2, ".", ",");  // Valor base en positivo
                $valrete = "$ " . number_format($collect["val_tota"] ?? 0, 2, ".", ",");

                $totalBruto += intval($collect["val_ingr"] ?? 0);
                $totalBase += abs(intval($collect["val_vaba"] ?? 0));
                $totalRetenido += intval($collect["val_tota"] ?? 0);

                $yPos = 120 + ($i * 3);
                $pdf->Text(55, $yPos, utf8_decode($this->shortenConcept($concepto, 40)));  // Mostrar concepto
                //$pdf->Text(120, $yPos, utf8_decode($valbase));  // Mostrar el valor base
                //$pdf->Text(150, $yPos, utf8_decode($valbruto));  // Mostrar el valor bruto
                $pdf->Text(150, $yPos, utf8_decode($valrete));  // Mostrar el valor retenido
                $i++;

            } else {
                $concepto=$collect["liq_nomb"]??"";
                $valbruto="$ ".number_format($collect["val_brut"]??0, 2, ".", ",");
                $valbase="$ ".number_format($collect["val_base"]??0, 2, ".", ",");
                $valrete="$ ".number_format($collect["val_rete"]??0, 2, ".", ",");
                $retef[toLower($concepto)]=intval($collect["val_rete"]??0);
                $tot_base[toLower($concepto)]=intval($collect["val_base"]??0);

                if(toLower($concepto)=="total"){
                    $pdf->Text(55, 146.8, utf8_decode($valbruto));
                }elseif($collect["liq_nomb"]=="IVA"){
                    $pdf->Text(55, 120, utf8_decode($collect["IVA RETENIDO"]??""));
                }elseif($collect["liq_nomb"]=="ESTAMPILLA BIENESTAR ADULTO MAYOR 2%"){
                    $pdf->Text(55, 120, utf8_decode($collect["liq_nomb"]??""));
                }elseif($collect["liq_nomb"]=="ESTAMPILLA PROCULTUR"){
                    $pdf->Text(55, 125, utf8_decode($collect["liq_nomb"]??""));
                }else{
                    $pdf->Text(55, 120+($i*5), utf8_decode($collect["liq_nomb"]??""));
                    //$pdf->Text(77, 126, utf8_decode($valbruto));
                    if($type=="IVA"){
                        //$pdf->Text(120, 170+($i*4), utf8_decode($collect["liq_nomb"]." ".$valbase));
                        $pdf->Text(55, 140, utf8_decode($collect["liq_nomb"]=="IVA RETENIDO"?$valbase:""));
                    } elseif ($type != "ICA") {
                        $pdf->Text(55, 140, utf8_decode($valbase));
                    }
                    //$pdf->Text(177, 126, utf8_decode($valrete));
                    $i++;
                }
            }
        });

        // Ajustar valores si no existe "ica 383 et" (para ICA, IVA, CON)
        if ($type !== "TRB") {
            $rteICA = $retef["ica- industria y com"] ?? 0;
            $rteIVA = $retef["iva retenido"] ?? 0;
            $rteCON = $retef["estampilla bienestar adulto mayor 2%"] ?? 0;
            $rteCON2 = $retef["estampilla procultur"] ?? 0;
            $ret = $retef["rte fuente"] ?? 0;
            $tot = $retef["total"] ?? 0;

            if ($type == "ICA") {
                $rteICA = ($retef["ica- industria y com"] ?? 0) + ($retef["ica 383 et"] ?? 0);
                $base = ($tot_base["ica- industria y com"] ?? 0) + ($tot_base["ica 383 et"] ?? 0);

                $pdf->Text(145, 120, utf8_decode("$ " . number_format($rteICA, 2, ".", ",")));
                $pdf->Text(55, 140, utf8_decode("$ " . number_format($base, 2, ".", ",")));
            } else if($type=="IVA"){
                $pdf->Text(145, 120, utf8_decode("$ ".number_format($rteIVA, 2, ".", ",")));
            } else if($type=="CON"){
                $rteCON = $retef["estampilla bienestar adulto mayor 2%"] ?? 0;
                $rteCON2 = $retef["estampilla procultur"] ?? 0;

                $pdf->Text(145, 120, utf8_decode("$ " . number_format($rteCON, 2, ".", ",")));
                $pdf->Text(145, 125, utf8_decode("$ " . number_format($rteCON2, 2, ".", ",")));

                //$base = ($tot_base["estampilla bienestar adulto mayor 2%"] ?? 0) + ($tot_base["estampilla procultur"] ?? 0);
                //$pdf->Text(55, 140, utf8_decode("$ " . number_format($base, 2, ".", ",")));
            } else {
                $pdf->Text(145, 119.5, utf8_decode(
                        $ret>1
                            ? "$ ".number_format($ret, 2, ".", ",")
                            :"$ ".number_format($tot, 2, ".", ",")
                    )
                );
            }

            if ($ret > 0) {
                $number = $ret;
            } else if ($rteICA > 0) {
                $number = $rteICA;
            } else if ($rteIVA > 0) {
                $number = $rteIVA;
            } else {
                $number = $rteCON + $rteCON2;
            }

            $decimals = 2;
            $currency = "PESOS M/CTE";
            $cents = "";
            $formatter = new NumeroALetras();
            $pdf->Text(55, 133.5, utf8_decode($formatter->toMoney($number, $decimals, $currency, $cents)));

        } else {
            // Para TRB, restar la devolución si existe y calcular el total retenido
            if ($devolucion > 0) {
                $totalRetenido -= $devolucion;
            }

            // Mostrar el valor total de retención en letras
            $decimals = 2;
            $currency = "PESOS M/CTE";
            $cents = "";
            $formatter = new NumeroALetras();
            $pdf->Text(55, 133.5, utf8_decode($formatter->toMoney($totalRetenido, $decimals, $currency, $cents)));

            // Mostrar el valor base y valor ingreso bruto
            $pdf->Text(55, 140, utf8_decode("$ " . number_format($totalBase, 2, ".", ","))); // Valor Base
            $pdf->Text(55, 146.5, utf8_decode("$ " . number_format($totalBruto, 2, ".", ","))); // Valor Ingreso Bruto

            // Mostrar el valor total retenido en el recuadro correcto
            //$pdf->SetFontSize(9); // Ajustar tamaño de letra para el recuadro
            //$pdf->Text(150, 127, utf8_decode("$ " . number_format($totalRetenido, 2, ".", ","))); // Total Retención Aplicada
        }

        // Crear título del certificado
        $title = "CERTIFICADO DE";
        if ($type === "ICA") {
            $title .= " RETENCIÓN DEL ICA";
        } elseif ($type === "IVA") {
            $title .= " RETENCIÓN DEL IVA";
        } elseif ($type === "CON") {
            $title .= " RETENCIÓN DE CONTRIBUCIONES";
        } else {
            $title .= " INGRESOS Y RETENCIONES";
        }

        // Añadir detalles comunes para todos los tipos de certificados
        $pdf->SetXY(11, 55);
        $pdf->SetFont("Courier", "B");
        $pdf->SetFontSize(10);
        $pdf->Cell(193.5, 5, utf8_decode($title), 0, 0, "C");

        $pdf->SetXY(11, 60);
        $pdf->SetFont("Courier");
        $pdf->SetFontSize(10);
        $pdf->Cell(193.5, 5, utf8_decode('INSTITUTO DISTRITAL DE RECREACIÓN Y DEPORTE - IDRD'), 0, 0, "C");

        $pdf->SetXY(11, 64);
        $pdf->SetFont("Courier");
        $pdf->SetFontSize(10);
        $pdf->Cell(193.5, 5, utf8_decode('NIT 860.061.099-1'), 0, 0, "C");

        $pdf->SetFont("Courier");
        $pdf->SetFontSize(9);
        $pdf->Text(55, 77.5, utf8_decode($periodo ?? "ENERO-DICIEMBRE"));
        $pdf->Text(55, 84.5, utf8_decode($year));
        $pdf->Text(55, 91.5, utf8_decode('BOGOTÁ D.C.'));
        $pdf->Text(55, 102, utf8_decode($name));
        $pdf->Text(55, 108.8, utf8_decode($document));
        $pdf->SetFont("Courier");
        $pdf->SetFontSize(8);
        $pdf->Text(64.5, 223.5, utf8_decode(now()->format("Y-m-d H:i:s")));

        // Agregar código QR y texto relacionado
        $token = $certification->token ?? $certification->type . '-' . Str::random(9);
        $path = config('app.env') != 'production' ? "/portal-contratista-dev" : "/";
        $subdomain = config('app.env') != 'production' ? "sim" : "portalcontratista";
        $url = "https://{$subdomain}.idrd.gov.co{$path}/es/validar-documento?validate=$token";

        QrCode::url($url)
            ->setErrorCorrectionLevel('H')
            ->setSize(10)
            ->setOutfile(storage_path("app/templates/{$token}.png"))
            ->png();

        $file = storage_path("app/templates/{$token}.png");
        $pdf->Image($file, 11.5, 182, 39, 39);

        $pdf->SetFontSize(7);
        $pdf->SetXY(12.5, 179);
        $pdf->Write(5, $url, $url);

        $pdf->SetXY(12.5, 176);
        $pdf->Cell(30, 5, utf8_decode('Lea el QR desde su dispositivo móvil o haga clic en el siguiente enlace para validar la autenticidad del documento:'));

        $pdf->SetXY(120, 221.5);
        $pdf->Cell(12, 2, utf8_decode('CÓDIGO DE VERIFICACIÓN: ' . $token));

        // Eliminar QR temporal
        if (Storage::disk('local')->exists("templates/{$token}.png")) {
            Storage::disk('local')->delete("templates/{$token}.png");
        }

        // Guardar token si es necesario
        if (!isset($certification->token)) {
            $certification->token = $token;
        }

        // Incrementar descargas y guardar
        $certification->increment('downloads');
        $certification->save();

        return $pdf->Output("S", "Certificado_Tributario.pdf");
    }

    // Función auxiliar para acortar conceptos largos
    private function shortenConcept($concept, $maxLength)
    {
        if (strlen($concept) > $maxLength) {
            return substr($concept, 0, $maxLength) . '...';
        }
        return $concept;
    }

    public function getMes($mes){
        switch ($mes){
            case 1:
                return "ENERO";
            case 2:
                return "FEBRERO";
            case 3:
                return "MARZO";
            case 4:
                return "ABRIL";
            case 5:
                return "MAYO";
            case 6:
                return "JUNIO";
            case 7:
                return "JULIO";
            case 8:
                return "AGOSTO";
            case 9:
                return "SEPTIEMBRE";
            case 10:
                return "OCTUBRE";
            case 11:
                return "NOVIEMBRE";
            case 12:
                return "DICIEMBRE";
            default:
                return"";
       }
    }
}

