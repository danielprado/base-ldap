<?php

namespace App\Console\Commands;

use App\Modules\CitizenPortal\src\Models\CitizenSchedule;
use App\Modules\CitizenPortal\src\Models\Status;
use App\Modules\PaymentGateway\src\Help\Helpers;
use App\Modules\PaymentGateway\src\Models\Pago;
use App\Modules\PaymentGateway\src\Models\Reservation;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class UpdateStatus extends Command
{
      /**
       * The name and signature of the console command.
       *
       * @var string
       */
      protected $signature = 'update:statuspse';

      /**
       * The console command description.
       *
       * @var string
       */
      protected $description = 'update status payment in pse';

      /**
       * Create a new command instance.
       *
       * @return void
       */
      public function __construct()
      {
            parent::__construct();
      }

      /**
       * Execute the console command.
       *
       * @return mixed
       */
      public function handle()
      {
            //
            $status_pending = 1;
            $payments = Pago::where('estado_id', $status_pending)->get();
            foreach ($payments as $payment) {
                  $responsePse = null;
                  $http = new Client();
                  $help = new Helpers();
                  $response = $http->get(env('URL_BASE_PAYMENTEZ') . '/pse/order/' . $payment->id_transaccion_pse . '/', [
                        'headers' => [
                              "auth-token" =>  $help->getAuthToken(),
                              "Content-Type" => "application/json",
                        ],
                  ]);
                  $responsePse =  json_decode($response->getBody()->getContents(), true);
                  $payment->estado_id = $help->getStatus($responsePse['transaction']['status']);
                  $payment->estado_banco = $responsePse['transaction']['status_bank'];
                  $payment->fecha_pago =  $responsePse['transaction']['paid_date'];
                  $payment->save();
                  $schedule = CitizenSchedule::query()->where("reference_pse", $payment->id_transaccion_pse)->first();
                  if (isset($schedule->reference_pse)) {
                      $payment_ok = 2;
                      if ((int) $payment->estado_id == $payment_ok) {
                          $schedule->status_id = Status::SUBSCRIBED;
                          $schedule->save();
                      }
                  }
                  $payment->load('state');
                  try {
                        if ($payment->id_reserva) {
                              $status_ok = 2;
                              if ($payment->state->id == $status_ok) {
                                    $reserva = Reservation::find($payment->id_reserva);
                                    if ($reserva) {
                                        $reserva->is_successful = true;
                                        $reserva->save();
                                    }
                                    $help->emailSendgridReservationCron($payment);
                              }
                        }
                  } catch (\Exception $e) {
                        $this->info($e->getMessage());
                  }
            }
            $this->info('finalizo');
            echo "se revisaron " .$payments->count()." pagos en espera\n";
            $paymentsAux = Pago::where('estado_id', 1)->get();
            echo "Quedan por revisar " .$paymentsAux->count()." pagos en espera\n";
      }
}
