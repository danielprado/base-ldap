<?php

namespace App\Modules\Contractors\src\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class ComplianceFile extends Model
{
    protected $connection = 'mysql_contractors';
    protected $table = 'compliance_files';

    const STORAGE_PATH = "contractor/compliance";

    protected $fillable = [
        'compliance_id',
        'file_name',
        'file_type'
    ];

    public function compliance() {
        return $this->belongsTo(Compliance::class, 'compliance_id');
    }

    public function fileType() {
        return $this->belongsTo(ComplianceFileType::class, 'file_type');
    }

    public function getPeriodAttribute()
    {
        $compliance = $this->compliance;
        return "$compliance->period_year-$compliance->period_month";
    }

    public function asUrl() {
        return route('compliance.file.resource', [
            'period' => $this->period,
            'name' => $this->file_name
        ]);
    }

    public function getStoragePath() {
        return self::STORAGE_PATH . "/$this->period";
    }

    public function getFilePath() {
        return "{$this->getStoragePath()}/$this->file_name";
    }

    public static function getFileName($fileType, $contractNumber) {
        $contract = extract_number_and_year_from_contract($contractNumber);
        return "{$fileType->name}_{$contract['contract_year']}{$contract['contract_number']}.pdf";
    }

    public function store(UploadedFile $file) {
        $file->storeAs(
            $this->getStoragePath(),
            $this->file_name,
            [ 'disk' => 'local' ]
        );
    }

    public function delete() {
        if (Storage::disk('local')->exists($this->getFilePath())) {
            Storage::disk('local')->delete($this->getFilePath());
        }

        parent::delete();
    }
}
