<?php


namespace App\Modules\Contractors\src\Request;


use Illuminate\Foundation\Http\FormRequest;

class SavePmrPayrollRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            "payroll_id" => "required|numeric",
            "pmrList" => "required|array",
            "pmrList.*.compliance_id" => "required|numeric",
            "complianceList" => "required|array",
            "complianceList.*.compliance_id" => "required|numeric"
        ];
    }
}
