<?php

namespace App\Modules\Certifications\src\Mail;

use App\Modules\Certifications\src\Models\ContractSupport;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SupportMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var ContractSupport
     */
    private $mail;

    /**
     * Create a new job instance.
     *
     * @param ContractSupport $user
     */
    public function __construct(ContractSupport $user)
    {
        $this->mail = $user;
    }

    public function build(): SupportMail
    {
        $id = isset( $this->mail->Id ) ? $this->mail->Id : '';
        $created_at = isset( $this->mail->created_at ) ? $this->mail->created_at->format('Y-m-d H:i:s') : '';

        $name = $this->mail->Nombre_Solicitante;
        $number = $this->mail->Numero_Contrato;
        $year = $this->mail->Anio_Contrato;
        $solution = $this->mail->Solucion;

        $path = config('app.env') != 'production'
            ? "/portal-contratista-dev"
            : "/";
        $subdomain = config('app.env') != 'production' ? "sim" : "portalcontratista";
        $url = "https://{$subdomain}.idrd.gov.co{$path}";

        return $this->view('mail.mail')
            ->subject('Solicitud Certificado Contractual')
            ->with([
                'header'    => 'IDRD',
                'title'     => 'Solicitud Certificado',
                'slogan'    => 'Notificación de Certificados IDRD',
                'content'   => "Hola $name, su solicitud de certificación fue tramitado.",
                'details'   =>  "
                    <p>Número de Registro: $id</p>
                    <p>Número de Contrato: $number</p>
                    <p>Año de Contrato: $year</p>
                    <p>Fecha de Registro: $created_at</p>
                    <p>$solution</p>
                    <p><strong><small>Nota: Si su certificación fue solicitada con obligaciones por favor despliegue la opción de descargar con obligaciones.</small></strong></p>
                ",
                'url'       =>  "$url/es/certificados-contractuales",
                'info'      =>  "Para descargar nuevamente el certificado, puede hacerlo a través del aplicativo PORTAL CONTRATISTA",
                'btn_text'  =>  "Ir a Certificados",
                'date'      => now()->year
            ]);
    }
}
