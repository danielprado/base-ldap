<?php

namespace App\Helpers;

use App\Modules\Contractors\src\Exceptions\NotStoredFilesException;
use Illuminate\Support\Facades\Storage;

class StorageTransactionManager 
{
  private $temporalDirectory;

  private function __construct(string $directory)
  {
    $this->temporalDirectory = $directory . '/temp';
    $this->initTemporalDirectory($directory);
  }

  public static function startTransaction($directory) 
  {
    return new StorageTransactionManager($directory);
  }

  public function operationOver($file, $transact) 
  {
    try {
      $transact($this->temporalDirectory, $file);
    } catch (\Throwable $th) {
      $this->triggerRollback();
      throw $th;
    }
  }

  public function commitTransaction() 
  {
    $this->handleFailedTransaction(
      $this->temporalDirectory, 
      function ($directory) {
        $this->makeDirectoryCopy($directory, function($file) {
          $originalFile = str_replace('/temp', '', $file);
    
          if (Storage::disk('local')->exists($originalFile)) {
            Storage::disk('local')->delete($originalFile);
          }
    
          Storage::disk('local')->copy($file, $originalFile);
        });
    
        Storage::disk('local')->deleteDirectory($directory);
      }
    );
  }

  public function triggerRollback() {
    $this->handleFailedTransaction(
      $this->temporalDirectory, 
      function ($directory) {
        Storage::deleteDirectory($directory);
      }
    );
  }

  private function initTemporalDirectory($directory) {
    Storage::disk('local')->makeDirectory($this->temporalDirectory);

    $this->makeDirectoryCopy($directory, function($file) use ($directory) {
      $copiedFile = str_replace($directory, $this->temporalDirectory, $file);
      Storage::disk('local')->copy($file, $copiedFile);
    });
  }

  private function makeDirectoryCopy($origin, $copyFunction) 
  {
    $files = Storage::disk('local')->files($origin);

    foreach ($files as $file) {
      $copyFunction($file);
    }
  }

  private function handleFailedTransaction($directory, $transactFunction, $attempts = 3) 
  {
    if ($attempts == 0) throw new NotStoredFilesException();

    try {
      $transactFunction($directory);
    } catch (\Throwable $th) {
      $this->handleFailedTransaction($directory, $transactFunction, $attempts - 1);
    }
  }
}