<?php

namespace App\Modules\Parks\src\Services;
use App\Modules\Parks\src\Services\Base\BaseExternalApiService;

use GuzzleHttp\Client;

class ParkExternalApiService extends BaseExternalApiService
{
    public function getById($park_code)
    {
        $route = '/Parques/MapServer/25/query';
        $params = ['outFields' => '*', 'returnGeometry' => 'true', 'where' => "ID_PARQUE='" . $park_code . "'" ];
        $response = parent::get($route, $params);
        return $response['features'][0] ?? null;
    }
}
