<?php

namespace App\Modules\Contractors\src\Resources;

use App\Modules\Contractors\src\Constants\Roles;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Crypt;
use App\Modules\Contractors\src\Resources\ActivityReportEvidenceResource;
use App\Modules\Contractors\src\Models\Contract;
use Carbon\Carbon;

class DocumentInitResource extends JsonResource
{
    protected $original_contract;
    protected $has_pending_document_init;

    public function __construct($resource)
    {
        parent::__construct($resource);
        $this->original_contract = null;
        $this->has_pending_document_init = false;
        $number_array = array_reverse(explode('-', $this->contract));
        $number_contract = $number_array[1] . '-' . $number_array[0];
        if ($this->contract_type_id !== Contract::NEW_CONTRACT){
            $this->original_contract = Contract::with('document_init')
                                                ->where('contract', 'like', '%'.$number_contract.'%')
                                                ->where('contract_type_id', Contract::NEW_CONTRACT)
                                                ->first();
            $this->has_pending_document_init = Contract::where('contract', 'like', '%'.$number_contract.'%')
                                                ->whereNotIn('contract_type_id', [Contract::NEW_CONTRACT, Contract::CONTRACT_SUSPENSION])
                                                ->where('id', '<', $this->id)
                                                ->whereDoesntHave('document_init')
                                                ->exists();                                                                               
        }
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $yearFee = isset($this->start_date) ? Carbon::parse($this->start_date)->year : null;
        return [
            'id' => $this->id ?? null,
            'name' => isset($this->contractor) ? ($this->contractor->name.' '.$this->contractor->surname) : null,
            'document' => $this->contractor->document ?? null,
            'date_init' => substr($this->start_date, 0, 10) ?? null,
            'date_end' => substr($this->final_date, 0, 10) ?? null,
            'value_total' => $this->total ?? null,
            'period_contract' => $this->duration ?? null,
            'value_month'=> $this->getValueMonth(),
            'months_contract'=> $this->document_init->months_contract ?? null,
            'days_contract'=> $this->document_init->days_contract ?? null,
            'review' => $this->document_init->review ?? null,
            'object_contract' => $this->object_contract ?? null,
            'subdirectory_id' => $this->subdirectorate_id ?? null,
            'dependency_id' => $this->dependency_id ?? null,
            'number_contract' => $this->contract ?? null,
            'total_sessions' => $this->document_init->total_sessions ?? null,
            'contract_type' => $this->contract_type ?? null,
            'start_suspension_date' => substr($this->start_suspension_date, 0, 10) ?? null,
            'final_suspension_date' => substr($this->final_suspension_date, 0, 10) ?? null,
            'yearFee' => $yearFee,
            'validation' => $this->validateContract(),
            'total_sessions' => $this->getTotalSessions()
        ];
    }
    
    private function getValueMonth() {
        if ($this->contract_type_id !== Contract::NEW_CONTRACT && $this->original_contract) {
            return isset($this->original_contract->document_init) ? $this->original_contract->document_init->value_month : null;
        }
        return $this->document_init->value_month ?? null;
    }

    private function validateContract()
    {
        // $dateInit = Carbon::createFromFormat('Y-m-d', substr($this->start_date, 0, 10));
        // $dateValidation = Carbon::createFromFormat('Y-m-d', '2024-11-01');
        // if (
        //     $this->contract_type_id === Contract::CONTRACT_ADD_AND_EXT 
        //     && $dateInit->lessThan($dateValidation)
        // ) {
        //     return [
        //         'can_generate' => false,
        //         'details' => 'No es posible generar el acta para una adición con fecha de inicio anterior al 2024-11-01'
        //     ];
        // }
        if (
            $this->contract_type_id !== Contract::NEW_CONTRACT 
            && $this->original_contract 
            && is_null($this->original_contract->document_init)
        ) {
            return [
                'can_generate' => false,
                'details' => 'El contrato original no tiene un acta de inicio parametrizada'
            ];
        }
        if ($this->has_pending_document_init) {
            return [
                'can_generate' => false,
                'details' => 'No todas las modificaciones previas del contrato cuentan con un acta de inicio generada.'
            ];
        }
        // if ($this->contract_type_id === Contract::CONTRACT_ADD_AND_EXT){
        //     $has_other_addition_contracts = Contract::where('contract', $this->contract)
        //                                         ->where('contract_type_id', Contract::CONTRACT_ADD_AND_EXT)
        //                                         ->where('id', '!=', $this->id)
        //                                         ->exists();
        //     if ($has_other_addition_contracts){
        //         return [
        //             'can_generate' => false,
        //             'details' => 'El contrato ya tiene adiciones previas'
        //         ];
        //     }
        // }
        return [
            'can_generate' => true,
            'details' => ''
        ];
    }    
    
    private function getTotalSessions() {
        if ($this->contract_type_id !== Contract::NEW_CONTRACT && $this->original_contract) {
            return isset($this->original_contract->document_init) ? (int)$this->original_contract->document_init['total_sessions'] : 0;
        }
        return 0;
    }
}
