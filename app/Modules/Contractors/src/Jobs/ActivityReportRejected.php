<?php

namespace App\Modules\Contractors\src\Jobs;

use App\Modules\Contractors\src\Mail\ActivityReportRejectedMail;
use App\Modules\Contractors\src\Models\ActivityReport;
use App\Modules\Contractors\src\Models\Contractor;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use SendGrid\Mail\Mail;
use SendGrid\Mail\To;

class ActivityReportRejected implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Contractor
     */
    private $user;

    /**
     * @var ActivityReport
     */
    private $activityReport;

    /**
     * @var string
     */
    private $observation;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        Contractor $user,
        ActivityReport $activityReport,
        string $observation
    ) {
        $this->user = $user;
        $this->activityReport = $activityReport;
        $this->observation = $observation;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mail $mailer)
    {
        $emails = [];

        if (isset($this->user->email) && filter_var($this->user->email, FILTER_VALIDATE_EMAIL)) {
            $emails[] = new To($this->user->email);
        }

        /*if (isset($this->user->institutional_email) && filter_var($this->user->institutional_email, FILTER_VALIDATE_EMAIL)) {
            $emails[] = new To($this->user->institutional_email);
        }*/

        if (!empty($emails)) {
            $mailer->setFrom(
                env('MAIL_CONTRACTS_FROM', 'portal.contratista@idrd.gov.co'),
                env('MAIL_CONTRACTS_FROM_NAME', 'Portal Contratista')
            );
            $mailer->setSubject('Informe de Actividades - Rechazado');
            $mailer->addTos($emails);

            $render = (new ActivityReportRejectedMail($this->activityReport, $this->observation))->render();
            $mailer->addContent("text/html", $render);

            $sendgrid = new \SendGrid(config('mail.sendgrid.api'));

            try {
                $response = $sendgrid->send($mailer);
                foreach ($emails as $email) {
                    sendgird_logger(
                        $email->getEmail(),
                        "Informe de Actividades - Rechazo",
                        $render,
                        [],
                        $response->statusCode(),
                        $response->body()
                    );
                }
            } catch (\Exception $e) {
                foreach ($emails as $email) {
                    sendgird_logger(
                        $email->getEmail(),
                        "[EXCEPTION] Informe de Actividades - Rechazo",
                        $render,
                        [],
                        $e->getCode(),
                        $e->getMessage()
                    );
                }
            }
        }
    }
}
