<?php

namespace App\Modules\Certifications\src\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Certifications\src\Jobs\CertificationProcessExport;
use App\Modules\Certifications\src\Models\CertificationContract;
use App\Modules\Certifications\src\Request\CertificationExcelRequest;
use App\Modules\Certifications\src\Request\UpdateContractRequest;
use App\Modules\Certifications\src\Resources\AdditionResource;
use App\Modules\Certifications\src\Resources\AssignmentResource;
use App\Modules\Certifications\src\Resources\ContractResource;
use App\Modules\Certifications\src\Resources\ExtensionResource;
use App\Modules\Certifications\src\Resources\MemberResource;
use App\Modules\Certifications\src\Resources\ObligationResource;
use App\Modules\Certifications\src\Resources\SuspensionResource;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Throwable;

class ContractController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request): JsonResponse
    {
        $contracts = CertificationContract::query()
            ->when($request->has('query'), function ($q) use ($request) {
                $data = $request->get('query');
                return $q->where('Cedula', 'like', "%$data%")
                    ->orWhere('Nombre_Contratista', 'like', "%$data%");
            })
            ->orderBy('created_At', 'desc')
            ->paginate($this->per_page);


        return $this->success_response(
            ContractResource::collection($contracts),
            Response::HTTP_OK,
            [
                'headers' => ContractResource::headers()
            ]
        );
    }

    public function show(CertificationContract $contract): JsonResponse
    {
        return $this->success_response(
            new ContractResource($contract->load('additions', 'assignments',
                'extensions', 'members', 'suspensions', 'obligations', 'expeditions')),
            Response::HTTP_OK,
            [
                'obligations_headers' => ObligationResource::headers(),
                'additions_headers' => AdditionResource::headers(),
                'suspensions_headers' => SuspensionResource::headers(),
                'extensions_headers' => ExtensionResource::headers(),
                'assignments_headers' => AssignmentResource::headers(),
                'members_headers' => MemberResource::headers(),
            ]
        );
    }

    /**
     * @throws Throwable
     */
    public function store(UpdateContractRequest $request): JsonResponse
    {

        $contract = CertificationContract::query()
            ->where('Tipo_Documento', $request->get('document_type_id'))
            ->where('Cedula', $request->get('document'))
            ->where('Numero_Contrato', $request->get('contract_number'))
            ->where('Fecha_Firma', $request->get('signature_date'))
            ->first();

        if ($contract) {
            return $this->error_response(
                "Ya existe un contrato con el número y fecha de firma para el contratista {$request->get('document')}"
            );
        }

        $contract = new CertificationContract();
        $contract->Tipo_Documento = $request->get('document_type_id');
        $contract->Cedula = $request->get('document');
        $contract->Dv = $request->get('dv');
        $contract->Nombre_Contratista = $request->get('name');
        $contract->Tipo_Documento_Representante = $request->get('document_type_representative_id');
        $contract->Cedula_Representante = $request->get('document_representative');
        $contract->Nombre_Representante = $request->get('name_representative');
        $contract->Tipo_Contrato_Id = $request->get('contract_type_id');
        $contract->Numero_Contrato = $request->get('contract_number');
        $contract->year = $request->get('contract_year');
        $contract->Objeto = $request->get('object');
        $contract->Fecha_Firma = $request->get('signature_date');
        $contract->Fecha_Inicio = $request->get('start_date');
        $contract->Fecha_Fin = $request->get('final_date');
        $contract->Fecha_Terminacion_Anticipada = $request->get('anticipated_date');
        $contract->Meses_Duracion = $request->get('duration_months');
        $contract->Dias_Duracion = $request->get('duration_days');
        $contract->Otra_Duracion = $request->get('duration_other');
        $contract->Valor_Inicial = $request->get('initial_value');
        $contract->Valor_Mensual = $request->get('monthly_value');
        $contract->Referencia = $request->get('reference');
        $contract->saveOrFail();

        return $this->success_message(
            __('validation.handler.success'),
            Response::HTTP_CREATED,
            Response::HTTP_CREATED,
            [
                'id'    => $contract->Id
            ]
        );
    }

    public function update(UpdateContractRequest $request, CertificationContract $contract): JsonResponse
    {
        $contract->update([
            'Tipo_Documento' => $request->get('document_type_id'),
            'Cedula' => $request->get('document'),
            'Dv' => $request->get('dv'),
            'Nombre_Contratista' => toUpper($request->get('name')),
            'Tipo_Documento_Representante' => $request->get('document_type_representative_id'),
            'Cedula_Representante' => $request->get('document_representative'),
            'Nombre_Representante' => toUpper($request->get('name_representative')),
            'Tipo_Contrato_Id' => $request->get('contract_type_id'),
            'Referencia' => $request->get('reference'),
            'Numero_Contrato' => $request->get('contract_number'),
            'year' => $request->get('contract_year'),
            'Objeto' => toUpper($request->get('object')),
            'Fecha_Firma' => $request->get('signature_date'),
            'Fecha_Inicio' => $request->get('start_date'),
            'Fecha_Fin' => $request->get('final_date'),
            'Fecha_Terminacion_Anticipada' => $request->get('anticipated_date'),
            'Meses_Duracion' => $request->get('duration_months'),
            'Dias_Duracion' => $request->get('duration_days'),
            'Otra_Duracion' => $request->get('duration_other'),
            'Valor_Inicial' => $request->get('initial_value'),
            'Valor_Mensual' => $request->get('monthly_value'),
        ]);

        return $this->success_message(__('validation.handler.updated'));
    }

    public function destroy(CertificationContract $contract): JsonResponse
    {
        try {
            $contract->delete();
            return $this->success_message(__('validation.handler.deleted'));
        } catch (\Exception $e) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }

    public function excel(CertificationExcelRequest $request): JsonResponse
    {
        $job = new CertificationProcessExport(
            $request->all(),
            $request->user('api'),
            ['queue' => 'excel-certifications-module', 'user_id' => auth('api')->user()->id]
        );
        $this->dispatch($job);
        return $this->success_message(
            'Estamos generando el reporte solcitado, te notificaremos una vez esté listo.',
            Response::HTTP_OK,
            Response::HTTP_OK,
            $job
        );
    }
}
