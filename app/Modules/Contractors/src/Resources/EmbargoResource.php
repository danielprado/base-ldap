<?php


namespace App\Modules\Contractors\src\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class EmbargoResource extends JsonResource
{
    protected $reports;

    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                =>  isset($this->id) ? (int) $this->id : null,
            'document'          =>  $this->document ?? null,
            'name'              =>  $this->name ?? null,
            'type'              =>  $this->type ?? null,
            'filed'             =>  $this->filed ?? null,
            'date'              =>  isset($this->date) ? $this->date->format('Y-m-d') : null,
            'job'               =>  $this->job ?? null,
            'judged'            =>  $this->judged ?? null,
            'description'       =>  $this->description ?? null,
            'judicial_account'  =>  $this->judicial_account ?? null,
            'value'             =>  $this->value,
            'value_format'      =>  is_numeric($this->value) ? cop_money_format($this->value ?? 0, '$',  null, 0) : $this->value,
            //'contractor'        => ContractorResource::make($this->whenLoaded('contractor')),
            'created_at'        =>  isset($this->created_at) ? $this->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'        =>  isset($this->updated_at) ? $this->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }

    public static function headers()
    {
        return [
            [
                'text' => "#",
                'value'  =>  "id",
            ],
            [
                'text' => 'Nombre del contratista',
                'value'  =>  'name',
                'icon'  =>  'mdi-face',
            ],
            [
                'text' => 'Documento del contratista',
                'value'  =>  'document',
                'icon'  =>  'mdi-number',
            ],
            [
                'text' => 'Tipo de embargo',
                'value'  =>  'type',
                'icon'  =>  'mdi-file-account-outline',
            ],
            [
                'text'  => 'Número de proceso',
                'value'  => 'filed',
                'icon'   => 'mdi-numeric',
            ],
            [
                'text'  => 'Fecha',
                'value'  => 'date',
                'icon'   => 'mdi-calendar',
            ],
            [
                'text'  => 'Oficio',
                'value'  => 'job',
                'icon'   => 'mdi-numeric',
            ],
            [
                'text'  => 'Juzgado',
                'value'  => 'judged',
                'icon'   => 'mdi-gavel',
            ],
            [
                'text'  => 'Descripción y/o concepto',
                'value'  => 'description',
                'icon'   => 'mdi-ab-testing',
            ],
            [
                'text'  => 'Cuenta Judicial',
                'value'  => 'judicial_account',
                'icon'   => 'mdi-numeric',
            ],
            [
                'text'  => 'Limite y/o valor de la medida',
                'value'  => 'value',
                'icon'   => 'mdi-currency-usd',
            ],
            [
                'text'  => 'Fecha de Registro',
                'value'  => 'created_at',
                'icon'   => 'mdi-calendar',
            ],
            [
                'text'  => 'Fecha de Actualización',
                'value'  => 'updated_at',
                'icon'   => 'mdi-calendar',
            ],
        ];
    }

}
