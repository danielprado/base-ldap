<?php

namespace App\Modules\Certifications\src\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AdditionResource extends JsonResource
{
    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    public function toArray($request): array
    {
        return [
            'id'                => isset($this->Id) ? (int)$this->Id : null,
            'contract_id'       => isset($this->Contrato_Id) ? (int)$this->Contrato_Id : null,
            'number'            => isset($this->Numero_Adicion) ? (int)$this->Numero_Adicion : null,
            'value'             => isset($this->Valor_Adicion) ? number_format($this->Valor_Adicion, 0, '.', '.') : null,
            'created_at'        =>  isset($this->created_at) ? $this->created_at->format('Y-m-d') : null,
            'updated_at'        =>  isset($this->updated_at) ? $this->updated_at->format('Y-m-d') : null,
        ];
    }

    public static function headers(): array
    {
        return [
            [
                'text' => "#",
                'value'  =>  "id",
                'sortable' => false
            ],
            [
                'align' => "right",
                'text' => "Número",
                'value'  =>  "number",
                'sortable' => false
            ],
            [
                'align' => "right",
                'text' => "Valor de la adición",
                'value'  =>  "value",
                'sortable' => false
            ],
            [
                'align' => "right",
                'text' => "Fecha de registro",
                'value'  =>  "created_at",
                'sortable' => false
            ],
            [
                'align'     => "center",
                'text'      => "Acciones",
                'value'     =>  "actions",
                'sortable'  => false
            ],
        ];
    }
}
