<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateColumnsToPagoPseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_pse')->table('pago_pse', function (Blueprint $table) {
            $table->text('permiso')->change();
            $table->text('tipo_persona')->nullable()->after('id_transaccion_pse');
            $table->text('razon_social')->nullable()->after('tipo_persona');
            $table->integer('codigo_verificacion')->nullable()->after('razon_social');
            $table->text('direccion')->nullable()->after('telefono');
            $table->text('direccion_pagador')->nullable()->after('telefono_pagador');
            $table->text('tipo_persona_pagador')->nullable()->after('direccion_pagador');
            $table->text('razon_social_pagador')->nullable()->after('tipo_persona_pagador');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_pse')->table('pago_pse', function (Blueprint $table) {
            $table->bigInteger('permiso')->change();
            $table->dropColumn('tipo_persona');
            $table->dropColumn('razon_social');
            $table->dropColumn('codigo_verificacion');
            $table->dropColumn('direccion');
            $table->dropColumn('tipo_persona_pagador');
            $table->dropColumn('razon_social_pagador');
            $table->dropColumn('direccion_pagador');
        });
    }
}
