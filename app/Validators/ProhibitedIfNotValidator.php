<?php

namespace App\Validators;

class ProhibitedIfNotValidator {

  public function validate($attr, $value, $parameters, $validator) {
    $input = $validator->getData();

    foreach ($parameters as $param) {
      if (isset($input[$param])) {
        if (!$input[$param]) return $value ? false : true;
      }
    }
    
    return true;
  }
}