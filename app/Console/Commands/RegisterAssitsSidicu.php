<?php

namespace App\Console\Commands;

use App\Modules\AttendanceRecord\src\Models\Attendee;
use App\Modules\AttendanceRecord\src\Models\Disability;
use App\Modules\AttendanceRecord\src\Models\Paidcare;
use App\Modules\AttendanceRecord\src\Models\Unpaidcare;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class RegisterAssitsSidicu extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'register:assits-sidicu';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'actualiza las asistentes de sidicu proporcionadas por la api de IBO';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $client = new Client();
        // URL de la API a la que deseas realizar la solicitud
        $url = env('URL_BASE_IBO') . '/api/users?is_loaded_to_idrd=false';
        $usersIds = [];
        try {
            $response = $client->get($url);
            $body = json_decode($response->getBody()->getContents(), true);
            foreach ($body['users'] as $user) {
                $attendee = Attendee::where('number_document', $user['number_document'])->first();
                if (!$attendee) {
                    if ($user['complete_names'] !== 'null null') {
                        $this->saveAttendee($user);
                        $usersIds[] = $user['id'];
                    }
                }
            }
            $this->info(count($usersIds));
            if (count($usersIds) > 0) {
                $this->updateUserExternal($usersIds);
            }
        } catch (\Throwable $th) {
            Log::error('ERROR ' . $th->getMessage());
            $this->info('ERROR ' . $th->getMessage());
        }
    }

    private function saveAttendee($attendeeN)
    {
        $attendee = new Attendee;
        $attendee->type_document = strtoupper($attendeeN['idrdData']['type_document']);
        $attendee->number_document = strtoupper($attendeeN['number_document']);
        $attendee->complete_names = strtoupper($attendeeN['complete_names']);
        $attendee->date_birth = strtoupper($attendeeN['date_birth']);
        $attendee->sex = strtoupper($attendeeN['idrdData']['sex']);
        $attendee->sexual_orientation = strtoupper($attendeeN['idrdData']['sexual_orientation']);
        $attendee->gender_identity = strtoupper($attendeeN['idrdData']['gender_identity']);
        $attendee->rh = strtoupper($attendeeN['idrdData']['rh']);
        $attendee->eps_id = $attendeeN['idrdData']['eps_id'];
        $attendee->phone = strtoupper($attendeeN['idrdData']['phone']);
        $attendee->country_birth_id = $attendeeN['idrdData']['country_birth_id'];
        $attendee->state_birth_id = $attendeeN['idrdData']['state_birth_id'];
        $attendee->city_birth_id = $attendeeN['idrdData']['city_birth_id'];
        $attendee->country_home_id = $attendeeN['idrdData']['country_home_id'];
        $attendee->state_home_id = $attendeeN['idrdData']['state_home_id'];
        $attendee->city_home_id = $attendeeN['idrdData']['city_home_id'];
        $attendee->civil_status = strtoupper($attendeeN['idrdData']['civil_status']);
        $attendee->stratum = $attendeeN['stratum'];

        $attendee->locality_home_id = $attendeeN['idrdData']['locality_home_id'];
        $attendee->upz_home_id = $attendeeN['idrdData']['upz_home_id'];
        $attendee->emergency_contac = strtoupper($attendeeN['idrdData']['emergency_contac']);
        $attendee->emergency_phone = strtoupper($attendeeN['idrdData']['emergency_phone']);
        $attendee->mail = strtoupper($attendeeN['idrdData']['mail']);
        $attendee->Adress = strtoupper($attendeeN['Adress']);
        $attendee->has_computed = strtoupper($attendeeN['idrdData']['has_computed']);
        $attendee->has_conectivity = strtoupper($attendeeN['idrdData']['has_conectivity']);
        $attendee->type_socialsecurity = strtoupper($attendeeN['idrdData']['type_socialsecurity']);

        $attendee->group_etnic = strtoupper($attendeeN['idrdData']['group_etnic']);
        $attendee->study_level = strtoupper($attendeeN['idrdData']['study_level']);
        $attendee->occupation = strtoupper($attendeeN['idrdData']['occupation']);
        $attendee->initial_skill = $attendeeN['idrdData']['initial_skill'];
        $attendee->progress_made = $attendeeN['idrdData']['progress_made'];
        $attendee->save();

        $disability[] = $attendeeN['idrdData']['disability_id'];
        $disabilities = Disability::find($disability);
        $paidcares = Paidcare::find($attendeeN['idrdData']['paidcare_id']);
        $unpaidcares = Unpaidcare::find($attendeeN['idrdData']['unpaidcare_id']);

        $attendee->disabilities()->sync($disabilities);
        $attendee->paidcares()->sync($paidcares);
        $attendee->unpaidcares()->sync($unpaidcares);
    }

    private function updateUserExternal($attendeesIds)
    {
        $client = new Client();
        $url =  env('URL_BASE_IBO') . '/api/users'; // Reemplaza con la URL de tu endpoint

        $data = [
            'usersIds' => $attendeesIds,
            'is_loaded_to_idrd' => true,
            // Agrega aquí otros datos que deseas enviar en el cuerpo (body)
        ];

        try {
            $response = $client->post($url, [
                'json' => $data, // Enviar datos como JSON
                'headers' => [
                    'Content-Type' => 'application/json',
                    // Puedes añadir más cabeceras si es necesario
                ],
            ]);

            $statusCode = $response->getStatusCode();
            $responseBody = $response->getBody()->getContents();
            $this->info('OK ' . $statusCode);
        } catch (\Throwable $th) {
            Log::error('ERROR ' . $th->getMessage());
            $this->info('ERROR ' . $th->getMessage());
        }
    }
}
