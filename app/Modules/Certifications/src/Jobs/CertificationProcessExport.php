<?php

namespace App\Modules\Certifications\src\Jobs;

use App\Jobs\NotifyUserOfCompletedExport;
use App\Models\Security\User;
use App\Modules\Certifications\src\Exports\CertificationDataExport;
use App\Modules\Certifications\src\Mail\ReportMail;
use App\Modules\Certifications\src\Models\CertificationContract;
use App\Modules\Certifications\src\Models\ContractSupport;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Imtigger\LaravelJobStatus\JobStatus;
use Imtigger\LaravelJobStatus\Trackable;
use Maatwebsite\Excel\Excel;

class CertificationProcessExport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Trackable;

    /**
     * @var array
     */
    private $request;

    /**
     * @var array
     */
    private $params;

    /**
     * @var string
     */
    private $name;

    /**
     * @var User
     */
    private $user;

    /**
     * Create a new job instance.
     *
     * @param array $request
     * @param User $user
     * @param array $params
     */
    public function __construct(array $request, User $user, array $params = [])
    {
        $this->name = "CERTIFICACION-CONTRATOS-".random_img_name().".xlsx";
        $this->params = array_merge(['key' => $this->name], $params);
        $this->request = $request;
        $this->prepareStatus($this->params);
        $this->user = $user;
        $this->setInput([
            'request' => $this->request,
            'user' => $this->user
        ]);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->update([
            'queue' => 'excel-certifications-module',
            'status' => JobStatus::STATUS_EXECUTING,
            'finished_at' => null,
        ]);

        $url = env('APP_ENV') == 'local'
            ? 'http://localhost:3000/es/certificaciones/reportes'
            : 'https://sim.idrd.gov.co/es/certificaciones/reportes' ??  null;

        $request = collect($this->request);
        $contracts = [];
        $supports = [];
        if ($request->has('solved') || $request->has('unsolved')) {
            ContractSupport::query()
                ->when($request->has(['solved', 'start_date', 'final_date']), function ($q) use ($request) {
                    return $q->where('Estado', 2)
                        ->whereDate('updated_at', '>=', $request->get('start_date'))
                        ->whereDate('updated_at', '<=', $request->get('final_date'));
                })
                ->when($request->has(['unsolved', 'start_date', 'final_date']), function ($q) use ($request) {
                    return $q->where('Estado', 1)
                        ->whereDate('updated_at', '>=', $request->get('start_date'))
                        ->whereDate('updated_at', '<=', $request->get('final_date'));
                })
                ->when($request->has('solved'), function ($q) {
                    return $q->where('Estado', 2);
                })
                ->when($request->has('unsolved'), function ($q) {
                    return $q->where('Estado', 1);
                })
                ->orderBy('created_at', 'desc')
                ->chunk(100, function ($models) use (&$supports) {
                    foreach ($models as $model) {
                        $supports[] = [
                            'id' => $model->Id,
                        ];
                    }
                });
            $keys = [
                'supports' => collect($supports)->pluck('id')->toArray(),
            ];
        } else {
            CertificationContract::query()
                ->with(['additions', 'assignments', 'expeditions', 'extensions',
                    'members', 'suspensions', 'obligations'])
                ->when($request->has('document'), function ($q) use ($request) {
                    return $q->where('Cedula', $request->get('document'));
                })
                ->when($request->has('contract'), function ($q) use ($request) {
                    return $q->where('Numero_Contrato', $request->get('contract'));
                })
                ->orderBy('created_at', 'desc')
                ->chunk(100, function ($models) use (&$contracts) {
                    foreach ($models as $model) {
                        $contracts[] = [
                            'id' => $model->Id,
                            'obligations' => $model->obligations->pluck('Id')->toArray(),
                            'additions' => $model->additions->pluck('Id')->toArray(),
                            'assignments' => $model->assignments->pluck('Id')->toArray(),
                            'expeditions' => $model->expeditions->pluck('Id')->toArray(),
                            'extensions' => $model->extensions->pluck('Id')->toArray(),
                            'members' => $model->members->pluck('Id')->toArray(),
                            'suspensions' => $model->suspensions->pluck('Id')->toArray(),
                        ];
                    }
                });

            $keys = [
                'contracts' => collect($contracts)->pluck('id')->toArray(),
                'obligations' => collect($contracts)->pluck('obligations')->flatten()->toArray(),
                'additions' => collect($contracts)->pluck('additions')->flatten()->toArray(),
                'extensions' => collect($contracts)->pluck('extensions')->flatten()->toArray(),
                'suspensions' => collect($contracts)->pluck('suspensions')->flatten()->toArray(),
                'assignments' => collect($contracts)->pluck('assignments')->flatten()->toArray(),
                'members' => collect($contracts)->pluck('members')->flatten()->toArray(),
                'expeditions' => collect($contracts)->pluck('expeditions')->flatten()->toArray(),
            ];
        }

        (new CertificationDataExport($keys, $this->getJobStatusId(), $this->user))
            ->queue("exports/$this->name", 'local', Excel::XLSX)
            ->chain([
                new NotifyExportReport($this->user, $this->name, $url)
            ]);

        $this->setOutput(['file' => $this->name]);
    }
}
