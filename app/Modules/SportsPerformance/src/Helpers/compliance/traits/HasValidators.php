<?php

namespace App\Modules\SportsPerformance\src\Helpers\compliance\traits;

trait HasValidators {
  protected $validators = [];

  public function useValidator($validator)
  {
    if (!$validator) return $this;

    $this->validators = array_merge($this->validators, [
      $validator->name() => $validator
    ]);

    return $this;
  }

  protected function getValidator(string $name)
  {
    return isset($this->validators[$name]) ? $this->validators[$name] : null;
  }
}