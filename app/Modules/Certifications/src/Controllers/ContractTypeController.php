<?php

namespace App\Modules\Certifications\src\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Certifications\src\Models\CertificationContractType;
use App\Modules\Certifications\src\Resources\ContractTypeResource;
use Illuminate\Http\JsonResponse;

class ContractTypeController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(): JsonResponse
    {
        return $this->success_response(
            ContractTypeResource::collection(CertificationContractType::all())
        );
    }
}
