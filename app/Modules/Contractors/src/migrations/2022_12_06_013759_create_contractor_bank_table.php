<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContractorBankTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contractor_bank', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('Identificador del registro');
            $table->unsignedBigInteger('contractor_id')->comment('Identificador del contratista');
            $table->unsignedBigInteger('bank_id')->comment('Identificador del banco');
            $table->unsignedBigInteger('account_type_id')->comment('Identificador del tipo de cuenta');
            $table->string('number')->comment('Número de cuenta');
            $table->foreign('contractor_id')
                ->references('id')
                ->on('contractors')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
            $table->foreign('bank_id')
                ->references('id')
                ->on('banks')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
            $table->foreign('account_type_id')
                ->references('id')
                ->on('account_types')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contractor_bank');
    }
}
