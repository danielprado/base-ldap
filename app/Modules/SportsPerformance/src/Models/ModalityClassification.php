<?php

namespace App\Modules\SportsPerformance\src\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\SportsPerformance\src\Models\Modality;
use App\Modules\SportsPerformance\src\Models\Disability;

class ModalityClassification extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_sim_rendimiento_deportivo';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'clasificacion_funcional_modalidad_discapacidad';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Id_ClasificacionFuncional',
        'Id_Deporte',
        'Id_Discapacidad',
        'Id_Modalidad'
    ];
    
     public $timestamps = false;

    public function disability()
    {
        return $this->hasOne(Disability::class, 'Id', 'Id_Discapacidad');
    }

    public function modality()
    {
        return $this->hasOne(Modality::class, 'Id', 'Id_Modalidad');
    }
}
