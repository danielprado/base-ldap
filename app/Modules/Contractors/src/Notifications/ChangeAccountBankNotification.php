<?php


namespace App\Modules\Contractors\src\Notifications;


use App\Modules\Contractors\src\Models\Contract;
use App\Modules\Contractors\src\Models\Contractor;
use App\Modules\Contractors\src\Models\ContractorBank;
use Illuminate\Bus\Queueable;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Notifications\Notification;

class ChangeAccountBankNotification extends Notification
{
    use Queueable;

    /**
     * @var Contractor
     */
    private $contractor;

    /**
     * @var ContractorBank
     */
    private $bank;

    /**
     * Create a new notification instance.
     *
     * @param Contractor $contractor
     * @param ContractorBank|HasOne $bank
     */
    public function __construct(Contractor $contractor, ContractorBank $bank)
    {
        $this->contractor = $contractor;
        $this->bank = $bank;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $id = isset( $this->contractor->id ) ? $this->contractor->id : '';
        $first = isset( $this->contractor->name ) ? $this->contractor->name : '';
        $second = isset( $this->contractor->surname ) ? $this->contractor->surname : '';
        $name = "$first $second";
        $bank_name = isset( $this->bank->bank->name ) ? $this->bank->bank->name : '';
        $account_type = isset( $this->bank->account_type->name ) ? $this->bank->account_type->name : '';
        $subject = "{$name} / {$bank_name} / {$account_type}";
        return [
            'title'         => 'Actualización de Cuenta Bancaria',
            'subject'       => $subject,
            'user'          => 'SYSTEM',
            'created_at'    => now()->format('Y-m-d H:i:s'),
            'url'           => [
                'name'      =>  'user-id-contractor',
                'params'    =>  [ 'id'  => $id ]
            ]
        ];
    }
}
