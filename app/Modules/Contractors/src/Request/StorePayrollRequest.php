<?php


namespace App\Modules\Contractors\src\Request;


use App\Modules\Contractors\src\Models\FileType;
use App\Modules\Contractors\src\Models\PlanPayment;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StorePayrollRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'year'              => 'required|integer',
            'month'             => 'required|integer',
            'subdirector_id'    => 'required|integer|exists:mysql_ldap.users,id',
            'subdirection_id'   => 'required|integer|exists:mysql_ldap.subdirectorates,id',
            'plans'             => 'required|array|max:300',
            'reserve'           => 'required|boolean',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'year'              =>  'año',
            'month'             =>  'mes',
            'subdirector_id'    =>  'identificador de la subdirección',
            'sub_address_id'    =>  'identificador del ordenador',
            'plans'             =>  'lista de planes de pago',
            'reserve'           =>  'tipo de planilla',
        ];
    }
}
