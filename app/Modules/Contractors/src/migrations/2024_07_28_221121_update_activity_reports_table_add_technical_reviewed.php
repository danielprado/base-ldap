<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateActivityReportsTableAddTechnicalReviewed extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activity_reports', function (Blueprint $table) {
            $table->tinyInteger('reviewed_technical')->default(0);
            $table->string('name_reviewer_technical', 255)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('activity_reports', function (Blueprint $table) {
            $table->dropColumn('reviewed_technical');
            $table->dropColumn('name_reviewer_technical');            
        });
    }
}
