<?php

namespace App\Modules\AttendanceRecord\src\Controllers;

use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Modules\AttendanceRecord\src\Models\InfoApp;
use Illuminate\Http\Request;

/**
 * @group Pasarela de pagos - Parques
 *
 * API para la gestión y consulta de datos de Parques Pse
 */
class InfoAppController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @group registro asistencia
     *
     * Parques
     *
     * Muestra un listado del recurso.
     *
     *
     * @return JsonResponse
     */

    public function saveInfoApp(Request $request)
    {
        try {
            $data = $request->json()->all();
            $infoApp = new InfoApp;
            $infoApp->age = (int) $data['age'];
            $infoApp->genero = $data['genero'];
            $infoApp->preferencias = $data['preferencias'];
            $infoApp->localidad = $data['localidad'];
            $infoApp->authorize = $data['authorize'];
            $infoApp->save();
            return response()->json([
                'status' => 'OK',
                'message' => 'registro guardado satisfactoriamente',
                'data' => null,
                'Error' => false,
            ], 200);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => 'OK',
                'message' => 'error al registrar la información',
                'data' => $ex->getMessage(),
                'Error' => true,
            ], 500);
        }
    }
}
