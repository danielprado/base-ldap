<?php


namespace App\Modules\CitizenPortal\src\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\CitizenPortal\src\Models\Consent;
use App\Modules\CitizenPortal\src\Request\ActivityRequest;
use App\Modules\CitizenPortal\src\Request\ConsentRequest;
use App\Modules\CitizenPortal\src\Resources\ConsentResource;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class ConsentController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        $query = $this->setQuery(Consent::query(), (new Consent)->getSortableColumn($this->column))
            ->when(isset($this->query), function ($query) {
                return $query->where('text', 'like', "%$this->query%");
            })
            ->orderBy((new Consent)->getSortableColumn($this->column), $this->order);
        return $this->success_response(
            ConsentResource::collection(
                (int) $this->per_page > 0
                    ? $query->paginate( $this->per_page )
                    : $query->get()
            ),
            Response::HTTP_OK,
            [
                'headers'   => ConsentResource::headers(),
                "expanded" => ConsentResource::expanded(),
            ]
        );
    }

    /**
     * @param ActivityRequest $request
     * @return JsonResponse
     */
    public function store(ConsentRequest $request)
    {
        $ext = $request->file('file')->getClientOriginalExtension();
        $name = random_img_name().now()->format("YmdHis").".".$ext;
        if ($request->file('file')->storeAs('consents', $name, [ 'disk' => 'citizen_portal_public' ])) {
            $model = new Consent();
            $model->fill([
                "title" => $request->get("title"),
                "adult" => $request->get("adult"),
                "file" => $name
            ]);
            $model->save();
            return $this->success_message(
                __('validation.handler.success'),
                Response::HTTP_CREATED
            );
        }
        return $this->error_response(__('validation.handler.unexpected_failure'));
    }

    /**
     * @param ConsentRequest $request
     * @param Consent $consent
     * @return JsonResponse
     */
    public function update(ConsentRequest $request, Consent $consent)
    {
        $ext = $request->file('file')->getClientOriginalExtension();
        $name = random_img_name().now()->format("YmdHis").".".$ext;
        if ($request->file('file')->storeAs('consents', $name, [ 'disk' => 'citizen_portal_public' ])) {
            if (Storage::disk("citizen_portal_public")->exists("consents/{$consent->file}")) {
                Storage::disk("citizen_portal_public")->delete("consents/{$consent->file}");
            }
            $consent->fill([
                "title" => $request->get("title"),
                "adult" => $request->get("adult"),
                "file" => $name
            ]);
            $consent->save();
            return $this->success_message(
                __('validation.handler.updated')
            );
        }
    }

    /**
     * @param Consent $consent
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Consent $consent)
    {
        if (Storage::disk("citizen_portal_public")->exists("consents/{$consent->file}")) {
            Storage::disk("citizen_portal_public")->delete("consents/{$consent->file}");
        }
        $consent->delete();
        return $this->success_message(
            __('validation.handler.deleted'),
            Response::HTTP_OK,
            Response::HTTP_NO_CONTENT
        );
    }
}
