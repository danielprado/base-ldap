<?php

namespace App\Modules\Contractors\src\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class ComplianceFileController extends Controller
{
    public function file(string $period, string $name) 
    {
        $fileRoute = "contractor/compliance/$period/{$name}";

        if (!Storage::disk('local')->exists($fileRoute)) {
            abort(Response::HTTP_NOT_FOUND);
        }

        return response()
            ->file(storage_path("app/{$fileRoute}"));
    }
}
