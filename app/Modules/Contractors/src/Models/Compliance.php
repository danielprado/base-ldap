<?php

namespace App\Modules\Contractors\src\Models;

use App\Models\Security\Area;
use App\Models\Security\Subdirectorate;
use App\Modules\Contractors\src\Constants\Contributions;
use App\Modules\Contractors\src\Helpers\compliance\validators\ComplianceDateValidator;
use App\Modules\Contractors\src\Helpers\compliance\validators\ComplianceMonthlyValidator;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Compliance extends Model
{

    const STATUS_PENDING = 'PENDING';
    const STATUS_APPROVED = 'APPROVED';
    const STATUS_REJECTED = 'REJECTED';

    protected $connection = 'mysql_contractors';

    protected $table = 'compliance';

    protected $fillable = [
        'contractor_id',
        'contract_id',
        'has_children',
        'children_number',
        'children_minor',
        'children_mayor_students',
        'children_disabilities',
        'spouse',
        'parents_dependency',
        'housing_interests',
        'prepaid_medicine',
        'income_tax_filer',
        'retention',
        'responsible_iva',
        'withholding',
        'third_people',
        'percentage',
        'modifiable',
        'spouse_tax',
        'withholding_value',
        'pensioner',
        'afc_value',
        'afp_voluntary_value',
        'afp_voluntary',
        'status',
        'reviewer',
        'period_month',
        'period_year',
        'secop_status',
        'observations',
        'afc_voluntary',
        'reviewed',
        'name_reviewer',
        'reviewed_technical',
        'name_reviewer_technical',
        'dependent_value',
        'contract_number'
    ];

    protected $casts = [
        'has_children' => 'bool',
        'children_minor' => 'bool',
        'children_mayor_students' => 'bool',
        'children_disabilities' => 'bool',
        'spouse' => 'bool',
        'parents_dependency' => 'bool',
        'housing_interests' => 'bool',
        'prepaid_medicine' => 'bool',
        'income_tax_filer' => 'bool',
        'pensioner' => 'bool',
        'withholding' => 'bool',
        'retention' => 'bool',
        'responsible_iva' => 'bool',
        'third_people' => 'bool',
        'spouse_tax' => 'bool',
        'modifiable' => 'date',
        'afp_voluntary' => 'bool',
        'afc_voluntary' => 'bool',
    ];

    const COMPLIANCE_PERIOD_START_DAY = 1;
    const COMPLIANCE_PERIOD_END_DAY = 30;

    public static function findBy($contractorId, $contractId)
    {
        return Compliance::query()
            ->where('contractor_id', $contractorId)
            ->where('contract_id', $contractId)
            ->first();
    }

    public static function findByContract($contractId)
    {
        return Compliance::query()
            ->where('contract_id', $contractId)
            ->first();
    }

    // relations

    public function contract()
    {
        return $this->belongsTo(Contract::class);
    }

    public function contractor()
    {
        return $this->belongsTo(Contractor::class);
    }

    public function files()
    {
        return $this->hasMany(ComplianceFile::class, 'compliance_id');
    }

    public function verifications()
    {
        return $this->hasMany(ComplianceVerification::class, 'compliance_id');
    }

    public function contributions()
    {
        return Contributions::from($this);
    }

    public function payroll()
    {
        return $this->belongsTo(Payroll::class);
    }

    public function pmrs()
    {
        return $this->hasMany(Pmr::class, 'compliance_id');
    }


    // Accessors

    public function getPreviousComplianceAttribute()
    {
        $periodDate = Carbon::create($this->period_year, $this->period_month);

        return self::select('*')
            ->where('contract_id', $this->contract_id)
            ->whereRaw(
                "DATE(CONCAT(period_year, '-', period_month, '-', '1')) <
                '{$periodDate->format('Y-m-d')}' "
            )
            ->latest()
            ->first();
    }

    public function getDocumentTypeIdAttribute()
    {
        return $this->contractor->document_type_id;
    }

    public function getDocumentTypeAttribute()
    {
        return $this->contractor->document_type->name;
    }

    public function getDocumentAttribute()
    {
        return $this->contractor->document;
    }

    public function getNameAttribute()
    {
        return $this->contractor->name;
    }

    public function getSurnameAttribute()
    {
        return $this->contractor->surname;
    }

    public function getContractTypeIdAttribute()
    {
        return $this->contract->contract_type_id;
    }

    public function getContractTypeAttribute()
    {
        return $this->contract->contract_type->name;
    }

    public function getContractNumberAttribute()
    {
        return $this->contract->contract;
    }

    public function getSubdirectorateIdAttribute()
    {
        return $this->contract->subdirectorate_id;
    }

    public function getAreaIdAttribute()
    {
        return $this->contract->dependency_id;
    }

    /* public function getSubdirectorateAttribute()
    {
        return Subdirectorate::find($this->subdirectorate_id)->name;
    }

    public function getAreaAttribute()
    {
        return Area::find($this->area_id)->name;
    }*/

    public function getHasChildrenAttribute()
    {
        return boolval($this->getFieldValue('has_children'));
    }

    public function getWithholdingAttribute()
    {
        return boolval($this->getFieldValue('withholding'));
    }

    public function getChildrenNumberAttribute()
    {
        return $this->getFieldValue('children_number');
    }

    public function getChildrenMinorAttribute()
    {
        return boolval($this->getFieldValue('children_minor'));
    }

    public function getChildrenMayorStudentsAttribute()
    {
        return boolval($this->getFieldValue('children_mayor_students'));
    }

    public function getChildrenDisabilitiesAttribute()
    {
        return boolval($this->getFieldValue('children_disabilities'));
    }

    public function getSpouseAttribute()
    {
        return boolval($this->getFieldValue('spouse'));
    }

    public function getParentsDependencyAttribute()
    {
        return boolval($this->getFieldValue('parents_dependency'));
    }

    public function getIncomeTaxFilerAttribute()
    {
        return boolval($this->getFieldValue('income_tax_filer'));
    }

    public function getWithholdingValueAttribute()
    {
        return $this->getFieldValue('withholding_value');
    }

    public function getDependentValueAttribute()
    {
        return $this->getFieldValue('dependent_value');
    }

    public function getAfcValueAttribute()
    {
        return $this->getFieldValue('afc_value');
    }

    public function getAfpVoluntaryValueAttribute()
    {
        return $this->getFieldValue('afp_voluntary_value');
    }

    public function getHousingInterestsAttribute()
    {
        return boolval($this->getFieldValue('housing_interests'));
    }

    public function getPrepaidMedicineAttribute()
    {
        return boolval($this->getFieldValue('prepaid_medicine'));
    }

    public function getPensionerAttribute()
    {
        return boolval($this->getFieldValue('pensioner'));
    }

    public function getPercentageAttribute()
    {
        return (int)($this->getFieldValue('percentage'));
    }

    public function getRetentionAttribute()
    {
        return boolval($this->getFieldValue('retention'));
    }

    public function getResponsibleIvaAttribute()
    {
        return boolval($this->getFieldValue('responsible_iva'));
    }

    public function getThirdPeopleAttribute()
    {
        return boolval($this->getFieldValue('third_people'));
    }

    public function getSpouseTaxAttribute()
    {
        return boolval($this->getFieldValue('spouse_tax'));
    }

    public function getAfpVoluntaryAttribute()
    {
        return boolval($this->getFieldValue('afp_voluntary'));
    }

    public function getAfcVoluntaryAttribute()
    {
        return boolval($this->getFieldValue('afc_voluntary'));
    }

    public function getPensionerFileAttribute()
    {
        return $this->files()
            ->whereHas('fileType', function ($q) {
                $q->name('pensioner');
            })
            ->first();
    }

    public function getAfpVoluntaryFileAttribute()
    {
        return $this->files()
            ->whereHas('fileType', function ($q) {
                $q->name('afp_voluntary');
            })
            ->first();
    }

    public function getAfcVoluntaryFileAttribute()
    {
        return $this->files()
            ->whereHas('fileType', function ($q) {
                $q->name('afc_voluntary');
            })
            ->first();
    }

    public function getSupportPaymentFileAttribute()
    {
        return $this->files()
            ->whereHas('fileType', function ($q) {
                $q->name('support_payment');
            })
            ->first();
    }

    public function getActivityReportFileAttribute()
    {
        return $this->files()
            ->whereHas('fileType', function ($q) {
                $q->name('activity_report');
            })
            ->first();
    }

    public function getVerificationCodeAttribute()
    {
        $verification = $this->getPendingVerification();
        return $verification ? $verification->code : null;
    }

    public function getDateValidatorAttribute()
    {
        return ComplianceMonthlyValidator::create(self::getPeriodStartDay(), self::getPeriodEndDay());
    }

    public function getPeriodStartAttribute()
    {
        return Carbon::create($this->period_year, $this->period_month, self::getPeriodStartDay());
    }

    public function getPeriodEndAttribute()
    {
        return Carbon::create($this->period_year, $this->period_month, 1, 23, 59, 59)
            ->setUnitNoOverflow('day', self::getPeriodEndDay(), 'month');
    }

    public function getArlTotalPercentageAttribute()
    {
        $risk = $this->contract->risk;

        switch ($risk) {
            case 1:
                return 0.00522;
            case 2:
                return 0.01044;
            case 3:
                return 0.02436;
            case 4:
                return 0.0435;
            case 5:
                return 0.0696;
            default:
                return 0;
        }
    }

    // Mutators

    public function setSubdirectorateIdAttribute($value)
    {
        $this->contract->subdirectorate_id = $value;
        $this->contract->save();
    }

    public function setAreaIdAttribute($value)
    {
        $this->contract->dependency_id = $value;
        $this->contract->save();
    }

    public function setHasChildrenAttribute($value)
    {
        $this->contribution(Contributions::HAS_CHILDREN)->valid();
        $this->attributes['has_children'] = $value;
    }

    public function setChildrenNumberAttribute($value)
    {
        $this->contribution(Contributions::HAS_CHILDREN)->valid();
        $this->attributes['children_number'] = $value;
    }

    public function setChildrenMinorAttribute($value)
    {
        $this->contribution(Contributions::CHILDREN_MINOR)->valid();
        $this->attributes['children_minor'] = $value;
    }

    public function setChildrenMinorFilesAttribute($value)
    {
        $this->contribution(Contributions::CHILDREN_MINOR)->valid()
            ->setFile($value);
    }

    public function setChildrenMayorStudentsAttribute($value)
    {
        $this->contribution(Contributions::CHILDREN_MAYOR_STUDENTS)->valid();
        $this->attributes['children_mayor_students'] = $value;
    }

    public function setChildrenMayorStudentsFilesAttribute($value)
    {
        $this->contribution(Contributions::CHILDREN_MAYOR_STUDENTS)->valid()
            ->setFile($value);
    }

    public function setChildrenDisabilitiesAttribute($value)
    {
        $this->contribution(Contributions::CHILDREN_DISABILITIES)->valid();
        $this->attributes['children_disabilities'] = $value;
    }

    public function setChildrenDisabilitiesFilesAttribute($value)
    {
        $this->contribution(Contributions::CHILDREN_DISABILITIES)->valid()
            ->setFile($value);
    }

    public function setSpouseAttribute($value)
    {
        $this->contribution(Contributions::SPOUSE)->valid();
        $this->attributes['spouse'] = $value;
    }

    public function setSpouseFilesAttribute($value)
    {
        $this->contribution(Contributions::SPOUSE)->valid()
            ->setFile($value);
    }

    public function setParentsDependencyAttribute($value)
    {
        $this->contribution(Contributions::PARENTS_DEPENDENCY)->valid();
        $this->attributes['parents_dependency'] = $value;
    }

    public function setParentsDependencyFilesAttribute($value)
    {
        $this->contribution(Contributions::PARENTS_DEPENDENCY)->valid()
            ->setFile($value);
    }

    public function setIncomeTaxFilerAttribute($value)
    {
        $this->contribution(Contributions::INCOME_TAX_FILER)->valid();
        $this->attributes['income_tax_filer'] = $value;
    }

    public function setWithholdingValueAttribute($value)
    {
        $this->contribution(Contributions::WITHHOLDING)->valid();
        $this->attributes['withholding_value'] = $value;
    }

    public function setDependentValueAttribute($value)
    {
        $this->contribution(Contributions::DEPENDENT_VALUE)->valid();
        $this->attributes['dependent_value'] = $value;
    }
    public function setAfcValueAttribute($value)
    {
        $this->contribution(Contributions::AFC_VALUE)->valid();
        $this->attributes['afc_value'] = $value;
    }

    public function setAfpVoluntaryValueAttribute($value)
    {
        $this->contribution(Contributions::AFP_VOLUNTARY_VALUE)->valid();
        $this->attributes['afp_voluntary_value'] = $value;
    }

    public function setHousingInterestsAttribute($value)
    {
        $this->contribution(Contributions::HOUSING_INTERESTS)->valid();
        $this->attributes['housing_interests'] = $value;
    }

    public function setHousingInterestsFilesAttribute($value)
    {
        $this->contribution(Contributions::HOUSING_INTERESTS)->valid()
            ->setFile($value);
    }

    public function setPrepaidMedicineAttribute($value)
    {
        $this->contribution(Contributions::PREPAID_MEDICINE)->valid();
        $this->attributes['prepaid_medicine'] = $value;
    }

    public function setPrepaidMedicineFileAttribute($value)
    {
        $this->contribution(Contributions::PREPAID_MEDICINE)->valid()
            ->setFile($value);
    }

    public function setSupportPaymentFileAttribute($value)
    {
        $fileType = ComplianceFileType::query()
            ->name('support_payment')
            ->first(['id', 'name']);

        $fileName = ComplianceFile::getFileName($fileType, $this->contract_number);

        $this->files()
            ->firstOrCreate(
                [ 'file_type' => $fileType->id ],
                [ 'file_type' =>  $fileType->id, 'file_name' =>  $fileName ]
            )
            ->store($value);
    }

    public function setPensionerFileAttribute($value)
    {
        $this->contribution(Contributions::PENSIONER)->valid()
            ->setFile($value);
    }

    public function setAfpVoluntaryFileAttribute($value)
    {
        $this->contribution(Contributions::AFP_VOLUNTARY)->valid()
            ->setFile($value);
    }

    public function setAfcVoluntaryFileAttribute($value)
    {
        $this->contribution(Contributions::AFC_VOLUNTARY)->valid()
            ->setFile($value);
    }

    public function setAfcVoluntaryAttribute($value)
    {
        $this->contribution(Contributions::AFC_VOLUNTARY)->valid();
        $this->attributes['afc_voluntary'] = $value;
    }

    public function setAfpVoluntaryAttribute($value)
    {
        $this->contribution(Contributions::AFP_VOLUNTARY)->valid();
        $this->attributes['afp_voluntary'] = $value;
    }

    public function setSpouseTaxAttribute($value)
    {
        $this->contribution(Contributions::SPOUSE_TAX)->valid();
        $this->attributes['spouse_tax'] = $value;
    }

    public function setThirdPeopleAttribute($value)
    {
        $this->contribution(Contributions::THIRD_PEOPLE)->valid();
        $this->attributes['third_people'] = $value;
    }

    public function setResponsibleIvaAttribute($value)
    {
        $this->contribution(Contributions::RESPONSIBLE_IVA)->valid();
        $this->attributes['responsible_iva'] = $value;
    }

    public function setRetentionAttribute($value)
    {
        $this->contribution(Contributions::RETENTION)->valid();
        $this->attributes['retention'] = $value;
    }

    public function setPercentageAttribute($value)
    {
        $this->contribution(Contributions::PERCENTAGE)->valid();
        $this->attributes['percentage'] = $value;
    }

    public function setActivityReportFileAttribute($value)
    {
        $fileType = ComplianceFileType::query()
            ->name('activity_report')
            ->first(['id', 'name']);

        $fileName = ComplianceFile::getFileName($fileType, $this->contract_number);

        $this->files()
            ->firstOrCreate(
                [ 'file_type' => $fileType->id ],
                [ 'file_type' =>  $fileType->id, 'file_name' =>  $fileName ]
            )
            ->store($value);
    }
    // methods

    public function canBeModified()
    {
        return $this->isEditable() || $this->isCorrectable();
    }

    public function isEditable()
    {
        $period = $this->date_validator->period();

        return $this->hasntBeenEdited()
            && now()->betweenIncluded(
                $period['start'],
                $period['end']
            );
    }

    public function isCorrectable()
    {
        $period = $this->date_validator->period();

        return $this->isRejected()
            && now()->betweenIncluded(
                $period['start'],
                $period['end']->addUnitNoOverflow('day', 1, 'month')
            );
    }

    public function isRejected()
    {
        return $this->status === 'REJECTED';
    }

    public function isFirstTime()
    {
        return !self::query()
            ->where('id', '!=', $this->id)
            ->where('contract_id', $this->contract_id)
            ->exists();
    }

    public function hasntBeenEdited()
    {
        return $this->created_at->equalTo($this->updated_at);
    }

    public function contribution(string $name)
    {
        return $this->contributions()[$name];
    }

    public function getFieldValue(string $name)
    {
        if ($this->isFirstTime()) return $this->attributes[$name];

        return $this->hasntBeenEdited()
            ? ($this->previous_compliance ? $this->previous_compliance->$name : $this->attributes[$name])
            : $this->attributes[$name];
    }

    public function getFieldFile(string $name)
    {
        $file = $this->files()
            ->whereHas('fileType', function ($q) use ($name) {
                $q->name($name);
            })
            ->first();

        if ($this->isFirstTime()) return $file;

        return $this->hasntBeenEdited() ?
            ($this->previous_compliance ? $this->previous_compliance->getFieldFile($name) : $file)
            : $file;
    }

    public static function getPeriodStartDay()
    {
        return env('APP_ENV') === 'production'
            ? self::COMPLIANCE_PERIOD_START_DAY
            : 1;
    }

    public static function getPeriodEndDay()
    {
        return env('APP_ENV') === 'production'
            ? self::COMPLIANCE_PERIOD_END_DAY
            : 30;
    }

    public static function getPeriodStart()
    {
        $now = now();
        return Carbon::create($now->year, $now->month, self::getPeriodStartDay());
    }

    public static function getPeriodEnd()
    {
        $now = now();
        return Carbon::create($now->year, $now->month, 1, 23, 59, 59)
            ->setUnitNoOverflow('day', self::getPeriodEndDay(), 'month');
    }

    public function getPendingVerification()
    {
        return $this->verifications()
            ->where('status', 'PENDING')
            ->latest()
            ->first();
    }

    public function createNewVerification(string $code)
    {
        return $this->verifications()
            ->updateOrCreate([ 'status' => 'REDEEMED' ],
                [
                    'code' => $code,
                    'status' => 'PENDING'
                ]
            );
    }
}
