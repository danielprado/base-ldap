<?php


namespace App\Modules\Contractors\src\Request;


use App\Modules\Contractors\src\Models\FileType;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreFileRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'file_type_id'  =>  'required|numeric|exists:mysql_contractors.file_types,id',
            'files.*'          =>  'required|file',
            'coverage_start_date' => 'nullable|required_if:file_type_id,'.FileType::ARL.'|date|date_format:Y-m-d,before:start_date',
            'coverage_final_date' => 'nullable|required_if:file_type_id,'.FileType::ARL.'|date|date_format:Y-m-d,after:final_date',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'file_type_id'  =>  'tipo de archivo',
            'file'  =>  'archivos',
        ];
    }
}
