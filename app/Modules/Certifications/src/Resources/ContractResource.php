<?php

namespace App\Modules\Certifications\src\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ContractResource extends JsonResource
{
    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    public function toArray($request): array
    {
        $expedition = $this->expeditions()->latest()->first();
        return [
            // Contractor
            'document_type_id' => isset($this->Tipo_Documento) ? (int)$this->Tipo_Documento : null,
            'document_type'     => $this->document_type->name ?? null,
            'document' => isset($this->Cedula) ? (int)$this->Cedula : null,
            'dv' => isset($this->Dv) ? (int)$this->Dv : null,
            'name' => toUpper($this->Nombre_Contratista) ?? null,
            // Legal Representative
            'document_type_representative_id' => isset($this->Tipo_Documento_Representante) ? (int)$this->Tipo_Documento_Representante : null,
            'document_type_representative'     => $this->document_type_representative->name ?? null,
            'document_representative' => isset($this->Cedula_Representante) ? (int)$this->Cedula_Representante : null,
            'name_representative' => $this->Nombre_Representante ?? null,
            // Contract
            'id' => isset($this->Id) ? (int)$this->Id : null,
            'contract_type_id' => isset($this->Tipo_Contrato_Id) ? (int)$this->Tipo_Contrato_Id : null,
            'contract_type' => $this->contractTypes->Nombre_Tipo_Contrato ?? null,
            'contract_number' => isset($this->Numero_Contrato) ? (int)$this->Numero_Contrato : null,
            'contract_year' => $this->contractYear($this->year, $this->Fecha_Firma) ?? null,
            'contract' => isset($this->Numero_Contrato) ? $this->contract_format($this->Referencia, $this->Numero_Contrato, $this->Fecha_Firma->format('Y')) : null,
            'object' => toUpper($this->Objeto) ?? null,
            'signature_date' => isset($this->Fecha_Firma) ? $this->dateFormat($this->Fecha_Firma) : null,
            'start_date' => isset($this->Fecha_Inicio) ? $this->dateFormat($this->Fecha_Inicio) : null,
            'final_date' => isset($this->Fecha_Fin) ? $this->dateFormat($this->Fecha_Fin) : null,
            'anticipated_date' => isset($this->Fecha_Terminacion_Anticipada) ? $this->dateFormat($this->Fecha_Terminacion_Anticipada) : null,
            'duration_months' => isset($this->Meses_Duracion) ? (int)$this->Meses_Duracion : null,
            'duration_days' => isset($this->Dias_Duracion) ? (int)$this->Dias_Duracion : null,
            'duration_other' => isset($this->Otra_Duracion) ? $this->otherDuration($this->Otra_Duracion) : null,
            'initial_value' => isset($this->Valor_Inicial) ? (int)$this->Valor_Inicial : null,
            'monthly_value' => isset($this->Valor_Mensual) ? (int)$this->Valor_Mensual : null,
            'status' => isset($this->Fecha_Fin) ? $this->status($this->Fecha_Fin) : null,
            'reference' => isset($this->Referencia) ? toUpper($this->Referencia) : null,
            'created_at' => isset($this->created_at) ? $this->created_at->format('Y-m-d') : null,
            'updated_at' => isset($this->updated_at) ? $this->updated_at->format('Y-m-d') : null,

            // Relationships
            'obligations' => ObligationResource::collection($this->whenLoaded('obligations')),
            'additions' => AdditionResource::collection($this->whenLoaded('additions')),
            'expeditions' => isset($expedition) ? ExpeditionResource::make($this->whenLoaded('expeditions', $expedition)) : [],
            'assignments' => AssignmentResource::collection($this->whenLoaded('assignments')),
            'extensions' => ExtensionResource::collection($this->whenLoaded('extensions')),
            'suspensions' => SuspensionResource::collection($this->whenLoaded('suspensions')),
            'members' => MemberResource::collection($this->whenLoaded('members')),
        ];
    }

    public function status($final_date): string
    {
        if ($final_date >= now()) {
            return 'Activo';
        }
        return 'Finalizado';
    }

    public function otherDuration($other_duration)
    {
        if ($other_duration != 0 || !empty($other_duration)) {
            return toUpper($other_duration);
        }
        return null;
    }

    public function dateFormat($date)
    {
        $dt = date_parse($date);
        if ($dt['year'] == -1) {
            return null;
        }
        return $date->format('Y-m-d');
    }

    public function contractYear($year, $date)
    {
        if ($year != null) {
            return $year;
        } elseif ($date != null && $date != '0000-00-00') {
            return $date->format('Y');
        } else {
            return null;
        }
    }

    public function contract_format($reference, $number, $year)
    {
        $contract_number = str_pad($number, 4, '0', STR_PAD_LEFT);
        if (isset($reference)) {
            return toUpper("IDRD-{$reference}-{$contract_number}-{$year}");
        } else {
            return toUpper("IDRD-CTO-{$contract_number}-{$year}");
        }
    }

    public static function headers(): array
    {
        return [
            [
                'text' => "#",
                'value'  =>  "id",
                'sortable' => false
            ],
            [
                'align' => "center",
                'text' => "Detalles",
                'value'  =>  "view",
                'sortable' => false
            ],
            [
                'align' => "center",
                'text' => "Nombres",
                'value'  =>  "name",
                'sortable' => false
            ],
            [
                'text' => "Documento",
                'value'  =>  "document",
                'sortable' => false
            ],
            [
                'text' => "Referencia",
                'value'  =>  "reference",
                'sortable' => false
            ],
            [
                'text' => "Número de contrato",
                'value'  =>  "contract_number",
                'sortable' => false
            ],
            [
                'text' => "Año de contrato",
                'value'  =>  "contract_year",
                'sortable' => false
            ],
            [
                'text' => "Tipo de contrato",
                'value'  =>  "contract_type",
                'sortable' => false
            ],
            [
                'text' => "Fecha de inicio",
                'value'  =>  "start_date",
                'sortable' => false
            ],
            [
                'text' => "Fecha de finalización",
                'value'  =>  "final_date",
                'sortable' => false
            ],
            [
                'text' => "Fecha de registro",
                'value'  =>  "created_at",
                'sortable' => false
            ],
            [
                'align'     => "center",
                'text'      => "Acciones",
                'value'     =>  "actions",
                'sortable'  => false
            ],
        ];
    }

}
