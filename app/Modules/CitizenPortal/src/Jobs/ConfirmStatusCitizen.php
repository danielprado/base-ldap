<?php


namespace App\Modules\CitizenPortal\src\Jobs;


use App\Modules\CitizenPortal\src\Mail\NotificationMail;
use App\Modules\CitizenPortal\src\Models\Profile;
use App\Modules\CitizenPortal\src\Models\ProfileView;
use App\Modules\CitizenPortal\src\Models\Status;
use App\Modules\CitizenPortal\src\Notifications\SendgridTemplates;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use SendGrid\Mail\Mail;
use Swift_Mailer;
use Swift_SmtpTransport;


class ConfirmStatusCitizen implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Profile
     */
    private $user;

    /**
     * @var string
     */
    private $observation;

    /**
     * @var string
     */
    private $test_mail;

    /**
     * Create a new job instance.
     *
     * @param Profile $profile
     * @param $observation
     * @param null $test_mail
     */
    public function __construct(Profile $profile, $observation, $test_mail = null)
    {
        $this->user = $profile;
        $this->observation = $observation;
        $this->test_mail = $test_mail;
    }

    /**
     * Execute the job.
     *
     * @param Mail $mailer
     * @return void
     */
    public function handle(Mail $mailer)
    {
        $email = isset( $this->user->user->email ) ? (string) $this->user->user->email : null;
        if (config('app.env') != 'production') {
         $email = $this->test_mail ?? null;
        }
        if ( $email  && filter_var( $email, FILTER_VALIDATE_EMAIL) ) {
            $subject = "Portal Ciudadano - Estado de Validación";
            $notify = new SendgridTemplates();
            $name = $this->user->profile_type_id != Profile::PROFILE_PERSONAL
                ? $this->user->user->user_profile->email_full_name ?? "Ciudadano"
                : "Ciudadano";
            $body = [
                "name"      => $name,
                "sub_user"  => "{$this->user->email_full_name}",
                "status"    =>  "{$this->user->status->name}",
                "observation" => $this->observation,
            ];
            switch ($this->user->status->id) {
                case Status::VALIDATING:
                    $notify->sendCustomMessage(
                        $email,
                        array_merge(
                            $body,
                            [
                                "info"  =>  "Por favor verifique y actualice la información, dispone de 2 días hábiles."
                            ]
                        ),
                        "$subject",
                        "#fb8c00",
                        "#f6dfc1"
                    );
                    break;
                case Status::RETURNED:
                case Status::FILE_RETURNED:
                    $notify->sendErrorMessage(
                        $email,
                        array_merge(
                            $body,
                            [
                                "info"  =>  "Por favor verifique y actualice la información, dispone de 2 días hábiles."
                            ]
                        ),
                        "$subject"
                    );
                    break;
                case Status::VERIFIED:
                case Status::FILE_VERIFIED:
                    $notify->sendSuccessMessage(
                        $email,
                        array_merge(
                            $body,
                            [
                                "info"  =>  "Ya puede acceder e inscribirse a los servicios disponibles a través del siguiente enlace."
                            ]
                        ),
                        "$subject"
                    );
                    break;
                default:
                    $notify->sendInfoMessage(
                        $email,
                        $body,
                        "$subject"
                    );
                    break;
            }
        }
    }
}
