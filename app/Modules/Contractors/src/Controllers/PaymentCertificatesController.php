<?php


namespace App\Modules\Contractors\src\Controllers;


use App\Helpers\FPDF;
use App\Http\Controllers\Controller;
use App\Modules\Contractors\src\Jobs\VerificationCodePaymentCertificate;
use App\Modules\Contractors\src\Models\Certification;
use App\Modules\Contractors\src\Models\Contractor;
use App\Modules\Contractors\src\Request\CertificatePaymentRequest;
use App\Modules\Contractors\src\Resources\UserSevenPaymentResource;
use Exception;
use Illuminate\Contracts\Container\BindingResolutionException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Modules\Contractors\src\Request\ConsultaRequest;
use App\Modules\Contractors\src\Request\ValidacionUsuarioRequest;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Modules\Contractors\src\Jobs\VerificationCodeTributario;
use App\Modules\Contractors\src\Request\ValidacionRequest;
use App\Modules\Contractors\src\Request\IVAICACONRequest;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use LaravelQRCode\Facades\QRCode;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use setasign\Fpdi\PdfParser\CrossReference\CrossReferenceException;
use setasign\Fpdi\PdfParser\Filter\FilterException;
use setasign\Fpdi\PdfParser\PdfParserException;
use setasign\Fpdi\PdfParser\Type\PdfTypeException;
use setasign\Fpdi\PdfReader\PdfReaderException;

class PaymentCertificatesController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @param CertificatePaymentRequest $request
     * @return JsonResponse
     */
    public function index(CertificatePaymentRequest $request): JsonResponse
    {
        try {
            DB::connection('mysql_contractors')->beginTransaction();
            $contractor = Contractor::query()
                ->where('document', $request->get('document'))
                ->where('birthdate', $request->get('birthdate'))
                ->with(['contracts' => function ($query) use ($request) {
                    $query->where('contract', 'like', "%-{$request->get('contract')}-{$request->get('year')}%");
                }])
                ->first();

            if (is_null($contractor)) {
                return $this->error_response(
                    'No se ha encontrado un contrato con la información proporcionada, por favor revise los datos e intente de nuevo.',
                    Response::HTTP_NOT_FOUND
                );
            }

            $http = new Client([
                'base_uri' => env('URL_DOCKER_SERVER'),
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                ],
            ]);

            $response=$http->post("/api/contractors-portal/certificate-payment", [
                "json" => [
                    'document' => $request->get('document'),
                    'contract' => $request->get('contract'),
                    'year' => $request->get('year')
                ],
            ]);

            $data = json_decode($response->getBody()->getContents(), true);

            if (!isset($data["data"][0])){
                return $this->error_response(
                    "No se encontró información de pagos para el contrato {$request->get('contract')} vinculado al contratista identificado con el número de documento {$request->get('document')}"
                );
            }

            $contract = $contractor->contracts->first();

            $certification = Certification::query()->firstOrCreate([
                'document' => $request->get('document'),
                'contract' => $contract->contract,
                'type' => 'PYM'
            ],[
                'name'  => $contractor->full_name,
                'document' => $request->get('document'),
                'contractor_id' => $contractor->id,
                'year' => $request->get('year'),
                'type' => 'PYM',
                'contract' => $contract->contract ?? null
            ]);

            $this->dispatch(new VerificationCodePaymentCertificate($contractor, $certification));
            $email=mask_email($contractor->email);
            DB::connection('mysql_contractors')->commit();
            return $this->success_message("Hemos enviado un código de verificación al correo $email.");
        } catch (\Throwable $e) {
            DB::connection('mysql_contractors')->rollBack();
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }

    /**
     *
     * @param CertificatePaymentRequest $request
     * @return JsonResponse
     */
    public function validateUser(CertificatePaymentRequest $request)
    {
        $certification = Certification::query()
            ->with('contractor.document_type')
            ->where('document', $request->get('document'))
            ->where('code', $request->get('code'))
            ->where('type', '=','PYM')
            ->first();

        if (is_null($certification)) {
            return $this->error_response(
                "El código ingresado no coincide. Verifíquelo nuevamente.",
                Response::HTTP_NOT_FOUND
            );
        }

        return $this->generateCertificate($request, $certification, $certification->contractor);
    }

    /**
     *
     * @param Request $request
     * @param Certification $certification
     * @param Contractor $contractor
     * @return JsonResponse
     */
    public function generateCertificate(Request $request, Certification $certification, Contractor $contractor): JsonResponse
    {
        $data = $this->getData($request);
        $name = $certification->name ?? '';
        $document= $certification->document ?? '';
        $document_type = $contractor->document_type->name ?? '';
        $contract = $certification->contract ?? $request->get('contract') . "-" . $request->get('year');
        $text  = "<p>Que de conformidad con los registros financieros que reposan en la Tesorería del IDRD, en el aplicativo SEVEN, se ";
        $text .= "encuentra que a <b>$name</b> identificado/a con <b>$document_type</b> No.<b>$document</b>, se le realizaron ";
        $text .= "pagos, por el Contrato No. <b>$contract</b>, como se relaciona a continuación: </p>";

        $file = $this->createFile($text, $certification, $data)->Output('S', "CERTIFICADO_PAGOS_{$document}.pdf", true);

        return $this->success_message([
            'file_name' =>  "CERTIFICADO_PAGOS_{$document}.pdf",
            'file'  => "data:application/pdf;base64,".base64_encode($file)
        ]);
    }

    /**
     *
     * @param Request $request
     * @return JsonResponse|mixed
     */
    public function getData(Request $request)
    {
        $http = new Client([
            'base_uri' => env('URL_DOCKER_SERVER'),
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ],
        ]);

        $response=$http->post("/api/contractors-portal/certificate-payment", [
            "json" => [
                'document' => $request->get('document'),
                'contract' => $request->get('contract'),
                'year' => $request->get('year')
            ],
        ]);

        $data = json_decode($response->getBody()->getContents(), true);

        if (!isset($data["data"][0])){
            return $this->error_response(
                "No se encontro un informe de pagos para el contrato {$request->get('contract')} vinculado al contratista identificado con el número de documento {$request->get('document')}"
            );
        }

        return $data["data"];
    }

    /**
     *
     * @param $text
     * @param Certification $certification
     * @param $data
     */
    public function createFile($text, Certification $certification, $data)
    {
        $collection = collect($data ?? []);
        $created_at = isset($certification->updated_at) ? $certification->updated_at->format('Y-m-d H:i:s') : null;

        $pdf = new FPDF("L", "mm", "Letter");
        $pdf->SetStyle("p","Helvetica","N",10,"0,0,0",0);
        $pdf->SetStyle("small","Helvetica","N",8,"0,0,0",0);
        $pdf->SetStyle("h1","Helvetica","N",14,"0,0,0",0);
        $pdf->SetStyle("a","Helvetica","BU",9,"0,0,0", 15);
        $pdf->SetStyle("pers","Helvetica","I",0,"0,0,0");
        $pdf->SetStyle("place","Helvetica","U",0,"0,0,0");
        $pdf->SetStyle("b","Helvetica","B",0,"0,0,0");
        $pdf->AddPage();

        $pdf->setSourceFile(storage_path("app/templates/CERTIFICADO_PAGOS.pdf"));
        $tplId = $pdf->importPage(1);
        $pdf->useTemplate($tplId, 0, 0, null, null, true);

        $pdf->SetFont('Helvetica', 'B');
        $pdf->SetFontSize(8);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(30, 32);
        $pdf->Cell(160,10, utf8_decode('Fecha de solicitud original: '.$created_at),0,0,'L');

        $pdf->SetFont('Helvetica');
        $pdf->SetFontSize(10);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetLeftMargin(30);
        $pdf->SetRightMargin(25);
        $pdf->SetXY(30, 65);
        $pdf->WriteTag(160, 5, utf8_decode($text));

        $tableWidth = 180;
        $pageWidth = $pdf->GetPageWidth();
        $xInitial = ($pageWidth - $tableWidth) / 2;

        $pdf->SetXY($xInitial, 95);
        $pdf->SetFont('Helvetica', 'B', 9);
        $pdf->Cell(30, 7, 'NO. DE PAGO', 1, 0, 'C');
        $pdf->Cell(60, 7, 'NO. COMPROBANTE DE EGRESO', 1, 0, 'C');
        $pdf->Cell(40, 7, 'FECHA PAGO', 1, 0, 'C');
        $pdf->Cell(50, 7, 'VALOR BRUTO', 1, 1, 'C');

        $total = 0;
        $number = 1;
        foreach ($collection as $row) {
            $pdf->SetX($xInitial);
            $pdf->SetFont('Helvetica', '', 9);
            if ($pdf->GetY() + 5 > $pdf->GetPageHeight() - 50) {
                $pdf->AddPage();
                $tplId = $pdf->importPage(1);
                $pdf->useTemplate($tplId, 0, 0, null, null, true);
                $pdf->SetXY($xInitial, 70);
            }

            $pdf->Cell(30, 7, utf8_decode($number), 1, 0, 'C');
            $pdf->Cell(60, 7, utf8_decode($row['mte_nume']), 1, 0, 'C');
            $pdf->Cell(40, 7, utf8_decode(date('d/m/Y', strtotime($row['mte_fech']))), 1, 0, 'C');
            $pdf->Cell(50, 7, number_format($row['dmp_valo'], 2, ',', '.'), 1, 1, 'C');

            $total += floatval($row['dmp_valo']);
            $number++;
        }

        $pdf->SetX($xInitial);
        $pdf->SetFont('Helvetica', 'B', 9);
        $pdf->Cell(130, 7, 'TOTAL', 1, 0, 'C');
        $pdf->Cell(50, 7, number_format($total, 2, ',', '.'), 1, 1, 'C');

        // Footer QR and document authentication
        $pdf->SetXY(30, 108);
        $name = isset( $certification->token ) ? $certification->token : 'PYM'.'-'.Str::random(9);
        $path = config('app.env') != 'production'
            ? "/portal-contratista-dev"
            : "/";
        $subdomain = config('app.env') != 'production' ? "sim" : "portalcontratista";
        $url = "https://{$subdomain}.idrd.gov.co{$path}/es/validar-documento?validate=$name";
        QrCode::url($url)
            ->setErrorCorrectionLevel('H')
            ->setSize(10)
            ->setOutfile(storage_path("app/templates/{$name}.png"))
            ->png();
        $file = storage_path("app/templates/{$name}.png");
        $pdf->Image($file, 30, 195, 50, 50);

        $signatureFile = storage_path("app/templates/Firma_Tesoreria.png");
        $pdf->Image($signatureFile, 85, 192, 50, 30);

        $pdf->SetXY(80, 220);
        $pdf->SetFontSize(8);
        $x = 'La autenticidad de este documento se puede validar a través del enlace inferior.';
        $pdf->Write(5 , utf8_decode($x));
        $pdf->SetXY(80, 225);
        $pdf->Cell(30, 5, utf8_decode('O escaneando el código QR desde un dispositivo móvil.'));
        $pdf->SetXY(80, 235);
        $pdf->SetFontSize(12);
        $pdf->Cell(30, 5, utf8_decode('Código de verificación: '.$name));
        $pdf->SetFontSize(8);
        $pdf->SetXY(32, 245);
        $pdf->Write(5, $url, $url);
        if (Storage::disk('local')->exists("templates/{$name}.png")) {
            Storage::disk('local')->delete("templates/{$name}.png");
        }

        if (!isset( $certification->token )) {
            $certification->token = $name;
        }
        $certification->increment('downloads');
        $certification->save();

        return $pdf;
    }

    public function getUserPayment(Request $request): JsonResponse
    {
        $data = DB::connection('oracle_ws')
            ->table('SEVEN.TS_MTESO as M')
            ->join('SEVEN.PG_MPRES as P', function($join) {
                $join->on('P.MPR_NUME', '=', 'M.MTE_NUME')
                    ->where(function($query) {
                        $query->whereColumn('P.TOP_CODI', 'M.TOP_CODI')
                            ->orWhere(function($subQuery) {
                                $subQuery->where('P.TOP_CODI', '=', 3511)
                                    ->whereIn('M.TOP_CODI', [1160, 1177]);
                            });
                    });
            })
            ->join('SEVEN.PG_DMPRE as D', function($join) {
                $join->on('D.EMP_CODI', '=', 'P.EMP_CODI')
                    ->on('D.MPR_CONT', '=', 'P.MPR_CONT');
            })
            ->join('SEVEN.PG_MPRES as O', function($join) {
                $join->on('D.MPR_BASE', '=', 'O.MPR_CONT')
                    ->on('D.EMP_CODI', '=', 'O.EMP_CODI')
                    ->whereIn('O.MPR_CLAS', ['OBRE', 'OB']);
            })
            ->join('SEVEN.PG_DMPRE as Q', function($join) {
                $join->on('D.DMP_CONT', '=', 'Q.DMP_CONT')
                    ->on('O.EMP_CODI', '=', 'Q.EMP_CODI')
                    ->on('O.MPR_CONT', '=', 'Q.MPR_CONT');
            })
            ->join('SEVEN.PG_MPRES as C', function($join) {
                $join->on('Q.EMP_CODI', '=', 'C.EMP_CODI')
                    ->on('Q.MPR_BASE', '=', 'C.MPR_CONT');
            })
            ->join('SEVEN.GN_TERCE as T', function($join) {
                $join->on('P.TER_CODI', '=', 'T.TER_CODI')
                    ->on('P.EMP_CODI', '=', 'T.EMP_CODI');
            })
            ->join('SEVEN.GN_TIPDO as O', 'T.TIP_CODI', '=', 'O.TIP_CODI')
            ->where('P.TER_CODI', $request->get('document'))
            ->where('C.MPR_NDOS', $request->get('contract'))
            ->whereIn('P.MPR_CLAS', ['PAGR', 'PAG'])
            ->select([
                'M.EMP_CODI',
                'T.TER_CODI',
                'M.MTE_NUME',
                'M.MTE_FECH',
                'C.MPR_NDOS',
                'D.DMP_VALO',
                'T.TER_NOCO',
                'O.TIP_ABRE'
            ])
            ->get();

        return $this->success_response(
            UserSevenPaymentResource::collection($data)
        );
    }

    public function getCertificateSeven(Request $request): JsonResponse
    {
        $data = DB::connection('oracle_ws')
            ->table(DB::raw('SEVEN.TS_MTESO M, SEVEN.PG_MPRES P, SEVEN.PG_DMPRE D, SEVEN.PG_MPRES O, SEVEN.PG_DMPRE Q, SEVEN.PG_MPRES C, SEVEN.GN_TERCE T, SEVEN.GN_TIPDO O'))
            ->select([
                'M.EMP_CODI',
                'T.TER_CODI',
                'M.MTE_NUME',
                'M.MTE_FECH',
                'C.MPR_NDOS',
                DB::raw('SUM(D.DMP_VALO) AS DMP_VALO'),
                'T.TER_NOCO',
                'O.TIP_ABRE'
            ])
            ->whereRaw("P.TER_CODI = '{$request->get('document')}'")
            ->whereRaw("C.MPR_NDOS = '{$request->get('contract')}'")
            ->whereRaw("P.MPR_ANOP = '{$request->get('year')}'")
            ->whereRaw("(P.TOP_CODI = M.TOP_CODI OR (P.TOP_CODI = 3511 AND M.TOP_CODI IN (1160, 1177)))")
            ->whereRaw("M.MTE_NUME = P.MPR_NUME")
            ->whereIn('P.MPR_CLAS', ['PAGR', 'PAG'])
            ->whereRaw("D.EMP_CODI = P.EMP_CODI")
            ->whereRaw("D.MPR_CONT = P.MPR_CONT")
            ->whereIn('O.MPR_CLAS', ['OBRE', 'OB'])
            ->whereRaw("D.DMP_CONT = Q.DMP_CONT")
            ->whereRaw("D.EMP_CODI = O.EMP_CODI")
            ->whereRaw("D.MPR_BASE = O.MPR_CONT")
            ->whereRaw("O.EMP_CODI = Q.EMP_CODI")
            ->whereRaw("O.MPR_CONT = Q.MPR_CONT")
            ->whereRaw("Q.EMP_CODI = C.EMP_CODI")
            ->whereRaw("Q.MPR_BASE = C.MPR_CONT")
            ->whereRaw("P.EMP_CODI = T.EMP_CODI")
            ->whereRaw("P.TER_CODI = T.TER_CODI")
            ->whereRaw("O.TIP_CODI = T.TIP_CODI")
            ->groupBy([
                'M.EMP_CODI',
                'T.TER_CODI',
                'M.MTE_NUME',
                'M.MTE_FECH',
                'C.MPR_NDOS',
                'T.TER_NOCO',
                'O.TIP_ABRE'
            ])
            ->orderBy('M.MTE_FECH', 'ASC')
            ->get();

        return $this->success_response(
            UserSevenPaymentResource::collection($data)
        );
    }
}

