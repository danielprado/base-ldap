<?php

namespace App\Modules\Contractors\src\Resources;

use App\Modules\Contractors\src\Models\Bank;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserThirdPartyFinancialResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'pvd_codi'  => $this->pvd_codi ?? null,
            'ban_codi'  => $this->ban_codi ?? null,
            'ban_codi_text'  => isset($this->ban_codi) ? $this->getBankName($this->ban_codi) : null,
            'cup_nume'  => $this->cup_nume ?? null,
            'cup_tipo'  => $this->cup_tipo ?? null,
            'cup_tipo_text'  => isset($this->cup_tipo) ? $this->getAccountType($this->cup_tipo) : null,
            'cup_esta'  => $this->cup_esta ?? null,
            'cup_esta_text'  => isset($this->cup_esta) ? $this->getState($this->cup_esta) : null,
            'mon_codi'  => $this->mon_codi ?? null,
            'sub_codi'  => $this->sub_codi ?? null,
        ];
    }

    public function getBankName($code)
    {
        $http = new Client();
        $response = $http->get("https://sim.idrd.gov.co/base-ldap/public/api/contractors-portal/banks", [
            'headers' => [
                'Accept'    => 'application/json',
                'Content-type' => 'application/json'
            ],
        ]);

        $data = json_decode($response->getBody()->getContents(), true);
        $bank = collect($data['data'])->where('id', $code)->first();

        return $bank['name'] ?? null;

    }

    public function getAccountType($type): string
    {
        switch ($type) {
            case('A'):
                $msg = "AHORROS";
                break;
            case('C'):
                $msg = "CORRIENTE";
                break;
            default:
                $msg = 'AHORROS';
        }

        return $msg;
    }

    public function getState($state): string
    {
        switch ($state) {
            case('A'):
                $msg = "ACTIVO";
                break;
            case('I'):
                $msg = "INACTIVO";
                break;
            default:
                $msg = 'ACTIVO';
        }

        return $msg;
    }

    public static function headers(): array
    {
        return [
            [
                'label'   =>  'Nombre del banco',
                'field'   =>  'ban_codi_text',
                'icon'    => 'mdi-bank',
            ],
            [
                'label'   =>  'Tipo de cuenta',
                'field'   =>  'cup_tipo_text',
                'icon'    => 'mdi-piggy-bank',
            ],
            [
                'label'   =>  'Número de cuenta',
                'field'   =>  'cup_nume',
                'icon'    => 'mdi-numeric',
            ],
            [
                'label'   =>  'Estado de la cuenta',
                'field'   =>  'cup_esta_text',
                'icon'    => 'mdi-cash-check',
            ],
        ];
    }

}
