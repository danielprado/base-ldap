<?php


namespace App\Modules\CitizenPortal\src\Request;



use App\Modules\CitizenPortal\src\Constants\Roles;
use App\Modules\CitizenPortal\src\Models\Activity;
use App\Modules\PaymentGateway\src\Models\ServiceOffered;
use Illuminate\Foundation\Http\FormRequest;

class RateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /*
        $permission = toLower($this->getMethod()) == 'post'
            ? Roles::can(Activity::class,'create_or_manage', true)
            : Roles::can(Activity::class,'update_or_manage', true);
        */
        return auth('api')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $service = new ServiceOffered();
        return [
            'name'             =>  'required|string|min:3|max:191',
            'description'      =>  'required|string|min:3|max:2500',
            'value'            =>  'required|numeric',
            'service_id'       =>  "required|numeric|exists:{$service->getConnectionName()}.{$service->getTable()},{$service->getKeyName()}",
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name' =>  __('citizen.validations.name'),
            'description' =>  __('citizen.validations.description'),
            'value' =>  __('citizen.validations.value'),
            'service_id' =>  __('citizen.validations.service_id'),
        ];
    }
}
