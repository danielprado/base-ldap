<?php

namespace App\Modules\AttendanceRecord\src\Models;

use App\Modulos\Parque\Localidad;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attendee extends Model
{
      //
      use SoftDeletes;
      protected $connection = 'mysql_attendance';
      protected $table = 'attendees';
      protected $dates = ['deleted_at'];
      protected $hidden = ['pivot', 'created_at', 'updated_at', 'deleted_at'];

      public function assists()
      {
            return $this->hasMany(Assist::class);
      }

      public function disabilities()
      {
            return $this->belongsToMany(Disability::class)->withTimestamps();
      }

      public function paidcares()
      {
            return $this->belongsToMany(Paidcare::class)->withTimestamps();
      }

      public function populations()
      {
            return $this->belongsToMany(Population::class)->withTimestamps();
      }

      public function unpaidcares()
      {
            return $this->belongsToMany(Unpaidcare::class)->withTimestamps();
      }

      public function eps()
      {
          return $this->hasOne(Eps::class);
      }

      public function city()
      {
          return $this->hasOne(City::class);
      }

      public function country_birth()
      {
          return $this->hasOne(Country::class, 'id', 'country_birth_id');
      }
      
      public function state()
      {
          return $this->hasOne(State::class);
      }

      public function locality()
      {
          return $this->hasOne(Locality::class, 'Id_Localidad', 'locality_home_id');
      }

      public function upz()
      {
          return $this->hasOne(Upz::class);
      }
}
