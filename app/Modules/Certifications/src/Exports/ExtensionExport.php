<?php

namespace App\Modules\Certifications\src\Exports;

use App\Models\Security\DocumentType;
use App\Models\Security\User;
use App\Modules\Certifications\src\Models\CertificationAddition;
use App\Modules\Certifications\src\Models\CertificationContract;
use App\Modules\Certifications\src\Models\CertificationContractType;
use App\Modules\Certifications\src\Models\CertificationExtension;
use App\Modules\Certifications\src\Models\CertificationObligation;
use App\Traits\AppendHeaderToExcel;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Builder;
use Imtigger\LaravelJobStatus\JobStatus;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ExtensionExport implements FromQuery, WithTitle, WithEvents, WithHeadings, WithMapping, WithColumnFormatting, ShouldQueue, ShouldAutoSize
{
    use Exportable, AppendHeaderToExcel;

    /**
     * @var array
     */
    private $request;

    /**
     * @var int
     */
    private $rowNumb = 2;

    /**
     * @var User
     */
    private $user;

    public function __construct(array $request, $job, $user)
    {
        $this->user = $user;
        $this->request = $request;
        update_status_job($job, JobStatus::STATUS_EXECUTING, 'excel-certifications-module');
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return CertificationExtension::query()
            ->whereKey($this->request['extensions'])
            ->orderBy('created_at', 'desc');
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $sheet) {
                $this->setHeader($sheet->sheet, 'PRÓRROGAS - CERTIFICACION DE CONTRATOS', 'A1:H1', 'H');
            }
        ];
    }

    public function headings(): array
    {
        return [
            'ID PRÓRROGA',
            'ID DEL CONTRATO',
            'NÚMERO PRÓRROGA',
            'MESES',
            'DÍAS',
            'FECHA DE FINALIZACIÓN',
            'FECHA DE CREACIÓN',
            'FECHA DE MODIFICACIÓN',
        ];
    }

    public function title(): string
    {
        return 'PRÓRROGAS';
    }

    public function columnFormats(): array
    {
        return [
            'F' => NumberFormat::FORMAT_DATE_YYYYMMDD2.' h'.NumberFormat::FORMAT_DATE_TIME4,
            'G' => NumberFormat::FORMAT_DATE_YYYYMMDD2.' h'.NumberFormat::FORMAT_DATE_TIME4,
            'H' => NumberFormat::FORMAT_DATE_YYYYMMDD2.' h'.NumberFormat::FORMAT_DATE_TIME4,
        ];
    }

    public function map($row): array
    {
        return [
            'id'            => $row['Id'] ?? null,
            'contract_id'   => $row['Contrato_Id'] ?? null,
            'number'        => $row['Numero_Prorroga'] ?? null,
            'months'        => $row['Meses'] ?? null,
            'days'          => $row['Dias'] ?? null,
            'final_date'    => isset($row['Fecha_Fin']) ? Carbon::parse($row['Fecha_Fin'])->format('Y-m-d') : null,
            'created_at'    => isset($row['created_at']) ? date_time_to_excel(Carbon::parse($row['created_at'])) : null,
            'updated_at'    => isset($row['updated_at']) ? date_time_to_excel(Carbon::parse($row['updated_at'])) : null,
        ];
    }
}
