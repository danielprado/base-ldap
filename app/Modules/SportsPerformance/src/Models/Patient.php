<?php

namespace App\Modules\SportsPerformance\src\Models;

use App\Models\Security\Department;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;
use App\Modules\SportsPerformance\src\Models\Person;
use App\Modules\SportsPerformance\src\Models\City;

class Patient extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_sim_rendimiento_deportivo';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'pacientes';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'persona_id',
        'ciudad_expedicion_doc',
        'departamento_expedicion_doc',
    ];


    public function expedition_city()
    {
        return $this->hasOne(City::class, 'Id', 'ciudad_expedicion_doc');
    }
    
    public function expedition_department()
    {
        return $this->hasOne(Department::class, 'Id_Departamento', 'departamento_expedicion_doc');
    }
}