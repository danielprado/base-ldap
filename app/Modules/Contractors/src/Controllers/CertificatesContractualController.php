<?php

namespace App\Modules\Contractors\src\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Certifications\src\Models\CertificationContract;
use App\Modules\Certifications\src\Models\CertificationExpedition;
use App\Modules\Certifications\src\Models\ContractSupport;
use App\Modules\Contractors\src\Models\Certification;
use App\Modules\Contractors\src\Models\Contractor;
use App\Modules\Contractors\src\Request\CertificateContractualRequest;
use App\Modules\Contractors\src\Request\SupportCertificateContractualRequest;
use App\Modules\Contractors\src\Resources\CertificateContractualResource;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use App\Modules\Contractors\src\Models\Contract;
use App\Modules\Contractors\src\Models\ActivityReport;
use App\Modules\Contractors\src\Models\ActivityReportEvidence;
use Illuminate\Support\Facades\Log;
class CertificatesContractualController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function index(CertificateContractualRequest $request): JsonResponse
    {

        $certifications = CertificationContract::query()
            ->with('obligations')
            ->where('Tipo_Documento', $request->get('documentType'))
            ->where('Cedula', $request->get('document'))
            ->whereYear('Fecha_Firma', $request->get('year'))
            ->get();

        $contracts_secop = get_contracts_secop_api($request->get('document'))
            //->whereIn('estado_contrato', ['En ejecución', 'Modificado', 'cedido', 'terminado', 'Suspendido', 'Cerrado'])
            ->whereIn('estado_contrato', ['En ejecución', 'terminado', 'Suspendido', 'Cerrado'])
            ->filter(function ($contract) use ($request) {
                return date('Y', strtotime($contract['fecha_de_firma'])) == $request->get('year');
            });

        if ($certifications->isEmpty() || $certifications->count() < $contracts_secop->count()) {
            // Si hay contratos en Secop que no están registrados en CertificationContract, se registran
            $contractor = $this->getContractor($request->get('documentType'), $request->get('document'), $request->get('year'));
            foreach ($contracts_secop as $contract_secop) {
                $existingContract = $certifications->first(function ($contract) use ($contract_secop) {
                    return $contract->Numero_Contrato === extract_number_format_contract($contract_secop['referencia_del_contrato']) &&
                        Carbon::parse($contract->Fecha_Firma)->format('Y') === Carbon::parse($contract_secop['fecha_de_firma'])->format('Y');
                });

                if (!$existingContract && !is_null($contractor)) {
                    $this->createCertificationContract($contract_secop, $contractor);
                }
            }
        }

        // Realiza la consulta nuevamente después de agregar los contratos faltantes, si los hay
        if($this->validateDateContract($request) === true){
            $certifications = $this->obtenerContratosWithObligations($request);
        }else{
           $certifications = CertificationContract::query()
            ->with('obligations')
            ->where('Tipo_Documento', $request->get('documentType'))
            ->where('Cedula', $request->get('document'))
            ->whereYear('Fecha_Firma', $request->get('year'))
            ->get();
        }


        if ($certifications->isEmpty()) {
            return $this->error_response(__('contractor.not_found_certificate'), 422);
        }

        return $this->success_message([
            'type' => $certifications->count() > 1 ? 'Multiple' : 'Unique',
            'check' => $request->get('certificateType'),
            'contract' => CertificateContractualResource::collection($certifications),
        ]);
    }


    public function validateDateContract($request){
        $bandera = false;
        if(intval($request->get('year')) > 2023){
            $contractor = Contractor::where('document' , $request->get('document'))->first();
            if($contractor){
                $contract = Contract::where('contractor_id', $contractor->id)
                    //->where('start_date', '>', '2024-05-31')
                    ->whereYear('start_date', $request->get('year'))
                    ->first();
                if($contract){
                    $bandera = true;
                }
            }

        }else{
            $bandera = false;
        }

        return $bandera;
    }


    public function obtenerContratosWithObligations($request){
        $contractor = Contractor::where('document' , $request->get('document'))->first();
        if($contractor){
            $contracts = Contract::where('contractor_id', $contractor->id)
                //->where('start_date', '>', '2024-05-31')
                ->whereYear('start_date', $request->get('year'))
                ->get();
        }
        $certifications = CertificationContract::query()
            ->with('obligations')
            ->where('Tipo_Documento', $request->get('documentType'))
            ->where('Cedula', $request->get('document'))
            ->whereYear('Fecha_Firma', $request->get('year'))
            ->get();
        foreach ($certifications as $key => $certification) {
            foreach ($contracts as $key => $contract) {
                $numeroContrato = explode("-", $contract->contract);

                if (
                    (count($numeroContrato) == 5 && $numeroContrato[3] == $certification->Numero_Contrato ||
                     count($numeroContrato) < 5 && $numeroContrato[2] == $certification->Numero_Contrato) &&
                    $contract->contract_type_id !== Contract::CONTRACT_ADD_AND_EXT
                ) {

                    $obligaciones = $this->getObligationsNewTypeContracts($contract->id) ?? [];

                    if (!empty($obligaciones)) {
                        $certification->obligations = $obligaciones;
                    }
                }

            }
        }

        return $certifications;

    }

    public function getObligationsNewTypeContracts($contractId){
        $obligations = [];
        $activity = ActivityReport::where('contract_id' , $contractId)->first();
        if($activity){
            $activityEvidences = ActivityReportEvidence::where('activity_report_id' , $activity->id)->get();
            if($activityEvidences->count() > 0){
                foreach ($activityEvidences as $key => $evidence) {
                    $obligations[$evidence->sequence] = $evidence->obligation_text;

                }
            }
        }
        return $obligations;
    }

    private function createCertificationContract($contract_secop, $contractor): void
    {
        try {
            DB::connection('mysql_certificacion_contratos')->beginTransaction();
            $contract = new CertificationContract();
            $contract->Tipo_Documento = $contractor->document_type_id ?? null;
            $contract->Cedula = $contractor->document ?? $contract_secop['documento_proveedor'];
            $contract->Dv = 0;
            $contract->Nombre_Contratista = $contractor->fullname ?? toUpper($contract_secop['proveedor_adjudicado']);
            $contract->Numero_Contrato = extract_number_format_contract($contract_secop['referencia_del_contrato']) ?? null;
            $contract->Referencia = strpos($contract_secop['referencia_del_contrato'], 'CPS') !== false ? extract_reference_format_contract($contract_secop['referencia_del_contrato']) : null;
            $contract->Tipo_Contrato_Id = 2;
            $contract->Objeto = $contract_secop['objeto_del_contrato'] ?? null;
            $contract->Fecha_Firma = Carbon::parse($contract_secop['fecha_de_firma'])->format('Y-m-d');
            $contract->Fecha_Inicio = Carbon::parse($contract_secop['fecha_de_inicio_del_contrato'])->format('Y-m-d');
            $contract->Fecha_Fin = Carbon::parse($contract_secop['fecha_de_fin_del_contrato'])->format('Y-m-d');
            $contract->Meses_Duracion = $contract_secop['unidad_de_duracion'] == 'Meses' || $contract_secop['unidad_de_duracion'] == 'Mes(es)' ? (int)$contract_secop['duracion'] : 0;
            $contract->Dias_Duracion = $contract_secop['unidad_de_duracion'] == 'Dias' || $contract_secop['unidad_de_duracion'] == 'día(s)' ? (int)$contract_secop['duracion'] : 0;
            $contract->Valor_Inicial = (int)$contract_secop['valor_del_contrato'] ?? (int)$contract_secop['precio_base'];
            $contract->Valor_Mensual = $this->setMonthlyValue($contract_secop['unidad_de_duracion'], $contract_secop['duracion'], $contract_secop['valor_del_contrato']);
            $contract->year = Carbon::parse($contract_secop['fecha_de_firma'])->format('Y') ?? null;
            $contract->saveOrFail();
            DB::connection('mysql_certificacion_contratos')->commit();
        } catch (\Throwable $exception) {
            DB::connection('mysql_certificacion_contratos')->rollBack();
            $this->error_response(__('validation.handler.unexpected_failure'), Response::HTTP_UNPROCESSABLE_ENTITY, $exception->getMessage());
            return;
        }
    }

    private function setMonthlyValue($unit, $duration, $value)
    {
        if ($unit == 'Mes(es)' || $unit == 'Meses') {
            return $value / $duration;
        } elseif ($unit == 'día(s)' || $unit == 'Dias') {
            $months = ceil($duration / 30); //  rounds up in case of decimals
            return $value / $months;
        }
        return 0;
    }

    public function store(SupportCertificateContractualRequest $request): JsonResponse
    {
        $number = $request->get('contract_number') ? $request->get('contract_number') : '"Sin número de contrato"';
        $description = "Solicitud certificación de contrato No. " . $number . " del año "
            . $request->get('year') . " a nombre del contratista " . $request->get('name') . " identificado con " . $request->get('document') .
            " ó contactar a la persona por medio de teléfono: " . $request->get('phone') . ", gracias por su solicitud.";
        $support = new ContractSupport;
        $support->Tipo_Documento_Solicitante_Id = $request->get('documentType');
        $support->Nombre_Solicitante = toUpper($request->get('name'));
        $support->Documento_Solicitante = $request->get('document');
        $support->Correo_Solicitante = $request->get('email_confirmation');
        $support->Telefono_Solicitante = $request->get('phone');
        $support->Numero_Contrato = $request->get('contract_number');
        $support->Anio_Contrato = $request->get('year');
        $support->Descripcion_Solicitud = $request->get('description') ?? $description;
        $support->Estado = 1;
        $support->saveOrFail();

        return $this->success_message(
            __('contractor.valid_certificate', [
                'name' => $request->get('name')
            ]),
            Response::HTTP_CREATED
        );
    }

    public function generateCertificate($contract_id, $obligations_check): JsonResponse
    {
        try {
            DB::beginTransaction();
            // Get Contractor and Contract in databases
            $certificate = CertificationContract::query()->find($contract_id);
            $contract_year = date('Y', strtotime($certificate->Fecha_Firma));

            // Ger certificate file
            $http = new Client();
            //$response = $http->get("http://sim.test/CertificacionContratos/descargarContrato/$contract_id/$obligations_check");
            $response = $http->get("https://sim1.idrd.gov.co/SIM/CertificacionContratos/descargarContrato/$contract_id/$obligations_check");
            //$response = $http->get("http://localhost:8001/descargarContrato/$contract_id/$obligations_check");
            $document = isset($certificate->Cedula) ? $certificate->Cedula : Str::random(9);
            $file_name = 'CERTIFICADO_CONTRACTUAL_' . $document . '.pdf';
            $file = base64_encode($response->getBody()->getContents());

            // save certificate in databases
            $type = $obligations_check == 0 ? 'CCO' : 'COB';
            $certification = $this->saveInDataBase($certificate, $contract_year, $type);
            $token = isset($certification->token) ? $certification->token : $certification->type . '-' . Str::random(9);
            if (!isset($certification->token)) {
                $certification->token = $token;
            }
            $certification->increment('downloads');
            $certification->save();

            DB::commit();
            return $this->success_message([
                'file_name' => $file_name,
                'file' => 'data:application/pdf;base64,'.$file,
            ]);
        } catch (\Throwable $exception) {
            DB::rollBack();
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $exception->getMessage()
            );
        }
    }

    public function getContractor($documentType, $document, $year) {
        $contractor = Contractor::query()
            ->where('document_type_id', $documentType)
            ->where('document', $document)
            ->with(['contracts' => function ($query) use ($year) {
                $query->where('contract', 'like', "IDRD-CTO-_%$year%");
            }])
            ->first();

        return $contractor ?? null;
    }

    public function saveInDataBase(CertificationContract $certificate, $contract_year, $type)
    {
        $contract = format_contract($certificate->Numero_Contrato, $contract_year);
        $contractor = Contractor::query()
            ->with('contracts')
            ->where('document', $certificate->Cedula)
            ->first();

        $certification = CertificationExpedition::query()->where('Contrato_Id', $certificate->Id)
            ->latest()
            ->first();

        return Certification::query()->updateOrCreate(
            [
                'document' => isset($contractor) ? $contractor->document : $certificate->Cedula,
                'contract' => $contract,
                'type' => $type
            ],
            [
                'name' => isset($contractor) ? $contractor->full_name : $certificate->Nombre_Contratista,
                'expires_at' => $certification->Fecha_Fin,
                'year' => $contract_year,
                'contractor_id' => isset($contractor) ? $contractor->id : null,
                'token' => isset($certification->Verify_Code) ? $certification->Verify_Code : null
            ]
        );
    }

}


