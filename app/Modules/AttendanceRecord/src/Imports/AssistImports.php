<?php

namespace App\Modules\AttendanceRecord\src\Imports;

use App\Modules\AttendanceRecord\src\Models\Assist;
use App\Modules\AttendanceRecord\src\Models\Session;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToModel;

class AssistImports implements ToModel
{
    /**
     * @param Collection $collection
     */
    public function model(array $row)
    {

        $session = Session::with('recreopersonas', 'recreopersonas.persona', 'cronograma.punto', 'Cronograma.jornada', 'Cronograma.punto.localidad')
            ->where('id', '=', $row[3])
            ->first();

        dd($session->Objetivo_General);
        $data = [
            'session_id' => $row[3],
            'objetive_session' => $session->Objetivo_General,
            'scenary_session' => $session->Cronograma->punto->Escenario,
            'workingday_session' => $session->Cronograma->jornada->Nombre,
            'documentTeacher' => $session->recreopersonas->persona->Cedula,
            'nameTeacher' => $session->recreopersonas->persona->Primer_Nombre . ' ' . $session->recreopersonas->persona->Primer_Apellido,
            'hour_init' => $session->Inicio,
            'hour_end' => $session->Fin,
            'locality_service' => $session->Cronograma->punto->localidad->Localidad,
            'attention_modality' => $session->Cronograma->Virtual == 'No' || $session->Cronograma->Virtual == null ? 'PRESENCIAL' : 'VIRTUAL',
            'type_population' => $row[4],
            'type_service' => $row[5],
            'service_offered' => $row[6],
            'entity' => $row[7],
            "type_location_service" => "URBANA",
            "form_operation" => "PUNTO ANCLA",
            "apple" => "",
            "name_equipment" => "",
            "hour_job" => "CERO/NINGUNA",
            "has_attendant" => "NO",
            "name_attendant" => "",
            "type_document_attendant" => "",
            "number_document_attendant" => "",
            "sex_attendant" => "",
            "phone_attendant" => "",
            "email_attendant" => "",
            "relationship" => "",
            "group_etnic_attendant" => "NINGUNO",
            "observations" => "",

        ];


        $sessionJson = json_encode($data);
        $excelDate = $row[2];
        $unixTimestamp = ($excelDate - 25569) * 86400; // Convierte a UNIX timestamp
        $dateAssist = Carbon::createFromTimestamp($unixTimestamp + 1 * 60);
        $dateAssistFormatted = $dateAssist->format('Y-m-d');

        $assist = new Assist([
            'attendee_id' => $row[0],
            'module_id' => $row[1],
            'date_assist' => $dateAssistFormatted,
            'session_id' => $row[3],
            'data' => $sessionJson,
        ]);

        $assist->save();

        return $assist;
    }
}
