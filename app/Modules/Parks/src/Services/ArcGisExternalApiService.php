<?php

namespace App\Modules\Parks\src\Services;

use GuzzleHttp\Client;
use App\Modules\Parks\src\Services\Base\BaseExternalApiService;

class ArcGisExternalApiService extends BaseExternalApiService
{
    public function getSubcategories($category){
        $route = "/$category/MapServer";
        $response = parent::get($route);
        return $response['layers'] ?? [];
    }

    public function getData($category, $subcategory, $park_code){
        $route = "/$category/MapServer/$subcategory/query";
        $params = ['where' => "UB_REFERENCIA='$park_code'", 'outFields' => '*'];
        $response = parent::get($route, $params);
        return [
            'data' => $response['features'] ?? [],
            'headers' => $response['fields'] ?? []
        ];
    }

    public function getFields($category, $subcategory){
        $route = "/$category/MapServer/$subcategory";
        $response = parent::get($route);
        return [
            'fields' => $response['fields'] ?? [],
            'types' => $response['types'] ?? []
        ];
    }
}
