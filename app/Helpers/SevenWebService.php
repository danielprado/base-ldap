<?php

namespace App\Helpers;

use GuzzleHttp\Client;
use Illuminate\Http\Request;

class SevenWebService
{
    protected $client;
    protected $client_docker;

    protected $max;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => env('URL_BASE_WEBSERVICE_SEVEN', 'dorado.idrd.gov.co'),
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
                //'Authorization' => 'Bearer ' . $token,
            ],
        ]);

        $this->client_docker = new Client([
            'base_uri' => env('URL_DOCKER_SERVER'),
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ],
        ]);

        $contracts = $this->client_docker->post("/api/contractors-portal/get-user-seven-list");

        $contracts_response = json_decode($contracts->getBody()->getContents(), true);
        $this->max = collect($contracts_response['data'])->max('counter');
    }

    public function storeProvider($parameters)
    {
        $response = $this->client->post("/Seven/WebServicesREST/SIEWSSEC/api/RIeWssec/SyncPoPvdorI", [
            'json' => [
                'emp_codi' => $parameters['emp_codi'] ?? 2,
                'tip_codi' => $parameters['tip_codi'],
                'pvd_coda' => $parameters['pvd_coda'],
                'pvd_dive' => $parameters['pvd_dive'] ?? 0,
                'pvd_nomb' => $parameters['pvd_nomb'],
                'pvd_apel' => $parameters['pvd_apel'],
                'pvd_noco' => $parameters['pvd_noco'] ?? $parameters['pvd_nomb']. " " .$parameters['pvd_apel'],
                'pai_codi' => $parameters['pai_codi'] ?? 169,
                'dep_codi' => $parameters['dep_codi'] ?? 11,
                'mun_codi' => $parameters['mun_codi'] ?? 1,
                'tpr_codi' => $parameters['tpr_codi'] ?? 262,
                'pvd_clad' => $parameters['pvd_clad'] ?? "N",
                'arb_csuc' => $parameters['arb_csuc'] ?? "6",
                'cal_codi' => $parameters['cal_codi'] ?? 0,
                'coc_codi' => $parameters['coc_codi'] ?? 5,
                'cim_codi' => $parameters['cim_codi'] ?? 27,
                'act_codi' => $parameters['act_codi'],
                'mon_codi' => $parameters['mon_codi'] ?? 1,
                'cup_nume' => $parameters['cup_nume'],
                'cup_idba' => $parameters['cup_idba'] ?? "37",
                'ban_codi' => $parameters['ban_codi'],
                'sub_codi' => $parameters['sub_codi'],
                'cup_tipo' => $parameters['cup_tipo'],
                'pvr_riva' => $parameters['pvr_riva'] ?? "S",
                'pvr_auto' => $parameters['pvr_auto'] ?? "N",
                'pvr_reau' => $parameters['pvr_reau'] ?? ".",
                'cup_tdba' => $parameters['cup_tdba'] ?? "N",
                'cup_tcta' => $parameters['cup_tcta'] ?? "03",
                'cup_ib04' => $parameters['cup_ib04'] ?? "21",
                'cup_ib05' => $parameters['cup_ib05'] ?? "2",
                'cup_ib06' => $parameters['cup_ib06'] ?? "CA",
                'cup_ib07' => $parameters['cup_ib07'] ?? "01",
                'cup_ib08' => $parameters['cup_ib08'] ?? "02",
                'cup_ib09' => $parameters['cup_ib09'] ?? "32",
                'cup_ib10' => $parameters['cup_ib10'] ?? "3",
                'dep_dire' => $parameters['dep_dire'],
                'dep_ntel' => $parameters['dep_ntel'],
                'dep_mail' => $parameters['dep_mail'],
                'dep_codd' => $parameters['dep_codd'] ?? 1,
                'cup_numi' => $parameters['cup_numi'] ?? null,
                'cup_nomi' => $parameters['cup_nomi'] ?? null,
                'ban_coin' => $parameters['ban_coin'] ?? 0,
                'ban_dire' => $parameters['ban_dire'] ?? null,
                'cup_coab' => $parameters['cup_coab'] ?? null,
                'cup_coib' => $parameters['cup_coib'] ?? null,
                'cup_coas' => $parameters['cup_coas'] ?? "005600010",
                'cup_cobi' => $parameters['cup_cobi'] ?? null,
                'cup_clas' => $parameters['cup_clas'] ?? "S",
                'dep_apar' => $parameters['dep_apar'] ?? null,
                'dep_nfax' => $parameters['dep_nfax'] ?? null,
                'dep_cloc' => $parameters['dep_cloc'] ?? ".",
                'dep_nomb' => $parameters['dep_nomb'] ?? $parameters['pvd_nomb']. " " .$parameters['pvd_apel'],
                'cup_esta' => $parameters['cup_esta'] ?? "A",
                'pvd_audp' => $parameters['pvd_audp'] ?? "S",
                'pvd_moin' => $parameters['pvd_moin'] ?? "N",
                'pvd_mrpu' => $parameters['pvd_mrpu'] ?? "N",
                'pvd_serp' => $parameters['pvd_serp'] ?? "N",
                'ite_codi_cope' => $parameters['ite_codi_cope'] ?? 0,
            ],
        ]);

        $data = json_decode($response->getBody()->getContents(), true);

        if (!$data['IsSucessfull']) {
            webservice_logger(
                auth()->user()->id,
                auth()->user()->full_name,
                env('URL_BASE_WEBSERVICE_SEVEN')."/Seven/WebServicesREST/SIEWSSEC/api/RIeWssec/SyncPoPvdorI",
                '[ERROR] CREAR PROVEEDOR',
                $parameters,
                $response->getStatusCode(),
                $data
            );
        } else {
            webservice_logger(
                auth()->user()->id,
                auth()->user()->full_name,
                env('URL_BASE_WEBSERVICE_SEVEN')."/Seven/WebServicesREST/SIEWSSEC/api/RIeWssec/SyncPoPvdorI",
                'CREAR PROVEEDOR',
                $parameters,
                $response->getStatusCode(),
                $data
            );
        }

        return $data;
    }

    public function storeThirdParty($parameters)
    {
        $response = $this->client->post("/Seven/WebServicesREST/SIEWSSEC/api/RIeWssec/SyncGnTerceI", [
            'json' => [
                'emp_codi' => $parameters['emp_codi'] ?? 2,
                'tip_codi' => $parameters['tip_codi'],
                'ter_coda' => $parameters['ter_coda'] ?? $parameters['pvd_coda'],
                'ter_dive' => $parameters['ter_dive'] ?? 0,
                'ter_nomb' => $parameters['ter_nomb'] ?? $parameters['pvd_nomb'],
                'ter_apel' => $parameters['ter_apel'] ?? $parameters['pvd_apel'],
                'ter_noco' => $parameters['ter_noco'] ?? $parameters['pvd_noco'],
                'mod_codi' => 6,
                'pai_codi' => $parameters['pai_codi'] ?? 169,
                'dep_codi' => $parameters['dep_codi'] ?? 11,
                'mun_codi' => $parameters['mun_codi'] ?? 1,
                'ter_dire' => $parameters['ter_dire'] ?? $parameters['dep_dire'],
                'ter_ntel' => $parameters['ter_ntel'] ?? $parameters['dep_ntel'],
                'ter_mail' => $parameters['ter_mail'] ?? $parameters['dep_mail'],
                'ter_nfax' => $parameters['ter_nfax'] ?? null,
                'ter_audp' => 'N',
            ],
        ]);

        $data = json_decode($response->getBody()->getContents(), true);

        if (!$data['IsSucessfull']) {
            webservice_logger(
                auth()->user()->id,
                auth()->user()->full_name,
                env('URL_BASE_WEBSERVICE_SEVEN')."/Seven/WebServicesREST/SIEWSSEC/api/RIeWssec/SyncGnTerceI",
                '[ERROR] CREAR TERCERO',
                $parameters,
                $response->getStatusCode(),
                $data
            );
        } else {
            webservice_logger(
                auth()->user()->id,
                auth()->user()->full_name,
                env('URL_BASE_WEBSERVICE_SEVEN')."/Seven/WebServicesREST/SIEWSSEC/api/RIeWssec/SyncGnTerceI",
                'CREAR TERCERO',
                $parameters,
                $response->getStatusCode(),
                $data
            );
        }

        return $data;
    }

    public function storeContract($parameters)
    {
        $max = $this->max;
        $request = [
            "Emp_codi" => 2, // Código de la empresa
            "Top_codi" => 800, // Código del tipo de operación
            "Con_nume" => $max, // consecutivo automatico
            "Con_fech" => $parameters['start_date'], // Fecha de firma
            "Con_desc" => "Prestación de servicios", // (tipo_de_contrato)
            "Arb_csuc" => "1",
            "Pvd_coda" => $parameters['document'],
            "Dep_codd" => 1,
            "Con_objt" => $parameters['object'], // Objeto del contrato
            "Ter_inte" => "0", // Número de identificación del interventor
            "Ter_supe" => "0", // Número de identificación del supervisor
            "Ter_resp" => "0", // Número de identificación del abogado responsable
            "Con_fpub" => $parameters['start_date'], // Fecha de publicación
            "Con_fini" => $parameters['start_date'],
            "Con_fefi" => $parameters['final_date'],
            "Con_plaz" => (int) $parameters['duration'],
            "Con_upla" => $parameters['term'], // M, D
            "Con_ncon" => ltrim($parameters['contract'], '0'),
            "Con_clas" => "C",
            "Con_tdis" => "A",
            "Con_cont" => 0, // Consecutivo contrato base. Aplica para adiciones.
            "Con_vlri" => (int) $parameters['total'],
            "Con_tadi" => "C", // Tipo de adición. Valores posibles [A]dición, [P]rórroga, [M]odificación [C]contrato.
            "Con_esta" => "A",
            "Tpc_codi" => "1",
            "P_con_fvig" => $parameters['final_date'],
            "vDetalle" => [
                [
                    "Pro_codi" => "460044",
                    "Dco_cant" => 1,
                    "Dco_valo" => $parameters['total'],
                    "Bod_codi" => 105,
                    "Dco_dest" => "13",
                    "Dco_desc" => "SERVICIOS PROFESIONALES",
                    "Dco_hnom" => "N",
                    "Dco_edef" => "N",
                    "vDEntrega" => [
                        [
                            "Den_ndia" => 0,
                            "Den_cant" => 1
                        ]
                    ]
                ]
            ]
        ];
        return $this->sendCreateContract($request);
    }

    public function storeAddContract($parameters)
    {
        $max = $this->max;
        $request = [
            "Emp_codi" => 2, // Código de la empresa
            "Top_codi" => 800, // Código del tipo de operación
            "Con_nume" => $max, // consecutivo automatico
            "Con_fech" => $parameters['start_date'], // Fecha de firma
            "Con_desc" => "Prestación de servicios", // (tipo_de_contrato)
            "Arb_csuc" => "1",
            "Pvd_coda" => $parameters['document'],
            "Dep_codd" => 1,
            "Con_objt" => $parameters['object'], // Objeto del contrato
            "Ter_inte" => "0", // Número de identificación del interventor
            "Ter_supe" => "0", // Número de identificación del supervisor
            "Ter_resp" => "0", // Número de identificación del abogado responsable
            "Con_fpub" => $parameters['start_date'], // Fecha de publicación
            "Con_fini" => $parameters['start_date'], // Fecha de inicio de la adición
            "Con_fefi" => $parameters['final_date'], // Fecha de finalización de la adición
            "Con_plaz" => (int) $parameters['duration'], // duración en dias
            "Con_upla" => $parameters['term'], // M, D
            "Con_ncon" => ltrim($parameters['contract'], '0'),
            "Con_clas" => "A",
            "Con_tdis" => "A",
            "Con_cont" => (int) $parameters['consecutive'], // Consecutivo contrato base. Aplica para adiciones.
            "Con_vlri" => (int) $parameters['total'], // valor de la adición
            "Con_tadi" => "A", // Tipo de adición. Valores posibles [A]dición, [P]rórroga, [M]odificación [N]uevo.
            "Con_esta" => "A",
            "Tpc_codi" => "1",
            "P_con_fvig" => $parameters['final_date'],
            "vDetalle" => [
                [
                    "Pro_codi" => "460044",
                    "Dco_cant" => 1,
                    "Dco_valo" => (int) $parameters['total'], // valor de la adición
                    "Bod_codi" => 105,
                    "Dco_dest" => "13",
                    "Dco_desc" => "SERVICIOS PROFESIONALES",
                    "Dco_hnom" => "N",
                    "Dco_edef" => "N",
                    "vDEntrega" => [
                        [
                            "Den_ndia" => 0,
                            "Den_cant" => 1
                        ]
                    ]
                ]
            ]
        ];
        return $this->sendCreateContract($request);
    }

    public function showContract($parameters)
    {
        $http = $this->client;
        $response = $http->get("/Seven/WebServicesREST/RCTCONTR/api/RCtContr/ConsultarCtContr", [
            'query' => [
                'emp_codi' => 2,
                'ter_coda' => $parameters['document'],
                'tip_codi' => 2,
                'con_ncon' => $parameters['contract'],
                'con_anop' => $parameters['year']
            ],
        ]);

        $data = json_decode($response->getBody()->getContents(), true);

        if ($data['Retorno'] == 1) {
            webservice_logger(
                env('URL_BASE_WEBSERVICE_SEVEN')."/Seven/WebServicesREST/RCTCONTR/api/RCtContr/ConsultarCtContr",
                '[ERROR] CONSULTAR CONTRATO',
                $parameters,
                $data['Retorno'],
                $data
            );
        } else {
            webservice_logger(
                env('URL_BASE_WEBSERVICE_SEVEN')."/Seven/WebServicesREST/RCTCONTR/api/RCtContr/ConsultarCtContr",
                'CONSULTAR CONTRATO',
                $parameters,
                $data['Retorno'],
                $data
            );
        }

        return $data;
    }

    /**
     * @param array $request
     * @return array
     */
    public function sendCreateContract(array $request): array
    {
        $response = $this->client->post("/Seven/WebServicesREST/RCTCONTR/api/RCtContr/Insertar", [
            'json' => $request
        ]);

        $data = json_decode($response->getBody()->getContents(), true);

        if ($data['Retorno'] == 1) {
            webservice_logger(
                auth()->user()->id,
                auth()->user()->full_name,
                env('URL_BASE_WEBSERVICE_SEVEN') . "/Seven/WebServicesREST/RCTCONTR/api/RCtContr/Insertar",
                $request['Con_tdis'] = 'N' ? '[ERROR] INSERTAR CONTRATO' : '[ERROR] INSERTAR ADICIÓN CONTRATO',
                $request,
                $data['Retorno'],
                $data
            );
        } else {
            webservice_logger(
                auth()->user()->id,
                auth()->user()->full_name,
                env('URL_BASE_WEBSERVICE_SEVEN') . "/Seven/WebServicesREST/RCTCONTR/api/RCtContr/Insertar",
                $request['Con_tdis'] = 'N' ? 'INSERTAR CONTRATO' : 'INSERTAR ADICIÓN CONTRATO',
                $request,
                $data['Retorno'],
                $data
            );
        }

        return $data;
    }

    public function sendUpdateContract()
    {

        $request = [
            'emp_codi' => 2,
            'ter_coda' => "1073518105",
            'tip_codi' => 2, // 2 para el caso de C.C.
            'con_ncon' => "2136", // solo el número de contrato ej: 125 sin ceros...
            'con_anop' => 2023,
            'prs_codi' => 0,
            'pro_codi' => '460044',
            'dco_valo' => 36972000,
            'ter_cont' => $parameters['pai_codi'] ?? 169,
            'ter_inte' => $parameters['dep_codi'] ?? 11,
            'ter_supe' => $parameters['mun_codi'] ?? 1,
            'ter_abog' => $parameters['ter_dire'] ?? $parameters['dep_dire'],
            'con_fini' => $parameters['ter_ntel'] ?? $parameters['dep_ntel'],
            'con_fefi' => $parameters['ter_mail'] ?? $parameters['dep_mail'],
        ];

        $response = $this->client->post("/Seven/WebServicesREST/RCTCONTR/api/RCtContr/modificarContrato", [
            'json' => $request
        ]);

        $data = json_decode($response->getBody()->getContents(), true);

        if ($data['Retorno'] == 1) {
            webservice_logger(
                auth()->user()->id,
                auth()->user()->full_name,
                env('URL_BASE_WEBSERVICE_SEVEN') . "/Seven/WebServicesREST/RCTCONTR/api/RCtContr/Insertar",
                $request['Con_tdis'] = 'N' ? '[ERROR] INSERTAR CONTRATO' : '[ERROR] INSERTAR ADICIÓN CONTRATO',
                $request,
                $data['Retorno'],
                $data
            );
        } else {
            webservice_logger(
                auth()->user()->id,
                auth()->user()->full_name,
                env('URL_BASE_WEBSERVICE_SEVEN') . "/Seven/WebServicesREST/RCTCONTR/api/RCtContr/Insertar",
                $request['Con_tdis'] = 'N' ? 'INSERTAR CONTRATO' : 'INSERTAR ADICIÓN CONTRATO',
                $request,
                $data['Retorno'],
                $data
            );
        }

        return $data;
    }
}
