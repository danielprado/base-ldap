<?php


namespace App\Modules\CitizenPortal\src\Resources;


use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class RegulationResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        =>  isset($this->id) ? (int) $this->id : null,
            'name'      =>  isset($this->name) ? (string) $this->name : null,
            'file'   =>  isset($this->file) ? (string) $this->file : null,
            'link'   =>  isset($this->file) ? $this->url($this->file) : null,
            'created_at'    =>  isset($this->created_at) ? $this->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'    =>  isset($this->updated_at) ? $this->updated_at->format('Y-m-d H:i:s') : null,
            'audits'       => []
        ];
    }

    public function url($file)
    {
        return "https://portalciudadano.idrd.gov.co/sim-ldap/public/storage/regulations/{$file}";
    }

    public static function headers()
    {
        return [
            [
                'align' => "right",
                'text' => "#",
                'value'  =>  "id",
                'sortable' => false
            ],
            [
                'align' => "left",
                'text' => Str::ucfirst(__('citizen.validations.name')),
                'value'  =>  "name",
                'sortable' => false
            ],
            [
                'align' => "left",
                'text' => Str::ucfirst(__('citizen.validations.file')),
                'value'  =>  "file",
                'sortable' => false
            ],
            [
                'align' => "right",
                'text' => Str::ucfirst(__('citizen.validations.created_at')),
                'value'  =>  "created_at",
                'sortable' => false
            ],
            [
                'align' => "right",
                'text' => Str::ucfirst(__('citizen.validations.updated_at')),
                'value'  =>  "updated_at",
                'sortable' => false
            ],
            [
                'align' => "right",
                'text' => Str::ucfirst(__('citizen.validations.actions')),
                'value'  =>  "actions",
                'sortable' => false
            ],
        ];
    }
}
