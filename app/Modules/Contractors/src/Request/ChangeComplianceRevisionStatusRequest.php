<?php

namespace App\Modules\Contractors\src\Request;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ChangeComplianceRevisionStatusRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contract_year' => 'required|numeric|digits:4|date_format:Y',
            'contract_number' => 'required|numeric|digits:4',
            'status' => ['required', Rule::in(['APPROVED', 'REJECTED'])],
            'observation' => [ Rule::requiredIf($this->input('status') === 'REJECTED'), 'string', 'min:3' ]
        ];
    }
}
