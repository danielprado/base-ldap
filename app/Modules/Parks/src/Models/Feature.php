<?php

namespace App\Modules\Parks\src\Models;

use Illuminate\Database\Eloquent\Model;

class Feature extends Model
{
    //
    protected $connection = 'mysql_parks';
    protected $table = 'Destacados';
    protected $primaryKey = 'id';
    protected $fillable = ['nombre', 'Icon'];

    public function parks()
    {
        return $this->belongsToMany(Park::class, 'parks_destacados', 'destacado_id', 'park_id');
    }
}
