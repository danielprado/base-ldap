<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBudgetChainsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('budget_chains', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('description_chain')->comment('descripción cadena presupuesta');
            $table->enum('type_chain', ['AGREEMENTS', 'COMPENSATORY_FUND', 'ECONOMIC_USE', 'PUBLIC_SHOWS', 'SANCTIONS_AND_FINES', 'TRANSFER', 'VALORIZATION'])->comment('tipo de cadena presupuesta');
            $table->tinyInteger('reserve')->comment('Es reserva');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('budget_chains');
    }
}
