<?php


namespace App\Modules\Contractors\src\Controllers;

use App\Helpers\SevenWebService;
use App\Http\Controllers\Controller;
use App\Models\Security\User;
use App\Modules\Contractors\src\Constants\Roles;
use App\Modules\Contractors\src\Jobs\ConfirmContractor;
use App\Modules\Contractors\src\Jobs\ConfirmUpdateStatusContractor;
use App\Modules\Contractors\src\Models\Contract;
use App\Modules\Contractors\src\Models\Contractor;
use App\Modules\Contractors\src\Models\DocumentInit;
use App\Modules\Contractors\src\Models\ActivityReport;
use App\Modules\Contractors\src\Models\Fees;
use App\Modules\Contractors\src\Models\ContractType;
use App\Modules\Contractors\src\Models\PlanPayment;
use App\Models\Security\Area;
use App\Models\Security\Subarea;
use App\Modules\Contractors\src\Notifications\ArlUpdateNotification;
use App\Modules\Contractors\src\Request\ChangeContractSubdirectorateRequest;
use App\Modules\Contractors\src\Request\DocumentInitRequest;
use App\Modules\Contractors\src\Request\StoreLawyerContractRequest;
use App\Modules\Contractors\src\Request\UpdateContractRequest;
use App\Modules\Contractors\src\Resources\ContractArlResource;
use App\Modules\Contractors\src\Resources\ContractResource;
use App\Modules\Contractors\src\Resources\ContractWithPlaneResource;
use App\Modules\Contractors\src\Resources\DocumentInitResource;
use App\Modules\Contractors\src\Resources\ContractSessionResource;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class ContractController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Contractor $contractor
     * @return JsonResponse
     */
    public function index(Contractor $contractor)
    {
        return $this->success_response(
            ContractResource::collection($contractor->contracts),
            Response::HTTP_OK,
            [
                'headers'   =>  ContractResource::headers(),
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreLawyerContractRequest $request
     * @param Contractor $contractor
     * @return JsonResponse
     */
    public function store(StoreLawyerContractRequest $request, Contractor $contractor)
    {
        try {
            DB::connection('mysql_contractors')->beginTransaction();
            $contract_number = str_pad($request->get('contract'), 4, '0', STR_PAD_LEFT);
            $contract_suffix = "{$contract_number}-{$request->get('contract_year')}";
            $request->get('type') !== null
                ? $contract = toUpper("IDRD-{$request->get('type')}-{$contract_number}-{$request->get('contract_year')}")
                : $contract = toUpper("IDRD-CTO-{$contract_number}-{$request->get('contract_year')}");

            $contractAssignment = Contract::query()
                ->where("contract", "LIKE", "%{$contract_suffix}")
                ->where("contract_type_id", Contract::CONTRACT_ASSIGNMENT)
                ->first();

            if ($contractAssignment) {
                $updateContractDates = $contractAssignment;
            } else {
                $updateContractDates = Contract::query()
                    ->where("contract", "LIKE", "%{$contract_suffix}")
                    ->where("contract_type_id", Contract::NEW_CONTRACT)
                    ->first();
            }
            /*
             * Si es una cesión de contrato actualizamos la fecha final del contrato con respecto
             * a la fecha de inicio de la cesión
             */
            if (Contract::CONTRACT_ASSIGNMENT == $request->get('contract_type_id')) {
                if (isset($updateContractDates->id)) {
                    $updateContractDates->final_date = $request->get('start_date');
                    $updateContractDates->save();
                }
            }
            /**
             * Si es una suspensión de contrato calculamos la nueva fecha de finalización del contrato
             */
            if (Contract::CONTRACT_SUSPENSION == $request->get('contract_type_id')) {
                if (isset($updateContractDates->id)) {
                    $start = Carbon::parse($request->get('start_suspension_date'));
                    $final  = Carbon::parse($request->get('final_suspension_date'));
                    //$updateContractDates->final_date = $updateContractDates->final_date->addDays($start->diffInDays($final));
                    $updateContractDates->save();
                }
            }
            /**
             * Si es una adición de contrato calculamos el valor total del contrato
             */
            if (Contract::CONTRACT_ADD == $request->get('contract_type_id')) {
                if (isset($updateContractDates->id)) {
                    $updateContractDates->total = $updateContractDates->total + $request->get('total_addition');
                    //$updateContractDates->final_date = $request->get('final_date');
                    $updateContractDates->save();
                }

            }
            /**
             * Si es una prórroga de contrato calculamos el valor total del contrato
             */
            if (Contract::CONTRACT_EXTENSION == $request->get('contract_type_id')) {
                if (isset($updateContractDates->id)) {
                    //$updateContractDates->duration = $updateContractDates->duration + $request->get('duration');
                    //$updateContractDates->duration_days = $updateContractDates->duration_days + $request->get('duration_days');
                    $updateContractDates->final_date = $request->get('final_date');
                    $updateContractDates->save();
                }
            }
            /**
             * Si es una adición y prórroga de contrato calculamos el valor total del contrato y duración del contrato
             */
            if (Contract::CONTRACT_ADD_AND_EXT == $request->get('contract_type_id')) {
                if (isset($updateContractDates->id)) {
                    $updateContractDates->total = $updateContractDates->total + $request->get('total_addition');
                    //$updateContractDates->duration = $updateContractDates->duration + $request->get('duration');
                    //$updateContractDates->duration_days = $updateContractDates->duration_days + $request->get('duration_days');
                    $updateContractDates->total_addition = $request->get('total_addition');
                    $updateContractDates->final_date = $request->get('final_date');
                    $updateContractDates->save();
                }
            }
            /**
             * Si es una terminación anticipada de contrato calculamos la fecha final del contrato
             */
            if (Contract::EARLY_TERMINATION == $request->get('contract_type_id')) {
                if (isset($updateContractDates->id)) {
                    $updateContractDates->final_date = $request->get('final_date');
                    $updateContractDates->save();
                }
            }
            /**
             * Si es un contrato nuevo verifica que no exista ese contrato con el mismo contratista
             */
            if (Contract::NEW_CONTRACT == $request->get('contract_type_id')) {
                $exists = Contract::query()
                    ->where([
                        ['contract_type_id', '=', $request->get('contract_type_id')],
                        ['contractor_id', '=', $contractor->id],
                        ['contract', '=', $contract]
                    ])->get();
                if (!$exists->isEmpty()) {
                    return $this->error_response(
                        __('validation.unique', ['attribute' => $contract])
                    );
                }
            }
            $newContract = $contractor->contracts()
                ->save(new Contract(
                    array_merge(
                        $request->validated(),
                        ['contract' => $contract],
                        ['lawyer_id' => auth()->user()->id],
                        ['position' => isset($updateContractDates) ? $updateContractDates->position : null],
                        //['day' => isset($updateContractDates) ? $updateContractDates->day : null],
                        ['risk' => isset($updateContractDates) ? $updateContractDates->risk : null],
                        ['subdirectorate_id' => isset($updateContractDates) ? $updateContractDates->subdirectorate_id : null],
                        ['dependency_id' => isset($updateContractDates) ? $updateContractDates->dependency_id : null],
                        ['object_contract' => isset($updateContractDates) ? $updateContractDates->object_contract : null],
                        ['supervisor_email' => isset($updateContractDates) ? $updateContractDates->supervisor_email : null]
                    )
                ));
            if ($contractor->isNotifiableAdmin($request->get('contract_type_id'))) {
                $this->dispatch(new ConfirmUpdateStatusContractor($contractor));
                Notification::send(
                    User::whereIs(Roles::ROLE_ARL, Roles::ROLE_THIRD_PARTY)->get(),
                    new ArlUpdateNotification($contractor, $newContract)
                );
            }
            if ($contractor->isNotifiable($request->get('contract_type_id'))) {
                $contractor->modifiable = now()->format('Y-m-d H:i:s');
                $contractor->saveOrFail();
                $this->dispatch(new ConfirmContractor($contractor));
            }
            DB::connection('mysql_contractors')->commit();
            return $this->success_message(__('validation.handler.success'), Response::HTTP_CREATED);
        } catch (\Throwable $e) {
            DB::connection('mysql_contractors')->rollBack();
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateContractRequest $request
     * @param Contractor $contractor
     * @return JsonResponse
     */
    public function update(UpdateContractRequest $request, Contractor $contractor)
    {
        try {
            DB::connection('mysql_contractors')->beginTransaction();
            $contract = $contractor->contracts()
                ->where('id', $request->get('contract_id'))
                ->first();
            $contract->update($request->validated());
            DB::connection('mysql_contractors')->commit();
            return $this->success_message(__('validation.handler.updated'));
        } catch (\Throwable $e) {
            DB::connection('mysql_contractors')->rollBack();
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }
    public function arlRisk(Request $request)
    {
        $contracts = array();
        foreach ($request->get('listContracts') as $contract) {
            $contract = toUpper("IDRD-CTO-{$contract}");
            $contracts[] = $contract;
        }

        $data = Contract::query()
            ->select('contract', 'risk')
            ->whereIn('contract', $contracts)
            ->get();

        return  $this->success_response(
            ContractArlResource::collection($data)
        );
    }

    /**
     * Update the specified subdirectorate and area in contract.
     *
     * @param ChangeContractSubdirectorateRequest $request
     * @return JsonResponse
     */
    public function changeAreaAndSubdirectorate(ChangeContractSubdirectorateRequest $request)
    {
        $validated = $request->validated();

        try {
            Contract::query()
                ->where('contract', $validated['contract'])
                ->update([
                    'subdirectorate_id' => $validated['subdirectorate_id'],
                    'dependency_id' => $validated['area_id']
                ]);
        } catch (\Throwable $th) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $th->getMessage()
            );
        }
    }

    public function getContractorForDocumentInit(Request $request)
    {
        try {
            $user = auth('api')->user();
            $currentDate = Carbon::now();
            $contracts = [];

            $contracts = $this->getContractsForSupervisor($currentDate, $user);
            // if ($user->isAn(Roles::ROLE_SUPERVISOR)) {
            //     $contracts = $this->getContractsForSupervisor($currentDate, $user);
            // } else if ($user->isAn(Roles::ROLE_SUPERVISOR_SUPPORT, Roles::ROLE_SUPERVISOR_TECHNICAL_SUPPORT)) {
            //     $contracts = $this->getContractsForSupportSupervisor($currentDate, $request);
            // }

            return $this->success_response(
                DocumentInitResource::collection($contracts)
            );
        } catch (\Throwable $th) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $th->getMessage()
            );
        }
    }

    private function getContractsForSupervisor($currentDate, $user)
    {
        $user = auth('api')->user()->load('areas');
        $area_ids = $user->areas()->pluck('areas.id');
        if ($area_ids->isEmpty()) return collect([]);

        return Contract::with(['contractor', 'document_init', 'dependency', 'contract_type'])
            ->whereIn('dependency_id', $area_ids)
            ->whereNotIn('contract_type_id', [Contract::CONTRACT_ADD])
            ->orderBy('start_date', 'ASC')
            ->get();
    }

    private function getContractsForSupportSupervisor($currentDate, $request)
    {
        $query = Contract::with(['contractor', 'document_init', 'dependency'])
            ->whereDate('final_date', '>', $currentDate);

        $area_id = null;
        $area_req_id = $request->query('areaId');
        if (isset($area_req_id)) {
            $area_id = $area_req_id;
        }
        if (isset($area_id) && Area::where('id', $area_id)->exists()) {
            $query->where('dependency_id', $area_id);
        }
        return $query->get();
    }

    public function getDocumentInitByContract(Request $request, $contract_id)
    {
        try {

            $contract = Contract::with(['contractor', 'document_init'])->where('id', $contract_id)->first();
            return $this->success_response(
                new DocumentInitResource($contract),
                Response::HTTP_OK
            );
        } catch (\Throwable $th) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $th->getMessage()
            );
        }
    }

    public function createDocumentInit(DocumentInitRequest $request)
    {
        try {
            $contract = Contract::with('contractor')->find($request->contract_id);
            $numberArray = array_reverse(explode('-', $contract->contract));
            $numberContract = $numberArray[1] . '-' . $numberArray[0];

            if ($request->get('create_seven_contract')){
                try {
                    $sevenWebService = new SevenWebService();
                    $duration_info = $this->calculateDuration($request);
                    $seven_contract = [
                        'type' => 'PRESTACIÓN DE SERVICIOS',
                        'document' => $contract->contractor->document,
                        'object' => $request->get('object_contract'),
                        'start_date' => $request->get('date_init'),
                        'final_date' => $request->get('date_end'),
                        'duration' => $duration_info['duration'],
                        'term' => $duration_info['term'],
                        'contract' => $numberArray[1],
                        'value' => $contract->total,
                        'total' => $contract->total
                    ];
                    $response = null;
                    if ($contract->contract_type_id === Contract::CONTRACT_ADD_AND_EXT){
                        $original_contract = Contract::where('contract_type_id', Contract::NEW_CONTRACT)->where('contract', 'like', '%'.$numberContract.'%')->first();
                        $seven_contract['consecutive'] = $original_contract->consecutive ?? null;
                        $response = $sevenWebService->storeAddContract($seven_contract);
                        if ($response['Retorno'] == 0){
                            $conCont = $response['ObjTransaction']['Con_Cont'] ?? null;
                            if (!is_null($conCont)) {
                                $contract->update(['consecutive' => (string) $conCont]);
                            }
                        }
                    } else {
                        $response = $sevenWebService->storeContract($seven_contract);
                        if ($response['Retorno'] == 0){
                            $conCont = $response['ObjTransaction']['Con_Cont'] ?? null;
                            if (!is_null($conCont)) {
                                $contract->update(['consecutive' => (string) $conCont]);
                            }
                        }
                    }

                    if ($response['Retorno'] == 1) {
                        return $this->error_response(
                            'No se pudo crear el contrato en este momento, por favor intente más tarde.',
                            Response::HTTP_UNPROCESSABLE_ENTITY,
                            $response['TxtError']
                        );
                    }
                } catch (Exception $exception) {
                    return $this->error_response(
                        'No podemos realizar la consulta en este momento, por favor intente más tarde.',
                        422,
                        $exception->getMessage()
                    );
                }
            }

            $contract->update([
                'start_date' => $request->date_init,
                'final_date' => $request->date_end,
                'object_contract' => $request->object_contract,
                'duration' => $request->months_contract,
                'duration_days' => $request->days_contract,
                'subdirectorate_id' => $request->subdirectorate_id,
                'dependency_id' => $request->dependency_id,
            ]);
            $documentInit = new DocumentInit;
            $documentInit->value_month = $request->value_month;
            $documentInit->months_contract = $request->months_contract;
            $documentInit->days_contract = $request->days_contract;
            $documentInit->contract_id = $request->contract_id;
            $documentInit->handles_sessions = $request->handles_sessions;
            $documentInit->total_sessions = $request->total_sessions;
            $documentInit->save();

            if ($contract->contract_type_id == Contract::CONTRACT_SUSPENSION) {
                $contract->update([
                    'start_suspension_date' => $request->start_suspension_date,
                    'final_suspension_date' => $request->final_suspension_date
                ]);

                $daysDifference = $this->getDaysDifference($request->start_suspension_date, $request->final_suspension_date) + 1;

                $latest_plan_by_contract = PlanPayment::where('contract', $numberContract)->latest()->first();
                if (isset($latest_plan_by_contract)) {
                    $original_contract = Contract::find($latest_plan_by_contract->contract_id);
                    if (isset($original_contract)) {
                        $newFinalDate = Carbon::parse($original_contract->final_date);
                        $remainingDays = $daysDifference;
                        while ($remainingDays > 0) {
                            $daysInCurrentMonth = 30; // Asumimos 30 días por mes
                            $currentDayOfMonth = ($newFinalDate->day > 30) ? 30 : $newFinalDate->day; // Limitamos a un máximo de 30 días por mes
                            $daysLeftInMonth = $daysInCurrentMonth - $currentDayOfMonth;

                            if ($remainingDays > $daysLeftInMonth) {
                                // Si los días restantes son mayores que los días que quedan en el mes actual
                                $remainingDays -= $daysLeftInMonth + 1; // Restamos los días restantes en el mes actual
                                $newFinalDate->addMonthNoOverflow(); // Avanzamos al siguiente mes sin considerar la longitud real del mes
                                $newFinalDate->day = 1; // Colocamos el primer día del mes siguiente
                            } else {
                                // Si los días restantes caben en el mes actual
                                $newFinalDate->addDays($remainingDays); // Añadimos los días restantes
                                $remainingDays = 0; // Ya no quedan días
                            }
                        }
                        $original_contract->update([
                            'final_date' => $newFinalDate->format('Y-m-d')
                        ]);
                    }
                }
            }

            if (in_array($contract->contract_type_id, [Contract::EARLY_TERMINATION, Contract::CONTRACT_ADD_AND_EXT, Contract::CONTRACT_EXTENSION])) {
                $assignment_contract = Contract::where('contract_type_id', Contract::CONTRACT_ASSIGNMENT)->where('contract', 'like', '%'.$numberContract.'%')->first();
                if (isset($assignment_contract)){
                    $assignment_contract->update([
                        'duration' => $assignment_contract->duration + (int)$request->months_contract,
                        'duration_days' => $assignment_contract->duration_days + (int)$request->days_contract,
                        'final_date' => $request->date_end
                    ]);
                } else {
                    $original_contract = Contract::where('contract_type_id', Contract::NEW_CONTRACT)->where('contract', 'like', '%'.$numberContract.'%')->first();
                    if (isset($original_contract)){
                        $original_contract->update([
                            'duration' => $original_contract->duration + (int)$request->months_contract,
                            'duration_days' => $original_contract->duration_days + (int)$request->days_contract,
                            'final_date' => $request->date_end
                        ]);
                    }
                }
            }

            $currentReport = ActivityReport::where('contract_id', $request->contract_id)->latest('id')->first();
            if ($currentReport) {
                $new_report_number = $this->calculateReportNumber($request->date_init, $request->date_end);
                $currentReport->update(['report_number' => $new_report_number]);
            }

            $http = new Client([
                'base_uri' => env('URL_PLAN_PAYMENT'),
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                ],
                'verify' => false,
            ]);
            $response = $http->post(env('URL_PLAN_PAYMENT') . "/payment-plans/create-projection", [
                'json' => [
                    "supervisorId" => auth()->user()->id,
                    "contractId" => $request->contract_id,
                    "contractNumber" => $numberContract,
                    "isCyclePathGuardian" => $request->handles_sessions
                ]
            ]);
            $responseData = json_decode($response->getBody()->getContents(), true);
            $status = $response->getStatusCode();
            if ($status != 200 && $status != 201) {
                return $this->error_response(
                    __('validation.handler.unexpected_failure'),
                    Response::HTTP_UNPROCESSABLE_ENTITY,
                    $responseData
                );
            }

            return $this->success_message(
                __('validation.handler.success'),
                Response::HTTP_OK,
                Response::HTTP_OK,
                [
                    'id'     => $request->contract_id,
                    'review' => DocumentInit::STATUS_REVIEW,
                    'date_init' => substr($contract->start_date, 0, 10),
                    'date_end' => substr($contract->final_date, 0, 10),
                    'months_contract' => $contract->duration,
                    'days_contract' => $documentInit->days_contract,
                    'value_month' => $documentInit->value_month,
                    'value_total' => $contract->total
                ]
            );
        } catch (\Throwable $th) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $th->getMessage()
            );
        }
    }

    public function calculateDuration($request)
    {
        $monthsContract = $request->get('months_contract') ?? 0;
        $daysContract = $request->get('days_contract') ?? 0;
        if ((int)$daysContract === 0) {
            return [
                "duration" => (string)$monthsContract,
                "term" => "M"
            ];
        } else {
            $duration = ((int)$monthsContract * 30) + (int)$daysContract;
            return [
                "duration" => (string)$duration,
                "term" => "D"
            ];
        }
    }

    public function getContractWithPlane()
    {
        try {
            $user = auth('api')->user();
            $currentDate = Carbon::now();
            $contracts = Contract::with(['contractor', 'document_init'])
                ->whereDate('final_date', '>', $currentDate)
                ->whereHas('document_init')
                ->whereExists(function ($query) use ($user) {
                    $query->select(DB::raw(1))
                        ->from(env('DB_LDAP_DATABASE') . '.areas')
                        ->whereColumn('contracts.dependency_id', 'areas.id');
                    if (isset($user)){
                        $query->where('contracts.dependency_id', $user->area_id);
                    }
                })
                ->get();
            return  $this->success_response(
                ContractWithPlaneResource::collection($contracts)
            );
        } catch (\Throwable $th) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $th->getMessage()
            );
        }
    }

    public function getPlanePayment(Request $request)
    {
        try {
            $http = new Client([
                'base_uri' => env('URL_PLAN_PAYMENT'),
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                ],
                'verify' => false,
            ]);
            $response = $http->get(env('URL_PLAN_PAYMENT') . "/payment-plans/get-by-contract/" . $request->query('contract'));
            $responseData = json_decode($response->getBody()->getContents(), true);
            return $this->success_message(
                __('validation.handler.success'),
                Response::HTTP_OK,
                Response::HTTP_OK,
                $responseData
            );
        } catch (\Throwable $th) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $th->getMessage()
            );
        }
    }

    public function getObject(Contract $contract): JsonResponse
    {
        $contractor = $contract->contractor()->first();
        $contracts_secop = get_active_contracts_secop_api($contractor->document);
        $contract_secop = $contracts_secop->firstWhere('referencia_del_contrato', $contract->contract);
        if (!$contract_secop) {
            return $this->error_response(
                "No se encontró el objeto del contrato en SECOP",
                Response::HTTP_NOT_FOUND
            );
        }

        $object = $contract_secop['objeto_del_contrato'] ?? null;
        return $this->success_message(
            $object,
            Response::HTTP_OK,
            Response::HTTP_OK
        );
    }

    public function calculateReportNumber($start_date, $final_date)
    {
        $start_date = Carbon::parse($start_date)->startOfMonth();
        $final_date = Carbon::parse($final_date)->endOfMonth();

        $diff_in_months = $start_date->diffInMonths($final_date);
        $current_diff = $start_date->diffInMonths(Carbon::now());

        if ($current_diff >= 0 && $current_diff <= $diff_in_months) {
            $report_number = $current_diff + 1;
            return $report_number;
        } else {
            return -1;
        }
    }

    public function getFees()
    {
        try {
            $fees = Fees::where('status', Fees::ACTIVE)->get();
            return $this->success_message(
                __('validation.handler.success'),
                Response::HTTP_OK,
                Response::HTTP_OK,
                $fees
            );
        } catch (\Throwable $th) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $th->getMessage()
            );
        }
    }

    public function getSessionContracts(Request $request)
    {
        try {
            $session_contracts = Contract::with([
                'contractor',
                'document_init',
                'plans'
            ])
            ->whereHas('document_init', function ($query) {
                $query->where('handles_sessions', true);
            })
            ->whereDate('final_date', '>', Carbon::now())
            ->paginate($this->per_page);

            return $this->success_response(
                ContractSessionResource::collection($session_contracts),
                Response::HTTP_OK,
                [
                    'headers' => ContractSessionResource::headers()
                ]
            );
        } catch (\Throwable $exception) {
            return $this->error_response(
                'No podemos realizar la consulta en este momento, por favor intente más tarde.',
                422,
                $exception->getMessage()
            );
        }
    }

    public function getDaysDifference($start_suspension_date,$final_suspension_date ){
        $startSuspensionDate = Carbon::parse($start_suspension_date);
        $finalSuspensionDate = Carbon::parse($final_suspension_date);

        // Si el día de la fecha inicial o final es 31, tratarlo como 30
        if ($startSuspensionDate->day == 31) {
            $startSuspensionDate->day = 30;
        }

        if ($finalSuspensionDate->day == 31) {
            $finalSuspensionDate->day = 30;
        }

        // Cálculo personalizado asumiendo que todos los meses tienen 30 días
        $startDay = $startSuspensionDate->day;
        $startMonth = $startSuspensionDate->month;
        $startYear = $startSuspensionDate->year;

        $endDay = $finalSuspensionDate->day;
        $endMonth = $finalSuspensionDate->month;
        $endYear = $finalSuspensionDate->year;

        // Calcular la diferencia en años, meses y días
        $yearDiff = $endYear - $startYear;
        $monthDiff = $endMonth - $startMonth + ($yearDiff * 12);
        $dayDiff = $endDay - $startDay;

        // Convertir todo a días, asumiendo 30 días por mes
        $daysDifference = ($monthDiff * 30) + $dayDiff;

        if ($daysDifference < 0) {
            // Asegurar que la diferencia sea positiva si hay un desbordamiento en los días
            $daysDifference += 30;
            $monthDiff -= 1;
        }

        return $daysDifference;
    }
}
