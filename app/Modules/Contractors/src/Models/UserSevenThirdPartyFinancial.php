<?php

namespace App\Modules\Contractors\src\Models;

use Illuminate\Database\Eloquent\Model;

class UserSevenThirdPartyFinancial extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'oracle_ws';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'seven.po_cupro';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'pvd_codi';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pvd_codi',
        'cup_nume',
        'ban_codi',
        'sub_codi',
        'mon_codi',
        'cup_tipo',
        'cup_esta',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'emp_codi' => 'int',
        'ban_codi' => 'int',
        'mon_codi' => 'int',
    ];
}
