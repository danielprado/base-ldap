<?php

namespace App\Channels;

use Illuminate\Notifications\Notification;
use SendGrid\Mail\Mail;

class SendGridChannel
{
    /**
     * Send the given notification.
     *
     * @param mixed $notifiable
     * @param \Illuminate\Notifications\Notification $notification
     * @return void
     */
    public function send($notifiable, Notification $notification)
    {
        $template = $notification->toMail($notifiable);
        $mailer =  new Mail();
        $mailer->setFrom(
            env('MAIL_FROM_ADDRESS', 'mails@idrd.gov.co'),
            env('MAIL_FROM_NAME', 'Notificaciones IDRD')
        );
        $mailer->setSubject($template->subject);
        $mailer->addTo($notifiable->getEmailForPasswordReset());
        $mailer->addContent("text/html", (string) $template->render());
        $sendgrid = new \SendGrid(env('SENDGRID_API_KEY'));
        $sendgrid->send($mailer);
    }
}
