<?php

namespace App\Modules\Contractors\src\Mail;

use App\Modules\Contractors\src\Models\ActivityReport;
use App\Modules\Contractors\src\Models\Compliance;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ActivityReportCorrectedMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var ActivityReport
     */
    private $activityReport;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(ActivityReport $activityReport) {
        $this->activityReport = $activityReport;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $contract = $this->activityReport->contract_number;

        $protocol = 'https';

        $path = config('app.env') != 'production'
            ? "/portal-contratista-dev"
            : "/";

        $subdomain = config('app.env') != 'production' ? "sim" : "portalcontratista";
        $domain = "{$subdomain}.idrd.gov.co";

        if (config('app.env') === 'local') {
            $protocol = 'http';
            $domain = 'localhost:3000';
        }

        $url = "{$protocol}://{$domain}{$path}";

        return $this->view('mail.mail')
            ->subject('Informe de Actividades - Corregido')
            ->with([
                'slogan'    => 'Notificación Informe de Actividades IDRD',
                'header'    => 'IDRD',
                'title'     => 'Informe de Actividades - Portal Contratista',
                'content'   => "¡El informe de actividades generado para el contrato {$contract} ha sido para corregido! Puede realizar la revisión dando click en el siguiente botón.",
                'details'   => "
                    <div style='text-align: center;'>
                        <p>Fecha de Actualización: {$this->activityReport->updated_at}</p>
                    </div>
                ",
                'btn_text'  => 'Revisar',
                'url'  => "{$url}/es/tramites-pagos",
                'year'      => now()->year
            ]);
    }
}
