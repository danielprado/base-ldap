<?php


namespace App\Modules\Contractors\src\Jobs;

use App\Modules\Contractors\src\Models\Contractor;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class CheckThirdParty implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Contractor
     */
    private $contractor;

    /**
     * Create a new job instance.
     *
     * @param Contractor $contractor
     */
    public function __construct(Contractor $contractor)
    {
        $this->contractor = $contractor;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        try {
            $data = $this->getUserThirdParty();
            $third_party = collect($data['third_party'])->first() ?? null;
            $third_party_financial = collect($data['third_party_financial'])->first() ?? null;
            $provider = collect($data['provider'])->first() ?? null;
            $provider['detail'] = collect($data['detail'])->first() ?? [];

            if ($this->contractor->bank_account()->first() && $third_party && $third_party_financial && $provider) {
                $document_type = $this->contractor->document_type()->first();
                $person_type = $provider['pvd_clas_text'] ?? null;
                $mail = isset($third_party['ter_mail']) ? strtolower($third_party['ter_mail']) : null;
                $address = preg_replace('/\s+/', ' ', $this->contractor['address']);
                $address_third = preg_replace('/\s+/', ' ', $third_party['ter_dire']);
                $bank = $this->contractor->bank_account()->first();
                $account_type = $bank->account_type_id == 2 ? 'A' : 'C';
                $economic_activity = $this->contractor->bank_account->economic_activity()->first();

                $conditions = [
                    $provider['tip_codi_text'] != $document_type['name'],
                    $provider['pvd_codi'] != $this->contractor['document'],
                    !str_contains(format_string_name($this->contractor['name']), format_string_name($provider['pvd_nomb'])) || !str_contains(format_string_name($provider['pvd_nomb']), format_string_name($this->contractor['name'])),
                    !str_contains(format_string_name($this->contractor['surname']), format_string_name($provider['pvr_apel'])) || !str_contains(format_string_name($provider['pvr_apel']), format_string_name($this->contractor['surname'])),
                    $person_type != 'NATURAL',
                    $provider['pvr_estate_text'] != 'APROBADO',
                    $third_party['ter_celu'] != $this->contractor['phone'] || $third_party['ter_ntel'] != $this->contractor['phone'],
                    $mail != $this->contractor['email'],
                    !str_contains($address, $address_third) || !str_contains($address_third, $address),
                    $third_party['ter_acti_text'] != 'APROBADO',
                    $third_party_financial['cup_esta_text'] != 'ACTIVO',
                    $provider['detail']['act_codi'] != $economic_activity->code,
                    $third_party_financial['ban_codi'] != $bank->bank_id,
                    $third_party_financial['cup_tipo'] != $account_type,
                    !str_contains($bank->number, preg_replace('/^\s+|\s+$|\s+(?=\s)/', '', $third_party_financial['cup_nume'])),
                ];

                $this->contractor->third_party = in_array(false, $conditions) ? null : 1;
                $this->contractor->saveOrFail();
            }
        } catch (\Throwable $e) {
            $this->fail($e->getMessage());
        }
    }

    public function getUserThirdParty(): array
    {
        $document = $this->contractor->document;
        $http = new Client([
            'base_uri' => env('URL_DOCKER_SERVER'),
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ],
        ]);
        $request = ['document' => $document];

        $responses = [
            $http->post("/api/contractors-portal/user-seven-third-party", [
                'json' => $request,
            ]),
            $http->post("/api/contractors-portal/user-seven-third-party-financial", [
                'json' => $request,
            ]),
            $http->post("/api/contractors-portal/user-seven-provider", [
                'json' => $request,
            ]),
            $http->post("/api/contractors-portal/user-seven-provider-detail", [
                'json' => $request,
            ]),
        ];

        foreach ($responses as $response) {
            $responseData = json_decode($response->getBody()->getContents(), true);
            $data[] = $responseData['data'] ?? null;
        }

        return [
            'third_party' => $data[0] ?? null,
            'third_party_financial' => $data[1] ?? null,
            'provider' => $data[2] ?? null,
            'detail' => $data[3] ?? null,
        ];
    }
}
