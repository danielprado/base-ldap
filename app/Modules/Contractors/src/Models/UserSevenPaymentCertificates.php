<?php

namespace App\Modules\Contractors\src\Models;

use App\Modules\Contractors\src\Models\Oracle\GnTerce;
use App\Modules\Contractors\src\Models\Oracle\GnTipdo;
use App\Modules\Contractors\src\Models\Oracle\PgDmpre;
use App\Modules\Contractors\src\Models\Oracle\PgMpres;
use Illuminate\Database\Eloquent\Model;

class UserSevenPaymentCertificates extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'oracle_ws';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'seven.ts_mteso';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'mte_nume';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['mte_fech'];

}
