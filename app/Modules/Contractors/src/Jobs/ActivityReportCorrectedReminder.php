<?php

namespace App\Modules\Contractors\src\Jobs;

use App\Modules\Contractors\src\Mail\ActivityReportCorrectedMail;
use App\Modules\Contractors\src\Mail\ComplianceCorrectedMail;
use App\Modules\Contractors\src\Mail\ComplianceRejectedMail;
use App\Modules\Contractors\src\Models\ActivityReport;
use App\Modules\Contractors\src\Models\Compliance;
use App\Modules\Contractors\src\Models\Contractor;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use SendGrid\Mail\Mail;

class ActivityReportCorrectedReminder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var ActivityReport
     */
    private $activityReport;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(ActivityReport $activityReport) {
        $this->activityReport = $activityReport;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mail $mailer)
    {
        $email = config('app.env') == 'local'
            ? env('SAMPLE_EMAIL', 'brayan.aldana@idrd.gov.co')
            : (isset($this->activityReport->reviewer) ? $this->activityReport->reviewer : null);

        if (isset($email) && filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $mailer->setFrom(
                env('MAIL_CONTRACTS_FROM', 'portal.contratista@idrd.gov.co'),
                env('MAIL_CONTRACTS_FROM_NAME', 'Portal Contratista')
            );
            $mailer->setSubject('Informe de Actividades - Corregido');
            $mailer->addTo($email);

            $render = (new ActivityReportCorrectedMail($this->activityReport))
                ->render();

            $mailer->addContent("text/html", $render);
            $sendgrid = new \SendGrid(config('mail.sendgrid.api'));

            try {
                $response = $sendgrid->send($mailer);
                sendgird_logger(
                    $email,
                    "Informe de Actividades - Corregido",
                    $render,
                    [],
                    $response->statusCode(),
                    $response->body()
                );
            } catch (\Exception $e) {
                sendgird_logger(
                    $email,
                    "[EXCEPTION] Informe de Actividades - Corregido",
                    $render,
                    [],
                    $e->getCode(),
                    $e->getMessage()
                );
            }
        }
    }
}
