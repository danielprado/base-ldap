<?php

namespace App\Modules\Certifications\src\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ExpeditionResource extends JsonResource
{
    public function __construct($resource)
    {
        parent::__construct($resource);
    }
    public function toArray($request): array
    {
        return [
            'id' => isset($this->Id) ? (int) $this->Id : null,
            'contract_id' => isset($this->Contrato_Id) ? (int) $this->Contrato_Id : null,
            'name' => $this->Nombre_Expedicion ?? null,
            'count' => isset($this->Conteo) ? (int) $this->Conteo : null,
            'code' => $this->Verify_Code ?? null,
            'created_at' => isset($this->created_at) ? $this->created_at->format('Y-m-d') : null,
            'updated_at' => isset($this->updated_at) ? $this->updated_at->format('Y-m-d') : null,
        ];
    }
}
