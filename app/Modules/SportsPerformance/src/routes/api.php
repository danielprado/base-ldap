<?php

use Illuminate\Support\Facades\Route;
use App\Modules\SportsPerformance\src\Controllers\UserController;
use App\Modules\SportsPerformance\src\Controllers\PatientController;

Route::prefix('doc-form')->group(function () {
    Route::middleware('auth:api')->group(function () {
        Route::resource('patients', PatientController::class, [
            'only' => ['store', 'update'],
            'parameters' => ['patients' => 'patient'],
        ]);
        Route::prefix('patients')->group(function () {
            Route::post('edit', [PatientController::class, 'edit']);
            Route::get('searchByPerson', [PatientController::class, 'searchByPerson']);
        });
    });
});
