<?php

namespace App\Modules\Contractors\src\Resources;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserSevenPaymentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'emp_codi'  => $this->emp_codi ?? null,
            'ter_codi'  => $this->ter_codi ?? null,
            'mte_nume'  => $this->mte_nume ?? null,
            'mte_fech'  => isset($this->mte_fech) ? Carbon::parse($this->mte_fech)->format('Y-m-d') : null,
            'mpr_ndos'  => $this->mpr_ndos ?? null,
            'dmp_valo'  => $this->dmp_valo ?? null,
            'ter_noco'  => $this->ter_noco ?? null,
            'tip_abre'  => $this->tip_abre ?? null,
        ];
    }

}
