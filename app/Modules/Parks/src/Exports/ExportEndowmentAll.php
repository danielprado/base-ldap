<?php

namespace App\Modules\Parks\src\Exports;

use App\Modules\Parks\src\Constants\Utils;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ExportEndowmentAll implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{

    use Exportable;

    protected $allContents;
    protected $currentPage;

    public function __construct($allContents = null, $currentPage = 1)
    {
        $this->allContents = $allContents;
        $this->currentPage = $currentPage;
    }

    public function collection()
    {
        $chunkSize = Utils::REGISTERS;
        $offset = ($this->currentPage - 1) * $chunkSize;
        return $this->allContents->slice($offset, $chunkSize);
    }

    public function map($allContents): array
    {
        return [
            $allContents['UB_REFERENCIA'] ??  '',
            $allContents['NOMBRE_PARQ'] ??  '',
            $allContents['ID_DOTACION'] ??  '',
            $allContents['NOM_DOTACION'] ??  '',
            $allContents['TIPO_DOTACION'] ??  '',
            $allContents['DESCR'] ??  '',
            $allContents['COOR_X'] ??  '',
            $allContents['COOR_Y'] ??  '',
            $allContents['RIESGO_1'] ??  '',
            $allContents['RIESGO_2'] ??  '',
            $allContents['RIESGO_3'] ??  '',
            $allContents['RIESGO_4'] ??  '',
            $allContents['MAT_PISO'] ??  '',
            $allContents['AREA_M2'] ??  '',
            $allContents['AREA_JUEGO'] ??  '',
            $allContents['CAPACIDAD'] ??  '',
            $allContents['ILUMINACION'] ??  '',
            $allContents['CUBIERTO'] ??  '',
            $allContents['CERRAMIENTO'] ??  '',
            $allContents['TIPO_CERRAMIENTO_LAT'] ??  '',
            $allContents['TIPO_CERRAMIENTO_FRO'] ??  '',
            $allContents['CAMERINO'] ??  '',
            $allContents['SISTEMA_RIEGO'] ??  '',
            $allContents['USO_INCLUYENTE'] ??  '',
            $this->getDate($allContents['FECHA_CREACION'])  ??  '',
            $this->getDate($allContents['FECHA_ACTUALIZACION'])  ??  '',
            $allContents['NRO_PISO'] ??  '',
            $allContents['ESTADO_DIAG'] ??  '',
        ];
    }

    public function headings(): array
    {
        return [
            'CODIGO PARQUE',
            'NOMBRE PARQUE',
            'ID DOTACION',
            'NOMBRE DOTACION',
            'TIPO',
            'DESCRIPCION',
            'COORDENADA X',
            'COORDENADA Y',
            'RIESGO 1',
            'RIESGO 2',
            'RIESGO 3',
            'RIESGO 4',
            'MATERIAL PISO',
            'AREA M2',
            'AREA JUEGO',
            'CAPACIDAD',
            'ILUMINACION',
            'CUBIERTO',
            'CERRAMIENTO',
            'TIPO_CERRAMIENTO_LAT',
            'TIPO_CERRAMIENTO_FRO',
            'CAMERINO',
            'SISTEMA_RIEGO',
            'USO INCLUYENTE',
            'FECHA CREACION',
            'FECHA ACTUALIZACION',
            'N DE PISOS',
            'ESTADO DIAGNOSTICO',
        ];
    }

    private function getDate($date): string{
        $timestamp = $date / 1000;
        $fecha = Carbon::createFromTimestamp($timestamp);
        $fechaFormateada = $fecha->format('Y-m-d');
        return $fechaFormateada;
    }
}
