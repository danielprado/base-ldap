<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateContractorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contractors', function (Blueprint $table) {
            $table->unsignedInteger('gender_id')->after('sex_id')->nullable()->comment('Identidad de género de la persona');
            $table->integer('orientation_id')->after('gender_id')->nullable()->comment('Orientación sexual de la persona');
            $table->unsignedInteger('population_id')->after('orientation_id')->nullable()->comment('Grupo poblacional de la persona');
            $table->integer('ethnic_id')->after('population_id')->nullable()->comment('Grupo étnico de la persona');
            $table->integer('disability_id')->after('ethnic_id')->nullable()->comment('Discapacidad de la persona');
            $table->unsignedBigInteger('bank_id')->after('bank')->nullable()->comment('Datos bancarios');
            $table->string('person_type')->after('disability_id')->nullable()->comment('Tipo de persona');
            $table->string('marital_status')->after('person_type')->nullable()->comment('Estado civil');
            $sim_database = env('DB_SIM_DATABASE');
            $contractors_database = env('DB_CONTRACTORS_DATABASE');
            $table->foreign('gender_id')
                ->references('id')
                ->on("{$sim_database}.identidad_de_genero")
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
            $table->foreign('orientation_id')
                ->references('id')
                ->on("{$sim_database}.lgbti")
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
            $table->foreign('population_id')
                ->references('id')
                ->on("{$sim_database}.social_poblacional")
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
            $table->foreign('ethnic_id')
                ->references('Id_Etnia')
                ->on("{$sim_database}.etnia")
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
            $table->foreign('disability_id')
                ->references('id')
                ->on("{$sim_database}.discapacidad")
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
            $table->foreign('bank_id')
                ->references('id')
                ->on("{$contractors_database}.contractor_bank")
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contractors', function (Blueprint $table) {
            $table->dropForeign([
                'contractors_gender_id_foreign',
                'contractors_orientation_id_foreign',
                'contractors_population_id_foreign',
                'contractors_ethnic_id_foreign',
                'contractors_disability_id_foreign',
                'contractors_bank_id_foreign',
            ]);
            $table->dropColumn([
                'gender_id',
                'orientation_id',
                'population_id',
                'ethnic_id',
                'disability_id',
                'person_type',
                'marital_status',
                'bank_id'
            ]);
        });
    }
}
