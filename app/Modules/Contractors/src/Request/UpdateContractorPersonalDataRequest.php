<?php


namespace App\Modules\Contractors\src\Request;


use App\Models\Security\Area;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateContractorPersonalDataRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'sex_id' =>  'required|numeric|exists:mysql_sim.genero,Id_Genero',
            'gender_id' => 'required|numeric|exists:mysql_sim.identidad_de_genero,id',
            'orientation_id' =>  'required|numeric|exists:mysql_sim.lgbti,id',
            'population_id' =>  'required|numeric|exists:mysql_sim.social_poblacional,id',
            'ethnic_id' =>  'required|numeric|exists:mysql_sim.etnia,Id_Etnia',
            'disability_id' =>  'required|numeric|exists:mysql_sim.discapacidad,id',
            'marital_status' => 'required|string',
            'blood_type_id' =>  'required|numeric|exists:mysql_sim.grupo_sanguineo,Id_GrupoSanguineo',
            'person_type' => 'required|string',
            'email' =>  'required|email',
            'institutional_email'   =>  'nullable|email',
            'phone'   =>  'required|numeric',
            'residence_country_id'  =>  'required|numeric|exists:mysql_ldap.countries,id',
            'residence_state_id'    =>  'required|numeric|exists:mysql_ldap.states,id',
            'residence_city_id' =>  'required|numeric|exists:mysql_ldap.cities,id',
            'locality_id'   =>  'required|numeric|sometimes',
            'upz_id'    =>  'required|numeric|sometimes',
            'neighborhood_id'   =>  'required|numeric|sometimes',
            'neighborhood'  =>  'string|nullable|min:3|max:191',
            'address'  =>  'string|required|min:3|max:191',
            'contact_name'  =>  'string|required|min:3|max:90',
            'contact_phone'   =>  'required|numeric',
            'contact_relationship'  =>  'string|required|min:3|max:90',
            'other_relationship'  =>  'string|min:3|max:90|required_if:contact_relationship,OTRO',
            'contract_id' =>  'required|numeric|exists:mysql_contractors.contracts,id',
            'subdirectorate_id'    =>  'required|numeric|exists:mysql_ldap.subdirectorates,id',
            'dependency_id'    =>  'required|numeric|exists:mysql_ldap.areas,id',
            'subarea_id'    =>  'nullable|numeric|exists:mysql_ldap.subareas,id',
            'other_dependency_subdirectorate'    =>  [
                Rule::requiredIf(function () {
                    $area = Area::query()->where('id', $this->get('dependency_id'))->first();
                    return isset($area->name) && $area->name === 'OTRO';
                }),
                'nullable',
                'string',
            ],
            'position'  =>  'required|string',
            'risk'  =>  'required|numeric|between:1,5',
            'supervisor_email'  =>  'nullable|email',
            'day'   =>  'required|array',
            'photo'   =>  'required|image|mimes:jpeg,png,jpg',
            'eps_id'    =>  'required|numeric|exists:mysql_ldap.eps,id',
            'afp_id'    =>  'required|numeric|exists:mysql_ldap.afp,id',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'sex_id' =>  'sexo',
            'gender_id' => 'identidad de género',
            'orientation_id' => 'orientacion sexual',
            'population_id' => 'grupo poblacional',
            'ethnic_id' => 'grupo etnico',
            'disability_id' => 'discapacidad',
            'person_type' => 'tipo de persona',
            'marital_status' => 'estado civil',
            'email' =>  'correo personal',
            'institutional_email'   =>  'correo institucional',
            'phone'   =>  'teléfono de contácto',
            'blood_type_id'   =>  'Grupo sanguíneo',
            'residence_country_id'  =>  'país de residencia',
            'residence_state_id'    =>  'departamaneto de residencia',
            'residence_city_id' =>  'ciudad de residencia',
            'locality_id'   =>  'localidad',
            'upz_id'    =>  'upz',
            'neighborhood_id'   =>  'barrio',
            'neighborhood'  =>  'otro nombre del bario',
            'address'  =>  'dirección',
            'contact_name'  =>  'Nombre de contacto de emergencia',
            'contact_phone'  =>  'Teléfono de contacto de emergencia',
            'contact_relationship'  =>  'Parentesco del contacto de emergencia',
            'other_relationship'  =>  'Otro tipo de parentesco',
            'subarea_id'    =>  'área',
            'eps_id'    =>  'eps',
            'afp_id'    =>  'fondo de pensiones',
        ];
    }

    /**
     * Get the validator instance for the request.
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getValidatorInstance(){
        $validator = parent::getValidatorInstance();

        $validator->sometimes('locality_id', 'exists:mysql_parks.localidad,Id_Localidad', function($input)
        {
            return $input->locality_id != 9999;
        });

        $validator->sometimes('upz_id', 'exists:mysql_parks.upz,Id_Upz', function($input)
        {
            return $input->upz_id != 9999;
        });

        $validator->sometimes('neighborhood_id', 'exists:mysql_parks.Barrios,IdBarrio', function($input)
        {
            return $input->neighborhood_id != 9999;
        });

        return $validator;
    }
}
