<?php

namespace App\Modules\Contractors\src\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ActivityEconomicResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'    => isset($this->id) ?  (int) $this->id : null,
            'code'  => $this->code ?? null,
            'name'  => toUpper($this->name) ?? null,
            'combined'  => toUpper("$this->code - $this->name"),
        ];
    }
}
