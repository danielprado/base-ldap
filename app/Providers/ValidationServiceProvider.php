<?php

namespace App\Providers;

use App\Validators\ProhibitedIfNotValidator;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class ValidationServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extendImplicit('prohibited_if_not', ProhibitedIfNotValidator::class);
        Validator::replacer('prohibited_if_not', function ($message, $attribute, $rule, $parameters) {
            $params = array_reduce($parameters, function($str, $param) {
                return $str . "$param ";
            }, '');
        
            return str_replace(':values', trim($params), $message);
        });
    }
}
