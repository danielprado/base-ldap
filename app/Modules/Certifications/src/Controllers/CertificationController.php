<?php

namespace App\Modules\Certifications\src\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Contractors\src\Models\Certification;
use Illuminate\Http\JsonResponse;

class CertificationController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function count(): JsonResponse
    {
        $year = now()->year;

        return $this->success_message([
            'contractual' => Certification::query()
                ->where('type', '=', 'CCO')
                ->orWhere('type', '=', 'COB')
                ->whereYear('updated_at', $year)
                ->count(),
            'tax' => Certification::query()
                ->where('type', '=', 'TRB')
                ->whereYear('updated_at', $year)
                ->count(),
            'systems' => Certification::query()
                ->where('type', '=', 'SYS')
                ->whereYear('updated_at', $year)
                ->count(),
            'warehouse' => Certification::query()
                ->where('type', '=', 'ALM')
                ->whereYear('updated_at', $year)
                ->count()
        ]);
    }

}
