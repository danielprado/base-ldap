<?php

namespace App\Modules\Contractors\src\Models;

use App\Models\Security\Afp;
use App\Models\Security\CityLDAP;
use App\Models\Security\CountryLDAP;
use App\Models\Security\Disability;
use App\Models\Security\DocumentType;
use App\Models\Security\Eps;
use App\Models\Security\EthnicGroup;
use App\Models\Security\GenderIdentity;
use App\Models\Security\PopulationGroup;
use App\Models\Security\Sex;
use App\Models\Security\SexualOrientation;
use App\Models\Security\StateLDAP;
use App\Models\Security\User;
use App\Modules\Parks\src\Models\Location;
use App\Modules\Parks\src\Models\Neighborhood;
use App\Modules\Parks\src\Models\Upz;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use OwenIt\Auditing\Contracts\Auditable;

class ActivityReport extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable, Notifiable;

    const STATUS_DRAFT = 'DRAFT';
    const STATUS_PENDING = 'PENDING';
    const STATUS_APPROVED = 'APPROVED';
    const STATUS_REJECTED = 'REJECTED';

    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'mysql_contractors';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'activity_reports';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'contractor_id',
        'contract_id',
        'contract_number',
        'report_number',
        'report_date',
        'virtual_expedient_number',
        'activity_period',
        'payment_number',
        'payment_date',
        'eps_name',
        'eps_value',
        'afp_name',
        'afp_value',
        'risks_name',
        'risks_value',
        'contributions_total',
        'state',
        'supervisor_id',
        'observations',
        'is_new_contractor',
        'attachments_path',
        'social_security_attachment_path',
        'reviewer',
        'reviewed',
        'name_reviewer',
        'reviewed_technical',
        'name_reviewer_technical',
        'peaceandsafe_path',
        'peaceandsafe_warehouse_path',
        'sessionsCompleted',
        'reviewed_plans'
    ];

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [
        'contractor_id',
        'contract_id',
        'contract_number',
        'report_number',
        'report_date',
        'virtual_expedient_number',
        'activity_period',
        'payment_number',
        'payment_date',
        'eps_name',
        'eps_value',
        'afp_name',
        'afp_value',
        'risks_name',
        'risks_value',
        'contributions_total',
        'state',
        'supervisor_signature_path',
        'peaceandsafe_path',
        'peaceandsafe_warehouse_path',
        'sessionsCompleted',
        'reviewed_plans'
    ];

    /*
     * ---------------------------------------------------------
     * Eloquent Relations
     * ---------------------------------------------------------
    */
    public function evidences()
    {
        return $this->hasMany(ActivityReportEvidence::class)->oldest();
    }

    public function contractor()
    {
        return $this->hasOne(Contractor::class, 'id', 'contractor_id');
    }

    public function contract()
    {
        return $this->belongsTo(Contract::class, 'id', 'contract_id');
    }

    public function contributions()
    {
        return $this->hasMany(ContractorContribution::class, 'report_id', 'id')->oldest();
    }
}
