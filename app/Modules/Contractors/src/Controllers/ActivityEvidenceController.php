<?php


namespace App\Modules\Contractors\src\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\Contractors\src\Constants\GlobalQuery;
use App\Modules\Contractors\src\Helpers\compliance\emails\Emails;
use App\Models\Security\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Throwable;
use App\Modules\Contractors\src\Models\ActivityReport;
use App\Modules\Contractors\src\Models\ActivityReportEvidence;
use App\Modules\Contractors\src\Request\ActivityEvidenceRequest;
use App\Modules\Contractors\src\Resources\ActivityEvidenceResource;
use Illuminate\Support\Facades\Log;

class ActivityEvidenceController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $evidences = ActivityReportEvidence::where('activity_report_id', $request->activity_report_id)->orderBy('sequence', 'asc')->get();
        return $this->success_response(
            ActivityEvidenceResource::collection($evidences),
            Response::HTTP_OK
        );
    }

    public function show(ActivityReportEvidence $evidence)
    {
        return $this->success_response(
            new ActivityEvidenceResource($evidence),
            Response::HTTP_OK
        );
    }

    public function store(ActivityEvidenceRequest $request)
    {
        try {
            DB::connection('mysql_contractors')->beginTransaction();     
            $form = ActivityReportEvidence::create($request->validated());
            $report = ActivityReport::find($request->activity_report_id);
            if ($request->has('support') && $report){
                $file = $request->file('support')[0];
                $ext = $file->getClientOriginalExtension();
                $now = now()->format('YmdHis');
                $file_path = "EVIDENCE_{$report->report_date}_{$request->sequence}_{$now}.$ext";
                $file->storeAs('activity-reports', $report->contract_number . '/' . $file_path, [ 'disk' => 'local' ]);
                $form->update(['file_path' => $file_path]);
            }
            DB::connection('mysql_contractors')->commit();

            return $this->success_message(
                __('validation.handler.success'),
                Response::HTTP_CREATED,
                Response::HTTP_CREATED,
                ['id' => $form->id]
            );
        } catch (Throwable $e) {
            DB::connection('mysql_contractors')->rollBack();
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }

    public function update(ActivityEvidenceRequest $request, ActivityReportEvidence $evidence)
    {
        try {
            DB::connection('mysql_contractors')->beginTransaction();
            $evidence->update($request->validated());
            $report = ActivityReport::find($request->activity_report_id);

            if ($request->has('support') && $report){
                $original_path = $report->contract_number . '/' . $evidence->getOriginal('file_path');
                if ($evidence->getOriginal('file_path') && Storage::disk('activity-reports')->exists($original_path)) {
                    Storage::disk('activity-reports')->delete($original_path);
                }
                $file = $request->file('support')[0];
                $ext = $file->getClientOriginalExtension();
                $now = now()->format('YmdHis');
                $file_path = "EVIDENCE_{$report->report_date}_{$evidence->sequence}_{$now}.$ext";
                $file->storeAs('activity-reports', $report->contract_number . '/' . $file_path, [ 'disk' => 'local' ]);
                $evidence->update(['file_path' => $file_path]);
            }
            
            DB::connection('mysql_contractors')->commit();

            return $this->success_message(
                __('validation.handler.success'),
                Response::HTTP_OK,
                Response::HTTP_OK,
                [
                    'evidence' => $evidence
                ]
            );
        } catch (\Exception $e) {
            DB::connection('mysql_contractors')->rollBack();
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }

    public function destroy(ActivityReportEvidence $evidence)
    {
        $report = ActivityReport::find($evidence->activity_report_id);
        if ($report){
            $original_path = $report->contract_number . '/' . $evidence->file_path;
            if ($evidence->file_path && Storage::disk('activity-reports')->exists($original_path)) {
                Storage::disk('activity-reports')->delete($original_path);
            }
        }
        $evidence->delete();

        return $this->success_message(
            __('validation.handler.deleted'),
            Response::HTTP_OK,
            Response::HTTP_NO_CONTENT
        );
    }

    public function deleteFile(Request $request, $id){
        $evidence = ActivityReportEvidence::with('activityReport')->where('id', $id)->first();
        if ($evidence){
            DB::connection('mysql_contractors')->beginTransaction();
            $delete_path = $evidence->activityReport->contract_number . '/' . $evidence->file_path;
            if ($evidence->file_path && Storage::disk('activity-reports')->exists($delete_path)) {
                Storage::disk('activity-reports')->delete($delete_path);
            }
            $evidence->update(['file_path' => '']);
            DB::connection('mysql_contractors')->commit();

            return $this->success_message(
                __('validation.handler.success'),
                Response::HTTP_OK,
                Response::HTTP_OK,
                []
            );
        }
        return $this->error_response(
            __('validation.handler.unexpected_failure'),
            Response::HTTP_NOT_FOUND,
            'Evidencia no encontrada'
        );
    }
}
