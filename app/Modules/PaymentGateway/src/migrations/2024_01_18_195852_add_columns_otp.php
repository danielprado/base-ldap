<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnsOtp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_pse')->table('pago_pse', function (Blueprint $table) {
            //
            $table->text('otp')->nullable()->after('medio_id');
            $table->timestamp('otp_expiration')->nullable()->after('otp');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_pse')->table('pago_pse', function (Blueprint $table) {
            $table->dropColumn('otp');
            $table->dropColumn('otp_expiration');
        });
    }
}
