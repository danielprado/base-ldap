<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmbargoesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('embargoes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('contractor_id');
            $table->string('document')->comment('Número de documento del contratista embargado');
            $table->string('name')->comment('Nombre del contratista embargado');
            $table->string('type')->comment('Tipo de embargo');
            $table->string('filed')->comment('Número de proceso (expediente)');
            $table->date('date')->comment('Fecha de cautelar de la medida');
            $table->string('job')->comment('Código del oficio del juzgado');
            $table->string('judged')->comment('Nombre del juzgado');
            $table->string('description')->comment('Descripción y/o concepto');
            $table->string('judicial_account')->comment('Número interno de la cuenta dicial asignada por el banco');
            $table->string('value')->comment('Limite y/o valor de la medida');
            $table->timestamps();

            $table->foreign('contractor_id')
                ->references('id')
                ->on('contractors')
                ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('embargoes');
    }
}
