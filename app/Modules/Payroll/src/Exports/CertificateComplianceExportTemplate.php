<?php

namespace App\Modules\Payroll\src\Exports;

use App\Modules\Payroll\src\Models\CertificateCompliance;
use Maatwebsite\Excel\Excel;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;

class CertificateComplianceExportTemplate
{
    /**
     * @var array
     */
    private $collections;

    /**
     * @var array
     */
    private $collectionSupervisorSupport;

    /**
     * @var \PhpOffice\PhpSpreadsheet\Spreadsheet
     */
    private $file;

    /**
     * @var \App\Modules\Payroll\src\Models\CertificateCompliance;
     */
    private $certificate;

    /**
     * WareHouseExport constructor.
     */
    /* public function __construct(Contractor $contractor, $collection)
    {
        $this->collections = $collection;
        $this->contractor = $contractor;
    } */
    /**
     * CertificateComplianceExportTemplate constructor.
     */
    public function __construct(CertificateCompliance $certificate, $collection, $supervisorSupportList)
    {
        $this->collections = $collection;
        $this->certificate = $certificate;
        $this->collectionSupervisorSupport = $supervisorSupportList;

    }

    public function create()
    {
        try {
            $this->file = IOFactory::load( storage_path('app/templates/CERTIFICADO_CUMPLIMIENTO_COLECTIVO_V2_FIRMAS_FIN.xlsx') );
            //$this->file->setActiveSheetIndex(1);
            $this->file->setActiveSheetIndex(0);
            $this->worksheet = $this->file->getActiveSheet();
            $this->worksheet->getProtection()->setSheet(true);

            $this->worksheet->getCell('Q9')->setValue($this->certificate->entry);
            $this->worksheet->getCell('W9')->setValue($this->certificate->funding_source);
            $style = array(
                'alignment' => array(
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                )
            );
            if(count($this->collections) > 1){
                $this->worksheet->insertNewRowBefore(14, count($this->collections) - 1);
            }
            $i = 13;
            $contador = 1;
            $total_pagar_aux = 0;

            $collectionAux = collect([]);
            $countByIdentification = $this->collections->countBy('contract_number');
            foreach ($this->collections as $key => $collection) {

                $this->worksheet->mergeCells("D$i:E$i");
                $this->worksheet->getStyle("C$i:U$i")->applyFromArray($style);

                //combinar celdas por identificación
                if(! $collectionAux->contains( $collection['contract_number'] ) ){
                    $y = $i + $countByIdentification["{$collection['contract_number']}"];
                    $y--;
                    $this->worksheet->getCell("C$i")->setValue($contador);
                    $this->worksheet->mergeCells("C$i:C$y");
                    $this->worksheet->mergeCells("G$i:G$y");
                    $this->worksheet->mergeCells("U$i:U$y");

                    // $filteredCollectionByIdentificaction = $this->collectisons->where('identification', $collection['identification']);

                    // Opción 1: Mostrar total pago mes, la sumatoria de todos los TOTAL A PAGAR POR PMU SEGUN LLOS DÍAS, de cada contratista
                    // $totalPagoMes = 0;
                    // foreach($filteredCollectionByIdentificaction as $key => $collectionIdentification){
                    //     $totalPagoMes += round( (int) $collectionIdentification['dias_trabajados'] / 30 * (double) $collectionIdentification['pago_mensual'] ) ;
                    // }
                    // $this->worksheet->getCell("U$i")->setValue(isset($totalPagoMes) ? (double) $totalPagoMes : null);

                    //Opción 2: Mostrar pago mes suimando TOTAL POR PMU sin tenerr en cuenta los días, porr cada contratista
                    //$totalPagoMes = $filteredCollectionByIdentificaction->sum('pago_mensual');

                    //Opción 3: Mostrar pogo mes campo full_payment que viene desde SEVEN
                    $this->worksheet->getCell("U$i")->setValue(isset($collection['full_payment']) ? (double) $collection['full_payment'] : null);
                    $contador++;
                    $collectionAux->push($collection['contract_number']);
                }

                $this->worksheet->getCell("D$i")->setValue(isset($collection['person_name']) ? (string) $collection['person_name'] : null);
                $this->worksheet->getCell("F$i")->setValue(isset($collection['identification']) ? (string) $collection['identification'] : null);
                $this->worksheet->getCell("G$i")->setValue(isset($collection['contract_object']) ? (string) $collection['contract_object'] : null);
                $this->worksheet->getCell("H$i")->setValue(isset($collection['entry']) ? (string) $collection['entry'] : null);
                $this->worksheet->getCell("I$i")->setValue(isset($collection['contract_number']) ? (string) $collection['contract_number'] : null);
                $this->worksheet->getCell("J$i")->setValue(isset($collection['registry_number']) ? (string) $collection['registry_number'] : null);

                $this->worksheet->getCell("K$i")->setValue(isset($collection['source']) ? (string) $collection['source'] : null);
                $this->worksheet->getCell("L$i")->setValue(isset($collection['exspense_concept']) ? (string) $collection['exspense_concept'] : null);

                $this->worksheet->getCell("M$i")->setValue(isset($collection['position']) ? (string) $collection['position'] : null);
                $this->worksheet->getCell("N$i")->setValue(isset($collection['pmr']) ? (string) $collection['pmr'] : null);

                $this->worksheet->getCell("O$i")->setValue(date('d/m/Y',strtotime($collection['start_date'])) );
                $this->worksheet->getCell("P$i")->setValue(date('d/m/Y',strtotime($collection['final_date'])) );

                $this->worksheet->getCell("Q$i")->setValue('X');
                $this->worksheet->getCell("R$i")->setValue(' ');
                $this->worksheet->getCell("S$i")->setValue('X');
                $this->worksheet->getCell("T$i")->setValue(' ');
                // $this->worksheet->getCell("U$i")->setValue(isset($collection['pago_mensual']) ? (double) $collection['pago_mensual'] : null);
                //$this->worksheet->getCell("R$i")->setValue(toUpper($this->certificate->settlement_period));
                $this->worksheet->getCell("V$i")->setValue("{$collection['startPeriod']} AL {$collection['finalPeriod']}");
                $this->worksheet->getCell("W$i")->setValue(isset($collection['dias_trabajados']) ? (int) $collection['dias_trabajados'] : null);
                //$this->worksheet->getCell("T$i")->setValue(isset($collection['total_pagar']) ? (double) $collection['total_pagar'] : null);
                $aux_total = round( (int) $collection['dias_trabajados'] / 30 * (double) $collection['pago_mensual'] );
                $total_pagar_aux += $aux_total;
                $this->worksheet->getCell("X$i")->setValue($aux_total);

                // mostrar si es reserva o el otro
                // $this->worksheet->getCell("Y$i")->setValue(isset($collection['is_reserve']) ?  (strcmp((string)$collection['is_reserve'], 'X') !== 0 ? 'VIGENCIA' : 'RESERVA' ) : 'VIGENCIA');
                //$this->worksheet->setCellValue('T$i','=ROUND(Q$i/30*S$i;0)');
                //$this->worksheet->getCell("T$i")->setValue('=ROUND(Q$i/30*S$i;0)');
                /* $this->worksheet->setCellValueExplicit(
                    "T$i",
                    '=ROUND(Q$i/30*S$i;0)',
                    \PhpOffice\PhpSpreadsheet\Cell\DataType::TYPE_NUMERIC
                ); */
                $i++;
            }
            $i++;

            //Total a pagar nomina
            $this->worksheet->getCell("X$i")->setValue($total_pagar_aux);
            // Notas u observaciones
            $this->worksheet->getCell("C$i")->setValue(isset($this->certificate->observations) ? "OBSERVACIONES: " . toUpper( (string) $this->certificate->observations ) : null);

            //i es para la fila nombre
            $i += 4;
            $filaFirma = $i - 1;
            $filaIdentificacion = $i + 1;
            $filaCargo = $i + 2;

            $this->worksheet->getCell("E$i")->setValue(toUpper($this->certificate->supervisor_name));
            $this->worksheet->getCell("E$filaIdentificacion")->setValue(toUpper($this->certificate->supervisor_identification));
            $this->worksheet->getCell("E$filaCargo")->setValue(toUpper($this->certificate->supervisor_profession));

            //Mostrar información auxiliares supervisor
            if(count($this->collectionSupervisorSupport) > 0){
                $contAux = 1;
                foreach ($this->collectionSupervisorSupport as $key => $item) {
                    switch ($contAux) {
                        case 1:
                        case 6:
                        case 11:
                            $this->worksheet->getCell("F$filaFirma")->setValue('Firma');
                            $this->worksheet->getCell("F$i")->setValue('Nombre');
                            $this->worksheet->getCell("F$filaIdentificacion")->setValue('Documento');
                            $this->worksheet->getCell("F$filaCargo")->setValue('Cargo');

                            $this->worksheet->getStyle("G$filaFirma")->getBorders()->getBottom()->setBorderStyle(Border::BORDER_THIN);
                            $this->worksheet->getStyle("G$i")->getBorders()->getBottom()->setBorderStyle(Border::BORDER_THIN);
                            $this->worksheet->getStyle("G$filaIdentificacion")->getBorders()->getBottom()->setBorderStyle(Border::BORDER_THIN);
                            $this->worksheet->getStyle("G$filaCargo")->getBorders()->getBottom()->setBorderStyle(Border::BORDER_THIN);

                            $this->worksheet->getCell("G$i")->setValue(isset($item['name']) ? (string) $item['name'] : null);
                            $this->worksheet->getCell("G$filaIdentificacion")->setValue(isset($item['numberIdentification']) ? (string) $item['numberIdentification'] : null);
                            $this->worksheet->getCell("G$filaCargo")->setValue(isset($item['profession']) ? (string) $item['profession'] : null);
                            break;
                        case 2:
                        case 7:
                        case 12:
                            if($contAux != 2){
                                $this->worksheet->mergeCells("I$i:L$i");
                                $this->worksheet->mergeCells("I$filaIdentificacion:L$filaIdentificacion");
                                $this->worksheet->mergeCells("I$filaCargo:L$filaCargo");
                            }
                            $this->worksheet->getCell("H$filaFirma")->setValue('Firma');
                            $this->worksheet->getCell("H$i")->setValue('Nombre');
                            $this->worksheet->getCell("H$filaIdentificacion")->setValue('Documento');
                            $this->worksheet->getCell("H$filaCargo")->setValue('Cargo');

                            $this->worksheet->getStyle("I$filaFirma:L$filaFirma")->getBorders()->getBottom()->setBorderStyle(Border::BORDER_THIN);
                            $this->worksheet->getStyle("I$i:L$i")->getBorders()->getBottom()->setBorderStyle(Border::BORDER_THIN);
                            $this->worksheet->getStyle("I$filaIdentificacion:L$filaIdentificacion")->getBorders()->getBottom()->setBorderStyle(Border::BORDER_THIN);
                            $this->worksheet->getStyle("I$filaCargo:L$filaCargo")->getBorders()->getBottom()->setBorderStyle(Border::BORDER_THIN);

                            $this->worksheet->getCell("I$i")->setValue(isset($item['name']) ? (string) $item['name'] : null);
                            $this->worksheet->getCell("I$filaIdentificacion")->setValue(isset($item['numberIdentification']) ? (string) $item['numberIdentification'] : null);
                            $this->worksheet->getCell("I$filaCargo")->setValue(isset($item['profession']) ? (string) $item['profession'] : null);
                            break;
                        case 3:
                        case 8:
                        case 13:
                            if($contAux != 3){
                                $this->worksheet->mergeCells("N$i:P$i");
                                $this->worksheet->mergeCells("N$filaIdentificacion:P$filaIdentificacion");
                                $this->worksheet->mergeCells("N$filaCargo:P$filaCargo");
                            }
                            $this->worksheet->getCell("M$filaFirma")->setValue('Firma');
                            $this->worksheet->getCell("M$i")->setValue('Nombre');
                            $this->worksheet->getCell("M$filaIdentificacion")->setValue('Documento');
                            $this->worksheet->getCell("M$filaCargo")->setValue('Cargo');

                            $this->worksheet->getStyle("N$filaFirma:P$filaFirma")->getBorders()->getBottom()->setBorderStyle(Border::BORDER_THIN);
                            $this->worksheet->getStyle("N$i:P$i")->getBorders()->getBottom()->setBorderStyle(Border::BORDER_THIN);
                            $this->worksheet->getStyle("N$filaIdentificacion:P$filaIdentificacion")->getBorders()->getBottom()->setBorderStyle(Border::BORDER_THIN);
                            $this->worksheet->getStyle("N$filaCargo:P$filaCargo")->getBorders()->getBottom()->setBorderStyle(Border::BORDER_THIN);

                            $this->worksheet->getCell("N$i")->setValue(isset($item['name']) ? (string) $item['name'] : null);
                            $this->worksheet->getCell("N$filaIdentificacion")->setValue(isset($item['numberIdentification']) ? (string) $item['numberIdentification'] : null);
                            $this->worksheet->getCell("N$filaCargo")->setValue(isset($item['profession']) ? (string) $item['profession'] : null);
                            break;
                        case 4:
                        case 9:
                        case 14:
                            if($contAux != 4){
                                $this->worksheet->mergeCells("S$i:V$i");
                                $this->worksheet->mergeCells("S$filaIdentificacion:V$filaIdentificacion");
                                $this->worksheet->mergeCells("S$filaCargo:V$filaCargo");


                                $this->worksheet->mergeCells("Q$filaFirma:R$filaFirma");
                                $this->worksheet->mergeCells("Q$i:R$i");
                                $this->worksheet->mergeCells("Q$filaIdentificacion:R$filaIdentificacion");
                                $this->worksheet->mergeCells("Q$filaCargo:R$filaCargo");
                            }
                            $this->worksheet->getCell("Q$filaFirma")->setValue('Firma');
                            $this->worksheet->getCell("Q$i")->setValue('Nombre');
                            $this->worksheet->getCell("Q$filaIdentificacion")->setValue('Documento');
                            $this->worksheet->getCell("Q$filaCargo")->setValue('Cargo');

                            $this->worksheet->getStyle("S$filaFirma:V$filaFirma")->getBorders()->getBottom()->setBorderStyle(Border::BORDER_THIN);
                            $this->worksheet->getStyle("S$i:V$i")->getBorders()->getBottom()->setBorderStyle(Border::BORDER_THIN);
                            $this->worksheet->getStyle("S$filaIdentificacion:V$filaIdentificacion")->getBorders()->getBottom()->setBorderStyle(Border::BORDER_THIN);
                            $this->worksheet->getStyle("S$filaCargo:V$filaCargo")->getBorders()->getBottom()->setBorderStyle(Border::BORDER_THIN);

                            $this->worksheet->getCell("S$i")->setValue(isset($item['name']) ? (string) $item['name'] : null);
                            $this->worksheet->getCell("S$filaIdentificacion")->setValue(isset($item['numberIdentification']) ? (string) $item['numberIdentification'] : null);
                            $this->worksheet->getCell("S$filaCargo")->setValue(isset($item['profession']) ? (string) $item['profession'] : null);
                            break;
                        case 5:
                        case 10:
                            $this->worksheet->insertNewRowBefore($filaCargo + 2, 5);
                            $i = $filaCargo + 4;

                            $filaFirma = $i - 1;
                            $filaIdentificacion = $i + 1;
                            $filaCargo = $i + 2;
                            $this->worksheet->getCell("D$filaFirma")->setValue('Firma');
                            $this->worksheet->getCell("D$i")->setValue('Nombre');
                            $this->worksheet->getCell("D$filaIdentificacion")->setValue('Documento');
                            $this->worksheet->getCell("D$filaCargo")->setValue('Cargo');

                            $this->worksheet->getStyle("E$filaFirma")->getBorders()->getBottom()->setBorderStyle(Border::BORDER_THIN);
                            $this->worksheet->getStyle("E$i")->getBorders()->getBottom()->setBorderStyle(Border::BORDER_THIN);
                            $this->worksheet->getStyle("E$filaIdentificacion")->getBorders()->getBottom()->setBorderStyle(Border::BORDER_THIN);
                            $this->worksheet->getStyle("E$filaCargo")->getBorders()->getBottom()->setBorderStyle(Border::BORDER_THIN);

                            $this->worksheet->getCell("E$i")->setValue(isset($item['name']) ? (string) $item['name'] : null);
                            $this->worksheet->getCell("E$filaIdentificacion")->setValue(isset($item['numberIdentification']) ? (string) $item['numberIdentification'] : null);
                            $this->worksheet->getCell("E$filaCargo")->setValue(isset($item['profession']) ? (string) $item['profession'] : null);

                            break;
                        default:
                            # code...
                            break;
                    }
                    $contAux ++;
                }
            }
            //Aumento para info de quien elaboró
            if (count($this->collectionSupervisorSupport) > 9 )
                $i += 4;
            elseif(count($this->collectionSupervisorSupport) > 4)
                $i += 5;
            else
                $i += 6;

            $this->worksheet->getCell("E$i")->setValue(isset($this->certificate->diligence_name) ? toUpper( (string) $this->certificate->diligence_name ) . " - " . toUpper( (string) $this->certificate->diligence_identification ) : null);

            $this->worksheet->getStyle("A1:Z$i")//A13:U$i
                            ->getProtection()
                            ->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_PROTECTED);

            return IOFactory::createWriter($this->file, Excel::XLSX);
            // return IOFactory::createWriter($this->file, 'Mpdf');
        } catch (\Exception $exception) {

        }
    }
}
