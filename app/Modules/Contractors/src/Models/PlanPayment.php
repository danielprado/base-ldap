<?php

namespace App\Modules\Contractors\src\Models;


use App\Models\Security\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlanPayment extends Model
{

    use SoftDeletes;

    const STATUS_PENDING = 'PENDING';
    const STATUS_APPROVED = 'APPROVED';
    const STATUS_PAYROLL = 'PAYROLL';
    const STATUS_BINNACLE = 'BINNACLE';
    const STATUS_INVOICING = 'INVOICING';
    const STATUS_PAYMENT = 'PAYMENT';
    const STATUS_CANCELLED = 'CANCELLED';

    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'mysql_contractors';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'plans';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'plan_number',
        'contract',
        'paid',
        'year',
        'month',
        'amount_day_pay',
        'start_day',
        'final_day',
        'total_payment',
        'activity_report_id',
        'compliance_id',
        'supervisor_id',
        'contract_id',
        'payroll_id',
        'status',
        'approval_date',
        'filed_number',
        'reserve'
    ];

    protected $dates = ['approval_date', 'deleted_at'];

    protected $casts = [
        'reserve' => 'bool',
    ];

    public function getPmrGroupedData()
    {
        return $this->pmrs()
            ->select('funding_source', 'binnacle_bir_numo', DB::raw('SUM(total_pay_month) as total_pay_month'))
            ->groupBy('funding_source', 'binnacle_bir_numo')
            ->get();
    }

    public function contract()
    {
        return $this->belongsTo(Contract::class, 'contract_id', 'id');
    }

    public function pmrs()
    {
        return $this->hasMany(PmrPlanPayment::class, 'payment_plan_id', 'id');
    }

    public function payroll()
    {
        return $this->belongsTo(Payroll::class, 'payroll_id');
    }

    public function supervisor()
    {
        return $this->belongsTo(User::class, 'supervisor_id');
    }

    public function activityReport()
    {
        return $this->belongsTo(ActivityReport::class, 'activity_report_id');
    }

    public function compliance()
    {
        return $this->belongsTo(Compliance::class, 'compliance_id');
    }
}
