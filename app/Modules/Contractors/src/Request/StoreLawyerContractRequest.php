<?php


namespace App\Modules\Contractors\src\Request;


use App\Modules\Contractors\src\Models\Contract;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreLawyerContractRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contract_type_id' =>  'required|numeric|exists:mysql_contractors.contract_types,id',
            'contract_year' =>  'required|numeric|min:2019|max:2999',
            'contract' =>  [
                'required',
                'string',
                /*
                Rule::unique('mysql_contractors.contracts')->where(function ($query) {
                    $contract_number = str_pad($this->get('contract'), 4, '0', STR_PAD_LEFT);
                    $contract = toUpper("IDRD-CTO-{$contract_number}-{$this->get('contract_year')}");
                    return $query
                        ->where('contract_type_id', $this->get('contract_type_id'))
                        ->where('contract', $contract);
                })
                */
            ],
            'start_date'    =>  'required|date|date_format:Y-m-d|before:final_date',
            'final_date'    =>  'required|date|date_format:Y-m-d|after:start_date',
            'start_suspension_date'    =>  'nullable|required_if:contract_type_id,'.Contract::CONTRACT_SUSPENSION.'|date|date_format:Y-m-d|before:final_suspension_date',
            'final_suspension_date'    =>  'nullable|required_if:contract_type_id,'.Contract::CONTRACT_SUSPENSION.'|date|date_format:Y-m-d|after:start_suspension_date',
            'total' =>  'required|numeric',
            'duration' =>  'required|numeric',
            'duration_days' =>  'required|numeric',
            'object_contract' =>  'nullable|string',
            'type' =>  'nullable|string',
            'total_addition'    =>  'nullable|required_if:contract_type_id,'.Contract::CONTRACT_ADD,
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'document_type_id'  =>  'tipo de documento',
            'document'  =>  'número de documento',
            'name'  =>  'nombres',
            'surname' =>  'apellidos',
            'email' =>  'correo personal',
            'contract_type_id'  =>  'tipo de trámite',
            'contract'    =>  'contrato',
            'start_date'    =>  'fecha de inicio del contrato',
            'final_date'    =>  'fecha de terminación del contrato',
            'start_suspension_date'    =>  'fecha de inicio de suspensión',
            'final_suspension_date'    =>  'fecha de terminación de suspensión',
            'total' =>  'valor del contrato o adición',
            'duration' => 'duración del contrato',
            'object_contract' => 'objeto del contrato'
        ];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'start_suspension_date.required_if' => 'La fecha de suspensión inicial es requerida cuento el tipo de trámite es: SUSPENSIÓN DE CONTRATO',
            'final_suspension_date.required_if'  => 'La fecha de suspensión final es requerida cuento el tipo de trámite es: SUSPENSIÓN DE CONTRATO',
        ];
    }
}
