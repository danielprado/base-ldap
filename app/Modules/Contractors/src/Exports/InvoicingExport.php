<?php

namespace App\Modules\Contractors\src\Exports;

use App\Models\Security\Subdirectorate;
use App\Models\Security\User;
use App\Traits\AppendHeaderToExcel;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Imtigger\LaravelJobStatus\JobStatus;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeWriting;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class InvoicingExport implements FromCollection, WithMapping, WithHeadings, WithColumnFormatting, ShouldAutoSize, WithEvents, WithTitle
{
    use Exportable, AppendHeaderToExcel;

    /**
     * @var int
     */
    private $counter;

    /**
     * @var JobStatus
     */
    private $job;

    /**
     * @var User
     */
    private $user;

    /**
     * @var Collection
     */
    private $payroll;

    /**
     * @var Subdirectorate|null
     */
    private $subdirectorate;

    /**
     * @var User|null
     */
    private $supervisor;

    /**
     * @var string
     */
    private $sheetTitle;

    private $payrollData;
    private $payrollGeneralInfo;

    public function __construct($payrollData, $subdirectorate, $supervisor, $sheetTitle, $payrollGeneralInfo)
    {

        $this->payrollData = $payrollData;
        $this->subdirectorate = $subdirectorate;
        $this->supervisor = $supervisor;
        $this->sheetTitle = $sheetTitle;
        $this->payrollGeneralInfo = $payrollGeneralInfo;
        //$this->job = $job;
        //$this->user = $user;
        //update_status_job($job, JobStatus::STATUS_EXECUTING, 'post-generate-binnacle');
    }

    public function collection()
    {
        $expandedData = [];
        $contractorCounter = 1;
        $currentPlanId = null;

        foreach ($this->payrollData as $item) {
            foreach ($item['pmrList'] as $pmr) {
                if ($currentPlanId !== $item['id']) {
                    $currentPlanId = $item['id'];
                    $counter = $contractorCounter++;
                }
                $expandedData[] = array_merge($item, $pmr, ['counter' => $counter]);
            }
        }

        $sortedData = collect($expandedData)->sortBy(function ($item) {
            return (int) $item['binnacleBirNumo'];
        })->values();

        return $sortedData;
    }

    public function headings(): array
    {
        return [
            'No.',
            'NOMBRE CONTRATISTA',
            'NÚMERO CEDULA',
            'OBJETO CONTRATO',
            'No. BITÁCORA',
            'RUBRO',
            'RP', // "pmrList -> rp"
            'FUENTE',
            'COMPONENTE',
            'PAGO MENSUAL', // "totalRawValue"
            'DIAS TRABAJADOS',
            'POSICIÓN',
            'PMR',
            'TOTAL A PAGAR', // "totalPaymentMonthPmr"
            'VALOR SALUD',
            'VALOR PENSIÓN',
            'VIVIENDA', // devolver si - no -> variable housingInterests true caso contrario NO cuando es false
            'MEDICINA PREPAGADA', // devolver si - no -> variable prepaidMedicine true caso contrario NO cuando es false
            'CANTIDAD DE DEPENDIENTES', // "childrenNumber"
            'HIJOS U OTROS (hasta 10%)', // dejar null por el momento
            'AFP',
            'AFC',
            'BASE ICA',   // totalIcaBase
            'EST. PCUL (0.5%)', // totalProCultureValue solo mostrar valores positivos
            'EST. PPM (0.2%)', // totalProOlderAdultValue solo mostrar valores positivos
            'TOTAL ICA 0.966', // totalIcaValue
            'RETEFUENTE LEY 1607', // totalRetefuenteValue
            'OTROS DESCUENTOS', // null
            'TOTAL DEDUCCIONES', // totalDeductions
            'NETO A PAGAR', // totalNetPayment
            'TOTAL EMBARGO' // totalNetPayment
        ];
    }

    public function map($row): array
    {
        return [
            $row['counter'],
            $row['contractorName'],
            $row['contractorDocument'],
            $row['contractObject'],
            $row['binnacleBirNumo'],
            $row['entry'],
            $row['rp'],
            $row['fundingSource'],
            $row['expenseComponent'],
            $row['contractMonthlypayment'],
            $row['planAmountDaysPay'],
            $row['position'],
            $row['pmr'],
            isset($row['totalPaymentMonthPmr']) ? (int) $row['totalPaymentMonthPmr'] : 0,
            isset($row['epsValue']) ? (string) $row['epsValue'] : '0',
            isset($row['afpValue']) ? (string) $row['afpValue'] : '0',
            isset($row['housingItem']) ? (string) $row['housingItem'] : '0',
            isset($row['prepaidMedicineValue']) ? (string) $row['prepaidMedicineValue'] : '0',
            $row['childrenNumber'] > 0 ? $row['childrenNumber'] : '0',
            isset($row['dependentValue']) ? (string) $row['dependentValue'] : '0',
            isset($row['afpVoluntaryValue']) ? (string) $row['afpVoluntaryValue'] : '0',
            isset($row['afcVoluntaryValue']) ? (string) $row['afcVoluntaryValue'] : '0',
            isset($row['icaBase']) ? (string) $row['icaBase'] : '0',
            isset($row['proCultureValue']) ? (string) $row['proCultureValue'] : '0',
            isset($row['proOlderAdultValue']) ? (string) $row['proOlderAdultValue'] : '0',
            isset($row['icaValue']) ? (string) $row['icaValue'] : '0',
            isset($row['retefuenteValue']) ? (string) $row['retefuenteValue'] : '0',
            isset($row['totalOtherDiscounts']) ? (string) $row['totalOtherDiscounts'] : '0',
            isset($row['totalDeductions']) ? (string) $row['totalDeductions'] : '0',
            isset($row['totalNetPayment']) ? (string) $row['totalNetPayment'] : '0',
            isset($row['totalEmbargoes']) ? (string) $row['totalEmbargoes'] : '0',
        ];
    }

    public function columnFormats(): array
    {
        return [
            'E' => NumberFormat::FORMAT_NUMBER,
            'J' => NumberFormat::FORMAT_CURRENCY_USD,
            'K' => NumberFormat::FORMAT_NUMBER,
            'N' => NumberFormat::FORMAT_CURRENCY_USD,
            'O' => NumberFormat::FORMAT_CURRENCY_USD,
            'P' => NumberFormat::FORMAT_CURRENCY_USD,
            'Q' => NumberFormat::FORMAT_CURRENCY_USD,
            'R' => NumberFormat::FORMAT_CURRENCY_USD,
            'S' => NumberFormat::FORMAT_NUMBER,
            'T' => NumberFormat::FORMAT_CURRENCY_USD,
            'U' => NumberFormat::FORMAT_CURRENCY_USD,
            'V' => NumberFormat::FORMAT_CURRENCY_USD,
            'W' => NumberFormat::FORMAT_CURRENCY_USD,
            'X' => NumberFormat::FORMAT_CURRENCY_USD,
            'Y' => NumberFormat::FORMAT_CURRENCY_USD,
            'Z' => NumberFormat::FORMAT_CURRENCY_USD,
            'AA' => NumberFormat::FORMAT_CURRENCY_USD,
            'AB' => NumberFormat::FORMAT_CURRENCY_USD,
            'AC' => NumberFormat::FORMAT_CURRENCY_USD,
            'AD' => NumberFormat::FORMAT_CURRENCY_USD,
            'AE' => NumberFormat::FORMAT_CURRENCY_USD
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $sheet = $event->sheet->getDelegate();
                $spreadsheet = $event->sheet->getParent();

                $spreadsheet->getProperties()
                    ->setCreator('Brayan Sebastian Aldana Casas')
                    ->setLastModifiedBy($this->supervisor['full_name']);

                // Proteger el archivo para que sea de solo lectura
                /*$sheet->getProtection()->setSheet(true);
                $sheet->getProtection()->setSort(true);
                $sheet->getProtection()->setInsertRows(true);
                $sheet->getProtection()->setFormatCells(true);
                $sheet->getProtection()->setPassword('');*/

                // Insertar filas vacías para ajustar el encabezado
                $sheet->insertNewRowBefore(1, 8);

                $drawing = new Drawing();
                $drawing->setName('Header Image');
                $drawing->setDescription('Header Image');
                $drawing->setPath(public_path('images/img.png'));
                $drawing->setHeight(50);
                $drawing->setCoordinates('B3');
                $drawing->setWorksheet($sheet);

                $sheet->mergeCells('A1:AE5');

                $sheet->setCellValue('A6', "CONSECUTIVO PLANILLA: " . $this->payrollGeneralInfo['name']);
                $sheet->mergeCells('A6:AE6');
                $sheet->getStyle('A6')->getFont()->setBold(true);

                $sheet->setCellValue('A7', "FECHA ELABORACIÓN: " . Carbon::parse($this->payrollGeneralInfo['createdAt'])->format('Y-m-d'));
                $sheet->mergeCells('A7:AE7');
                $sheet->getStyle('A7')->getFont()->setBold(true);

                $periodPac = Carbon::parse($this->payrollGeneralInfo['createdAt'])->format('Y-m');
                $sheet->setCellValue('A8', "PERIODO PAC: " . $periodPac);
                $sheet->mergeCells('A8:AE8');
                $sheet->getStyle('A8')->getFont()->setBold(true);


                $sheet->getStyle('A9:AE9')->getFont()->setBold(true);
                $sheet->getStyle('A9:AE9')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('A9:AE9')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $sheet->getStyle('A9:AE9')->getAlignment()->setWrapText(true);


                foreach (range('A', 'AE') as $columnID) {
                    $sheet->getColumnDimension($columnID)->setAutoSize(true);
                }

                // Ajustar el ancho de las columnas manualmente para que se ajuste al contenido
                $columnWidths = ['A' => 4, 'B' => 40, 'C' => 18, 'D' => 45, 'E' => 15, 'F' => 28, 'G' => 12, 'H' => 15, 'I' => 30, 'J' => 15, 'M' => 30,
                    'N' => 15, 'O' => 15, 'P' => 15, 'Q' => 15, 'R' => 15, 'S' => 15, 'T' => 15, 'U' => 15, 'V' => 15, 'W' => 15, 'X' => 15, 'Y' => 15,
                    'Z' => 15, 'AA' => 15, 'AB' => 15, 'AC' => 15, 'AD' => 15, 'AE' => 15];

                foreach ($columnWidths as $columnID => $width) {
                    $sheet->getColumnDimension($columnID)->setWidth($width);
                }


                $columnsToMerge = [
                    'A', 'B', 'C', 'D', 'J', 'K', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'AB', 'AC', 'AD', 'AE'
                ];

                // Columnas a solo centrar contenido
                $columnsToCenter = ['E', 'F', 'G', 'H', 'I', 'L', 'M', 'N', 'AA'];

                $rows = $sheet->toArray();
                $startRow = 10;
                $highestRow = count($rows);

                foreach ($columnsToMerge as $column) {
                    $previousCounter = null;
                    $previousValue = null;
                    $mergeStartRow = $startRow;

                    for ($i = $startRow; $i <= $highestRow; $i++) {
                        $currentCounter = $rows[$i - 1][0]; // Ajustar para la nueva fila de inicio
                        $currentValue = $rows[$i - 1][ord($column) - 65]; // 65 es el valor ASCII de 'A'

                        if ($currentCounter !== $previousCounter || $currentValue !== $previousValue) {
                            if ($previousValue !== null && $mergeStartRow < $i - 1) {
                                $mergeEndRow = $i - 1;
                                $sheet->mergeCells("$column{$mergeStartRow}:$column{$mergeEndRow}");
                            }
                            // Aplicar alineación a todas las celdas, incluso si solo una fila está presente
                            $sheet->getStyle("$column{$mergeStartRow}:$column{$i}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                            $sheet->getStyle("$column{$mergeStartRow}:$column{$i}")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                            $sheet->getStyle("$column{$mergeStartRow}:$column{$i}")->getAlignment()->setWrapText(true);
                            $mergeStartRow = $i;
                        }

                        if ($currentCounter !== $previousCounter) {
                            $previousCounter = $currentCounter;
                        }
                        $previousValue = $currentValue;
                    }

                    // Último grupo de celdas
                    if ($mergeStartRow <= $highestRow) {
                        $mergeEndRow = $highestRow;
                        $sheet->mergeCells("$column{$mergeStartRow}:$column{$mergeEndRow}");
                        $sheet->getStyle("$column{$mergeStartRow}:$column{$mergeEndRow}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                        $sheet->getStyle("$column{$mergeStartRow}:$column{$mergeEndRow}")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                        $sheet->getStyle("$column{$mergeStartRow}:$column{$mergeEndRow}")->getAlignment()->setWrapText(true);
                    }
                }

                // Combinación y centrado de celdas en la columna AA (retefuenteValue)
                $previousFundingSource = null;
                $previousRetefuenteValue = null;
                $mergeStartRow = $startRow; // Fila de inicio de la agrupación

                for ($i = $startRow; $i <= $highestRow; $i++) {
                    // Obtener el valor actual de la fuente de financiación y el valor de retefuente
                    $currentFundingSource = $rows[$i - 1][ord('H') - 65]; // Columna de fuente de financiación
                    $currentRetefuenteValue = $rows[$i - 1][ord('AA') - 65]; // Columna de retefuente

                    // Si la fuente de financiación o el valor de retefuente son diferentes, hacemos el merge
                    if ($currentFundingSource !== $previousFundingSource || $currentRetefuenteValue !== $previousRetefuenteValue) {
                        if ($previousRetefuenteValue !== null && $mergeStartRow < $i - 1) {
                            // Agrupar las celdas en la columna AA
                            $sheet->mergeCells("AA{$mergeStartRow}:AA" . ($i - 1));
                            $sheet->getStyle("AA{$mergeStartRow}:AA" . ($i - 1))->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                            $sheet->getStyle("AA{$mergeStartRow}:AA" . ($i - 1))->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                            $sheet->getStyle("AA{$mergeStartRow}:AA" . ($i - 1))->getAlignment()->setWrapText(true);
                        }
                        $mergeStartRow = $i; // Actualizamos la fila de inicio del siguiente merge
                    }

                    // Actualizar los valores anteriores
                    $previousFundingSource = $currentFundingSource;
                    $previousRetefuenteValue = $currentRetefuenteValue;
                }

                // Último merge para las filas restantes en la columna AA
                if ($mergeStartRow <= $highestRow) {
                    $sheet->mergeCells("AA{$mergeStartRow}:AA{$highestRow}");
                    $sheet->getStyle("AA{$mergeStartRow}:AA{$highestRow}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                    $sheet->getStyle("AA{$mergeStartRow}:AA{$highestRow}")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                    $sheet->getStyle("AA{$mergeStartRow}:AA{$highestRow}")->getAlignment()->setWrapText(true);
                }

                // Centrando contenido de columnas sin combinar
                foreach ($columnsToCenter as $column) {
                    $sheet->getStyle("$column{$startRow}:$column{$highestRow}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                    $sheet->getStyle("$column{$startRow}:$column{$highestRow}")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                    $sheet->getStyle("$column{$startRow}:$column{$highestRow}")->getAlignment()->setWrapText(true);
                }

                // Aplicar bordes a todas las celdas
                $highestColumn = $sheet->getHighestColumn();
                $sheet->getStyle("A9:{$highestColumn}{$highestRow}")
                    ->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

                $startingDataRow = 10; // Definir la fila donde comienzan los datos, ajústala según corresponda
                $highestRow = $sheet->getHighestRow(); // Obtener la última fila con datos

                // Calcular la fila de inicio para los totales
                $totalsRow = $highestRow + 2;

                // Encabezados para la fila de totales
                $sheet->setCellValue("M{$totalsRow}", 'TOTAL F.F.');
                $sheet->getStyle("M{$totalsRow}")->getFont()->setBold(true);
                $sheet->getStyle("M{$totalsRow}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle("M{$totalsRow}")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);


                // Calcular y añadir los totales de las columnas
                $columnsToTotal = ['N' => 'totalPaymentMonthPmr', 'O' => 'epsValue', 'P' => 'afpValue', 'Q' => 'housingItem', 'R' => 'prepaidMedicineValue', 'S' => 'childrenNumber', 'T' => 'dependentValue', 'U' => 'afpVoluntaryValue', 'V' => 'afcVoluntaryValue', 'W' => 'icaBase', 'X' => 'proCultureValue', 'Y' => 'proOlderAdultValue', 'Z' => 'icaValue', 'AA' => 'retefuenteValue', 'AB' => 'totalOtherDiscounts', 'AC' => 'totalDeductions', 'AD' => 'totalNetPayment', 'AE' => 'totalEmbargoes'];

                foreach ($columnsToTotal as $column => $field) {
                    $formula = "=SUM({$column}{$startingDataRow}:{$column}{$highestRow})";
                    $sheet->setCellValue("{$column}{$totalsRow}", $formula);

                    // Configurar el formato de la celda para mostrar como moneda o número
                    if ($column === 'S') {
                        $sheet->getStyle("{$column}{$totalsRow}")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_NUMBER);
                    } else {
                        $sheet->getStyle("{$column}{$totalsRow}")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_USD);
                    }

                    $sheet->getStyle("{$column}{$totalsRow}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                    $sheet->getStyle("{$column}{$totalsRow}")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                }

                // Aplicar bordes a la fila de totales
                $sheet->getStyle("M{$totalsRow}:AE{$totalsRow}")->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

                // Dejar una línea en blanco después de toda la información
                $highestRow = $totalsRow + 1;

                // Encabezados para la tabla de resumen
                $highestRow += 1; // Ajustar para dejar una línea en blanco
                $sheet->setCellValue("B{$highestRow}", 'FUENTE DE FINANCIAMIENTO');
                $sheet->setCellValue("C{$highestRow}", 'TOTAL');
                $sheet->getStyle("B{$highestRow}:C{$highestRow}")->getFont()->setBold(true);
                $sheet->getStyle("B{$highestRow}:C{$highestRow}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

                // Ajustar el ancho de las columnas de la tabla de resumen
                $sheet->getColumnDimension('B')->setAutoSize(true);
                $sheet->getColumnDimension('C')->setAutoSize(true);

                // Insertar datos de la tabla de resumen fuente de financiamiento
                $fundingSourceTotalList = $this->payrollGeneralInfo['fundingSourceTotalList'];
                foreach ($fundingSourceTotalList as $index => $fundingSource) {
                    $row = $highestRow + $index + 1;
                    $sheet->setCellValue("B{$row}", $fundingSource['description']);
                    $sheet->setCellValue("C{$row}", $fundingSource['total']);
                    $sheet->getStyle("B{$row}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                    $sheet->getStyle("C{$row}")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_USD);
                    $sheet->getStyle("C{$row}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                }

                // Aplicar bordes a la tabla de resumen
                $endRow = $highestRow + count($fundingSourceTotalList);
                $sheet->getStyle("B{$highestRow}:C{$endRow}")->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

                // Calcular la fila de inicio para la tabla de resumen de rubros
                $highestRow = $endRow + 2; // Añadir dos filas de espacio

                // Encabezados para la tabla de resumen de rubros
                $sheet->setCellValue("B{$highestRow}", 'RUBRO');
                $sheet->setCellValue("C{$highestRow}", 'TOTAL');
                $sheet->getStyle("B{$highestRow}:C{$highestRow}")->getFont()->setBold(true);
                $sheet->getStyle("B{$highestRow}:C{$highestRow}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

                // Insertar datos de la tabla de resumen de rubros
                $entryTotalList = $this->payrollGeneralInfo['entryTotalList'];
                foreach ($entryTotalList as $index => $entry) {
                    $row = $highestRow + $index + 1;
                    $sheet->setCellValue("B{$row}", $entry['description']);
                    $sheet->setCellValue("C{$row}", $entry['total']);
                    $sheet->getStyle("B{$row}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                    $sheet->getStyle("C{$row}")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_USD);
                    $sheet->getStyle("C{$row}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                }

                // Aplicar bordes a la tabla de resumen de rubros
                $endRow = $highestRow + count($entryTotalList);
                $sheet->getStyle("B{$highestRow}:C{$endRow}")->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

                // Calcular la fila de inicio para la tabla de resumen de pospre
                $highestRow = $endRow + 2; // Añadir dos filas de espacio

                // Encabezados para la tabla de resumen de pospre
                $sheet->setCellValue("B{$highestRow}", 'POSPRE');
                $sheet->setCellValue("C{$highestRow}", 'TOTAL');
                $sheet->getStyle("B{$highestRow}:C{$highestRow}")->getFont()->setBold(true);
                $sheet->getStyle("B{$highestRow}:C{$highestRow}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

                // Insertar datos de la tabla de resumen de pospre
                $pospreTotalList = $this->payrollGeneralInfo['pospresTotalList'];
                foreach ($pospreTotalList as $index => $pospre) {
                    $row = $highestRow + $index + 1;
                    $sheet->setCellValue("B{$row}", $pospre['description']);
                    $sheet->setCellValue("C{$row}", $pospre['total']);
                    $sheet->getStyle("B{$row}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                    $sheet->getStyle("C{$row}")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_USD);
                    $sheet->getStyle("C{$row}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                }

                // Aplicar bordes a la tabla de resumen de pospre
                $endRow = $highestRow + count($pospreTotalList);
                $sheet->getStyle("B{$highestRow}:C{$endRow}")->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

                $footerRow = $endRow + 4;

                $sheet->setCellValue("B{$footerRow}", 'Firma: ');
                $sheet->mergeCells("B{$footerRow}:C{$footerRow}");
                $sheet->getStyle("B{$footerRow}")->getFont()->setBold(true);

                $signaturePath = storage_path('app/user/' . $this->supervisor['signature']);
                if (file_exists($signaturePath)) {
                    $drawing = new Drawing();
                    $drawing->setName('Signature');
                    $drawing->setDescription('Signature');
                    $drawing->setPath($signaturePath);
                    $drawing->setHeight(65);
                    $drawing->setCoordinates("B" . ($footerRow + 1));
                    $drawing->setWorksheet($sheet);
                }

                $sheet->setCellValue("B" . ($footerRow + 5), "Nombre: " . $this->supervisor['full_name']);
                $sheet->getStyle("B" . ($footerRow + 5))->getFont()->setBold(true);

                $sheet->setCellValue("B" . ($footerRow + 6), "Cargo: " . $this->supervisor['description']);
                $sheet->getStyle("B" . ($footerRow + 6))->getFont()->setBold(true);
            },
        ];
    }

    public function title(): string
    {
        return $this->sheetTitle;
    }
}
