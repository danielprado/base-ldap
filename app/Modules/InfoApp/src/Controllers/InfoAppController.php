<?php

namespace App\Modules\InfoApp\src\Controllers;

use App\Http\Controllers\Controller;

use App\Modules\InfoApp\src\Models\Categories;
use App\Modules\InfoApp\src\Models\Version;
use Illuminate\Http\Request;

class InfoAppController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function categories()
    {
        return Categories::all();
    }

    public function getVersionApp(Request $request)
    {
        try {
            $version = Version::where('nameapp', $request->get('name'))->select('id', 'nameapp', 'latestversion', 'message')->first();
            return response()->json([
                'status' => 'OK',
                'message' => 'versión app obtenida satisfactoriamente',
                'data' => $version,
                'Error' => false,
            ], 200);
        } catch (\Exception $ex) {
            return response()->json([
                'status' => 'OK',
                'message' => 'error al obtener la versión de la app',
                'data' => $ex->getMessage(),
                'Error' => true,
            ], 500);
        }
    }
}