<?php


namespace App\Modules\PaymentGateway\src\Mail;


use App\Modules\Contractors\src\Models\Contractor;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Crypt;

class BookingMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Contractor
     */
    private $texto;
    private $ambiente;

    /**
     * Create a new job instance.
     *
     * @param string $text
     * @param string $entorn
     */
    public function __construct(string $text, string $entorn)
    {
        $this->texto = $text;
        $this->entorno = $entorn;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mail.email')
            // ->subject('Creación de Usuario Portal Contratista')
            ->with([
                'texto'    => $this->texto,
                'entorno'    => $this->entorno,
            ]);
    }
}
