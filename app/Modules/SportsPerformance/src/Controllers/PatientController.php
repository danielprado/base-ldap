<?php

namespace App\Modules\SportsPerformance\src\Controllers;

use Throwable;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use App\Models\Security\Sex as PersonSex;
use App\Models\Security\Country;
use App\Models\Security\Locality;
use App\Models\Security\Department;
use App\Models\Security\DocumentType;
use App\Models\Security\GenderIdentity as PersonGenderIdentity;
use App\Modules\SportsPerformance\src\Models\Person;
use App\Modules\SportsPerformance\src\Models\Athlete;
use App\Modules\SportsPerformance\src\Models\Patient;
use App\Modules\SportsPerformance\src\Models\PopulationCharacterizationPeople;
use App\Modules\SportsPerformance\src\Models\Sex;
use App\Modules\SportsPerformance\src\Models\GenderIdentity;
use App\Modules\SportsPerformance\src\Models\SexualOrientation;
use App\Modules\SportsPerformance\src\Models\City;
use App\Modules\SportsPerformance\src\Models\Sport;
use App\Modules\SportsPerformance\src\Models\AthleteClassification;
use App\Modules\SportsPerformance\src\Models\ModalityClassification;
use App\Modules\SportsPerformance\src\Models\ParalympicAthlete;
use App\Modules\SportsPerformance\src\Models\Modality;
use App\Modules\SportsPerformance\src\Models\Disability;
use App\Modules\SportsPerformance\src\Request\PatientRequest;
use App\Modules\SportsPerformance\src\Resources\PersonResource;

class PatientController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function store(PatientRequest $request)
    {
        try {
            $document_type = DocumentType::where('Nombre_TipoDocumento', $request->get('document_type_code'))->first();
            if (!$document_type){
                return $this->error_response(
                    'El tipo de documento no se encuentra registrado en el sistema.',
                    Response::HTTP_NOT_FOUND
                );
            }
            $person = Person::where('Cedula', $request->get('document'))->where('Id_TipoDocumento', $document_type->Id_TipoDocumento)->first();
            if (!$person){
                return $this->error_response(
                    'La persona no se encuentra registrada en el sistema.',
                    Response::HTTP_NOT_FOUND
                );
            }

            $athlete = Athlete::where('Persona_Id', $person->Id_Persona)->first();
            if (!$athlete){
                return $this->error_response(
                    'El deportista no se encuentra registrado en el sistema.',
                    Response::HTTP_NOT_FOUND
                );
            }
            
            $patient = Patient::where('persona_id', $request->get('sim_person_id'))->first();
            if ($patient){
                return $this->error_response(
                    'El paciente ya se encuentra registrado en el sistema.',
                    Response::HTTP_CONFLICT
                );
            }

            DB::connection('mysql_sim')->beginTransaction();

            $patientData = $request->get('patient');
            $expedition_city = City::whereRaw('LOWER(nombre) = ?', [strtolower($patientData['expedition_municipality'])])->first();
            $expedition_department = Department::whereRaw('LOWER(Nombre_Departamento) = ?', [strtolower($patientData['expedition_department'])])->first();

            $patientData['persona_id'] = $person->Id_Persona;
            if (isset($expedition_city)){
                $patientData['ciudad_expedicion_doc'] = $expedition_city->Id;
            }
            if (isset($expedition_department)){
                $patientData['departamento_expedicion_doc'] = $expedition_department->Id_Departamento;
            }
            $form = Patient::create($patientData);

            $this->updatePerson($request, $person);
            $this->updateAthlete($request, $athlete);

            DB::connection('mysql_sim')->commit();

            return $this->success_message(
                __('validation.handler.success'),
                Response::HTTP_CREATED,
                Response::HTTP_CREATED,
                ['sim_person_id' => $form->persona_id]
            );
        } catch (Throwable $e) {
            DB::connection('mysql_sim')->rollBack();
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }
    
    public function update(PatientRequest $request, Patient $patient)
    {
        try {
            $document_type = DocumentType::where('Nombre_TipoDocumento', $request->get('document_type_code'))->first();
            if (!$document_type){
                return $this->error_response(
                    'El tipo de documento no se encuentra registrado en el sistema.',
                    Response::HTTP_NOT_FOUND
                );
            }
            $person = Person::where('Cedula', $request->get('document'))->where('Id_TipoDocumento', $document_type->Id_TipoDocumento)->first();
            if (!$person){
                return $this->error_response(
                    'La persona no se encuentra registrada en el sistema.',
                    Response::HTTP_NOT_FOUND
                );
            }
            $athlete = Athlete::where('Persona_Id', $person->Id_Persona)->first();
            if (!$athlete){
                return $this->error_response(
                    'El deportista no se encuentra registrado en el sistema.',
                    Response::HTTP_NOT_FOUND
                );
            }
    
            DB::connection('mysql_sim')->beginTransaction();
    
            $patientData = $request->get('patient');
            $expedition_city = City::whereRaw('LOWER(nombre) = ?', [strtolower($patientData['expedition_municipality'])])->first();
            $expedition_department = Department::whereRaw('LOWER(Nombre_Departamento) = ?', [strtolower($patientData['expedition_department'])])->first();

            $patientData['persona_id'] = $person->Id_Persona;
            if (isset($expedition_city)){
                $patientData['ciudad_expedicion_doc'] = $expedition_city->Id;
            }
            if (isset($expedition_department)){
                $patientData['departamento_expedicion_doc'] = $expedition_department->Id_Departamento;
            }
            $patient->update($patientData);

            $this->updatePerson($request, $person);
            $this->updateAthlete($request, $athlete);
    
            DB::connection('mysql_sim')->commit();
    
            return $this->success_message(
                __('validation.handler.success'),
                Response::HTTP_OK,
                Response::HTTP_OK,
                ['sim_person_id' => $patient->persona_id]
            );
        } catch (Throwable $e) {
            DB::connection('mysql_sim')->rollBack();
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }
    
    public function edit(PatientRequest $request)
    {
        try {
            $document_type = DocumentType::where('Nombre_TipoDocumento', $request->get('document_type_code'))->first();
            if (!$document_type){
                return $this->error_response(
                    'El tipo de documento no se encuentra registrado en el sistema.',
                    Response::HTTP_NOT_FOUND
                );
            }
            $person = Person::where('Cedula', $request->get('document'))->where('Id_TipoDocumento', $document_type->Id_TipoDocumento)->first();
            if (!$person){
                return $this->error_response(
                    'La persona no se encuentra registrada en el sistema.',
                    Response::HTTP_NOT_FOUND
                );
            }
            $athlete = Athlete::where('Persona_Id', $person->Id_Persona)->first();
            if (!$athlete){
                return $this->error_response(
                    'El deportista no se encuentra registrado en el sistema.',
                    Response::HTTP_NOT_FOUND
                );
            }
            $patient = Patient::where('persona_id', $person->Id_Persona)->first();
            if (!$patient) {
                return $this->error_response(
                    'El paciente no está registrado en el sistema.',
                    Response::HTTP_NOT_FOUND
                );
            }
    
            DB::connection('mysql_sim')->beginTransaction();
    
            $patientData = $request->get('patient');
            $expedition_city = City::whereRaw('LOWER(nombre) = ?', [strtolower($patientData['expedition_municipality'])])->first();
            $expedition_department = Department::whereRaw('LOWER(Nombre_Departamento) = ?', [strtolower($patientData['expedition_department'])])->first();

            $patientData['persona_id'] = $person->Id_Persona;
            if (isset($expedition_city)){
                $patientData['ciudad_expedicion_doc'] = $expedition_city->Id;
            }
            if (isset($expedition_department)){
                $patientData['departamento_expedicion_doc'] = $expedition_department->Id_Departamento;
            }
            $patient->update($patientData);

            $this->updatePerson($request, $person);
            $this->updateAthlete($request, $athlete);
    
            DB::connection('mysql_sim')->commit();
    
            return $this->success_message(
                __('validation.handler.success'),
                Response::HTTP_OK,
                Response::HTTP_OK,
                ['sim_person_id' => $patient->persona_id]
            );
        } catch (Throwable $e) {
            DB::connection('mysql_sim')->rollBack();
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }

    public function searchByPerson(Request $request)
    {
        try {
            $document_type = DocumentType::where('Nombre_TipoDocumento', $request->get('document_type_code'))->first();
            if (!$document_type){
                return $this->error_response(
                    'El tipo de documento no se encuentra registrado en el sistema.',
                    Response::HTTP_NOT_FOUND
                );
            }
            $person = Person::with(['patient', 'document_type'])->where('Cedula', $request->get('document'))
                                                ->where('Id_TipoDocumento', $document_type->Id_TipoDocumento)->first();
            if (!$person){
                return $this->error_response(
                    'La persona no se encuentra registrada en el sistema.',
                    Response::HTTP_NOT_FOUND
                );
            }
            $athlete = Athlete::where('Persona_Id', $person->Id_Persona)->first();
            if (!$athlete){
                return $this->error_response(
                    'El deportista no se encuentra registrado en el sistema.',
                    Response::HTTP_NOT_FOUND
                );
            }
            return $this->success_response(
                new PersonResource($person),
                Response::HTTP_OK
            );
        } catch (Throwable $e) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }

    public function updatePerson(PatientRequest $request, Person $person){
        $patient = $request->get('patient');
        $document_type = DocumentType::whereRaw('LOWER(Descripcion_TipoDocumento) = ?', [strtolower($patient['document_type'])])->first();
        $sex = PersonSex::whereRaw('LOWER(Nombre_Genero) = ?', [strtolower($patient['sex'])])->first();
        $gender_identity = PersonGenderIdentity::whereRaw('LOWER(identidad) = ?', [strtolower($patient['gender_identity'])])->first();

        $person['Primer_Nombre'] = $patient['first_name'];
        $person['Segundo_Nombre'] = $patient['middle_name'];
        $person['Primer_Apellido'] = $patient['last_name'];
        $person['Segundo_Apellido'] = $patient['second_last_name'];
        $person['Fecha_Nacimiento'] = $patient['birth_date'];
        
        if (isset($sex)){
            $person['Id_Genero'] = $sex->Id_Genero;
        }
        if (isset($gender_identity)){
            $person['Id_Identidad_Genero'] = $gender_identity->id;
        }
        if (isset($document_type)){
            $person['Id_TipoDocumento'] = $document_type->Id_TipoDocumento;
        }
        $person->save();
    }

    public function updateAthlete(PatientRequest $request, Athlete $athlete) {
        $patient = $request->get('patient');
        $sex = Sex::whereRaw('LOWER(nombre) = ?', [strtolower($patient['sex'])])->first();
        $gender_identity = GenderIdentity::whereRaw('LOWER(nombre) = ?', [strtolower($patient['gender_identity'])])->first();
        $sexual_orientation = SexualOrientation::whereRaw('LOWER(nombre) = ?', [strtolower($patient['sexual_orientation'])])->first();
        $birth_city = City::whereRaw('LOWER(nombre) = ?', [strtolower($patient['birth_municipality'])])->first();
        $birth_department = Department::whereRaw('LOWER(Nombre_Departamento) = ?', [strtolower($patient['birth_department'])])->first();
        $locality = Locality::whereRaw('LOWER(Nombre_Localidad) = ?', [strtolower($patient['locality'])])->first();

        $characterization = PopulationCharacterizationPeople::where('persona_id', $athlete->Persona_Id)->first();
        if (isset($characterization)){
            if (isset($sex)){
                $characterization['sexo_id'] = $sex->id;
            }
            if (isset($gender_identity)){
                $characterization['identidad_genero_id'] = $gender_identity->id;
            }
            if (isset($gender_identity)){
                $characterization['orien_sexual_id'] = $sexual_orientation->id;
            }
            $characterization->save();
        }

        if (isset($birth_city)){
            $athlete['Id_Ciudad_Nacimiento'] = $birth_city->Id;
        }
        if (isset($birth_department)){
            $athlete['Departamento_Id_Nac'] = $birth_department->Id_Departamento;
        }
        if (isset($locality)){
            $athlete['Localidad_Id_Localiza'] = $locality->Id_Localidad;
        }
        $athlete['Direccion_Localiza'] = $patient['address'];
        $athlete['Barrio_Localiza'] = $patient['neighborhood'];
        $athlete['Fijo_Localiza'] = $patient['landline'];
        $athlete['Celular_Localiza'] = $patient['mobile'];

        $sport  = Sport::whereRaw('LOWER(V_NOMBRE_DEPORTE) = ?', [strtolower($patient['sport'])])->first();
        $classification = AthleteClassification::whereRaw('LOWER(Nombre_Clasificacion_Deportista) = ?', [strtolower($patient['sector'])])->first();
        $disability = Disability::whereRaw('LOWER(Nombre_Discapacidad) = ?', [strtolower($patient['limitation_type'])])->first();
        $modality = Modality::whereRaw('LOWER(Nombre) = ?', [strtolower($patient['modality'])])->first();

        if (isset($sport)){
            $athlete['Id_Deporte'] = $sport->PK_I_ID_DEPORTE;
        }
        if (isset($classification)){
            $athlete['Clasificacion_Deportista_Id'] = $classification->Id;
            if (isset($disability) && $classification->Id == 2){
                $paralympic = ParalympicAthlete::updateOrCreate(
                    ['Deportista_Id' => $athlete->Id],
                    [
                        'Discapacidad_Id' => $disability->Id,
                        'Clasificacion_Funcional_Id' => 1
                    ]
                );
            }
        }
        $new_modality = null;
        if (isset($sport) && isset($classification)){
            if (!isset($modality) || $modality['Id_Deporte'] != $sport->PK_I_ID_DEPORTE){
                $new_modality = Modality::create([
                    'Id_Deporte' => $sport->PK_I_ID_DEPORTE,
                    'Id_Clasificacion' => $classification->Id,
                    'Nombre' => $patient['modality']
                ]);
            }
        }
        if (isset($new_modality) && isset($disability)){
            ModalityClassification::create([
                'Id_ClasificacionFuncional' => $classification->Id,
                'Id_Deporte' => $sport->PK_I_ID_DEPORTE,
                'Id_Discapacidad' => $disability->Id,
                'Id_Modalidad' => $new_modality->Id
            ]);
        }
        
        $athlete->save();
    }
}
