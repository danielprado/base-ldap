<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableTblSurveyResponses extends Migration
{
     /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'mysql_contractors';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->create('survey_responses', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('contractor_id')->comment('Id del contratista');
            $table->string('payment_changes'); // Almacena cómo percibe los cambios realizados en la forma de pago
            $table->boolean('timely_payment'); // Si considera que el pago es más oportuno
            $table->enum('tool_instructions', ['Adecuada', 'Medianamente adecuada', 'Poco adecuada']); // Calificación del instructivo
            $table->enum('tool_video', ['Adecuada', 'Medianamente adecuada', 'Poco adecuada']); // Calificación del video
            $table->enum('tool_infographic', ['Adecuada', 'Medianamente adecuada', 'Poco adecuada']); // Calificación de la infografía
            $table->enum('tool_support_desk', ['Adecuada', 'Medianamente adecuada', 'Poco adecuada']); // Calificación de la mesa de ayuda
            $table->string('information_clarity'); // Opinión sobre la claridad de la información proporcionada
            $table->enum('payment_satisfaction', ['Muy satisfecho/a', 'Satisfecho/a', 'Indiferente', 'Insatisfecho/a', 'Muy insatisfecho/a']); // Satisfacción con el tiempo de procesamiento del pago
            $table->text('recommendations')->nullable(); // Sugerencias o recomendaciones
            $table->timestamps(); // Campos created_at y updated_at
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->dropIfExists('survey_responses');
    }
}
