<?php

namespace App\Modules\Parks\src\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class FeatureFurnitureResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'UB_REFERENCIA' => isset($this['UB_REFERENCIA']) ? $this['UB_REFERENCIA'] : '',
            'NOMBRE_PARQ' => isset($this['NOMBRE_PARQ']) ? $this['NOMBRE_PARQ'] : '',
            'ID_MOBILIARIO' => isset($this['ID_MOBILIARIO']) ? $this['ID_MOBILIARIO'] : '',
            'NOM_MOBILIARIO' => isset($this['NOM_MOBILIARIO']) ? $this['NOM_MOBILIARIO'] : '',
            'TIPO_MOBILIARIO' => isset($this['TIPO_MOBILIARIO']) ? $this['TIPO_MOBILIARIO'] : '',
            'DESCR' => isset($this['DESCR']) ? $this['DESCR'] : '',
            'COOR_X' => isset($this['COOR_X']) ? $this['COOR_X'] : '',
            'COOR_Y' => isset($this['COOR_Y']) ? $this['COOR_Y'] : '',
            'MAT_ELEMENTO' => isset($this['MAT_ELEMENTO']) ? $this['MAT_ELEMENTO'] : '',
            'CAPACIDAD' => isset($this['CAPACIDAD']) ? $this['CAPACIDAD'] : '',
            'GRUPO_ETAREO' => isset($this['GRUPO_ETAREO']) ? $this['GRUPO_ETAREO'] : '',
            'USO_INCLUYENTE' => isset($this['USO_INCLUYENTE']) ? $this['USO_INCLUYENTE'] : '',
            'RIESGO_1' => isset($this['RIESGO_1']) ? $this['RIESGO_1'] : '',
            'RIESGO_2' => isset($this['RIESGO_2']) ? $this['RIESGO_2'] : '',
            'RIESGO_3' => isset($this['RIESGO_3']) ? $this['RIESGO_3'] : '',
            'RIESGO_4' => isset($this['RIESGO_4']) ? $this['RIESGO_4'] : '',
            'FECHA_CREACION' => isset($this['FECHA_CREACION']) ? $this->getDate($this['FECHA_CREACION']) : '',
            'FECHA_ACTUALIZACION' => isset($this['FECHA_ACTUALIZACION']) ? $this->getDate($this['FECHA_ACTUALIZACION']) : '',
            'NRO_PISO' => isset($this['NRO_PISO']) ? $this['NRO_PISO'] : '',
            'ESTADO_DIAG' => isset($this['ESTADO_DIAG']) ? $this['ESTADO_DIAG'] : '',
        ];
    }

    private function getDate($date): string
    {
        $timestamp = $date / 1000;
        $fecha = Carbon::createFromTimestamp($timestamp);
        $fechaFormateada = $fecha->format('Y-m-d');
        return $fechaFormateada;
    }
}
