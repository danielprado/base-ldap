<?php

namespace App\Modules\Contractors\src\Request;
use Illuminate\Foundation\Http\FormRequest;


class DocumentInitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'value_month' => 'required|numeric',
            'date_init' => 'required|string',
            'date_end' => 'required|string',
            'months_contract' => 'required|integer|min:0',
            'days_contract' => 'required|integer|min:0',
            'contract_id' => 'required|integer',
            'object_contract' => 'required|string',
            'subdirectorate_id' => 'required|integer|min:0',
            'dependency_id' => 'required|integer|min:0',
            'create_seven_contract' => 'required|boolean'
        ];
    }

}
