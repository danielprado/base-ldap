<?php

namespace App\Modules\Parks\src\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class FeatureEndowmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'UB_REFERENCIA' => $this['UB_REFERENCIA'],
            'NOMBRE_PARQ' => $this['NOMBRE_PARQ'],
            'ID_DOTACION' => $this['ID_DOTACION'],
            'NOM_DOTACION' => $this['NOM_DOTACION'],
            'TIPO_DOTACION' => $this['TIPO_DOTACION'],
            'DESCR' => $this['DESCR'],
            'COOR_X' => $this['COOR_X'],
            'COOR_Y' => $this['COOR_Y'],
            'RIESGO_1' => $this['RIESGO_1'],
            'RIESGO_2' => $this['RIESGO_2'],
            'RIESGO_3' => $this['RIESGO_3'],
            'RIESGO_4' => $this['RIESGO_4'],
            'MAT_PISO' => $this['MAT_PISO'],
            'AREA_JUEGO' => $this['AREA_JUEGO'],
            'CAPACIDAD' => $this['CAPACIDAD'],
            'ILUMINACION' => $this['ILUMINACION'],
            'CUBIERTO' =>  $this['CUBIERTO'],
            'CERRAMIENTO' =>  $this['CERRAMIENTO'],
            'TIPO_CERRAMIENTO_LAT' => $this['TIPO_CERRAMIENTO_LAT'],
            'TIPO_CERRAMIENTO_FRO' => $this['TIPO_CERRAMIENTO_FRO'],
            'CAMERINO' => $this['CAMERINO'],
            'SISTEMA_RIEGO' =>  $this['SISTEMA_RIEGO'],
            'USO_INCLUYENTE' => $this['USO_INCLUYENTE'],
            'FECHA_CREACION' => $this->getDate($this['FECHA_CREACION']),
            'FECHA_ACTUALIZACION' => $this->getDate($this['FECHA_ACTUALIZACION']),
            'NRO_PISO' => $this['NRO_PISO'],
            'ESTADO_DIAG' => $this['ESTADO_DIAG'],

        ];
    }

    private function getDate($date): string
    {
        $timestamp = $date / 1000;
        $fecha = Carbon::createFromTimestamp($timestamp);
        $fechaFormateada = $fecha->format('Y-m-d');
        return $fechaFormateada;
    }
}
