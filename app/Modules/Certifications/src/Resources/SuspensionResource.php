<?php

namespace App\Modules\Certifications\src\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SuspensionResource extends JsonResource
{
    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    public function toArray($request): array
    {
        return [
            'id' => isset($this->Id) ? (int)$this->Id : null,
            'contract_id' => isset($this->Contrato_Id) ? (int)$this->Contrato_Id : null,
            'number' => isset($this->Numero_Suspencion) ? (int)$this->Numero_Suspencion : null,
            'months' => isset($this->Meses) ? (int)$this->Meses : null,
            'days' => isset($this->Dias) ? (int)$this->Dias : null,
            'start_date' => $this->Fecha_Inicio ?? null,
            'final_date' => isset($this->Fecha_Fin) ? $this->Fecha_Fin->format('Y-m-d') : null,
            'restart_date' => $this->Fecha_Reinicio ?? null,
            'final_date_cto' => $this->Fecha_Fin_CTO ?? null,
            'created_at' => isset($this->created_at) ? $this->created_at->format('Y-m-d') : null,
            'updated_at' => isset($this->updated_at) ? $this->updated_at->format('Y-m-d') : null,
        ];
    }

    public static function headers(): array
    {
        return [
            [
                'text' => "#",
                'value'  =>  "id",
                'sortable' => false
            ],
            [
                'align' => "center",
                'text' => "Número",
                'value'  =>  "number",
                'sortable' => false
            ],
            [
                'align' => "center",
                'text' => "Meses",
                'value'  =>  "months",
                'sortable' => false
            ],
            [
                'align' => "right",
                'text' => "Días",
                'value'  =>  "days",
                'sortable' => false
            ],
            [
                'align' => "right",
                'text' => "Fecha de inicio",
                'value'  =>  "start_date",
                'sortable' => false
            ],
            [
                'align' => "right",
                'text' => "Fecha de finalización",
                'value'  =>  "final_date",
                'sortable' => false
            ],
            [
                'align' => "right",
                'text' => "Fecha de reinicio",
                'value'  =>  "restart_date",
                'sortable' => false
            ],
            [
                'align' => "right",
                'text' => "Fecha de finalización del contrato",
                'value'  =>  "final_date_cto",
                'sortable' => false
            ],
            [
                'align'     => "center",
                'text'      => "Acciones",
                'value'     =>  "actions",
                'sortable'  => false
            ],
        ];
    }
}
