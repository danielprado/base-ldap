<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBeneficiaryUnpaidcareTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_attendance')->create('attendee_unpaidcare', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('attendee_id')->unsigned();
            $table->bigInteger('unpaidcare_id')->unsigned();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendee_unpaidcare');
    }
}
