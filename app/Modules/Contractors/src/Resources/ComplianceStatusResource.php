<?php

namespace App\Modules\Contractors\src\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Modules\Contractors\src\Models\Compliance;
use App\Modules\Contractors\src\Models\PlanPayment;
use App\Modules\Contractors\src\Models\ActivityReport;

class ComplianceStatusResource extends JsonResource
{
    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    public function toArray($request)
    {
        return [
            'contract_id' => $this->id ?? null,
            'contract' => $this->contract ?? null,
            'plan'  => $this->plans->isEmpty() ? null : $this->plans[0],
            'compliance'  => $this->compliances->isEmpty() ? null : $this->compliances[0],
            'activity_report'  => $this->activityReports->isEmpty() ? null : $this->activityReports[0],
            'supervisor_status' => $this->getSupervisorStatus(),
            'auth_officer_status' => $this->getAuthorizingOfficerStatus(),
            'central_accounts_status' => $this->getCentralAccountsStatus(),
            'treasury_status' => $this->getTreasuryStatus()
        ];
    }

    function getSupervisorStatus() {
        if ($this->compliances->isEmpty() || $this->activityReports->isEmpty()){
            return Compliance::STATUS_PENDING;
        }
        $report = $this->activityReports[0];
        $compliance = $this->compliances[0];

        if ($report->state == ActivityReport::STATUS_PENDING || $compliance->status == Compliance::STATUS_PENDING){
            return Compliance::STATUS_PENDING;
        }
        if ($report->state == ActivityReport::STATUS_REJECTED || $compliance->status == Compliance::STATUS_REJECTED){
            return Compliance::STATUS_REJECTED;
        }
        if ($report->state == ActivityReport::STATUS_APPROVED && $compliance->status == Compliance::STATUS_APPROVED){
            return Compliance::STATUS_APPROVED;
        }
        return Compliance::STATUS_PENDING;
    }

    function getAuthorizingOfficerStatus() {
        if ($this->getSupervisorStatus() != Compliance::STATUS_APPROVED){
            return Compliance::STATUS_PENDING;
        }
        if ($this->plans->isEmpty()){
            return Compliance::STATUS_PENDING;
        }
        $plan = $this->plans[0];
        if ($plan->status == PlanPayment::STATUS_APPROVED){
            return Compliance::STATUS_PENDING;
        }
        if ($plan->status == PlanPayment::STATUS_PAYROLL || $plan->status == PlanPayment::STATUS_BINNACLE || $plan->status == PlanPayment::STATUS_INVOICING || $plan->status == PlanPayment::STATUS_PAYMENT){
            return Compliance::STATUS_APPROVED;
        }
        return Compliance::STATUS_PENDING;
    }

    function getCentralAccountsStatus() {
        if ($this->getAuthorizingOfficerStatus() != Compliance::STATUS_APPROVED){
            return Compliance::STATUS_PENDING;
        }
        $plan = $this->plans[0];
        if ($plan->status == PlanPayment::STATUS_APPROVED || $plan->status == PlanPayment::STATUS_BINNACLE){
            return Compliance::STATUS_PENDING;
        }
        if ($plan->status == PlanPayment::STATUS_INVOICING || $plan->status == PlanPayment::STATUS_PAYMENT){
            return Compliance::STATUS_APPROVED;
        }
        return Compliance::STATUS_PENDING;
    }

    function getTreasuryStatus() {
        if ($this->getCentralAccountsStatus() != Compliance::STATUS_APPROVED){
            return Compliance::STATUS_PENDING;
        }
        $plan = $this->plans[0];
        if ($plan->status == PlanPayment::STATUS_INVOICING){
            return Compliance::STATUS_PENDING;
        }
        if ($plan->status == PlanPayment::STATUS_PAYMENT){
            return Compliance::STATUS_APPROVED;
        }
        return Compliance::STATUS_PENDING;
    }
}
