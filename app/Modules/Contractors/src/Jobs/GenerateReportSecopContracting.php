<?php

namespace App\Modules\Contractors\src\Jobs;

use App\Modules\Contractors\src\Mail\ReportSecopContractingMail;
use App\Modules\Contractors\src\Mail\ReportSecopSevenMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use SendGrid\Mail\Mail;

class GenerateReportSecopContracting implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $to;
    private $file_name;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($email, $file_name)
    {
        $this->to = $email;
        $this->file_name = $file_name;
    }

    /**
     * Execute the job.
     *
     * @param Mail $mailer
     * @return void
     */
    public function handle(Mail $mailer)
    {
        $email = config('app.env') == 'local'
            ? env('SAMPLE_EMAIL', 'brayan.aldana@idrd.gov.co')
            : $this->to ?? null;

        $path = storage_path("app/exports/$this->file_name");
        $file_encoded = base64_encode(file_get_contents($path));
        $attachment = ['file' => $this->file_name];

        if ( isset($email)  && filter_var( $email, FILTER_VALIDATE_EMAIL) ) {
            $template = new ReportSecopContractingMail();
            $mailer->setFrom(
                env('MAIL_CONTRACTS_FROM', 'portal.contratista@idrd.gov.co'),
                env('MAIL_CONTRACTS_FROM_NAME', 'Portal Contratista')
            );
            $mailer->setSubject("REPORTE CONTRATOS SECOP - CONTRATACIÓN");
            $mailer->addTo($email);
            //$mailer->addTos($tos);
            $render = $template->render();
            $mailer->addContent("text/html", $render);
            $mailer->addAttachment(
                $file_encoded,
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                $this->file_name,
                "attachment"
            );
            $sendgrid = new \SendGrid(config('mail.sendgrid.api'));
            try {
                $response = $sendgrid->send($mailer);
                sendgird_logger(
                    $email,
                    "Reporte contratos secop para contratación",
                    $render,
                    $attachment,
                    $response->statusCode(),
                    $response->body()
                );
            } catch (\Exception $e) {
                sendgird_logger(
                    $email,
                    "[EXCEPTION] Reporte contratos secop para contratación",
                    $render,
                    $attachment,
                    $e->getCode(),
                    $e->getMessage()
                );
            }
        }
    }
}
