<?php

namespace App\Models\Security;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
     /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_sim';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ciudad';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id_Ciudad';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['Nombre_Ciudad' , 'Id_Pais'];
}
