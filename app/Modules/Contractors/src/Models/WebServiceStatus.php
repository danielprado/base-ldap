<?php

namespace App\Modules\Contractors\src\Models;

use Illuminate\Database\Eloquent\Model;

class WebServiceStatus extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = "mysql_contractors";

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'webservice_status';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'url',
        'request_type',
        'request',
        'status',
        'response',
        'user_id',
        'user'
    ];

    protected $casts = [
        'request' => 'array',
        'response' => 'array',
    ];
}
