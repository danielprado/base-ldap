<?php


namespace App\Modules\Contractors\src\Mail;


use App\Modules\Contractors\src\Models\Contractor;
use App\Modules\Contractors\src\Models\Payroll;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Crypt;

class AccountingCreationPayrollMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Payroll
     */
    private $payroll;

    /**
     * Create a new job instance.
     *
     * @param Payroll $payroll
     */
    public function __construct(Payroll $payroll)
    {
        $this->payroll = $payroll;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $id = isset( $this->payroll->id ) ? $this->payroll->id : '';
        $created_at = isset( $this->payroll->created_at ) ? $this->payroll->created_at->format('Y-m-d H:i:s') : '';
        $subdirectorate = isset($this->payroll->subdirectorate) ? $this->payroll->subdirectorate->name : '';
        $number = str_pad($this->payroll->id, 4, '0', STR_PAD_LEFT);
        $type = $this->payroll->reserve ? 'RESERVA' : 'VIGENCIA';

        $path = config('app.env') != 'production'
            ? "/portal-contratista-dev"
            : "/";
        $subdomain = config('app.env') != 'production' ? "sim" : "portalcontratista";
        $url = "https://{$subdomain}.idrd.gov.co{$path}";

        return $this->view('mail.mail')
            ->subject("Creación Orden de pago planilla {$this->payroll['id']} Portal Contratista")
            ->with([
                'slogan'    => 'Notificación Planillas IDRD',
                'header'    => 'IDRD',
                'title'     => 'Planillas Portal Contratista',
                'content'   =>  "Se ha realizado la creación de pago planilla  {$this->payroll['id']} para su respectivo trámite",
                'details'   =>  "
                        <p>Número de Registro: {$id}</p>
                        <p>Subdirección: {$subdirectorate}</p>
                        <p>Nombre de la planilla: {$number} {$subdirectorate}</p>
                        <p>Tipo de planilla: {$type}</p>
                        <p>Fecha de Registro: {$created_at}</p>
                        ",
                // 'hide_btn'  => true,
                'btn_text'  =>  "Ver",
                'url'       =>  "$url" . "/es/planillas",
                // 'info'      =>  "Se ha habilitado el reporte de CAUSACIÓN y de PAC para su respectiva validación y verificación",
                'year'      =>  Carbon::now()->year
            ]);
    }
}
