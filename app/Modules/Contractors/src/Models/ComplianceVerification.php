<?php

namespace App\Modules\Contractors\src\Models;

use Illuminate\Database\Eloquent\Model;

class ComplianceVerification extends Model
{
    protected $connection = 'mysql_contractors';

    protected $table = 'compliance_verifications';

    protected $fillable = [
        'code',
        'status',
        'start',
        'end'
    ];

    public function verifyCode(string $code)
    {
        if (!($this->code == $code)) {
            return false;
        }

        $this->status = 'REDEEMED';
        $this->save();

        return true;
    }
}
