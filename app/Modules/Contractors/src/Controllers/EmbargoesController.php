<?php


namespace App\Modules\Contractors\src\Controllers;


use App\Http\Controllers\Controller;
use App\Models\Security\User;
use App\Modules\Contractors\src\Constants\Roles;
use App\Modules\Contractors\src\imports\EmbargoImport;
use App\Modules\Contractors\src\Jobs\ConfirmContractor;
use App\Modules\Contractors\src\Jobs\ConfirmUpdateStatusContractor;
use App\Modules\Contractors\src\Models\Contract;
use App\Modules\Contractors\src\Models\Contractor;
use App\Modules\Contractors\src\Models\Embargo;
use App\Modules\Contractors\src\Notifications\ArlUpdateNotification;
use App\Modules\Contractors\src\Request\EmbargoExcelRequest;
use App\Modules\Contractors\src\Request\StoreEmbargoRequest;
use App\Modules\Contractors\src\Request\StoreLawyerContractRequest;
use App\Modules\Contractors\src\Request\UpdateEmbargoRequest;
use App\Modules\Contractors\src\Resources\ContractorResource;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Throwable;

class EmbargoesController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }


    /**
     * Display the specified resource.
     *
     * @param Contractor $contractor
     * @return JsonResponse
     */
    public function show(Contractor $contractor)
    {
        return $this->success_response(
            new ContractorResource($contractor->load('embargoes'))
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreEmbargoRequest $request
     * @param Contractor $contractor
     * @return JsonResponse
     */
    public function store(StoreEmbargoRequest $request, Contractor $contractor): JsonResponse
    {
        try {
            DB::connection('mysql_contractors')->beginTransaction();
            $contractor->embargoed = true;
            $contractor->save();
            $contractor->embargoes()
                ->save(new Embargo(
                    array_merge(
                        $request->validated(),
                        ['document' => $contractor->document],
                        ['name' => $contractor->full_name]
                    )
                ));
            DB::connection('mysql_contractors')->commit();
            return $this->success_message(__('validation.handler.success'), Response::HTTP_CREATED);
        } catch (\Throwable $e) {
            DB::connection('mysql_contractors')->rollBack();
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreEmbargoRequest $request
     * @param Contractor $contractor
     * @return JsonResponse
     */
    public function update(StoreEmbargoRequest $request, Contractor $contractor): JsonResponse
    {
        try {
            DB::connection('mysql_contractors')->beginTransaction();
            $embargo = $contractor->embargoes()->findOrFail($request->get('id'));
            $embargo->update($request->validated());
            DB::connection('mysql_contractors')->commit();
            return $this->success_message(__('validation.handler.updated'));
        } catch (Throwable $e) {
            DB::connection('mysql_contractors')->rollBack();
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }

    /**
     * @param UpdateEmbargoRequest $request
     * @param Contractor $contractor
     * @return JsonResponse
     */
    public function embargoed(UpdateEmbargoRequest $request, Contractor $contractor): JsonResponse
    {
        try {
            $contractor->embargoed = $request->get('embargo');
            $contractor->saveOrFail();
            return $this->success_message(__('validation.handler.updated'));
        } catch (Throwable $e) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }

    /**
     * @return JsonResponse
     */
    public function formatFile(): JsonResponse
    {
        $filePath = storage_path("app/templates/formato_embargos.xlsx");
        if (file_exists($filePath)) {
            $fileContents = file_get_contents($filePath);
            $base64File = base64_encode($fileContents);
            return $this->success_message([
                'file_name' => "formato_embargos_" . now()->format('Y-m-d') .".xlsx",
                'file' => 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,' . $base64File
            ]);
        }

        abort(Response::HTTP_NOT_FOUND, __('validation.handler.resource_not_found_url'));
    }

    public function upload(EmbargoExcelRequest $request): JsonResponse
    {
        try {
            DB::connection('mysql_contractors')->beginTransaction();

            Excel::import(new EmbargoImport, $request->file('excel'));

            DB::connection('mysql_contractors')->commit();
            return $this->success_message(
                __('validation.handler.success'),
                Response::HTTP_OK,
                Response::HTTP_OK
            );
        } catch (Throwable $e) {
            DB::connection('mysql_contractors')->rollBack();
            return $this->error_response(
                $e->getMessage()
            );
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Embargo $embargo
     * @return JsonResponse
     */
    public function delete(Embargo $embargo): JsonResponse
    {
        try {
            DB::connection('mysql_contractors')->beginTransaction();
            $embargo->delete();
            DB::connection('mysql_contractors')->commit();
            return $this->success_message(__('validation.handler.deleted'));
        } catch (Throwable $e) {
            DB::connection('mysql_contractors')->rollBack();
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }
}
