<?php

namespace App\Modules\Contractors\src\Jobs;

use App\Modules\Contractors\src\Mail\ComplianceReminderMail;
use App\Modules\Contractors\src\Models\Contractor;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use SendGrid\Mail\Mail;

class ComplianceReminder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var int
     */
    private $complianceFirstDay;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(int $complianceFirstDay)
    {
        $this->complianceFirstDay = $complianceFirstDay;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mail $mailer)
    {
        $contractors = Contractor::with(['contracts'])
            ->whereHas('compliances', function($query) {
                $query->where('updated_at', '<', Carbon::create(now()->year, now()->month, $this->complianceFirstDay));
            })
            ->orWhereDoesntHave('compliances')
            ->get();
            
        foreach ($contractors as $contractor) {
            $this->sendMail($mailer, $contractor);
        }
    }

    private function sendMail(Mail $mailer, $contractor) {
        $email = config('app.env') == 'local'
            ? env('SAMPLE_EMAIL', 'andres.tellez@idrd.gov.co')
            : (isset($contractor->email) ? $contractor->email : null);

        if (isset($email) && filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $mailer->setFrom(
                env('MAIL_CONTRACTS_FROM', 'portal.contratista@idrd.gov.co'),
                env('MAIL_CONTRACTS_FROM_NAME', 'Portal Contratista')
            );
            $mailer->setSubject('Recordatorio de Certificado de Cumplimiento');
            $mailer->addTo($email);
    
            foreach ($contractor->contracts as $contract) {
                $render = (new ComplianceReminderMail($contractor, $contract))
                    ->render();
    
                $mailer->addContent("text/html", $render);
                $sendgrid = new \SendGrid(config('mail.sendgrid.api'));
    
                try {
                    $response = $sendgrid->send($mailer);
                    sendgird_logger(
                        $email,
                        "Recordatorio de Certificado de Cumplimiento",
                        $render,
                        [],
                        $response->statusCode(),
                        $response->body()
                    );
                } catch (\Exception $e) {
                    sendgird_logger(
                        $email,
                        "[EXCEPTION] Recordatorio de Certificado de Cumplimiento",
                        $render,
                        [],
                        $e->getCode(),
                        $e->getMessage()
                    );
                }
            }
        }
    }
}
