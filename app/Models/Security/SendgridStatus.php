<?php

namespace App\Models\Security;

use Illuminate\Database\Eloquent\Model;

class SendgridStatus extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = "mysql_ldap";

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'sendgrid_status';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'to',
        'subject',
        'body',
        'status',
        'response',
        'attachment',
    ];

    protected $casts = [
        'attachment' => 'array',
    ];
}
