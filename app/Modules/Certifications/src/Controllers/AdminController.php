<?php

namespace App\Modules\Certifications\src\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Resources\Auth\RoleResource;
use App\Http\Resources\Auth\UserResource;
use App\Models\Security\User;
use App\Modules\Certifications\src\Constants\Roles;
use App\Modules\Certifications\src\Request\RoleRequest;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Silber\Bouncer\Database\Role;

class AdminController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function index(): JsonResponse
    {
        $users = User::whereIs(...Roles::all())
            ->with([
                'roles' => function ($query) {
                    return $query->whereIn('name', Roles::all());
                }
            ])
            ->get();

        return $this->success_response(
            UserResource::collection($users)
        );
    }

    public function roles(): JsonResponse
    {
        return $this->success_response(
            RoleResource::collection(Role::query()->whereIn('name', Roles::all())->get())
        );
    }

    public function find(Request $request): JsonResponse
    {
        $users = User::search($request->get('username'))
            ->take(50)
            ->get();

        return $this->success_response(
            UserResource::collection($users)
        );
    }

    public function store(RoleRequest $request, User $user): JsonResponse
    {
        $user->assign($request->get('roles'));
        return $this->success_message(
            __('validation.handler.success'),
            Response::HTTP_CREATED
        );
    }

    public function destroy(RoleRequest $request, User $user): JsonResponse
    {
        $user->retract($request->get('roles'));
        return $this->success_message(
            __('validation.handler.success'),
            Response::HTTP_CREATED
        );
    }
}
