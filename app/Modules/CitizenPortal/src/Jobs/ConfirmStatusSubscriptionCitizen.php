<?php


namespace App\Modules\CitizenPortal\src\Jobs;


use App\Modules\CitizenPortal\src\Mail\NotificationSubscriptionMail;
use App\Modules\CitizenPortal\src\Models\Profile;
use App\Modules\CitizenPortal\src\Models\ProfileView;
use App\Modules\CitizenPortal\src\Models\Schedule;
use App\Modules\CitizenPortal\src\Models\Status;
use App\Modules\CitizenPortal\src\Notifications\SendgridTemplates;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use SendGrid\Mail\Mail;

class ConfirmStatusSubscriptionCitizen implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Profile
     */
    private $user;

    /**
     * @var Status
     */
    private $status;

    /**
     * @var string
     */
    private $test_mail;

    /**
     * @var string
     */
    private $observation;
    /**
     * @var Schedule
     */
    private $schedule;

    /**
     * Create a new job instance.
     *
     * @param Profile $profile
     * @param Status $status
     * @param $observation
     * @param null $test_mail
     */
    public function __construct(Profile $profile, Status $status, Schedule $schedule, $observation, $test_mail = null)
    {
        $this->user = $profile;
        $this->status = $status;
        $this->observation = $observation;
        $this->test_mail = $test_mail;
        $this->schedule = $schedule;
    }

    /**
     * Execute the job.
     *
     * @param Mail $mailer
     * @return void
     */
    public function handle(Mail $mailer)
    {
        $email = isset( $this->user->user->email ) ? (string) $this->user->user->email : null;
        if (config('app.env') != 'production') {
            $email = $this->test_mail
                ?? explode(',', env('SAMPLE_CITIZEN_PORTAL_EMAIL', 'daniel.prado@idrd.gov.co'));
        }
        if ( $email  && filter_var( $email, FILTER_VALIDATE_EMAIL) ) {
            $subject = "Portal Ciudadano - Estado de Inscripción";
            $notify = new SendgridTemplates(true);
            $name = $this->user->profile_type_id != Profile::PROFILE_PERSONAL
                ? $this->user->user->user_profile->email_full_name ?? "Ciudadano"
                : "Ciudadano";

            $body = [
                "name"      => $name,
                "sub_user"  => "{$this->user->email_full_name}",
                "sport"     => $this->schedule->getKey()." - ".toFirstUpper($this->schedule->activities->name ?? ""),
                "stage"     => toFirstUpper($this->schedule->stage->name ?? ""),
                "hour"     => toFirstUpper($this->schedule->day->name ?? "")." ".toUpper($this->schedule->hour->name ?? ""),
                "status"    =>  "{$this->status->name}",
                "observation" => $this->observation,
                "info"  =>  "Le invitamos a ingresar al Portal Ciudadano para conocer más servicios que el IDRD tiene para usted."
            ];

            switch ($this->status->getKey()) {
                case Status::RETURNED:
                case Status::UNSUBSCRIBED:
                    $notify->sendErrorMessage(
                        $email,
                        $body,
                        "$subject"
                    );
                    break;
                case Status::VERIFIED:
                case Status::SUBSCRIBED:
                    $notify->sendSuccessMessage(
                        $email,
                        $body,
                        "$subject"
                    );
                    break;
                default:
                    $notify->sendInfoMessage(
                        $email,
                        $body,
                        "$subject"
                    );
                    break;
            }
        }
    }
}
