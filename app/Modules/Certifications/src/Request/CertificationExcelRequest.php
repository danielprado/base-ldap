<?php

namespace App\Modules\Certifications\src\Request;

class CertificationExcelRequest extends \Illuminate\Foundation\Http\FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'solved'        =>  'required_without_all:contract,document,unsolved|string',
            'unsolved'      =>  'required_without_all:contract,document,solved|string',
            'start_date'    =>  'nullable|date|before_or_equal:final_date',
            'final_date'    =>  'nullable|date|after_or_equal:start_date',
            'contract'      =>  'nullable|numeric',
            'document'      =>  'nullable|numeric',
        ];
    }

    public function attributes()
    {
        return [
            'start_date'    => 'fecha inicial',
            'final_date'    => 'fecha final',
            'contract'      =>  'número de contrato',
            'document'      =>  'número de documento',
            'solved'        =>  'soportes solucionados',
            'unsolved'      =>  'soportes sin solucionar',
        ];
    }
}
