<?php

namespace App\Modules\Contractors\src\Request;

use App\Modules\Contractors\src\Constants\Contributions;
use App\Modules\Contractors\src\Models\Compliance;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreComplianceContractorRequest extends FormRequest
{
    private $compliance;

    protected function prepareForValidation()
    {
        $compliance = Compliance::query()
            ->whereHas('contract', function ($query) {
                $query->where('contract', $this->input('contract', '0000'));
            })
            ->where('period_month', $this->input('period_month', 0))
            ->where('period_year', $this->input('period_year', 0))
            ->firstOrFail();

        $this->compliance = $compliance;

        $dependentOrOptionalFields = [
            'income_tax_filer'          => 'withholding_value',
            'afc_value'                 => 'afc_value',
            'afp_voluntary_value'       => 'afp_voluntary_value',
            'children_minor'            => 'children_minor_files',
            'children_mayor_students'   => 'children_mayor_students_files',
            'children_disabilities'     => 'children_disabilities_files',
            'spouse'                    => 'spouse_files',
            'parents_dependency'        => 'parents_dependency_files',
            'housing_interests'         => 'housing_interests_file',
            'prepaid_medicine'          => 'prepaid_medicine_file'
        ];

        foreach ($dependentOrOptionalFields as $parent => $dependent) {
            if ($this->shouldRequireContribution($parent)) {
                $this->request->set($dependent, $this->input($dependent));
            }
        }

        if ($this->input('pensioner')) $this->request->set('afp_value', null);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'document'                      => 'required|numeric|digits_between:3,12',
            'contract'                      => 'required|string|max:100',
            'period_month'                  => 'required|numeric',
            'period_year'                   => 'required|numeric|digits:4|date_format:Y',
            'contract_id'                   => 'required|numeric',

            'pensioner'                     => 'nullable|boolean',
            'income_tax_filer'              => 'nullable|boolean',
            'responsible_iva'               => 'nullable|boolean',
            'withholding'                   => 'nullable|boolean',
            'withholding_value'             => 'required_if:withholding,1|prohibited_if_not:withholding|nullable',

            'housing_interests'             => 'nullable|boolean',
            'third_people'                  => 'required_if:housing_interests,1|prohibited_if_not:housing_interests|nullable|boolean',
            'spouse_tax'                    => 'required_if:housing_interests,1|prohibited_if_not:housing_interests|nullable|boolean',
            'percentage'                    => 'required_if:housing_interests,1|nullable|string',
            'retention'                     => 'nullable|boolean',
            'prepaid_medicine'              => 'nullable|boolean',
            'has_children'                  => 'nullable|boolean',
            'children_number'               => 'required_if:has_children,1|prohibited_if_not:has_children|nullable|numeric|digits_between:1,12',
            'children_minor'                => 'required_if:has_children,1|prohibited_if_not:has_children|nullable|boolean',
            'children_mayor_students'       => 'required_if:has_children,1|prohibited_if_not:has_children|nullable|boolean',
            'children_disabilities'         => 'required_if:has_children,1|prohibited_if_not:has_children|nullable|boolean',
            'spouse'                        => 'nullable|boolean',
            'parents_dependency'            => 'nullable|boolean',
            'afp_voluntary'                 => 'nullable|boolean',
            'afp_voluntary_value'           => 'required_if:afp_voluntary,1|prohibited_if_not:afp_voluntary|nullable|numeric',
            'afc_voluntary'                 => 'nullable|boolean',
            'afc_value'                     => 'required_if:afc_voluntary,1|prohibited_if_not:afc_voluntary|nullable|numeric',

            //files
            'pensioner_file'                => 'required_if:pensioner,1|prohibited_if_not:pensioner|nullable|file|mimes:pdf',
            'afp_voluntary_file'            => 'required_if:afp_voluntary,1|prohibited_if_not:afp_voluntary|nullable|file|mimes:pdf',
            'afc_voluntary_file'            => 'required_if:afc_voluntary,1|prohibited_if_not:afc_voluntary|nullable|file|mimes:pdf',
            'housing_interests_files'       => 'required_if:housing_interests,1|prohibited_if_not:housing_interests|nullable|file|mimes:pdf',
            'prepaid_medicine_file'         => 'required_if:prepaid_medicine,1|prohibited_if_not:prepaid_medicine|nullable|file|mimes:pdf',
            'children_minor_files'          => 'required_if:children_minor,1|prohibited_if_not:children_minor|nullable|file|mimes:pdf',
            'children_mayor_students_files' => 'required_if:children_mayor_students,1|prohibited_if_not:children_mayor_students|nullable|file|mimes:pdf',
            'children_disabilities_files'   => 'required_if:children_disabilities,1|prohibited_if_not:children_disabilities|nullable|file|mimes:pdf',
            'spouse_files'                  => 'required_if:spouse,1|prohibited_if_not:spouse|nullable|file|mimes:pdf',
            'parents_dependency_files'      => 'required_if:parents_dependency,1|prohibited_if_not:parents_dependency|nullable|file|mimes:pdf',
        ];
    }

    private function shouldRequireContribution($name)
    {
        if ($this->compliance->isFirstTime()) return true;
        return $this->compliance->contribution($name)->isValid();
    }

    public function attributes()
    {
        return [
            'document' => 'documento',
            'contract' => 'contrato',
            'period_month' => 'mes a certificar',
            'period_year' => 'año a certificar',
            'contract_id' => 'identificación del contrato',

            'pensioner' => 'pensionado',
            'income_tax_filer' => 'declarante de impuesto sobre la renta',
            'responsible_iva' => 'responsable del IVA',
            'withholding' => 'retención',
            'withholding_value' => 'valor de la retención',

            'housing_interests' => 'intereses de vivienda',
            'third_people' => 'a nombre de terceros',
            'spouse_tax' => 'impuesto del cónyuge',
            'percentage' => 'porcentaje',
            'retention' => 'retención',
            'prepaid_medicine' => 'medicina prepagada',
            'has_children' => 'tiene dependientes',
            'children_number' => 'número de dependientes',
            'children_minor' => 'hijos menores',
            'children_mayor_students' => 'hijos mayores estudiantes',
            'children_disabilities' => 'hijos con discapacidades',
            'spouse' => 'cónyuge',
            'parents_dependency' => 'dependencia de los padres',
            'afp_voluntary' => 'AFP voluntario',
            'afp_voluntary_value' => 'valor de AFP voluntario',
            'afc_voluntary' => 'AFC voluntario',
            'afc_value' => 'valor de AFC voluntario',

            // archivos
            'pensioner_file' => 'archivo de pensionado',
            'afp_voluntary_file' => 'archivo de AFP voluntario',
            'afc_voluntary_file' => 'archivo de AFC voluntario',
            'housing_interests_files' => 'archivos de intereses de vivienda',
            'prepaid_medicine_file' => 'archivo de medicina prepagada',
            'children_minor_files' => 'archivos de hijos menores',
            'children_mayor_students_files' => 'archivos de hijos mayores estudiantes',
            'children_disabilities_files' => 'archivos de hijos con discapacidades',
            'spouse_files' => 'archivos del cónyuge',
            'parents_dependency_files' => 'archivos de dependencia de los padres',
        ];
    }

    public function messages()
    {
        return [
            'document.required' => 'El campo documento es obligatorio.',
            'document.numeric' => 'El campo documento debe ser numérico.',
            'document.digits_between' => 'El campo documento debe tener entre :min y :max dígitos.',

            'contract.required' => 'El campo contrato es obligatorio.',
            'contract.string' => 'El campo contrato debe ser una cadena de texto.',
            'contract.max' => 'El campo contrato no puede tener más de :max caracteres.',

            'period_month.required' => 'El campo mes a certificar es obligatorio.',
            'period_month.numeric' => 'El campo mes a certificar debe ser numérico.',

            'period_year.required' => 'El campo año a certificar es obligatorio.',
            'period_year.numeric' => 'El campo año a certificar debe ser numérico.',
            'period_year.digits' => 'El campo año a certificar debe tener :digits dígitos.',
            'period_year.date_format' => 'El campo año a certificar no coincide con el formato :format.',

            'contract_id.required' => 'El campo identificación del contrato es obligatorio.',
            'contract_id.numeric' => 'El campo identificación del contrato debe ser numérico.',

            'withholding_value.required_if' => 'El campo valor de la retención es obligatorio cuando el campo retención es verdadero.',
            'withholding_value.prohibited_if_not' => 'El campo valor de la retención no puede estar presente a menos que el campo retención adicional esté presente.',

            'third_people.required_if' => 'El campo a nombre de terceros es obligatorio cuando el campo intereses de vivienda es verdadero.',
            'spouse_tax.required_if' => 'El campo impuesto del cónyuge es obligatorio cuando el campo intereses de vivienda es verdadero.',
            'percentage.required_if' => 'El campo porcentaje es obligatorio cuando el campo intereses de vivienda es verdadero.',

            'children_number.required_if' => 'El campo número de dependientes es obligatorio cuando el campo tiene dependientes es verdadero.',
            'children_number.prohibited_if_not' => 'El campo número de dependientes no puede estar presente a menos que el campo tiene dependientes esté presente.',
            'children_minor.required_if' => 'El campo hijos menores es obligatorio cuando el campo tiene dependientes es verdadero.',
            'children_minor.prohibited_if_not' => 'El campo hijos menores no puede estar presente a menos que el campo tiene dependientes esté presente.',
            'children_mayor_students.required_if' => 'El campo hijos mayores estudiantes es obligatorio cuando el campo tiene dependientes es verdadero.',
            'children_mayor_students.prohibited_if_not' => 'El campo hijos mayores estudiantes no puede estar presente a menos que el campo tiene dependientes esté presente.',
            'children_disabilities.required_if' => 'El campo hijos con discapacidades es obligatorio cuando el campo tiene dependientes es verdadero.',
            'children_disabilities.prohibited_if_not' => 'El campo hijos con discapacidades no puede estar presente a menos que el campo tiene dependientes esté presente.',

            'afp_voluntary_value.required_if' => 'El campo valor de AFP voluntario es obligatorio cuando el campo AFP voluntario es verdadero.',
            'afp_voluntary_value.prohibited_if_not' => 'El campo valor de AFP voluntario no puede estar presente a menos que el campo AFP voluntario esté presente.',
            'afc_value.required_if' => 'El campo valor de AFC voluntario es obligatorio cuando el campo AFC voluntario es verdadero.',
            'afc_value.prohibited_if_not' => 'El campo valor de AFC voluntario no puede estar presente a menos que el campo AFC voluntario esté presente.',

            'pensioner_file.required_if' => 'El campo archivo de pensionado es obligatorio cuando el campo pensionado es verdadero.',
            'pensioner_file.prohibited_if_not' => 'El campo archivo de pensionado no puede estar presente a menos que el campo pensionado esté presente.',
            'afp_voluntary_file.required_if' => 'El campo archivo de AFP voluntario es obligatorio cuando el campo AFP voluntario es verdadero.',
            'afp_voluntary_file.prohibited_if_not' => 'El campo archivo de AFP voluntario no puede estar presente a menos que el campo AFP voluntario esté presente.',
            'afc_voluntary_file.required_if' => 'El campo archivo de AFC voluntario es obligatorio cuando el campo AFC voluntario es verdadero.',
            'afc_voluntary_file.prohibited_if_not' => 'El campo archivo de AFC voluntario no puede estar presente a menos que el campo AFC voluntario esté presente.',
            'housing_interests_files.required_if' => 'El campo archivos de intereses de vivienda es obligatorio cuando el campo intereses de vivienda es verdadero.',
            'housing_interests_files.prohibited_if_not' => 'El campo archivos de intereses de vivienda no puede estar presente a menos que el campo intereses de vivienda esté presente.',
            'prepaid_medicine_file.required_if' => 'El campo archivo de medicina prepagada es obligatorio cuando el campo medicina prepagada es verdadero.',
            'prepaid_medicine_file.prohibited_if_not' => 'El campo archivo de medicina prepagada no puede estar presente a menos que el campo medicina prepagada esté presente.',
            'children_minor_files.required_if' => 'El campo archivos de hijos menores es obligatorio cuando el campo hijos menores es verdadero.',
            'children_minor_files.prohibited_if_not' => 'El campo archivos de hijos menores no puede estar presente a menos que el campo hijos menores esté presente.',
            'children_mayor_students_files.required_if' => 'El campo archivos de hijos mayores estudiantes es obligatorio cuando el campo hijos mayores estudiantes es verdadero.',
            'children_mayor_students_files.prohibited_if_not' => 'El campo archivos de hijos mayores estudiantes no puede estar presente a menos que el campo hijos mayores estudiantes esté presente.',
            'children_disabilities_files.required_if' => 'El campo archivos de hijos con discapacidades es obligatorio cuando el campo hijos con discapacidades es verdadero.',
            'children_disabilities_files.prohibited_if_not' => 'El campo archivos de hijos con discapacidades no puede estar presente a menos que el campo hijos con discapacidades esté presente.',
            'spouse_files.required_if' => 'El campo archivos del cónyuge es obligatorio cuando el campo cónyuge es verdadero.',
            'spouse_files.prohibited_if_not' => 'El campo archivos del cónyuge no puede estar presente a menos que el campo cónyuge esté presente.',
            'parents_dependency_files.required_if' => 'El campo archivos de dependencia de los padres es obligatorio cuando el campo dependencia de los padres es verdadero.',
            'parents_dependency_files.prohibited_if_not' => 'El campo archivos de dependencia de los padres no puede estar presente a menos que el campo dependencia de los padres esté presente.',
        ];
    }

}
