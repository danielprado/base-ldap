<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableComplianceAddContractNumberColumn extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('compliance', function (Blueprint $table) {
            $table->string('contract_number', 50)
                  ->nullable()
                  ->default(null)
                  ->collation('utf8mb4_unicode_ci')
                  ->after('name_reviewer_technical');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('compliance', function (Blueprint $table) {
            $table->dropColumn('contract_number');
        });
    }
}
