<?php

namespace App\Modules\Contractors\src\Models;

use App\Models\Security\Area;
use App\Models\Security\Subarea;
use App\Models\Security\Subdirectorate;
use App\Models\Security\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use OwenIt\Auditing\Contracts\Auditable;

/**
 * Contract Model
 *
 * @property int id
 * @property string contract
 * @property bool transport
 * @property string position
 * @property Carbon|string start_date
 * @property Carbon|string final_date
 * @property Carbon|string start_suspension_date
 * @property Carbon|string final_suspension_date
 * @property int total
 * @property float duration
 * @property string day
 * @property int risk
 * @property int subdirectorate_id
 * @property int dependency_id
 * @property int subarea_id
 * @property string other_dependency_subdirectorate
 * @property string supervisor_email
 * @property int contractor_id
 * @property int contract_type_id
 * @property int lawyer_id
 * @property Carbon|string created_at
 * @property Carbon|string updated_at
 */
class Contract extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    const NEW_CONTRACT = 1;    // CONTRATO NUEVO
    const CONTRACT_ADD = 2;    // ADICIÓN DE CONTRATO
    const CONTRACT_SUSPENSION = 3;    // SUSPENSIÓN DE CONTRATO
    const CONTRACT_EXTENSION = 4;    // PRÓRROGA DEL CONTRATO
    const CONTRACT_ASSIGNMENT = 5;    // CESIÓN DE CONTRATO
    const CONTRACT_ADD_AND_EXT = 6;    // ADICIÓN Y PRÓRROGA
    const EARLY_TERMINATION = 7;    // TERMINACIÓN ANTICIPADA

    const CYCLE_PATH_GUARDIAN_DEP = 113; // DEPENDENCIA DE GUARDIANES DE CICLOVÍA

    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'mysql_contractors';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'contracts';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'contract',
        'transport',
        'position',
        'start_date',
        'final_date',
        'start_suspension_date',
        'final_suspension_date',
        'total',
        'day',
        'risk',
        'subdirectorate_id',
        'dependency_id',
        'other_dependency_subdirectorate',
        'supervisor_email',
        'contractor_id',
        'contract_type_id',
        'lawyer_id',
        'duration',
        'duration_days',
        'object_contract',
        'subarea_id',
        'total_addition',
        'consecutive'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'transport'  => 'boolean',
        'risk'      => 'int',
        'subdirectorate_id'      => 'int',
        'dependency_id'      => 'int',
        'subarea_id'      => 'int',
        'contractor_id'    => 'int',
        'contract_type_id' => 'int',
        'lawyer_id' => 'int',
        'duration' => 'int',
        'duration_days' => 'int',
        'count' => 'int',
        'month' => 'int'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['start_date', 'final_date', 'start_suspension_date', 'final_suspension_date'];

    /*
    * ---------------------------------------------------------
    * Data Change Auditor
    * ---------------------------------------------------------
    */

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [
        'contract',
        'transport',
        'position',
        'start_date',
        'final_date',
        'start_suspension_date',
        'final_suspension_date',
        'total',
        'day',
        'risk',
        'subdirectorate_id',
        'dependency_id',
        'subarea_id',
        'other_dependency_subdirectorate',
        'supervisor_email',
        'contractor_id',
        'contract_type_id',
        'lawyer_id',
        'duration',
        'duration_days',
        'total_addition'
    ];

    /**
     * Generating tags for each model audited.
     *
     * @return array
     */
    public function generateTags(): array
    {
        return ['contractors_contract'];
    }

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator
     * ---------------------------------------------------------
     */

    /**
     * Set value in uppercase
     *
     * @param $value
     */
    public function setContractAttribute($value)
    {
        $this->attributes['contract'] = toUpper($value);
    }

    /**
     * Set value in uppercase
     *
     * @param $value
     */
    public function setPositionAttribute($value)
    {
        $this->attributes['position'] = toUpper($value);
    }

    /**
     * Set value in uppercase
     *
     * @param $value
     */
    public function setDayAttribute($value)
    {
        $this->attributes['day'] = implode(', ', $value);
    }

    /**
     * Set value in uppercase
     *
     * @param $value
     */
    public function setOtherDependencySubdirectorateAttribute($value)
    {
        $this->attributes['other_dependency_subdirectorate'] = toUpper($value);
    }

    /**
     * Set value in uppercase
     *
     * @param $value
     */
    public function getDayAttribute($value)
    {
        return explode(', ', $value);
    }

    /*
     * ---------------------------------------------------------
     * Query Scopes
     * ---------------------------------------------------------
    */
    public function scopeExtractYears($query)
    {
        $years = array();
        $contracts = $query->select(DB::raw("SUBSTRING(contracts.contract, 15) as extractYear"))
            ->groupBy('extractYear')
            ->orderBy('extractYear', 'asc')
            ->get();
        foreach ($contracts as $contract) {
            $years[] = (int) $contract['extractYear'];
        }
        return $years;
    }
    public function scopeNewsByYear($query)
    {
        $new = array();
        $contracts = $query->select(DB::raw("(COUNT(*)) AS count"), DB::raw("SUBSTRING(contracts.contract, 15) as extractYear"))
            ->where('contract_type_id', 1)
            ->groupBy('extractYear')
            ->orderBy('extractYear', 'asc')
            ->get();
        foreach ($contracts as $contract) {
            $new[] = $contract['count'];
        }
        return $new;
    }
    public function scopeAdditionByYear($query)
    {
        $add = array();
        $contracts = $query->select(DB::raw("(COUNT(*)) AS count"), DB::raw("SUBSTRING(contracts.contract, 15) as extractYear"))
            ->where('contract_type_id', 2)
            ->groupBy('extractYear')
            ->orderBy('extractYear', 'asc')
            ->get();
        foreach ($contracts as $contract) {
            $add[] = $contract['count'];
        }
        return $add;
    }
    public function scopeSuspensionByYear($query)
    {
        $sus = array();
        $contracts = $query->select(DB::raw("(COUNT(*)) AS count"), DB::raw("SUBSTRING(contracts.contract, 15) as extractYear"))
            ->where('contract_type_id', 3)
            ->groupBy('extractYear')
            ->orderBy('extractYear', 'asc')
            ->get();
        foreach ($contracts as $contract) {
            $sus[] = $contract['count'];
        }
        return $sus;
    }
    public function scopeExtensionByYear($query)
    {
        $pro = array();
        $contracts = $query->select(DB::raw("(COUNT(*)) AS count"), DB::raw("SUBSTRING(contracts.contract, 15) as extractYear"))
            ->where('contract_type_id', 4)
            ->groupBy('extractYear')
            ->orderBy('extractYear', 'asc')
            ->get();
        foreach ($contracts as $contract) {
            $pro[] = $contract['count'];
        }
        return $pro;
    }
    public function scopeAssignmentByYear($query)
    {
        $ces = array();
        $contracts = $query->select(DB::raw("(COUNT(*)) AS count"), DB::raw("SUBSTRING(contracts.contract, 15) as extractYear"))
            ->where('contract_type_id', 5)
            ->groupBy('extractYear')
            ->orderBy('extractYear', 'asc')
            ->get();
        foreach ($contracts as $contract) {
            $ces[] = $contract['count'];
        }
        return $ces;
    }


    /*
     * ---------------------------------------------------------
     * Eloquent Relations
     * ---------------------------------------------------------
    */

    public function subdirectorate()
    {
        return $this->belongsTo(Subdirectorate::class);
    }

    public function dependency()
    {
        return $this->belongsTo(Area::class);
    }

    public function subarea()
    {
        return $this->belongsTo(Subarea::class);
    }

    public function contractor()
    {
        return $this->belongsTo(Contractor::class);
    }

    public function contract_type()
    {
        return $this->belongsTo(ContractType::class);
    }


    public function lawyer()
    {
        return $this->belongsTo(User::class, 'lawyer_id');
    }

    public function files()
    {
        return $this->hasMany(File::class, 'contract_id');
    }

    public function document_init()
    {
        return $this->hasOne(DocumentInit::class, 'contract_id', 'id');
    }

    public function activityReports()
    {
        return $this->hasMany(ActivityReport::class, 'contract_number', 'contract');
    }

    public function compliances()
    {
        return $this->hasMany(Compliance::class, 'contract_number', 'contract');
    }

    public function plans()
    {
        return $this->hasMany(PlanPayment::class, 'contract_id', 'id');
    }

}
