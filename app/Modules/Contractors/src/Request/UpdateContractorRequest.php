<?php


namespace App\Modules\Contractors\src\Request;


use App\Models\Security\Area;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateContractorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'  =>  'required|string|min:3|max:191',
            'surname'   =>  'required|string|min:3|max:191',
            'birthdate' =>  'required|date|date_format:Y-m-d',
            'birthdate_country_id'  =>  'required|numeric|exists:mysql_ldap.countries,id',
            'birthdate_state_id'    =>  'required|numeric|exists:mysql_ldap.states,id',
            'birthdate_city_id'     =>  'required|numeric|exists:mysql_ldap.cities,id',
            'sex_id' =>  'required|numeric|exists:mysql_sim.genero,Id_Genero',
            'gender_id' => 'required|numeric|exists:mysql_sim.identidad_de_genero,id',
            'orientation_id' =>  'required|numeric|exists:mysql_sim.lgbti,id',
            'population_id' =>  'required|numeric|exists:mysql_sim.social_poblacional,id',
            'ethnic_id' =>  'required|numeric|exists:mysql_sim.etnia,Id_Etnia',
            'disability_id' =>  'required|numeric|exists:mysql_sim.discapacidad,id',
            'person_type' => 'required|string',
            'marital_status' => 'required|string',
            'email' =>  'required|email',
            'institutional_email'   =>  'nullable|email',
            'phone'   =>  'required|numeric',
            'eps_id'    =>  'required|numeric|exists:mysql_ldap.eps,id',
            'eps'   =>  'required_if:eps_id,62|string|nullable|min:3|max:191',
            'afp_id'    =>  'required|numeric|exists:mysql_ldap.afp,id',
            'afp'   =>  'required_if:afp_id,10|string|nullable|min:3|max:191',
            'residence_country_id'  =>  'required|numeric|exists:mysql_ldap.countries,id',
            'residence_state_id'    =>  'required|numeric|exists:mysql_ldap.states,id',
            'residence_city_id' =>  'required|numeric|exists:mysql_ldap.cities,id',
            'locality_id'   =>  'required|numeric|sometimes',
            'upz_id'    =>  'required|numeric|sometimes',
            'neighborhood_id'   =>  'required|numeric|sometimes',
            'neighborhood'  =>  'string|nullable|min:3|max:191',
            'address'  =>  'string|required|min:3|max:191',
            'blood_type_id' =>  'required|numeric|exists:mysql_sim.grupo_sanguineo,Id_GrupoSanguineo',
            'economic_activity_id'  =>  'required|numeric|exists:mysql_contractors.economic_activities,id',
            'contact_name'  =>  'string|required|min:3|max:90',
            'contact_phone'   =>  'required|numeric',
            'contact_relationship'  =>  'string|required|min:3|max:90',
            //'other_relationship'  =>  'string|min:3|max:90|required_if:contact_relationship,OTRO',
            // Contract
            'contract_id' =>  'required|numeric|exists:mysql_contractors.contracts,id',
            'transport' =>  'required|boolean',
            'position'  =>  'required|string',
            'day'   =>  'required|array',
            'risk'  =>  'required|numeric|between:1,5',
            'subdirectorate_id'    =>  'required|numeric|exists:mysql_ldap.subdirectorates,id',
            'dependency_id'    =>  'required|numeric|exists:mysql_ldap.areas,id',
            'subarea_id'    =>  'nullable|numeric|exists:mysql_ldap.subareas,id',
            'other_dependency_subdirectorate'    =>  [
                Rule::requiredIf(function () {
                    $area = Area::query()->where('id', $this->get('dependency_id'))->first();
                    return isset($area->name) && $area->name === 'OTRO';
                }),
                'nullable',
                'string',
            ],
            'supervisor_email'  =>  'nullable|email',
            'academic_level_id'  =>  'required|numeric|exists:mysql_contractors.academic_level,id',
            'career_id'  =>  'required|numeric|exists:mysql_contractors.careers,id',
            'graduate'  =>  'required|boolean',
            'year_approved'  =>  'nullable|numeric',
            'rut'   =>  'required|file|mimes:pdf',
            'bank'  =>  'required|file|mimes:pdf',
            'photo'   =>  'required|image|mimes:jpeg,png,jpg',
            'bank_id'  =>  'required|numeric|exists:mysql_contractors.banks,id',
            'account_type_id'  =>  'required|numeric|exists:mysql_contractors.account_types,id',
            'number'   => [
                'required',
                'numeric',
                function ($attribute, $value, $fail) {
                    $bankId = $this->input('bank_id');
                    $bankAccountDigits = getBankAccountDigits();

                    if (isset($bankAccountDigits[$bankId]) && $bankAccountDigits[$bankId] !== null) {
                        $expectedLength = $bankAccountDigits[$bankId];
                        if (strlen($value) !== $expectedLength) {
                            $fail("El número de cuenta bancaria debe tener $expectedLength dígitos para el banco seleccionado.");
                        }
                    }
                },
            ],
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'document_type_id'  =>  'tipo de documento',
            'document'  =>  'número de documento',
            'name'  =>  'required|string|min:3|max:191',
            'surname'   =>  'required|string|min:3|max:191',
            'birthdate' =>  'fecha de nacimiento',
            'birthdate_country_id'  =>  'país de nacimiento',
            'birthdate_state_id'    =>  'departamaneto de nacimiento',
            'birthdate_city_id' =>  'ciudad de nacimiento',
            'sex_id' =>  'sexo',
            'gender_id' => 'identidad de género',
            'orientation_id' => 'orientacion sexual',
            'population_id' => 'grupo poblacional',
            'ethnic_id' => 'grupo etnico',
            'disability_id' => 'discapacidad',
            'person_type' => 'tipo de persona',
            'marital_status' => 'estado civil',
            'email' =>  'correo personal',
            'institutional_email'   =>  'correo institucional',
            'phone'   =>  'teléfono de contácto',
            'eps_id'    =>  'eps',
            'eps'    =>  'otra eps',
            'afp_id'    =>  'fondo de pensiones',
            'afp'    =>  'otro fondo de pensiones',
            'residence_country_id'  =>  'país de residencia',
            'residence_state_id'    =>  'departamaneto de residencia',
            'residence_city_id' =>  'ciudad de residencia',
            'locality_id'   =>  'localidad',
            'upz_id'    =>  'upz',
            'neighborhood_id'   =>  'barrio',
            'neighborhood'  =>  'otro nombre del bario',
            'address'  =>  'dirección',
            // Contrato
            'contract_id'    =>  'contrato',
            'contract_type'    =>  'tipo de trámite',
            'transport' =>  'se suministra transporte',
            'contract'    =>  'contrato',
            'position'  =>  'cargo a desempeñar',
            'start_date'    =>  'fecha de inicio del contrato',
            'final_date'    =>  'fecha de terminación del contrato',
            'total' =>  'valor del contrato o adición',
            'day'   =>  'día que no trabaja',
            'risk'  =>  'nivel de riesgo',
            'subdirectorate_id'    =>  'subdirección',
            'dependency_id'    =>  'dependencia',
            'subarea_id'    =>  'área',
            'other_dependency_subdirectorate'  =>  'otra dependencia o subdirección',
            'supervisor_email'  =>  'correo del supervisor',
            'academic_level_id'  =>  'nivel académico',
            'career_id'  =>  'título académico',
            'graduate'  =>  'graduado',
            'year_approved'  =>  'último semestre o año aprobado',
            'rut'  =>  'certificado rut',
            'bank'  =>  'certificación bancaria',
            'bank_id' => 'nombre del banco',
            'account_type_id' => 'tipo de cuenta bancaria',
            'number' => 'número de la cuenta bancaria',
            'economic_activity_id'  =>  'código de la actividad económica',
        ];
    }

    /**
     * Get the validator instance for the request.
     *
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function getValidatorInstance(){
        $validator = parent::getValidatorInstance();

        $validator->sometimes('locality_id', 'exists:mysql_parks.localidad,Id_Localidad', function($input)
        {
            return $input->locality_id != 9999;
        });

        $validator->sometimes('upz_id', 'exists:mysql_parks.upz,Id_Upz', function($input)
        {
            return $input->upz_id != 9999;
        });

        $validator->sometimes('neighborhood_id', 'exists:mysql_parks.Barrios,IdBarrio', function($input)
        {
            return $input->neighborhood_id != 9999;
        });

        return $validator;
    }
}
