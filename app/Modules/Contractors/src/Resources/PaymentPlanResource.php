<?php


namespace App\Modules\Contractors\src\Resources;


use App\Models\Security\Subdirectorate;
use App\Models\Security\User;
use App\Modules\Contractors\src\Constants\Roles;
use App\Modules\Contractors\src\Models\Contract;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;


class PaymentPlanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $contract = Contract::with(['subdirectorate', 'dependency', 'contractor'])->find($this->contract_id);
        $supervisor = User::query()->where('id', $this->supervisor_id)->first();
        $pmr = collect($this->pmrs)->first();
        return [
            'id'                    => isset($this->id) ? (int) $this->id : null,
            'activity_report_id'    => isset($this->activity_report_id) ? (int) $this->activity_report_id : null,
            'compliance_id'         => isset($this->compliance_id) ? (int) $this->compliance_id : null,
            'status'                => $this->getStatus() ?? null,
            'sub_address_name'      => isset($contract->subdirectorate->name) ? $contract->subdirectorate->name : null,
            'area_name'             => isset($contract->dependency->name) ? $contract->dependency->name : null,
            'contractor_name'       => isset($contract->contractor->full_name) ? $contract->contractor->full_name : null,
            'contractor_document'   => isset($contract->contractor->document) ? $contract->contractor->document : null,
            'contract'              => $this->contract ?? null,
            'period'                => $this->getPeriod() ?? null,
            'amount_days_pay'       => isset($this->amount_day_pay) ? (int) $this->amount_day_pay : null,
            'total_payment'         => isset($this->total_payment) ? $this->applyMoneyFormat($this->total_payment) : null,
            'start_day'             => isset($this->start_day) ? (int) $this->start_day : null,
            'end_day'               => isset($this->final_day) ? (int) $this->final_day : null,
            'supervisor_name'       => isset($supervisor) ? $supervisor->full_name : null,
            'approval_date'         => isset($this->approval_date) ? Carbon::parse($this->approval_date)->format('Y-m-d') : null,
            'binnacle'              => isset($pmr) ? (int) $pmr->binnacle_bir_numo : null,
            'cycle_path_guardian'   => isset($this->cycle_path_guardian) ? (bool) $this->cycle_path_guardian : null
        ];
    }

    public function getStatus(): string
    {
        switch ($this->status) {
            case 'APPROVED':
                return 'APROBADO';
            case 'PAYROLL':
                return 'AGREGADO';
            case 'BINNACLE':
                return 'CON BITÁCORA';
            case 'INVOICING':
                return 'CAUSADA';
            case 'PAYMENT':
                return 'PAGADA';
            default:
                return 'PENDIENTE';
        }
    }
    public function getPeriod(): string
    {
        $date = Carbon::create(null, $this->month);
        Carbon::setLocale('es');
        return toUpper($date->isoFormat('MMMM')) . ' - ' . $this->year;
    }
    private function applyMoneyFormat($value)
    {
        return $value
            ? preg_replace('/\s+/', '', cop_money_format($value, '$', null, 0, null, '.'))
            : null;
    }

    public static function headers()
    {
        return [
            [
                'text' => "No.",
                'value'  =>  "id",
                'sortable'  =>  false,
            ],
            [
                'align' => "center",
                'text' => "Estado",
                'value'  =>  "status",
            ],
            [
                'align' => "center",
                'text' => "Informe de Actividades",
                'value'  =>  "activity_report_id",
            ],
            [
                'align' => "center",
                'text' => "Certificado Cumplimiento Tributario",
                'value'  =>  "compliance_id",
            ],
            [
                'align' => "center",
                'text' => "Bitácora",
                'value'  =>  "binnacle",
            ],
            [
                'text' => "Área",
                'value'  =>  "area_name",
            ],
            [
                'text' => "Nombre",
                'value'  =>  "contractor_name",
            ],
            [
                'text' => "Documento",
                'value'  =>  "contractor_document",
            ],
            [
                'align' => "center",
                'text' => "Contrato",
                'value'  =>  "contract",
            ],
            [
                'align' => "center",
                'text' => "Periodo",
                'value'  =>  "period",
            ],
            [
                'align' => "center",
                'text' => "Días a pagar",
                'value'  =>  "amount_days_pay",
            ],
            [
                'align' => "center",
                'text' => "Día incial",
                'value'  =>  "start_day",
            ],
            [
                'align' => "center",
                'text' => "Día final",
                'value'  =>  "end_day",
            ],
            [
                'align' => "center",
                'text' => "Valor a pagar",
                'value'  =>  "total_payment",
            ],
            [
                'text' => "Fecha de aprobación",
                'value'  =>  "approval_date",
            ],
            [
                'align' => "end",
                'text' => "Nombre del supervisor",
                'value'  =>  "supervisor_name",
            ],
        ];
    }
}
