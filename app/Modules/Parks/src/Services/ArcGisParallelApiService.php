<?php

namespace App\Modules\Parks\src\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Promise\Utils;
use App\Modules\Parks\src\Services\Base\BaseExternalApiService;

class ArcGisParallelApiService
{
    public function get($urls){
        $client = new Client();
        $promises = [];
        foreach ($urls as $url) {
            $promises[] = $client->getAsync($url);
        }
        Utils::unwrap($promises);
        $results = Utils::settle($promises)->wait();
        return $results;
    }


}
