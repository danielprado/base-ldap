<?php
namespace App\Modules\InfoApp\src\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Event extends Model
{
    use SoftDeletes;

    protected $connection = 'mysql_infoapp';

    protected $fillable = [
        'nombre',
        'nombre2',
        'lugar',
        'resumen',
        'descripcion',
        'link',
        'fecha_fin',
        'imagen',
        'fecha',
        'hora',
        'categoria_id',
        'tipo',
        'destacado',
        'tipo_poblacion',
        'entrada',
        'localidad',
        'latitud',
        'longitud',
    ];

    protected $dates = ['deleted_at'];

    public function categoria()
    {
        return $this->belongsTo('App\Models\Category', 'categoria_id');
    }
}