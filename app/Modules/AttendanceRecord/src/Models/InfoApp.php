<?php

namespace App\Modules\AttendanceRecord\src\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InfoApp extends Model
{
      //
      use SoftDeletes;
      protected $connection = 'mysql_attendance';
      protected $table = 'infoApp';
      protected $dates = ['deleted_at'];
      protected $fillable = [
            'id',
            'age',
            'genero',
            'preferencias',
            'localidad',
            'authorize',
      ];
}
