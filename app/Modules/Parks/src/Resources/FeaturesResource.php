<?php

namespace App\Modules\Parks\src\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class FeaturesResource extends JsonResource
{

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' =>  isset( $this->id ) ? (int) $this->id : null,
            'name' =>  isset( $this->Nombre ) ? $this->Nombre : null,

        ];
    }
}
