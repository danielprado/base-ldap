<?php

namespace App\Modules\PaymentGateway\src\Exports;

use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;

class BookingsExport implements FromCollection, WithHeadings, WithMapping, WithTitle
{
    use Exportable;

    /**
     * @var array
     */
    private $array;
    public function __construct($collection)
    {
        $this->array = $collection;
    }

    public function collection()
    {
        return $this->array;
    }

    public function headings(): array
    {
        return [
            'ID',
            'FECHA RESERVA',
            'TOTAL',
            'HORA INICIO',
            'HORA FIN',
            'DURACIÓN',
            'RANGO HORAS',
            'ID SERVICIO',
            'NOMBRE SERVICIO',
            'CÓDIGO SERVICIO',
            'DESCRIPCIÓN SERIVICIO',
            'NOMBRE PARQUE',
            'DIRECCION PARQUE',
            'ESTRATO',
            'ID EQUIPAMIENTO',
            'TIPO EQUIPAMIENTO',
            'NOMBRE EQUIPAMIENTO',
            'CONCEPTO',
            'PERMISO',
            'TIPO PERMISO',
            'PAGO TOTAL',
            'ESTADO PAGO',
            'TIPO DOCUMENTO',
            'DOCUMENTO',
            'EMAIL PAGADOR',
            'NOMBRE PAGADOR',
            'APELLIDO PAGADOR',
            'TELÉFONO PAGADOR',
            'ID USUARIO PSE',
            'CÓDIGO PAGO',
            'ID PSE',
            'FECHA CREACIÓN',
        ];
    }

    public function map($row): array
    {
        return [
            'id'                        => $row['id'] ?? null,
            'fecha reserva'             => $row['date'] ?? null,
            'total'                     => $row['total'] ?? null,
            'hora inicio'               => $row['start_hour'] ?? null,
            'hora fin'                  => $row['final_hour'] ?? null,
            'duracion'                  => $this->setRangeHoursForHumans($row['start_hour'], $row['final_hour']),
            'rango horas'               => $row['hour_range'] ?? null,
            'id servicio'               => $row['service_id'] ?? null,
            'nombre servicio'           => $row['service_name'] ?? null,
            'codigo servicio'           => $row['service_code'] ?? null,
            'descripcion servicio'      => $row['name'] ?? null,
            'nombre parque'             => $row['park'] ?? null,
            'direccion parque'          => $row['address'] ?? null,
            'estrato'                   => $row['stratum'] ?? null,
            'id equipamiento'           => $row['endowment_id'] ?? null,
            'tipo equipamiento'         => $row['endowment_type'] ?? null,
            'nombre equipamiento'       => $row['equipment'] ?? null,
            'concepto'                  => $row['concept'] ?? null,
            'permiso'                   => $row['permission'] ?? null,
            'tipo permiso'              => $row['permission_type'] ?? null,
            'pago total'                => $row['payer_total'] ?? null,
            'estado pago'               => $row['status_bank'] ?? null,
            'tipo documento'            => $row['document_type'] ?? null,
            'documento'                 => $row['document'] ?? null,
            'email pagador'             => $row['payer_email'] ?? null,
            'nombre pagador'            => $row['payer_name'] ?? null,
            'apellido pagador'          => $row['payer_surname'] ?? null,
            'telefono pagador'          => $row['payer_phone'] ?? null,
            'id usuario pse'            => $row['pse_user_id'] ?? null,
            'codigo pago'               => $row['payment_code'] ?? null,
            'id trasaccion pse'         => $row['pse_transaction_id'] ?? null,
            'FECHA CREACIÓN'            => $row['created_at'] ?? null,
        ];
    }

    public function title(): string
    {
        return 'Reservas';
    }

    public function setRangeHoursForHumans($start_time, $end_time): ? string
    {
        $start = Carbon::createFromTimeString($start_time);
        $end = Carbon::createFromTimeString($end_time);
        $diff = $end->longAbsoluteDiffForHumans($start);
        return $diff ?? null;
    }
}
