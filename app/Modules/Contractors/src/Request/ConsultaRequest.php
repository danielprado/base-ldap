<?php


namespace App\Modules\Contractors\src\Request;


use Illuminate\Foundation\Http\FormRequest;
class ConsultaRequest extends FormRequest{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function rules(){
        return ["document"=>"required|numeric", "year"=>"required|numeric",
        "mesini"=>"nullable|required_if:type,ICA,IVA,CON|numeric|lt:mesfin|between:1,12",
        "mesfin"=>"nullable|required_if:type,ICA,IVA,CON|numeric|gt:mesini|between:1,12",
        "type"=>"nullable|string"];
    }
}
