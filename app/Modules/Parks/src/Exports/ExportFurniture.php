<?php

namespace App\Modules\Parks\src\Exports;

use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class ExportFurniture implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{

    use Exportable;

    protected $allContents;

    public function __construct($allContents = null)
    {
        $this->allContents = $allContents;
    }

    public function collection()
    {
        return $this->allContents;
    }

    public function map($allContents): array
    {
        return [
            $allContents['UB_REFERENCIA'] ??  '',
            $allContents['NOMBRE_PARQ'] ??  '',
            $allContents['ID_MOBILIARIO'] ??  '',
            $allContents['NOM_MOBILIARIO'] ??  '',
            $allContents['TIPO_MOBILIARIO'] ??  '',
            $allContents['DESCR'] ??  '',
            $allContents['COOR_X'] ??  '',
            $allContents['COOR_Y'] ??  '',
            $allContents['MAT_ELEMENTO'] ??  '',
            $allContents['CAPACIDAD'] ??  '',
            $allContents['GRUPO_ETAREO'] ??  '',
            $allContents['USO_INCLUYENTE'] ??  '',
            $allContents['RIESGO_1'] ??  '',
            $allContents['RIESGO_2'] ??  '',
            $allContents['RIESGO_3'] ??  '',
            $allContents['RIESGO_4'] ??  '',
            $this->getDate($allContents['FECHA_CREACION'])  ??  '',
            $this->getDate($allContents['FECHA_ACTUALIZACION'])  ??  '',
            $allContents['NRO_PISO'] ??  '',
            $allContents['ESTADO_DIAG'] ??  '',
        ];
    }

    public function headings(): array
    {
        return [
            'CODIGO PARQUE',
            'NOMBRE PARQUE',
            'ID MOBILIARIO',
            'NOMBRE MOBILIARIO',
            'TIPO',
            'DESCRIPCION',
            'COORDENADA X',
            'COORDENADA Y',
            'MATERIAL ELEMENTO',
            'CAPACIDAD',
            'GRUPO ETAREO',
            'USO_INCLUYENTE',
            'RIESGO 1',
            'RIESGO 2',
            'RIESGO 3',
            'RIESGO 4',
            'FECHA_CREACION',
            'FECHA_ACTUALIZACION',
            'N DE PISOS',
            'ESTADO DIAGNOSTICO',
        ];
    }

    private function getDate($date): string{
        $timestamp = $date / 1000;
        $fecha = Carbon::createFromTimestamp($timestamp);
        $fechaFormateada = $fecha->format('Y-m-d');
        return $fechaFormateada;
    }
}
