<?php

namespace App\Modules\Certifications\src\Jobs;

use App\Models\Security\User;
use App\Modules\Certifications\src\Mail\ReportMail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Imtigger\LaravelJobStatus\JobStatus;
use SendGrid\Mail\Mail;

class NotifyExportReport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $file;

    /**
     * @var $url
     */
    private $url;

    /**
     * Create a new job instance.
     *
     * @param User $user
     * @param $file
     * @param $url
     */
    public function __construct(User $user, $file, $url)
    {
        $this->user = $user;
        $this->file = $file;
        $this->url = $url;
    }


    public function handle(Mail $mailer)
    {
        $job = JobStatus::query()->where('key', $this->file)->first();
        if (isset($job->key)) {
            $job->status = 'exported';
            $job->finished_at = now();
            $job->save();
        }

        $email = env('APP_ENV') == 'local'
            ? env('SAMPLE_EMAIL', 'brayan.aldana@idrd.gov.co')
            : $this->user->email ?? null;

        $path = storage_path("app/exports/$this->file");
        $file_encoded = base64_encode(file_get_contents($path));
        $attachment = ['file' => $this->file];

        if ( isset( $email )  && filter_var( $email, FILTER_VALIDATE_EMAIL) ) {
            $subject = "SOLICITUD REPORTE - $this->file";
            $template = new ReportMail($this->user, $this->file, $this->url);
            $mailer->setFrom(
                env('MAIL_FROM_ADDRESS', 'mails@idrd.gov.co'),
               'CERTIFICACIÓN DE CONTRATOS'
            );
            $mailer->setSubject($subject);
            $mailer->addTo($email);
            $render = $template->render();
            $mailer->addContent("text/html", $render);
            $mailer->addAttachment(
                $file_encoded,
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                $this->file,
                "attachment"
            );
            $sendgrid = new \SendGrid(env('SENDGRID_API_KEY'));
            try {
                $response = $sendgrid->send($mailer);
                sendgird_logger(
                    $email,
                    $subject,
                    $render,
                    $attachment,
                    $response->statusCode(),
                    $response->body()
                );
            } catch (\Exception $e) {
                sendgird_logger(
                    $email,
                    "[EXCEPTION] $subject",
                    $render,
                    $attachment,
                    $e->getCode(),
                    $e->getMessage()
                );
            }
        }
    }
}
