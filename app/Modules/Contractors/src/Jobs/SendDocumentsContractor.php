<?php


namespace App\Modules\Contractors\src\Jobs;


use App\Modules\Contractors\src\Mail\ComplianceRejectedMail;
use App\Modules\Contractors\src\Mail\ContractorLegalMail;
use App\Modules\Contractors\src\Mail\ContractorMail;
use App\Modules\Contractors\src\Mail\ContractorSendArlMail;
use App\Modules\Contractors\src\Mail\ContractorSendDocumentsMail;
use App\Modules\Contractors\src\Mail\ContractorUpdateMail;
use App\Modules\Contractors\src\Models\ActivityReport;
use App\Modules\Contractors\src\Models\Contract;
use App\Modules\Contractors\src\Models\Contractor;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use SendGrid\Mail\Mail;
use SendGrid\Mail\To;

class SendDocumentsContractor implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Contractor
     */
    private $user;

    /**
     * @var string
     */
    private $compliance_file;

    /**
     * @var string
     */
    private $activity_report_file;

    /**
     * @var ActivityReport
     */
    private $activity_report;

    /**
     * @var string
     */
    private $filed_number;

    /**
     * Create a new job instance.
     *
     * @param Contractor $user
     * @param string $compliance_file
     * @param string $activity_report_file
     * @param ActivityReport $activity_report
     * @param string $filed_number
     */
    public function __construct(Contractor $user, string $compliance_file, string $activity_report_file, ActivityReport $activity_report, string $filed_number)
    {
        $this->user = $user;
        $this->compliance_file = $compliance_file;
        $this->activity_report_file = $activity_report_file;
        $this->activity_report = $activity_report;
        $this->filed_number = $filed_number;
    }

    /**
     * Execute the job.
     *
     * @param Mail $mailer
     // * @return void
     */
    public function handle(Mail $mailer)
    {
        $emails = [];

        if (isset($this->user->email) && filter_var($this->user->email, FILTER_VALIDATE_EMAIL)) {
            $emails[] = new To($this->user->email);
        }

        /*if (isset($this->user->institutional_email) && filter_var($this->user->institutional_email, FILTER_VALIDATE_EMAIL)) {
            $emails[] = new To($this->user->institutional_email);
        }*/

        $filed_number = $this->filed_number ?? 'No se encontró un número de radicado';

        $file_name = $this->compliance_file ?? "No se encontró certificado de cumplimiento tributario";
        $path = storage_path("app/contractor/{$file_name}");
        $file_encoded = base64_encode(file_get_contents($path));

        $file_name_activity = $this->activity_report_file ?? "No se encontró informe de actividades";
        $path_activity = storage_path("app/contractor/{$file_name_activity}");
        $file_activity_encoded = base64_encode(file_get_contents($path_activity));

        $file_name_security_social = $this->activity_report->social_security_attachment_path ?? "No se encontró archivo de seguridad social";
        $path_security_social = storage_path('app/activity-reports/'.$this->activity_report->contract_number.'/'.$this->activity_report->social_security_attachment_path);
        $file_security_social_encoded = base64_encode(file_get_contents($path_security_social));

        $file_name_instructive = "INSTRUCTIVO_CARGUE_SECOP_II.pdf";
        $path_instructive = storage_path('app/templates/'.$file_name_instructive);
        $file_instructive_encoded = base64_encode(file_get_contents($path_instructive));

        $attachment = [
            'file' => $file_name,
            'file_activity' => $file_name_activity,
            'file_security' => $file_name_security_social,
            'instructive' => $file_name_instructive
        ];

        if (!empty($emails)) {
            $template = new ContractorSendDocumentsMail($this->user, $this->compliance_file, $this->activity_report_file, $this->activity_report, $filed_number);
            $mailer->setFrom(
                env('MAIL_CONTRACTS_FROM', 'portal.contratista@idrd.gov.co'),
                env('MAIL_CONTRACTS_FROM_NAME', 'Portal Contratista')
            );
            $mailer->setSubject('Documentos aprobados por el supervisor');
            $mailer->addTos($emails);
            $render = $template->render();
            $mailer->addContent("text/html", $render);
            $mailer->addAttachment(
                $file_encoded,
                "application/pdf",
                $file_name,
                "attachment"
            );
            $mailer->addAttachment(
                $file_activity_encoded,
                "application/pdf",
                $file_name_activity,
                "attachment"
            );
            $mailer->addAttachment(
                $file_security_social_encoded,
                "application/pdf",
                $file_name_security_social,
                "attachment"
            );
            $mailer->addAttachment(
                $file_instructive_encoded,
                "application/pdf",
                $file_name_instructive,
                "attachment"
            );

            $sendgrid = new \SendGrid(config('mail.sendgrid.api'));

            try {
                $response = $sendgrid->send($mailer);
                foreach ($emails as $email) {
                    sendgird_logger(
                        $email->getEmail(),
                        "Documentos aprobados por el supervisor",
                        $render,
                        $attachment,
                        $response->statusCode(),
                        $response->body()
                    );
                }
            } catch (\Exception $e) {
                foreach ($emails as $email) {
                    sendgird_logger(
                        $email->getEmail(),
                        "[EXCEPTION] Documentos aprobados por el supervisor",
                        $render,
                        $attachment,
                        $e->getCode(),
                        $e->getMessage()
                    );
                }
            }
        }
    }
}
