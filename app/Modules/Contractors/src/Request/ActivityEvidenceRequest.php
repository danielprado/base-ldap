<?php


namespace App\Modules\Contractors\src\Request;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ActivityEvidenceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'activity_report_id'  =>  'required|numeric',
            'sequence'  =>  'required|string',
            'obligation_text'  =>  'required|string',
            'evidence_text'  =>  'required|string',
            'evidence_link'  =>  ''
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'activity_report_id'  =>  'id reporte',
            'sequence'  =>  'secuencia',
            'obligation_text'  =>  'descripción obligacion',
            'evidence_text'  =>  'descripción evidencia',
            'evidence_link'  =>  'link de evidencia',
            'support'  =>  'soporte evidencia'
        ];
    }
}
