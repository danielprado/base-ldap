<?php

namespace App\Modules\Contractors\src\Models;

use Illuminate\Database\Eloquent\Model;

class SurveyResponse extends Model
{

    protected $connection = 'mysql_contractors';
    /**
     * La tabla asociada con el modelo.
     *
     * @var string
     */
    protected $table = 'survey_responses';

    protected $primaryKey = 'id';

    /**
     * Los atributos que se pueden asignar de forma masiva.
     *
     * @var array
     */
    protected $fillable = [
        'contractor_id',
        'payment_changes',
        'timely_payment',
        'tool_instructions',
        'tool_video',
        'tool_infographic',
        'tool_support_desk',
        'information_clarity',
        'payment_satisfaction',
        'recommendations',
    ];

}
