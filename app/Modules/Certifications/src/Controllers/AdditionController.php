<?php

namespace App\Modules\Certifications\src\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Certifications\src\Models\CertificationContract;
use App\Modules\Certifications\src\Request\AdditionRequest;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class AdditionController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function store(AdditionRequest $request, CertificationContract $contract): JsonResponse
    {
        $contract->additions()
            ->create([
                'Numero_Adicion' => $request->get('number'),
                'Valor_Adicion' => $request->get('value')
            ]);

        return $this->success_message(
            __('validation.handler.success'),
            Response::HTTP_CREATED
        );
    }

    public function update(AdditionRequest $request, CertificationContract $contract): JsonResponse
    {
        $addition = $contract->additions()
            ->where('Id', $request->get('id'))
            ->firstOrFail();

        $addition->update([
            'Numero_Adicion' => $request->get('number'),
            'Valor_Adicion' => $request->get('value')
        ]);

        return $this->success_message(__('validation.handler.updated'));
    }

    /**
     * @throws Exception
     */
    public function destroy(CertificationContract $contract, $id): JsonResponse
    {
        if ($id) {
            $contract->additions()->find($id)->delete();
            $order = 1;
            foreach ($contract->additions as $addition) {
                $addition->Numero_Adicion = $order;
                $addition->update();
                $order++;
            }
            return $this->success_message(__('validation.handler.deleted'));
        }
        return $this->error_response(__('validation.handler.unexpected_failure'));
    }
}
