<?php


namespace App\Modules\Contractors\src\Request;


use App\Models\Security\Area;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UpdateContractorAccountBankRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'rut'   =>  'required|file|mimes:pdf',
            'bank'  =>  'required|file|mimes:pdf',
            'bank_id'  =>  'required|numeric|exists:mysql_contractors.banks,id',
            'account_type_id'  =>  'required|numeric|exists:mysql_contractors.account_types,id',
            'number'   => [
                'required',
                'numeric',
                function ($attribute, $value, $fail) {
                    $bankId = $this->input('bank_id');
                    $bankAccountDigits = getBankAccountDigits();

                    if (isset($bankAccountDigits[$bankId]) && $bankAccountDigits[$bankId] !== null) {
                        $expectedLength = $bankAccountDigits[$bankId];
                        if (strlen($value) !== $expectedLength) {
                            $fail("El número de cuenta bancaria debe tener $expectedLength dígitos para el banco seleccionado.");
                        }
                    }
                },
            ],
            'economic_activity_id'  =>  'required|numeric|exists:mysql_contractors.economic_activities,id',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'rut'  =>  'certificado rut',
            'bank'  =>  'certificación bancaria',
            'bank_id' => 'nombre del banco',
            'account_type_id' => 'tipo de cuenta bancaria',
            'number' => 'número de la cuenta bancaria',
            'economic_activity_id' => 'código de la actividad económica',
        ];
    }
}
