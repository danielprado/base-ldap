<?php


namespace App\Modules\Contractors\src\Jobs;


use App\Modules\Contractors\src\Mail\ContractorMail;
use App\Modules\Contractors\src\Mail\ContractorUpdateMail;
use App\Modules\Contractors\src\Models\Contractor;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use SendGrid\Mail\Mail;

class ConfirmUpdateContractor implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Contractor
     */
    private $user;

    /**
     * Create a new job instance.
     *
     * @param Contractor $user
     */
    public function __construct(Contractor $user)
    {
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @param Mail $mailer
     * @return void
     */
    public function handle(Mail $mailer)
    {
        $template = new ContractorUpdateMail( $this->user );
        $contract = $this->user->contracts()->latest()->first();
        $number = isset($contract->contract) ? $contract->contract : '000';
        $type = isset($contract->contract_type->name) ? $contract->contract_type->name : '';
        $mailer->setFrom(
            env('MAIL_CONTRACTS_FROM', 'portal.contratista@idrd.gov.co'),
            env('MAIL_CONTRACTS_FROM_NAME', 'Portal Contratista')
        );
        $mailer->setSubject("Actualización {$this->user->getFullNameAttribute()} / $number / $type");
        if ( config('mail.mails.arl.email')  && filter_var( config('mail.mails.arl.email'), FILTER_VALIDATE_EMAIL) ) {
            $mailer->addTo(config('mail.mails.arl.email'));
        }
        if ( config('mail.mails.third_party.email')  && filter_var( config('mail.mails.third_party.email'), FILTER_VALIDATE_EMAIL) ) {
            $mailer->addTo(config('mail.mails.third_party.email'));
        }
        $render = $template->render();
        $mailer->addContent("text/html", $render);
        $sendgrid = new \SendGrid(config('mail.sendgrid.api'));
        try {
            $response = $sendgrid->send($mailer);
            sendgird_logger(
                'afiliacion.arl@idrd.gov.co, legalizacion.contratos@idrd.gov.co',
                "Actualización {$this->user->getFullNameAttribute()} / $number / $type",
                $render,
                [],
                $response->statusCode(),
                $response->body()
            );
        } catch (\Exception $e) {
            sendgird_logger(
                'afiliacion.arl@idrd.gov.co, legalizacion.contratos@idrd.gov.co',
                "[EXCEPTION] Actualización {$this->user->getFullNameAttribute()} / $number / $type",
                $render,
                [],
                $e->getCode(),
                $e->getMessage()
            );
        }
    }
}
