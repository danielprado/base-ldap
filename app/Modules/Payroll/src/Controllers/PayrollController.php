<?php


namespace App\Modules\Payroll\src\Controllers;


use App\Http\Controllers\Controller;

use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\Modules\Payroll\src\Resources\PayrollResource;
use App\Modules\Payroll\src\Models\PayrollSeven;
use App\Modules\Payroll\src\Request\PayrollExcelRequest;
use App\Modules\Payroll\src\Exports\PayrollExportTemplate;
use Tightenco\Collect\Support\Collection;
use Maatwebsite\Excel\Excel;
use PhpOffice\PhpSpreadsheet\IOFactory;
use Illuminate\Support\Str;
use Exception;
use GuzzleHttp\Client;

use Illuminate\Support\Facades\Storage;

class PayrollController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */

    public function getPayrollSevenList(Request $request)
    {
        //$data = [];
        $data = PayrollSeven::query()
                ->whereIn('TER_CODI', $request->listDocuments)
                ->orderBy('TER_NOCO')
                ->paginate(10000);
        return  $this->success_response(
            PayrollResource::collection( $data )
        );
    }

    /**
     * @param Request $request
     * @return JsonResponse|string
     */
    public function consultPayrollSevenList(Request $request)
    {
        try {
            $http = new Client([
                'base_uri' => env('URL_DOCKER_SERVER'),
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                ],
            ]);
            //crear registro DNS publico(registro A)
            //$response = $http->post("/api/payroll/getUserSevenList", [
            $response = $http->post("/api/payroll/getPayrollSevenList", [
                'json' => [
                    'listDocuments' => $request->get('listDocuments'),
                ],
            ]);
            $data = json_decode($response->getBody()->getContents(), true);
            return  response()->json($data);
        } catch (Exception $exception) {
            return $this->error_response(
                'No podemos realizar la consulta en este momento, por favor intente más tarde.',
                422,
                $exception->getMessage()
            );
        }
    }

    /**
     * @param CertificateComplianceRequest $request
     * @return JsonResponse|\Symfony\Component\HttpFoundation\StreamedResponse
     */
    public function excelPayroll(PayrollExcelRequest $request)
    {
        try {
            $writer = new PayrollExportTemplate($request);

            $response = response()->streamDownload(function() use ($writer) {
                $writer->create()->save('php://output');
            });
            $response->setStatusCode(200);
            $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            $response->headers->set('Content-Disposition', 'attachment; filename="PLANILLA_DE_PAGO_IDRD.xlsx"');
            return $response->send();

        } catch (Exception $exception) {
            return $this->error_response(
                'No podemos realizar la consulta en este momento, por favor intente más tarde.',
                422,
                $exception->getMessage()
            );
        }
    }
}
