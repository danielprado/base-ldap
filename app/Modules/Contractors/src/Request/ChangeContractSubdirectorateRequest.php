<?php

namespace App\Modules\Contractors\src\Request;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ChangeContractSubdirectorateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contract' => 'required|exists:mysql_contractors.contracts,contract',
            'subdirectorate_id' => 'required|exists:mysql_ldap.subdirectorates,id',
            'area_id' => 'required|exists:mysql_ldap.areas,id'
        ];
    }
}
