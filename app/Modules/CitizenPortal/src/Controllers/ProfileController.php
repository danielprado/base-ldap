<?php


namespace App\Modules\CitizenPortal\src\Controllers;


use App\Http\Controllers\Controller;
use App\Models\Security\User;
use App\Modules\CitizenPortal\src\Constants\Roles;
use App\Modules\CitizenPortal\src\Exports\CitizenExport;
use App\Modules\CitizenPortal\src\Exports\ProfilesExport;
use App\Modules\CitizenPortal\src\Jobs\ConfirmStatusCitizen;
use App\Modules\CitizenPortal\src\Models\Citizen;
use App\Modules\CitizenPortal\src\Models\CitizenSchedule;
use App\Modules\CitizenPortal\src\Models\File;
use App\Modules\CitizenPortal\src\Models\FileType;
use App\Modules\CitizenPortal\src\Models\Observation;
use App\Modules\CitizenPortal\src\Models\Profile;
use App\Modules\CitizenPortal\src\Models\ProfileView;
use App\Modules\CitizenPortal\src\Models\Schedule;
use App\Modules\CitizenPortal\src\Models\Status;
use App\Modules\CitizenPortal\src\Notifications\ProfileStatusNotification;
use App\Modules\CitizenPortal\src\Notifications\SendgridTemplates;
use App\Modules\CitizenPortal\src\Notifications\ValidatorNotification;
use App\Modules\CitizenPortal\src\Request\AssignorRequest;
use App\Modules\CitizenPortal\src\Request\ProfileFilterRequest;
use App\Modules\CitizenPortal\src\Request\StatusProfileRequest;
use App\Modules\CitizenPortal\src\Request\UpdateProfileRequest;
use App\Modules\CitizenPortal\src\Resources\ProfileResource;
use App\Modules\CitizenPortal\src\Resources\ProfileViewResource;
use Exception;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Excel;
use Silber\Bouncer\BouncerFacade;

class ProfileController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
        /*
        $this->middleware(Roles::actions(Profile::class, 'status'))
            ->only('status');
        $this->middleware(Roles::actions(Profile::class, 'validator'))
            ->only('validator');
        $this->middleware(Roles::actions(Profile::class, 'destroy_or_manage'))
            ->only('destroy', 'destroyAccount');
        */
    }

    /**
     * @param ProfileFilterRequest $request
     * @return JsonResponse
     */
    public function index2(ProfileFilterRequest $request)
    {
        $query = $this->setQuery(ProfileView::query(), (new ProfileView)->getSortableColumn($this->column))
            ->withCount([
                'observations',
                'files',
                'files as pending_files_count' => function($query) {
                    return $query->where('status_id', "!=", Profile::VERIFIED);
                },
            ])
            /*
            ->when($this->query, function ($query) {
                $keys = Profile::search($this->query)->get(['id'])->pluck('id')->toArray();
                return $query->whereKey($keys);
            })
            */
            ->when($this->query, function ($query) use ($request) {
                return $query->where('email', 'like', "%{$this->query}%")
                            ->orWHere('id', 'like', "%{$this->query}%")
                            ->orWHere('user_id', 'like', "%{$this->query}%")
                            ->orWHere('name', 'like', "%{$this->query}%")
                            ->orWHere('document', 'like', "%{$this->query}%")
                            ->orWHere('s_name', 'like', "%{$this->query}%")
                            ->orWHere('surname', 'like', "%{$this->query}%")
                            ->orWHere('s_surname', 'like', "%{$this->query}%");
            })
            ->when($request->has('id'), function ($query) use ($request) {
                return $query->where('id', '=', $request->get('id'));
            })
            ->when($request->has('document'), function ($query) use ($request) {
                return $query->where('document', 'like', "%{$request->get('document')}%");
            })
            ->when($request->has('status_id'), function ($query) use ($request) {
                $status = $request->get('status_id');
                if (is_array($status) && in_array(Profile::PENDING, $status)) {
                    return $query->whereNull('status_id')
                        ->orWhereIn('status_id', $status);
                }
                if ( $request->get('status_id') == Profile::PENDING ) {
                    return $query->whereNull('status_id')
                        ->orWhere('status_id', $request->get('status_id'));
                }
                return is_array($status)
                    ?  $query->whereIn('status_id', $request->get('status_id'))
                    :  $query->where('status_id', $request->get('status_id'));
            })
            ->when($request->has('not_assigned'), function ($query) use ($request) {
                return $query->whereNull('checker_id');
            })
            ->when($request->has('assigned'), function ($query) use ($request) {
                return $query->whereNotNull('checker_id');
            })
            ->when($request->has('validators_id'), function ($query) use ($request) {
                return $query->whereIn('checker_id', $request->get('validators_id'));
            })
            ->when($request->has('profile_type_id'), function ($query) use ($request) {
                return $query->whereIn('profile_type_id', $request->get('profile_type_id'));
            })
            ->when($request->has('start_date'), function ($query) use ($request) {
                return $query->whereDate('created_at', '>=', $request->get('start_date'));
            })
            ->when($request->has('final_date'), function ($query) use ($request) {
                return $query->whereDate('created_at', '<=', $request->get('final_date'));
            })
            ->when(
                (
                    auth('api')->user()->isA(Roles::ROLE_VALIDATOR)
                    && auth('api')->user()->isNotA(...Roles::adminAnd(Roles::ROLE_ASSIGNOR)
                 )
                ), function ($query) use ($request) {
                return $query->where('checker_id', auth('api')->user()->id);
            })
            ->orderBy((new ProfileView)->getSortableColumn($this->column), $this->order);
        return $this->success_response(
            ProfileResource::collection($query->paginate( $this->per_page)),
            Response::HTTP_OK,
            ProfileResource::headers()
        );
    }

    public function index(ProfileFilterRequest $request)
    {
        $query = $this->setQuery(Profile::query(), (new Profile)->getSortableColumn($this->column))
                    ->withCount([
                        'observations',
                        'files',
                        'files as pending_files_count' => function($query) {
                            return $query->where('status_id', "!=", Profile::VERIFIED);
                        },
                    ])
                    ->when($this->query, function (Builder $query) use ($request) {
                        return $query->orWhereHas('user', function (Builder $query) {
                                return $query->where('email','like', "%{$this->query}%");
                            })
                            ->orWHere('id', 'like', "%{$this->query}%")
                            ->orWHere('user_id', 'like', "%{$this->query}%")
                            ->orWHere('name', 'like', "%{$this->query}%")
                            ->orWHere('document', 'like', "%{$this->query}%")
                            ->orWHere('s_name', 'like', "%{$this->query}%")
                            ->orWHere('surname', 'like', "%{$this->query}%")
                            ->orWHere('s_surname', 'like', "%{$this->query}%");
                    })
                    ->when($request->has('id'), function ($query) use ($request) {
                        return $query->where('id', $request->get('id'));
                    })
                    ->when($request->has('document'), function ($query) use ($request) {
                        return $query->where('document', 'like', "%{$request->get('document')}%");
                    })
                    ->when($request->has('status_id'), function ($query) use ($request) {
                        $status = $request->get('status_id');
                        if (is_array($status) && in_array(Profile::PENDING, $status)) {
                            return $query->whereNull('status_id')
                                ->orWhereIn('status_id', $status);
                        }
                        if ( $request->get('status_id') == Profile::PENDING ) {
                            return $query->whereNull('status_id')
                                ->orWhere('status_id', $request->get('status_id'));
                        }
                        return is_array($status)
                            ?  $query->whereIn('status_id', $request->get('status_id'))
                            :  $query->where('status_id', $request->get('status_id'));
                    })
                    ->when($request->has('not_assigned'), function ($query) use ($request) {
                        return $query->whereNull('checker_id');
                    })
                    ->when($request->has('assigned'), function ($query) use ($request) {
                        return $query->whereNotNull('checker_id');
                    })
                    ->when($request->has('validators_id'), function ($query) use ($request) {
                        return $query->whereIn('checker_id', $request->get('validators_id'));
                    })
                    ->when($request->has('profile_type_id'), function ($query) use ($request) {
                        return $query->whereIn('profile_type_id', $request->get('profile_type_id'));
                    })
                    ->when($request->has('start_date'), function ($query) use ($request) {
                        return $query->whereDate('created_at', '>=', $request->get('start_date'));
                    })
                    ->when($request->has('final_date'), function ($query) use ($request) {
                        return $query->whereDate('created_at', '<=', $request->get('final_date'));
                    })
                    ->when($request->has('checker_id'), function ($query) use ($request) {
                        return $query->where('checker_id', auth('api')->user()->id);
                    })
                    ->orderBy((new Profile)->getSortableColumn($request->get("column", "updated_at")), $this->order);
        return $this->success_response(
            ProfileViewResource::collection($query->paginate( $this->per_page)),
            Response::HTTP_OK,
            ProfileViewResource::headers()
        );
    }

    /**
     * @param ProfileView $profile
     * @return JsonResponse
     */
    public function show(Profile $profile)
    {
        return $this->success_response(
            new ProfileViewResource($profile->loadCount(
                [
                    'observations',
                    'files',
                    'files as pending_files_count' => function($query) {
                        return $query->where('status_id', "!=", Profile::VERIFIED);
                    },
                ]
            )),
            Response::HTTP_OK,
            [
                'headers' =>  array_values(
                    array_merge( ProfileResource::headers()['headers'], ProfileResource::headers()['expanded'] )
                )
            ]
        );
    }

    public function update(Profile $profile, UpdateProfileRequest $request)
    {
        $profile->fill($request->validated());
        $profile->save();
        if ($request->has("email")) {
            $profile->user()->update([
                "email" => $request->get("email")
            ]);
        }
        return $this->success_message(
            __('validation.handler.success')
        );
    }

    /**
     * @param ProfileFilterRequest $request
     * @return JsonResponse
     */
    public function excel(ProfileFilterRequest $request)
    {
        $does_not_have_params = !$request->hasAny([
            'query',
            'document',
            'status_id',
            'not_assigned',
            'assigned',
            'validators_id',
            'profile_type_id',
            'start_date',
            'final_date',
        ]);

        if ($does_not_have_params) {
            $request->request->add([
                'start_date'   => now()->startOfMonth(),
                'final_date'    => now()->endOfMonth(),
            ]);
        }

        $file = \App\Modules\CitizenPortal\src\Exports\Excel::raw(new CitizenExport($request), Excel::XLSX);
        $name = random_img_name();
        $response =  array(
            'name' => toUpper(str_replace(' ', '-', __('citizen.validations.citizen_portal')))."-$name.xlsx",
            'file' => "data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,".base64_encode($file),
        );
        return $this->success_message($response);
    }

    /**
     * @param AssignorRequest $request
     * @param Profile $profile
     * @return JsonResponse
     */
    public function validator(AssignorRequest $request, Profile $profile)
    {
        try {
            DB::beginTransaction();
            if (isset($profile->checker_id)) {
                // $profile->viewer->disallow(Roles::can(Profile::class, 'status'));
            }
            $profile->assigned_by_id = auth('api')->user()->id;
            $profile->assigned_at = now();
            $profile->status_id = Profile::VALIDATING;
            $profile->checker_id = $request->get('validator_id');
            $profile->verified_at = null;
            $profile->save();
            $user = User::find($request->get('validator_id'));
            // BouncerFacade::ownedVia(Profile::class, 'checker_id');
            $user->allow(Roles::can(Profile::class, 'status'));
            // BouncerFacade::allow($user)->to(Roles::can(Profile::class, 'status'));
            $name = isset( $user->full_name ) ? (string) $user->full_name : 'SIN DATOS';
            $profile->observations()->create([
                'observation'   => auth('api')->user()->full_name." ha asignado a {$name} para validar los datos.",
                'user_ldap_id'       =>  auth('api')->user()->id,
            ]);
            Notification::send($user, new ValidatorNotification($profile));
            DB::commit();
            return $this->success_message(
                __('validation.handler.success')
            );
        } catch (Exception $exception) {
            DB::rollBack();
            return $this->error_response(
                __('validation.handler.service_unavailable'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $exception->getMessage()
            );
        }
    }

    /**
     * @param StatusProfileRequest $request
     * @param Profile $profile
     * @return JsonResponse
     */
    public function status(StatusProfileRequest $request, Profile $profile)
    {
        try {
            DB::beginTransaction();
            if ( $request->get('status_id') == Status::VERIFIED ) {
                $profile->verified_at = now()->format('Y-m-d H:i:s');
                $profile->status_id = $request->get('status_id');
                $file = $profile->files()->where('file_type_id', FileType::IDENTIFICATION_DOCUMENT)->first();
                if (isset($file->id)) {
                    $file->status_id = Status::FILE_VERIFIED;
                    $file->verified_at = now();
                    $file->save();
                }
            } else {
                $profile->verified_at = null;
                $profile->status_id = $request->get('status_id');
            }
            if (! isset($profile->assigned_by_id)) {
                $profile->assigned_by_id = auth('api')->user()->id;
                $profile->assigned_at = now();
            }
            $profile->save();
            $status = Status::find($request->get('status_id'));
            $statusName = isset( $status->name ) ? (string) $status->name : null;
            $observation = toUpper($request->get('observation', $statusName));
            $profile->observations()->create([
                'profile_id'    => $profile->id,
                'observation'   => $observation,
                'user_ldap_id'       =>  auth('api')->user()->id,
            ]);
            $this->dispatch( new ConfirmStatusCitizen(
                $profile,
                toFirstUpper($observation),
                auth('api')->user()->email
            ) );
            $profile->user->notify(new ProfileStatusNotification(
                $profile,
                $status,
                $observation,
                auth('api')->user()->full_name
            ));
            DB::commit();
            if ($request->get('status_id') == Status::RETURNED) {
                if ($profile->profile_type_id == Profile::PROFILE_PERSONAL) {
                    $profiles = Profile::query()->where("user_id", $profile->user_id)->count();
                    if ($profiles > 1) {
                        return $this->error_response(
                            "Este usuario es un perfil personal que tiene a cargo {$profiles} perfil(es) por favor realizar la validaciòn manual de estos usuarios y eliminarlos uno a uno.",
                            Response::HTTP_UNPROCESSABLE_ENTITY
                        );
                    }
                } else {
                    return $this->destroy($profile);
                }
            }
            return $this->success_message(
                __('validation.handler.success')
            );
        } catch (Exception $exception) {
            DB::rollBack();
            return $this->error_response(
                __('validation.handler.service_unavailable'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $exception->getMessage()
            );
        }
    }

    /**
     * @param Profile $profile
     * @return JsonResponse
     */
    public function destroy(Profile $profile)
    {
        try {
            DB::beginTransaction();
            foreach ($profile->observations as $observation) {
                $observation->delete();
            }
            foreach ($profile->files as $file) {
                if (Storage::disk('citizen_portal')->exists($file->file) ) {
                    if (config("app.env") == 'production') {
                        Storage::disk('citizen_portal')->delete($file->file);
                    }
                }
                if (config("app.env") == 'production') {
                    $file->delete();
                }
            }
            foreach ($profile->user_schedules as $schedule) {
                $schedule->delete();
            }
            $profile->delete();
            DB::commit();
            return $this->success_message(
                'Se han eliminado todos los registros relacionados a este usuario.',
                Response::HTTP_OK,
                Response::HTTP_NO_CONTENT
            );
        } catch (Exception $exception) {
            DB::rollBack();
            return $this->error_response(
                __('validation.handler.service_unavailable'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $exception->getMessage()
            );
        }
    }

    /**
     * @param Citizen $citizen
     * @return JsonResponse
     */
    public function destroyAccount(Citizen $citizen)
    {
        try {
            foreach ($citizen->profiles as $profile) {
                $this->destroy($profile);
            }
            $citizen->socialAccounts()->delete();
            $citizen->delete();
            $notify = new SendgridTemplates();
            $notify->sendDeleteAccountMessage("{$citizen->email}");
            return $this->success_message(
                'Se han eliminado todos los registros relacionados a este usuario.',
                Response::HTTP_OK,
                Response::HTTP_NO_CONTENT
            );
        } catch (Exception $exception) {
            return $this->error_response(
                __('validation.handler.service_unavailable')
            );
        }
    }
}
