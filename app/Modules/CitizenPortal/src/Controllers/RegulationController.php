<?php


namespace App\Modules\CitizenPortal\src\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\CitizenPortal\src\Models\Consent;
use App\Modules\CitizenPortal\src\Models\Regulation;
use App\Modules\CitizenPortal\src\Request\ActivityRequest;
use App\Modules\CitizenPortal\src\Request\ConsentRequest;
use App\Modules\CitizenPortal\src\Request\RegulationRequest;
use App\Modules\CitizenPortal\src\Resources\ConsentResource;
use App\Modules\CitizenPortal\src\Resources\RegulationResource;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class RegulationController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        $query = $this->setQuery(Regulation::query(), (new Regulation())->getSortableColumn($this->column))
            ->when(isset($this->query), function ($query) {
                return $query->where('name', 'like', "%$this->query%");
            })
            ->orderBy((new Consent)->getSortableColumn($this->column), $this->order);
        return $this->success_response(
            RegulationResource::collection(
                (int) $this->per_page > 0
                    ? $query->paginate( $this->per_page )
                    : $query->get()
            ),
            Response::HTTP_OK,
            [
                'headers'   => RegulationResource::headers(),
            ]
        );
    }

    /**
     * @param RegulationRequest $request
     * @return JsonResponse
     */
    public function store(RegulationRequest $request)
    {
        $ext = $request->file('file')->getClientOriginalExtension();
        $name = random_img_name().now()->format("YmdHis").".".$ext;
        if ($request->file('file')->storeAs('regulations', $name, [ 'disk' => 'citizen_portal_public' ])) {
            $model = new Regulation();
            $model->fill([
                "name" => $request->get("name"),
                "file" => $name
            ]);
            $model->save();
            return $this->success_message(
                __('validation.handler.success'),
                Response::HTTP_CREATED
            );
        }
        return $this->error_response(__('validation.handler.unexpected_failure'));
    }

    /**
     * @param RegulationRequest $request
     * @param Regulation $regulation
     * @return JsonResponse
     */
    public function update(RegulationRequest $request, Regulation $regulation)
    {
        $ext = $request->file('file')->getClientOriginalExtension();
        $name = random_img_name().now()->format("YmdHis").".".$ext;
        if ($request->file('file')->storeAs('regulations', $name, [ 'disk' => 'citizen_portal_public' ])) {
            if (Storage::disk("citizen_portal_public")->exists("regulations/{$regulation->file}")) {
                Storage::disk("citizen_portal_public")->delete("regulations/{$regulation->file}");
            }
            $regulation->fill([
                "name" => $request->get("name"),
                "file" => $name
            ]);
            $regulation->save();
            return $this->success_message(
                __('validation.handler.updated')
            );
        }
    }

    /**
     * @param Regulation $regulation
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Regulation $regulation)
    {
        if (Storage::disk("citizen_portal_public")->exists("regulations/{$regulation->file}")) {
            Storage::disk("citizen_portal_public")->delete("regulations/{$regulation->file}");
        }
        $regulation->delete();
        return $this->success_message(
            __('validation.handler.deleted'),
            Response::HTTP_OK,
            Response::HTTP_NO_CONTENT
        );
    }
}
