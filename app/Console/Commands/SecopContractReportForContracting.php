<?php

namespace App\Console\Commands;

use App\Modules\Contractors\src\Exports\ContractsSecopExport;
use App\Modules\Contractors\src\Jobs\GenerateReportSecopContracting;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class SecopContractReportForContracting extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'secop:report-hiring';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generates SECOP contract report for contracting';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->info('El comando se ha iniciado.');
        $contracts = get_all_contracts_secop_api();
        $this->info('Consulta de contratos ejecutada.');
        if (!$contracts->isEmpty()) {
            $file_name = "reporte-contratos-secop-" . now()->format('Y-m-d') . ".xlsx";
            Excel::store(new ContractsSecopExport(collect($contracts)), "exports/$file_name", 'local', \Maatwebsite\Excel\Excel::XLSX);
            $emails = [
                env('SECOP_REPORT_HIRING_EMAIL1'),
                env('SECOP_REPORT_HIRING_EMAIL2'),
                env('SECOP_REPORT_HIRING_EMAIL3')
            ];
            foreach ($emails as $email) {
                if (isset($email) && filter_var($email, FILTER_VALIDATE_EMAIL)) {
                    $this->info("Enviando correo a: $email");
                    dispatch(new GenerateReportSecopContracting($email, $file_name));
                }
            }
            $this->info("Report contracts generated.");
        } else {
            $this->info("Finished report without contracts news");
        }
    }
}
