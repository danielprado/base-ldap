<?php

namespace App\Modules\Contractors\src\Models;

use Illuminate\Database\Eloquent\Model;

class UserSevenProvider extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'oracle_ws';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'seven.po_pvdor';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'pvd_codi';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pvd_codi',
        'pvd_dive',
        'pvd_nomb',
        'pvr_apel',
        'tip_codi',
        'pvr_esta',
        'pvd_fcre',
        'pvd_clas',
        'tpr_codi',
        'mon_codi',
        'pvr_clad',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'emp_codi' => 'int',
        'tip_codi' => 'int',
        'tpr_codi' => 'int',
        'cal_codi' => 'int',
        'mon_codi' => 'int',
        'ite_clap' => 'int',
        'tso_codi' => 'int',
        'esm_codi' => 'int',
        'ite_cope' => 'int',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];
}
