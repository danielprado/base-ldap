<?php

namespace App\Modules\Contractors\src\Models;

use Illuminate\Database\Eloquent\Model;


class Fees extends Model
{

    const ACTIVE = 'ACTIVE';
    const INACTIVE = 'INACTIVE';

    protected $connection = 'mysql_contractors';
    protected $table = 'fees';
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    protected $primaryKey = 'id';


}
