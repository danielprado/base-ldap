<?php

namespace App\Modules\Certifications\src\Exports;

use App\Models\Security\DocumentType;
use App\Models\Security\User;
use App\Modules\Certifications\src\Models\CertificationAssignment;
use App\Modules\Certifications\src\Models\CertificationContract;
use App\Modules\Certifications\src\Models\CertificationContractType;
use App\Modules\Certifications\src\Models\CertificationMember;
use App\Traits\AppendHeaderToExcel;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Builder;
use Imtigger\LaravelJobStatus\JobStatus;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class MemberExport implements FromQuery, WithTitle, WithEvents, WithHeadings, WithMapping, WithColumnFormatting, ShouldQueue, ShouldAutoSize
{
    use Exportable, AppendHeaderToExcel;

    /**
     * @var array
     */
    private $request;

    /**
     * @var int
     */
    private $rowNumb = 2;

    /**
     * @var User
     */
    private $user;

    public function __construct(array $request, $job, $user)
    {
        $this->user = $user;
        $this->request = $request;
        update_status_job($job, JobStatus::STATUS_EXECUTING, 'excel-certifications-module');
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return CertificationMember::query()
            ->whereKey($this->request['members'])
            ->orderBy('created_at', 'desc');
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $sheet) {
                $this->setHeader($sheet->sheet, 'INTEGRANTES - CERTIFICACION DE CONTRATOS', 'A1:H1', 'H');
            }
        ];
    }

    public function headings(): array
    {
        return [
            'ID',
            'ID DEL CONTRATO',
            'TIPO DE DOCUMENTO INTEGRANTE',
            'DOCUMENTO INTEGRANTE',
            'NOMBRE INTEGRANTE',
            'PORCENTAJE',
            'FECHA DE CREACIÓN',
            'FECHA DE MODIFICACIÓN',
        ];
    }

    public function title(): string
    {
        return 'INTEGRANTES';
    }

    public function columnFormats(): array
    {
        return [
            'G' => NumberFormat::FORMAT_DATE_YYYYMMDD2.' h'.NumberFormat::FORMAT_DATE_TIME4,
            'H' => NumberFormat::FORMAT_DATE_YYYYMMDD2.' h'.NumberFormat::FORMAT_DATE_TIME4,
        ];
    }

    public function map($row): array
    {
        return [
            'id'                => $row['Id'] ?? null,
            'contract_id'       => $row['Contrato_Id'] ?? null,
            'document_type'     => $this->getDocumentType($row['Tipo_Documento_Integrante_Id']) ?? null,
            'document'          => $row['Documento_Integrante'] ?? null,
            'name'              => $row['Nombre_Integrante'] ?? null,
            'percent'           => $row['Porcentaje_Integrante'] ?? null,
            'created_at'        => isset($row['created_at']) ? date_time_to_excel(Carbon::parse($row['created_at'])) : null,
            'updated_at'        => isset($row['updated_at']) ? date_time_to_excel(Carbon::parse($row['updated_at'])) : null,
        ];
    }

    public function getDocumentType($id)
    {
        $document_type_id = isset($id) ? (int) $id : null;
        if ($document_type_id) {
            $document_type = DocumentType::query()->where('Id_TipoDocumento', $document_type_id)->pluck('Descripcion_TipoDocumento');
            return $document_type[0];
        }
        return  $document_type_id;
    }
}
