<?php

namespace App\Modules\Certifications\src\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Certifications\src\Models\CertificationContract;
use App\Modules\Certifications\src\Request\SuspensionRequest;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class SuspensionController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function store(SuspensionRequest $request, CertificationContract $contract): JsonResponse
    {
        $contract->suspensions()
            ->create([
                'Numero_Suspencion' => $request->get('number'),
                'Meses' => $request->get('months'),
                'Dias' => $request->get('days'),
                'Fecha_Inicio' => $request->get('start_date'),
                'Fecha_Fin' => $request->get('final_date'),
                'Fecha_Reinicio' => $request->get('restart_date'),
                'Fecha_Fin_CTO' => $request->get('final_date_cto'),
            ]);

        return $this->success_message(
            __('validation.handler.success'),
            Response::HTTP_CREATED
        );
    }

    public function update(SuspensionRequest $request, CertificationContract $contract): JsonResponse
    {
        $suspension = $contract->suspensions()
            ->where('Id', $request->get('id'))
            ->firstOrFail();

        $suspension->update([
            'Numero_Suspencion' => $request->get('number'),
            'Meses' => $request->get('months'),
            'Dias' => $request->get('days'),
            'Fecha_Inicio' => $request->get('start_date'),
            'Fecha_Fin' => $request->get('final_date'),
            'Fecha_Reinicio' => $request->get('restart_date'),
            'Fecha_Fin_CTO' => $request->get('final_date_cto'),
        ]);

        return $this->success_message(__('validation.handler.updated'));
    }

    /**
     * @throws Exception
     */
    public function destroy(CertificationContract $contract, $id): JsonResponse
    {
        $suspension = $contract->suspensions()->find($id);
        if ($suspension) {
            $suspension->delete();
            $order = 1;
            foreach ($contract->suspensions as $suspension) {
                $suspension->Numero_Suspencion = $order;
                $suspension->update();
                $order++;
            }
            return $this->success_message(__('validation.handler.deleted'));
        }
        return $this->error_response(__('validation.handler.unexpected_failure'));
    }
}
