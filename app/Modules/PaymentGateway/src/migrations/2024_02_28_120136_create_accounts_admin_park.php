<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAccountsAdminPark extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_pse')->create('accounts_admin_transaction', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('email');
            $table->text('otp')->nullable();
            $table->timestamp('otp_expiration')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('accounts_admin_transaction');
    }
}
