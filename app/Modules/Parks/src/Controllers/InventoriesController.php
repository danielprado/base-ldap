<?php

namespace App\Modules\Parks\src\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Parks\src\Constants\ArgisUrls;
use App\Modules\Parks\src\Constants\Utils;
use App\Modules\Parks\src\Exports\ExportFurniture;
use App\Modules\Parks\src\Exports\ExportFurnitureAll;
use App\Modules\Parks\src\Exports\ExportTypeEndowment;
use App\Modules\Parks\src\Help\ArrayHelper;
use App\Modules\Parks\src\Help\ExportReportHelper;
use App\Modules\Parks\src\Help\ReportExporter;
use App\Modules\Parks\src\Help\ResultsProcessor;
use App\Modules\Parks\src\Models\Endowment;
use App\Modules\Parks\src\Models\Material;
use App\Modules\Parks\src\Models\Park;
use App\Modules\Parks\src\Models\ParkEndowment;
use App\Modules\Parks\src\Request\ParkEndowmentCreateRequest;
use App\Modules\Parks\src\Request\ParkEndowmentUpdateRequest;
use App\Modules\Parks\src\Resources\EndowmentResource;
use App\Modules\Parks\src\Resources\EndowmentResourceC;
use App\Modules\Parks\src\Resources\FeatureEndowmentResource;
use App\Modules\Parks\src\Resources\FeatureFurnitureResource;
use App\Modules\Parks\src\Resources\LocalitiesEndowmentsResource;
use App\Modules\Parks\src\Resources\MaterialResource;
use App\Modules\Parks\src\Resources\NamesResource;
use App\Modules\Parks\src\Resources\ParkResourceArgis;
use App\Modules\Parks\src\Resources\TypesResource;
use App\Modules\Parks\src\Services\ArcGisParallelApiService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class InventoriesController extends Controller
{

    protected $argisUrls;
    protected $arcGisApiService;
    public function __construct()
    {
        parent::__construct();
        $this->middleware('auth:api')->only(['create', 'update', 'destroy', 'endowments', 'material']);
        /*
		$this->middleware(Roles::actions(ParkEndowment::class, 'create_or_manage'))->only('create');
		$this->middleware(Roles::actions(ParkEndowment::class, 'update_or_manage'))->only('update');
		$this->middleware(Roles::actions(ParkEndowment::class, 'destroy_or_manage'))->only('destroy');
        */
        $this->argisUrls = new ArgisUrls();
        $this->arcGisApiService = new ArcGisParallelApiService();
    }

    /**
     * @group Parques-inventarios
     * Dotaciones
     *
     * En desarrollo. Muestra el listado de docationes de un parque especificado y un equipamiento especificado.
     *
     * @urlParam park int required Id del parque. Example: 9478
     * @urlParam equipment int required Id del equipamiento. Example: 4
     *
     * @param $park
     * @param $equipment
     * @return JsonResponse
     */
    public function index(Park $park, $equipment)
    {
        $endowment = $park->park_endowment()->whereHas('endowment', function ($query) use ($equipment) {
            return $query->where('Id_Equipamento', $equipment);
        })
            ->paginate($this->per_page);
        return $this->success_response(EndowmentResource::collection($endowment));
    }

    /**
     * @group Parques-inventarios
     *
     * Crear dotacion
     *
     * Crea una dotacion para el parque especifico.
     *
     * @authenticated
     * @response 201 {
     *      "data": "Datos almacenados satisfactoriamente",
     *      "details": null,
     *      "code": 201,
     *      "requested_at": "2021-09-20T17:52:01-05:00"
     * }
     *
     * @param ParkEndowmentCreateRequest $request
     * @return JsonResponse
     */
    public function store(Park $park, ParkEndowmentCreateRequest $request)
    {
        try {
            $Parkendowment = new ParkEndowment();
            $filled = $Parkendowment->transformRequest($request->validated(), 'create');
            $Parkendowment->fill($filled);
            $Parkendowment->save();
            return $this->success_message(__('validation.handler.success'), Response::HTTP_CREATED);
        } catch (\Exception $e) {
            return $this->error_response(
                __('validation.handler.service_unavailable'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }

    /**
     * @group Parques-inventarios
     *
     * Actualizar dotación parque.
     *
     * Actualiza información de la dotacion de un parque en específico
     *
     * @authenticated
     * @response {
     *      "data": "Datos actualizados satisfactoriamente",
     *      "details": null,
     *      "code": 200,
     *      "requested_at": "2021-09-20T17:52:01-05:00"
     * }
     * @param ParkEndowmentUpdateRequest $request
     * @param $id int
     * @return JsonResponse
     */
    public function update(ParkEndowmentUpdateRequest $request, $id)
    {
        try {
            $parkendowment = ParkEndowment::find($id);
            $filled = $parkendowment->transformRequest($request->validated(), 'update', $parkendowment->Imagen);
            $parkendowment->fill($filled);
            $parkendowment->save();
            return $this->success_message(__('validation.handler.updated'));
        } catch (\Exception $e) {
            return $this->error_response(
                __('validation.handler.service_unavailable'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }

    /**
     * @group Parques-inventarios
     *
     * Eliminar dotacion parque.
     *
     * @authenticated
     * @response {
     *      "data": "Datos eliminados satisfactoriamente",
     *      "details": null,
     *      "code": 204,
     *      "requested_at": "2021-09-20T17:52:01-05:00"
     * }
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        try {
            $parkendowment = ParkEndowment::find($id);
            $existImage =  Storage::disk('public')->exists("parks/{$parkendowment->Imagen}");
            if ($existImage) {
                Storage::disk('public')->delete("parks/{$parkendowment->Imagen}");
            }
            $parkendowment->delete();
            return $this->success_message(__('validation.handler.deleted'), Response::HTTP_OK, Response::HTTP_NO_CONTENT);
        } catch (\Exception $e) {
            return $this->error_response(
                __('validation.handler.service_unavailable'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }

    /**
     * @group Parques-inventarios
     *
     * Muestra el listado de dotaciones existentes.
     *
     * @authenticated
     *
     * @return JsonResponse
     */
    public function endowments()
    {
        return $this->success_response(
            EndowmentResourceC::collection(Endowment::all())
        );
    }

    /**
     * @group Parques-inventarios
     *
     * Muestra el listado de materiales existentes.
     *
     * @authenticated
     *
     * @return JsonResponse
     */
    public function material()
    {
        return $this->success_response(
            MaterialResource::collection(Material::all())
        );
    }

    public function getTypes()
    {
        $urls = $this->argisUrls->urls();
        $results = $this->arcGisApiService->get($urls);
        $allTypes = [];
        foreach ($results as $index => $result) {
            if ($result['state'] === 'fulfilled') {
                $response = $result['value'];
                $content = json_decode($response->getBody()->getContents(), true)['types'];
                foreach ($content as $type) {
                    $allTypes[] = [
                        'id' => $type['id'],
                        'name' => $type['name'],
                        'category' => $this->getCategory($index)
                    ];
                }
            } else {
                // Maneja el error, por ejemplo, $result['reason']
            }
        }
        return $this->success_response(
            TypesResource::collection(collect($allTypes))
        );
    }

    public function getTypesFurniture()
    {
        $urls = $this->argisUrls->urlsTypesFurniture();
        $results = $this->arcGisApiService->get($urls);
        $allTypes = [];
        foreach ($results as $index => $result) {
            if ($result['state'] === 'fulfilled') {
                $response = $result['value'];
                $content = json_decode($response->getBody()->getContents(), true)['types'];
                foreach ($content as $type) {
                    $allTypes[] = [
                        'id' => $type['id'],
                        'name' => $type['name'],
                        'category' =>  $this->getCategoryFurniture($index)
                    ];
                }
            } else {
                // Maneja el error, por ejemplo, $result['reason']
            }
        }
        return $this->success_response(
            TypesResource::collection(collect($allTypes))
        );
    }

    public function getNamesEndowment()
    {
        $urls = $this->argisUrls->urls();
        $results = $this->arcGisApiService->get($urls);
        $allNamesEndowment = [];
        foreach ($results as $index => $result) {
            if ($result['state'] === 'fulfilled') {
                $response = $result['value'];
                $content = json_decode($response->getBody()->getContents(), true);
                foreach ($content['types'] as $type) {
                    if ($type['domains']['NOM_DOTACION']['type'] === 'codedValue') {
                        foreach ($type['domains']['NOM_DOTACION']['codedValues'] as $name) {
                            $allNamesEndowment[] = [
                                'idType' => $type['id'],
                                'nameType' => $type['name'],
                                'category' => $this->getCategory($index),
                                'name' => $name['name'],
                                'id' => $name['code']
                            ];
                        }
                    } else {
                        $matchField = ArrayHelper::find($content['fields'], 'name', 'NOM_DOTACION');
                        foreach ($matchField['domain']['codedValues'] as $field) {
                            $allNamesEndowment[] = [
                                'idType' => $type['id'],
                                'nameType' => $type['name'],
                                'category' => $this->getCategory($index),
                                'name' => $field['name'],
                                'id' => $field['code']
                            ];
                        }
                    }
                }
            } else {
                // Maneja el error, por ejemplo, $result['reason']
            }
        }
        return $this->success_response(
            NamesResource::collection(collect($allNamesEndowment))
        );
    }

    public function getNamesFurniture()
    {
        $urls = $this->argisUrls->urlsTypesFurniture();
        $results = $this->arcGisApiService->get($urls);
        $allNamesFurniture = [];
        foreach ($results as $index => $result) {
            if ($result['state'] === 'fulfilled') {
                $response = $result['value'];
                $content = json_decode($response->getBody()->getContents(), true);
                foreach ($content['types'] as $type) {
                    if ($type['domains']['NOM_MOBILIARIO']['type'] === 'codedValue') {
                        foreach ($type['domains']['NOM_MOBILIARIO']['codedValues'] as $name) {
                            $allNamesFurniture[] = [
                                'idType' => $type['id'],
                                'nameType' => $type['name'],
                                'category' => $this->getCategoryFurniture($index),
                                'name' => $name['name'],
                                'id' => $name['code']
                            ];
                        }
                    } else {
                        $matchField = ArrayHelper::find($content['fields'], 'name', 'NOM_MOBILIARIO');
                        foreach ($matchField['domain']['codedValues'] as $field) {
                            $allNamesFurniture[] = [
                                'idType' => $type['id'],
                                'nameType' => $type['name'],
                                'category' => $this->getCategoryFurniture($index),
                                'name' => $field['name'],
                                'id' => $field['code']
                            ];
                        }
                    }
                }
            } else {
                // Maneja el error, por ejemplo, $result['reason']
            }
        }
        return $this->success_response(
            NamesResource::collection(collect($allNamesFurniture))
        );
    }

    public function getLocalities()
    {
        $urls = [
            env('URL_BASE_ARCGIS') . '/Parques/MapServer/0?f=pjson'
        ];
        $results = $this->arcGisApiService->get($urls);
        $localities = [];
        if ($results[0]['state'] === 'fulfilled') {
            $content = json_decode($results[0]['value']->getBody()->getContents(), true)['fields'];
            $matchLocalities = ArrayHelper::find($content, 'name', 'LOCNOMBRE');
            $localities = $matchLocalities['domain']['codedValues'];
        } else {
            // Maneja el error, por ejemplo, $result['reason']
        }
        return $this->success_response(
            LocalitiesEndowmentsResource::collection(collect($localities))
        );
    }

    public function getParksArgis()
    {

        $urls = [
            env('URL_BASE_ARCGIS') . '/Parques/MapServer/0/query?where=NUM_CONT_ACT+%3D+%273056-2022%27&outFields=*&returnGeometry=false&f=pjson',
        ];
        $results = $this->arcGisApiService->get($urls);
        $allParks = [];
        foreach ($results as $index => $result) {
            if ($result['state'] === 'fulfilled') {
                $response = $result['value'];
                $content = json_decode($response->getBody()->getContents(), true)['features'];
                foreach ($content as $park) {
                    $allParks[] = [
                        'id' => $park['attributes']['ID_PARQUE'],
                        'name' => $park['attributes']['NOMBRE_PARQ'],
                    ];
                }
            } else {
                // Maneja el error, por ejemplo, $result['reason']
            }
        }
        return $this->success_response(
            ParkResourceArgis::collection(collect($allParks))
        );
    }

    public function getEndowment(Request $request)
    {
        $allFeatures = [];
        $allFields = [];
        $allTypes = [];
        $flagFilterOne = false;
        $flagFilterTwo = false;
        $flagFilterThree = false;
        $flagFilterFour = false;

        if ($request->query('id') && $request->query('categoryType')) {
            $flagFilterOne = true;
            $parts = explode("-", $request->query('categoryType'));
            $urls = [
                env('URL_BASE_ARCGIS') . '/' . $parts[0] . '/MapServer/' . $parts[1] . '?f=pjson',
                env('URL_BASE_ARCGIS') . '/' . $parts[0] . '/MapServer/' . $parts[1] . '/query?where=TIPO_DOTACION=' . $request->query('id') . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
                env('URL_BASE_ARCGIS') . '/' . $parts[0] . '/MapServer/' . $parts[1] . '/query?where=TIPO_DOTACION=' . $request->query('id') . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',
            ];
            $results = $this->arcGisApiService->get($urls);
            if ($results[0]['state'] === 'fulfilled') {
                $content = json_decode($results[0]['value']->getBody()->getContents(), true);
                foreach ($content['fields'] as $field) {
                    $allFields[] = [
                        'name' => $field['name'],
                        'domain' => $field['domain'],
                    ];
                }
                foreach ($content['types'] as $type) {
                    $allTypes[] = [
                        'id' => $type['id'],
                        'name' => $type['name'],
                        'domains' => $type['domains'],
                    ];
                }
            } else {
                // Maneja el error, por ejemplo, $result['reason']
            }
            $results1 = json_decode($results[1]['value']->getBody()->getContents(), true)['features'];
            $results2 = json_decode($results[2]['value']->getBody()->getContents(), true)['features'];
            $resultSTotal = array_merge($results1, $results2);
            foreach ($resultSTotal as $feature) {
                $allFeatures[] = [
                    'UB_REFERENCIA' => isset($feature['attributes']['UB_REFERENCIA']) ? $feature['attributes']['UB_REFERENCIA'] : null,
                    'NOMBRE_PARQ' => isset($feature['attributes']['NOMBRE_PARQ']) ? $feature['attributes']['NOMBRE_PARQ'] : null,
                    'ID_DOTACION' => isset($feature['attributes']['ID_DOTACION']) ? $feature['attributes']['ID_DOTACION'] : null,
                    'NOM_DOTACION' => isset($feature['attributes']['NOM_DOTACION']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['NOM_DOTACION'], 'NOM_DOTACION', $feature['attributes']['TIPO_DOTACION']) : null,
                    'NOM_DOTACION_ORIGINAL' => isset($feature['attributes']['NOM_DOTACION']) ? $feature['attributes']['NOM_DOTACION'] : null,
                    'TIPO_DOTACION' => $this->getType($allTypes, $feature['attributes']['TIPO_DOTACION']),
                    'DESCR' => isset($feature['attributes']['DESCR']) ? $feature['attributes']['DESCR'] : null,
                    'COOR_X' => isset($feature['attributes']['COOR_X']) ? $feature['attributes']['COOR_X'] : null,
                    'COOR_Y' => isset($feature['attributes']['COOR_Y']) ? $feature['attributes']['COOR_Y'] : null,
                    'RIESGO_1' => isset($feature['attributes']['RIESGO_1']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['RIESGO_1'], 'RIESGO_1', $feature['attributes']['TIPO_DOTACION']) : null,
                    'RIESGO_2' => isset($feature['attributes']['RIESGO_2']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['RIESGO_2'], 'RIESGO_2', $feature['attributes']['TIPO_DOTACION']) : null,
                    'RIESGO_3' => isset($feature['attributes']['RIESGO_3']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['RIESGO_3'], 'RIESGO_3', $feature['attributes']['TIPO_DOTACION']) : null,
                    'RIESGO_4' => isset($feature['attributes']['RIESGO_4']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['RIESGO_4'], 'RIESGO_4', $feature['attributes']['TIPO_DOTACION']) : null,
                    'MAT_PISO' => isset($feature['attributes']['MAT_PISO']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['MAT_PISO'], 'MAT_PISO', $feature['attributes']['TIPO_DOTACION']) : null,
                    'AREA_M2' => isset($feature['attributes']['AREA_M2']) ? $feature['attributes']['AREA_M2'] : null,
                    'AREA_JUEGO' => isset($feature['attributes']['AREA_JUEGO']) ? $feature['attributes']['AREA_JUEGO'] : null,
                    'CAPACIDAD' => isset($feature['attributes']['CAPACIDAD']) ? $feature['attributes']['CAPACIDAD'] : null,
                    'ILUMINACION' => isset($feature['attributes']['ILUMINACION']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['ILUMINACION'], 'ILUMINACION', $feature['attributes']['TIPO_DOTACION']) : null,
                    'CUBIERTO' => isset($feature['attributes']['CUBIERTO']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['CUBIERTO'], 'CUBIERTO', $feature['attributes']['TIPO_DOTACION']) : null,
                    'CERRAMIENTO' => isset($feature['attributes']['CERRAMIENTO']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['CERRAMIENTO'], 'CERRAMIENTO', $feature['attributes']['TIPO_DOTACION']) : null,
                    'TIPO_CERRAMIENTO_LAT' => isset($feature['attributes']['TIPO_CERRAMIENTO_LAT']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['TIPO_CERRAMIENTO_LAT'], 'TIPO_CERRAMIENTO_LAT', $feature['attributes']['TIPO_DOTACION']) : null,
                    'TIPO_CERRAMIENTO_FRO' => isset($feature['attributes']['TIPO_CERRAMIENTO_FRO']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['TIPO_CERRAMIENTO_FRO'], 'TIPO_CERRAMIENTO_FRO', $feature['attributes']['TIPO_DOTACION']) : null,
                    'CAMERINO' => isset($feature['attributes']['CAMERINO']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['CAMERINO'], 'CAMERINO', $feature['attributes']['TIPO_DOTACION']) : null,
                    'SISTEMA_RIEGO' => isset($feature['attributes']['SISTEMA_RIEGO']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['SISTEMA_RIEGO'], 'SISTEMA_RIEGO', $feature['attributes']['TIPO_DOTACION']) : null,
                    'USO_INCLUYENTE' => isset($feature['attributes']['USO_INCLUYENTE']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['USO_INCLUYENTE'], 'USO_INCLUYENTE', $feature['attributes']['TIPO_DOTACION']) : null,
                    'FECHA_CREACION' =>  isset($feature['attributes']['FECHA_CREACION']) ? $feature['attributes']['FECHA_CREACION'] : null,
                    'FECHA_ACTUALIZACION' => isset($feature['attributes']['FECHA_ACTUALIZACION']) ? $feature['attributes']['FECHA_ACTUALIZACION'] : null,
                    'NRO_PISO' => isset($feature['attributes']['NRO_PISO']) ? $feature['attributes']['NRO_PISO'] : null,
                    'ESTADO_DIAG' => isset($feature['attributes']['ESTADO_DIAG']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['ESTADO_DIAG'], 'ESTADO_DIAG', $feature['attributes']['TIPO_DOTACION']) : null,
                    'ESTADO_DIAG_ORIGINAL' => isset($feature['attributes']['ESTADO_DIAG']) ? $feature['attributes']['ESTADO_DIAG'] : null,
                ];
            }
            //} else {
            // Maneja el error, por ejemplo, $result['reason']
            //}
        }

        if ($request->query('idType') && $request->query('categoryName') && $request->query('idEndowment')) {
            $flagFilterTwo = true;
            if (count($allFeatures) === 0 && $flagFilterOne === false) {
                $parts = explode("-", $request->query('categoryName'));
                $urls = [
                    env('URL_BASE_ARCGIS') . '/' . $parts[0] . '/MapServer/' . $parts[1] . '?f=pjson',
                    env('URL_BASE_ARCGIS') . '/' . $parts[0] . '/MapServer/' . $parts[1] . '/query?where=TIPO_DOTACION=' . $request->query('idType') . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
                    env('URL_BASE_ARCGIS') . '/' . $parts[0] . '/MapServer/' . $parts[1] . '/query?where=TIPO_DOTACION=' . $request->query('idType') . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',
                ];
                return $urls;
                $results = $this->arcGisApiService->get($urls);
                if ($results[0]['state'] === 'fulfilled') {
                    $content = json_decode($results[0]['value']->getBody()->getContents(), true);
                    foreach ($content['fields'] as $field) {
                        $allFields[] = [
                            'name' => $field['name'],
                            'domain' => $field['domain'],
                        ];
                    }
                    foreach ($content['types'] as $type) {
                        $allTypes[] = [
                            'id' => $type['id'],
                            'name' => $type['name'],
                            'domains' => $type['domains'],
                        ];
                    }
                } else {
                    // Maneja el error, por ejemplo, $result['reason']
                }
                $resuls1 = json_decode($results[1]['value']->getBody()->getContents(), true)['features'];
                $resuls2 = json_decode($results[2]['value']->getBody()->getContents(), true)['features'];
                $resultSTotal = array_merge($resuls1, $resuls2);
                $value = $request->query('idEndowment');
                $match = array_filter($resultSTotal, function ($f) use ($value) {
                    return $f['attributes']['NOM_DOTACION'] == $value;
                });
                foreach ($match as $feature) {
                    $allFeatures[] = [
                        'UB_REFERENCIA' => isset($feature['attributes']['UB_REFERENCIA']) ? $feature['attributes']['UB_REFERENCIA'] : null,
                        'NOMBRE_PARQ' => isset($feature['attributes']['NOMBRE_PARQ']) ? $feature['attributes']['NOMBRE_PARQ'] : null,
                        'ID_DOTACION' => isset($feature['attributes']['ID_DOTACION']) ? $feature['attributes']['ID_DOTACION'] : null,
                        'NOM_DOTACION' => isset($feature['attributes']['NOM_DOTACION']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['NOM_DOTACION'], 'NOM_DOTACION', $feature['attributes']['TIPO_DOTACION']) : null,
                        'NOM_DOTACION_ORIGINAL' => isset($feature['attributes']['NOM_DOTACION']) ? $feature['attributes']['NOM_DOTACION'] : null,
                        'TIPO_DOTACION' => $this->getType($allTypes, $feature['attributes']['TIPO_DOTACION']),
                        'DESCR' => isset($feature['attributes']['DESCR']) ? $feature['attributes']['DESCR'] : null,
                        'COOR_X' => isset($feature['attributes']['COOR_X']) ? $feature['attributes']['COOR_X'] : null,
                        'COOR_Y' => isset($feature['attributes']['COOR_Y']) ? $feature['attributes']['COOR_Y'] : null,
                        'RIESGO_1' => isset($feature['attributes']['RIESGO_1']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['RIESGO_1'], 'RIESGO_1', $feature['attributes']['TIPO_DOTACION']) : null,
                        'RIESGO_2' => isset($feature['attributes']['RIESGO_2']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['RIESGO_2'], 'RIESGO_2', $feature['attributes']['TIPO_DOTACION']) : null,
                        'RIESGO_3' => isset($feature['attributes']['RIESGO_3']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['RIESGO_3'], 'RIESGO_3', $feature['attributes']['TIPO_DOTACION']) : null,
                        'RIESGO_4' => isset($feature['attributes']['RIESGO_4']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['RIESGO_4'], 'RIESGO_4', $feature['attributes']['TIPO_DOTACION']) : null,
                        'MAT_PISO' => isset($feature['attributes']['MAT_PISO']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['MAT_PISO'], 'MAT_PISO', $feature['attributes']['TIPO_DOTACION']) : null,
                        'AREA_M2' => isset($feature['attributes']['AREA_M2']) ? $feature['attributes']['AREA_M2'] : null,
                        'AREA_JUEGO' => isset($feature['attributes']['AREA_JUEGO']) ? $feature['attributes']['AREA_JUEGO'] : null,
                        'CAPACIDAD' => isset($feature['attributes']['CAPACIDAD']) ? $feature['attributes']['CAPACIDAD'] : null,
                        'ILUMINACION' => isset($feature['attributes']['ILUMINACION']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['ILUMINACION'], 'ILUMINACION', $feature['attributes']['TIPO_DOTACION']) : null,
                        'CUBIERTO' => isset($feature['attributes']['CUBIERTO']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['CUBIERTO'], 'CUBIERTO', $feature['attributes']['TIPO_DOTACION']) : null,
                        'CERRAMIENTO' => isset($feature['attributes']['CERRAMIENTO']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['CERRAMIENTO'], 'CERRAMIENTO', $feature['attributes']['TIPO_DOTACION']) : null,
                        'TIPO_CERRAMIENTO_LAT' => isset($feature['attributes']['TIPO_CERRAMIENTO_LAT']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['TIPO_CERRAMIENTO_LAT'], 'TIPO_CERRAMIENTO_LAT', $feature['attributes']['TIPO_DOTACION']) : null,
                        'TIPO_CERRAMIENTO_FRO' => isset($feature['attributes']['TIPO_CERRAMIENTO_FRO']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['TIPO_CERRAMIENTO_FRO'], 'TIPO_CERRAMIENTO_FRO', $feature['attributes']['TIPO_DOTACION']) : null,
                        'CAMERINO' => isset($feature['attributes']['CAMERINO']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['CAMERINO'], 'CAMERINO', $feature['attributes']['TIPO_DOTACION']) : null,
                        'SISTEMA_RIEGO' => isset($feature['attributes']['SISTEMA_RIEGO']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['SISTEMA_RIEGO'], 'SISTEMA_RIEGO', $feature['attributes']['TIPO_DOTACION']) : null,
                        'USO_INCLUYENTE' => isset($feature['attributes']['USO_INCLUYENTE']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['USO_INCLUYENTE'], 'USO_INCLUYENTE', $feature['attributes']['TIPO_DOTACION']) : null,
                        'FECHA_CREACION' =>  isset($feature['attributes']['FECHA_CREACION']) ? $feature['attributes']['FECHA_CREACION'] : null,
                        'FECHA_ACTUALIZACION' => isset($feature['attributes']['FECHA_ACTUALIZACION']) ? $feature['attributes']['FECHA_ACTUALIZACION'] : null,
                        'NRO_PISO' => isset($feature['attributes']['NRO_PISO']) ? $feature['attributes']['NRO_PISO'] : null,
                        'ESTADO_DIAG' => isset($feature['attributes']['ESTADO_DIAG']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['ESTADO_DIAG'], 'ESTADO_DIAG', $feature['attributes']['TIPO_DOTACION']) : null,
                        'ESTADO_DIAG_ORIGINAL' => isset($feature['attributes']['ESTADO_DIAG']) ? $feature['attributes']['ESTADO_DIAG'] : null,
                    ];
                }
                // } else {
                //     // Maneja el error, por ejemplo, $result['reason']
                // }
            } else {
                $allFeatures = ($request->query('categoryName') !== $request->query('categoryType'))
                    ? []
                    : array_values(array_filter($allFeatures, function ($f) use ($request) {
                        return $f['NOM_DOTACION_ORIGINAL'] == $request->query('idEndowment');
                    }));
            }
        }

        if ($request->query('status')) {
            $flagFilterThree = true;
            if (count($allFeatures) === 0 && $flagFilterOne === false && $flagFilterTwo === false) {

                // obtienen tipos
                $urlsTypesFields = $this->argisUrls->urls();
                $resultsEndTypesFields = $this->arcGisApiService->get($urlsTypesFields);

                // procesamiento de los tipos
                $resultsProcessor = new ResultsProcessor();
                $resultData =  $resultsProcessor->processResults($resultsEndTypesFields);
                $allFields = $resultData['allFields'];
                $allTypes = $resultData['allTypes'];

                // obtienen datos de argis
                $urls = $this->argisUrls->urlsEndowmentStatus($request->query('status'));
                $results = $this->arcGisApiService->get($urls);

                // procesamiento de los datos y los tipos
                $allFeatures = $resultsProcessor->processResultsFeatures($results, $allFields, $allTypes, 'endowment', 2);

            } else {
                $allFeatures =  array_values(array_filter($allFeatures, function ($f) use ($request) {
                    return $f['ESTADO_DIAG_ORIGINAL'] == $request->query('status');
                }));
            }
        }

        if ($request->query('risk')) {
            $flagFilterFour = true;
            $parts = explode("-", $request->query('risk'));
            if (count($allFeatures) === 0 && $flagFilterOne === false && $flagFilterTwo === false && $flagFilterThree === false) {

                // obtienen tipos
                $urlsTypesFields = $this->argisUrls->urls();
                $resultsEndTypesFields = $this->arcGisApiService->get($urlsTypesFields);

                // procesamiento de los tipos
                $resultsProcessor = new ResultsProcessor();
                $resultData = $resultsProcessor->processResults($resultsEndTypesFields);
                $allFields = $resultData['allFields'];
                $allTypes = $resultData['allTypes'];

                // obtienen datos de argis
                $urls = $this->argisUrls->urlsEndowmentRisk($parts[0], $parts[1]);
                $results = $this->arcGisApiService->get($urls);

                // procesamiento de los datos y los tipos
                $allFeatures = $resultsProcessor->processResultsFeatures($results, $allFields, $allTypes, 'endowment', 2);

            } else {
                $allFeatures =  array_values(array_filter($allFeatures, function ($f) use ($parts) {
                    $value = $parts[1] == '1' ? 'Si' : 'No';
                    return $f[$parts[0]] == $value;
                }));
            }
        }

        if ($request->query('locality')) {

            $urlParksInLocality = [
                env('URL_BASE_ARCGIS') . "/Parques/MapServer/0/query?where=LOCNOMBRE='" . $request->query('locality') . "'&outFields=*&returnGeometry=false&f=pjson"
            ];
            $results = $this->arcGisApiService->get($urlParksInLocality);
            if (count($allFeatures) === 0 && $flagFilterOne === false && $flagFilterTwo === false && $flagFilterThree === false && $flagFilterFour === false) {
                $result = '(';
                if ($results[0]['state'] === 'fulfilled') {
                    $content = json_decode($results[0]['value']->getBody()->getContents(), true);
                    foreach ($content['features'] as $feature) {
                        $result .= "'" . $feature['attributes']['ID_PARQUE'] . "'";
                        if ($feature !== end($content['features'])) {
                            $result .= ',';
                        }
                    }
                    $result .= ')';
                } else {
                    // Maneja el error, por ejemplo, $result['reason']
                }

                // obtienen tipos
                $urlsTypes = $this->argisUrls->urls();
                $resultsEndTypesFields = $this->arcGisApiService->get($urlsTypes);

                // procesamiento de los tipos
                $resultsProcessor = new ResultsProcessor();
                $resultData =  $resultsProcessor->processResults($resultsEndTypesFields);
                $allFields = $resultData['allFields'];
                $allTypes = $resultData['allTypes'];

                // obtienen datos de argis
                $urls = $this->argisUrls->urlsEndowmentLocalities($result);
                $resultsEndLocalities = $this->arcGisApiService->get($urls);

                // procesamiento de los datos y los tipos
                $allFeatures = $resultsProcessor->processResultsFeatures($resultsEndLocalities, $allFields, $allTypes, 'endowment', 2);

            } else {
                $idsParks = [];
                if ($results[0]['state'] === 'fulfilled') {
                    $content = json_decode($results[0]['value']->getBody()->getContents(), true);
                    foreach ($content['features'] as $feature) {
                        $idsParks[] = [
                            "UB_REFERENCIA" => $feature['attributes']['ID_PARQUE'],
                        ];
                    }
                } else {
                    // Maneja el error, por ejemplo, $result['reason']
                }
                $resultFilter = array_filter($allFeatures, function ($element) use ($idsParks) {
                    return in_array(["UB_REFERENCIA" => $element["UB_REFERENCIA"]], $idsParks);
                });
                $allFeatures = array_values($resultFilter);
            }
        }

        if ($request->query('action') === 'obtain') {
            return $this->success_response(
                FeatureEndowmentResource::collection(collect($allFeatures))
            );
        }

        if (count($allFeatures) <= Utils::REGISTERS) {
            return Excel::download(new ExportTypeEndowment(collect($allFeatures)), 'REPORTE.xlsx');
        }

        // cuando excede el número de registros descarga reporte en un zip para optimización  (evita error falta de memoria)
        $reportExporter = new ReportExporter();
        $zipFileName = $reportExporter->exportEndowmentReport($allFeatures);

        return response()->download($zipFileName)->deleteFileAfterSend(true);
    }

    public function getFurniture(Request $request)
    {
        $allFeatures = [];
        $allFields = [];
        $allTypes = [];
        $flagFilterOne = false;
        $flagFilterTwo = false;
        $flagFilterThree = false;
        $flagFilterFour = false;
        if ($request->query('id') && $request->query('categoryType')) {
            $flagFilterOne = true;
            $parts = explode("-", $request->query('categoryType'));

            $urls = [
                env('URL_BASE_ARCGIS') . '/' . $parts[0] . '/MapServer/' . $parts[1] . '?f=pjson',
                env('URL_BASE_ARCGIS') . '/' . $parts[0] . '/MapServer/' . $parts[1] . '/query?where=TIPO_MOBILIARIO=' . $request->query('id') . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
                env('URL_BASE_ARCGIS') . '/' . $parts[0] . '/MapServer/' . $parts[1] . '/query?where=TIPO_MOBILIARIO=' . $request->query('id') . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',
                env('URL_BASE_ARCGIS') . '/' . $parts[0] . '/MapServer/' . $parts[1] . '/query?where=TIPO_MOBILIARIO=' . $request->query('id') . '&outFields=*&returnGeometry=false&resultOffset=4000&f=pjson',
                env('URL_BASE_ARCGIS') . '/' . $parts[0] . '/MapServer/' . $parts[1] . '/query?where=TIPO_MOBILIARIO=' . $request->query('id') . '&outFields=*&returnGeometry=false&resultOffset=6000&f=pjson',
                env('URL_BASE_ARCGIS') . '/' . $parts[0] . '/MapServer/' . $parts[1] . '/query?where=TIPO_MOBILIARIO=' . $request->query('id') . '&outFields=*&returnGeometry=false&resultOffset=8000&f=pjson',
                env('URL_BASE_ARCGIS') . '/' . $parts[0] . '/MapServer/' . $parts[1] . '/query?where=TIPO_MOBILIARIO=' . $request->query('id') . '&outFields=*&returnGeometry=false&resultOffset=10000&f=pjson',
                env('URL_BASE_ARCGIS') . '/' . $parts[0] . '/MapServer/' . $parts[1] . '/query?where=TIPO_MOBILIARIO=' . $request->query('id') . '&outFields=*&returnGeometry=false&resultOffset=12000&f=pjson',
            ];
            $results = $this->arcGisApiService->get($urls);
            if ($results[0]['state'] === 'fulfilled') {
                $content = json_decode($results[0]['value']->getBody()->getContents(), true);
                foreach ($content['fields'] as $field) {
                    $allFields[] = [
                        'name' => $field['name'],
                        'domain' => $field['domain'],
                    ];
                }
                foreach ($content['types'] as $type) {
                    $allTypes[] = [
                        'id' => $type['id'],
                        'name' => $type['name'],
                        'domains' => $type['domains'],
                    ];
                }
            } else {
                // Maneja el error, por ejemplo, $result['reason']
            }

            $resuls1 = json_decode($results[1]['value']->getBody()->getContents(), true)['features'];
            $resuls2 = json_decode($results[2]['value']->getBody()->getContents(), true)['features'];
            $resuls3 = json_decode($results[3]['value']->getBody()->getContents(), true)['features'];
            $resuls4 = json_decode($results[4]['value']->getBody()->getContents(), true)['features'];
            $resuls5 = json_decode($results[5]['value']->getBody()->getContents(), true)['features'];
            $resuls6 = json_decode($results[6]['value']->getBody()->getContents(), true)['features'];
            $resuls7 = json_decode($results[7]['value']->getBody()->getContents(), true)['features'];
            $resultSTotal = array_merge($resuls1, $resuls2, $resuls3, $resuls4, $resuls5, $resuls6, $resuls7);
            //if ($results[1]['state'] === 'fulfilled') {
            // $contentFeature = json_decode($results[1]['value']->getBody()->getContents(), true)['features'];
            foreach ($resultSTotal as $feature) {
                $allFeatures[] = [
                    'UB_REFERENCIA' => isset($feature['attributes']['UB_REFERENCIA']) ? $feature['attributes']['UB_REFERENCIA'] : null,
                    'NOMBRE_PARQ' => isset($feature['attributes']['NOMBRE_PARQ']) ? $feature['attributes']['NOMBRE_PARQ'] : null,
                    'ID_MOBILIARIO' => isset($feature['attributes']['ID_MOBILIARIO']) ? $feature['attributes']['ID_MOBILIARIO'] : null,
                    'NOM_MOBILIARIO' => isset($feature['attributes']['NOM_MOBILIARIO']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['NOM_MOBILIARIO'], 'NOM_MOBILIARIO', $feature['attributes']['TIPO_MOBILIARIO']) : null,
                    'NOM_MOBILIARIO_ORIGINAL' => isset($feature['attributes']['NOM_MOBILIARIO']) ? $feature['attributes']['NOM_MOBILIARIO'] : null,
                    'TIPO_MOBILIARIO' => $this->getType($allTypes, $feature['attributes']['TIPO_MOBILIARIO']),
                    'DESCR' => isset($feature['attributes']['DESCR']) ? $feature['attributes']['DESCR'] : null,
                    'COOR_X' => isset($feature['attributes']['COOR_X']) ? $feature['attributes']['COOR_X'] : null,
                    'COOR_Y' => isset($feature['attributes']['COOR_Y']) ? $feature['attributes']['COOR_Y'] : null,
                    'MAT_ELEMENTO' => isset($feature['attributes']['MAT_ELEMENTO']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['MAT_ELEMENTO'], 'MAT_ELEMENTO', $feature['attributes']['TIPO_MOBILIARIO']) : null,
                    'CAPACIDAD' => isset($feature['attributes']['CAPACIDAD']) ? $feature['attributes']['CAPACIDAD'] : null,
                    'GRUPO_ETAREO' => isset($feature['attributes']['GRUPO_ETAREO']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['GRUPO_ETAREO'], 'GRUPO_ETAREO', $feature['attributes']['TIPO_MOBILIARIO']) : null,
                    'USO_INCLUYENTE' => isset($feature['attributes']['USO_INCLUYENTE']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['USO_INCLUYENTE'], 'USO_INCLUYENTE', $feature['attributes']['TIPO_MOBILIARIO']) : null,
                    'RIESGO_1' => isset($feature['attributes']['RIESGO_1']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['RIESGO_1'], 'RIESGO_1', $feature['attributes']['TIPO_MOBILIARIO']) : null,
                    'RIESGO_2' => isset($feature['attributes']['RIESGO_2']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['RIESGO_2'], 'RIESGO_2', $feature['attributes']['TIPO_MOBILIARIO']) : null,
                    'RIESGO_3' => isset($feature['attributes']['RIESGO_3']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['RIESGO_3'], 'RIESGO_3', $feature['attributes']['TIPO_MOBILIARIO']) : null,
                    'RIESGO_4' => isset($feature['attributes']['RIESGO_4']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['RIESGO_4'], 'RIESGO_4', $feature['attributes']['TIPO_MOBILIARIO']) : null,
                    'FECHA_CREACION' =>  isset($feature['attributes']['FECHA_CREACION']) ? $feature['attributes']['FECHA_CREACION'] : null,
                    'FECHA_ACTUALIZACION' => isset($feature['attributes']['FECHA_ACTUALIZACION']) ? $feature['attributes']['FECHA_ACTUALIZACION'] : null,
                    'NRO_PISO' => isset($feature['attributes']['NRO_PISO']) ? $feature['attributes']['NRO_PISO'] : null,
                    'ESTADO_DIAG' => isset($feature['attributes']['ESTADO_DIAG']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['ESTADO_DIAG'], 'ESTADO_DIAG', $feature['attributes']['TIPO_MOBILIARIO']) : null,
                    'ESTADO_DIAG_ORIGINAL' => isset($feature['attributes']['ESTADO_DIAG']) ? $feature['attributes']['ESTADO_DIAG'] : null,
                ];
            }
            // } else {
            //     // Maneja el error, por ejemplo, $result['reason']
            // }
        }
        if ($request->query('idType') && $request->query('categoryName') && $request->query('idFurniture')) {
            $flagFilterTwo = true;
            if (count($allFeatures) === 0 && $flagFilterOne === false) {
                $parts = explode("-", $request->query('categoryName'));
                $urls = [
                    env('URL_BASE_ARCGIS') . '/' . $parts[0] . '/MapServer/' . $parts[1] . '?f=pjson',
                    env('URL_BASE_ARCGIS') . '/' . $parts[0] . '/MapServer/' . $parts[1] . '/query?where=TIPO_MOBILIARIO=' . $request->query('idType') . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
                    env('URL_BASE_ARCGIS') . '/' . $parts[0] . '/MapServer/' . $parts[1] . '/query?where=TIPO_MOBILIARIO=' . $request->query('idType') . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',
                    env('URL_BASE_ARCGIS') . '/' . $parts[0] . '/MapServer/' . $parts[1] . '/query?where=TIPO_MOBILIARIO=' . $request->query('idType') . '&outFields=*&returnGeometry=false&resultOffset=4000&f=pjson',
                    env('URL_BASE_ARCGIS') . '/' . $parts[0] . '/MapServer/' . $parts[1] . '/query?where=TIPO_MOBILIARIO=' . $request->query('idType') . '&outFields=*&returnGeometry=false&resultOffset=6000&f=pjson',
                    env('URL_BASE_ARCGIS') . '/' . $parts[0] . '/MapServer/' . $parts[1] . '/query?where=TIPO_MOBILIARIO=' . $request->query('idType') . '&outFields=*&returnGeometry=false&resultOffset=8000&f=pjson',
                    env('URL_BASE_ARCGIS') . '/' . $parts[0] . '/MapServer/' . $parts[1] . '/query?where=TIPO_MOBILIARIO=' . $request->query('idType') . '&outFields=*&returnGeometry=false&resultOffset=10000&f=pjson',
                ];
                $results = $this->arcGisApiService->get($urls);

                if ($results[0]['state'] === 'fulfilled') {
                    $content = json_decode($results[0]['value']->getBody()->getContents(), true);
                    foreach ($content['fields'] as $field) {
                        $allFields[] = [
                            'name' => $field['name'],
                            'domain' => $field['domain'],
                        ];
                    }
                    foreach ($content['types'] as $type) {
                        $allTypes[] = [
                            'id' => $type['id'],
                            'name' => $type['name'],
                            'domains' => $type['domains'],
                        ];
                    }
                } else {
                    // Maneja el error, por ejemplo, $result['reason']
                }

                $resuls1 = json_decode($results[1]['value']->getBody()->getContents(), true)['features'];
                $resuls2 = json_decode($results[2]['value']->getBody()->getContents(), true)['features'];
                $resuls3 = json_decode($results[3]['value']->getBody()->getContents(), true)['features'];
                $resuls4 = json_decode($results[4]['value']->getBody()->getContents(), true)['features'];
                $resuls5 = json_decode($results[5]['value']->getBody()->getContents(), true)['features'];
                $resuls6 = json_decode($results[6]['value']->getBody()->getContents(), true)['features'];
                $resultSTotal = array_merge($resuls1, $resuls2, $resuls3, $resuls4, $resuls5, $resuls6);

                // if ($results[1]['state'] === 'fulfilled') {
                //     $contentFeature = json_decode($results[1]['value']->getBody()->getContents(), true)['features'];
                $value = $request->query('idFurniture');
                $match = array_filter($resultSTotal, function ($f) use ($value) {
                    return $f['attributes']['NOM_MOBILIARIO'] == $value;
                });
                foreach (array_values($match) as $feature) {
                    $allFeatures[] = [
                        'UB_REFERENCIA' => isset($feature['attributes']['UB_REFERENCIA']) ? $feature['attributes']['UB_REFERENCIA'] : null,
                        'NOMBRE_PARQ' => isset($feature['attributes']['NOMBRE_PARQ']) ? $feature['attributes']['NOMBRE_PARQ'] : null,
                        'ID_MOBILIARIO' => isset($feature['attributes']['ID_MOBILIARIO']) ? $feature['attributes']['ID_MOBILIARIO'] : null,
                        'NOM_MOBILIARIO' => isset($feature['attributes']['NOM_MOBILIARIO']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['NOM_MOBILIARIO'], 'NOM_MOBILIARIO', $feature['attributes']['TIPO_MOBILIARIO']) : null,
                        'NOM_MOBILIARIO_ORIGINAL' => isset($feature['attributes']['NOM_MOBILIARIO']) ? $feature['attributes']['NOM_MOBILIARIO'] : null,
                        'TIPO_MOBILIARIO' => $this->getType($allTypes, $feature['attributes']['TIPO_MOBILIARIO']),
                        'DESCR' => isset($feature['attributes']['DESCR']) ? $feature['attributes']['DESCR'] : null,
                        'COOR_X' => isset($feature['attributes']['COOR_X']) ? $feature['attributes']['COOR_X'] : null,
                        'COOR_Y' => isset($feature['attributes']['COOR_Y']) ? $feature['attributes']['COOR_Y'] : null,
                        'MAT_ELEMENTO' => isset($feature['attributes']['MAT_ELEMENTO']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['MAT_ELEMENTO'], 'MAT_ELEMENTO', $feature['attributes']['TIPO_MOBILIARIO']) : null,
                        'CAPACIDAD' => isset($feature['attributes']['CAPACIDAD']) ? $feature['attributes']['CAPACIDAD'] : null,
                        'GRUPO_ETAREO' => isset($feature['attributes']['GRUPO_ETAREO']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['GRUPO_ETAREO'], 'GRUPO_ETAREO', $feature['attributes']['TIPO_MOBILIARIO']) : null,
                        'USO_INCLUYENTE' => isset($feature['attributes']['USO_INCLUYENTE']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['USO_INCLUYENTE'], 'USO_INCLUYENTE', $feature['attributes']['TIPO_MOBILIARIO']) : null,
                        'RIESGO_1' => isset($feature['attributes']['RIESGO_1']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['RIESGO_1'], 'RIESGO_1', $feature['attributes']['TIPO_MOBILIARIO']) : null,
                        'RIESGO_2' => isset($feature['attributes']['RIESGO_2']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['RIESGO_2'], 'RIESGO_2', $feature['attributes']['TIPO_MOBILIARIO']) : null,
                        'RIESGO_3' => isset($feature['attributes']['RIESGO_3']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['RIESGO_3'], 'RIESGO_3', $feature['attributes']['TIPO_MOBILIARIO']) : null,
                        'RIESGO_4' => isset($feature['attributes']['RIESGO_4']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['RIESGO_4'], 'RIESGO_4', $feature['attributes']['TIPO_MOBILIARIO']) : null,
                        'FECHA_CREACION' =>  isset($feature['attributes']['FECHA_CREACION']) ? $feature['attributes']['FECHA_CREACION'] : null,
                        'FECHA_ACTUALIZACION' => isset($feature['attributes']['FECHA_ACTUALIZACION']) ? $feature['attributes']['FECHA_ACTUALIZACION'] : null,
                        'NRO_PISO' => isset($feature['attributes']['NRO_PISO']) ? $feature['attributes']['NRO_PISO'] : null,
                        'ESTADO_DIAG' => isset($feature['attributes']['ESTADO_DIAG']) ? $this->getInfo($allFields,  $allTypes, $feature['attributes']['ESTADO_DIAG'], 'ESTADO_DIAG', $feature['attributes']['TIPO_MOBILIARIO']) : null,
                        'ESTADO_DIAG_ORIGINAL' => isset($feature['attributes']['ESTADO_DIAG']) ? $feature['attributes']['ESTADO_DIAG'] : null,
                    ];
                }
                // } else {
                //     // Maneja el error, por ejemplo, $result['reason']
                // }
            } else {
                $allFeatures = ($request->query('categoryName') !== $request->query('categoryType'))
                    ? []
                    : array_values(array_filter($allFeatures, function ($f) use ($request) {
                        return $f['NOM_MOBILIARIO_ORIGINAL'] == $request->query('idFurniture');
                    }));
            }
        }

        if ($request->query('status')) {
            $flagFilterThree = true;
            if (count($allFeatures) === 0 && $flagFilterOne = true && $flagFilterTwo === false) {
                $urlsTypesFields = $this->argisUrls->urlsTypesFurniture();
                $resultsEndTypesFields = $this->arcGisApiService->get($urlsTypesFields);

                $resultsProcessor = new ResultsProcessor();
                $resultData =  $resultsProcessor->processResults($resultsEndTypesFields);

                $allFields = $resultData['allFields'];
                $allTypes = $resultData['allTypes'];

                $urls = $this->argisUrls->urlsFurnitureStatus($request->query('status'));
                $results = $this->arcGisApiService->get($urls);

                $allFeatures = $resultsProcessor->processResultsFeatures($results, $allFields, $allTypes, 'furniture', 13);

            } else {
                $allFeatures =  array_values(array_filter($allFeatures, function ($f) use ($request) {
                    return $f['ESTADO_DIAG_ORIGINAL'] == $request->query('status');
                }));
            }
        }

        if ($request->query('risk')) {
            $flagFilterFour = true;
            $parts = explode("-", $request->query('risk'));
            if (count($allFeatures) === 0 && $flagFilterOne === false && $flagFilterTwo === false && $flagFilterThree === false) {
                $urlsTypesFields = $this->argisUrls->urlsTypesFurniture();
                $resultsEndTypesFields = $this->arcGisApiService->get($urlsTypesFields);

                $resultsProcessor = new ResultsProcessor();
                $resultData =  $resultsProcessor->processResults($resultsEndTypesFields);
                $allFields = $resultData['allFields'];
                $allTypes = $resultData['allTypes'];

                $urls = $this->argisUrls->urlsFurnitureRisk($parts[0], $parts[1]);
                $results = $this->arcGisApiService->get($urls);

                $allFeatures = $resultsProcessor->processResultsFeatures($results, $allFields, $allTypes, 'furniture', 23);

            } else {
                $allFeatures =  array_values(array_filter($allFeatures, function ($f) use ($parts) {
                    $value = $parts[1] == '1' ? 'Si' : 'No';
                    return $f[$parts[0]] == $value;
                }));
            }
        }

        if ($request->query('locality')) {
            $urls = [
                env('URL_BASE_ARCGIS') . "/Parques/MapServer/0/query?where=LOCNOMBRE='" . $request->query('locality') . "'&outFields=*&returnGeometry=false&f=pjson"
            ];
            $results = $this->arcGisApiService->get($urls);
            if (count($allFeatures) === 0 && $flagFilterOne === false && $flagFilterTwo === false && $flagFilterThree === false && $flagFilterFour === false) {
                $result = '(';
                if ($results[0]['state'] === 'fulfilled') {
                    $content = json_decode($results[0]['value']->getBody()->getContents(), true);
                    foreach ($content['features'] as $feature) {
                        $result .= "'" . $feature['attributes']['ID_PARQUE'] . "'";
                        if ($feature !== end($content['features'])) {
                            $result .= ',';
                        }
                    }
                    $result .= ')';
                } else {
                    // Maneja el error, por ejemplo, $result['reason']
                }

                $urls = $this->argisUrls->urlsTypesFurniture();
                $resultsEndTypesFields = $this->arcGisApiService->get($urls);

                $resultsProcessor = new ResultsProcessor();
                $resultData =  $resultsProcessor->processResults($resultsEndTypesFields);

                $allFields = $resultData['allFields'];
                $allTypes = $resultData['allTypes'];

                $urls = $this->argisUrls->urlsFurnitureLocalities($result);
                $resultsEndLocalities = $this->arcGisApiService->get($urls);

                $allFeatures = $resultsProcessor->processResultsFeatures($resultsEndLocalities, $allFields, $allTypes, 'furniture', 13);

            } else {
                $idsParks = [];
                if ($results[0]['state'] === 'fulfilled') {
                    $content = json_decode($results[0]['value']->getBody()->getContents(), true);
                    foreach ($content['features'] as $feature) {
                        $idsParks[] = [
                            "UB_REFERENCIA" => $feature['attributes']['ID_PARQUE'],
                        ];
                    }
                } else {
                    // Maneja el error, por ejemplo, $result['reason']
                }
                $resultFilter = array_filter($allFeatures, function ($element) use ($idsParks) {
                    return in_array(["UB_REFERENCIA" => $element["UB_REFERENCIA"]], $idsParks);
                });
                $allFeatures = array_values($resultFilter);
            }
        }

        if ($request->query('action') === 'obtain') {
            return $this->success_response(
                FeatureFurnitureResource::collection(collect($allFeatures))
            );
        }

        if (count($allFeatures) <= Utils::REGISTERS) {
            return Excel::download(new ExportFurniture(collect($allFeatures)), 'REPORTE.xlsx');
        }

        // cuando excede el número de registros descarga reporte en un zip para optimización  (evita error falta de memoria)
        $reportExporter = new ReportExporter();
        $zipFileName = $reportExporter->exportFurnitureReport($allFeatures);

        return response()->download($zipFileName)->deleteFileAfterSend(true);

    }

    public function getAllEndowment(Request $request)
    {
        $allFields = [];
        $allTypes = [];
        $allFeatures = [];

        $urlsTypesFields = $this->argisUrls->urls();
        $resultsEndTypesFields = $this->arcGisApiService->get($urlsTypesFields);

        $resultsProcessor = new ResultsProcessor();
        $resultData =  $resultsProcessor->processResults($resultsEndTypesFields);

        $allFields = $resultData['allFields'];
        $allTypes = $resultData['allTypes'];

        $urls = $this->argisUrls->urlsAllEndowment();
        $results = $this->arcGisApiService->get($urls);

        $allFeatures = $resultsProcessor->processResultsFeatures($results, $allFields, $allTypes, 'endowment', 2);

        if ($request->query('action') === 'obtain') {
            return $this->success_response(
                FeatureEndowmentResource::collection(collect($allFeatures))
            );
        }

        if (count($allFeatures) <= Utils::REGISTERS) {
            return Excel::download(new ExportTypeEndowment(collect($allFeatures)), 'REPORTE.xlsx');
        }

        // cuando excede el número de registros descarga reporte en un zip para optimización  (evita error falta de memoria)
        $reportExporter = new ReportExporter();
        $zipFileName = $reportExporter->exportEndowmentReport($allFeatures);

        return response()->download($zipFileName)->deleteFileAfterSend(true);
    }

    public function getAllFurniture(Request $request)
    {
        $allFields = [];
        $allTypes = [];
        $allFeatures = [];

        $urlsTypesFields = $this->argisUrls->urlsTypesFurniture();
        $resultsEndTypesFields = $this->arcGisApiService->get($urlsTypesFields);

        $resultsProcessor = new ResultsProcessor();
        $resultData =  $resultsProcessor->processResults($resultsEndTypesFields);

        $allFields = $resultData['allFields'];
        $allTypes = $resultData['allTypes'];

        $urls = $this->argisUrls->urlsAllFurniture();
        $results = $this->arcGisApiService->get($urls);

        $allFeatures = $resultsProcessor->processResultsFeatures($results, $allFields, $allTypes, 'furniture', 20);

        if ($request->query('action') === 'obtain') {
            return $this->success_response(
                FeatureFurnitureResource::collection(collect($allFeatures))
            );
        }

        if (count($allFeatures) <= Utils::REGISTERS) {
            return Excel::download(new ExportFurniture(collect($allFeatures)), 'REPORTE.xlsx');
        }

        // cuando excede el número de registros descarga reporte en un zip para optimización  (evita error falta de memoria)
        $reportExporter = new ReportExporter();
        $zipFileName = $reportExporter->exportFurnitureReport($allFeatures);

        return response()->download($zipFileName)->deleteFileAfterSend(true);
    }

    public function getEndowmentForPark(Request $request)
    {
        $allFields = [];
        $allTypes = [];
        $allFeatures = [];

        $park = "('" . $request->query('id') . "')";
        $urlsTypesFields = $this->argisUrls->urls();
        $resultsEndTypesFields = $this->arcGisApiService->get($urlsTypesFields);

        $resultsProcessor = new ResultsProcessor();
        $resultData =  $resultsProcessor->processResults($resultsEndTypesFields);

        $allFields = $resultData['allFields'];
        $allTypes = $resultData['allTypes'];

        $urls = $this->argisUrls->urlsEndowmentLocalities($park);
        $results = $this->arcGisApiService->get($urls);

        $allFeatures = $resultsProcessor->processResultsFeatures($results, $allFields, $allTypes, 'endowment', 2);

        if ($request->query('action') === 'obtain') {
            return $this->success_response(
                FeatureEndowmentResource::collection(collect($allFeatures))
            );
        }
        return Excel::download(new ExportTypeEndowment(collect($allFeatures)), 'REPORTE_DOTACIONES_PARQUE.xlsx');
    }

    public function getFurnitureForPark(Request $request)
    {
        $allFields = [];
        $allTypes = [];
        $allFeatures = [];

        $park = "('" . $request->query('id') . "')";
        $urls = (new ArgisUrls())->urlsFurnitureLocalities($park);

        $urlsTypesFields = $this->argisUrls->urlsTypesFurniture();
        $resultsEndTypesFields = $this->arcGisApiService->get($urlsTypesFields);

        $resultsProcessor = new ResultsProcessor();
        $resultData =  $resultsProcessor->processResults($resultsEndTypesFields);
        $allFields = $resultData['allFields'];
        $allTypes = $resultData['allTypes'];

        $urls = $this->argisUrls->urlsFurnitureLocalities($park);
        $results = $this->arcGisApiService->get($urls);

        $allFeatures = $resultsProcessor->processResultsFeatures($results, $allFields, $allTypes, 'furniture', 13);

        if ($request->query('action') === 'obtain') {
            return $this->success_response(
                FeatureFurnitureResource::collection(collect($allFeatures))
            );
        }
        return Excel::download(new ExportFurniture(collect($allFeatures)), 'REPORTE_MOBILIARIO_PARQUE.xlsx');
    }

    private function getCategory($index): string
    {
        $categories = [
            'juegos-0',
            'juegos-1',
            'juegos-2',
            'Deportivo-20',
            'Deportivo-21',
            'Deportivo-22',
            'Deportivo-23',
            'Deportivo-24',
            'Ecologico-0',
            'Ecologico-1',
            'Ecologico-2',
            'Multifuncional-0',
            'Plazoletas-0',
            'Plazoletas-1',
            'ServiciosAuxiliares-0',
            'ServiciosAuxiliares-1',
            'ServiciosAuxiliares-2'
        ];
        return $categories[$index];
    }

    private function getCategoryFurniture($index): string
    {
        $categories = [
            'Mobiliario-0',
            'Mobiliario-1',
            'Mobiliario-2',
            'Mobiliario-31',
            'Mobiliario-32',
            'Mobiliario-33',
            'Mobiliario-34',

        ];
        return $categories[$index];
    }

    private function getType($allTypes, $id): string
    {
        $matchType = ArrayHelper::find($allTypes, 'id', $id);
        return $matchType['name'];
    }

    private function getInfo($arrayFields, $arrayTypes, $value, $propName, $type): ?string
    {
        $matchTypes = ArrayHelper::find($arrayTypes, 'id', $type);
        if ($matchTypes['domains'][$propName]['type'] === 'codedValue') {
            $matchTypeDomains = ArrayHelper::find($matchTypes['domains'][$propName]['codedValues'], 'code', $value);
            return $matchTypeDomains['name'];
        } else {
            $matchFields = ArrayHelper::find($arrayFields, 'name', $propName);
            $object = ArrayHelper::find($matchFields['domain']['codedValues'], 'code', $value);
            return $object['name'];
        }
    }
}
