<?php

namespace App\Modules\Certifications\src\Request;

use Illuminate\Foundation\Http\FormRequest;

class MemberRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'document_type_id'  => 'required|numeric',
            'document'          => 'required|numeric',
            'name'              => 'required|string',
            'percent'           => 'required|numeric',
        ];
    }

    public function attributes()
    {
        return [
            'document_type_id'  => 'Tipo de documento',
            'document'          => 'Documento',
            'name'              => 'Nombre',
            'percent'           => 'Porcentaje',
        ];
    }
}
