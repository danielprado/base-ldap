<?php

namespace App\Modules\PaymentGateway\src\Help;

use DateTime;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use \SendGrid\Mail\Mail as mailer;
use App\Modules\PaymentGateway\src\Mail\BookingMail;

class Helpers
{
      public function getAuthToken()
      {
            $server_application_code = env('API_LOGIN_DEV');
            $server_app_key = env('API_KEY_DEV');
            $date = new DateTime();
            $unix_timestamp = $date->getTimestamp();
            $uniq_token_string = $server_app_key . $unix_timestamp;
            $uniq_token_hash = hash('sha256', $uniq_token_string);
            $auth_token = base64_encode($server_application_code . ";" . $unix_timestamp . ";" . $uniq_token_hash);
            return $auth_token;
      }

      public function getTypePerson($type_person) {
          switch ($type_person) {
              case 'N':
                  return 'NATURAL';
              case 'J':
                  return 'JURIDICA';
          }
      }

      public function getTypeDocument($type)
      {
            switch ($type) {
                  case 'CC':
                        return 1;
                        break;
                  case 'TI':
                        return 2;
                        break;
                  case 'NIT':
                        return 7;
                        break;
                  case 'CE':
                        return 4;
                        break;
                  case 'PP':
                        return 6;
                        break;
                  case 'RC':
                        return 3;
                        break;
                  default:
                        return 14;
                        break;
            }
      }

      public function getStatus($status)
      {
            switch ($status) {
                  case 'pending':
                        return 1;
                        break;
                  case 'approved':
                        return 2;
                        break;
                  case 'cancelled':
                        return 3;
                        break;
                  case 'rejected':
                        return 4;
                        break;
                  default:
                        return 5;
                        break;
            }
      }

      public function getStatusWebHook($status)
      {
            switch ($status) {
                  case '0':
                        return 1;
                        break;
                  case '1':
                        return 2;
                        break;
                  case '2':
                        return 3;
                        break;
                  case '4':
                        return 4;
                        break;
                  default:
                        return 5;
                        break;
            }
      }

      public function addCopyEmailCEFE($endowmentID, $mailer){
        
            switch ($endowmentID) {
                  // Piscinas
                  case 17256:
                      $mailer->addBcc('cefe.tunal@idrd.gov.co');
                      break;
                  case 18181:
                      $mailer->addBcc('cefecometas@idrd.gov.co');
                      break;
                  case 17082:
                      $mailer->addBcc('cefe.fontanar@idrd.gov.co');
                      break;
                  case 16893:
                      $mailer->addBcc('cefe.sancristobal@idrd.gov.co');
                      break;
                  // Gimnasios
                  case 15227: // San Cristóbal
                      $mailer->addBcc('cefe.sancristobal@idrd.gov.co');
                      break;
                  case 18178: // Cometas
                      $mailer->addBcc('cefecometas@idrd.gov.co');
                      break;
                  case 17167: // Tunal
                      $mailer->addBcc('cefe.tunal@idrd.gov.co');
                      break;
                  case 17598: // Fontanar
                      $mailer->addBcc('cefe.fontanar@idrd.gov.co');
                      break;
                  default:
                      break;
              }
        }

       
      public function sendEmailReservation($payment)
      {

            $environment = App::environment();
            $reservation =  DB::connection('mysql_pse')->table('reserva_sintetica')->where('id', '=', $payment->id_reserva)->first();
            $park_endowment = DB::connection('mysql_parks')->table('parquedotacion')->where('Id', '=', $reservation->id_dotacion)->first();
            $park = DB::connection('mysql_parks')->table('parque')->where('Id', '=', $park_endowment->Id_Parque)->first();
            $horai = date('g:i A', strtotime($reservation->hora_inicio));
            $horaf = date('g:i A', strtotime($reservation->hora_fin));
            $profile =  DB::connection('mysql_citizen_portal')->table('profiles')->where('document', '=', $payment->identificacion)->first();
            $birthdate = $profile ? $profile->birthdate : 'N/A';
            $address = $profile ? $profile->address : 'N/A';

            $park_email = $park->Email == '' || $park->Email == null ? 'sim@idrd.gov.co' : $park->Email;


            $subject = 'Reserva dotación ' . $reservation->id_dotacion;
           
            $mailer = new mailer();
            $mailer->setFrom("mails@idrd.gov.co", "Reserva IDRD");
            $mailer->addTo($payment->email, $payment->nombre);
            $mailer->setSubject($subject);
            $url_validar_pago = '';
            if ($environment == 'production') {
                $mailer->addBcc($park_email);
                $this->addCopyEmailCEFE($reservation->id_dotacion, $mailer);
                $mailer->addBcc('sim@idrd.gov.co');
                $url_validar_pago = 'https://sim.idrd.gov.co/pasarela-pagos/transactions/';
            } else {
                // $mailer->addBcc('sim@idrd.gov.co');
                $mailer->addBcc('andres.cabeza@idrd.gov.co');
                $mailer->addBcc('daniel.forero@idrd.gov.co');
                $mailer->addBcc('jhon.zabala@idrd.gov.co');
                $url_validar_pago = 'https://sim.idrd.gov.co/pasarela-pagos-dev/transactions/';

            }

            $mailer->setTemplateId("d-1186bc3663cf4c42b22fa0c2012f9f20");
            $mailer->addDynamicTemplateData('numero_pago',  $payment->id_transaccion_pse);
            $mailer->addDynamicTemplateData('dotacion_id',  $reservation->id_dotacion );
            $mailer->addDynamicTemplateData('horario_reserva',  $horai . ' a ' . $horaf );
            $mailer->addDynamicTemplateData('fecha_reserva',  $reservation->fecha );
            $mailer->addDynamicTemplateData('total_reserva',  number_format( $payment->total, 2, ',', '.') );
            $mailer->addDynamicTemplateData('nombre_parque',  $park->Nombre );
            $mailer->addDynamicTemplateData('direccion_parque',  $park->Direccion );
            $mailer->addDynamicTemplateData('url_validar',  $url_validar_pago );
            $mailer->addDynamicTemplateData('nombre_persona',     $payment->nombre . " " . $payment->apellido);
            $mailer->addDynamicTemplateData('documento_persona',  $payment->identificacion);
            $mailer->addDynamicTemplateData('fecha_persona',  $birthdate );
            $mailer->addDynamicTemplateData('direccion_persona',  $address );
            $mailer->addDynamicTemplateData('telefono_persona', $payment->telefono );
            $mailer->addDynamicTemplateData('email_persona',  $payment->email);

            $sendgrid = new \SendGrid(env('SENDGRID_API_KEY'));
            $response = $sendgrid->send($mailer);
            $response->body();
            $response->statusCode();
            if ($response->statusCode() != 202) {
                \Illuminate\Support\Facades\Log::info("Email: $to - Status: {$response->statusCode()} ".$response->body());
            }
      }

      public function emailSendgridReservationCron($payment)
      {
            $environment = App::environment();
            $reservation =  DB::connection('mysql_pse')->table('reserva_sintetica')->where('id', '=', $payment->id_reserva)->first();
            $park_endowment = DB::connection('mysql_parks')->table('parquedotacion')->where('Id', '=', $reservation->id_dotacion)->first();
            $park = DB::connection('mysql_parks')->table('parque')->where('Id', '=', $park_endowment->Id_Parque)->first();
            $horai = date('g:i A', strtotime($reservation->hora_inicio));
            $horaf = date('g:i A', strtotime($reservation->hora_fin));
            $profile =  DB::connection('mysql_citizen_portal')->table('profiles')->where('document', '=', $payment->identificacion)->first();
            $birthdate = $profile ? $profile->birthdate : 'N/A';
            $address = $profile ? $profile->address : 'N/A';

            $park_email = $park->Email == '' || $park->Email == null ? 'sim@idrd.gov.co' : $park->Email;


            $subject = 'Reserva dotación ' . $reservation->id_dotacion;

            $mailer = new mailer();
            $mailer->setFrom("mails@idrd.gov.co", "Reserva - IDRD");
            $mailer->addTo($payment->email, $payment->nombre);
            $mailer->setSubject($subject);
            $url_validar_pago = '';
            if ($environment == 'production') {
                  $mailer->addBcc($park_email);
                  $this->addCopyEmailCEFE($mailer, $reservation->id_dotacion);
                  $mailer->addBcc('sim@idrd.gov.co');
                  $url_validar_pago = 'https://sim.idrd.gov.co/pasarela-pagos/transactions/';
            } else {
                  // $mailer->addBcc('sim@idrd.gov.co');
                  $mailer->addBcc('andres.cabeza@idrd.gov.co');
                  $mailer->addBcc('daniel.forero@idrd.gov.co');
                  $mailer->addBcc('jhon.zabala@idrd.gov.co');
                  $url_validar_pago = 'https://sim.idrd.gov.co/pasarela-pagos-dev/transactions/';

            }

            $mailer->setTemplateId("d-1186bc3663cf4c42b22fa0c2012f9f20");
            $mailer->addDynamicTemplateData('numero_pago',  $payment->id_transaccion_pse);
            $mailer->addDynamicTemplateData('dotacion_id',  $reservation->id_dotacion );
            $mailer->addDynamicTemplateData('horario_reserva',  $horai . ' a ' . $horaf );
            $mailer->addDynamicTemplateData('fecha_reserva',  $reservation->fecha );
            $mailer->addDynamicTemplateData('total_reserva',  number_format( $payment->total, 2, ',', '.') );
            $mailer->addDynamicTemplateData('nombre_parque',  $park->Nombre );
            $mailer->addDynamicTemplateData('direccion_parque',  $park->Direccion );
            $mailer->addDynamicTemplateData('url_validar',  $url_validar_pago );
            $mailer->addDynamicTemplateData('nombre_persona',     $payment->nombre . " " . $payment->apellido);
            $mailer->addDynamicTemplateData('documento_persona',  $payment->identificacion);
            $mailer->addDynamicTemplateData('fecha_persona',  $birthdate );
            $mailer->addDynamicTemplateData('direccion_persona',  $address );
            $mailer->addDynamicTemplateData('telefono_persona', $payment->telefono );
            $mailer->addDynamicTemplateData('email_persona',  $payment->email);

            $sendgrid = new \SendGrid(env('SENDGRID_API_KEY'));
            $response = $sendgrid->send($mailer);
            $response->body();
            $response->statusCode();
            if ($response->statusCode() != 202) {
                  \Illuminate\Support\Facades\Log::info("Email: $to - Status: {$response->statusCode()} ".$response->body());
            }
      }

      public function statusVoucher($statusId)
      {
            switch ($statusId) {
                  case '1':
                        return ['r' => 0, 'g' => 0, 'b' => 255];
                        break;
                  case '2':
                        return ['r' => 0, 'g' => 128, 'b' => 0];
                        break;
                  default:
                        return ['r' => 255, 'g' => 0, 'b' => 0];
                        break;
            }
      }

      public function messagesuccess($message, $data, $code)
      {
            return response()->json([
                  'status' => 'OK',
                  'message' => $message,
                  'data' =>  $data,
                  'Error' => false,
            ], $code);
      }

      public function messageerror($message, $data, $code)
      {
            return response()->json([
                  'status' => 'Error',
                  'message' => $message,
                  'data' =>  $data,
                  'Error' => true,
            ], $code);
      }

      public function sendEmailOtp($pay)
      {
          $sendgrid = new \SendGrid(env('SENDGRID_API_KEY'));
          $mail = new mailer();
          $mail->setFrom('mails@idrd.gov.co', 'Notificaciones IDRD-pagos');
          $mail->setSubject('Código de verificación');
          $mail->setFrom('mails@idrd.gov.co', 'Notificaciones IDRD-pagos');
          $mail->addTo($pay->email);
          if (strtolower($pay->email) != strtolower($pay->email_pagador)) {
              $mail->addTo($pay->email_pagador);
          }
          $mail->addContent(
              'text/html',
              '<p>Su código de verificación para obtener la información de su transacción es: <strong>' . $pay->otp . '</strong></p>' .
              '<p>Este código tiene una vigencia de 5 minutos</p>'
          );
          $sendgrid->send($mail);
      }

    public function sendEmailOtpAdminPark($email, $otp)
      {
          $sendgrid = new \SendGrid(env('SENDGRID_API_KEY'));
          $mail = new mailer();
          $mail->setFrom('mails@idrd.gov.co', 'Notificaciones IDRD-pagos');
          $mail->setSubject('Código de verificación');
          $mail->setFrom('mails@idrd.gov.co', 'Notificaciones IDRD-pagos');
          $mail->addTo($email);
          $mail->addContent(
              'text/html',
              '<p>El código de verificación para obtener la información de la transacción es: <strong>' . $otp . '</strong></p>' .
              '<p>Este código tiene una vigencia de 5 minutos</p>'
          );
          $sendgrid->send($mail);
      }
}
