<?php

namespace App\Modules\Contractors\src\Helpers\compliance\validators;

use App\Modules\Contractors\src\Models\Compliance;
use Carbon\Carbon;

abstract class ComplianceDateValidator extends ComplianceValidator
{
  public function name(): string
  {
    return 'date';
  }

  abstract function period(Carbon $date = null);

  public function validate($compliance): bool 
  {
    $period = $this->period();

    if ($compliance->isFirstTime()) return true;
    
    if ($compliance->status === Compliance::STATUS_APPROVED) return false;

    return $period['start']->gte($compliance->period_start) 
        && $period['end']->lte($compliance->period_end);
  }
}