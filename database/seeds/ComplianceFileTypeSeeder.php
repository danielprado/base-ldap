<?php

use App\Modules\Contractors\src\Models\ComplianceFileType;
use Illuminate\Database\Seeder;

class ComplianceFileTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $complianceFileTypes = [
            [
                'name' => 'SOPORTE_MENORES',
                'support_field_name' => 'children_minor'
            ],
            [
                'name' => 'SOPORTE_MAYORES_ESTUDIANTES',
                'support_field_name' => 'children_mayor_students'
            ],
            [
                'name' => 'SOPORTE_HIJOS_DISCAPACITADOS',
                'support_field_name' => 'children_disabilities'
            ],
            [
                'name' => 'SOPORTE_CONYUGE',
                'support_field_name' => 'spouse'
            ],
            [
                'name' => 'SOPORTE_DEPENDENCIA_PADRES',
                'support_field_name' => 'parents_dependency'
            ],
            [
                'name' => 'SOPORTE_INTERESES_VIVIENDA',
                'support_field_name' => 'housing_interests'
            ],
            [
                'name' => 'SOPORTE_MEDICINA_PREPAGADA',
                'support_field_name' => 'prepaid_medicine'
            ],
            [
                'name' => 'SOPORTE_PAGO_PLANILLA',
                'support_field_name' => 'support_payment'
            ],
            [
                'name' => 'SOPORTE_INFORME_ACTIVIDADES',
                'support_field_name' => 'activity_report'
            ]
        ];

        ComplianceFileType::insert($complianceFileTypes);
    }
}
