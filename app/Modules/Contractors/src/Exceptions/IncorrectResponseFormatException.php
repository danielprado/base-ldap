<?php

namespace App\Modules\Contractors\src\Exceptions;

use Exception;
use Illuminate\Http\Response;

class IncorrectResponseFormatException extends Exception
{
    private $data;

    public function __construct($data = null) {
        $this->data = $data;
    }
    /**
     * Render the exception into an HTTP response.
     */
    public function render($request)
    {
        return response()
            ->json([
                'message' => 'El formato de la respuesta no es el correcto.',
                'details' => $this->data,
                'code' => Response::HTTP_NOT_ACCEPTABLE,
                'requested_at' => now()->toDateTimeLocalString()
            ], Response::HTTP_NOT_ACCEPTABLE);
    }
}
