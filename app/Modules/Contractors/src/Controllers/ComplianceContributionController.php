<?php

namespace App\Modules\Contractors\src\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Contractors\src\Jobs\ComplianceRejectedReminder;
use App\Modules\Contractors\src\Models\Compliance;
use App\Modules\Contractors\src\Request\ChangeComplianceRevisionStatusRequest;
use Illuminate\Http\Response;

class ComplianceContributionController extends Controller
{
    public function changeRevisionStatus(ChangeComplianceRevisionStatusRequest $request)
    {
        $validated = $request->validated();

        $compliance = Compliance::query()
            ->whereHas('contract', function ($query) use ($validated) {
                $query->where('contract', format_contract($validated['contract_number'], $validated['contract_year']));
            })
            ->where('status', 'PENDING')
            ->first();

        if (!$compliance) {
            return $this->error_response(
                'No hay certificados de cumplimiento pendientes por revisar.',
                Response::HTTP_NOT_FOUND
            );
        }

        $compliance->timestamps = false;

        $compliance->status = $validated['status'];
        $compliance->reviewer = auth('api')->user()->email;

        if ($validated['status'] === 'REJECTED') {
            $compliance->observations = $validated['observation'];

            $this->dispatch(new ComplianceRejectedReminder(
                $compliance->contractor, 
                $compliance, 
                $validated['observation']
            ));
        }
        
        $compliance->save();

        return $this->success_message(
            'El estado de revisión se ha actualizado con éxito',
            Response::HTTP_NO_CONTENT
        );
    }
}
