<?php

namespace App\Modules\Contractors\src\Jobs;

use App\Modules\Contractors\src\Mail\ComplianceRejectedMail;
use App\Modules\Contractors\src\Models\Compliance;
use App\Modules\Contractors\src\Models\Contractor;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use SendGrid\Mail\Mail;
use SendGrid\Mail\To;

class ComplianceRejectedReminder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Contractor
     */
    private $user;

    /**
     * @var Compliance
     */
    private $compliance;

    /**
     * @var string
     */
    private $observation;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        Contractor $user,
        Compliance $compliance,
        string $observation
    ) {
        $this->user = $user;
        $this->compliance = $compliance;
        $this->observation = $observation;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mail $mailer)
    {
        $emails = [];

        if (isset($this->user->email) && filter_var($this->user->email, FILTER_VALIDATE_EMAIL)) {
            $emails[] = new To($this->user->email);
        }

        /*if (isset($this->user->institutional_email) && filter_var($this->user->institutional_email, FILTER_VALIDATE_EMAIL)) {
            $emails[] = new To($this->user->institutional_email);
        }*/

        if (!empty($emails)) {
            $mailer->setFrom(
                env('MAIL_CONTRACTS_FROM', 'portal.contratista@idrd.gov.co'),
                env('MAIL_CONTRACTS_FROM_NAME', 'Portal Contratista')
            );
            $mailer->setSubject('Certificado de Cumplimiento Tributario - Rechazado');
            $mailer->addTos($emails);

            $render = (new ComplianceRejectedMail($this->compliance, $this->observation))->render();
            $mailer->addContent("text/html", $render);

            $sendgrid = new \SendGrid(config('mail.sendgrid.api'));

            try {
                $response = $sendgrid->send($mailer);
                foreach ($emails as $email) {
                    sendgird_logger(
                        $email->getEmail(),
                        "Certificado de Cumplimiento Tributario - Rechazo",
                        $render,
                        [],
                        $response->statusCode(),
                        $response->body()
                    );
                }
            } catch (\Exception $e) {
                foreach ($emails as $email) {
                    sendgird_logger(
                        $email->getEmail(),
                        "[EXCEPTION] Certificado de Cumplimiento Tributario - Rechazo",
                        $render,
                        [],
                        $e->getCode(),
                        $e->getMessage()
                    );
                }
            }
        }
    }
}
