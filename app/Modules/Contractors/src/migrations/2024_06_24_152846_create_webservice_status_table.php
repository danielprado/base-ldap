<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWebserviceStatusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('webservice_status', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('Identificador del registro');
            $table->unsignedBigInteger('user_id')->nullable()->comment('Identificador del Usuario LDAP');
            $table->string('user', 191)->nullable()->comment('Nombre del usuario');
            $table->text('url')->nullable()->comment('URL del servicio');
            $table->string('request_type')->nullable()->comment('Tipo de solicitud');
            $table->longText('request')->nullable()->comment('Cuerpo de la solicitud');
            $table->string('status', 20)->nullable()->comment('Webservice Status');
            $table->longText('response')->nullable()->comment('Webservice Response');
            $table->timestamps();

            $ldap_database = env('DB_LDAP_DATABASE');
            $table->foreign('user_id')
                ->references('id')
                ->on("{$ldap_database}.users")
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('webservice_status');
    }
}
