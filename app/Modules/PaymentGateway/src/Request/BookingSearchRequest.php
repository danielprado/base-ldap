<?php

namespace App\Modules\PaymentGateway\src\Request;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class BookingSearchRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function getValidatorInstance()
    {
        return parent::getValidatorInstance();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'start_date'    => 'nullable|date',
            'final_date'    => 'nullable|date',
            'status'        => 'nullable|string',
            'park_name'     => 'nullable|string',
        ];
    }

    public function attributes()
    {
        return [
            'start_date' => 'fecha inicial',
            'final_date' => 'fecha final',
            'status'     =>  'estado de la reserva',
            'park_name'  =>  'nombre del parque',
        ];
    }
}
