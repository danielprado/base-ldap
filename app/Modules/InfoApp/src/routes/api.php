<?php

use App\Modules\InfoApp\src\Controllers\InfoAppController;
use App\Modules\InfoApp\src\Controllers\EventController;
use App\Modules\InfoApp\src\Controllers\NoticeController;
use Illuminate\Support\Facades\Route;

Route::prefix('infoapp')->group(function () {
    Route::get('/categories', [InfoAppController::class, 'categories']);
    // Route::middleware('auth:api')->group(function () {
    //     Route::get('/roles', [AdminController::class, 'roles']);
    // });
    Route::apiResource('events', EventController::class);
    Route::apiResource('notices', NoticeController::class);
    Route::get('events/date-range-days/{number_days}', [EventController::class, 'indexWithDateRange']);
    Route::post('version-app', [InfoAppController::class, 'getVersionApp']);
});
