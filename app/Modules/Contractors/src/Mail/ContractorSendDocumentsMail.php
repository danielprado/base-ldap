<?php


namespace App\Modules\Contractors\src\Mail;


use App\Modules\Contractors\src\Models\ActivityReport;
use App\Modules\Contractors\src\Models\Contract;
use App\Modules\Contractors\src\Models\Contractor;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;

class ContractorSendDocumentsMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Contractor
     */
    private $mail;

    /**
     * @var string
     */
    private $file_compliance;

    /**
     * @var string
     */
    private $file_activity;

    /**
     * @var ActivityReport
     */
    private $activity_report;

    /**
     * @var string
     */
    private $filed_number;

    /**
     * Create a new job instance.
     *
     * @param Contractor $user
     * @param string $file_compliance
     * @param string $file_activity
     * @param ActivityReport $activity_report
     * @param string $filed_number
     */
    public function __construct(Contractor $user, string $file_compliance, string $file_activity, ActivityReport $activity_report, string $filed_number)
    {
        $this->mail = $user;
        $this->file_compliance = $file_compliance;
        $this->file_activity = $file_activity;
        $this->activity_report = $activity_report;
        $this->filed_number = $filed_number;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $created_at = now()->format('Y-m-d H:i:s');
        $document = isset( $this->mail->document ) ? $this->mail->document : '';
        $first = isset( $this->mail->name ) ? $this->mail->name : '';
        $second = isset( $this->mail->surname ) ? $this->mail->surname : '';
        $name = "$first $second";
        $subject = "{$name}";
        $filed_number = $this->filed_number ?? '';

        if (Storage::disk('local')->exists("contractor/{$this->file_compliance}") &&
            Storage::disk('local')->exists("contractor/{$this->file_activity}")) {
            $email = $this->view('mail.mail')
                ->subject($subject)
                ->with([
                    'slogan'    => 'Notificación Documentación Aprobada IDRD',
                    'header'    => 'IDRD',
                    'title'     => 'Documentos Trámite de Pagos Portal Contratista',
                    'content'   => "Hola {$name}, adjunto encontrarás los documentos para cargar al #7 en SECOP como evidencia contractual.",
                    'details'   => "
                        <p>Nº Documento: {$document}</p>
                        <p>Informe de Actividades: APROBADO</p>
                        <p>Certificado de Cumplimiento Tributario: APROBADO</p>
                        <p class='font-weight-bold'>Número de radicado: {$filed_number}</p>
                        <p>Fecha de Actualización: {$created_at}</p>
                    ",
                    'hide_btn'  => true,
                    'info'      => "Si no puede visualizar el archivo adjunto recuerde que puede descargarlos a través del portal.",
                    'remember_secop'  =>  'Recuerde cargar en el SECOP II los documentos adjuntos en el ítem plan de pagos, clic en crear y posteriormente registre su factura con los documentos e información solicitada y en el campo de acciones dar clic en la opción de Publicar para aprobación de la Entidad Estatal.',
                    'date'      => Carbon::now()->year,
                ])->attachFromStorageDisk('local', "contractor/{$this->file_compliance}", "{$this->file_compliance}", [
                    'mime' => 'application/pdf'
                ])->attachFromStorageDisk('local', "contractor/{$this->file_activity}", "{$this->file_activity}", [
                    'mime' => 'application/pdf'
                ])->attachFromStorageDisk('local', 'app/activity-reports/'.$this->activity_report->contract_number.'/'.$this->activity_report->report_date.'/'.$this->activity_report->social_security_attachment_path, [
                    'mime' => 'application/pdf'
                ])->attachFromStorageDisk('local', 'app/templates/INSTRUCTIVO_CARGUE_SECOP_II.pdf', [
                    'mime' => 'application/pdf'
                ]);

            Storage::disk('local')->delete("contractor/{$this->file_compliance}");
            Storage::disk('local')->delete("contractor/{$this->file_activity}");

            return $email;
        }

    }
}
