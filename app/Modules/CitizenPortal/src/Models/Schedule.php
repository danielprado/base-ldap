<?php

namespace App\Modules\CitizenPortal\src\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasManyThrough;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;
use OwenIt\Auditing\Contracts\Auditable;

class Schedule extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = "mysql_citizen_portal";

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "schedule";

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'weekday_id',
        'daily_id',
        'min_age',
        'max_age',
        'quota',
        'activity_id',
        'stage_id',
        'is_paid',
        'rate_id',
        'rate_value',
        'program_id',
        'fecha_apertura',
        'fecha_cierre',
        'is_activated',
        'disability',
        'project_id',
        'activity_type_id',
        'set_population_params',
        'sex_id',
        'orientation_id',
        'gender_id',
        'ethnic_group_id',
        'population_group_id',
        'disability_id',
        'is_teams',
        'men_participants_num',
        'women_participants_num',
        'staff_participants_num',
        'min_team_participants_quota',
        'max_team_participants_quota'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'program_id'    => 'int',
        'activity_id'   => 'int',
        'stage_id'      => 'int',
        'weekday_id'       => 'int',
        'daily_id'       => 'int',
        'fecha_apertura'=> 'datetime',
        'fecha_cierre'  => 'datetime',
        'min_age'       => 'int',
        'rate_id'       => 'int',
        'rate_value'    => 'float',
        'max_age'       => 'int',
        'quota'         => 'int',
        'is_paid'       => 'boolean',
        'is_activated'  => 'boolean',
        'disability'  => 'boolean',
        'project_id' => 'int',
        'activity_type_id' => 'int',
        'set_population_params' => 'boolean',
        'sex_id' => 'string',
        'orientation_id' => 'string',
        'gender_id' => 'string',
        'ethnic_group_id' => 'string',
        'population_group_id' => 'string',
        'disability_id' => 'string',
        'is_teams' => 'boolean',
        'men_participants_num' => 'int',
        'women_participants_num' => 'int',
        'staff_participants_num' => 'int',
        'min_team_participants_quota' => 'int',
        'max_team_participants_quota' => 'int'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [ 'name' ];

    /*
     * ---------------------------------------------------------
     * Data Change Auditor
     * ---------------------------------------------------------
     */

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [
        'weekday_id',
        'daily_id',
        'min_age',
        'max_age',
        'quota',
        'activity_id',
        'stage_id',
        'is_paid',
        'rate_id',
        'rate_value',
        'program_id',
        'fecha_apertura',
        'fecha_cierre',
        'is_activated',
        'disability',
        'project_id',
        'activity_type_id',
        'set_population_params',
        'sex_id',
        'orientation_id',
        'gender_id',
        'ethnic_group_id',
        'population_group_id',
        'disability_id',
        'is_teams',
        'men_participants_num',
        'women_participants_num',
        'staff_participants_num'
    ];

    /**
     * Generating tags for each model audited.
     *
     * @return array
     */
    public function generateTags(): array
    {
        return ['citizen_portal_schedule'];
    }

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator
     * ---------------------------------------------------------
     */

    /**
     * @return string
     */
    public function getNameAttribute()
    {
        return "{$this->day->name} {$this->hour->name} - ({$this->min_age}-{$this->max_age}) AÑOS";
    }

    /**
     * @param array $data
     * @return array
     */
    public function fillModel(array $data)
    {
        $rate_id = Arr::get($data, 'rate_id');
        $value = null;
        if ($rate_id) {
            $rate = Rate::find($rate_id);
            $value = $rate->value ?? null;
        }
        return [
            'program_id'    => Arr::get($data, 'program_id'),
            'activity_id'   => Arr::get($data, 'activity_id'),
            'stage_id'      => Arr::get($data, 'stage_id'),
            'weekday_id'    => Arr::get($data, 'weekday_id'),
            'daily_id'      => Arr::get($data, 'daily_id'),
            'min_age'       => Arr::get($data, 'min_age'),
            'max_age'       => Arr::get($data, 'max_age'),
            'quota'         => Arr::get($data, 'quota'),
            'fecha_apertura' => Arr::get($data, 'start_date'),
            'fecha_cierre'   => Arr::get($data, 'final_date'),
            'is_paid'        => Arr::get($data, 'is_paid'),
            'rate_id'        => Arr::get($data, 'rate_id'),
            'rate_value'     => $value,
            'is_activated'   => Arr::get($data, 'is_activated'),
            'disability'   => Arr::get($data, 'disability'),
            'min_team_participants_quota'   => Arr::get($data, 'min_team_participants_quota'),
            'max_team_participants_quota'   => Arr::get($data, 'max_team_participants_quota'),
            'project_id' => Arr::get($data, 'project_id'),
            'activity_type_id' => Arr::get($data, 'activity_type_id'),
            'set_population_params' => Arr::get($data, 'set_population_params', 0),
            'sex_id'                => $this->getArrayAsString($data, 'sex_id'),
            'orientation_id'        => $this->getArrayAsString($data, 'orientation_id'),
            'gender_id'             => $this->getArrayAsString($data, 'gender_id'),
            'ethnic_group_id'       => $this->getArrayAsString($data, 'ethnic_group_id'),
            'population_group_id'   => $this->getArrayAsString($data, 'population_group_id'),
            'disability_id'         => $this->getArrayAsString($data, 'disability_id'),
            'is_teams'   => Arr::get($data, 'is_teams', 0),
            'men_participants_num'  => Arr::get($data, 'men_participants_num', 0),
            'women_participants_num'=> Arr::get($data, 'women_participants_num', 0),
            'staff_participants_num'=> Arr::get($data, 'staff_participants_num', 0)
        ];
    }

    function getArrayAsString($data, $key) {
        $value = Arr::get($data, $key);
        if (!isset($value) || empty($value) || !is_array($value)) return null;
        $filtered_array = array_filter($value, function($val) { return $val !== 0; });
        if (empty($filtered_array)) return null;
        return implode(',', $filtered_array);
    }

    /**
     * @param $column
     * @return string
     */
    public function getSortableColumn($column)
    {
        switch ($column) {
            case 'start_date':
                return 'fecha_apertura';
            case 'final_date':
                return 'fecha_cierre';
            case 'created_at':
            case 'updated_at':
            case 'deleted_at':
                return $column;
            default:
                return in_array($column, $this->fillable)
                    ? $column
                    : $this->primaryKey;
        }
    }

    /*
     * ---------------------------------------------------------
     * Eloquent Relationship
     * ---------------------------------------------------------
     */

    /**
     * @return BelongsTo
     */
    public function activities()
    {
        return $this->belongsTo( Activity::class, 'activity_id', 'id');
    }

    /**
     * @return BelongsTo
     */
    public function stage()
    {
        return $this->belongsTo( Stage::class, 'stage_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function users_schedules()
    {
        return $this->hasMany(CitizenSchedule::class, 'schedule_id', 'id');
    }

    /**
     * @return HasManyThrough
     */
    public function consents()
    {
        return $this->hasManyThrough(
            Consent::class,
            ScheduleConsent::class,
            'informed_id',
            'schedule_id'
        );
    }

    /**
     * @return HasOne
     */
    public function rate()
    {
        return $this->hasOne(Rate::class, 'id','rate_id');
    }

    /**
     * @return HasOne
     */
    public function day()
    {
        return $this->hasOne(Day::class, 'id_weekdays_schedule','weekday_id');
    }

    /**
     * @return HasOne
     */
    public function hour()
    {
        return $this->hasOne(Hour::class, 'id_daily_schedule','daily_id');
    }
}
