<?php

namespace App\Modules\AttendanceRecord\src\Models;

use Illuminate\Database\Eloquent\Model;

class Localidad extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_parks';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'localidad';
    protected $primaryKey = 'Id_Localidad';

    public function puntos()
    {
    	return $this->hasMany(Punto::class, 'Id_Localidad');
    }

}
