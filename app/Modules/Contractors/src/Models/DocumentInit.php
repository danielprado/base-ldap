<?php

namespace App\Modules\Contractors\src\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class DocumentInit extends Model
{
    use SoftDeletes;

    const STATUS_REVIEW = 1;

    protected $connection = 'mysql_contractors';
    protected $table = 'documents_initiation';
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];
    public $timestamps = true;

    protected $primaryKey = 'id';
    protected $fillable = [
        'value_month',
        'months_contract',
        'days_contract',
        'review',
        'handles_sessions',
        'total_sessions'
    ];

    public function contract()
    {
        return $this->belongsTo(Contract::class, 'contract_id', 'id');
    }
}