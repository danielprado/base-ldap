<?php

namespace App\Modules\Contractors\src\Mail;

use allejo\Socrata\Exceptions\InvalidResourceException;
use allejo\Socrata\SodaClient;
use allejo\Socrata\SodaDataset;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Storage;

class ReportSecopContractingMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     * @throws InvalidResourceException
     */
    public function build()
    {
        $created_at = now()->format('Y-m-d H:i:s');

        return $this->view('mail.mail')
            ->subject('REPORTE CONTRATOS SECOP')
            ->with([
                'slogan'    => 'Notificación Reporte SECOP',
                'header'    => 'IDRD',
                'title'     => 'Reporte Contratos',
                'content'   => 'Reporte generado, adjunto encontrará reporte de contratos en SECOP',
                'details'   => "<p>Fecha de Creación: {$created_at}</p>",
                'hide_btn'  => true,
                //'url'       =>  url(response()->file(storage_path('app/exports/contracts.csv'))),
                //'info'      =>  "Si no puede visualizar el archivo adjunto de clic sobre el botón a continuación.",
                'year'      =>  Carbon::now()->year
            ]);
    }
}
