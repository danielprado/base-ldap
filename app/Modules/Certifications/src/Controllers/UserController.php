<?php

namespace App\Modules\Certifications\src\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Certifications\src\Constants\Roles;
use Illuminate\Http\JsonResponse;

class UserController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function menu(): JsonResponse
    {
        $menu = collect([
            [
                'icon'  => 'mdi-account-multiple-plus',
                'title' => 'Usuarios',
                'to'    => ['name' => 'certifications-user-admin'],
                'exact' => true,
                'can'   => auth('api')->user()->isAn(Roles::ROLE_ADMIN)
            ],
            [
                'icon'  =>  'mdi-view-dashboard',
                'title' =>  'Dashboard',
                'to'    =>  [ 'name' => 'certifications' ],
                'exact' =>  true,
                'can'   =>  auth('api')->user()->isAn(...Roles::all())
            ],
            [
                'icon'  => 'mdi-help-rhombus',
                'title' => 'Solicitudes',
                'to'    => ['name' => 'certifications-supports'],
                'exact' => true,
                'can'   => auth('api')->user()->isAn(Roles::ROLE_ADMIN, Roles::ROLE_HIRING)
            ],
            [
                'icon'  => 'mdi-folder-file',
                'title' => 'Contratos',
                'to'    => ['name' => 'certifications-contracts'],
                'exact' => true,
                'can'   => auth('api')->user()->isAn(Roles::ROLE_ADMIN, Roles::ROLE_HIRING)
            ],
            [
                'icon'  => 'mdi-file',
                'title' => 'Reportes',
                'to'    => ['name' => 'certifications-reports'],
                'exact' => true,
                'can'   => auth('api')->user()->isAn(...Roles::all())
            ]
        ]);

        return $this->success_message(
            array_values($menu->where('can', true)->toArray())
        );
    }

    public function permissions(): JsonResponse
    {
        return $this->success_message([
            ['name' => 'certifications-super-admin', 'can'  => auth('api')->user()->isA(Roles::ROLE_ADMIN)],
            ['name' => 'certifications-observer', 'can'     => auth('api')->user()->isA(Roles::ROLE_OBSERVER)],
            ['name' => 'certifications-hiring', 'can'     => auth('api')->user()->isA(Roles::ROLE_HIRING)]
        ]);
    }
}
