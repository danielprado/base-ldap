<?php

namespace App\Modules\Certifications\src\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Certifications\src\Models\CertificationContract;
use App\Modules\Certifications\src\Request\ObligationRequest;
use App\Modules\Certifications\src\Request\UpdateObligationRequest;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ObligationController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function store(ObligationRequest $request, CertificationContract $contract): JsonResponse
    {
        $contract->obligations()
            ->create([
                'Numero_Obligacion' => $request->get('number'),
                'Objeto_Obligacion' => $request->get('object')
            ]);

        return $this->success_message(
            __('validation.handler.success'),
            Response::HTTP_CREATED
        );
    }

    public function update(UpdateObligationRequest $request, CertificationContract $contract): JsonResponse
    {
        $obligation = $contract->obligations()
            ->where('Id', $request->get('id'))
            ->firstOrFail();

        $obligation->update([
            'Numero_Obligacion' => $request->get('number'),
            'Objeto_Obligacion' => $request->get('object')
        ]);

        return $this->success_message(__('validation.handler.updated'));
    }

    /**
     * @throws Exception
     */
    public function destroy(CertificationContract $contract, $id): JsonResponse
    {
        if ($id) {
            $contract->obligations()->find($id)->delete();
            $order = 1;
            foreach ($contract->obligations as $obligation) {
                $obligation->Numero_Obligacion = $order;
                $obligation->update();
                $order++;
            }
            return $this->success_message(__('validation.handler.deleted'));
        }
        return $this->error_response(__('validation.handler.unexpected_failure'));
    }
}
