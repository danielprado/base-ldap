<?php

namespace App\Modules\Contractors\src\Resources;

use App\Modules\Contractors\src\Constants\Roles;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Crypt;
use App\Modules\Contractors\src\Resources\ActivityReportEvidenceResource;

class ActivityReportDateResource extends JsonResource
{

    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => isset($this->id) ? (int) $this->id : null,
            'code' => isset($this->code) ? $this->code : null,
            'start_date' => isset($this->start_date) ? (int) $this->start_date : null,
            'final_date' => isset($this->final_date) ? (int) $this->final_date : null
        ];
    }
}
