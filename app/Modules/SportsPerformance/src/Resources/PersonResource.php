<?php

namespace App\Modules\SportsPerformance\src\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Modules\SportsPerformance\src\Resources\PatientResource;

class PersonResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'document_type_code'  => $this->document_type_code ?? 'CC',
            'document'            => $this->Cedula ?? null,
            'sim_person_id'       => $this->Id_Persona ?? null,
            'patient'             => isset($this->patient) ? new PatientResource($this->patient, $this) : null
        ];
    }
}
