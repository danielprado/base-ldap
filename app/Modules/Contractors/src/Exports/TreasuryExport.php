<?php

namespace App\Modules\Contractors\src\Exports;

use App\Models\Security\Subdirectorate;
use App\Models\Security\User;
use App\Traits\AppendHeaderToExcel;
use Carbon\Carbon;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class TreasuryExport implements FromCollection, WithMapping, WithHeadings, WithColumnFormatting, ShouldAutoSize, WithEvents, WithTitle
{
    use Exportable, AppendHeaderToExcel;

    /**
     * @var Collection
     */
    private $payroll;

    /**
     * @var Subdirectorate|null
     */
    private $subdirectorate;

    /**
     * @var User|null
     */
    private $supervisor;


    public function __construct(Collection $payroll, ?Subdirectorate $subdirectorate, ?User $supervisor)
    {
        $this->payroll = $payroll;
        $this->subdirectorate = $subdirectorate;
        $this->supervisor = $supervisor;
    }

    public function collection()
    {
        $data = collect($this->payroll['fullPaymentPlanList']);
        $expandedData = [];
        $contractorCounter = 1;
        $currentContractor = null;
        foreach ($data as $item) {
            foreach ($item['pmrList'] as $pmr) {
                if ($currentContractor !== $item['contractorDocument']) {
                    $currentContractor = $item['contractorDocument'];
                    $counter = $contractorCounter++;
                }

                $mergedItem = array_merge($item, $pmr, [
                    'counter' => $counter,
                    'name' => $this->payroll['name'],
                    'isReserve' => $this->payroll['isReserve'],
                ]);

                $expandedData[] = $mergedItem;
            }
        }

        return collect($expandedData);
    }

    public function headings(): array
    {
        return [
            'No.',
            'FECHA',
            'TIPO',
            'SUBDIRECCIÓN',
            'NOMBRE PLANILLA',
            'NOMBRE CONTRATISTA',
            'DOCUMENTO CONTRATISTA',
            'BITÁCORA',
            'CÓDIGO RUBRO',
            'POSICIÓN',
            'FONDO',
            'POSPRE',
            'VALOR BRUTO',
            'BASE ICA',
            'BASE RETEFUENTE',
            'EST. PCUL.0,5%',
            'EST. PPM.0,2%',
            'ICA',
            'RETFUENTE LEY 1607',
            'OTROS DESCUEN.',
            'TOTAL DEDUCCIONES',
            'VALOR_NETO'
        ];
    }

    public function map($row): array
    {
        return [
            $row['counter'],
            isset($row['createdAt']) ? Carbon::parse($row['createdAt'])->format('Y-m-d') : null,
            !$row['isReserve'] ? 'VIGENCIA' : 'RESERVA',
            $this->subdirectorate->name ?? null,
            $row['name'] ?? null,
            $row['contractorName'] ?? null,
            $row['contractorDocument'] ?? null,
            $row['binnacleBirNumo'] ?? null,
            $row['entry'] ?? null,
            $row['position'] ?? null,
            $row['fundingSource'] ?? null,
            $row['expenseComponent'] ?? null,
            isset($row['totalPaymentMonthPmr']) ? (string) $row['totalPaymentMonthPmr'] : '0',
            isset($row['icaBase']) ? (string) $row['icaBase'] : '0',
            isset($row['retefuenteBase']) ? (string) $row['retefuenteBase'] : '0',
            isset($row['proCultureValue']) ? (string) $row['proCultureValue'] : '0',
            isset($row['proOlderAdultValue']) ? (string) $row['proOlderAdultValue'] : '0',
            isset($row['icaValue']) ? (string) $row['icaValue'] : '0',
            isset($row['retefuenteValue']) ? (string) $row['retefuenteValue'] : '0',
            isset($row['totalOtherDiscounts']) ? (string) $row['totalOtherDiscounts'] : '0',
            isset($row['totalDeductions']) ? (string) $row['totalDeductions'] : '0',
            isset($row['totalNetPayment']) ? (string) $row['totalNetPayment'] : '0',
        ];
    }

    public function columnFormats(): array
    {
        return [
            'M' => NumberFormat::FORMAT_CURRENCY_USD,
            'N' => NumberFormat::FORMAT_CURRENCY_USD,
            'O' => NumberFormat::FORMAT_CURRENCY_USD,
            'P' => NumberFormat::FORMAT_CURRENCY_USD,
            'Q' => NumberFormat::FORMAT_CURRENCY_USD,
            'R' => NumberFormat::FORMAT_CURRENCY_USD,
            'S' => NumberFormat::FORMAT_CURRENCY_USD,
            'T' => NumberFormat::FORMAT_CURRENCY_USD,
            'U' => NumberFormat::FORMAT_CURRENCY_USD,
            'V' => NumberFormat::FORMAT_CURRENCY_USD,
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $sheet = $event->sheet->getDelegate();
                $spreadsheet = $event->sheet->getParent();

                // Agregar el autor al archivo
                $spreadsheet->getProperties()
                    ->setCreator('Brayan Sebastian Aldana Casas')
                    ->setLastModifiedBy($this->supervisor['full_name']);

                // Proteger el archivo para que sea de solo lectura
                /*$sheet->getProtection()->setSheet(true);
                $sheet->getProtection()->setSort(true);
                $sheet->getProtection()->setInsertRows(true);
                $sheet->getProtection()->setFormatCells(true);
                $sheet->getProtection()->setPassword('');*/

                // Insertar filas vacías para ajustar el encabezado
                $sheet->insertNewRowBefore(1, 8);

                // Insertar imagen en el encabezado
                $drawing = new Drawing();
                $drawing->setName('Header Image');
                $drawing->setDescription('Header Image');
                $drawing->setPath(public_path('images/img.png')); // Ruta a tu imagen
                $drawing->setHeight(50);
                $drawing->setCoordinates('B3');
                $drawing->setWorksheet($sheet);

                // Combinar celdas para el encabezado de la imagen
                $sheet->mergeCells('A1:V5');

                $sheet->setCellValue('A6', "CONSECUTIVO PLANILLA: " . $this->payroll['name']);
                $sheet->mergeCells('A6:V6');
                $sheet->getStyle('A6')->getFont()->setBold(true);

                $sheet->setCellValue('A7', "FECHA ELABORACIÓN: " . Carbon::parse($this->payroll['createdAt'])->format('Y-m-d'));
                $sheet->mergeCells('A7:V7');
                $sheet->getStyle('A7')->getFont()->setBold(true);

                $periodPac = Carbon::parse($this->payroll['createdAt'])->format('Y-m');
                $sheet->setCellValue('A8', "PERIODO PAC: " . $periodPac);
                $sheet->mergeCells('A8:V8');
                $sheet->getStyle('A8')->getFont()->setBold(true);

                // Encabezado en negrita y centrado
                $sheet->getStyle('A9:V9')->getFont()->setBold(true);
                $sheet->getStyle('A9:V9')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('A9:V9')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $sheet->getStyle('A9:V9')->getAlignment()->setWrapText(true);

                // Ajuste de ancho de columna automático
                foreach (range('A', 'V') as $columnID) {
                    $sheet->getColumnDimension($columnID)->setAutoSize(true);
                }

                // Ajustar el ancho de las columnas manualmente para que se ajuste al contenido
                $columnWidths = [
                    'A' => 4,
                    'B' => 12,
                    'C' => 12,
                    'D' => 12,
                    'E' => 12,
                    'H' => 12,
                    'I' => 12,
                    'J' => 12,
                    'K' => 12,
                    'L' => 15,
                    'M' => 15,
                    'N' => 15,
                    'O' => 15,
                    'P' => 15,
                    'Q' => 15,
                    'R' => 15,
                    'S' => 15,
                    'T' => 15,
                    'U' => 15,
                    'V' => 15,
                ];

                foreach ($columnWidths as $columnID => $width) {
                    $sheet->getColumnDimension($columnID)->setWidth($width);
                }

                // Columnas a combinar y centrar
                $columnsToMerge = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V'];

                // Columnas a solo centrar contenido
                $columnsToCenter = ['I', 'J', 'K', 'L', 'M'];

                // Combinación y centrado de celdas
                $rows = $sheet->toArray();
                $startRow = 10; // Ajusta según sea necesario
                $highestRow = count($rows);

                foreach ($columnsToMerge as $column) {
                    $previousCounter = null;
                    $previousValue = null;
                    $mergeStartRow = $startRow;

                    for ($i = $startRow; $i <= $highestRow; $i++) {
                        $currentCounter = $rows[$i - 1][0]; // Ajustar para la nueva fila de inicio
                        $currentValue = $rows[$i - 1][ord($column) - 65]; // 65 es el valor ASCII de 'A'

                        if ($currentCounter !== $previousCounter || $currentValue !== $previousValue) {
                            if ($previousValue !== null && $mergeStartRow < $i - 1) {
                                $mergeEndRow = $i - 1;
                                $sheet->mergeCells("$column{$mergeStartRow}:$column{$mergeEndRow}");
                            }
                            // Aplicar alineación a todas las celdas, incluso si solo una fila está presente
                            $sheet->getStyle("$column{$mergeStartRow}:$column{$i}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                            $sheet->getStyle("$column{$mergeStartRow}:$column{$i}")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                            $sheet->getStyle("$column{$mergeStartRow}:$column{$i}")->getAlignment()->setWrapText(true);
                            $mergeStartRow = $i;
                        }

                        if ($currentCounter !== $previousCounter) {
                            $previousCounter = $currentCounter;
                        }
                        $previousValue = $currentValue;
                    }

                    // Último grupo de celdas
                    if ($mergeStartRow <= $highestRow) {
                        $mergeEndRow = $highestRow;
                        $sheet->mergeCells("$column{$mergeStartRow}:$column{$mergeEndRow}");
                        $sheet->getStyle("$column{$mergeStartRow}:$column{$mergeEndRow}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                        $sheet->getStyle("$column{$mergeStartRow}:$column{$mergeEndRow}")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                        $sheet->getStyle("$column{$mergeStartRow}:$column{$mergeEndRow}")->getAlignment()->setWrapText(true);
                    }
                }

                // Centrando contenido de columnas sin combinar
                foreach ($columnsToCenter as $column) {
                    $sheet->getStyle("$column{$startRow}:$column{$highestRow}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                    $sheet->getStyle("$column{$startRow}:$column{$highestRow}")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                    $sheet->getStyle("$column{$startRow}:$column{$highestRow}")->getAlignment()->setWrapText(true);
                }

                // Aplicar bordes a todas las celdas
                $highestColumn = $sheet->getHighestColumn();
                $sheet->getStyle("A9:{$highestColumn}{$highestRow}")
                    ->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

                // Encabezados para la tabla de resumen
                $rowForTitle = $highestRow + 2;
                $sheet->setCellValue("L{$rowForTitle}", "CONSOLIDADO");
                $sheet->mergeCells("L{$rowForTitle}:M{$rowForTitle}");
                $sheet->getStyle("L{$rowForTitle}")->getFont()->setBold(true);
                $sheet->getStyle("L{$rowForTitle}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

                // Añadiendo encabezados de la tabla
                $rowForHeaders = $rowForTitle + 1;
                $sheet->setCellValue("L{$rowForHeaders}", "CADENA PRESUPUESTAL");
                $sheet->setCellValue("M{$rowForHeaders}", "VALOR BRUTO");
                $sheet->getStyle("L{$rowForHeaders}:M{$rowForHeaders}")->getFont()->setBold(true);
                $sheet->getStyle("L{$rowForHeaders}:M{$rowForHeaders}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

                $rowData = $rowForHeaders + 1;
                foreach ($this->payroll['budgetChainTotalList'] as $chain) {
                    $sheet->setCellValue("L{$rowData}", $chain['description']);
                    $sheet->setCellValue("M{$rowData}", $chain['total']);
                    $sheet->getStyle("M{$rowData}")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_USD);
                    $rowData++;
                }

                $totalSum = 0;
                foreach ($this->payroll['budgetChainTotalList'] as $chain) {
                    $totalSum += $chain['total'];
                }

                // Incrementa una fila después de añadir los datos
                $totalRow = $rowData;

                // Colocar el texto "TOTAL" en la columna L
                $sheet->setCellValue("L{$totalRow}", "TOTAL");
                $sheet->getStyle("L{$totalRow}")->getFont()->setBold(true);
                $sheet->getStyle("L{$totalRow}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

                // Colocar la suma total en la columna M
                $sheet->setCellValue("M{$totalRow}", $totalSum);
                $sheet->getStyle("M{$totalRow}")->getFont()->setBold(true);
                $sheet->getStyle("M{$totalRow}")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_USD);
                $sheet->getStyle("M{$totalRow}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

                // Aplicar bordes a la fila de "TOTAL"
                $sheet->getStyle("L{$totalRow}:M{$totalRow}")
                    ->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

                // Aplicar bordes al título "CONSOLIDADO"
                $sheet->getStyle("L{$rowForTitle}:M{$rowForTitle}")
                    ->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

                // Aplicar bordes a las celdas de encabezados
                $sheet->getStyle("L{$rowForHeaders}:M{$rowData}")
                    ->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

                // Insertar el RESUMEN PAC debajo del consolidado
                $rowForTitle = $highestRow + 2;
                $sheet->setCellValue("L{$rowForTitle}", "CONSOLIDADO");
                $sheet->mergeCells("L{$rowForTitle}:M{$rowForTitle}");
                $sheet->getStyle("L{$rowForTitle}")->getFont()->setBold(true);
                $sheet->getStyle("L{$rowForTitle}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

                // Añadir encabezados del resumen PAC
                $rowForPacSummaryTitle = $totalRow + 3; // Incrementa una fila después del total del consolidado
                $sheet->setCellValue("L{$rowForPacSummaryTitle}", "RESUMEN PAC");
                $sheet->mergeCells("L{$rowForPacSummaryTitle}:N{$rowForPacSummaryTitle}");
                $sheet->getStyle("L{$rowForPacSummaryTitle}")->getFont()->setBold(true);
                $sheet->getStyle("L{$rowForPacSummaryTitle}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

                // Añadir encabezados de la tabla PAC
                $rowForPacSummaryHeaders = $rowForPacSummaryTitle + 1;
                $sheet->setCellValue("L{$rowForPacSummaryHeaders}", "DESCRIPCIÓN");
                $sheet->setCellValue("M{$rowForPacSummaryHeaders}", "TOTAL");
                $sheet->setCellValue("N{$rowForPacSummaryHeaders}", "BITÁCORAS");
                $sheet->getStyle("L{$rowForPacSummaryHeaders}:N{$rowForPacSummaryHeaders}")->getFont()->setBold(true);
                $sheet->getStyle("L{$rowForPacSummaryHeaders}:N{$rowForPacSummaryHeaders}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);

                // Añadir los datos del resumen PAC
                $rowForPacSummaryData = $rowForPacSummaryHeaders + 1;
                $pacTotalSum = 0;
                foreach ($this->payroll['pacSummaryList'] as $pacSummary) {
                    $sheet->setCellValue("L{$rowForPacSummaryData}", $this->getDescription($pacSummary['description']));
                    $sheet->setCellValue("M{$rowForPacSummaryData}", $pacSummary['total']);
                    $sheet->getStyle("M{$rowForPacSummaryData}")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_USD);
                    $sheet->setCellValue("N{$rowForPacSummaryData}", $pacSummary['binnacleRanges']);
                    $sheet->getStyle("N{$rowForPacSummaryData}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                    $pacTotalSum += $pacSummary['total'];
                    $rowForPacSummaryData++;
                }

                // Añadir el total general del PAC
                $sheet->setCellValue("L{$rowForPacSummaryData}", "TOTAL");
                $sheet->getStyle("L{$rowForPacSummaryData}")->getFont()->setBold(true);
                $sheet->getStyle("L{$rowForPacSummaryData}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $sheet->setCellValue("M{$rowForPacSummaryData}", $pacTotalSum);
                $sheet->getStyle("M{$rowForPacSummaryData}")->getFont()->setBold(true);
                $sheet->getStyle("M{$rowForPacSummaryData}")->getNumberFormat()->setFormatCode(NumberFormat::FORMAT_CURRENCY_USD);

                // Aplicar bordes a las celdas del resumen PAC
                $sheet->getStyle("L{$rowForPacSummaryHeaders}:N{$rowForPacSummaryData}")
                    ->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

                // Aplicar bordes al título "RESUMEN PAC"
                $sheet->getStyle("L{$rowForPacSummaryTitle}:N{$rowForPacSummaryTitle}")
                    ->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);

            },
        ];
    }

    /**
     * Retorna la descripción en español basada en la clave proporcionada.
     *
     * @param string $description
     * @return string
     */
    public function getDescription(string $description): string
    {
        $descriptions = [
            'AGREEMENTS' => 'ACUERDOS',
            'COMPENSATORY_FUND' => 'FONDO COMPENSATORIO',
            'ECONOMIC_USE' => 'USO ECONÓMICO',
            'PUBLIC_SHOWS' => 'ESPECTÁCULOS PÚBLICOS',
            'SANCTIONS_AND_FINES' => 'SANCIONES Y MULTAS',
            'TRANSFER' => 'TRANSFERENCIA',
            'IS_VALORIZATION' => 'ES VALORIZACIÓN',
        ];

        return $descriptions[$description] ?? $description;
    }

    public function title(): string
    {
        return "REPORTE";
    }
}
