<?php

namespace App\Modules\Certifications\src\Exports;

use App\Models\Security\DocumentType;
use App\Models\Security\User;
use App\Modules\Certifications\src\Models\CertificationAssignment;
use App\Modules\Certifications\src\Models\CertificationContract;
use App\Modules\Certifications\src\Models\CertificationContractType;
use App\Traits\AppendHeaderToExcel;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Builder;
use Imtigger\LaravelJobStatus\JobStatus;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class AssignmentExport implements FromQuery, WithTitle, WithEvents, WithHeadings, WithMapping, WithColumnFormatting, ShouldQueue, ShouldAutoSize
{
    use Exportable, AppendHeaderToExcel;

    /**
     * @var array
     */
    private $request;

    /**
     * @var int
     */
    private $rowNumb = 2;

    /**
     * @var User
     */
    private $user;

    public function __construct(array $request, $job, $user)
    {
        $this->user = $user;
        $this->request = $request;
        update_status_job($job, JobStatus::STATUS_EXECUTING, 'excel-certifications-module');
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return CertificationAssignment::query()
            ->whereKey($this->request['assignments'])
            ->orderBy('created_at', 'desc');
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $sheet) {
                $this->setHeader($sheet->sheet, 'CESIONES - CERTIFICACION DE CONTRATOS', 'A1:K1', 'K');
            }
        ];
    }

    public function headings(): array
    {
        return [
            'ID CESIÓN',
            'ID DEL CONTRATO',
            'NÚMERO CESIÓN',
            'TIPO DE DOCUMENTO CESIONARIO',
            'DOCUMENTO CESIONARIO',
            'NOMBRE CESIONARIO',
            'DÍGITO DE VERIFICACIÓN',
            'VALOR DE LA CESIÓN',
            'FECHA DE LA CESIÓN',
            'FECHA DE CREACIÓN',
            'FECHA DE MODIFICACIÓN',
        ];
    }

    public function title(): string
    {
        return 'CESIONES';
    }

    public function columnFormats(): array
    {
        return [
            'I' => NumberFormat::FORMAT_DATE_YYYYMMDD2.' h'.NumberFormat::FORMAT_DATE_TIME4,
            'J' => NumberFormat::FORMAT_DATE_YYYYMMDD2.' h'.NumberFormat::FORMAT_DATE_TIME4,
            'K' => NumberFormat::FORMAT_DATE_YYYYMMDD2.' h'.NumberFormat::FORMAT_DATE_TIME4,
        ];
    }

    public function map($row): array
    {
        return [
            'id'                => $row['Id'] ?? null,
            'contract_id'       => $row['Contrato_Id'] ?? null,
            'number'            => $row['Numero_Cesion'] ?? null,
            'contract_type'     => $this->getDocumentType($row['Tipo_Documento_Cesionario']) ?? null,
            'document'          => $row['Cedula_Cesionario'] ?? null,
            'name'              => $row['Nombre_Cesionario'] ?? null,
            'dv'                => $row['Dv_Cesion'] ?? null,
            'value'             => $row['Valor_Cedido'] ?? null,
            'date'              => isset($row['Fecha_Cesion']) ? Carbon::parse($row['Fecha_Cesion'])->format('Y-m-d') : null,
            'created_at'        => isset($row['created_at']) ? date_time_to_excel(Carbon::parse($row['created_at'])) : null,
            'updated_at'        => isset($row['updated_at']) ? date_time_to_excel(Carbon::parse($row['updated_at'])) : null,
        ];
    }

    public function getDocumentType($id)
    {
        $document_type_id = isset($id) ? (int) $id : null;
        if ($document_type_id) {
            $document_type = DocumentType::query()->where('Id_TipoDocumento', $document_type_id)->pluck('Descripcion_TipoDocumento');
            return $document_type[0];
        }
        return  $document_type_id;
    }
}
