<?php

namespace App\Modules\Contractors\src\Controllers;

use App\Helpers\SevenWebService;
use App\Http\Controllers\Controller;
use App\Modules\Contractors\src\Models\Contractor;
use App\Modules\Contractors\src\Models\UserSevenProvider;
use App\Modules\Contractors\src\Models\UserSevenProviderDetail;
use App\Modules\Contractors\src\Models\UserSevenThirdParty;
use App\Modules\Contractors\src\Models\UserSevenThirdPartyFinancial;
use App\Modules\Contractors\src\Request\StoreSyncPoPvdorRequest;
use App\Modules\Contractors\src\Resources\UserProviderDetailResource;
use App\Modules\Contractors\src\Resources\UserProviderResource;
use App\Modules\Contractors\src\Resources\UserThirdPartyFinancialResource;
use App\Modules\Contractors\src\Resources\UserThirdPartyResource;
use Exception;
use GuzzleHttp\Client;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ThirdPartyController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function show(Contractor $contractor): JsonResponse
    {
        try {
            $http = new Client([
                'base_uri' => env('URL_DOCKER_SERVER'),
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                ],
            ]);
            $request = [
                'document' => $contractor['document'],
            ];

            $endpoints = [
                "/api/contractors-portal/user-seven-third-party",
                "/api/contractors-portal/user-seven-third-party-financial",
                "/api/contractors-portal/user-seven-provider",
                "/api/contractors-portal/user-seven-provider-detail",
            ];

            $responses = [];
            foreach ($endpoints as $endpoint) {
                $responses[] = $http->post($endpoint, [
                    'json' => $request,
                ]);
            }

            $data = [];
            foreach ($responses as $response) {
                $responseData = json_decode($response->getBody()->getContents(), true);
                $data[] = collect($responseData['data'])->first();
            }

            $provider = $data[2] ?? null;
            $provider['detail'] = $data[3] ?? null;

            $array_check = $this->checkMatch($contractor, $provider, $data[0] ?? null, $data[1] ?? null);

            return $this->success_message(
                [
                    'third_party' => $data[0] ?? null,
                    'third_party_financial' => $data[1] ?? null,
                    'provider' => $provider,
                    'conflicts' => $array_check['conflicts'] ?? [],
                    'conflicts_text' => $array_check['conflicts_text'] ?? [],
                ],
                Response::HTTP_OK,
                null,
                [
                    'keys' => UserProviderResource::headers(),
                    'keysDetail' => UserProviderDetailResource::headers(),
                    'keysThirdParty' => UserThirdPartyResource::headers(),
                    'keysThirdPartyFinancial' => UserThirdPartyFinancialResource::headers(),
                ]
            );

        } catch (Exception $exception) {
            return $this->error_response(
                'No podemos realizar la consulta en este momento, por favor intente más tarde.',
                422,
                $exception->getMessage()
            );
        }
    }

    public function store(StoreSyncPoPvdorRequest $request, Contractor $contractor): JsonResponse
    {
        try {
            $sevenWebService = new SevenWebService();
            $provider = $sevenWebService->storeProvider($request->validated());
            $thirdParty = $sevenWebService->storeThirdParty($request->validated());

            if (!$provider['IsSucessfull'] && !$thirdParty['IsSucessfull']) {
                return $this->error_response(
                    'No podemos realizar la sincronización en este momento, por favor intente más tarde.',
                    Response::HTTP_UNPROCESSABLE_ENTITY,
                    'La sincronización no se realizó con éxito, por favor intente más tarde.'
                );
            }

            $contractor->third_party = 1;
            $contractor->saveOrFail();

            return $this->success_message(
                __('validation.handler.success'),
                Response::HTTP_CREATED,
                Response::HTTP_CREATED,
                [
                    'id'    => $contractor->id
                ]
            );

        } catch (Exception $exception) {
            return $this->error_response(
                'No podemos realizar la consulta en este momento, por favor intente más tarde.',
                422,
                $exception->getMessage()
            );
        }

    }

    public function checkMatch(Contractor $contractor, $provider, $third_party, $third_party_financial): array
    {
        $conflicts = [];
        $conflicts_text = [];

        // Check if there is a bank account and data in SEVEN
        if (is_null($contractor->bank_account()->first()) && !empty($provider) && !empty($third_party) && !empty($third_party_financial)) {
            $conflicts = ['ter_dire', 'ban_codi_text', 'cup_tipo_text', 'cup_nume', 'act_codi'];
            $conflicts_text = ['el contratista aún no ha actualizado datos.', 'Dirección', 'Banco', 'Tipo de cuenta', 'Número de cuenta', 'Código de la actividad económica'];
        } elseif ($contractor->bank_account()->first() && $third_party && $third_party_financial && $provider) {
            // If there is a bank account and data in SEVEN
            // Check provider data conflicts
            $this->checkCommonDataConflicts($provider, $contractor, $conflicts, $conflicts_text);
            // Check third party data conflicts
            $this->checkThirdPartyDataConflicts($third_party, $contractor, $conflicts, $conflicts_text);
            // Check financial data conflicts
            $this->checkFinancialDataConflicts($third_party_financial, $contractor, $conflicts, $conflicts_text);
        }

        return [
            'conflicts' => $conflicts,
            'conflicts_text' => $conflicts_text,
        ];
    }

    private function checkCommonDataConflicts($provider, $contractor, &$conflicts, &$conflicts_text)
    {
        $document_type = $contractor->document_type()->first();
        $economic_activity = $contractor->bank_account ? $contractor->bank_account->economic_activity()->first() : null;

        if ($provider['tip_codi_text'] <> $document_type['name']) {
            $conflicts[] = 'tip_codi_text';
            $conflicts_text[] = "Tipo de documento: " . $document_type['name'];
        }
        // Check economic activity conflict
        if (is_null($economic_activity) || $provider['detail']['act_codi'] <> $economic_activity->code) {
            $conflicts[] = 'act_codi';
            $conflicts_text[] = is_null($economic_activity) ? "Código de la actividad económica: Sin registrar" : "Código de la actividad económica: $economic_activity->code";
        }
    }

    private function checkThirdPartyDataConflicts($third_party, $contractor, &$conflicts, &$conflicts_text)
    {
        $mail_seven = isset($third_party['ter_mail']) ? strtolower($third_party['ter_mail']) : $third_party['ter_mail'];
        $address = str_replace('  ', '', str_replace(array('#', '-'), ' ', $contractor['address']));
        $address_third = preg_replace('/^\s+|\s+$|\s+(?=\s)/', '', $third_party['ter_dire']);

        if ($third_party['ter_celu'] <> $contractor['phone'] && $third_party['ter_ntel'] <> $contractor['phone']) {
            isset($third_party['ter_celu']) ? $conflicts[] = 'ter_celu' : $conflicts[] = 'ter_ntel';
            $conflicts_text[] = "Teléfono: " . $contractor['phone'];
        }
        if ($mail_seven <> $contractor['email']) {
            $conflicts[] = 'ter_mail';
            $conflicts_text[] = "E-mail: " . $contractor['email'];
        }
        if (!str_contains($address, $address_third) && !str_contains($address_third, $address)) {
            $conflicts[] = 'ter_dire';
            $conflicts_text[] = "Dirección: " . $address;
        }
        if ($third_party['ter_acti_text'] <> 'APROBADO') {
            $conflicts[] = 'ter_acti_text';
            $conflicts_text[] = 'Estado del tercero';
        }
    }

    private function checkFinancialDataConflicts($third_party_financial, $contractor, &$conflicts, &$conflicts_text)
    {
        $bank = $contractor->bank_account()->first();
        $account_type = $bank->account_type_id == 2 ? 'A' : 'C';

        if ($third_party_financial['cup_esta_text'] <> 'ACTIVO') {
            $conflicts[] = 'cup_esta_text';
            $conflicts_text[] = 'Estado de la cuenta';
        }

        if ($third_party_financial['ban_codi'] <> $bank->bank_id) {
            $conflicts[] = 'ban_codi_text';
            $conflicts_text[] = "Banco: " . $bank->bank['name'];
        }
        if ($third_party_financial['cup_tipo'] <> $account_type) {
            $conflicts[] = 'cup_tipo';
            $conflicts_text[] = "Tipo de cuenta: " . $bank->account_type['name'];
        }
        if (!str_contains($bank->number, preg_replace('/^\s+|\s+$|\s+(?=\s)/', '', $third_party_financial['cup_nume']))) {
            $conflicts[] = 'cup_nume';
            $conflicts_text[] = "Número de cuenta: " . $bank['number'];
        }
    }

    public function getUserThirdParty(Request $request): JsonResponse
    {
        $data = UserSevenThirdParty::query()
            ->select(['ter_codi', 'ter_dire', 'ter_mail', 'ter_celu', 'ter_acti', 'ter_ntel'])
            ->where('ter_codi', $request->get('document'))
            ->get();

        return $this->success_response(
            UserThirdPartyResource::collection($data)
        );
    }
    public function getUserThirdPartyFinancial(Request $request): JsonResponse
    {
        $data = UserSevenThirdPartyFinancial::query()
            ->select(['pvd_codi','cup_nume','ban_codi','sub_codi','mon_codi','cup_tipo','cup_esta'])
            ->where('pvd_codi', $request->get('document'))
            ->where('cup_esta', '=', 'A')
            ->get();

        return $this->success_response(
            UserThirdPartyFinancialResource::collection($data)
        );
    }

    public function getUserProvider(Request $request): JsonResponse
    {
        $data = UserSevenProvider::query()
            ->select(['pvd_codi', 'pvd_dive', 'pvd_nomb', 'pvr_apel', 'tip_codi', 'pvr_esta', 'pvd_fcre', 'pvd_clas', 'pvr_noco', 'pvr_auto', 'pvr_riva', 'tpr_codi', 'mon_codi', 'pvr_clad'])
            ->where('pvd_codi', $request->get('document'))
            ->get();

        return $this->success_response(
            UserProviderResource::collection($data)
        );
    }

    public function getUserProviderDetail(Request $request): JsonResponse
    {
        $data = UserSevenProviderDetail::query()
            ->select(['pvd_codi', 'dep_nomb', 'coc_codi', 'cim_codi', 'pai_codi', 'dep_codi', 'mun_codi', 'act_codi', 'dep_dire', 'dep_ntel', 'dep_mail'])
            ->where('pvd_codi', $request->get('document'))
            ->get();

        return $this->success_response(
            UserProviderDetailResource::collection($data)
        );
    }

    public function testWS(Request $request): JsonResponse
    {
        try {
            $sevenWebService = new SevenWebService();
            $contract = $sevenWebService->storeContract($request->all());

            if ($contract['Retorno'] == 1) {
                return $this->error_response(
                    'No se pudo crear el contrato en este momento, por favor intente más tarde.',
                    Response::HTTP_UNPROCESSABLE_ENTITY,
                    $contract['TxtError']
                );
            }

            return $this->success_message(
                __('validation.handler.success'),
                Response::HTTP_CREATED,
                Response::HTTP_CREATED,
                $contract
            );
        } catch (Exception $exception) {
            return $this->error_response(
                'No podemos realizar la consulta en este momento, por favor intente más tarde.',
                422,
                $exception->getMessage()
            );
        }

    }

    public function testupdateWS(Request $request): JsonResponse
    {
        try {
            $sevenWebService = new SevenWebService();
            $contract = $sevenWebService->storeContract($request->all());

            if ($contract['Retorno'] == 1) {
                return $this->error_response(
                    'No se pudo crear el contrato en este momento, por favor intente más tarde.',
                    Response::HTTP_UNPROCESSABLE_ENTITY,
                    $contract['TxtError']
                );
            }

            return $this->success_message(
                __('validation.handler.success'),
                Response::HTTP_CREATED,
                Response::HTTP_CREATED,
                $contract
            );
        } catch (Exception $exception) {
            return $this->error_response(
                'No podemos realizar la consulta en este momento, por favor intente más tarde.',
                422,
                $exception->getMessage()
            );
        }

    }
}
