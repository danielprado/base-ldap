<?php

namespace App\Modules\Contractors\src\Exports;

use App\Traits\AppendHeaderToExcel;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class UserSevenExport implements FromCollection, WithMapping, WithColumnFormatting, ShouldAutoSize, WithTitle
{
    use Exportable, AppendHeaderToExcel;

    /**
     * @var int
     */
    private $counter;
    /**
     * @var array
     */
    private $array;

    /**
     * DataSevenExport constructor.
     */
    public function __construct($collection, $counter)
    {
        $this->counter = $counter;
        $this->array = $collection;
    }

    public function collection()
    {
        return $this->array;
    }

    public function columnFormats(): array
    {
        return [
            'C' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'M' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'V' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'X' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'Y' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'Z' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'AC' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'AD' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'AE' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'BC' => NumberFormat::FORMAT_DATE_DDMMYYYY,
        ];
    }

    public function map($row): array
    {
        return [
            'Código Tipo de Operación'              => 800 ?? null,
            'Número de Documento'                   => $this->counter++,
            'Fecha'                                 =>  isset($row['fecha_de_firma']) ? date_time_to_excel(Carbon::parse($row['fecha_de_firma']), 'd/m/Y') : null,
            'Código de la Sucursal'                 => 1 ?? null,
            'Descripción'                           => $row['tipo_de_contrato'] ?? null,
            'Tipo de Documento'                     => 'C' ?? null,
            'Número del Contrato'                   => extract_number_format_contract($row['referencia_del_contrato']) ?? null,
            'Código del Contratista'                => $row['documento_proveedor'] ?? null,
            'Tipo de Contrato'                      => 7 ?? null,
            'Código Calificación del Contrato'      => 3 ?? null,
            'Clase de Contrato'                     => 'O' ?? null,
            'Fuente'                                => 'F' ?? null,
            'Acta de Inicio'                        =>  isset($row['fecha_de_firma']) ? date_time_to_excel(Carbon::parse($row['fecha_de_firma']), 'd/m/Y') : null,
            'Lugar Ejecución del Contrato'          => 'BOGOTA' ?? null,
            'Objeto del Contrato'                   => $this->setObject($row['objeto_del_contrato']),
            'Valor Inicial'                         => $row['valor_del_contrato'] ?? null,
            'Valor IVA'                             => '0' ?? null,
            'Valor Adiciones'                       => '0' ?? null,
            'Porcentaje Cláusula Penal'             => '0' ?? null,
            'Valor Cláusula Penal'                  => '0' ?? null,
            'Moneda'                                => '1' ?? null,
            'Fecha de Tasa'                         =>  isset($row['fecha_de_firma']) ? date_time_to_excel(Carbon::parse($row['fecha_de_firma']), 'd/m/Y') : null,
            'Valor Tasa'                            => '1' ?? null,
            'Fecha de Inicio'                       =>  isset($row['fecha_de_inicio_del_contrato']) ? date_time_to_excel(Carbon::parse($row['fecha_de_inicio_del_contrato']), 'd/m/Y') : date_time_to_excel(Carbon::parse($row['fecha_de_firma']), 'd/m/Y'),
            'Fecha Finalización'                    =>  isset($row['fecha_de_fin_del_contrato']) ? date_time_to_excel(Carbon::parse($row['fecha_de_fin_del_contrato'])->subDay(), 'd/m/Y') : null,
            'Fecha Publicación'                     =>  isset($row['fecha_de_firma']) ? date_time_to_excel(Carbon::parse($row['fecha_de_firma']), 'd-m-Y') : null,
            'Plazo'                                 =>  $this->setPlazo($row['duracion'], $row['unidad_de_duracion'], isset($row['fecha_de_inicio_del_contrato']) ? $row['fecha_de_inicio_del_contrato'] : date_time_to_excel(Carbon::parse($row['fecha_de_firma']), 'd/m/Y'), Carbon::parse($row['fecha_de_fin_del_contrato'])->subDay()),
            'Unidad Plazo'                          => 'D' ?? null,
            'Fecha Perfeccionamiento'               =>  isset($row['fecha_de_firma']) ? date_time_to_excel(Carbon::parse($row['fecha_de_firma']), 'd-m-Y') : null,
            'Fecha Vigencia'                        =>  isset($row['fecha_de_fin_del_contrato']) ? date_time_to_excel(Carbon::parse($row['fecha_de_fin_del_contrato'])->subDay(), 'd/m/Y') : null,
            'Fecha Garantía'                        =>  isset($row['fecha_de_fin_del_contrato']) ? date_time_to_excel(Carbon::parse($row['fecha_de_fin_del_contrato'])->subDay(), 'd/m/Y') : null,
            'Tercero Interventor'                   => '0' ?? null,
            'Tercero Supervisor'                    => '0' ?? null,
            'Tercero Responsable'                   => '0' ?? null,
            'Fuente de Recursos'                    => '0' ?? null,
            'Administrador de los Recursos'         => '0' ?? null,
            'Código Producto o Servicio'            => 460044 ?? null,
            'Cantidad'                              => 1 ?? null,
            'Valor'                                 => $row['valor_del_contrato'] ?? null,
            'Destino'                               => 13 ?? null,
            'Bodega'                                => 105 ?? null,
            'Unidad de Medida'                      => 5 ?? null,
            'Sucursal Detalle'                      => 1 ?? null,
            'Area de Negocios'                      => '0' ?? null,
            'Centro de Costos'                      => '0' ?? null,
            'Proyecto'                              => '0' ?? null,
            'Convenio'                              => '0' ?? null,
            'Tipo Distribución'                     => 'A' ?? null,
            'Identificación Supervisor Técnico1'    => '0' ?? null,
            'Identificación Supervisor Técnico2'    => '0' ?? null,
            'Identificación Supervisor Técnico3'    => '0' ?? null,
            'Origen'                                => 'M' ?? null,
            'Tipo de Operación Documento Base'      => '0' ?? null,
            'Numero Documento Base'                 => 1 ?? null,
            'Fecha de Documento Base'               =>  isset($row['fecha_de_inicio_del_contrato']) ? date_time_to_excel(Carbon::parse($row['fecha_de_inicio_del_contrato']), 'd/m/Y') : date_time_to_excel(Carbon::parse($row['fecha_de_firma']), 'd/m/Y'),
        ];
    }

    public function title(): string
    {
        return 'Contratos ' . now()->year;
    }

    public function setPlazo($duracion, $unidad, $star_date, $final_date)
    {
        if ($unidad == 'Meses' || $unidad == 'Mes(es)') {
            return $duracion * 30;
        } elseif ($unidad == 'Dias' || $unidad == 'día(s)') {
            return $duracion;
        } else {
            $start = Carbon::parse($star_date);
            $end =  Carbon::parse($final_date);
            return $start->diffInDays($end) ?? 'N/D';
        }
    }

    public function setObject($object)
    {
        return mb_strtoupper(preg_replace('/[\r\n"]/', '', $object), 'UTF-8');
    }
}
