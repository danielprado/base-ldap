<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableActivityReportAddColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('activity-reports', function (Blueprint $table) {
            $table->string('peaceandsafe_path', 255)
                ->nullable()
                ->default(null);
      
            $table->string('peaceandsafe_warehouse_path', 255)
                ->nullable()
                ->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contracts', function (Blueprint $table) {
            $table->dropColumn('peaceandsafe_path');
            $table->dropColumn('peaceandsafe_warehouse_path');
        });
    }
}
