<?php

namespace App\Modules\Contractors\src\Mail;

use App\Modules\Contractors\src\Models\Contract;
use App\Modules\Contractors\src\Models\Compliance;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ComplianceRevisionMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Contract
     */
    private $contract;

    /**
     * @var Compliance
     */
    private $compliance;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Contract $contract, Compliance $compliance)
    {
        $this->compliance = $compliance;
        $this->contract = $contract;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $contractFormat = extract_number_and_year_from_contract($this->contract->contract);

        $monthlyUpdatedAt = $this->compliance->updated_at;

        return $this->view('mail.mail')
            ->subject('Certificado de Cumplimiento - Revisión Pendiente')
            ->with([
                'header'    => 'IDRD',
                'title'     => 'Registro Portal Contratista',
                'content'   => "¡Un nuevo certificado de cumplimiento ha sido generado para el contrato {$contractFormat['contract_number']}! Puede realizar la revisión dando click en el siguiente botón.",
                'details'   => "<p>Fecha de Creación: {$monthlyUpdatedAt}</p>",
                'url'       => route('compliance.status.change'),
                'year'      => now()->year
            ]);
    }
}
