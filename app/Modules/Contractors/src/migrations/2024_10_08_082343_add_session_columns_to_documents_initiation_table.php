<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTotalSessionsToDocumentsInitiationTable extends Migration
{
    public function up()
    {
        Schema::table('documents_initiation', function (Blueprint $table) {
            $table->boolean('handles_sessions')->default(0)->after('contract_id');
            $table->unsignedInteger('total_sessions')->default(0)->notNullable()->after('handles_sessions');
        });
    }
    
    public function down()
    {
        Schema::table('documents_initiation', function (Blueprint $table) {
            $table->dropColumn('handles_sessions');
            $table->dropColumn('total_sessions');
        });
    }
}
