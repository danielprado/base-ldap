<?php

namespace App\Modules\Contractors\src\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasOne;
use OwenIt\Auditing\Contracts\Auditable;

class ContractorBank extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'mysql_contractors';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'contractor_bank';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [ 'contractor_id', 'bank_id', 'account_type_id', 'number', 'economic_activity_id'];

    /*
    * ---------------------------------------------------------
    * Data Change Auditor
    * ---------------------------------------------------------
    */

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [
        'contractor_id', 'bank_id', 'account_type_id', 'number', 'economic_activity_id'
    ];

    /**
     * Generating tags for each model audited.
     *
     * @return array
     */
    public function generateTags(): array
    {
        return ['contractors_bank_relation'];
    }

    /*
     * ---------------------------------------------------------
     * Eloquent Relations
     * ---------------------------------------------------------
    */

    /**
     * @return BelongsTo
     */
    public function contractor()
    {
        return $this->belongsTo(Contractor::class, 'contractor_id');
    }

    /**
     * @return HasOne
     */
    public function bank()
    {
        return $this->hasOne(Bank::class, 'id', 'bank_id');
    }

    /**
     * @return HasOne
     */
    public function account_type()
    {
        return $this->hasOne(AccountType::class, 'id', 'account_type_id');
    }

    /**
     * @return HasOne
     */
    public function economic_activity()
    {
        return $this->hasOne(EconomicActivity::class, 'id', 'economic_activity_id');
    }
}
