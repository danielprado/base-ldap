<?php


namespace App\Modules\CitizenPortal\src\Request;



use App\Models\Security\DocumentType;
use App\Modules\CitizenPortal\src\Models\Citizen;
use App\Modules\CitizenPortal\src\Models\PreRegister;
use App\Modules\CitizenPortal\src\Models\Profile;
use Illuminate\Foundation\Http\FormRequest;

class StorePreRegisterRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('api')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $document = new DocumentType();
        $pre = new PreRegister();
        $profile = new Profile();
        $user = new Citizen();
        return [
            'document_type_id'  => "required|numeric|exists:{$document->getConnectionName()}.{$document->getTable()},Id_TipoDocumento",
            'document'  => "required|numeric|unique:{$profile->getConnectionName()}.{$profile->getTable()},document|digits_between:3,20|unique:{$pre->getConnectionName()}.{$pre->getTable()}",
            'name'      =>  'required|string|min:3',
            's_name'      =>  'nullable|string|min:3',
            'surname'      =>  'required|string|min:3',
            's_surname'      =>  'nullable|string|min:3',
            'email'      =>  "required|email|unique:{$user->getConnectionName()}.{$user->getTable()},email|unique:{$pre->getConnectionName()}.{$pre->getTable()},email",
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'document_type_id' =>  __('citizen.validations.document_type'),
            'document' =>  __('citizen.validations.document'),
            'name' =>  __('citizen.validations.name'),
            's_name' =>  __('citizen.validations.s_name'),
            'surname' =>  __('citizen.validations.surname'),
            's_surname' =>  __('citizen.validations.s_surname'),
        ];
    }
}
