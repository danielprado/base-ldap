<?php

namespace App\Console\Commands;

use App\Modules\AttendanceRecord\src\Models\Assist;
use App\Modules\AttendanceRecord\src\Models\Attendee;
use App\Modules\AttendanceRecord\src\Models\Session;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Log;

class RegisterClassSidicu extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'register:class-sidicu';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'actualiza las sesiones de clases proporcionadas por la api de IBO';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        $client = new Client();
        // URL de la API a la que deseas realizar la solicitud
        $url = env('URL_BASE_IBO') . '/api/classes?is_loaded_to_external=false&entityId=idrd';
        $classIds = [];
        try {
            $response = $client->get($url);
            $body = json_decode($response->getBody()->getContents(), true);
            foreach ($body['dataClasses'] as $class) {
                $session = $this->getSession($class['sessionId']);
                if (isset($session['data'])) {
                    $classIds[] = $class['classId'];
                    foreach ($class['assistantsDocuments'] as $assist) {
                        if (isset($assist)) {
                            $this->saveClass($session, $assist);
                        }
                    }
                }
            }
            $this->info(count($classIds));
            if (count($classIds) > 0) {
                $this->updateClassExternal($classIds);
            }
        } catch (\Throwable $th) {
            Log::error('ERROR ' . $th->getMessage());
            $this->info('ERROR ' . $th->getMessage());
        }
    }

    private function getSession($session_id)
    {
        $session = Session::with('recreopersonas', 'recreopersonas.persona', 'cronograma.punto', 'Cronograma.jornada', 'Cronograma.punto.localidad')
            ->where('id', '=', $session_id)
            ->first();
        if ($session) {
            $data = [
                'session_id' => strval($session_id),
                'objetive_session' => $session->Objetivo_General,
                'scenary_session' => $session->Cronograma->punto->Escenario,
                'workingday_session' => $session->Cronograma->jornada->Nombre,
                'documentTeacher' => $session->recreopersonas->persona->Cedula,
                'nameTeacher' => $session->recreopersonas->persona->Primer_Nombre . ' ' . $session->recreopersonas->persona->Primer_Apellido,
                'hour_init' => $session->Inicio,
                'hour_end' => $session->Fin,
                'locality_service' => $session->Cronograma->punto->localidad->Localidad,
                'attention_modality' => $session->Cronograma->Virtual == 'No' || $session->Cronograma->Virtual == null ? 'PRESENCIAL' : 'VIRTUAL',
                'type_population' => 'CUIDADORAS',
                'type_service' => 'ACTIVIDAD FISICA MANZANAS DEL CUIDADO',
                'service_offered' => 'ACTIVIDADES ACUÁTICAS MANZANAS/ACTIVIDADES FISICAS MANZANAS',
                'entity' => 'IDRD(INSTITUTO DISTRITAL DE RECREACION Y DEPORTE)',
                'type_assist' =>  'USUARIO REGULAR',
                "type_location_service" => "URBANA",
                "form_operation" => "PUNTO ANCLA",
                "apple" => "",
                "name_equipment" => "",
                "hour_job" => "CERO/NINGUNA",
                "has_attendant" => "NO",
                "name_attendant" => "",
                "type_document_attendant" => "",
                "number_document_attendant" => "",
                "sex_attendant" => "",
                "phone_attendant" => "",
                "email_attendant" => "",
                "relationship" => "",
                "group_etnic_attendant" => "NINGUNO",
                "observations" => "",
            ];
            return ["data" => $data, "date_assist" => $session->Fecha];
        }
        return ["data" => null, "date_assist" => null];
    }

    private function saveClass($session, $document)
    {
        $attendee = Attendee::where('number_document', $document)->first();
        if ($attendee) {
            $assist = new Assist([
                'attendee_id' => $attendee->id,
                'module_id' => 2,
                'date_assist' => $session['date_assist'],
                'session_id' => $session['data']['session_id'],
                'data' => json_encode($session['data']),
            ]);
            $assist->save();
        }
    }

    private function updateClassExternal($classIds)
    {
        $client = new Client();
        $url = env('URL_BASE_IBO') . '/api/classes';

        $data = [
            'classesIds' => $classIds,
            'is_loaded_to_external' => true,
        ];

        try {
            $response = $client->post($url, [
                'json' => $data,
                'headers' => [
                    'Content-Type' => 'application/json',
                ],
            ]);

            $statusCode = $response->getStatusCode();
            $responseBody = $response->getBody()->getContents();
            $this->info('OK ' . $statusCode);
        } catch (\Throwable $th) {
            Log::error('ERROR ' . $th->getMessage());
            $this->info('ERROR ' . $th->getMessage());
        }
    }
}
