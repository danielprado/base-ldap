<?php

namespace App\Modules\Parks\src\Help;

class ArrayHelper
{
      public static function find($array, $prop_name, $value)
      {
        // $match = array_filter($array, fn($f) => $f[$prop_name] === $value);
        $match = array_filter($array, function($f) use ($prop_name, $value) {
            return $f[$prop_name] === $value;
        });
        if (count($match) > 0)
            return reset($match);
        return null;
      }
}
