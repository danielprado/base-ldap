<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_infoapp')->create('events', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('nombre')->nullable();
            $table->text('nombre2')->nullable();
            $table->text('lugar')->nullable();
            $table->text('resumen')->nullable();
            $table->text('descripcion')->nullable();
            $table->text('link')->nullable();
            $table->date('fecha_fin')->nullable();
            $table->text('imagen')->nullable();
            $table->date('fecha')->nullable();
            $table->text('hora')->nullable();
            $table->unsignedBigInteger('categoria_id');
            $table->text('tipo')->nullable();
            $table->integer('destacado')->nullable();
            $table->text('tipo_poblacion')->nullable();
            $table->text('entrada')->nullable();
            $table->text('localidad')->nullable();
            $table->text('latitud')->nullable();
            $table->text('longitud')->nullable();
            $table->timestamps();
            $table->softDeletes();
            // Foreing key
            $table->foreign('categoria_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_infoapp')->dropIfExists('events');
    }
}
