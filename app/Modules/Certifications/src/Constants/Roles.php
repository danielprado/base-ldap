<?php

namespace App\Modules\Certifications\src\Constants;

class Roles
{
    const ROLE_ADMIN = 'certifications-super-admin';
    const ROLE_OBSERVER = 'certifications-observer';
    const ROLE_HIRING = 'certifications-hiring';

    public static function all(): array
    {
        return [
            self::ROLE_ADMIN,
            self::ROLE_OBSERVER,
            self::ROLE_HIRING,
        ];
    }
    public static function keyed(): array
    {
        return [
          self::ROLE_ADMIN => self::ROLE_ADMIN,
          self::ROLE_OBSERVER => self::ROLE_OBSERVER,
          self::ROLE_HIRING => self::ROLE_HIRING,
        ];
    }

    public static function find($role): ?string
    {
        return self::keyed()[$role] ?? null;
    }

    public static function adminAnd($role)
    {
        $find = self::keyed()[$role] ?? null;
        $roles = [
            Roles::ROLE_ADMIN
        ];
        return $find ? array_push($roles, [$find]) : $roles;
    }

}
