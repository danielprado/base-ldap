<?php

namespace App\Modules\Parks\src\Help;

class FormatEndowment
{

    public static function formatEndowment ($contentFeature){
        $allFeatures = [];
        foreach ($contentFeature as $feature) {
            $allFeatures[] = [
                'UB_REFERENCIA' => isset($feature['attributes']['UB_REFERENCIA']) ? $feature['attributes']['UB_REFERENCIA'] : null,
                'NOMBRE_PARQ' => isset($feature['attributes']['NOMBRE_PARQ']) ? $feature['attributes']['NOMBRE_PARQ'] : null,
                'ID_DOTACION' => isset($feature['attributes']['ID_DOTACION']) ? $feature['attributes']['ID_DOTACION'] : null,
                'NOM_DOTACION' => isset($feature['attributes']['NOM_DOTACION']) ? $feature['attributes']['NOM_DOTACION'] : null,
                'TIPO_DOTACION' => isset($feature['attributes']['TIPO_DOTACION']) ? $feature['attributes']['TIPO_DOTACION'] : null,
                'DESCR' => isset($feature['attributes']['DESCR']) ? $feature['attributes']['DESCR'] : null,
                'COOR_X' => isset($feature['attributes']['COOR_X']) ? $feature['attributes']['COOR_X'] : null,
                'COOR_Y' => isset($feature['attributes']['COOR_Y']) ? $feature['attributes']['COOR_Y'] : null,
                'RIESGO_1' => isset($feature['attributes']['RIESGO_1']) ? $feature['attributes']['RIESGO_1'] : null,
                'RIESGO_2' => isset($feature['attributes']['RIESGO_2']) ? $feature['attributes']['RIESGO_2'] : null,
                'RIESGO_3' => isset($feature['attributes']['RIESGO_3']) ? $feature['attributes']['RIESGO_3'] : null,
                'RIESGO_4' => isset($feature['attributes']['RIESGO_4']) ? $feature['attributes']['RIESGO_4'] : null,
                'MAT_PISO' => isset($feature['attributes']['MAT_PISO']) ? $feature['attributes']['MAT_PISO'] : null,
                'AREA_M2' => isset($feature['attributes']['AREA_M2']) ? $feature['attributes']['AREA_M2'] : null,
                'AREA_JUEGO' => isset($feature['attributes']['AREA_JUEGO']) ? $feature['attributes']['AREA_JUEGO'] : null,
                'CAPACIDAD' => isset($feature['attributes']['CAPACIDAD']) ? $feature['attributes']['CAPACIDAD'] : null,
                'ILUMINACION' => isset($feature['attributes']['ILUMINACION']) ? $feature['attributes']['ILUMINACION'] : null,
                'CUBIERTO' => isset($feature['attributes']['CUBIERTO']) ? $feature['attributes']['CUBIERTO'] : null,
                'CERRAMIENTO' => isset($feature['attributes']['CERRAMIENTO']) ? $feature['attributes']['CERRAMIENTO'] : null,
                'TIPO_CERRAMIENTO_LAT' => isset($feature['attributes']['TIPO_CERRAMIENTO_LAT']) ? $feature['attributes']['TIPO_CERRAMIENTO_LAT'] : null,
                'TIPO_CERRAMIENTO_FRO' => isset($feature['attributes']['TIPO_CERRAMIENTO_FRO']) ? $feature['attributes']['TIPO_CERRAMIENTO_FRO'] : null,
                'CAMERINO' => isset($feature['attributes']['CAMERINO']) ? $feature['attributes']['CAMERINO'] : null,
                'SISTEMA_RIEGO' => isset($feature['attributes']['SISTEMA_RIEGO']) ? $feature['attributes']['SISTEMA_RIEGO'] : null,
                'USO_INCLUYENTE' => isset($feature['attributes']['USO_INCLUYENTE']) ? $feature['attributes']['USO_INCLUYENTE'] : null,
                'FECHA_CREACION' =>  isset($feature['attributes']['FECHA_CREACION']) ? $feature['attributes']['FECHA_CREACION'] : null,
                'FECHA_ACTUALIZACION' => isset($feature['attributes']['FECHA_ACTUALIZACION']) ? $feature['attributes']['FECHA_ACTUALIZACION'] : null,
                'NRO_PISO' => isset($feature['attributes']['NRO_PISO']) ? $feature['attributes']['NRO_PISO'] : null,
                'ESTADO_DIAG' => isset($feature['attributes']['ESTADO_DIAG']) ? $feature['attributes']['ESTADO_DIAG'] : null,

            ];
        }
        return $allFeatures;
    }

}
