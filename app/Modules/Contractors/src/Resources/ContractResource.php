<?php


namespace App\Modules\Contractors\src\Resources;


use App\Modules\Contractors\src\Models\Contract;
use App\Modules\Contractors\src\Models\Contractor;
use App\Modules\Contractors\src\Models\ActivityReport;
use App\Modules\Contractors\src\Models\DocumentInit;
use App\Modules\Contractors\src\Models\PendingContractAddition;
use Illuminate\Http\Resources\Json\JsonResource;
use Carbon\CarbonPeriod;
use NumberFormatter;
use Carbon\Carbon;

class ContractResource extends JsonResource
{
    protected $reports;
    protected $numberContract;
    protected $childContracts = [];

    public function __construct($resource)
    {
        parent::__construct($resource);
        $this->reports = ActivityReport::where('contract_id', $resource->id)->orderBy('id', 'desc')->get();
        $numberArray = array_reverse(explode('-', $this->contract));
        $this->numberContract = $numberArray[1] . '-' . $numberArray[0];
        if (in_array($this->contract_type_id, [Contract::NEW_CONTRACT, Contract::CONTRACT_ASSIGNMENT])){
            $this->childContracts = Contract::where('contract', 'like', '%' . $this->numberContract . '%')
                                ->where('id', '!=', $this->id)
                                ->get();
        }
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $transport = $this->transport ?? null;
        $type = isset($this->contract_type_id) ? (int) $this->contract_type_id : null;
        $left = isset($this->final_date) ? now()->diffInDays($this->final_date, false) : 0;
        if ($type == Contract::CONTRACT_SUSPENSION) {
            $left = isset($this->final_suspension_date) ? now()->diffInDays($this->final_suspension_date, false) : 0;
        }
        $document_init = DocumentInit::where('contract_id', $this->id)->first();
        return [
            'contract_id'                       =>  isset($this->id) ? (int) $this->id : null,
            'contract'                          => $this->contract ?? null,
            'transport'                         =>  $transport,
            'transport_text'                    =>  $transport ? 'SI' : 'NO',
            'position'                          => $this->position ?? null,
            'start_date'                        => $this->getStartDate(),
            'final_date'                        =>  isset($this->final_date) ? $this->final_date->format('Y-m-d') : null,
            'days_left'                         => $left < 1 ? 0 : $left,
            'days_left_text'                    => $this->leftText($left, $type == Contract::CONTRACT_SUSPENSION),
            'start_suspension_date'             =>  isset($this->start_suspension_date) ? $this->start_suspension_date->format('Y-m-d') : null,
            'final_suspension_date'             =>  isset($this->final_suspension_date) ? $this->final_suspension_date->format('Y-m-d') : null,
            'total'                             =>  isset($this->total) ? (int) $this->total : null,
            'total_addition'                    =>  isset($this->total_addition) ? (int) $this->total_addition : null,
            'duration'                          =>  isset($this->duration) ? $this->duration : $this->start_date->diffInMonths($this->final_date, false),
            'duration_days_text'                =>  $this->durationText($document_init),
            'duration_days'                     =>  isset($this->duration_days) ? (int) $this->duration_days : null,
            'day'                               => $this->day ?? [],
            'day_string'                        =>  isset($this->day) ? $this->getOriginal('day') : null,
            'risk'                              => $this->risk ?? null,
            'subdirectorate_id'                 =>  isset($this->subdirectorate_id) ? (int) $this->subdirectorate_id : null,
            'subdirectorate'                    => $this->subdirectorate->name ?? null,
            'dependency_id'                     =>  isset($this->dependency_id) ? (int) $this->dependency_id : null,
            'dependency'                        => $this->dependency->name ?? null,
            'subarea_id'                        =>  isset($this->subarea_id) ? (int) $this->subarea_id : null,
            'subarea'                           => $this->subarea->name ?? null,
            'other_dependency_subdirectorate'   => $this->other_dependency_subdirectorate ?? null,
            'supervisor_email'                  => $this->supervisor_email ?? null,
            'contractor_id'                     =>  isset($this->contractor_id) ? (int) $this->contractor_id : null,
            'lawyer_id'                         =>  isset($this->lawyer_id) ? (int) $this->lawyer_id : null,
            'lawyer'                            => $this->lawyer->full_name ?? null,
            'contractor'                        =>  new ContractorResource($this->whenLoaded('contractor' )),
            'contract_type_id'                  =>  $type,
            'contract_type'                     => $this->contract_type->name ?? null,
            'files'                             =>  FileResource::collection( $this->files ),
            'object_contract'                   =>  $this->object_contract ?? null,
            'report_dates'                      =>  $this->getReportDates($this->start_date, $this->final_date, $this->id),
            'virtual_expedient_number'          =>  $this->getVirtualExpedient(),
            'reports_waiting_count'             =>  $this->getReportsWaitingCount(),
            'value_month'                       =>  $this->getCurrencyFormat(isset($document_init) ? $document_init->value_month : 0),
            'childs_contract'                   =>  $this->childContracts,
            'created_at'                        =>  isset($this->created_at) ? $this->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'                        =>  isset($this->updated_at) ? $this->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }

    /**
     * @param $days
     * @param $isSuspension
     * @return string
     */
    public function leftText($days, $isSuspension = false)
    {
        if ($days > 1) {
            return "Quedan $days días para finalizar.";
        } elseif ($days == 1) {
            return "Queda un día para finalizar.";
        } else {
            return $isSuspension ? 'Suspensión finalizada' : 'Trámite finalizado';
        }
    }

    /**
     * @return string
     */
    private function durationText($document_init)
    {
        if (isset($document_init)){
            $months = $document_init->months_contract;
            $days = $document_init->days_contract;
            if ($days == 0) {
                return "$months meses";
            }
            return "$months meses $days días";    
        }

        $start_date = Carbon::createFromFormat('Y-m-d', substr($this->start_date, 0, 10));
        $final_date = Carbon::createFromFormat('Y-m-d', substr($this->final_date, 0, 10));
        $difference = $start_date->diff($final_date);
        if ($difference->y > 0 || $difference->m > 0) {
            return "{$difference->m} meses" . ($difference->d > 0 ? " {$difference->d} días" : "");
        }
        return $difference->days > 0 ? "{$difference->days} días" : "";
    }
    
    private function getStartDate()
    {
        if (in_array($this->contract_type_id, [Contract::NEW_CONTRACT, Contract::CONTRACT_ASSIGNMENT])){
            return isset($this->start_date) ? $this->start_date->format('Y-m-d') : null;
        }
        $original_contract = Contract::where('contract', 'like', '%' . $this->numberContract . '%')->where('contract_type_id', Contract::NEW_CONTRACT)->first();
        if (isset($original_contract)){
            return isset($original_contract->start_date) ? $original_contract->start_date->format('Y-m-d') : null;
        }
        return null;
    }

    public function getVirtualExpedient(){
        return $this->reports->isNotEmpty() ? $this->reports->first()->virtual_expedient_number : null;
    }

    public function getReportsWaitingCount(){
        $state = ActivityReport::STATUS_APPROVED;
        $filteredReports = $this->reports->filter(function ($report) use ($state) {
            return $report->state != $state;
        });
        return $filteredReports->count();
    }

    public function getReportDates($start_date, $end_date, $contract_id)
    {
        $start = Carbon::parse($start_date);
        $end = Carbon::parse($end_date)->endOfMonth();
        
        if (!$start->isSameDay($start->copy()->startOfMonth())) {
            $start = $start->copy()->startOfMonth();
        }
    
        $period = CarbonPeriod::create($start, '1 month', $end);
        $report_dates = [];
        $total_records = iterator_count($period);
    
        foreach ($period as $index => $date) {
            $month_start = $date->copy()->startOfMonth();
            $month_end = $date->copy()->endOfMonth();
    
            if ($date->equalTo($start)) {
                $month_start = Carbon::parse($start_date);
            }
            if ($date->equalTo($end) || $index + 1 === $total_records) {
                $month_end = $end;
            }
    
            $number = $index + 1;
            $report_date = $month_start->format('Y-m');
    
            if ($this->compareDates($report_date, '2024-06') < 0) continue;
            
            $created = $this->reports->contains('report_date', $report_date);
            $is_last_report = $number === $total_records;
    
            $report_dates[] = [
                'created' => $created,
                'report_number' => $number,
                'report_date' => $report_date,
                'is_last_report' => $is_last_report,
                'activity_period' => [
                    $month_start->format('Y-m-d'),
                    $is_last_report ? Carbon::parse($end_date)->format('Y-m-d') : $month_end->format('Y-m-d')
                ]
            ];
        }
    
        return $report_dates;
    }

    public function compareDates($date1, $date2)
    {
        $carbonDate1 = Carbon::createFromFormat('Y-m', $date1);
        $carbonDate2 = Carbon::createFromFormat('Y-m', $date2);
        if ($carbonDate1->lt($carbonDate2)) {
            return -1;
        } elseif ($carbonDate1->gt($carbonDate2)) {
            return 1;
        } else {
            return 0;
        }
    }

    public function getCurrencyFormat($value)
    {
        $formatter = new NumberFormatter('es_CO', NumberFormatter::CURRENCY);
        return $formatter->formatCurrency($value, 'COP');
    }

    public static function headers()
    {
        return [
            [
                'text' => "#",
                'value'  =>  "contract_id",
            ],
            [
                'text' => 'Número de contrato',
                'value'  =>  'contract',
                'icon'  =>  'mdi-file',
            ],
            [
                'text' => 'Tipo de trámite',
                'value'  =>  'contract_type',
                'icon'  =>  'mdi-clipboard-text-outline',
            ],
            [
                'text' => 'Se suministra transporte',
                'value'  =>  'transport_text',
                'icon'  =>  'mdi-car',
            ],
            [
                'text'  => 'Cargo a desempeñar',
                'value'  => 'position',
                'icon'   => 'mdi-book-account-outline',
            ],
            [
                'text'  => 'Fecha inicio contrato',
                'value'  => 'start_date',
                'icon'   => 'mdi-calendar',
            ],
            [
                'text'  => 'Fecha fin contrato',
                'value'  => 'final_date',
                'icon'   => 'mdi-calendar',
            ],
            [
                'text'  => 'Fecha inicio suspención',
                'value'  => 'start_suspension_date',
                'icon'   => 'mdi-calendar',
            ],
            [
                'text'  => 'Fecha fin suspención',
                'value'  => 'final_suspension_date',
                'icon'   => 'mdi-calendar',
            ],
            [
                'text'  => 'Valor total del contrato',
                'value'  => 'total',
                'icon'   => 'mdi-currency-usd',
            ],
            [
                'text'  => 'Valor total de la adición',
                'value'  => 'total_addition',
                'icon'   => 'mdi-currency-usd',
            ],
            [
                'label'  => 'Duración del contrato',
                'field'  => 'duration_days_text',
                'icon'   => 'mdi-book-clock',
            ],
            [
                'label'  => 'Duración del contrato días',
                'field'  => 'duration_days',
                'icon'   => 'mdi-book-clock',
            ],
            [
                'text'  => 'Día(s) que no trabaja',
                'value'  => 'day_string',
                'icon'   => 'mdi-calendar',
            ],
            [
                'text'  => 'Nivel de riesgo',
                'value'  => 'risk',
                'icon'   => 'mdi-alert-outline',
            ],
            [
                'text'  => 'Subdirección',
                'value'  => 'subdirectorate',
                'icon'   => 'mdi-domain',
            ],
            [
                'text'  => 'Dependencia',
                'value'  => 'dependency',
                'icon'   => 'mdi-layers',
            ],
            [
                'text'  => 'Area',
                'value'  => 'subarea',
                'icon'   => 'mdi-domain',
            ],
            [
                'text'  => 'Otra subdirección o dependencia',
                'value'  => 'other_dependency_subdirectorate',
                'icon'   => 'mdi-domain',
            ],
            [
                'text'  => 'Creado por',
                'value'  => 'lawyer',
                'icon'   => 'mdi-account',
            ],
            [
                'text'  => 'Fecha de Registro',
                'value'  => 'created_at',
                'icon'   => 'mdi-calendar',
            ],
            [
                'text'  => 'Fecha de Actualización',
                'value'  => 'updated_at',
                'icon'   => 'mdi-calendar',
            ],
        ];
    }

}
