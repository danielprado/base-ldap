<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReserveProcessStatusTable extends Migration
{

    protected $connection = 'mysql_contractors';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection($this->connection)->create('reserve_process_status', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('contract_id');
            $table->string('contract', 255);
            $table->integer('validity_year');
            $table->tinyInteger('update_data');
            $table->text('message_data');
            $table->timestamps();
            $table->softDeletes();
            
            $table->foreign('contract_id')
            ->references('id')
            ->on('contracts')
            ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection($this->connection)->dropIfExists('reserve_process_status');
    }
}
