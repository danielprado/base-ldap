<?php

namespace App\Modules\Certifications\src\Request;

use Illuminate\Foundation\Http\FormRequest;

class SupportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'solution'          => 'required|string',
            'contract_document' => 'nullable|numeric',
            'contract_number'   => 'nullable|numeric',
            'contract_year'     => 'nullable|numeric',
        ];
    }

    public function attributes()
    {
        return [
            'solution'          => 'Solución del soporte',
            'contract_document' => 'Número de documento',
            'contract_number'   => 'Número de contrato',
            'contract_year'     => 'Año de contrato'
        ];
    }


}
