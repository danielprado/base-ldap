<?php


namespace App\Modules\Parks\src\Request;


use App\Modules\Passport\src\Constants\Roles;
use Illuminate\Foundation\Http\FormRequest;

class PrintParkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('api')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'park_code'  => 'required|string',
        ];
    }
}
