<?php

namespace App\Modules\Contractors\src\Jobs;

use App\Modules\Contractors\src\Mail\ComplianceCorrectedMail;
use App\Modules\Contractors\src\Mail\ComplianceRejectedMail;
use App\Modules\Contractors\src\Models\Compliance;
use App\Modules\Contractors\src\Models\Contractor;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use SendGrid\Mail\Mail;

class ComplianceCorrectedReminder implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Compliance
     */
    private $compliance;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Compliance $compliance) {
        $this->compliance = $compliance;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mail $mailer)
    {
        $email = config('app.env') == 'local'
            ? env('SAMPLE_EMAIL', 'brayan.aldana@idrd.gov.co')
            : (isset($this->compliance->reviewer) ? $this->compliance->reviewer : null);

        if (isset($email) && filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $mailer->setFrom(
                env('MAIL_CONTRACTS_FROM', 'portal.contratista@idrd.gov.co'),
                env('MAIL_CONTRACTS_FROM_NAME', 'Portal Contratista')
            );
            $mailer->setSubject('Certificado de Cumplimiento Tributario - Corregido');
            $mailer->addTo($email);

            $render = (new ComplianceCorrectedMail($this->compliance))
                ->render();

            $mailer->addContent("text/html", $render);
            $sendgrid = new \SendGrid(config('mail.sendgrid.api'));

            try {
                $response = $sendgrid->send($mailer);
                sendgird_logger(
                    $email,
                    "Certificado de Cumplimiento Tributario - Corregido",
                    $render,
                    [],
                    $response->statusCode(),
                    $response->body()
                );
            } catch (\Exception $e) {
                sendgird_logger(
                    $email,
                    "[EXCEPTION] Certificado de Cumplimiento Tributario - Corregido",
                    $render,
                    [],
                    $e->getCode(),
                    $e->getMessage()
                );
            }
        }
    }
}
