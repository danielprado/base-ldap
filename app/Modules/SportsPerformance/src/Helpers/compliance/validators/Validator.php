<?php

namespace App\Modules\SportsPerformance\src\Helpers\compliance\validators;

interface Validator
{
  function name(): string;
  function validate($object): bool;
}