<?php

namespace App\Http\Controllers\GlobalData;

use App\Http\Controllers\Controller;
use App\Http\Resources\GlobalData\AreaResource;
use App\Http\Resources\GlobalData\SubAreaResource;
use App\Http\Resources\GlobalData\SubdirectorateResource;
use App\Models\Security\Area;
use App\Models\Security\Subdirectorate;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;


class AreaController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return JsonResponse
     */
    public function office()
    {
        return $this->success_response(
            SubdirectorateResource::collection(
                $this->setQuery(
                    Subdirectorate::query()->where('state', '=', 1), 'id')
                    ->orderBy('name')
                    ->get()
            )
        );
    }

    /**
     * @return JsonResponse
     */
    public function areas()
    {
        return $this->success_response(
            AreaResource::collection(Area::query()->orderBy('name')->get())
        );
    }

    /**
     * @param Subdirectorate $office
     * @return JsonResponse
     */
    public function areasOld(Subdirectorate $office)
    {
        return $this->success_response(
            AreaResource::collection( $office->areas )
        );
    }

    /**
     * @param Area $area
     * @return JsonResponse
     */
    public function subareas(Area $area)
    {
        return $this->success_response(
            SubAreaResource::collection( $area->subareas )
        );
    }
}
