<?php

namespace App\Modules\AttendanceRecord\src\Models;

use Illuminate\Database\Eloquent\Model;

class Recreopersonas extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_recreovia';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Recreopersonas';
    protected $primaryKey = 'Id_Recreopersona';

    public function session()
    {
        return $this->hasMany(Session::class,'Id_Recreopersona' );
    }

    public function persona()
    {
        return $this->hasOne(Persona::class,'Id_Persona', 'Id_Persona');
    }
}
