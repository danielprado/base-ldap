<?php

namespace App\Modules\Contractors\src\Controllers;

use allejo\Socrata\Exceptions\InvalidResourceException;
use allejo\Socrata\SodaClient;
use allejo\Socrata\SodaDataset;
use allejo\Socrata\SoqlQuery;
use App\Http\Controllers\Controller;
use App\Models\Security\Subdirectorate;
use App\Modules\Contractors\src\Helpers\SubdirectorateHelper;
use App\Modules\Contractors\src\Models\Contract;
use App\Modules\Contractors\src\Models\ContractFileCountView;
use App\Modules\Contractors\src\Models\Contractor;
use App\Modules\Contractors\src\Models\ContractType;
use App\Modules\Contractors\src\Models\UserSeven;
use App\Modules\Contractors\src\Resources\SubdirectorateResource;
use App\Modules\Contractors\src\Resources\UserSevenSecopResource;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;


class SecopController extends Controller
{
    /**
     * @return SodaDataset
     * @throws InvalidResourceException
     */
    protected function getSodaDatasetContracts(): SodaDataset
    {
        return new SodaDataset(new SodaClient('www.datos.gov.co'), 'jbjy-vk9h');
    }

    /**
     * @throws InvalidResourceException
     */
    public function counter(): JsonResponse
    {
        $ds = $this->getSodaDatasetContracts();
        $soqlQuery = new SoqlQuery();
        $year = now()->year;
        $subYear = now()->subYear()->year;
        $soqlQuery
            ->where("nit_entidad = 860061099
                AND estado_contrato in ('Activo', 'Borrador')
                AND (
                    referencia_del_contrato LIKE 'IDRD-CTO_%$year'
                    OR referencia_del_contrato LIKE 'IDRD-CTO_%$subYear'
                    OR referencia_del_contrato LIKE 'IDRD-STRD-CPS-%$year%'
                    OR referencia_del_contrato LIKE 'IDRD-STRD-CPS-%-%$year'
                    OR referencia_del_contrato LIKE 'IDRD-SAF-CPS-%$year%'
                    OR referencia_del_contrato LIKE 'IDRD-SAF-CPS-%-%$year'
                    OR referencia_del_contrato LIKE 'IDRD-STP-CPS-%$year%'
                    OR referencia_del_contrato LIKE 'IDRD-STP-CPS-%-%$year'
                    OR referencia_del_contrato LIKE 'IDRD-STC-CPS-%$year%'
                    OR referencia_del_contrato LIKE 'IDRD-STC-CPS-%-%$year'
                )
            ")
            ->limit(10000);

        $contracts_portal = Contract::query()
            ->where('contract', 'like', "IDRD-CTO-_%$year")
            ->orWhere('contract', 'like', "IDRD-CTO-_%$subYear")
            ->count();

        $not_portal = collect($ds->getData($soqlQuery))->count() - $contracts_portal;

        return $this->success_message([
            'secop' => collect($ds->getData($soqlQuery))->count(),
            'portal' => $contracts_portal,
            'not_portal' => max($not_portal, 0),
            'not_arl'   => ContractFileCountView::withoutArl()->count(),
            'not_data' => Contractor::query()
                ->whereNotNull('modifiable')
                ->whereHas('contracts', function ($query) use ($year, $subYear) {
                    return $query
                        ->where('contract', 'like', "IDRD-CTO-_%$year")
                        ->orWhere('contract', 'like', "IDRD-CTO-_%$subYear")
                        ->whereNull('subdirectorate_id');
                })->count(),
        ]);
    }

    public function stats()
    {
        $year = now()->year;

        return $this->success_message([
            'types' => ContractType::query()->withCount([
                'contracts',
                'contracts as contracts_count_year' => function ($query) use ($year) {
                    $query->where('contract', 'like', "IDRD-CTO-_%$year");
                },
            ])->with(['contracts' => function ($query) use ($year) {
                $query->addselect('contract_type_id', DB::raw("MONTH(start_date) month"), DB::raw('count(id) as `count`'))
                    ->where('contract', 'like', "IDRD-CTO-_%$year")
                    //->whereDate('final_date', '>=', now()->format('Y-m-d'))
                    ->groupBy(['contract_type_id', 'month'])
                    ->orderBy('start_date');
            }])->get()
        ]);
    }

    /**
     * @throws InvalidResourceException
     */
    public function userContract(Request $request): JsonResponse
    {
        $date = Carbon::now()->format('Y-m-d');
        $contracts = get_contracts_secop_api($request->get('document'));
        if ($contracts->isEmpty()) {
            return $this->error_response(
                __('contractor.not_found_secop'),
                Response::HTTP_NOT_FOUND
            );
        } else {
            $contracts = $contracts->where('fecha_de_fin_del_contrato', '>=', $date);
            $data = $contracts;
            $data = $data->map(function ($item) {
                if (strpos($item['referencia_del_contrato'], 'CPS') !== false) {
                    $item['current'] = true;
                } else {
                    $item['current'] = false;
                }
                return $item;
            });

            return $this->success_message($data->toArray());
        }

    }

    /**
     * @throws InvalidResourceException
     */
    public function contract(Request $request): JsonResponse
    {
        $contracts = get_contracts_secop_api($request->get('document'));
        $contractor = Contractor::query()->where('document', $request->input('document'))->first();
        if ($contracts->isEmpty()) {
            return $this->error_response(
                __($contractor->name . ' No tiene contratos nuevos.'),
                Response::HTTP_NOT_FOUND
            );
        } else {
            $contracts = $contracts->whereIn('estado_contrato', ['Activo', 'En aprobación', 'cedido']);
            $data = $contracts;
            $data = $data->map(function ($item) {
                if (strpos($item['referencia_del_contrato'], 'CPS') !== false) {
                    $item['current'] = true;
                } else {
                    $item['current'] = false;
                }
                return $item;
            });

            return $this->success_message($data->toArray());
        }
    }

    /**
     * @return JsonResponse
     */
    public function getUserSevenBySecop(): JsonResponse
    {
        $data = UserSeven::query()
            ->where('CON_ESTA', '=', 'A')
            ->where('CON_ANOP', now()->year)
            ->orWhere('CON_ANOP', now()->subYear()->year)
            ->limit(10000)
            ->get(['TER_CODI', 'CON_NCON', 'CON_ANOP', 'CON_NUME']);
        return  $this->success_response(
            UserSevenSecopResource::collection($data)
        );
    }

    public function getAreasAndSubdirectorates()
    {
        return $this->success_response(
            SubdirectorateResource::collection(Subdirectorate::where('state', '=', 1)->get())
        );
    }
}
