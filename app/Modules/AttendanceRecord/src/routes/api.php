<?php

use App\Modules\AttendanceRecord\src\Controllers\AttendanceController;
use App\Modules\AttendanceRecord\src\Controllers\InfoAppController;
use App\Modules\AttendanceRecord\src\Controllers\InfoController;
use App\Modules\AttendanceRecord\src\Controllers\ReportController;
use Illuminate\Support\Facades\Route;


Route::prefix('attendance-record')->group(function () {
      // Public routes
      Route::get('eps', [InfoController::class, 'geteps']);
      Route::get('countries', [InfoController::class, 'getcountries']);
      Route::post('states', [InfoController::class, 'getstates']);
      Route::post('cities', [InfoController::class, 'getcities']);
      Route::get('localities', [InfoController::class, 'getlocalities']);
      Route::post('upz', [InfoController::class, 'getupz']);
      Route::post('massiveInsertion', [ReportController::class, 'massiveInsertion']);

      // Public privates
      Route::post('getuser', [AttendanceController::class, 'getuser']);
      Route::post('getuserrecreovia', [AttendanceController::class, 'getuserrecreovia']);
      Route::post('create', [AttendanceController::class, 'create']);
      Route::get('excel/{dateInit}/{dateEnd}', [ReportController::class, 'excelsidicu']);
      Route::get('excel_actividad_fisica/{dateInit}/{dateEnd}', [ReportController::class, 'excelActividadFisica']);

      // routes info app vive idrd guardar informacion

      Route::post('save-info', [InfoAppController::class, 'saveInfoApp']);
});
