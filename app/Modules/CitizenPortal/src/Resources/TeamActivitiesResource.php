<?php


namespace App\Modules\CitizenPortal\src\Resources;


use App\Modules\CitizenPortal\src\Models\Profile;
use App\Modules\CitizenPortal\src\Models\Status;
use App\Modules\PaymentGateway\src\Models\Pago;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class TeamActivitiesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            $this->merge(new ScheduleResource($this->schedule_view)),
            'schedule_status_id'    => null,
            'citizen_schedule_id'    => $this->id,
            'schedule_status_color' => null,
            'schedule_status_name'  => null,
            'citizen_schedule_payment_files'  => [],
            'citizen_pse_reference' => null,
            'citizen_schedule_payment_at'   => null,
            'citizen_schedule_created_at'   => null,
            'citizen_schedule_updated_at'   => null
        ];
    }

    public static function headers()
    {
        $headers = ProfileResource::headers()['headers'];
        $expanded = ProfileResource::headers()['expanded'];
        $headers = array_insert_in_position($headers, [
            [
                'align' => "center",
                'text' => 'Estado inscripción',
                'value'  =>  "schedule_status_name",
                'sortable' => false
            ]
        ], 2);
        return [
            'headers'   => $headers,
            'expanded'   => $expanded,
        ];
    }
}
