<?php


namespace App\Modules\CitizenPortal\src\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\CitizenPortal\src\Models\Rate;
use App\Modules\CitizenPortal\src\Request\RateRequest;
use App\Modules\CitizenPortal\src\Resources\RateResource;
use App\Modules\PaymentGateway\src\Models\ServiceOffered;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class RateController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
        /*
        $this->middleware(Roles::actions(Rate::class, 'view_or_manage'))
            ->only('index');
        $this->middleware(Roles::actions(Rate::class, 'create_or_manage'))
            ->only('store');
        $this->middleware(Roles::actions(Rate::class, 'update_or_manage'))
            ->only('update');
        $this->middleware(Roles::actions(Rate::class, 'destroy_or_manage'))
            ->only('destroy');
        */
    }

    /**
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $query = $this->setQuery(Rate::query(), (new Rate)->getSortableColumn($this->column))
            ->when(isset($this->query), function ($query) {
                return $query->where('name', 'like', "%$this->query%");
            })
            ->orderBy((new Rate)->getSortableColumn($this->column), $this->order);
        return $this->success_response(
            RateResource::collection(
                (int) $this->per_page > 0
                ? $query->paginate( $this->per_page )
                : $query->get()
            ),
            Response::HTTP_OK,
            [
                'headers'   => RateResource::headers()
            ]
        );
    }

    public function pse_services()
    {
        return $this->success_message(
            ServiceOffered::all()->map(function ($model) {
                return [
                    "id" => $model->getKey(),
                    "name" => toUpper($model->servicio_nombre ?? null),
                    "pse_code" => toUpper($model->codigo_servicio ?? null)
                ];
            })
        );
    }

    public function store(RateRequest $request)
    {
        $model = new Rate();
        $model->fill($request->validated());
        $model->save();
        return $this->success_message(
            __('validation.handler.success'),
            Response::HTTP_CREATED
        );
    }

    public function update(RateRequest $request, Rate $rate)
    {
        $rate->fill($request->validated());
        $rate->save();
        return $this->success_message(
            __('validation.handler.updated')
        );
    }

    public function destroy(Rate $rate)
    {
        $rate->delete();
        return $this->success_message(
            __('validation.handler.deleted'),
            Response::HTTP_OK,
            Response::HTTP_NO_CONTENT
        );
    }
}
