<?php

namespace App\Modules\Certifications\src\Request;

use Illuminate\Foundation\Http\FormRequest;

class AssignmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number'            => 'required|numeric',
            'document_type_id'  => 'required|numeric',
            'document'          => 'required|numeric',
            'dv'                => 'required|numeric',
            'name'              => 'required|string',
            'value'             => 'required|numeric',
            'date'              => 'required|date',
        ];
    }

    public function attributes()
    {
        return [
            'number'            => 'Número de la cesión',
            'document_type_id'  => 'Tipo de documento',
            'document'          => 'Documento',
            'dv'                => 'Dígito de verificación',
            'name'              => 'Nombre',
            'value'             => 'Valor cedido',
            'date'              => 'Fecha de la cesión',
        ];
    }
}
