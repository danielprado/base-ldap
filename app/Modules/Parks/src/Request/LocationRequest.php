<?php

namespace App\Modules\Parks\src\Request;

use App\Modules\Parks\src\Constants\Roles;
use App\Modules\Parks\src\Models\Location;
use App\Modules\Parks\src\Rules\ParkFinderRule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @bodyParam name string required Nombre de la localidad máximo 50 caracteres. Example: USAQUÉN
 */
class LocationRequest extends FormRequest
{
    public $forUpdate = false;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $method = toLower($this->getMethod());
        $action = in_array($method, ['put', 'patch']) ? 'update' : 'create';
        $this->forUpdate = $action == 'update';
        /*
        return
            auth('api')->user()->can(Roles::can(Location::class, $action), Location::class) ||
            auth('api')->user()->can(Roles::can(Location::class, 'manage'), Location::class);
        */
        return auth('api')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $locality = new Location();
        $add = $this->forUpdate ? ",{$this->route('location')->id},{$locality->getKeyName()}" : "";
        return [
            'name'  => "required|string|max:50|unique:{$locality->getConnectionName()}.{$locality->getTable()},Localidad{$add}",
        ];
    }
}
