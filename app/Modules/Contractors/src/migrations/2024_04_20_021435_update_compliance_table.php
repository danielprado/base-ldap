<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateComplianceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('compliance', function (Blueprint $table) {
            $table->boolean('retention')
                ->after('withholding_value')
                ->default(false)
                ->comment('retención');

            $table->boolean('responsible_iva')
                ->after('retention')
                ->default(false)
                ->comment('responsable de iva');

            $table->boolean('withholding')
                ->after('responsible_iva')
                ->default(false)
                ->comment('es responsable de iva');

            $table->boolean('third_people')
                ->after('withholding')
                ->default(false)
                ->comment('terceros intereses de vivienda');

            $table->boolean('spouse_tax')
                ->after('third_people')
                ->default(false)
                ->comment('cónyuge intereses de vivienda');

            $table->string('percentage')
                ->after('spouse_tax')
                ->default(false)
                ->comment('porcentaje');

            $table->timestamp('modifiable')
                ->after('status')
                ->nullable()
                ->comment('El certificado se puede modificar si el campo contiene una fecha');

            $table->boolean('afc_voluntary')
                ->after('modifiable')
                ->default(false)
                ->comment('hizo aportes voluntarios a cuentas AFC');

            $table->boolean('afp_voluntary')
                ->after('afc_voluntary')
                ->default(false)
                ->comment('hizo aportes voluntarios a pensión');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('compliance', function (Blueprint $table) {
            $table->dropColumn([
                'retention',
                'responsible_iva',
                'withholding',
                'third_people',
                'spouse_tax',
                'percentage',
                'modifiable',
                'afc_voluntary',
                'afp_voluntary',
            ]);
        });
    }
}
