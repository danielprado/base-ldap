<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePmrConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pmr_configurations', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('Identificador del registro');
            $table->unsignedBigInteger('payment_plan_id')->nullable()->comment('Identificador del plan de pago');
            $table->integer('pmr_seven_id')->nullable()->comment('Identificador PMR SEVEN');
            $table->integer('rp_seven_id')->nullable()->comment('Identificador RP SEVEN');
            $table->string('description', 255)->comment('Descripción de la configuración');
            $table->string('rp_nume', 255)->comment('Número del RP');
            $table->string('position', 255)->comment('Posición');
            $table->integer('balance')->comment('Balance PMR');
            $table->integer('total_pay_month')->comment('Total mes PMR');
            $table->integer('adjusted_units')->default(0)->comment('Ajuste unidades');
            $table->string('funding_source', 255)->comment('Fuente de financiamiento');
            $table->tinyInteger('reserve')->comment('Es reserva');
            $table->string('entry', 255)->comment('Entrada');
            $table->string('financial_type', 255)->comment('Tipo de financiación');
            $table->string('destinity_code', 255)->comment('Código de destino');
            $table->string('expense_component', 255)->comment('Componente de gastos');
            $table->integer('invoicing_fac_cont')->nullable()->comment('Facturación cont');
            $table->dateTime('binnacle_date')->nullable()->comment('Fecha de bitácora');
            $table->integer('binnacle_bir_numo')->nullable()->comment('Bitácora numo');
            $table->integer('binnacle_bir_cont')->nullable()->comment('Bitácora cont');
            $table->dateTime('invoicing_date')->nullable()->comment('Fecha de facturación');
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('payment_plan_id')
                ->references('id')
                ->on('plans')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pmr_configurations');
    }
}
