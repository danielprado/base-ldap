<?php

namespace App\Modules\Certifications\src\Mail;

use App\Models\Security\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ReportMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $file;

    /**
     * @var $url
     */
    private $url;

    /**
     * Create a new job instance.
     *
     * @param User $user
     * @param $file
     * @param $url
     */
    public function __construct(User $user, $file, $url)
    {
        $this->user = $user;
        $this->file = $file;
        $this->url = $url;
    }

    public function build()
    {
        $created_at = now()->format('Y-m-d H:i:s');
        $name = isset($this->user) ? $this->user->name : null;

        return $this->view('mail.mail')
            ->subject('REPORTE - CERTIFICACIÓN DE CONTRATOS')
            ->with([
                'slogan'    => 'Notificación Certificación Contratos',
                'header'    => 'IDRD',
                'title'     => 'Reporte',
                'content'   => "¡$name! su reporte se ha generado, la solicitud de reporte se ha completado correctamente puede hacer clic en el botón descargar para obtener el archivo.",
                'details'   => "
                    <p>Nombre Solicitante: {$this->user->name}</p>
                    <p>Dependencia: {$this->user->dependency}</p>
                    <p>Fecha de Solicitud: {$created_at}</p>
                ",
                'btn_text'  => 'Descargar',
                'url'       =>  $this->url,
                //'info'      =>  "Si no puede visualizar el archivo adjunto de clic sobre el botón a continuación.",
                'year'      =>  now()->year
            ]);

    }
}
