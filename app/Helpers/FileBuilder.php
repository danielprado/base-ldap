<?php

namespace App\Helpers;

interface FileBuilder
{
  public static function build($model);
  public function from($origin);
  public function with(string $key, $file);
  public function store();
}