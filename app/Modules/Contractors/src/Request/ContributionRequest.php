<?php


namespace App\Modules\Contractors\src\Request;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ContributionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'report_id'  =>  'required|numeric',
            'payroll_number'  =>  'required|string',
            'payment_date'  =>  'required|string',
            'eps_name'  =>  'required|string',
            'afp_name'  =>  'required|string',
            'arl_name'  =>  'required|string',
            'eps_value'  =>  'required|numeric',
            'afp_value'  =>  'required|numeric',
            'arl_value'  =>  'required|numeric',
            'total'  =>  'required|numeric',
            'is_new_contractor'  =>  'required|boolean',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'report_id'  =>  'id reporte',
            'payroll_number'  =>  'número planilla',
            'payment_date'  =>  'fecha de pago',
            'eps_name'  =>  'empresa eps',
            'afp_name'  =>  'empresa pensión',
            'arl_name'  =>  'empresa arl',
            'eps_value'  =>  'valor aporte eps',
            'afp_value'  =>  'valor aporte pensión',
            'arl_value'  =>  'valor aporte arl',
            'total'  =>  'valor total',
            'is_new_contractor'  =>  'es contratista nuevo',
            'support'   =>  'archivo soporte'
        ];
    }
}
