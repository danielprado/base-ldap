<?php

namespace App\Modules\Certifications\src\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Certifications\src\Models\CertificationContract;
use App\Modules\Certifications\src\Request\ExtensionRequest;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ExtensionController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function store(ExtensionRequest $request, CertificationContract $contract): JsonResponse
    {
        $contract->extensions()
            ->create([
                'Numero_Prorroga' => $request->get('number'),
                'Meses' => $request->get('months'),
                'Dias' => $request->get('days'),
                'Fecha_Fin' => $request->get('final_date'),
            ]);

        return $this->success_message(
            __('validation.handler.success'),
            Response::HTTP_CREATED
        );
    }

    public function update(ExtensionRequest $request, CertificationContract $contract): JsonResponse
    {
        $extension = $contract->extensions()
            ->where('Id', $request->get('id'))
            ->firstOrFail();

        $extension->update([
            'Numero_Prorroga' => $request->get('number'),
            'Meses' => $request->get('months'),
            'Dias' => $request->get('days'),
            'Fecha_Fin' => $request->get('final_date'),
        ]);

        return $this->success_message(__('validation.handler.updated'));
    }

    /**
     * @throws Exception
     */
    public function destroy(CertificationContract $contract, $id): JsonResponse
    {
        $extension = $contract->extensions()->find($id);
        if ($extension) {
            $extension->delete();
            $order = 1;
            foreach ($contract->extensions as $extension) {
                $extension->Numero_Prorroga = $order;
                $extension->update();
                $order++;
            }
            return $this->success_message(__('validation.handler.deleted'));
        }
        return $this->error_response(__('validation.handler.unexpected_failure'));
    }
}
