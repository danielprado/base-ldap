<?php

namespace App\Modules\Contractors\src\Models;

//use Illuminate\Database\Eloquent\Model;
use Yajra\Oci8\Eloquent\OracleEloquent as Model;

class UserSevenBank extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'oracle';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'ct_plani';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ter_codi';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ter_codi',
        'tip_cuen',
        'cue_nume',
        'ban_codi',
        'ban_nomb',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'ban_codi' => 'int',
    ];
}
