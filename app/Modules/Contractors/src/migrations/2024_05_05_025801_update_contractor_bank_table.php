<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateContractorBankTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contractor_bank', function (Blueprint $table) {
            $table->unsignedBigInteger('economic_activity_id')->after('number')->nullable()->comment('Código de la actividad económica');
            $table->foreign('economic_activity_id')
                ->references('id')
                ->on('economic_activities')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contractor_bank', function (Blueprint $table) {
            $table->dropColumn('economic_activity_id');
            $table->dropForeign('economic_activity_id_foreign');
        });
    }
}
