<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePayrollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('payrolls', function (Blueprint $table) {
            $table->bigIncrements('id')->comment('Identificador del registro');
            $table->unsignedBigInteger('subdirection_id')->nullable()->comment('Subdirección a la que pertenece');
            $table->unsignedBigInteger('subdirector_id')->nullable()->comment('Identificador del Usuario LDAP');
            $table->enum('status', ['PENDING', 'BINNACLE', 'INVOICING', 'PAYMENT'])->default('PENDING')->comment('[PENDING] creada la ordenación del gasto, [BINNACLE] aplicadas todas las bitacoras, [INVOICING] aplicadas todas las causaciones, [PAYMENT]  pagada');
            $table->integer('year')->comment('Año de la planilla');
            $table->integer('month')->comment('Mes de la planilla');
            $table->timestamps();
            $table->softDeletes();

            $ldap_database = env('DB_LDAP_DATABASE');
            $table->foreign('subdirection_id')
                ->references('id')
                ->on("{$ldap_database}.subdirectorates")
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
            $table->foreign('subdirector_id')
                ->references('id')
                ->on("{$ldap_database}.users")
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('payrolls');
    }
}
