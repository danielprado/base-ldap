<?php

namespace App\Modules\Contractors\src\Models;

use Illuminate\Database\Eloquent\Model;

class UserSevenProviderDetail extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'oracle_ws';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'seven.po_depro';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'pvd_codi';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'pvd_codi',
        'dep_nomb',
        'coc_codi',
        'cim_codi',
        'pai_codi',
        'dep_codi',
        'mun_codi',
        'act_codi',
        'dep_dire',
        'dep_ntel',
        'dep_mail',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'coc_codi' => 'int',
        'cim_codi' => 'int',
        'pai_codi' => 'int',
        'dep_codi' => 'int',
        'mun_codi' => 'int',
        'act_codi' => 'int',
    ];
}
