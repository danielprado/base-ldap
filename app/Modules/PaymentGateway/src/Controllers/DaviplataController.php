<?php

namespace App\Modules\PaymentGateway\src\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\PaymentGateway\src\Help\Helpers;
use App\Modules\PaymentGateway\src\Models\Pago;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Ramsey\Uuid\Uuid;

/**
 * @group Pasarela de pagos - Parques
 *
 * API para la gestión y consulta de datos de Parques Pse
 */
class DaviplataController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function authDaviplata()
    {
        try {
            $help = new Helpers();
            $http = new Client();
            $response = $http->post(env('URL_BASE_DAVIPLATA') . '/oauth2Provider/type1/v1/token', [
                'headers' => [
                    "Accept" => 'application/json',
                    "Content-Type" => 'application/x-www-form-urlencoded',
                ],
                'cert' => storage_path('app/certifiedDaviplata/cert_dummy_lab_v2.crt'),
                'ssl_key' => storage_path('app/certifiedDaviplata/cert_dummy_lab_key_v2.pem'),
                'form_params' => [
                    'grant_type' => 'client_credentials',
                    'client_id' => env('CLIENT_ID'),
                    'client_secret' => env('CLIENT_SECRET'),
                    'scope' => 'daviplata'
                ]
            ]);
            $responseAuthDaviplata = json_decode($response->getBody()->getContents(), true);
            return $help->messagesuccess('', $responseAuthDaviplata, 200);
        } catch (\Exception $ex) {
            return $help->messageerror('Error en la transacción', $ex->getMessage(), 500);
        }
    }

    public function buysDaviplata(Request $request)
    {
        try {
            $help = new Helpers();
            $http = new Client();
            $response = $http->post(env('URL_BASE_DAVIPLATA') . '/daviplata/v1/compra', [
                'headers' => [
                    "Accept" => "application/json",
                    "Authorization" => 'Bearer ' . $request->token,
                    "Content-Type" => "application/json",
                    "x-ibm-client-id" => env('CLIENT_ID')
                ],
                'cert' => storage_path('app/certifiedDaviplata/cert_dummy_lab_v2.crt'),
                'ssl_key' => storage_path('app/certifiedDaviplata/cert_dummy_lab_key_v2.pem'),
                'json' => [
                    'valor' =>  $request->value,
                    'numeroIdentificacion' =>  $request->document,
                    'tipoDocumento' => $request->typeDocument,
                ]
            ]);
            $responseBuysDaviplata = json_decode($response->getBody()->getContents(), true);
            return $help->messagesuccess('', $responseBuysDaviplata, 200);
        } catch (\Exception $ex) {
            return $help->messageerror('Error en la transacción de la compra', $ex->getMessage(), 500);
        }
    }

    //ambiente de pruebas
    public function getOtpDaviplata(Request $request)
    {
        try {
            $help = new Helpers();
            $http = new Client();
            $response = $http->post(env('URL_BASE_DAVIPLATA') . '/otpSec/v1/read', [
                'headers' => [
                    "Accept" => "application/json",
                    "Content-Type" => "application/json",
                    "x-ibm-client-id" => env('CLIENT_ID')
                ],
                'cert' => storage_path('app/certifiedDaviplata/cert_dummy_lab_v2.crt'),
                'ssl_key' => storage_path('app/certifiedDaviplata/cert_dummy_lab_key_v2.pem'),
                'json' => [
                    'typeDocument' =>  $request->typeDocument,
                    'numberDocument' =>  $request->numberDocument,
                    'notificationType' => 'API_DAVIPLATA',
                ]
            ]);
            $responseOtpDaviplata = json_decode($response->getBody()->getContents(), true);
            return $help->messagesuccess('', $responseOtpDaviplata, 200);
        } catch (\Exception $ex) {
            return $help->messageerror('Error al obtener el otp', $ex->getMessage(), 500);
        }
    }

    public function confirmDaviplata(Request $request)
    {
        try {
            $help = new Helpers();
            $http = new Client();
            $id_transaccion = Uuid::uuid1();
            $response = $http->post(env('URL_BASE_DAVIPLATA') . '/daviplata/v1/confirmarCompra', [
                'headers' => [
                    "Accept" => "application/json",
                    "Authorization" => 'Bearer ' . $request->token,
                    "Content-Type" => "application/json",
                    "x-ibm-client-id" => env('CLIENT_ID')
                ],
                'cert' => storage_path('app/certifiedDaviplata/cert_dummy_lab_v2.crt'),
                'ssl_key' => storage_path('app/certifiedDaviplata/cert_dummy_lab_key_v2.pem'),
                'json' => [
                    'otp' =>  $request->otp,
                    'idSessionToken' =>  $request->idSessionToken,
                    'idComercio' => env('IDCOMERCIO'),
                    'idTerminal' => env('IDTERMINAL'),
                    'idTransaccion' =>  rand(1, 999999),
                ]
            ]);
            $responseConfirmDaviplata = json_decode($response->getBody()->getContents(), true);
            $responseConfirmDaviplata['codePay'] = $id_transaccion;
            if (isset($responseConfirmDaviplata['estado'])) {
                if ($responseConfirmDaviplata['estado'] == 'Aprobado') {
                    $pago = new Pago;
                    $pago->parque_id = $request->formPse['parkSelected'];
                    $pago->servicio_id = $request->formPse['serviceParkSelected'];
                    $pago->identificacion = $request->formPse['document'];
                    $pago->tipo_identificacion =  $help->getTypeDocument($request->formPse['documentTypeSelected']);
                    $pago->codigo_pago = $id_transaccion;
                    $pago->id_transaccion_pse = 'DAVIPLATA-' . $responseConfirmDaviplata['numAprobacion'];
                    $pago->tipo_persona = $help->getTypePerson($request->formPse['typePersonSelected']);
                    $pago->razon_social = isset($request->formPse['socialReason']) ? toUpper($request->formPse['socialReason']) : null;
                    $pago->codigo_verificacion = isset($request->formPse['codeVerification']) ? $request->formPse['codeVerification'] : null;
                    $pago->direccion = toUpper($request->formPse['address']);
                    $pago->email = toUpper($request->formPse['email']);
                    $pago->nombre = toUpper($request->formPse['name']);
                    $pago->apellido = toUpper($request->formPse['lastName']);
                    $pago->telefono = $request->formPse['phone'];
                    //data payer
                    $pago->tipo_persona_pagador = $help->getTypePerson($request->formPse['typePersonSelectedPayer']);
                    $pago->razon_social_pagador = isset($request->formPse['socialReasonPayer']) ? toUpper($request->formPse['socialReasonPayer']) : null;
                    $pago->direccion_pagador = toUpper($request->formPse['addressPayer']);
                    $pago->tipo_identificacion_pagador =  $help->getTypeDocument($request->formPse['documentTypeSelectedPayer']);
                    $pago->identificacion_pagador = $request->formPse['documentPayer'];
                    $pago->email_pagador = toUpper($request->formPse['emailPayer']);
                    $pago->nombre_pagador = toUpper($request->formPse['namePayer']);
                    $pago->apellido_pagador = toUpper($request->formPse['lastNamePayer']);
                    $pago->telefono_pagador = $request->formPse['phonePayer'];
                    $pago->codigo_banco_seleccionado = '1551';

                    $pago->estado_id = 2;
                    $pago->estado_banco = 'OK';
                    $pago->concepto = toUpper($request->formPse['concept']);
                    $pago->moneda = 'COP';
                    $pago->total = $request->formPse['totalPay'];
                    $pago->iva = 0;
                    $pago->permiso = $request->formPse['permitNumber'];
                    $pago->tipo_permiso = $request->formPse['permitTypeSelected'];
                    $pago->id_reserva = $request->formPse['reservationId'];
                    $pago->fecha_pago = null;
                    $pago->user_id_pse = 'DAVIPLATA' . $request->formPse['document'];
                    $pago->medio_id = 6;
                    $pago->save();
                    return $help->messagesuccess('', $responseConfirmDaviplata, 200);
                }else{
                    return $help->messageerror('Error en la confirmacion de la compra', 'Intenta nuevamente', 200);
                }
            } else {
                return $help->messageerror('Error en la confirmacion de la compra', $responseConfirmDaviplata, 200);
            }
        } catch (\Exception $ex) {
            return $help->messageerror('Error en la confirmacion de la compra', $ex->getMessage(), 500);
        }
    }
}
