<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContributionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contractor_contributions', function (Blueprint $table) {
            $table->id();
            $table->integer('report_id');
            $table->string('payroll_number', 50);
            $table->string('payment_date', 50);
            $table->string('eps_name', 50);
            $table->integer('eps_value');
            $table->string('afp_name', 50);
            $table->integer('afp_value');
            $table->string('arl_name', 50);
            $table->integer('arl_value');
            $table->integer('total');
            $table->tinyInteger('is_new_contractor')->default(0);
            $table->text('support_file')->charset('utf8mb4');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contractor_contributions');
    }
}
