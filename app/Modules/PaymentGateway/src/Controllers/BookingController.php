<?php

namespace App\Modules\PaymentGateway\src\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\PaymentGateway\src\Exports\BookingsExport;
use App\Modules\PaymentGateway\src\Models\BookingView;
use App\Modules\PaymentGateway\src\Models\Status;
use App\Modules\PaymentGateway\src\Request\BookingSearchRequest;
use App\Modules\PaymentGateway\src\Resources\BookingResource;
use Maatwebsite\Excel\Facades\Excel;
use function Clue\StreamFilter\fun;

class BookingController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function counter(): \Illuminate\Http\JsonResponse
    {
        return $this->success_message([
            'total_approved' => BookingView::BookingsApproved()->count(),
            'total_failed' => BookingView::BookingsFailed()->count(),
            'total' => BookingView::Bookings()->count(),
        ]);
    }

    public function search(BookingSearchRequest $request): \Illuminate\Http\JsonResponse
    {
        $data = collect($request);
        $bookings = BookingView::query()
            ->whereIn('status_id', [1,2,3,4,5])
            ->when(!is_null($data->get('status')), function ($q) {
                return $q->where('status_id', '=', Status::APPROVED);
            })
            ->when(!is_null($data->get('park_name')), function ($q) use ($data) {
                $name = toUpper($data->get('park_name'));
                return $q->where('park', 'like', "%$name%");
            })
            ->when(!is_null($data->get('start_date')) && !is_null($data->get('final_date')), function ($q) use ($data) {
                return $q->whereBetween('created_at', [$data->get('start_date'), $data->get('final_date')]);
            })
            ->when(!is_null($data->get('start_date')), function ($q) use ($data) {
                return $q->whereDate('created_at', $data->get('start_date'));
            })
            ->orderBy('created_at', 'desc')
            ->get();

        return $this->success_response(BookingResource::collection($bookings));
    }

    public function index(): \Illuminate\Http\JsonResponse
    {
        $bookings = BookingView::query()
            ->whereIn('status_id', [Status::APPROVED,Status::FAILED])
            ->orderBy('created_at', 'desc')
            ->get();

        return $this->success_response(BookingResource::collection($bookings));
    }

    public function export($status, $park, $start_date, $final_date)
    {

        $file_name = "reporte-reservas-" . now()->format('Y-m-d') . ".xlsx";
        $bookings = [];
        BookingView::query()
            ->whereIn('status_id', [1,2,3,4,5])
            ->when($status != 'null', function ($q) {
                return $q->where('status_id', '=', 2);
            })
            ->when($park != 'null', function ($q) use ($park) {
                $name = toUpper($park);
                return $q->where('park', 'like', "%$name%");
            })
            ->when($start_date != 'null' && $final_date != 'null', function ($q) use ($start_date, $final_date) {
                return $q->whereBetween('created_at', [$start_date, $final_date]);
            })
            ->when($start_date != 'null', function ($q) use ($start_date) {
                return $q->whereDate('created_at', $start_date);
            })
            ->orderBy('created_at', 'desc')
            ->chunk(200, function ($list) use (&$bookings) {
                foreach ($list as $booking) {
                    $bookings[] = $booking;
                }
            });

        return Excel::download(new BookingsExport(collect($bookings)), $file_name);
    }

}
