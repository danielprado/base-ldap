<?php


namespace App\Modules\Contractors\src\Jobs;


use App\Modules\Contractors\src\Mail\ContractorLegalMail;
use App\Modules\Contractors\src\Mail\ContractorMail;
use App\Modules\Contractors\src\Mail\ContractorSendArlMail;
use App\Modules\Contractors\src\Mail\ContractorUpdateMail;
use App\Modules\Contractors\src\Models\Contract;
use App\Modules\Contractors\src\Models\Contractor;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use SendGrid\Mail\Mail;

class SendArlContractor implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Contractor
     */
    private $user;

    /**
     * @var Contract
     */
    private $contract;

    /**
     * Create a new job instance.
     *
     * @param Contractor $user
     * @param Contract $contract
     */
    public function __construct(Contractor $user, Contract $contract)
    {
        $this->user = $user;
        $this->contract = $contract;
    }

    /**
     * Execute the job.
     *
     * @param Mail $mailer
     * @return void
     */
    public function handle(Mail $mailer)
    {
        $email = config('app.env') == 'local'
            ? env('SAMPLE_EMAIL', 'daniel.prado@idrd.gov.co')
            : isset($this->user->email) ? $this->user->email : null;

        $file = $this->contract->files()->where('file_type_id', 1)->latest()->first();
        $file_name = isset($file->name) ? $file->name : null;
        $path = storage_path("app/arl/{$file_name}");
        $file_encoded = base64_encode(file_get_contents($path));
        $attachment = ['file' => $file_name];

        if ( isset($email)  && filter_var( $email, FILTER_VALIDATE_EMAIL) ) {
            $template = new ContractorSendArlMail($this->user, $this->contract);
            $mailer->setFrom(
                env('MAIL_CONTRACTS_FROM', 'portal.contratista@idrd.gov.co'),
                env('MAIL_CONTRACTS_FROM_NAME', 'Portal Contratista')
            );
            $mailer->setSubject("Certificado ARL {$this->user->getFullNameAttribute()} / {$this->contract->contract_type->name}");
            $mailer->addTo($email);
            $render = $template->render();
            $mailer->addContent("text/html", $render);
            $mailer->addAttachment(
                $file_encoded,
                "application/pdf",
                $file_name,
                "attachment"
            );
            $sendgrid = new \SendGrid(config('mail.sendgrid.api'));
            try {
                $response = $sendgrid->send($mailer);
                sendgird_logger(
                    $email,
                    "Certificado ARL {$this->user->getFullNameAttribute()} / {$this->contract->contract_type->name}",
                    $render,
                    $attachment,
                    $response->statusCode(),
                    $response->body()
                );
            } catch (\Exception $e) {
                sendgird_logger(
                    $email,
                    "[EXCEPTION] Certificado ARL {$this->user->getFullNameAttribute()} / {$this->contract->contract_type->name}",
                    $render,
                    $attachment,
                    $e->getCode(),
                    $e->getMessage()
                );
            }
        }
    }
}
