<?php

namespace App\Modules\SportsPerformance\src\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Security\Department;
use App\Models\Security\Locality;
use App\Modules\SportsPerformance\src\Models\Person;
use App\Modules\SportsPerformance\src\Models\Sport;
use App\Modules\SportsPerformance\src\Models\City;
use App\Modules\SportsPerformance\src\Models\AthleteClassification;

class Athlete extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_sim_rendimiento_deportivo';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'deportista';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Persona_Id',
        'Id_Deporte',
        'Clasificacion_Deportista_Id',
        'Localidad_Id_Localiza',
        'Fijo_Localiza',
        'Celular_Localiza',
        'Direccion_Localiza',
        'Barrio_Localiza',
        'Id_Ciudad_Nacimiento',
        'Departamento_Id_Nac'
    ];

    
    public function birth_city()
    {
        return $this->hasOne(City::class, 'Id', 'Id_Ciudad_Nacimiento');
    }

    public function birth_department()
    {
        return $this->hasOne(Department::class, 'Id_Departamento', 'Departamento_Id_Nac');
    }
    
    public function locality()
    {
        return $this->hasOne(Locality::class, 'Id_Localidad', 'Localidad_Id_Localiza');
    }
    
    public function classification()
    {
        return $this->hasOne(AthleteClassification::class, 'Id', 'Clasificacion_Deportista_Id');
    }
    
    public function sport()
    {
        return $this->hasOne(Sport::class, 'PK_I_ID_DEPORTE', 'Id_Deporte');
    }
}
