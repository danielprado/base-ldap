<?php


namespace App\Modules\Payroll\src\Resources;


use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class ConvertSevenResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'codigo'      =>  isset($this->top_codi) ? $this->top_codi : null,
            'identificacion'      =>  isset($this->ter_codi) ? $this->ter_codi : null,
            'anio'      =>  isset($this->mpr_anop) ? $this->mpr_anop : null,
            'mes'  =>  isset($this->mpr_mesp) ? $this->mpr_mesp : null,
            'dia'  =>  isset($this->mpr_diap) ? $this->mpr_diap : null,
            'rubro'   =>  isset($this->rub_codi) ? $this->rub_codi : null,
            'centro_costo' =>  isset($this->arb_ccec) ? $this->arb_ccec : null,
            'proyecto' =>  isset($this->arb_cpro) ? $this->arb_cpro : null,
            'area'  =>  isset($this->arb_care) ? $this->arb_care : null,
            'referencia' =>  isset($this->dmp_ref1) ? $this->dmp_ref1 : null,
            'errepe' =>  isset($this->mpr_nume) ? $this->mpr_nume : null,
        ];
    }
}
