<?php

namespace App\Modules\Certifications\src\Exports;

use App\Models\Security\DocumentType;
use App\Models\Security\User;
use App\Modules\Certifications\src\Models\CertificationContract;
use App\Modules\Certifications\src\Models\CertificationContractType;
use App\Traits\AppendHeaderToExcel;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Builder;
use Imtigger\LaravelJobStatus\JobStatus;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ContractExport implements FromQuery, WithTitle, WithEvents, WithHeadings, WithMapping, WithColumnFormatting, ShouldQueue, ShouldAutoSize
{
    use Exportable, AppendHeaderToExcel;

    /**
     * @var array
     */
    private $request;

    /**
     * @var int
     */
    private $rowNumb = 2;

    /**
     * @var User
     */
    private $user;

    public function __construct(array $request, $job, $user)
    {
        $this->user = $user;
        $this->request = $request;
        update_status_job($job, JobStatus::STATUS_EXECUTING, 'excel-certifications-module');
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return CertificationContract::query()
            ->with(['contractTypes', 'document_type', 'document_type_representative'])
            ->whereKey($this->request['contracts'])
            ->orderBy('Fecha_Firma', 'desc');
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $sheet) {
                $this->setHeader($sheet->sheet, 'CONTRATOS - CERTIFICACION DE CONTRATOS', 'A1:W1', 'W');
            }
        ];
    }

    public function headings(): array
    {
        return [
            // Contractor
            'DOCUMENTO',
            'TIPO DE DOCUMENTO',
            'DÍGITO DE VERIFICACIÓN',
            'NOMBRE',
            // Legal Representative
            'TIPO DOCUMENTO REPRESENTANTE LEGAL',
            'DOCUMENTO REPRESENTANTE LEGAL',
            'NOMBRE REPRESENTANTE LEGAL',
            // Contract
            'ID CONTRATO',
            'ESTADO DEL CONTRATO',
            'TIPO DE CONTRATO',
            'NÚMERO CONTRATO',
            'AÑO CONTRATO',
            'FECHA DE FIRMA',
            'FECHA DE INICIO',
            'FECHA DE FINALZACIÓN',
            'FECHA DE FINALIZACIÓN ANTICIPADA',
            'DURACIÓN EN MESES',
            'DURACIÓN EN DÍAS',
            'OTRA DURACIÓN',
            'VALOR',
            'VALOR MENSUAL',
            'FECHA DE CREACIÓN',
            'FECHA DE MODIFICACIÓN',
        ];
    }

    public function title(): string
    {
        return 'CONTRATOS';
    }

    public function columnFormats(): array
    {
        return [
            'M' => NumberFormat::FORMAT_DATE_YYYYMMDD2.' h'.NumberFormat::FORMAT_DATE_TIME4,
            'N' => NumberFormat::FORMAT_DATE_YYYYMMDD2.' h'.NumberFormat::FORMAT_DATE_TIME4,
            'O' => NumberFormat::FORMAT_DATE_YYYYMMDD2.' h'.NumberFormat::FORMAT_DATE_TIME4,
            'P' => NumberFormat::FORMAT_DATE_YYYYMMDD2.' h'.NumberFormat::FORMAT_DATE_TIME4,
            'V' => NumberFormat::FORMAT_DATE_YYYYMMDD2.' h'.NumberFormat::FORMAT_DATE_TIME4,
            'W' => NumberFormat::FORMAT_DATE_YYYYMMDD2.' h'.NumberFormat::FORMAT_DATE_TIME4,
        ];
    }

    public function map($row): array
    {
        return [
            'document'          => $row['Cedula'] ?? null,
            'document_type'     => $this->getDocumentType($row['Tipo_Documento']),
            'dv'                => $row['Dv'] ?? null,
            'name'              => toUpper($row['Nombre_Contratista']) ?? null,
            //
            'document_type_representative'  => $this->getDocumentType($row['Tipo_Documento_Representante']) ?? null,
            'document_representative'       => $row['Cedula_Representante'] ?? null,
            'name_representative'           => $row['Nombre_Representante'] ?? null,
            //
            'id'                => $row['Id'] ?? null,
            'state'             => isset($row['Fecha_Fin']) ? $this->status($row['Fecha_Fin']) : null,
            'contract_type'     => $this->getContractType($row['Tipo_Contrato_Id']) ?? null,
            'contract_number'   => $row['Numero_Contrato'] ?? null,
            'contract_year'     => $row['year'] ?? null,//$this->getContractYear($row['year'], $row['Fecha_Firma']) ?? null,
            'signature_date'    => $row['Fecha_Firma'] ?? null,
            'start_date'        => $row['Fecha_Inicio'] ?? null,
            'final_date'        => $row['Fecha_Fin'] ?? null,
            'anticipated_date'  => $row['Fecha_Terminacion_Anticipada'] ?? null,
            'duration_months'   => $row['Meses_Duracion'] ?? null,
            'duration_days'     => $row['Dias_Duracion'] ?? null,
            'duration_other'    => $row['Otra_Duracion'] ?? null,
            'initial_value'     => $row['Valor_Inicial'] ?? null,
            'monthly_value'     => $row['Valor_Mensual'] ?? null,
            'created_at'        => isset($row['created_at']) ? date_time_to_excel(Carbon::parse($row['created_at'])) : null,
            'updated_at'        => isset($row['updated_at']) ? date_time_to_excel(Carbon::parse($row['updated_at'])) : null,
        ];
    }

    public function status($final_date): string
    {
        if ($final_date >= now()) {
            return 'ACTIVO';
        }
        return 'FINALIZADO';
    }

    public function getDocumentType($id)
    {
        $document_type_id = isset($id) ? (int) $id : null;
        if ($document_type_id) {
            $document_type = DocumentType::query()->where('Id_TipoDocumento', $document_type_id)->pluck('Descripcion_TipoDocumento');
            return $document_type[0];
        }
        return  $document_type_id;
    }

    public function getContractType($id)
    {
        $contract_type_id = isset($id) ? (int) $id : null;
        if ($contract_type_id) {
            $contract_type = CertificationContractType::query()->where('Id', $contract_type_id)->pluck('Nombre_Tipo_Contrato');
            return $contract_type[0];
        }
        return  $contract_type_id;
    }

    public function getContractYear($year, $sign): string
    {
        if (!$year) {
            return Carbon::parse($sign)->format('Y');
        }

        return $year;
    }
}
