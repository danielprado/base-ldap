<?php

namespace App\Modules\Contractors\src\Models;

use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class ContractFileCountView extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable;

    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'mysql_contractors';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'contracts_files_counts';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'contract_id',
        'document',
        'contract',
        'contract_type_id',
        'final_date',
        'name',
        'arl_files_count',
        'other_files_count',
        'created_at',
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [ 'final_date', 'created_at' ];

    /*
     * ---------------------------------------------------------
     * Query Scopes
     * ---------------------------------------------------------
    */

    public function scopeWithoutArl($query)
    {
        return $query->where("arl_files_count", 0);
    }

    public function scopeWithoutSecop($query)
    {
        return $query->where("other_files_count", 0);
    }

    public function scopeWithArl($query)
    {
        return $query->where("arl_files_count", ">", 0);
    }

    public function scopeWithSecop($query)
    {
        return $query->where("other_files_count", ">", 0);
    }
}
