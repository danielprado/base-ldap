<?php


namespace App\Modules\Contractors\src\Resources;


use App\Models\Security\Subdirectorate;
use App\Models\Security\User;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;


class PayrollResourceSeven extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $subdirectorate_id = isset($this->subdirectionId) ? (int) $this->subdirectionId : null;
        $subdirector_id = isset($this->subdirectorId) ? (int) $this->subdirectorId : null;
        $subdirectorate = Subdirectorate::query()->find($subdirectorate_id);
        $subdirector = User::query()->find($subdirector_id);
        return [
            'id'                =>  isset($this->id) ? (int) $this->id : null,
            'status'            =>  $this->getStatus() ?? null,
            'year'              =>  isset($this->year) ? (int) $this->year :  null,
            'month'             =>  isset($this->month) ? (int) $this->month :  null,
            'period'            =>  $this->getPeriod() ?? null,
            'subdirectorate'    =>  $subdirectorate ?? null,
            'subdirector'       =>  $subdirector ?? null,
            'plans'             =>  $this->paymentList,
            'created_at'        =>  isset($this->createdAt) ? Carbon::parse($this->createdAt)->format('Yd/m/y H:i') : null,
            'updated_at'        =>  isset($this->updatedAt) ? Carbon::parse($this->updatedAt)->format('Yd/m/y H:i') : null,
            'deleted_at'        =>  isset($this->deletedAt) ? Carbon::parse($this->deletedAt)->format('Yd/m/y H:i') : null,
        ];
    }

    public function getStatus(): string
    {
        switch ($this->status) {
            case 'BINNACLE':
                return 'CON BITÁCORA';
            case 'INVOICING':
                return 'CAUSADA';
            case 'PAYMENT':
                return 'PAGADA';
            default:
                return 'PENDIENTE';
        }
    }

    public function getPeriod(): string
    {
        $date = Carbon::create(null, $this->month);
        Carbon::setLocale('es');
        return toUpper($date->isoFormat('MMMM')) . '-' . $this->year;
    }
}
