<?php


namespace App\Modules\CitizenPortal\src\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\CitizenPortal\src\Constants\Roles;
use App\Modules\CitizenPortal\src\Models\Activity;
use App\Modules\CitizenPortal\src\Models\CitizenSchedule;
use App\Modules\CitizenPortal\src\Models\Modality;
use App\Modules\CitizenPortal\src\Models\ParticipantRole;
use App\Modules\CitizenPortal\src\Models\Schedule;
use App\Modules\CitizenPortal\src\Request\ActivityRequest;
use App\Modules\CitizenPortal\src\Request\ModalityRequest;
use App\Modules\CitizenPortal\src\Request\ParticipantRoleRequest;
use App\Modules\CitizenPortal\src\Resources\ActivityResource;
use App\Modules\CitizenPortal\src\Resources\ModalityResource;
use App\Modules\CitizenPortal\src\Resources\ParticipantRoleResource;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ParticipantRoleController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        $query = $this->setQuery(ParticipantRole::query(), (new ParticipantRole)->getSortableColumn($this->column))
            ->when(isset($this->query), function ($query) {
                return $query->where('name', 'like', "%$this->query%");
            })
            ->orderBy((new ParticipantRole)->getSortableColumn($this->column), $this->order);
        return $this->success_response(
            ParticipantRoleResource::collection(
                (int) $this->per_page > 0
                    ? $query->paginate( $this->per_page )
                    : $query->get()
            ),
            Response::HTTP_OK,
            [
                'headers'   => ParticipantRoleResource::headers()
            ]
        );
    }

    /**
     * @param ParticipantRoleRequest $request
     * @return JsonResponse
     */
    public function store(ParticipantRoleRequest $request)
    {
        $model = new ParticipantRole();
        $model->fill($request->validated());
        $model->save();
        return $this->success_message(
            __('validation.handler.success'),
            Response::HTTP_CREATED
        );
    }

    /**
     * @param ParticipantRoleRequest $request
     * @param ParticipantRole $role
     * @return JsonResponse
     */
    public function update(ParticipantRoleRequest $request, ParticipantRole $role)
    {
        $role->fill($request->validated());
        $role->save();
        return $this->success_message(
            __('validation.handler.updated')
        );
    }

    /**
     * @param ParticipantRole $role
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(ParticipantRole $role)
    {
        $role->delete();
        return $this->success_message(
            __('validation.handler.deleted'),
            Response::HTTP_OK,
            Response::HTTP_NO_CONTENT
        );
    }
}
