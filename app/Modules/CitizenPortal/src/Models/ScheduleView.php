<?php

namespace App\Modules\CitizenPortal\src\Models;

use App\Modules\Parks\src\Models\Park;
use App\Traits\PaginationWithHavings;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Database\Eloquent\SoftDeletes;

class ScheduleView extends Model
{
    use SoftDeletes, PaginationWithHavings;

    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = "mysql_citizen_portal";

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = "schedule_view";

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = "id";

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'program_id',
        'program_name',
        'activity_id',
        'activity_name',
        'stage_id',
        'stage_name',
        'park_id',
        'weekday_id',
        'weekday_name',
        'daily_id',
        'daily_name',
        'min_age',
        'max_age',
        'quota',
        'taken',
        'is_paid',
        'rate_id',
        'rate_name',
        'rate_service_id',
        'rate_value',
        'is_initiate',
        'start_date',
        'final_date',
        'is_activated',
        'disability',
        'project_id',
        'activity_type_id',
        'set_population_params',
        'sex_id',
        'orientation_id',
        'gender_id',
        'ethnic_group_id',
        'population_group_id',
        'disability_id',
        'is_teams',
        'men_participants_num',
        'women_participants_num',
        'staff_participants_num',
        'activity_type_name',
        'project_name'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'start_date',
        'final_date',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'program_id'    => 'int',
        'activity_id'   => 'int',
        'stage_id'      => 'int',
        'park_id'       => 'int',
        'weekday_id'    => 'int',
        'daily_id'      => 'int',
        'min_age'       => 'int',
        'max_age'       => 'int',
        'quota'         => 'int',
        'rate_id'       => 'int',
        'rate_value'    => 'int',
        'rate_service_id' => 'int',
        'is_paid'       => 'boolean',
        'is_initiate'   => 'boolean',
        'is_activated'  => 'boolean',
        'disability'  => 'boolean',
        'project_id' => 'int',
        'activity_type_id' => 'int',
        'set_population_params' => 'boolean',
        'sex_id' => 'string',
        'orientation_id' => 'string',
        'gender_id' => 'string',
        'ethnic_group_id' => 'string',
        'population_group_id' => 'string',
        'disability_id' => 'string',
        'is_teams' => 'boolean',
        'men_participants_num' => 'int',
        'women_participants_num' => 'int',
        'staff_participants_num' => 'int',
        'activity_type_name' => 'string',
        'project_name' => 'string'
    ];

    /**
     * The accessors to append to the model's array form.
     *
     * @var array
     */
    protected $appends = [
        'name',
        'park_code',
        'park_name',
        'park_address',
    ];

    /*
     * ---------------------------------------------------------
     * Query Scopes
     * ---------------------------------------------------------
     */

    /**
     * @param $query
     * @param $value
     * @return mixed
     */
    public function scopeSearch($query, $value)
    {
        return $query->where(function ($query) use ($value) {
            $parks = Park::query()->where('Id_IDRD', 'like', "%$value%")
                ->orWhere('Nombre', 'like', "%$value%")
                ->orWhere('Direccion', 'like', "%$value%")
                ->get(["Id"])->pluck("Id")->toArray();
            return $query
                ->where('program_name', 'like', "%$value%")
                ->orWhere('activity_name', 'like', "%$value%")
                ->orWhere('stage_name', 'like', "%$value%")
                ->orWhereIn("park_id", $parks)
                ->orWhere('weekday_name', 'like', "%$value%")
                ->orWhere('daily_name', 'like', "%$value%")
                ->orWhere('min_age', 'like', "%$value%")
                ->orWhere('max_age', 'like', "%$value%")
                ->orWhere('quota', 'like', "%$value%");
        });
    }

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator
     * ---------------------------------------------------------
     */

    /**
     * @return string
     */
    public function getNameAttribute()
    {
        return "{$this->weekday_name} {$this->daily_name} - ({$this->min_age}-{$this->max_age}) AÑOS";
    }

    /**
     * @return mixed|string|null
     */
    public function getParkNameAttribute()
    {
        return isset($this->park->Nombre) ? toUpper( $this->park->Nombre ) : null;
    }

    /**
     * @return mixed|string|null
     */
    public function getParkCodeAttribute()
    {
        return isset($this->park->Id_IDRD) ? toUpper( $this->park->Id_IDRD ) : null;
    }

    /**
     * @return mixed|string|null
     */
    public function getParkAddressAttribute()
    {
        return isset($this->park->Direccion) ? toUpper( $this->park->Direccion ) : null;
    }

    /**
     * @param $column
     * @return string
     */
    public function getSortableColumn($column)
    {
        switch ($column) {
            case 'created_at':
            case 'updated_at':
            case 'deleted_at':
                return $column;
            default:
                return in_array($column, $this->fillable)
                    ? $column
                    : $this->primaryKey;
        }
    }

    /*
     * ---------------------------------------------------------
     * Eloquent Relationship
     * ---------------------------------------------------------
     */

    /**
     * @return HasMany
     */
    public function users_schedules()
    {
        return $this->hasMany(CitizenSchedule::class, 'schedule_id', 'id');
    }

    /**
     * @return HasMany
     */
    public function teams_schedules()
    {
        return $this->hasMany(TeamSchedule::class, 'schedule_id', 'id');
    }

    /**
     * @return mixed
     */
    public function schedule()
    {
        return $this->belongsTo(Schedule::class, 'id', 'id');
    }

    /**
     * @return mixed
     */
    // public function park()
    // {
    //     return $this->belongsTo(Park::class, 'park_id', 'Id');
    // }
}
