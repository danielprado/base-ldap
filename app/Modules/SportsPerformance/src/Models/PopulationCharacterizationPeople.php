<?php

namespace App\Modules\SportsPerformance\src\Models;

use Illuminate\Database\Eloquent\Model;
use App\Modules\SportsPerformance\src\Models\Person;
use App\Modules\SportsPerformance\src\Models\GenderIdentity;
use App\Modules\SportsPerformance\src\Models\Sex;
use App\Modules\SportsPerformance\src\Models\SexualOrientation;

class PopulationCharacterizationPeople extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_sim_rendimiento_deportivo';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'caracterizacion_poblacional_personas';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['persona_id' , 'sexo_id' , 'orien_sexual_id' , 'identidad_genero_id' , 'grupo_vital_id' , 'sector_social_id' , 'grupo_etnico_id'];

    public function sexual_orientation()
    {       
        return $this->belongsTo(SexualOrientation::class, 'orien_sexual_id', 'id');
    }

    public function gender_identity()
    {       
        return $this->belongsTo(GenderIdentity::class, 'identidad_genero_id', 'id');
    }

    public function sex()
    {       
        return $this->belongsTo(Sex::class, 'sexo_id', 'id');
    }

}
