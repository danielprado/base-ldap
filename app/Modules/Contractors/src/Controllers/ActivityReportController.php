<?php

namespace App\Modules\Contractors\src\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Contractors\src\Constants\GlobalQuery;
use App\Modules\Contractors\src\Helpers\compliance\emails\Emails;
use App\Modules\Contractors\src\Jobs\ActivityReportCorrectedReminder;
use App\Modules\Contractors\src\Jobs\ActivityReportRejected;
use App\Modules\Contractors\src\Models\Contract;
use App\Modules\Contractors\src\Models\ActivityReport;
use App\Modules\Contractors\src\Models\ActivityReportDate;
use App\Modules\Contractors\src\Models\Compliance;
use App\Modules\Contractors\src\Models\Contractor;
use App\Modules\Contractors\src\Models\ActivityReportEvidence;
use App\Modules\Contractors\src\Models\DocumentInit;
use App\Models\Security\User;
use App\Modules\Contractors\src\Request\ActivityReportRequest;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Throwable;
use App\Modules\Contractors\src\Resources\ActivityReportResource;
use App\Modules\Contractors\src\Resources\ActivityReportDateResource;
use App\Modules\Contractors\src\Resources\ActivityReportResourceSupervisor;
use App\Helpers\FPDF;
use App\Modules\Contractors\src\Constants\Roles;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Log;
use NumberFormatter;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Http\Resources\Json\JsonResource;
use Dompdf\Dompdf;
use Dompdf\Options;
use App\Modules\Contractors\src\Models\PlanPayment;
use GuzzleHttp\Client;

class ActivityReportController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $contractor = Contractor::where('document', $request->get('document'))->first();
        $activityReports = ActivityReport::where('contractor_id', $contractor->id)->latest()->paginate($this->per_page);
        return $this->success_response(
            ActivityReportResource::collection($activityReports),
            Response::HTTP_OK,
            [
                'headers'   => ActivityReportResource::headers()
            ]
        );
    }

    public function show(ActivityReport $report)
    {
        return $this->success_response(
            new ActivityReportResource($report->load(['evidences', 'contributions'])),
            Response::HTTP_OK
        );
    }

    public function getLatest(Request $request, $contract_id)
    {
        $report = ActivityReport::where('contract_id', $contract_id)->first();
        if (isset($report)) {
            return $this->success_response(
                new ActivityReportResource($report->load('evidences')),
                Response::HTTP_OK
            );
        } else {
            return $this->error_response(
                __('validation.handler.resource_not_found'),
                Response::HTTP_NOT_FOUND
            );
        }
    }

    public function store(ActivityReportRequest $request)
    {
        try {
            $report_date = $request->get('report_date');
            $contract_id = $request->get('contract_id');
            $existingReport = ActivityReport::where('report_date', $report_date)
                ->where('contract_id', $contract_id)
                ->first();
            if ($existingReport) {
                abort(Response::HTTP_CONFLICT, 'Ya existe un informe de actividades para el periodo seleccionado.');
            }
            if (isset($request->activity_period)) {
                $request->merge(['activity_period' => implode(',', $request->activity_period)]);
            }

            DB::connection('mysql_contractors')->beginTransaction();
            $latest_report = ActivityReport::with('evidences')->where('contract_id', $contract_id)->latest()->first();
            $form = ActivityReport::with('contractor')->create($request->all());

            if ($latest_report) {
                foreach ($latest_report->evidences as $evidence) {
                    $newEvidence = $evidence->replicate();
                    $newEvidence->activity_report_id = $form->id;
                    $newEvidence->file_path = '';
                    $newEvidence->evidence_text = '';
                    $newEvidence->evidence_link = '';
                    $newEvidence->save();
                }
            }
            DB::connection('mysql_contractors')->commit();

            return $this->success_message(
                __('validation.handler.success'),
                Response::HTTP_CREATED,
                Response::HTTP_CREATED,
                ['id' => $form->id]
            );
        } catch (Throwable $e) {
            DB::connection('mysql_contractors')->rollBack();
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }

    public function update(ActivityReportRequest $request, ActivityReport $report)
    {
        try {
            $id = $request->get('id');
            $report_date = $request->get('report_date');
            $contract_id = $request->get('contract_id');

            if (isset($request->activity_period)) {
                $request->merge(['activity_period' => implode(',', $request->activity_period)]);
            }
            DB::connection('mysql_contractors')->beginTransaction();
            $report->update($request->all());
            $report = $report->load('contractor');

            $user = User::where('document', $report->contractor->document)->first();
            $signature = $user->signature;
            if ($request->has('contractor_signature')) {
                if ($signature && Storage::disk('user')->exists($signature)) {
                    Storage::disk('user')->delete($signature);
                }
                $file = $request->file('contractor_signature')[0];
                $ext = $file->getClientOriginalExtension();
                $now = now()->format('YmdHis');
                $signature = "SIGNATURE_{$user->document}_{$now}.$ext";
                $file->storeAs('user', $signature, ['disk' => 'local']);
                $user->update(['signature' => $signature]);
            }

            $attachments_path = $report->attachments_path;
            if ($request->has('attachments')) {
                $original_path = $report->contract_number . '/' . $report->getOriginal('attachments_path');
                if ($report->getOriginal('attachments_path') && Storage::disk('activity-reports')->exists($original_path)) {
                    Storage::disk('activity-reports')->delete($original_path);
                }
                $file = $request->file('attachments')[0];
                $ext = $file->getClientOriginalExtension();
                $now = now()->format('YmdHis');
                $attachments_path = "FINAL_ATTACHMENT_{$report->contract_number}_{$now}.$ext";
                $file->storeAs('activity-reports', $report->contract_number . '/' . $attachments_path, ['disk' => 'local']);
                $report->update(['attachments_path' => $attachments_path]);
            }
            DB::connection('mysql_contractors')->commit();

            return $this->success_message(
                __('validation.handler.success'),
                Response::HTTP_OK,
                Response::HTTP_OK,
                [
                    'id' => $report->id,
                    'attachments_path' => $attachments_path,
                    'contractor_signature_path' => $signature
                ]
            );
        } catch (\Exception $e) {
            DB::connection('mysql_contractors')->rollBack();
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }

    public function sendToApprove(ActivityReport $report)
    {
        try {
            DB::connection('mysql_contractors')->beginTransaction();
            if ($report->state == ActivityReport::STATUS_REJECTED) {
                $this->dispatch(new ActivityReportCorrectedReminder($report));
            }
            $report->update([
                                'state' => ActivityReport::STATUS_PENDING,
                                'reviewed' => 0,
                                'name_reviewer' => '',
                                'reviewed_technical' => 0,
                                'name_reviewer_technical' => ''
                            ]);
            list($year, $month) = explode("-", $report->report_date);
            $compliance = Compliance::where('contract_id', $report->contract_id)->where('period_year', intval($year))->where('period_month', intval($month))->first();
            $redirect_to_compliance = true;
            if (isset($compliance)) {
                $redirect_to_compliance = $compliance->isRejected();
            }
            DB::connection('mysql_contractors')->commit();
            return $this->success_message(
                __('validation.handler.success'),
                Response::HTTP_OK,
                Response::HTTP_OK,
                [
                    'id' => $report->id,
                    'state' => $report->state,
                    'redirect_to_compliance' => $redirect_to_compliance
                ]
            );
        } catch (\Exception $e) {
            DB::connection('mysql_contractors')->rollBack();
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }

    public function getReportByPeriodSupervisor(Request $request)
    {
        try {
            $reportActivity = ActivityReport::with(['evidences', 'contract', 'contract.contract_type', 'contractor'])
                ->whereIn('state', [ActivityReport::STATUS_PENDING, ActivityReport::STATUS_REJECTED])
                ->where('report_date', $request->query('period'))
                ->get();
            return $this->success_response(
                ActivityReportResourceSupervisor::collection($reportActivity)
            );
        } catch (\Throwable $exception) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $exception->getMessage()
            );
        }
    }


    public function getByReportDate(Request $request, $contract_id, $report_date)
    {
        try {
            $report = ActivityReport::where('contract_id', $contract_id)->where('report_date', $report_date)->first();
            return $this->success_response(
                new ActivityReportResource($report),
                Response::HTTP_OK
            );
        } catch (\Throwable $exception) {
            return $this->error_response(
                __('validation.handler.resource_not_found'),
                Response::HTTP_NOT_FOUND
            );
        }
    }

    public function approvedReportActivity(Request $request)
    {
        try {
            //$help = new Emails();
            $user = Auth::guard('api')->user();
            $report = ActivityReport::with(['contractor'])->find($request->id);
            if (!$report) {
                return $this->error_response(
                    __('validation.handler.unexpected_failure'),
                    Response::HTTP_UNPROCESSABLE_ENTITY,
                    'Informe de actividad no encontrado'
                );
            }
            $report->update([
                'state' => ActivityReport::STATUS_APPROVED, 'observations' => '',
                'supervisor_id' => $user->id,
            ]);
            return $this->success_message(
                __('validation.handler.success'),
                Response::HTTP_OK,
                Response::HTTP_OK,
                'Reporte de actividades aprobado'
            );
        } catch (\Throwable $th) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $th->getMessage()
            );
        }
    }

    public function reviewedReportActivity(Request $request)
    {
        try {
            $user = Auth::guard('api')->user();
            $report = ActivityReport::find($request->id);
            if (!isset($report)) {
                return $this->error_response(
                    __('validation.handler.unexpected_failure'),
                    Response::HTTP_UNPROCESSABLE_ENTITY,
                    'Informe de actividad no encontrado'
                );
            }

            $reviewed_plans = isset($report->reviewed_plans) ? json_decode($report->reviewed_plans, true) : [];
            $contract = Contract::with('document_init')->find($request->get('contract_id'));
            if (isset($contract) && $contract->dependency_id === Contract::CYCLE_PATH_GUARDIAN_DEP){
                list($year, $month) = explode('-', $report->report_date);
                $plan = PlanPayment::where('contract_id', $contract->id)->where('year', (int)$year)->where('month', (int)$month)->first();
                if (isset($plan)){
                    $http = new Client([
                        'base_uri' => env('URL_PLAN_PAYMENT'),
                        'headers' => [
                            'Accept' => 'application/json',
                            'Content-Type' => 'application/json',
                        ],
                        'verify' => false,
                    ]);
                    $total_payment = (int)($contract->document_init->value_month * $request->sessions);
                    $response = $http->put(env('URL_PLAN_PAYMENT') . "/payment-plans/pre-approve/" . $plan->id, [
                        'json' => [
                            'totalPayment' => $total_payment,
                            'sessionsCompleted' => (int)$request->sessions
                        ]
                    ]);

                    $responseData = json_decode($response->getBody()->getContents(), true);
                    $status = $response->getStatusCode();
                    if ($status != 200 && $status != 201) {
                        return $this->error_response(
                            __('validation.handler.unexpected_failure'),
                            Response::HTTP_UNPROCESSABLE_ENTITY,
                            $responseData
                        );
                    }

                    $reviewed_plans[] = $plan->id;
                }
            }

            $sessions_completed = isset($report->sessionsCompleted) ? (intval($report->sessionsCompleted) + intval($request->sessions)) : intval($request->sessions);
            $report->update([
                'reviewed' => 1,
                'reviewed_plans' => json_encode($reviewed_plans),
                'name_reviewer' => $user->name . ' ' . $user->surname,
                'sessionsCompleted' => $sessions_completed
            ]);

            return $this->success_message(
                __('validation.handler.success'),
                Response::HTTP_OK,
                Response::HTTP_OK,
                'Reporte de actividades revisado'
            );
        } catch (\Throwable $th) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $th->getMessage()
            );
        }
    }

    public function reviewedTechnicalReportActivity(Request $request)
    {
        try {
            $user = Auth::guard('api')->user();
            $report = ActivityReport::find($request->id);
            if (!isset($report)) {
                return $this->error_response(
                    __('validation.handler.unexpected_failure'),
                    Response::HTTP_UNPROCESSABLE_ENTITY,
                    'Informe de actividad no encontrado'
                );
            }
            $report->update([
                'reviewed_technical' => 1,
                'name_reviewer_technical' => $user->name . ' ' . $user->surname,
                'sessionsCompleted' =>$request->sessions,
            ]);
            return $this->success_message(
                __('validation.handler.success'),
                Response::HTTP_OK,
                Response::HTTP_OK,
                'Reporte de actividades revisado técnicamente'
            );
        } catch (\Throwable $th) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $th->getMessage()
            );
        }
    }


    public function rejectReportActivity(Request $request)
    {
        try {
            $user = Auth::guard('api')->user();
            $report = ActivityReport::with(['contractor'])->find($request->id);
            if (!$report) {
                return $this->error_response(
                    __('validation.handler.unexpected_failure'),
                    Response::HTTP_NOT_FOUND,
                    'Informe de actividad no encontrado'
                );
            }
            $report->update([
                'state' => ActivityReport::STATUS_REJECTED,
                'observations' => $request->observations,
                'reviewer' => $user->email,
            ]);
            $contractor = $report->contractor;
            $this->dispatch(new ActivityReportRejected($contractor, $report, $request->get('observations')));
            return $this->success_message(
                __('validation.handler.success'),
                Response::HTTP_OK,
                'Reporte de actividades rechazado correctamente'
            );
        } catch (\Throwable $th) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $th->getMessage()
            );
        }
    }

    public function getRangeDates(Request $request)
    {
        try {
            $range_dates = ActivityReportDate::where('code', 'RG')->first();
            if (!$range_dates) {
                return $this->error_response(
                    __('validation.handler.unexpected_failure'),
                    Response::HTTP_NOT_FOUND,
                    'Rango de fechas no encontrado'
                );
            }
            return $this->success_response(
                new ActivityReportDateResource($range_dates),
                Response::HTTP_OK
            );
        } catch (\Throwable $th) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $th->getMessage()
            );
        }
    }

    public function reminder(Request $request)
    {
        try {
            $user = Auth::guard('api')->user();
            $contractor = Contractor::where('document', $user->document)->first();
            $pending_report = false;
            $current_day = Carbon::now()->day;
            if ($current_day >= 25){
                $current_date = Carbon::now()->format('Y-m');
                $report = ActivityReport::where('contractor_id', $contractor->id)->where('report_date', $current_date)->first();
                $pending_report = !isset($report) || $report->state === ActivityReport::STATUS_DRAFT;
            }
            return $this->error_response(
                __('validation.handler.success'),
                Response::HTTP_OK,
                ['pending' => $pending_report]
            );
            return $this->success_response(
                new ActivityReportDateResource($range_dates),
                Response::HTTP_OK
            );
        } catch (\Throwable $th) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $th->getMessage()
            );
        }
    }

    public function reminder_final_report(Request $request)
    {
        try {
            $user = Auth::guard('api')->user();
            $contractor = Contractor::where('document', $user->document)->first();

            if (!$contractor) {
                return $this->error_response('Contractor not found', Response::HTTP_NOT_FOUND);
            }
            $contract = Contract::where('contractor_id', $contractor->id)
                                ->orderBy('id', 'desc')
                                ->first();
            if (!$contract) {
                return $this->error_response('Contract not found', Response::HTTP_NOT_FOUND);
            }
            $now = Carbon::now();
            $daysDifference = $now->diffInDays($contract->final_date, false);
            $daysCheck = ($daysDifference > 0 && $daysDifference <= 5)
                            ? [true, $daysDifference]
                            : [false, 0];
            return response()->json([
                'message' => __('validation.handler.success'),
                'pending' => $daysCheck
            ], Response::HTTP_OK);

        } catch (\Throwable $th) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $th->getMessage()
            );
        }
    }

    public function createPDF(ActivityReport $report)
    {
        $report = $report->load(['contractor', 'evidences', 'contributions']);
        $contract = Contract::find($report->contract_id);
        $contractor = $report->contractor;
        $user = User::includeExpiredAndTrashed()->where('document', $contractor->document)->first();
        $supervisor_user = User::find($report->supervisor_id);
        $evidences = $report->evidences->sortBy('sequence');
        $numberArray = array_reverse(explode('-', $contract->contract));
        $numberContract = $numberArray[1] . '-' . $numberArray[0];
        $start_date = $this->getStartDate($contract, $numberContract);
        $final_date = Carbon::parse($contract->final_date)->format('d/m/Y');
        $object_contract = $contract->object_contract;
        if (!isset($object_contract)) {
            $secop_contract = get_active_contract_secop_api($contractor->document, $contract->contract);
            if (isset($secop_contract)) {
                $object_contract = $secop_contract['objeto_del_contrato'];
            }
        }
        $document_init = DocumentInit::where('contract_id', $contract->id)->first();

        $src_logo = 'data:image/png;base64,' . base64_encode(Storage::get('public/idrd.png'));
        $src_signature_user = 'data:image/' . pathinfo($user->signature, PATHINFO_EXTENSION) . ';base64,' . base64_encode(Storage::get('user/'.$user->signature));
        $src_signature_supervisor = '';
        if ($supervisor_user != null){
            $src_signature_supervisor = 'data:image/'. pathinfo($supervisor_user->signature, PATHINFO_EXTENSION) .';base64,' . base64_encode(Storage::get('user/'.$supervisor_user->signature));
        }

        $suspension_contract = Contract::where('contract', 'like', '%' . $numberContract . '%')->where('contract_type_id', Contract::CONTRACT_SUSPENSION)->get();
        $addition_contract = Contract::where('contract', 'like', '%' . $numberContract . '%')->whereIn('contract_type_id', [Contract::CONTRACT_ADD_AND_EXT, Contract::CONTRACT_EXTENSION])->first();
        $datesStringSuspension = $suspension_contract->map(function ($contract) {
            return substr($contract->start_suspension_date, 0, 10) . ' - ' . substr($contract->final_suspension_date, 0, 10);
        })->implode(', ');

        $contractDetails = [
            'NOMBRE DEL CONTRATISTA' => $contractor->name . ' ' . $contractor->surname,
            'NÚMERO DE IDENTIFICACIÓN' => $contractor->document,
            'PLAZO DE EJECUCIÓN' => $this->durationText($contract, $document_init),
            'VALOR' => $this->getCurrencyFormat($contract->total),
            'FECHA ACTA DE INICIO' => $start_date,
            'FECHA DE TERMINACIÓN' => $final_date,
            'ADICIÓN Y/O PRÓRROGA' => isset($addition_contract) ? substr($addition_contract->start_date, 0, 10).'  -  '.substr($addition_contract->final_date, 0, 10) : 'NO APLICA',
            'SUSPENSIONES' => $suspension_contract->isNotEmpty() ? $datesStringSuspension : 'NO APLICA',
            'VALOR HONORARIOS MENSUAL' =>  $this->getCurrencyFormat(isset($document_init) ? $document_init->value_month : 0),
            'PERIODO DE ACTIVIDADES DE ESTE INFORME' => implode(' - ', explode(',', $report->activity_period)),
            'OBJETO DEL CONTRATO' => $object_contract
        ];

        if($contract->dependency_id == 113){
            $newElement = ['JORNADAS RESTANTES POR EJECUTAR' => $this->calculateMissingSessions($contract)];
            $contractDetails = array_slice($contractDetails, 0, array_search('OBJETO DEL CONTRATO', array_keys($contractDetails)), true)
                             + $newElement
                             + array_slice($contractDetails, array_search('OBJETO DEL CONTRATO', array_keys($contractDetails)), null, true);

            $contractDetails['TOTAL JORNADAS A PAGAR'] = $this->get_total_sessions($contract);
        }


        $total = $report->risks_value + $report->afp_value + $report->eps_value;
        $contributions = [
            ['PAGO APORTES SALUD', $report->eps_name, isset($report->eps_value) ? $this->getCurrencyFormat($report->eps_value) : 'N/A'],
            ['PAGO APORTES PENSIÓN', $report->afp_name, isset($report->afp_value) ? $this->getCurrencyFormat($report->afp_value) : 'N/A'],
            ['PAGO RIESGOS LABORALES', $report->risks_name, isset($report->risks_value) ? $this->getCurrencyFormat($report->risks_value) : 'N/A'],
            ['', 'TOTAL', isset($report->eps_value) ? $this->getCurrencyFormat($total) : 'N/A']
        ];

        $reportDate = Carbon::parse($report->report_date);
        $reportDateComplete = $reportDate->endOfMonth()->format('d-m-Y');
        $virtual_expedient_number = isset($report->virtual_expedient_number) ? $report->virtual_expedient_number : 'No definido';
        $base_path = 'storage/app/activity-reports/' . $report->contract_number . '/';

        $social_security_link = '';
        if (isset($report->social_security_attachment_path)){
            $path = $base_path . $report->social_security_attachment_path;
            $social_security_link = '<br/><a href="'.str_replace('public', '', url($path)).'">Ver Planilla de Pago</a>';
        }

        $attachments_link = '';
        if (isset($report->attachments_path)){
            $path = $base_path . $report->attachments_path;
            $attachments_link = '<a href="'.str_replace('public', '', url($path)).'">Ver Informe Final</a>';
        }

        $peaceandsafe_link = '';
        if (isset($report->peaceandsafe_path)){
            $path = $base_path . $report->peaceandsafe_path;
            $peaceandsafe_link = '<br/><br/><a href="'.str_replace('public', '', url($path)).'">Ver Paz y Salvo Sistemas</a>';
        }

        $peaceandsafe_warehouse_link = '';
        if (isset($report->peaceandsafe_warehouse_path)){
            $path = $base_path . $report->peaceandsafe_warehouse_path;
            $peaceandsafe_warehouse_link = '<br/><br/><a href="'.str_replace('public', '', url($path)).'">Ver Paz y Salvo Almacén</a>';
        }

        $html = '
        <!DOCTYPE html>
        <html lang="es">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Informe de Actividades</title>
            <style>
                @page {
                    margin: 24mm 18mm 24mm 28mm;
                }
                body {
                    font-family: Arial, sans-serif;
                    line-height: 1.3;
                    margin: 0;
                    font-size: 12px;
                }
                .text-center {
                    text-align: center;
                }
                .table {
                    width: 100%;
                    border-collapse: collapse;
                }
                .table > th, td {
                    border: 1px solid #000;
                    padding: 5px;
                    text-align: left;
                    width: 50%;
                }
                .custom-table {
                    border-collapse: collapse;
                    width: 100%;
                }
                .bordered {
                    border: 1px solid black;
                    width: 33%;
                    text-align: center;
                }
                .empty-column {
                    border: none;
                    width: 33%;
                }
                .compact-text p {
                    margin: 5px 0;
                    padding: 0;
                }
                .row {
                    width: 97%;
                    padding: 8px;
                    border-bottom: 1px solid black;
                }
            </style>
        </head>
        <body>
            <section>
                <div class="text-center">
                    <img src="' . $src_logo . '" width="270" height="80" />
                    <h2>INFORME DE ACTIVIDADES Y CONCEPTO DEL SUPERVISOR DE CONTRATOS DEL IDRD</h2>
                </div>
                <p>NO APLICA PARA: CONTRATOS DE OBRA, CONSULTORÍA, NI GUARDIANES, (QUIENES DEBEN PRESENTAR SU INFORME EN EL FORMATO QUE ACTUALMENTE REPORTAN)</p>
            </section>
            <br/>
            <section>
                <table class="custom-table">
                    <tr>
                        <th class="bordered">INFORME No.</th>
                        <th class="empty-column"></th>
                        <th class="bordered">No. DEL CONTRATO Y FECHA</th>
                    </tr>
                    <tr>
                        <td class="bordered">0'.$report->report_number.'</td>
                        <td class="empty-column"></td>
                        <td class="bordered">'.$report->contract_number.'</td>
                    </tr>
                    <tr>
                        <th class="bordered">FECHA DEL INFORME</th>
                        <th class="empty-column"></th>
                        <th class="bordered">No. DEL EXPEDIENTE VIRTUAL</th>
                    </tr>
                    <tr>
                        <td class="bordered">'.$reportDateComplete.'</td>
                        <td class="empty-column"></td>
                        <td class="bordered">'.$virtual_expedient_number.'</td>
                    </tr>
                </table>
            </section>
            <br/>
            <section>
                <h2>1. INFORMACIÓN GENERAL</h2>
                <br/>
                <table class="table">';


        foreach ($contractDetails as $label => $value) {
            $html .= '
            <tr>
                <td>' . $label . '</td>
                <td>' . $value . '</td>
            </tr>';
        }

        $html .= '
                </table>
            </section>
            <br/>
            <section>
                <h2>2. DESCRIPCIÓN DE ACTIVIDADES</h2>
                <p>Enunciar cada una de las obligaciones específicas establecidas en el contrato y describir las actividades realizadas en el período para el cumplimiento de la misma, así como enunciar las evidencias y su ubicación (actas, documentos, planillas, listados, etc.).</p>
                <br/><br/>
        ';

        $has_inner_table = $evidences->filter(function($evidence) {
            return strpos($evidence->evidence_text, '<table><tbody>') !== false;
        })->isNotEmpty();

        if ($has_inner_table){
            $html .= '
                <div style="border: 1px solid black;">
            ';
            foreach ($evidences as $evidence) {
                $innerContent = $evidence->evidence_text;
                $html .= '
                    <div style="clear: both;"></div>
                    <div class="row">
                        <b>Obligación ' . $evidence->sequence . ':</b> ' . $evidence->obligation_text . '
                        <br/></br>
                        <b>Actividades, Evidencias y Ubicación:</b>
                        <br/>
                        ' . $innerContent . (empty($evidence->evidence_link) ? '' : '<br/><a href="' . $evidence->evidence_link . '">Evidencias</a>') . '
                    </div>';
            }
            $html .= '</div>';
        } else {
            $html .= '
                <table class="table">
                    <tr>
                        <td><b>OBLIGACIONES ESPECÍFICAS</b></td>
                        <td><b>ACTIVIDADES, EVIDENCIAS Y UBICACIÓN</b></td>
                    </tr>
            ';
            foreach ($evidences as $evidence) {
                $html .= '
                <tr>
                    <td><b>Obligación '.$evidence->sequence.': </b>' . $evidence->obligation_text . '</td>
                    <td>' . $evidence->evidence_text . (empty($evidence->evidence_link) ? '' : '<a href="' . $evidence->evidence_link . '">Evidencias</a>') . '</td>
                </tr>';
            }
            $html .= '</table>';
        }

        $html .= '
            </section>
            <br/>
            <section>
                <h2>3. INFORMACIÓN APORTES PRESTACIONES SOCIALES</h2>
                <br/>
                <table class="table">
                    <tr>
                        <td><b>DESCRIPCIÓN DEL APORTE</b></td>
                        <td><b>NOMBRE EMPRESA</b></td>
                        <td><b>VALOR APORTE PAGADO</b></td>
                    </tr>
        ';

        foreach ($contributions as $contribution) {
            $html .= '
            <tr>
                <td>'. $contribution[0] .'</td>
                <td>'. $contribution[1] .'</td>
                <td>'. $contribution[2] .'</td>
            </tr>';
        }

        $html .= '
                    </table>
                    '. $social_security_link .'
                </section>
                <br/>
                <section>
                    <h2>4. ANEXOS</h2>
                    <p>Para la entrega del informe correspondiente al último periodo de actividades; es indispensable anexar adicionalmente: el documento expedido por el Almacén General y el Área de Sistemas donde se evidencie que el contratista se encuentra a paz y salvo con la entidad y el medio magnético con las evidencias de las actividades realizadas durante la ejecución contractual.</p>
                    '. $attachments_link . $peaceandsafe_link . $peaceandsafe_warehouse_link .'
                </section>
                <section>
                    <div class="compact-text">
                        <img src="' . $src_signature_user . '" width="180" height="120" style="border-bottom: 2px solid #000;" />
                        <br/>
                        <b>Firma Contratista</b>
                        <p><b>Nombre Completo:</b> '.$user->name . ' ' . $user->surname.'</p>
                        <p><b>No. Identificación:</b> '.$user->document.'</p>
                        <p><b>Cargo:</b> '.$user->description.'</p>
                    </div>
                    <br/>
                    <p>CONCEPTO DEL SUPERVISOR DEL CONTRATO SOBRE EL CUMPLIMIENTO DE LAS OBLIGACIONES DEL CONTRATISTA</p>
                    <p>El contratista cumple con las obligaciones contractuales y las tareas asignadas en los tiempos pactados, demostrando su compromiso, calidad y colaboración con el equipo de trabajo.</p>
                    <br/>
                    <div class="compact-text">
                        <img src="' . $src_signature_supervisor . '" width="180" height="120" style="border-bottom: 2px solid #000;" />
                        <br/>
                        <b>Firma Supervisor</b>
                        <p><b>Nombre Completo:</b> '.( $supervisor_user != null ? $supervisor_user->name . ' ' . $supervisor_user->surname : 'Undefined' ).'</p>
                        <p><b>No. Identificación:</b> '.( $supervisor_user != null ? $supervisor_user->document : 'Undefined' ).'</p>
                        <p><b>Cargo:</b> '.( $supervisor_user != null ? $supervisor_user->description : 'Undefined' ).'</p>
                    </div>
                </section>
                <br/>
                </body>
            </html>
        ';

        $options = new Options();
        $options->set('defaultFont', 'Arial');
        $options->set('isHtml5ParserEnabled', true);
        $options->set('isRemoteEnabled', true);
        $options->set("isPhpEnabled", true);
        $dompdf = new Dompdf($options);

        $dompdf->loadHtml($html);
        $dompdf->setPaper('A4', 'portrait');
        $dompdf->render();

        $canvas = $dompdf->getCanvas();
        $fontMetrics = $dompdf->getFontMetrics();
        $pageCount = $canvas->get_page_count();

        $canvas->page_script(function ($pageNumber, $pageCount, $canvas, $fontMetrics) {
            $text = sprintf("Página %d de %d", $pageNumber, $pageCount);
            $font = $fontMetrics->getFont('Arial', 'normal');
            $size = 9;
            $color = array(0, 0, 0);
            $x = 520;
            $y = 800;
            $canvas->text($x, $y, $text, $font, $size, $color);
        });

        $result = $dompdf->output();

        return $this->success_message([
            "file_name" => $report->contract_number . '.pdf',
            "file" => "data:application/pdf;base64," . base64_encode($result)
        ]);
    }

    public function get_missing_sessions($report , $contract){
        $plan =PlanPayment::where('activity_report_id' , $report->id)->first();
        return (isset($plan)) ? $plan->missing_sessions : $this->calculateMissingSessions($contract);
    }

    public function calculateMissingSessions($contract){
        $total_sessions = $this->get_total_sessions($contract);
        $plans = PlanPayment::where('contract_id' , $contract->id)->get();
        $jornadasEjecutadas = 0;
        foreach ($plans as $key => $plan) {
           $jornadasEjecutadas =$jornadasEjecutadas+$plan->amount_day_pay;
        }
        return $total_sessions - $jornadasEjecutadas;
    }

    public function get_total_sessions($contract){
        $documentInit = DocumentInit::where('contract_id' , $contract->id)->first();
        return (isset($documentInit)) ? $documentInit->total_sessions : 0;
    }

    // Obsoleto: Usar createPDF
    public function createPDFOld(ActivityReport $report)
    {
        $report = $report->load(['contractor', 'evidences', 'contributions']);
        $contract = Contract::find($report->contract_id);
        $contractor = $report->contractor;
        $user = User::where('document', $contractor->document)->first();
        $supervisor_user = User::find($report->supervisor_id);
        $evidences = $report->evidences->sortBy('sequence');

        $pdf = new FPDF("P", "mm", "Letter");
        $pdf->AddPage();
        $pdf->setSourceFile(storage_path("app/templates/INFORME_ACTIVIDADES.pdf"));
        $template = $pdf->importPage(1);
        $pdf->useTemplate($template, 0, 0, null, null, true);
        $pdf->SetMargins(28, 24, 18);
        $pdf->SetAutoPageBreak(true, 28);
        $pdf->setHeaderEnabled(true);
        $pdf->setFooterEnabled(true);
        $pageWidth = ($pdf->getTemplateSize($template)['width'] - 48);

        $pdf->Ln(20);
        $font_name = 'Arial';
        $pdf->SetFont($font_name, 'B', 10);
        $pdf->Cell($pageWidth, 5, utf8_decode('INFORME DE ACTIVIDADES Y CONCEPTO DEL SUPERVISOR DE CONTRATOS DEL IDRD'), 0, 1, "C");
        $pdf->Ln(2);
        $pdf->SetFont($font_name, '', 8);
        $pdf->Multicell($pageWidth, 5, utf8_decode('NO APLICA PARA: CONTRATOS DE OBRA, CONSULTORÍA, NI GUARDIANES, (QUIENES DEBEN PRESENTAR SU INFORME EN EL FORMATO QUE ACTUALMENTE REPORTAN)'), 0, "L");
        $pdf->Ln(7);

        $reportDate = Carbon::parse($report->report_date);
        $reportDateComplete = $reportDate->endOfMonth()->format('d-m-Y');

        // $pdf->SetFont($font_name, 'B', 10);
        // $pdf->Cell(($pageWidth / 3), 7, utf8_decode('INFORME No.'), 'LTRB', 0, "C");
        // $pdf->SetX((($pageWidth / 3) * 2) + 25);
        // $pdf->SetFont($font_name, 'B', 10);
        // $pdf->Cell(($pageWidth / 3), 7, utf8_decode('No. DEL CONTRATO Y FECHA'), 'LTRB', 1, "C");

        // $pdf->SetFont($font_name, '', 10);
        // $pdf->Cell(($pageWidth / 3), 7, utf8_decode('0' . $report->report_number), 'LTRB', 0, "C");
        // $pdf->SetX((($pageWidth / 3) * 2) + 25);
        // $pdf->SetFont($font_name, '', 10);
        // $pdf->Cell(($pageWidth / 3), 7, utf8_decode($report->contract_number), 'LTRB', 1, "C");

        $pdf->SetFont($font_name, 'B', 10);
        $pdf->Cell(($pageWidth / 3), 7, utf8_decode('FECHA DEL INFORME'), 'LTRB', 0, "C");
        $pdf->SetX((($pageWidth / 3) * 2) + 25);
        $pdf->SetFont($font_name, 'B', 10);
        $pdf->Cell(($pageWidth / 3), 7, utf8_decode('No. DEL EXPEDIENTE VIRTUAL'), 'LTRB', 1, "C");

        $pdf->SetFont($font_name, '', 10);
        $pdf->Cell(($pageWidth / 3), 7, utf8_decode($reportDateComplete), 'LTRB', 0, "C");
        $pdf->SetX((($pageWidth / 3) * 2) + 25);
        $pdf->SetFont($font_name, '', 10);
        $pdf->Cell(($pageWidth / 3), 7, utf8_decode(isset($report->virtual_expedient_number) ? $report->virtual_expedient_number : 'No definido'), 'LTRB', 1, "C");

        $pdf->Ln(8);
        $pdf->SetFont($font_name, 'B', 12);
        $pdf->Cell($pageWidth, 5, utf8_decode('1. INFORMACIÓN GENERAL'), 0, 1, 'L');
        $pdf->Ln(8);

        $start_date = Carbon::parse($contract->start_date)->format('d/m/Y');
        $final_date = Carbon::parse($contract->final_date)->format('d/m/Y');
        $object_contract = $contract->object_contract;
        if (!isset($object_contract)) {
            $secop_contract = get_active_contract_secop_api($contractor->document, $contract->contract);
            if (isset($secop_contract)) {
                $object_contract = $secop_contract['objeto_del_contrato'];
            }
        }
        $document_init = DocumentInit::where('contract_id', $contract->id)->first();

        $general = [
            ['NOMBRE DEL CONTRATISTA', $contractor->name . ' ' . $contractor->surname],
            ['NÚMERO DE IDENTIFICACIÓN', $contractor->document],
            ['PLAZO DE EJECUCIÓN', $this->durationText($contract, $document_init)],
            ['VALOR', $this->getCurrencyFormat($contract->total)],
            ['FECHA ACTA DE INICIO', $start_date],
            ['FECHA DE TERMINACIÓN', $final_date],
            ['ADICIÓN Y/O PRÓRROGA', $contract->contract_type === 2 ? 'APLICA' : 'NO APLICA'],
            ['SUSPENSIONES', $contract->contract_type === 3 ? 'APLICA' : 'NO APLICA'],
            ['VALOR HONORARIOS MENSUAL',  $this->getCurrencyFormat(isset($document_init) ? $document_init->value_month : 0)],
            ['PERIODO DE ACTIVIDADES DE ESTE INFORME', implode(' - ', explode(',', $report->activity_period))]
        ];
        $columnWidth = $pageWidth / 2;
        $pdf->SetFont($font_name, '', 10);
        foreach ($general as $item) {
            $pdf->Cell($columnWidth, 8, utf8_decode($item[0]), 'LTRB', 0, 'L');
            $pdf->Cell($columnWidth, 8, utf8_decode($item[1]), 'LTRB', 1, 'L');
        }
        $pdf->Cell($pageWidth, 7, utf8_decode('OBJETO DEL CONTRATO'), 'LTRB', 1, 'L');
        $pdf->SetFont($font_name, '', 10);
        $pdf->Multicell($pageWidth, 7, utf8_decode($object_contract), 1, 'L');

        $pdf->Ln(8);
        $pdf->SetFont($font_name, 'B', 12);
        $pdf->Cell($pageWidth, 5, utf8_decode('2. DESCRIPCIÓN DE ACTIVIDADES'), 0, 1, 'L');
        $pdf->Ln(2);
        $pdf->SetFont($font_name, '', 10);
        $pdf->Multicell($pageWidth, 5, utf8_decode('Enunciar cada una de las obligaciones específicas establecidas en el contrato y describir las actividades realizadas en el período para el cumplimiento de la misma, así como enunciar las evidencias y su ubicación (actas, documentos, planillas, listados, etc.).'), 0, 'L');
        $pdf->Ln(30);

        $pdf->SetFont($font_name, 'B', 10);
        $pdf->Cell($columnWidth, 5, utf8_decode('Obligaciones específicas'), 'LTRB', 0, 'C');
        $pdf->Cell($columnWidth, 5, utf8_decode('Actividades, evidencias y ubicación'), 'LTRB', 1, 'C');

        $pdf->SetWidths(array($columnWidth, $columnWidth));
        $pdf->SetFont($font_name, '', 10);
        foreach ($evidences as $evidence) {
            $obligation_text = Str::replaceFirst('Obligación ' . $evidence->sequence, '', $evidence->obligation_text);
            $pdf->Row(array(utf8_decode('Obligación ' . $evidence->sequence . ' ' . $obligation_text), utf8_decode($evidence->evidence_text) . '   ' . utf8_decode($evidence->evidence_link)));
        }
        $pdf->Multicell($pageWidth, 5, utf8_decode('Declaración: Manifiesto que he cumplido con las obligaciones derivadas del contrato y que las actividades mencionadas en el presente informe corresponden a las labores efectivamente desarrolladas en el período indicado, declarando que seré responsable por las afirmaciones contenidas en el presente documento, que sirve como soporte para certificar el cumplimiento del objeto del contrato'), 1, 'L');

        $pdf->Ln(8);
        $pdf->SetFont($font_name, 'B', 12);
        $pdf->Cell($pageWidth, 5, utf8_decode('3. INFORMACIÓN APORTES PRESTACIONES SOCIALES'), 0, 1, 'L');
        $pdf->Ln(7);

        $total = $report->risks_value + $report->afp_value + $report->eps_value;
        $contributions = [
            ['DESCRIPCIÓN DEL APORTE', 'NOMBRE EMPRESA', 'VALOR APORTE PAGADO'],
            ['PAGO APORTES SALUD', $report->eps_name, isset($report->eps_value) ? $this->getCurrencyFormat($report->eps_value) : 'N/A'],
            ['PAGO APORTES PENSIÓN', $report->afp_name, isset($report->afp_value) ? $this->getCurrencyFormat($report->afp_value) : 'N/A'],
            ['PAGO RIESGOS LABORALES', $report->risks_name, isset($report->risks_value) ? $this->getCurrencyFormat($report->risks_value) : 'N/A'],
            ['', 'TOTAL', isset($report->eps_value) ? $this->getCurrencyFormat($total) : 'N/A']
        ];

        $pdf->SetFont($font_name, 'B', 10);
        $columnWidth = $pageWidth / 3;
        $pdf->SetWidths(array($columnWidth, $columnWidth, $columnWidth));
        foreach ($contributions as $item) {
            $pdf->Row(array(utf8_decode($item[0]), utf8_decode($item[1]), utf8_decode($item[2])));
            $pdf->SetFont($font_name, '', 10);
        }

        $pdf->Ln(4);

        if (isset($report->social_security_attachment_path)) {
            $pdf->SetFont($font_name, 'U', 10);
            $pdf->SetTextColor(0, 0, 255);
            $path = '/storage/app/activity-reports/' . $report->contract_number . '/' . $report->social_security_attachment_path;
            $link = str_replace('public', '', url($path));
            $pdf->Cell(0, 10, utf8_decode('Ver Archivo Anexo'), 0, 1, 'L', false, $link);
            $pdf->SetTextColor(0, 0, 0);
        }

        $pdf->Ln(4);
        $pdf->SetFont($font_name, 'B', 12);
        $pdf->Cell($pageWidth, 5, utf8_decode('4. ANEXOS'), 0, 1, 'L');
        $pdf->Ln(2);
        $pdf->SetFont($font_name, '', 10);
        $pdf->Multicell($pageWidth, 5, utf8_decode('Para la entrega del informe correspondiente al último periodo de actividades; es indispensable anexar adicionalmente: el documento expedido por el Almacén General y el Área de Sistemas donde se evidencie que el contratista se encuentra a paz y salvo con la entidad y el medio magnético con las evidencias de las actividades realizadas durante la ejecución contractual.'), 0, 'L');
        $pdf->Ln(4);

        if (isset($report->attachments_path)) {
            $pdf->SetFont($font_name, 'U', 10);
            $pdf->SetTextColor(0, 0, 255);
            $path = '/storage/app/activity-reports/' . $report->contract_number . '/' . $report->attachments_path;
            $link = str_replace('public', '', url($path));
            $pdf->Cell(0, 10, utf8_decode('Ver Informe Final'), 0, 1, 'L', false, $link);
            $pdf->SetTextColor(0, 0, 0);
        }

        if (isset($report->peaceandsafe_path)) {
            $pdf->SetFont($font_name, 'U', 10);
            $pdf->SetTextColor(0, 0, 255);
            $path = '/storage/app/activity-reports/' . $report->contract_number . '/' . $report->peaceandsafe_path;
            $link = str_replace('public', '', url($path));
            $pdf->Cell(0, 10, utf8_decode('Ver Paz y Salvo Sistemas'), 0, 1, 'L', false, $link);
            $pdf->SetTextColor(0, 0, 0);
        }


        if (isset($report->peaceandsafe_warehouse_path)) {
            $pdf->SetFont($font_name, 'U', 10);
            $pdf->SetTextColor(0, 0, 255);
            $path = '/storage/app/activity-reports/' . $report->contract_number . '/' . $report->peaceandsafe_warehouse_path;
            $link = str_replace('public', '', url($path));
            $pdf->Cell(0, 10, utf8_decode('Ver Paz y Salvo Almacén'), 0, 1, 'L', false, $link);
            $pdf->SetTextColor(0, 0, 0);
        }

        $pdf->Ln(4);
        $pdf->SetFont($font_name, 'B', 10);
        if (isset($user->signature)) {
            $signaturePath = storage_path('app/user/' . $user->signature);
            $jpgSignaturePath = storage_path('app/user/' . pathinfo($user->signature, PATHINFO_FILENAME) . '.jpg');
            if ($this->convertToJPG($signaturePath, $jpgSignaturePath)) {
                $pdf->Image($jpgSignaturePath, $pdf->GetX(), $pdf->GetY(), 40);
                $pdf->Ln(17);
            }
        }
        $pdf->Cell(($pageWidth / 3), 1, '', 'T', 1, 'L');
        $pdf->Cell($pageWidth, 5, utf8_decode('Firma Contratista'), 0, 1, 'L');
        $pdf->SetFont($font_name, '', 10);
        $pdf->Cell($pageWidth, 5, utf8_decode('Nombre Completo: ' . $user->name . ' ' . $user->surname), 0, 1, 'L');
        $pdf->Cell($pageWidth, 5, utf8_decode('No. Identificación: ' . $user->document), 0, 1, 'L');
        $pdf->Cell($pageWidth, 5, utf8_decode('Cargo: ' . $user->description), 0, 1, 'L');
        $pdf->Ln(7);

        $pdf->SetFont($font_name, '', 10);
        $pdf->Multicell($pageWidth, 5, utf8_decode('CONCEPTO DEL SUPERVISOR DEL CONTRATO SOBRE EL CUMPLIMIENTO DE LAS OBLIGACIONES DEL CONTRATISTA'), 0, 'L');
        $pdf->Ln(1);
        $pdf->Multicell($pageWidth, 5, utf8_decode('El contratista cumple con las obligaciones contractuales y las tareas asignadas en los tiempos pactados, demostrando su compromiso, calidad y colaboración con el equipo de trabajo.'), 0, 'L');

        $pdf->Ln(7);
        $pdf->SetFont($font_name, 'B', 10);
        if (isset($supervisor_user->signature)) {
            $file = storage_path('app/user/' . $supervisor_user->signature);
            $pdf->Image($file, $pdf->GetX(), $pdf->GetY(), 40);
            $pdf->Ln(17);
        }
        $pdf->Cell(($pageWidth / 3), 1, '', 'T', 1, 'L');
        $pdf->Cell($pageWidth, 5, utf8_decode('Firma Supervisor'), 0, 1, 'L');
        $pdf->SetFont($font_name, '', 10);
        if ($supervisor_user) {
            $pdf->Cell($pageWidth, 5, utf8_decode('Nombre Completo: ' . $supervisor_user->name . ' ' . $supervisor_user->surname), 0, 1, 'L');
            $pdf->Cell($pageWidth, 5, utf8_decode('No. Identificación: ' . $supervisor_user->document), 0, 1, 'L');
            $pdf->Cell($pageWidth, 5, utf8_decode('Cargo: ' . $supervisor_user->description), 0, 1, 'L');
        } else {
            $pdf->Cell($pageWidth, 5, utf8_decode('Nombre Completo: No disponible'), 0, 1, 'L');
            $pdf->Cell($pageWidth, 5, utf8_decode('No. Identificación: No disponible'), 0, 1, 'L');
            $pdf->Cell($pageWidth, 5, utf8_decode('Cargo: No disponible'), 0, 1, 'L');
        }
        $pdf->Ln(7);

        $result = $pdf->Output("S", "Informe_de_Actividades.pdf");
        return $this->success_message([
            "file_name" => $report->contract_number . '.pdf',
            "file" => "data:application/pdf;base64," . base64_encode($result)
        ]);
    }

    private function durationText($contract, $document_init)
    {
        if (isset($document_init)){
            $months = $document_init->months_contract;
            $days = $document_init->days_contract;
            if ($days == 0) {
                return "$months meses";
            }
            return "$months meses $days días";
        }

        $start_date = Carbon::createFromFormat('Y-m-d', substr($contract->start_date, 0, 10));
        $final_date = Carbon::createFromFormat('Y-m-d', substr($contract->final_date, 0, 10));
        $difference = $start_date->diff($final_date);
        if ($difference->y > 0 || $difference->m > 0) {
            return "{$difference->m} meses" . ($difference->d > 0 ? " {$difference->d} días" : "");
        }
        return $difference->days > 0 ? "{$difference->days} días" : "";
    }

    private function getStartDate($contract, $numberContract)
    {
        if (in_array($contract->contract_type_id, [Contract::NEW_CONTRACT, Contract::CONTRACT_ASSIGNMENT])){
            return isset($contract->start_date) ? $contract->start_date->format('Y-m-d') : null;
        }
        $original_contract = Contract::where('contract', 'like', '%' . $numberContract . '%')->where('contract_type_id', Contract::NEW_CONTRACT)->first();
        if (isset($original_contract)){
            return isset($original_contract->start_date) ? $original_contract->start_date->format('Y-m-d') : null;
        }
        return null;
    }

    public function getCurrencyFormat($value)
    {
        $formatter = new NumberFormatter('es_CO', NumberFormatter::CURRENCY);
        return $formatter->formatCurrency($value, 'COP');
    }

    public function validate_reports_create(Request $request)
    {
        try {
            $user = Auth::guard('api')->user();
            $contractor = Contractor::where('document', $user->document)->first();

            if (!$contractor) {
                return $this->error_response('Contractor not found', Response::HTTP_NOT_FOUND);
            }
            $contract = Contract::where('contractor_id', $contractor->id)
                                ->orderBy('id', 'desc')
                                ->first();

            if (!$contract) {
                return $this->error_response('Contract not found', Response::HTTP_NOT_FOUND);
            }

            $document_init = DocumentInit::where('contract_id', $contract->id)->first();
            return response()->json([
                'message' => __('validation.handler.success'),
                'validate' => $document_init ? true : false,
            ], Response::HTTP_OK);

        } catch (\Throwable $th) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $th->getMessage()
            );
        }
    }
}

