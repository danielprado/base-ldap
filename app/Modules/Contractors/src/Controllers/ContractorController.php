<?php


namespace App\Modules\Contractors\src\Controllers;


use App\Http\Controllers\Controller;
use App\Jobs\NotifyUserOfCompletedExport;
use App\Jobs\RestartStatusJob;
use App\Models\Security\User;
use App\Modules\Contractors\src\Constants\GlobalQuery;
use App\Modules\Contractors\src\Constants\Roles;
use App\Modules\Contractors\src\Exports\ContractorsExport;
use App\Modules\Contractors\src\Exports\DataExport;
use App\Modules\Contractors\src\Jobs\ChangeAccountBankContractor;
use App\Modules\Contractors\src\Jobs\CheckThirdParty;
use App\Modules\Contractors\src\Jobs\ConfirmAccountBankContractor;
use App\Modules\Contractors\src\Jobs\ConfirmContractor;
use App\Modules\Contractors\src\Jobs\ConfirmUpdateContractor;
use App\Modules\Contractors\src\Jobs\ProcessExport;
use App\Modules\Contractors\src\Jobs\SendDocumentsContractor;
use App\Modules\Contractors\src\Models\ActivityReport;
use App\Modules\Contractors\src\Models\Compliance;
use App\Modules\Contractors\src\Models\ComplianceVerification;
use App\Modules\Contractors\src\Models\Contract;
use App\Modules\Contractors\src\Models\ContractFileCountView;
use App\Modules\Contractors\src\Models\Contractor;
use App\Modules\Contractors\src\Models\ContractorBank;
use App\Modules\Contractors\src\Models\ContractorCareer;
use App\Modules\Contractors\src\Models\ContractorView;
use App\Modules\Contractors\src\Models\ContractType;
use App\Modules\Contractors\src\Models\File;
use App\Modules\Contractors\src\Models\PlanPayment;
use App\Modules\Contractors\src\Notifications\ArlNotification;
use App\Modules\Contractors\src\Notifications\ChangeAccountBankNotification;
use App\Modules\Contractors\src\Notifications\ContractorChangeAccountBankNotification;
use App\Modules\Contractors\src\Request\ExcelRequest;
use App\Modules\Contractors\src\Request\FinderRequest;
use App\Modules\Contractors\src\Request\StoreLawyerRequest;
use App\Modules\Contractors\src\Request\UpdateContractorAccountBankRequest;
use App\Modules\Contractors\src\Request\UpdateContractorLawyerRequest;
use App\Modules\Contractors\src\Request\UpdateContractorPersonalDataRequest;
use App\Modules\Contractors\src\Request\UpdateContractorRequest;
use App\Modules\Contractors\src\Request\UpdateThirdPartyRequest;
use App\Modules\Contractors\src\Resources\ContractorResource;
use App\Modules\Contractors\src\Resources\ContractResource;
use App\Modules\Contractors\src\Resources\UserContractorResource;
use Illuminate\Database\Concerns\BuildsQueries;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use Illuminate\Support\Facades\Storage;
use Imtigger\LaravelJobStatus\JobStatus;
use Maatwebsite\Excel\Excel;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Throwable;

class ContractorController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function counter()
    {
        return $this->success_message([
            'total'  => Contractor::count(),
            'arl'    =>  ContractFileCountView::withArl()->count(),
            'secop' =>  ContractFileCountView::withSecop()->count(),
            'users' =>  Contractor::whereNotNull('modifiable')->count(),
            'active' => Contract::query()
                ->whereHas('files', function ($q) {
                    return $q->where('file_type_id', 1);
                })
                ->where('contract_type_id', '!=', 3)
                ->whereDate('final_date', '>', now()->format('Y-m-d'))
                ->count(),
            'inactive' => Contract::query()
                ->whereHas('files', function ($q) {
                    return $q->where('file_type_id', 1);
                })
                ->where('contract_type_id', '!=', 3)
                ->whereDate('final_date', '<=', now()->format('Y-m-d'))
                ->count(),
            'certified' => [
                'arl'   => ContractFileCountView::withArl()->count(),
                'not_arl'   => ContractFileCountView::withoutArl()->count()
            ]
        ]);
    }

    public function stats()
    {

        $collection = Contract::query()
            ->withCount([
                'files' => function ($query) {
                    return $query->where('file_type_id', 1);
                },
            ])
            ->whereDate('final_date', '>=', now()->format('Y-m-d'))
            ->where('contract_type_id', '!=', 3)
            ->latest()
            ->get(['files_count', 'contractor_id', 'created_at'])
            ->sortByDesc('created_at')
            ->unique('contractor_id');

        $with_arl = $collection
            ->filter(function ($value, $key) {
                return $value['files_count'] > 0;
            })
            ->count();
        $without_arl = $collection
            ->whereNotIn(
                'contractor_id',
                Contractor::query()
                    ->whereNotNull('modifiable')
                    ->get()
                    ->pluck('id')
                    ->toArray()
            )
            ->filter(function ($value, $key) {
                return $value['files_count'] == 0;
            })
            ->count();

        $year = now()->year;
        $subYear = now()->subYear()->year;
        $subdirectories = Contract::query()
            ->select('name', DB::raw('(COUNT(*)) AS count'))
            ->from('idrdgov_sim_ldap.subdirectorates as subdirectorate')
            ->join('contracts as contract', 'contract.subdirectorate_id', '=', 'subdirectorate.id')
            ->where('contract', 'like', "IDRD-CTO-_%$year")
            ->orWhere('contract', 'like', "IDRD-CTO-_%$subYear")
            ->groupBy('subdirectorate.id')
            ->get();

        return $this->success_message([
            'types' => ContractType::withCount('contracts')->get(),
            'subdirectories' => $subdirectories,
            'dash' => [
                'categories' => Contract::ExtractYears(),
                'series' => [
                    'news' => Contract::NewsByYear(),
                    'additions' => Contract::AdditionByYear(),
                    'suspensions' => Contract::SuspensionByYear(),
                    'extensions' => Contract::ExtensionByYear(),
                    'assignments' => Contract::AssignmentByYear()
                ],
            ]
        ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {

        $contractors = GlobalQuery::query($request, Contractor::query());
        $contractors = $contractors->with('contracts')
            ->latest()
            ->paginate($this->per_page);
        return $this->success_response(
            ContractorResource::collection($contractors),
            Response::HTTP_OK,
            [
                'headers'   =>  ContractorResource::headers(),
                'expanded'  =>  ContractorResource::additionalData(),
            ]
        );
    }

    /**
     * @param ExcelRequest $request
     * @return JsonResponse
     */
    public function excel(ExcelRequest $request)
    {
        $job = new ProcessExport(
            $request->all(),
            $request->user('api'),
            ['queue' => 'excel-contractor-portal', 'user_id' => auth('api')->user()->id]
        );
        $this->dispatch($job);
        return $this->success_message(
            'Estamos generando el reporte solcitado, te notificaremos una vez esté listo.',
            Response::HTTP_OK,
            Response::HTTP_OK,
            $job
        );
    }

    /**
     * Display the specified resource.
     *
     * @param Contractor $contractor
     * @return JsonResponse
     */
    public function show(Contractor $contractor)
    {
        return $this->success_response(
            new ContractorResource($contractor->load('contracts')),
            Response::HTTP_OK,
            [
                'keys'  => array_merge( ContractorResource::headers(), ContractorResource::additionalData())
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param StoreLawyerRequest $request
     * @return JsonResponse
     * @throws Throwable
     */
    public function store(StoreLawyerRequest $request)
    {
        try {
            DB::connection('mysql_contractors')->beginTransaction();
            $form = new Contractor();
            $form->fill($request->validated());
            $form->user_id = auth()->user()->id;
            $form->modifiable = now()->format('Y-m-d H:i:s');
            $form->saveOrFail();
            //$contract_number = str_pad($request->get('contract'), 4, '0', STR_PAD_LEFT);
            //$contract = toUpper("IDRD-CTO-{$contract_number}-{$request->get('contract_year')}");
            $year = $request->get('contract_year');
            if ($request->get('type') !== null) {
                $contract_number = str_pad($request->get('contract'), 4, '0', STR_PAD_LEFT);
                $contract = toUpper("IDRD-{$request->get('type')}-{$contract_number}-{$request->get('contract_year')}");
            } else {
                $contract_number = str_pad($request->get('contract'), 4, '0', STR_PAD_LEFT);
                $contract = toUpper("IDRD-CTO-{$contract_number}-{$request->get('contract_year')}");
            }

            if ($request->get('contract_type_id') == Contract::CONTRACT_ASSIGNMENT) {
                $originalContract = Contractor::query()
                    ->whereHas('contracts', function ($query) use ($contract_number, $year) {
                        $query->where('contract', 'LIKE', "%-{$contract_number}-{$year}");
                    })
                    ->with(['contracts' => function ($query) use ($contract_number, $year) {
                        $query->where('contract', 'LIKE', "%-{$contract_number}-{$year}")
                            ->latest();
                    }])
                    ->firstOrFail();

                $originalContractData = $originalContract->contracts->first();

                if (!$originalContractData) {
                    return $this->error_response(
                        'El contrato original no fue encontrado, por favor verifique.',
                        Response::HTTP_NOT_FOUND
                    );
                }

                $form->contracts()->create(array_merge(
                    $originalContractData->toArray(),
                    $request->validated(),
                    [
                        'contract' => $contract,
                        'lawyer_id' => auth()->user()->id
                    ]
                ));
            } else {
                $form->contracts()->create(array_merge(
                    $request->validated(),
                    ['contract' => $contract],
                    ['lawyer_id' => auth()->user()->id]
                ));
            }
            $this->dispatch(new ConfirmContractor($form));
            DB::connection('mysql_contractors')->commit();
            return $this->success_message(
                __('validation.handler.success'),
                Response::HTTP_CREATED,
                Response::HTTP_CREATED,
                [
                    'id'    => $form->id
                ]
            );
        } catch (Throwable $e) {
            DB::connection('mysql_contractors')->rollBack();
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }

    /**
     * @param Contractor $contractor
     * @return JsonResponse
     */
    public function resendNotification(Contractor $contractor)
    {
        try {
            $contractor->modifiable = now()->format('Y-m-d H:i:s');
            $contractor->saveOrFail();
            $this->dispatch(new ConfirmContractor($contractor));
            return $this->success_message(__('validation.handler.success'));
        } catch (Throwable $e) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }

    /**
     * @param Contractor $contractor
     * @return JsonResponse
     */
    public function accountBankNotification(Contractor $contractor)
    {
        try {
            $this->dispatch(new ConfirmAccountBankContractor($contractor));
            $contract = $contractor->contracts()->latest()->first();
            Notification::send(User::query()->where('document', $contractor->document)->get(), new ContractorChangeAccountBankNotification($contractor, $contract));
            return $this->success_message(__('validation.handler.success'));
        } catch (Throwable $e) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }

    /**
     * @param UpdateContractorLawyerRequest $request
     * @param Contractor $contractor
     * @return JsonResponse
     */
    public function updateBasicData(UpdateContractorLawyerRequest $request, Contractor $contractor)
    {
        try {
            $contractor->fill($request->validated());
            $contractor->saveOrFail();
            if ($request->has('notify') && $request->get('notify')) {
                $contractor->modifiable = now()->format('Y-m-d H:i:s');
                $contractor->saveOrFail();
                $this->dispatch(new ConfirmContractor($contractor));
            }
            return $this->success_message(__('validation.handler.success'));
        } catch (Throwable $e) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }

    /**
     * @param $contractor
     * @return JsonResponse
     */
    public function user($contractor)
    {
        try {
            $document = Crypt::decrypt($contractor);
            $form = Contractor::query()->where('document', $document)->firstOrFail();
            abort_unless(!is_null($form->modifiable), Response::HTTP_UNPROCESSABLE_ENTITY, __('validation.handler.unauthorized'));
            return $this->success_response(
                new UserContractorResource($form)
            );
        } catch (\Exception $exception) {
            return $this->error_response(
                'No fue posible decodificar el recurso',
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $exception->getMessage()
            );
        }
    }

    /**
     * @param $document
     * @return JsonResponse
     */
    public function userContractor($document)
    {
        try {
            $form = Contractor::query()->where('document', $document)->firstOrFail();
            abort_unless(!is_null($form), Response::HTTP_UNPROCESSABLE_ENTITY, __('validation.handler.unauthorized'));
            return $this->success_response(
                new UserContractorResource($form)
            );
        } catch (\Exception $exception) {
            return $this->error_response(
                __('contractor.not_found'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $exception->getMessage()
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateContractorRequest $request
     * @param $contractor
     * @return JsonResponse
     */
    public function update(UpdateContractorRequest $request, $contractor)
    {
        try {
            $document = Crypt::decrypt($contractor);
            $form = Contractor::where('document', $document)->firstOrFail();
            abort_unless(!is_null($form->modifiable), Response::HTTP_UNPROCESSABLE_ENTITY, __('validation.handler.unauthorized'));
            DB::connection('mysql_contractors')->beginTransaction();

            $files = $this->handleFileUploads($request, $form);
            $data = $this->updateAdditionalData($request, $form);

            $form->fill($request->validated());
            // This always after form fill
            $form->rut = $files['rut'];
            $form->bank = $files['bank'];
            $form->photo = $files['photo'];
            $form->modifiable = null;
            $form->bank_id = $data['bank_account']->id;
            $form->saveOrFail();

            Notification::send(
                User::whereIs(Roles::ROLE_ARL, Roles::ROLE_THIRD_PARTY)->get(),
                new ArlNotification($form, $data['contract'])
            );

            $this->dispatch(new ConfirmUpdateContractor($form));
            $this->dispatch(new CheckThirdParty($form));
            DB::connection('mysql_contractors')->commit();
            return $this->success_message(__('validation.handler.updated'));
        } catch (Throwable $e) {
            DB::connection('mysql_contractors')->rollBack();
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $request
     * @param $contractor
     * @return array
     */
    public function handleFileUploads($request, $contractor): array
    {
        $files = [];

        if ($request->has('rut')) {
            $this->deleteExistingFile($contractor->getOriginal('rut'));
            $ext = $request->file('rut')->getClientOriginalExtension();
            $now = now()->format('YmdHis');
            $rut = "RUT_{$contractor->document}_{$now}.$ext";
            $request->file('rut')->storeAs('contractor', $rut, ['disk' => 'local']);
            $files['rut'] = $rut;
        }

        if ($request->has('bank')) {
            $this->deleteExistingFile($contractor->getOriginal('bank'));
            $ext = $request->file('bank')->getClientOriginalExtension();
            $now = now()->format('YmdHis');
            $bank = "CERTIFICADO_BANCARIO_{$contractor->document}_{$now}.$ext";
            $request->file('bank')->storeAs('contractor', $bank, ['disk' => 'local']);
            $files['bank'] = $bank;
        }

        if ($request->has('photo')) {
            $this->deleteExistingFile($contractor->getOriginal('photo'));
            $ext = $request->file('photo')->getClientOriginalExtension();
            $now = now()->format('YmdHis');
            $photo = "PHOTO_{$contractor->document}_{$now}.$ext";
            $request->file('photo')->storeAs('contractor', $photo, ['disk' => 'local']);
            $files['photo'] = $photo;
        }

        return $files;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param $request
     * @param $contractor
     * @return array
     */
    public function updateAdditionalData($request, $contractor): array
    {
        $bank_account = ContractorBank::updateOrCreate(
            ['contractor_id' => $contractor->id],
            $request->only(['bank_id', 'account_type_id', 'number', 'economic_activity_id'])
        );

        $contract = Contract::findOrFail($request->get('contract_id'));
        $contract->update($request->validated());

        ContractorCareer::query()->updateOrCreate(
            ['contractor_id' => $contractor->id],
            $request->only(['career_id', 'graduate', 'year_approved'])
        );

        return compact('bank_account', 'contract');
    }

    private function deleteExistingFile($filePath)
    {
        if ($filePath && Storage::disk('contractor')->exists($filePath)) {
            Storage::disk('contractor')->delete($filePath);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateContractorAccountBankRequest $request
     * @param Contractor $contractor
     * @return JsonResponse
     */
    public function updateAccountBank(UpdateContractorAccountBankRequest $request, Contractor $contractor): JsonResponse
    {
        $time = ComplianceVerification::query()
            ->where('status', '=', 'TIME')
            ->where('code', '=', 'ACB')
            ->first();

        $currentDay = now()->day;
        if ($currentDay < $time->start || $currentDay > $time->end) {
            return $this->error_response(
                "La actualización de cuenta bancaria solo está disponible del $time->start al $time->end de cada mes"
            );
        }

        $lastBankUpdateDate = optional($contractor->bank_account)->updated_at;

        if ($lastBankUpdateDate && $lastBankUpdateDate->month === now()->month) {
            return $this->error_response(
                'Ya se ha realizado una actualización de cuenta bancaria este mes'
            );
        }

        try {
            DB::connection('mysql_contractors')->beginTransaction();

            $files = $this->handleFileUploads($request, $contractor);
            // This always after form fill
            $contractor->rut = $files['rut'];
            $contractor->bank = $files['bank'];
            $contractor->timestamps = false;
            $contractor->saveOrFail();
            $contractor->timestamps = true;
            $contractor->bank_account()->update([
                'bank_id'     =>  $request->get('bank_id'),
                'account_type_id'     =>  $request->get('account_type_id'),
                'number'     =>  $request->get('number'),
                'economic_activity_id'     =>  $request->get('economic_activity_id'),
            ]);
            $bank_account = $contractor->bank_account()->first();
            Notification::send(User::whereIs(Roles::ROLE_THIRD_PARTY)->get(), new ChangeAccountBankNotification($contractor, $bank_account));
            $this->dispatch(new ChangeAccountBankContractor($contractor));
            DB::connection('mysql_contractors')->commit();
            return $this->success_message(
                'Hemos registrado tu solicitud con éxito.',
                Response::HTTP_CREATED,
                Response::HTTP_CREATED,
                'Se enviará un correo electrónico confirmando la actualización de su cuenta bancaria.'
            );
        } catch (Throwable $e) {
            DB::connection('mysql_contractors')->rollBack();
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UpdateContractorPersonalDataRequest $request
     * @param Contractor $contractor
     * @return JsonResponse
     */
    public function updatePersonalData(UpdateContractorPersonalDataRequest $request, Contractor $contractor): JsonResponse
    {
        /*$lastUpdateDate = $contractor->updated_at;
        if ($lastUpdateDate) {
            $now = now();
            $diffInDays = $lastUpdateDate->diffInDays($now);
            $remainingDays = 90 - $diffInDays; // 3 meses aprx

            if ($diffInDays < 90) {
                return $this->error_response(
                    'Ya se ha realizado una actualización de datos en los últimos tres meses. Podrás realizar una nueva actualización en ' . $remainingDays . ' días.'
                );
            }
        }*/

        try {
            DB::connection('mysql_contractors')->beginTransaction();

            $files = $this->handleFileUploads($request, $contractor);
            $contractor->fill($request->validated());
            $contractor->photo = $files['photo'];
            // This always after form fill
            $contractor->saveOrFail();
            $contract = Contract::findOrFail($request->get('contract_id'));
            $contract->update($request->validated());
            DB::connection('mysql_contractors')->commit();
            return $this->success_message(
                'La actualización de datos se ha realizado correctamente.',
                Response::HTTP_CREATED,
                Response::HTTP_CREATED
            );
        } catch (Throwable $e) {
            DB::connection('mysql_contractors')->rollBack();
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }

    /**
     * @param FinderRequest $request
     * @return JsonResponse
     */
    public function find(FinderRequest $request)
    {
        $contractor =  Contractor::query()->where('document', $request->get('document'))->firstOrFail();
        return $this->success_response(
            new ContractorResource($contractor)
        );
    }

    /**
     * @param Contractor $contractor
     * @param $name
     * @return BinaryFileResponse
     */
    public function rut(Contractor $contractor, $name)
    {
        if ($contractor->getOriginal('rut') == $name) {
            if (Storage::disk('contractor')->exists($name)) {
                return response()->file(storage_path("app/contractor/{$name}"));
            }
        }
        abort(Response::HTTP_NOT_FOUND, __('validation.handler.resource_not_found_url'));
    }

    /**
     * @param Contractor $contractor
     * @param $name
     * @return BinaryFileResponse
     */
    public function bank(Contractor $contractor, $name)
    {
        if ($contractor->getOriginal('bank') == $name) {
            if (Storage::disk('contractor')->exists($name)) {
                return response()->file(storage_path("app/contractor/{$name}"));
            }
        }
        abort(Response::HTTP_NOT_FOUND, __('validation.handler.resource_not_found_url'));
    }

    /**
     * @param Contractor $contractor
     * @param $name
     * @return BinaryFileResponse
     */
    public function photo(Contractor $contractor, $name)
    {
        if ($contractor->getOriginal('photo') == $name) {
            if (Storage::disk('contractor')->exists($name)) {
                return response()->file(storage_path("app/contractor/{$name}"));
            }
        }
        abort(Response::HTTP_NOT_FOUND, __('validation.handler.resource_not_found_url'));
    }

    /**
     * @param UpdateThirdPartyRequest $request
     * @param Contractor $contractor
     * @return JsonResponse
     */
    public function thirdParty(UpdateThirdPartyRequest $request, Contractor $contractor)
    {
        try {
            $contractor->third_party = $request->get('third_party');
            $contractor->saveOrFail();
            return $this->success_message(__('validation.handler.updated'));
        } catch (Throwable $e) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }

    public function approvedToPayment(Request $request)
    {
        $compliance = Compliance::query()->find($request->get('complianceId'));
        $activityReport = ActivityReport::query()->find($request->get('activityReportId'));
        if (!$compliance || !$activityReport) {
            return $this->error_response("No se encontraron archivos para notificar al contratista");
        }

        $contractor = $compliance->contractor;
        $document = $contractor->document;
        $response = app(ComplianceController::class)->getPdf($compliance);
        $jsonString = $response->getContent();
        $date = now();
        $file_name = "CERTIFICADO_CUMPLIMIENTO_TRIBUTARIO_{$document}_{$date->format('YmdHis')}.pdf";
        $parts = explode(',', $jsonString);
        $base64Content = $parts[1];
        $pdfBinary = base64_decode($base64Content);
        Storage::disk('local')->put("contractor/{$file_name}", $pdfBinary);

        $responseActivityReport = app(ActivityReportController::class)->createPDF($activityReport);
        $jsonStringActivity = $responseActivityReport->getContent();
        $file_name_activity = "INFORME_DE_ACTIVIDADES_{$document}_{$date->format('YmdHis')}.pdf";
        $partsActivity = explode(',', $jsonStringActivity);
        $base64ContentActivity = $partsActivity[2];
        $pdfBinaryActivity = base64_decode($base64ContentActivity);
        Storage::disk('local')->put("contractor/{$file_name_activity}", $pdfBinaryActivity);

        $filed_number = null;
        $report_date = $activityReport['report_date'];
        list($year, $month) = explode('-', $report_date);
        $plan = PlanPayment::query()
            ->where('year', $year)
            ->where('month', $month)
            ->where('contract_id', $request->get('contractId'))
            ->first();

        if ($plan && !is_null($plan->filed_number)) {
            $filed_number = $plan->filed_number;
        } elseif ($plan) {
            $contract = $activityReport['contract_number'];
            $contract_number = substr($contract, strrpos($contract, '-') - 4);
            $plan_number = str_pad($plan->plan_number, 4, '0', STR_PAD_LEFT);
            $filed_number = $contract_number . '-' . $plan_number;
        }

        $this->dispatch(new SendDocumentsContractor($contractor, $file_name, $file_name_activity, $activityReport, $filed_number));
        return $this->success_message(
            __('validation.handler.success'),
            Response::HTTP_OK,
            Response::HTTP_OK
        );

    }

    public function getStatusPayment($document, $date){
        try {
            $date_parts = explode('-', $date);
            $compliance = Contractor::with(['compliances' => function ($query) use ($date_parts) {
                $query->where('period_year', intval($date_parts[0]))
                    ->where('period_month', intval($date_parts[1]));
            }])
                ->where('document', $document)
                ->first();
            if ($compliance) {
                return $compliance->compliances;
            }
            return [];
        } catch (\Throwable $e) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }
}
