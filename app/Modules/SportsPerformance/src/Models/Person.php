<?php

namespace App\Modules\SportsPerformance\src\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\Security\DocumentType;


class Person extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_sim';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'persona';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id_Persona';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['Id_Persona' , 'Id_Genero' , 'Id_Identidad_Genero', 'Primer_Apellido' , 'Segundo_Apellido' , 'Primer_Nombre' , 'Segundo_Nombre' , 'Fecha_Nacimiento' , 'Id_TipoDocumento' , 'Cedula'];

    public function patient()
    {
        return $this->hasOne(Patient::class, 'persona_id', 'Id_Persona');
    }
    
    public function document_type()
    {
        return $this->hasOne(DocumentType::class, 'Id_TipoDocumento', 'Id_TipoDocumento');
    }

    public $timestamps = false;
}
