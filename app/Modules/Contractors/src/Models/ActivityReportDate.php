<?php

namespace App\Modules\Contractors\src\Models;

use App\Models\Security\Afp;
use App\Models\Security\CityLDAP;
use App\Models\Security\CountryLDAP;
use App\Models\Security\Disability;
use App\Models\Security\DocumentType;
use App\Models\Security\Eps;
use App\Models\Security\EthnicGroup;
use App\Models\Security\GenderIdentity;
use App\Models\Security\PopulationGroup;
use App\Models\Security\Sex;
use App\Models\Security\SexualOrientation;
use App\Models\Security\StateLDAP;
use App\Models\Security\User;
use App\Modules\Parks\src\Models\Location;
use App\Modules\Parks\src\Models\Neighborhood;
use App\Modules\Parks\src\Models\Upz;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use OwenIt\Auditing\Contracts\Auditable;

class ActivityReportDate extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable, Notifiable;
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'mysql_contractors';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'activity_reports_dates';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'code',
        'start_date',
        'final_date'
    ];

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [
        'code',
        'start_date',
        'final_date'
    ];
}
