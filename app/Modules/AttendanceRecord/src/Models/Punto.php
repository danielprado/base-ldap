<?php

namespace App\Modules\AttendanceRecord\src\Models;

use Illuminate\Database\Eloquent\Model;

class Punto extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_recreovia';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Puntos';
    protected $primaryKey = 'Id_Punto';

    public function cronogramas()
    {
        return $this->hasMany(Cronograma::class, 'Id_Punto');
    }

    public function localidad()
    {
        return $this->belongsTo(Localidad::class, 'Id_Localidad');
    }
}
