<?php
namespace App\Modules\InfoApp\src\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modules\InfoApp\src\Models\Event;

class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::all();
        return response()->json($events);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = new Event();
        $event->fill($request->only([
            'nombre',
            'nombre2',
            'lugar',
            'resumen',
            'descripcion',
            'link',
            'fecha_fin',
            'imagen',
            'fecha',
            'hora',
            'categoria_id',
            'tipo',
            'destacado',
            'tipo_poblacion',
            'entrada',
            'localidad',
            'latitud',
            'longitud'
        ]));
        $event->save();
        return response()->json($event, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::find($id);
        if (!$event) {
            return response()->json(['error' => 'Evento no encontrado'], 404);
        }
        return response()->json($event);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $event = Event::find($id);
        if (!$event) {
            return response()->json(['error' => 'Evento no encontrado'], 404);
        }
        $event->fill($request->only([
            'nombre',
            'nombre2',
            'lugar',
            'resumen',
            'descripcion',
            'link',
            'fecha_fin',
            'imagen',
            'fecha',
            'hora',
            'categoria_id',
            'tipo',
            'destacado',
            'tipo_poblacion',
            'entrada',
            'localidad',
            'latitud',
            'longitud'
        ]));
        $event->save();
        return response()->json($event);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $event = Event::find($id);
        if (!$event) {
            return response()->json(['error' => 'Evento no encontrado'], 404);
        }
        $event->delete();
        return response()->json(['message' => 'Evento borrado correctamente']);
    }

    /**
     * Display a listing of the resource with a date range filter.
     *
     * @param  int  $number_days
     * @return \Illuminate\Http\Response
     */
    public function indexWithDateRange($number_days)
    {
        $start_date = now()->subDays($number_days);
        $end_date = now()->addDays($number_days);

        $events = Event::whereBetween('fecha', [$start_date, $end_date])->get();

        return response()->json($events);
    }
}