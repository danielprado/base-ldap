<?php

namespace App\Modules\Certifications\src\Request;

use Illuminate\Foundation\Http\FormRequest;

class UpdateObligationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number'        => 'required|numeric',
            'object'        => 'required|string'
        ];
    }

    public function attributes()
    {
        return [
            'number'        =>  'Número de la obligación',
            'object'        =>  'Objeto de la obligación',
        ];
    }
}
