<?php

namespace App\Modules\AttendanceRecord\src\Exports;

use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class AssistsExport implements FromCollection, WithHeadings, WithMapping, ShouldAutoSize
{
      use Exportable;

      protected $assists;

      public function __construct($assists = null)
      {
            $this->assists = $assists;
      }

      /**
       * @return \Illuminate\Support\Collection
       */
      public function collection()
      {
            return $this->assists;
      }

      public function map($assists): array
      {
            return [
                $assists->attendee->number_document,
                $assists->attendee->type_document,
                mb_strtoupper($assists->attendee->complete_names, 'UTF-8'),
                $assists->attendee->date_birth,
                $assists->attendee->sex,
                $assists->attendee->sexual_orientation,
                $assists->attendee->gender_identity,
                $assists->attendee->phone,
                $assists->attendee->country_birth ? $assists->attendee->country_birth->name : '',
                $assists->attendee->civil_status,
                $assists->attendee->stratum,
                strtoupper($assists->attendee->locality ? $assists->attendee->locality->Localidad: ''),
                $assists->attendee->mail,
                $assists->attendee->Adress,
                $assists->attendee->has_computed,
                $assists->attendee->has_conectivity,
                $assists->attendee->type_socialsecurity,
                $assists->attendee->group_etnic,
                $assists->attendee->study_level,
                $assists->attendee->occupation,
                count($assists->attendee->disabilities) > 0 ? $this->getdata($assists->attendee->disabilities)  : '',
                count($assists->attendee->populations) > 0 ? $this->getdata($assists->attendee->populations) : '',
                count($assists->attendee->paidcares) > 0 ? $this->getdata($assists->attendee->paidcares) : '',
                count($assists->attendee->unpaidcares) > 0 ? $this->getdata($assists->attendee->unpaidcares) : '',
                json_decode($assists->data)->type_population,
                json_decode($assists->data)->type_service,
                json_decode($assists->data)->service_offered,
                json_decode($assists->data)->entity,
                isset(json_decode($assists->data)->type_assist) ? json_decode($assists->data)->type_assist: '',
                $assists->date_assist,
                json_decode($assists->data)->hour_init,
                json_decode($assists->data)->hour_end,
                json_decode($assists->data)->locality_service,
                json_decode($assists->data)->type_location_service,
                json_decode($assists->data)->form_operation,
                json_decode($assists->data)->apple,
                json_decode($assists->data)->name_equipment,
                json_decode($assists->data)->attention_modality,
                json_decode($assists->data)->hour_job,
                json_decode($assists->data)->has_attendant,
                json_decode($assists->data)->name_attendant,
                json_decode($assists->data)->type_document_attendant,
                json_decode($assists->data)->number_document_attendant,
                json_decode($assists->data)->sex_attendant,
                json_decode($assists->data)->phone_attendant,
                json_decode($assists->data)->observations,
                $assists->module->name
            ];
      }

      public function headings(): array
      {
            return ['NUMERO DE IDENTIFICACION', 'TIPO DE DOCUMENTO', 'NOMBRES','FECHA NACIMIENTO','SEXO','ORIENTACION SEXUAL','IDENTIDAD DE GENERO','TELEFONO','PAIS DE NACIMIENTO',
                    'ESTADO CIVIL','ESTRATO','LOCALIDAD','CORREO','DIRECCION','TIENE COMPUTADOR','TIENE CONECTIVIDAD','TIPO SEGURIDAD SOCIAL','GRUPO ETNICO','NIVEL DE ESTUDIOS','OCUPACION',
                    'DISCAPACIDADES(,)','TIPOS DE POBLACION(,)','CUIDADO DIRECTO NO REMUNERADO(,)','CUIDADO INDIRECTO NO REMUNERADO(,)','POBLACION OBJETIVO','TIPO SERVICIO OFRECIDO','SERVICIO PRESTADO',
                    'ENTIDAD','TIPO DE ASISTENCIA','FECHA DE ASISTENCIA','HORA INICIO','HORA FIN','LOCALIDAD DE SERVICIO','UBICACION DEL SERVICIO','FORMA DE OPERACION','MANZANA','NOMBRE EQUIPAMIENTO',
                    'MODALIDAD DE ATENCION','HORAS DE TRABAJO','TIENE ACUDIENTE','NOMBRE ACUDIENTE','TIPO DOCUMENTO ACUDIENTE','NUMERO DOCUMENTO ACUDIENTE','SEXO ACUDIENTE','TELEFONO ACUDIENTE','OBSERVACIONES', 'MODULO ASISTENCIA'];
      }

      private function getdata($array)
      {
            $data_concat = '';
            foreach ($array as $item) {
                  $data_concat .= $item->name . ',';
            }
            return $data_concat;
      }
}
