<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplianceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compliance', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('contractor_id')
                ->comment('Id del contratista');

            $table->unsignedBigInteger('contract_id')
                ->comment('Id del contrato');

            $table->boolean('tax_sales')
                ->default(false)
                ->comment('Impuesto a la venta');

            $table->boolean('simple_regime')
                ->default(false)
                ->comment('Pertenece a régimen simple');

            $table->boolean('resident_foreigners')
                ->default(false)
                ->comment('Residente extranjero');

            $table->boolean('total_annual_income')
                ->default(false)
                ->comment('Ingreso anual superior a UVT');

            $table->boolean('has_children')
                ->default(false)
                ->comment('Tiene hijos');

            $table->unsignedInteger('children_number')
                ->default(0)
                ->nullable();

            $table->boolean('children_minor')
                ->default(false)
                ->comment('Tiene hijos menores');

            $table->boolean('children_mayor_students')
                ->default(false)
                ->comment('Tiene hijos mayores estudiantes');

            $table->boolean('children_disabilities')
                ->default(false)
                ->comment('Tiene hijos con discapacidades');

            $table->boolean('spouse')
                ->default(false)
                ->comment('Tiene cónyuge dependiente');

            $table->boolean('parents_dependency')
                ->default(false)
                ->comment('Tiene padres dependendientes');

            $table->boolean('housing_interests')
                ->default(false)
                ->comment('Intereses por vivienda');

            $table->boolean('prepaid_medicine')
                ->default(false)
                ->comment('Medicina Prepagada');

            $table->boolean('income_tax_filer')
                ->default(false)
                ->comment('Impuesto de ingresos');

            $table->unsignedInteger('withholding_value')
                ->default(null)
                ->nullable();

            $table->string('payroll_number', 20)
                ->default(null)
                ->nullable();

            $table->date('payment_date')
                ->default(null)
                ->nullable();

            $table->boolean('pensioner')
                ->default(false)
                ->comment('Es pensionado');

            $table->unsignedInteger('payment_value')
                ->default(null)
                ->nullable();

            $table->unsignedInteger('eps_value')
                ->default(null)
                ->nullable();

            $table->unsignedInteger('afp_value')
                ->default(null)
                ->nullable();

            $table->unsignedInteger('arl_value')
                ->default(null)
                ->nullable();

            $table->unsignedInteger('afc_value')
                ->default(null)
                ->nullable();

            $table->unsignedInteger('afp_voluntary_value')
                ->default(null)
                ->nullable();

            $table->enum('status', ['PENDING', 'APPROVED', 'REJECTED'])
                ->default('PENDING');

            $table->foreign('contractor_id')
                ->references('id')
                ->on('contractors')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');

            $table->foreign('contract_id')
                ->references('id')
                ->on('contracts')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compliance');
    }
}
