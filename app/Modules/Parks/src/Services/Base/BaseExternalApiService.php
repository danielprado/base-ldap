<?php

namespace App\Modules\Parks\src\Services\Base;

use GuzzleHttp\Client;

abstract class BaseExternalApiService
{
    public function get($route, $params = [])
    {
        $http = new Client();
        $params['f'] = 'json';
        $arcgis_api = env('URL_BASE_ARCGIS');
        $url = $this->buildUrlWithParams($arcgis_api . $route, $params);
        $response = $http->get($url);
        $data = json_decode($response->getBody(), true);
        return $data;
    }

    private function buildUrlWithParams($baseUrl, $params = [])
    {
        if (!empty($params)) {
            $queryString = http_build_query($params);
            $url = $baseUrl . '?' . $queryString;
        } else {
            $url = $baseUrl;
        }
        return $url;
    }
}
