<?php

namespace App\Modules\SportsPerformance\src\Request;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PatientRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('api')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'document_type_code'  => 'required|string|max:255',
            'document'     => 'required|string',
            'sim_person_id'       => 'required|string',
            'patient'         => 'required|array',
            'patient.first_name'              => 'nullable|string|max:255',
            'patient.middle_name'             => 'nullable|string|max:255',
            'patient.last_name'               => 'nullable|string|max:255',
            'patient.second_last_name'        => 'nullable|string|max:255',
            'patient.sex'                     => 'nullable|string',
            'patient.gender_identity'         => 'nullable|string',
            'patient.sexual_orientation'      => 'nullable|string',
            'patient.sector'                  => 'nullable|string|max:255',
            'patient.sport'                   => 'nullable|string|max:255',
            'patient.modality'                => 'nullable|string|max:255',
            'patient.limitation_type'         => 'nullable|string|max:255',
            'patient.birth_date'              => 'nullable|date',
            'patient.birth_municipality'      => 'nullable|string|max:255',
            'patient.birth_department'        => 'nullable|string|max:255',
            'patient.birth_country'           => 'nullable|string|max:255',
            'patient.document_type'           => 'nullable|string',
            'patient.document'                => 'nullable|numeric',
            'patient.expedition_municipality' => 'nullable|string|max:255',
            'patient.expedition_department'   => 'nullable|string|max:255',
            'patient.address'                 => 'nullable|string|max:255',
            'patient.neighborhood'            => 'nullable|string|max:255',
            'patient.locality'                => 'nullable|string|max:255',
            'patient.landline'                => 'nullable|string',
            'patient.mobile'                  => 'nullable|string',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'document_type_code'  => 'código del tipo documento',
            'document'     => 'número documento',
            'sim_person_id'       => 'ID Persona BD SIM',
            'patient.first_name'              => 'primer nombre',
            'patient.middle_name'             => 'segundo nombre',
            'patient.last_name'               => 'apellido',
            'patient.second_last_name'        => 'segundo apellido',
            'patient.sex'                     => 'sexo',
            'patient.gender_identity'         => 'identidad de género',
            'patient.sexual_orientation'      => 'orientación sexual',
            'patient.sector'                  => 'sector',
            'patient.sport'                   => 'deporte',
            'patient.modality'                => 'modalidad',
            'patient.limitation_type'         => 'tipo limitación',
            'patient.birth_date'              => 'fecha de nacimiento',
            'patient.birth_municipality'      => 'municipio de nacimiento',
            'patient.birth_department'        => 'departamento de nacimiento',
            'patient.birth_country'           => 'país de nacimiento',
            'patient.document_type'           => 'tipo de documento',
            'patient.document'                => 'número de documento',
            'patient.expedition_municipality' => 'municipio de expedición',
            'patient.expedition_department'   => 'departamento de expedición',
            'patient.address'                 => 'dirección',
            'patient.neighborhood'            => 'barrio',
            'patient.locality'                => 'localidad',
            'patient.landline'                => 'teléfono fijo',
            'patient.mobile'                  => 'celular',
        ];
    }
}
