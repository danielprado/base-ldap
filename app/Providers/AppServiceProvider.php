<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use App\Modules\Parks\src\Services\ArcGisExternalApiService;
use App\Modules\Parks\src\Services\ParkExternalApiService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('arcgis-service-api', ArcGisExternalApiService::class);
        $this->app->bind('park-service-api', ParkExternalApiService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        $_SERVER["SERVER_NAME"] = "sim.idrd.gov.co";
    }
}
