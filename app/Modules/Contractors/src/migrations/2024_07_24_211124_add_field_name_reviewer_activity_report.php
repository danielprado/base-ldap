<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldNameReviewerActivityReport extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_contractors')->table('activity_reports', function (Blueprint $table) {
            $table->text('name_reviewer')->nullable()->before('reviewed');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_contractors')->table('activity_reports', function (Blueprint $table) {
            $table->dropColumn('name_reviewer');

        });
    }
}
