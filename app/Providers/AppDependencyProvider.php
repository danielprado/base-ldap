<?php

namespace App\Providers;

use App\Modules\Contractors\src\Helpers\clients\OrfeoClient;
use App\Modules\Contractors\src\Helpers\SubdirectorateHelper;
use Illuminate\Support\ServiceProvider;
use GuzzleHttp\Client;
use Illuminate\Foundation\Application;

class AppDependencyProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(OrfeoClient::class, function(Application $app) {
          return new OrfeoClient($app->get(Client::class));
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    }
}
