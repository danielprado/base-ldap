<?php

namespace App\Modules\Contractors\src\Constants;

use App\Modules\Contractors\src\Models\Compliance;
use App\Modules\Contractors\src\Helpers\compliance\ComplianceContribution;

class Contributions {
    const HAS_CHILDREN = 'has_children';
    const CHILDREN_MINOR = 'children_minor';
    const CHILDREN_MAYOR_STUDENTS = 'children_mayor_students';
    const CHILDREN_DISABILITIES = 'children_disabilities';
    const SPOUSE = 'spouse';
    const PARENTS_DEPENDENCY = 'parents_dependency';
    const INCOME_TAX_FILER = 'income_tax_filer';
    const HOUSING_INTERESTS = 'housing_interests';
    const PREPAID_MEDICINE = 'prepaid_medicine';
    const PENSIONER = 'pensioner';
    const AFP_VOLUNTARY = 'afp_voluntary';
    const AFC_VOLUNTARY = 'afc_voluntary';
    const AFC_VALUE = 'afc_value';
    const AFP_VOLUNTARY_VALUE = 'afp_voluntary_value';
    const SPOUSE_TAX = 'spouse_tax';
    const THIRD_PEOPLE = 'third_people';
    const RESPONSIBLE_IVA = 'responsible_iva';
    const RETENTION = 'retention';
    const WITHHOLDING = 'withholding';
    const PERCENTAGE = 'percentage';
    const WITHHOLDING_VALUE = 'withholding_value';
    const DEPENDENT_VALUE = 'dependent_value';

    public static function from(Compliance $compliance) {
      return [
        self::HAS_CHILDREN            => ComplianceContribution::from($compliance)(self::HAS_CHILDREN),
        self::CHILDREN_MINOR          => ComplianceContribution::from($compliance)(self::CHILDREN_MINOR),
        self::CHILDREN_MAYOR_STUDENTS => ComplianceContribution::from($compliance)(self::CHILDREN_MAYOR_STUDENTS),
        self::CHILDREN_DISABILITIES   => ComplianceContribution::from($compliance)(self::CHILDREN_DISABILITIES),
        self::SPOUSE                  => ComplianceContribution::from($compliance)(self::SPOUSE),
        self::PARENTS_DEPENDENCY      => ComplianceContribution::from($compliance)(self::PARENTS_DEPENDENCY),
        self::INCOME_TAX_FILER        => ComplianceContribution::from($compliance)(self::INCOME_TAX_FILER),
        self::AFC_VALUE               => ComplianceContribution::from($compliance)(self::AFC_VALUE),
        self::AFP_VOLUNTARY_VALUE     => ComplianceContribution::from($compliance)(self::AFP_VOLUNTARY_VALUE),
        self::HOUSING_INTERESTS       => ComplianceContribution::from($compliance)(self::HOUSING_INTERESTS),
        self::PREPAID_MEDICINE        => ComplianceContribution::from($compliance)(self::PREPAID_MEDICINE),
        self::PENSIONER               => ComplianceContribution::from($compliance)(self::PENSIONER),
        self::AFP_VOLUNTARY           => ComplianceContribution::from($compliance)(self::AFP_VOLUNTARY),
        self::SPOUSE_TAX              => ComplianceContribution::from($compliance)(self::SPOUSE_TAX),
        self::THIRD_PEOPLE            => ComplianceContribution::from($compliance)(self::THIRD_PEOPLE),
        self::RESPONSIBLE_IVA         => ComplianceContribution::from($compliance)(self::RESPONSIBLE_IVA),
        self::RETENTION               => ComplianceContribution::from($compliance)(self::RETENTION),
        self::WITHHOLDING             => ComplianceContribution::from($compliance)(self::WITHHOLDING),
        self::PERCENTAGE              => ComplianceContribution::from($compliance)(self::PERCENTAGE),
        self::AFC_VOLUNTARY           => ComplianceContribution::from($compliance)(self::AFC_VOLUNTARY),
        self::WITHHOLDING_VALUE       => ComplianceContribution::from($compliance)(self::WITHHOLDING_VALUE),
        self::DEPENDENT_VALUE         => ComplianceContribution::from($compliance)(self::DEPENDENT_VALUE),
      ];
    }
}
