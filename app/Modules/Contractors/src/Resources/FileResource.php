<?php


namespace App\Modules\Contractors\src\Resources;


use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Storage;

class FileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $coverage = $this->coverage_final_date ?? null;
        $contract = $this->contract->final_date ?? null;
        $status = null;
        $covered = 0;
        if ($coverage && $contract) {
            $covered = $coverage->diffInDays($contract, $absolute = false);
            $status = $coverage->lessThan($contract)
                ? "La cobertura de ARL es menor a la fecha de terminación del contrato por ({$coverage->diffInDays($contract)}) día(s)."
                : "La cobertura de ARL es mayor a la fecha de terminación del contrato por ({$coverage->diffInDays($contract)}) día(s).";
        }
        return [
            'id'           =>  isset($this->id) ? (int) $this->id : null,
            'name'         => $this->name ?? null,
            'path'         =>  $this->checkFile(),
            'file_type_id' =>  isset($this->file_type_id) ? (int) $this->file_type_id : null,
            'file_type'    => $this->file_type->name ?? null,
            'mime'         => $this->file_type->mimes ?? null,
            'contract_id'  =>  isset($this->contract_id) ? (int) $this->contract_id : null,
            'contract_final_date'   =>  $contract ? $contract->format('Y-m-d') : null,
            'covered'       => $covered,
            'status_coverage' => $status,
            'user_id'      =>  isset($this->user_id) ? (int) $this->user_id : null,
            'user'         => $this->user->full_name ?? null,
            'coverage_start_date'   =>  isset($this->coverage_start_date) ? $this->coverage_start_date->format('Y-m-d') : null,
            'coverage_final_date'   =>  $coverage ? $coverage->format('Y-m-d') : null,
            'created_at'   =>  isset($this->created_at) ? $this->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'   =>  isset($this->updated_at) ? $this->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }

    public function checkFile()
    {
        $id = isset($this->id) ? (int) $this->id : 0;
        $file = isset($this->name) ? $this->name : null;
        if ($file) {
            if (Storage::disk('local')->exists("arl/{$file}")) {
                return route('file.resource', ['file' => $id, 'name' => $file]);
            }
        }
        return null;
    }
}
