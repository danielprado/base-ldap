<?php

namespace App\Modules\Certifications\src\Request;

use Illuminate\Foundation\Http\FormRequest;

class AdditionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number'    => 'required|numeric',
            'value'     => 'required|numeric'
        ];
    }

    public function attributes()
    {
        return [
            'number'    => 'Número de la adición',
            'value'     => 'Valor de la adición',
        ];
    }
}
