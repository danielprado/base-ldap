<?php

namespace App\Modules\Certifications\src\Jobs;

use App\Modules\Certifications\src\Mail\SupportMail;
use App\Modules\Certifications\src\Models\ContractSupport;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use SendGrid\Mail\Mail;

class ReplySupport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var ContractSupport
     */
    private $support;

    private $file;

    /**
     * Create a new job instance.
     *
     * @param ContractSupport $support
     * @param $file
     */
    public function __construct(ContractSupport $support, $file)
    {
        $this->support = $support;
        $this->file = $file;
    }


    public function handle(Mail $mailer)
    {
        $email = env('APP_ENV') == 'local'
            ? env('SAMPLE_EMAIL', 'brayan.aldana@idrd.gov.co')
            : $this->support->Correo_Solicitante ??  null;

        $attachment = [];

        if ( isset( $email )  && filter_var( $email, FILTER_VALIDATE_EMAIL) ) {
            if ($this->file != null) {
                $file_name = "CERTIFICADO-CONTRACTUAL-".random_img_name().".pdf";
                $attachment = ['file' => $file_name];
                $mailer->addAttachment(
                    $this->file,
                    "application/pdf",
                    $file_name,
                    "attachment"
                );
            }
            $mailer->setFrom(
                env('MAIL_FROM_ADDRESS', 'mails@idrd.gov.co'),
                'CERTIFICACIÓN DE CONTRATOS'
            );
            $mailer->setSubject("SOLICITUD CERTIFICADO CONTRACTUAL");
            $mailer->addTo($email);
            $render = (new SupportMail($this->support))->render();
            $mailer->addContent("text/html", $render);
            $sendgrid = new \SendGrid(env('SENDGRID_API_KEY'));
            try {
                $response = $sendgrid->send($mailer);
                sendgird_logger(
                    $email,
                    "Solicitud de Certificado Contractual",
                    $render,
                    $attachment,
                    $response->statusCode(),
                    $response->body()
                );
            } catch (\Exception $e) {
                sendgird_logger(
                    $email,
                    "[EXCEPTION] Solicitud de Certificado Contractual",
                    $render,
                    $attachment,
                    $e->getCode(),
                    $e->getMessage()
                );
            }
        }
    }
}
