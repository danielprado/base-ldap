<?php


namespace App\Modules\Contractors\src\Mail;


use App\Modules\Contractors\src\Models\Contractor;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Crypt;

class ContractorAccountBankMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Contractor
     */
    private $mail;

    /**
     * Create a new job instance.
     *
     * @param Contractor $user
     */
    public function __construct(Contractor $user)
    {
        $this->mail = $user;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $id = isset( $this->mail->id ) ? $this->mail->id : '';
        $created_at = isset( $this->mail->created_at ) ? $this->mail->created_at->format('Y-m-d H:i:s') : '';

        $first = isset( $this->mail->name ) ? $this->mail->name : '';
        $second = isset( $this->mail->surname ) ? $this->mail->surname : '';
        $name = "$first $second";
        $bank = $this->mail->bank_account->bank->name ?? null;
        $account_type = $this->mail->bank_account->account_type->name ?? null;

        return $this->view('mail.mail')
            ->subject('Actualización de cuenta bancaria Portal Contratista')
            ->with([
                'slogan'    => 'Notificación de Actualización',
                'header'    => 'IDRD',
                'title'     => 'Actualización de datos Portal Contratista',
                'content'   =>  "¡{$name}! hemos actualizado sus datos de manera satisfactoria",
                'details'   =>  "
                        <p>Número de Registro: {$id}</p>
                        <p>Nombre: {$name}</p>
                        <p>Nombre del banco: {$bank}</p>
                        <p>Tipo de cuenta: {$account_type}</p>
                        <p>Fecha de Registro: {$created_at}</p>
                        ",
                'hide_btn'  => true,
                'remember'  =>  'Recuerde que la actualización de la cuenta bancaria dentro del plazo establecido se verá reflejado en el siguiente periodo de pago.',
                'year'      =>  Carbon::now()->year
            ]);
    }
}
