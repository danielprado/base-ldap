<?php

namespace App\Modules\Contractors\src\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ContractorBankResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => isset($this->id) ? (int) $this->id : null,
            'bank_id' => isset($this->bank_id) ? (int) $this->bank_id : null,
            'bank' => isset($this->bank->name) ? $this->bank->name : null,
            'account_type_id' => isset($this->account_type_id) ? (int) $this->account_type_id : null,
            'account_type' => isset($this->account_type->name) ? $this->account_type->name : null,
            'number' => isset($this->number) ? $this->number : null,
            'economic_activity_id' => isset($this->economic_activity_id) ? (int) $this->economic_activity_id : null,
            'economic_activity' => isset($this->economic_activity->name) ? $this->economic_activity->name : null,
        ];
    }
}
