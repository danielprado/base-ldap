<?php


namespace App\Modules\Contractors\src\Controllers;


use App\Http\Controllers\Controller;
use App\Models\Security\Subdirectorate;
use App\Models\Security\User;
use App\Modules\Contractors\src\Constants\PayrollGlobalQuery;
use App\Modules\Contractors\src\Exports\BinnacleExport;
use App\Modules\Contractors\src\Exports\InvoicingExport;
use App\Modules\Contractors\src\Exports\TreasuryExport;
use App\Modules\Contractors\src\Jobs\GenerateBinnacle;
use App\Modules\Contractors\src\Jobs\GenerateInvoicing;
use App\Modules\Contractors\src\Models\Payroll;
use App\Modules\Contractors\src\Models\PlanPayment;
use App\Modules\Contractors\src\Request\PayrollReportRequest;
use App\Modules\Contractors\src\Resources\PaymentPlanResource;
use App\Modules\Contractors\src\Resources\PayrollResource;
use App\Modules\Contractors\src\Resources\PayrollResourceSeven;
use GuzzleHttp\Client;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Bus;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel as ExcelFacade;
use App\Modules\Parks\src\Exports\Excel as ExcelRaw;
use Maatwebsite\Excel\Excel;

class PlanController extends Controller
{

    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function getApproved(Request $request): JsonResponse
    {
        $plans = PlanPayment::query()
            ->whereHas('contract', function ($query) {
                $query->where('subdirectorate_id', auth()->user()->subdirectorate_id);
            })
            ->where('year', $request->get('year'))
            ->where('month', $request->get('month'))
            ->where('status', '=',PlanPayment::STATUS_APPROVED)
            ->where('reserve', $request->get('reserve'))
            ->where('cycle_path_guardian', $request->get('cycle_path_guardian'))
            ->latest()
            ->get();

        return $this->success_response(
            PaymentPlanResource::collection($plans),
            Response::HTTP_OK,
            [
                'headers'   =>  PaymentPlanResource::headers(),
            ]
        );
    }

    public function getPlansCount(Request $request): JsonResponse
    {
        $statusPending = PlanPayment::STATUS_PENDING;
        $statusApproved = PlanPayment::STATUS_APPROVED;
        $statusBinnacle = PlanPayment::STATUS_BINNACLE;
        $statusInvoicing = PlanPayment::STATUS_INVOICING;
        $statusPayroll = PlanPayment::STATUS_PAYROLL;
        $statusPayment = PlanPayment::STATUS_PAYMENT;


        $counts = PlanPayment::query()
            ->whereHas('contract', function ($query) {
                $query->where('subdirectorate_id', auth()->user()->subdirectorate_id);
            })
            ->where('year', $request->get('year'))
            ->where('month', $request->get('month'))
            ->select(
                DB::raw("COUNT(CASE WHEN status = '$statusPending' THEN 1 END) as count_pending"),
                DB::raw("COUNT(CASE WHEN status = '$statusApproved' THEN 1 END) as count_approved"),
                DB::raw("COUNT(CASE WHEN status = '$statusBinnacle' THEN 1 END) as count_binnacle"),
                DB::raw("COUNT(CASE WHEN status = '$statusPayroll' THEN 1 END) as count_payroll"),
                DB::raw("COUNT(CASE WHEN status = '$statusInvoicing' THEN 1 END) as count_invoicing"),
                DB::raw("COUNT(CASE WHEN status = '$statusPayment' THEN 1 END) as count_payment")
            )
            ->first();

        return $this->success_message([
            'count_pending' => $counts->count_pending,
            'count_approved' => $counts->count_approved,
            'count_binnacle' => $counts->count_binnacle,
            'count_payroll' => $counts->count_payroll,
            'count_invoicing' => $counts->count_invoicing,
            'count_payment' => $counts->count_payment,
        ]);
    }

    public function getPlansStatus(Request $request): JsonResponse
    {
        $plans = PlanPayment::query()
            ->whereHas('contract', function ($query) {
                $query->where('subdirectorate_id', auth()->user()->subdirectorate_id);
            })
            ->where('year', $request->get('year'))
            ->where('month', $request->get('month'))
            ->where('status', '=', $request->get('status'))
            ->latest()
            ->get();

        return $this->success_response(
            PaymentPlanResource::collection($plans),
            Response::HTTP_OK,
            [
                'headers'   =>  PaymentPlanResource::headers(),
            ]
        );
    }

}
