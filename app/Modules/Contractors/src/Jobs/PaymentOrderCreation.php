<?php


namespace App\Modules\Contractors\src\Jobs;


use App\Models\Security\SendgridStatus;
use App\Modules\Contractors\src\Mail\AccountingCreationPayrollMail;
use App\Modules\Contractors\src\Mail\ContractorMail;
use App\Modules\Contractors\src\Mail\PayrollMail;
use App\Modules\Contractors\src\Models\Contractor;
use App\Modules\Contractors\src\Models\Payroll;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use SendGrid\Mail\Mail;

class PaymentOrderCreation implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Payroll
     */
    private $payroll;

    /**
     * Create a new job instance.
     *
     * @param array $payroll
     */
    public function __construct(array $payroll)
    {
        $this->payroll = $payroll;
    }

    /**
     * Execute the job.
     *
     * @param Mail $mailer
     * @return void
     */
    public function handle(Mail $mailer)
    {
        $email = config('app.env') == 'local'
            ? env('SAMPLE_EMAIL', 'brayan.aldana@idrd.gov.co')
            : env('ACCOUNTING_EMAIL_NOTIFICATION');

        if ( isset( $email )  && filter_var( $email, FILTER_VALIDATE_EMAIL) ) {
            // Crear una instancia del modelo Payroll
            $payroll = new Payroll([
                'id' => $this->payroll['id'],
                'subdirection_id' => $this->payroll['subdirectionId'],
                'subdirector_id' => $this->payroll['subdirectorId'],
                'year' => $this->payroll['year'],
                'month' => $this->payroll['month'],
                'verified' => false,
                'reserve' => $this->payroll['isReserve'],
                'cycle_path_guardian' => $this->payroll['isCyclePathGuardian'],
                'status' => $this->payroll['status'],
                'created_at' => $this->payroll['createdAt'],
                'updated_at' => $this->payroll['updatedAt'],
            ]);
            $mailer->setFrom(
                env('MAIL_CONTRACTS_FROM', 'portal.contratista@idrd.gov.co'),
                env('MAIL_CONTRACTS_FROM_NAME', 'Portal Contratista')
            );
            $mailer->setSubject("Creación Orden de pago planilla {$this->payroll['id']} Portal Contratista");
            $mailer->addTo($email);
            $render = ( new AccountingCreationPayrollMail( $payroll ) )->render();
            $mailer->addContent("text/html", $render);
            $sendgrid = new \SendGrid(config('mail.sendgrid.api'));
            try {
                $response = $sendgrid->send($mailer);
                sendgird_logger(
                    $email,
                    "Creación Orden de pago planilla {$this->payroll['id']} Portal Contratista",
                    $render,
                    [],
                    $response->statusCode(),
                    $response->body()
                );
            } catch (\Exception $e) {
                sendgird_logger(
                    $email,
                    "[EXCEPTION] Creación Orden de pago planilla {$this->payroll['id']} Portal Contratista",
                    $render,
                    [],
                    $e->getCode(),
                    $e->getMessage()
                );
            }
        }
    }
}
