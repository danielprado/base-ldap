<?php

namespace App\Modules\Contractors\src\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\Contractors\src\Constants\GlobalQuery;
use App\Modules\Contractors\src\Helpers\compliance\emails\Emails;
use App\Modules\Contractors\src\Jobs\ActivityReportCorrectedReminder;
use App\Modules\Contractors\src\Jobs\ActivityReportRejected;
use App\Modules\Contractors\src\Models\Contract;
use App\Modules\Contractors\src\Models\ActivityReport;
use App\Modules\Contractors\src\Models\ActivityReportDate;
use App\Modules\Contractors\src\Models\Compliance;
use App\Modules\Contractors\src\Models\Contractor;
use App\Modules\Contractors\src\Models\SurveyResponse;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Throwable;
use App\Modules\Contractors\src\Resources\ActivityReportResource;
use App\Modules\Contractors\src\Resources\ActivityReportDateResource;
use App\Modules\Contractors\src\Resources\ActivityReportResourceSupervisor;
use App\Helpers\FPDF;
use App\Modules\Contractors\src\Constants\Roles;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;
use Illuminate\Support\Facades\Log;
use NumberFormatter;
use Carbon\Carbon;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Illuminate\Http\Resources\Json\JsonResource;
use Dompdf\Dompdf;
use Dompdf\Options;
use App\Modules\Contractors\src\Models\PlanPayment;
use GuzzleHttp\Client;

class SurveyResponseController extends Controller
{
    public function store(Request $request): JsonResponse
    {

        $inputData = $request->input('data');
        $user = Auth::guard('api')->user();
        $contractor = Contractor::where('document', $user->document)->first();
         $mappedData = [
            'payment_changes' => $inputData['paymentChanges'] ?? null,
            'timely_payment' => ($inputData['timelyPayment'] === 'Sí') ? 1 : 0, // Convierte a booleano
            'tool_instructions' => $inputData['toolRatings']['instructions'] ?? null,
            'tool_video' => $inputData['toolRatings']['video'] ?? null,
            'tool_infographic' => $inputData['toolRatings']['infographic'] ?? null,
            'tool_support_desk' => $inputData['toolRatings']['supportDesk'] ?? null,
            'information_clarity' => $inputData['informationClarity'] ?? null,
            'payment_satisfaction' => $inputData['paymentSatisfaction'] ?? null,
            'recommendations' => $inputData['recommendations'] ?? null,
            'contractor_id' => ($contractor) ? $contractor->id : null
        ];
        $surveyResponse = SurveyResponse::create($mappedData);

        return response()->json([
            'message' => 'Encuesta guardada exitosamente.',
            'data' => $surveyResponse,
        ], 201);
    }


    public function validate_survey(Request $request): JsonResponse
    {
        try {
            // Obtener el usuario autenticado
            $user = Auth::guard('api')->user();
            $contractor = Contractor::query()->where('document', $user->document)->first();

            if (!$contractor) {
                return $this->error_response('Contractor not found', Response::HTTP_NOT_FOUND);
            }
            $survey = SurveyResponse::query()->where('contractor_id' , $contractor->id)->first();
            return response()->json([
                'message' => __('validation.handler.success'),
                'survey' => (!$survey) ? true : false ,
            ], Response::HTTP_OK);

        } catch (\Throwable $th) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $th->getMessage()
            );
        }
    }
}
