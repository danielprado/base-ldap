<?php

namespace App\Modules\Contractors\src\Models;

use Illuminate\Database\Eloquent\Model;

class ComplianceFileType extends Model
{
    protected $connection = 'mysql_contractors';

    protected $table = 'compliance_file_types';

    public $timestamps = false;

    protected $fillable = [
        'name',
        'support_field_name'
    ];

    public function scopeName($query, $name)
    {
        return $query
            ->where('support_field_name', $name)
            ->orWhere('name', $name);
    }
}
