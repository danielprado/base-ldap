<?php

namespace App\Modules\AttendanceRecord\src\Help;


class Helpers
{
      public function messagesuccess($message, $data, $code)
      {
            return response()->json([
                  'status' => 'OK',
                  'message' => $message,
                  'data' =>  $data,
                  'Error' => false,
            ], $code);
      }
      public function messageerror($message, $data, $code)
      {
            return response()->json([
                  'status' => 'Error',
                  'message' => $message,
                  'data' =>  $data,
                  'Error' => true,
            ], $code);
      }
}
