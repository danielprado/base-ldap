<?php

namespace App\Modules\AttendanceRecord\src\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\AttendanceRecord\src\Exports\AssistsExport;
use App\Modules\AttendanceRecord\src\Exports\AssistsExportActivity;
use App\Modules\AttendanceRecord\src\Imports\AssistImports;
use App\Modules\AttendanceRecord\src\Models\Assist;
use App\Modules\AttendanceRecord\src\Models\Session;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\IOFactory;

/**
 * @group Pasarela de pagos - Parques
 *
 * API para la gestión y consulta de datos de Parques Pse
 */
class ReportController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function excelsidicu($dateInit, $dateEnd)
    {

        $assists = Assist::with('attendee.disabilities', 'attendee.paidcares', 'attendee.populations', 'attendee.unpaidcares', 'attendee.country_birth', 'attendee.locality', 'module')
            ->where('date_assist', '>=', $dateInit)
            ->where('date_assist', '<=', $dateEnd)
            ->get();
        return Excel::download(new AssistsExport($assists), 'Reporte_Asistencia.xlsx');
    }

    public function excelActividadFisica($dateInit, $dateEnd)
    {

        $assists = Assist::with('attendee.disabilities', 'attendee.paidcares', 'attendee.populations', 'attendee.unpaidcares', 'attendee.country_birth', 'attendee.locality', 'module')
            ->where('date_assist', '>=', $dateInit)
            ->where('date_assist', '<=', $dateEnd)
            ->where('module_id', '=', 2)
            ->get();

        return Excel::download(new AssistsExportActivity($assists), 'Reporte_Asistencia_Actividad_Fisica.xlsx');
    }

    public function massiveInsertion(Request $request)
    {
        $file = $request->file('file');
        $spreadsheet = IOFactory::load($file);

        $worksheet = $spreadsheet->getActiveSheet();

        foreach ($worksheet->getRowIterator() as $row) {

            $sessionCelda = $worksheet->getCell('D' . $row->getRowIndex());
            $sessionId = $sessionCelda->getValue();

            $populationCelda = $worksheet->getCell('E' . $row->getRowIndex());
            $populationVal = $populationCelda->getValue();

            $typeServiceCelda = $worksheet->getCell('F' . $row->getRowIndex());
            $serviceVal = $typeServiceCelda->getValue();

            $serviceOfferedCelda = $worksheet->getCell('G' . $row->getRowIndex());
            $serviceOfferedVal = $serviceOfferedCelda->getValue();

            $entityCelda = $worksheet->getCell('H' . $row->getRowIndex());
            $entityVal = $entityCelda->getValue();

            $dateCelda = $worksheet->getCell('C' . $row->getRowIndex());
            $dateVal = $dateCelda->getValue();

            $attendeeCelda = $worksheet->getCell('A' . $row->getRowIndex());
            $attendeeVal = $attendeeCelda->getValue();

            $moduleCelda = $worksheet->getCell('B' . $row->getRowIndex());
            $moduleVal = $moduleCelda->getValue();

            $typeAssistCelda = $worksheet->getCell('I' . $row->getRowIndex());
            $typeAssistVal = $typeAssistCelda->getValue();

            $session = Session::with('recreopersonas', 'recreopersonas.persona', 'cronograma.punto', 'Cronograma.jornada', 'Cronograma.punto.localidad')
                ->where('id', '=', $sessionId)
                ->first();
            $data = [
                'session_id' => strval($sessionId),
                'objetive_session' => $session->Objetivo_General,
                'scenary_session' => $session->Cronograma->punto->Escenario,
                'workingday_session' => $session->Cronograma->jornada->Nombre,
                'documentTeacher' => $session->recreopersonas->persona->Cedula,
                'nameTeacher' => $session->recreopersonas->persona->Primer_Nombre . ' ' . $session->recreopersonas->persona->Primer_Apellido,
                'hour_init' => $session->Inicio,
                'hour_end' => $session->Fin,
                'locality_service' => $session->Cronograma->punto->localidad->Localidad,
                'attention_modality' => $session->Cronograma->Virtual == 'No' || $session->Cronograma->Virtual == null ? 'PRESENCIAL' : 'VIRTUAL',
                'type_population' => $populationVal,
                'type_service' => $serviceVal,
                'service_offered' => $serviceOfferedVal,
                'entity' => $entityVal,
                'type_assist' => isset($typeAssistVal) ? $typeAssistVal : '',
                "type_location_service" => "URBANA",
                "form_operation" => "PUNTO ANCLA",
                "apple" => "",
                "name_equipment" => "",
                "hour_job" => "CERO/NINGUNA",
                "has_attendant" => "NO",
                "name_attendant" => "",
                "type_document_attendant" => "",
                "number_document_attendant" => "",
                "sex_attendant" => "",
                "phone_attendant" => "",
                "email_attendant" => "",
                "relationship" => "",
                "group_etnic_attendant" => "NINGUNO",
                "observations" => "",
            ];

            $sessionJson = json_encode($data);
            $excelDate = $dateVal;
            $unixTimestamp = ($excelDate - 25569) * 86400; // Convierte a UNIX timestamp
            $dateAssist = Carbon::createFromTimestamp($unixTimestamp + 1 * 60);
            $dateAssistFormatted = $dateAssist->format('Y-m-d');

            $assist = new Assist([
                'attendee_id' => $attendeeVal,
                'module_id' => $moduleVal,
                'date_assist' => $dateAssistFormatted,
                'session_id' => $sessionId,
                'data' => $sessionJson,
            ]);
            $assist->save();
        }
        return 'ok';
    }
}
