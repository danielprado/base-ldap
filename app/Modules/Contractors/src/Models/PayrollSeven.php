<?php

namespace App\Modules\Contractors\src\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class PayrollSeven extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'oracle';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'CT_VPAGO';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'TER_CODI';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'TER_CODI',
        'TER_NOCO',
        'MPR_DESC',
        'CON_FINI',
        'CON_FFIN',
        'CON_PLAZ',
        'MPR_NDOS',
        'MPR_NUME',
        'RUB_CODI',
        'ARB_CARE',
        'ARB_CPRO',
        'ARB_CECO',
        'ARB_REF1',
        'DMP_SALD',
        'CON_VLRI',
        'CON_VALT',
        'TIP_CUEN',
        'CUE_NUME',
        'BAN_CODI',
        'BAN_NOMB',
    ];
    //Add extra attribute
    protected $attributes = ['id_aux'];

    //Make it available in the json response
    protected $appends = ['id_aux'];

    //implement the attribute
    public function getIdAuxAttribute()
    {
        return Str::random(9);
    }
}
