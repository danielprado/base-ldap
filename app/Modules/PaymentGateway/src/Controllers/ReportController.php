<?php

namespace App\Modules\PaymentGateway\src\Controllers;

use Adldap\Laravel\Facades\Adldap;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Modules\PaymentGateway\src\Exports\PaymentzExport;
use App\Modules\PaymentGateway\src\Help\Helpers;
use App\Modules\PaymentGateway\src\Models\OtpAdminPark;
use App\Modules\PaymentGateway\src\Resources\ReportPaymentResource;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Modules\PaymentGateway\src\Models\Pago;

/**
 * @group Pasarela de pagos - Parques
 *
 * API para la gestión y consulta de datos de Parques Pse
 */
class ReportController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct(\SendGrid\Mail\Mail $mailer)
    {
        parent::__construct();
    }

    /**
     * @group Pasarela de pagos - Parques
     *
     * Reportes
     *
     * Muestra un listado de los pagos efectuados efectivos.
     *
     *
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $di = Carbon::createFromFormat('Y-m-d', $request->dateInit)->startOfDay();
        $de = Carbon::createFromFormat('Y-m-d', $request->dateEnd)->endOfDay();
        $paymentz = DB::connection('mysql_pse')->table('pago_pse')
            ->leftJoin('parque', 'pago_pse.parque_id', '=', 'parque.id_parque')
            ->leftJoin('servicio', 'pago_pse.servicio_id', '=', 'servicio.id_servicio')
            ->leftJoin('medio_pago', 'pago_pse.medio_id', '=', 'medio_pago.id')
            ->where('estado_id', 2)
            ->whereBetween('pago_pse.created_at', [$di, $de])
            ->select('pago_pse.*', 'servicio.id_servicio', 'servicio.servicio_nombre', 'servicio.codigo_servicio', 'parque.nombre_parque', 'parque.codigo_parque', 'medio_pago.Nombre as medio_pago')
            ->get();
        return $this->success_response(ReportPaymentResource::collection($paymentz));
    }

    public function excel($dateInit, $dateEnd)
    {
        $di = Carbon::createFromFormat('Y-m-d', $dateInit)->startOfDay();
        $de = Carbon::createFromFormat('Y-m-d', $dateEnd)->endOfDay();
        $paymentz = DB::connection('mysql_pse')->table('pago_pse')
            ->leftJoin('parque', 'pago_pse.parque_id', '=', 'parque.id_parque')
            ->leftJoin('servicio', 'pago_pse.servicio_id', '=', 'servicio.id_servicio')
            ->leftJoin('medio_pago', 'pago_pse.medio_id', '=', 'medio_pago.id')
            ->where('estado_id', 2)
            ->whereBetween('pago_pse.created_at', [$di, $de])
            ->select('pago_pse.*', 'servicio.id_servicio', 'servicio.servicio_nombre', 'servicio.codigo_servicio', 'parque.nombre_parque', 'parque.codigo_parque', 'medio_pago.Nombre as medio_pago')
            ->get();
        return Excel::download(new PaymentzExport($paymentz), 'Reporte_Pagos.xlsx');
    }

    public function json(Request $request)
    {
        $tabla = Pago::with('service', 'park', 'state')->where('estado_id', 2)->get();
        return response()->json($tabla, 200);
    }

    public function saveOtp(Request $request)
    {
        try {
            $help = new Helpers();
            $pay = Pago::where('codigo_pago',  $request->code_payment)->first();
            $pay->otp = strval(mt_rand(100000, 999999));;
            $pay->otp_expiration = Carbon::now()->addMinutes(5);
            $pay->save();
            $help->sendEmailOtp($pay);
            return $this->success_message([
                'message' => 'OTP registrada con éxito',
                'error' => false
            ]);
        } catch (\Exception $ex) {
            return $this->error_response([
                'message' => 'Error al registrar OTP',
                'error' => true
            ]);
        }
    }

    public function confirmOtp(Request $request)
    {
        $pay = Pago::where('codigo_pago', $request->code_payment)->first();
        if (!$pay) {
            return $this->error_response([
                'message' => 'No existe una transacción asociada al código',
                'error' => true
            ]);
        }

        if ($request->otp != $pay->otp) {
            return $this->error_response([
                'message' => 'El Código es incorrecto',
                'error' => true
            ]);
        }

        $now = Carbon::now();
        $expirationTime = Carbon::parse($pay->otp_expiration);
        // Calcula la diferencia en minutos
        $differenceInMinutes = $now->diffInMinutes($expirationTime);

        // Comprueba si la segunda fecha es anterior a la primera
        $isExpirationTimeBeforeNow = $expirationTime->diff($now)->invert;

        // Si invert es true, entonces $expirationTime es anterior a $now
        if ($isExpirationTimeBeforeNow) {
            $differenceInMinutes = -$differenceInMinutes;
        }

        if ($differenceInMinutes > 0) {
            return $this->error_response([
                'message' => 'El codigo ha expirado',
                'error' => true
            ]);
        }

        return $this->success_message([
            'message' => 'Otp correcto',
            'error' => false
        ]);
    }

    public function verifyUser(Request $request) {

        $this->validate($request, [
            'username' => 'required',
            'password' => 'required',
        ]);

        if (Adldap::auth()->attempt($request->username, $request->password)) {
            return $this->success_message([
                'message' => 'Usuario Valido',
                'error' => false
            ]);
        } else {
            return $this->error_response([
                'message' => 'Usuario no Valido',
                'error' => true
            ]);
        }
    }

    public function saveOtpAdminPark(Request $request) {

        $this->validate($request, ['email_partial' => 'required']);

        try {
            $help = new Helpers();
            $query = new OtpAdminPark();
            $fragmentos  = explode("@", $request->email_partial);
            $query->email = trim(strtolower($fragmentos[0])) . '@idrd.gov.co';
            $query->otp = strval(mt_rand(100000, 999999));;
            $query->otp_expiration = Carbon::now()->addMinutes(5);
            $query->save();
            $help->sendEmailOtpAdminPark($query->email, $query->otp);
            return $this->success_message([
                'message' => 'OTP registrada con éxito',
                'error' => false,
                'id' => $query->id,
            ]);
        } catch (\Exception $ex) {
            return $this->error_response([
                'message' => 'Error al registrar OTP',
                'error' => true,
                'id' => null,
            ]);
        }
    }

    public function confirmOtpAdminPark(Request $request)
    {
        $transaction = OtpAdminPark::where('id', $request->id)->first();
        if (!$transaction) {
            return $this->error_response([
                'message' => 'No existe una transacción asociada al código',
                'error' => true
            ]);
        }

        if ($request->otp != $transaction->otp) {
            return $this->error_response([
                'message' => 'El Código es incorrecto',
                'error' => true
            ]);
        }

        $now = Carbon::now();
        $expirationTime = Carbon::parse($transaction->otp_expiration);
        // Calcula la diferencia en minutos
        $differenceInMinutes = $now->diffInMinutes($expirationTime);

        // Comprueba si la segunda fecha es anterior a la primera
        $isExpirationTimeBeforeNow = $expirationTime->diff($now)->invert;

        // Si invert es true, entonces $expirationTime es anterior a $now
        if ($isExpirationTimeBeforeNow) {
            $differenceInMinutes = -$differenceInMinutes;
        }

        if ($differenceInMinutes > 0) {
            return $this->error_response([
                'message' => 'El codigo ha expirado',
                'error' => true
            ]);
        }

        return $this->success_message([
            'message' => 'Otp correcto',
            'error' => false
        ]);
    }

}
