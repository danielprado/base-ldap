<?php

namespace App\Modules\Parks\src\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Parks\src\Constants\Roles;
use App\Modules\Parks\src\Models\Park;
use App\Modules\Parks\src\Models\EmergencyPlanFile;
use App\Modules\Parks\src\Models\EmergencyPlan;
use App\Modules\Parks\src\Models\EmergencyPlanCategory;
use Illuminate\Support\Carbon;
use Ramsey\Uuid\Uuid;

class PlansEmergencyController extends Controller
{
    //
    public function __construct()
	{
		parent::__construct();
		$this->middleware('auth:api')->only(['remove', 'add']);
		// $this->middleware(Roles::actions(Feature::class, 'create_or_manage'))->only('create');
		// $this->middleware(Roles::actions(Feature::class, 'update_or_manage'))->only('update');
		// $this->middleware(Roles::actions(Feature::class, 'destroy_or_manage'))->only('destroy');
        
	}

    public function add(Request $request)
    {
        try {
            if ($request->hasFile('file')) {
                $now = Carbon::now()->format('Y-m-d H:i:s');
                $ext = $request->file('file')->extension();
                $random = Uuid::uuid1();
                $name = strtoupper($random) . "." . $ext;
                $request->file('file')->storeAs('parks/planes', $name, [ 'disk' => 'public' ]);
                $planFile = new EmergencyPlanFile();
                $planFile->descripcionArchivo = strtoupper($request->descripcionArchivo);
                $planFile->idCategoria = $request->idCategoria;
                $planFile->nombreArchivo = $name;
                $planFile->orden = $request->orden;
                $planFile->fecha = $now;
                $planFile->usuario = 0;
                $planFile->save();
                $planEmergency = new EmergencyPlan();
                $planEmergency->version = $request->version;
                $planEmergency->idArchivo = $planFile->id;
                $planEmergency->idParque = $request->idParque;
                $planEmergency->Fecha_Version = $now;
                $planEmergency->save();
                return $this->success_message(__('validation.handler.success'));
            }
            return $this->error_response(__('validation.handler.unexpected_failure'));
        } catch (\Exception $ex) {
            return $this->error_response(__('validation.handler.unexpected_failure'));
        }
       
    }

    public function categories(){
        $categories = EmergencyPlanCategory::all();
        return $categories;
    }

}
