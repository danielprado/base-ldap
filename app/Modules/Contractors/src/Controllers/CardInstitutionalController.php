<?php


namespace App\Modules\Contractors\src\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\Contractors\src\Models\Contractor;
use Dompdf\Dompdf;
use Dompdf\Options;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Auth;
use LaravelQRCode\Facades\QRCode;

class CardInstitutionalController extends Controller
{
    public function getCardInst()
    {
        $user = Auth::guard('api')->user();
        $contractor = Contractor::with(['contracts', 'contracts.dependency', 'blood_type'])->where('document', $user->document)->first();

        if ($contractor->photo == null) {
            return $this->error_response(
                'No existe fotografía asociada al contratista',
                Response::HTTP_BAD_REQUEST,
                'Ve al sección de actualizar datos, y actualiza tu fotografía'
            );
        }

        if (empty($contractor->contracts) || !isset($contractor->contracts[0])) {
            return $this->error_response(
                'No existen contratos asociados al contratista',
                Response::HTTP_BAD_REQUEST,
                'Asegúrate de que el contratista tiene contratos asignados'
            );
        }

        $photoValue = $contractor->getAttributes()['photo'];
        $src_carnet = 'data:image/png;base64,' . base64_encode(Storage::get('templates/CARNET_INSTITUCIONAL.png'));
        $src_logo = 'data:image/png;base64,' . base64_encode(Storage::get('templates/LOGO_IDRD.png'));
        $src_sign = 'data:image/png;base64,' . base64_encode(Storage::get('templates/SIGN_CARNET.png'));

        $name = isset($contractor->name) && !is_null($contractor->name)
            ? mb_strtoupper($contractor->name, 'UTF-8')
            : '';

        $surname = isset($contractor->surname) && !is_null($contractor->surname)
            ? mb_convert_case(mb_strtolower($contractor->surname, 'UTF-8'), MB_CASE_TITLE, 'UTF-8')
            : '';

        // Validar el área
        $area = isset($contractor->contracts[0]->dependency->name) && !empty($contractor->contracts[0]->dependency->name)
            ? mb_convert_case(mb_strtolower($contractor->contracts[0]->dependency->name, 'UTF-8'), MB_CASE_TITLE, 'UTF-8')
            : 'Área no disponible';

        // Validar el documento
        $document = isset($contractor->document) && !empty($contractor->document)
            ? mb_convert_case(mb_strtolower($contractor->document, 'UTF-8'), MB_CASE_TITLE, 'UTF-8')
            : 'Documento no disponible';

        // Validar la imagen de usuario
        $src_img_user = isset($photoValue) && Storage::exists('contractor/' . $photoValue)
            ? 'data:image/' . pathinfo($photoValue, PATHINFO_EXTENSION) . ';base64,' . base64_encode(Storage::get('contractor/' . $photoValue))
            : null; // Puedes manejar un valor por defecto si no hay imagen

        // Validar la fecha de inicio del contrato
        $start_date = isset($contractor->contracts[0]->start_date) && !empty($contractor->contracts[0]->start_date)
            ? substr($contractor->contracts[0]->start_date, 0, 10)
            : 'Fecha de inicio no disponible';

        // Validar la fecha final del contrato
        $final_date = isset($contractor->contracts[0]->final_date) && !empty($contractor->contracts[0]->final_date)
            ? substr($contractor->contracts[0]->final_date, 0, 10)
            : 'Fecha final no disponible';

        // Validar el número de contrato
        $number_contract = isset($contractor->contracts[0]->contract) && !empty($contractor->contracts[0]->contract)
            ? $contractor->contracts[0]->contract
            : 'Número de contrato no disponible';

        // Validar rh
        $rh = isset($contractor->blood_type) && !empty($contractor->blood_type->Nombre_GrupoSanguineo)
        ? $contractor->blood_type->Nombre_GrupoSanguineo
        : '--';


        $qrData = 'Número de contrato: ' . $number_contract . ', Fecha de Inicio: ' . $start_date . ', Fecha final: ' . $final_date;
        $passport = $contractor->document;
        QrCode::text($qrData)
            ->setSize(100)
            ->setErrorCorrectionLevel('H')
            ->setMargin(2)
            ->setOutfile(storage_path("app/contractor/qr/$passport.png"))
            ->png();

        $qrCodePath = storage_path("app/contractor/qr/$passport.png");
        $qrCodeBase64 = 'data:image/png;base64,' . base64_encode(file_get_contents($qrCodePath));


        $html = '
        <!DOCTYPE html>
        <html lang="es">
        <head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <title>Carnet Institucional</title>
            <link rel="preconnect" href="https://fonts.googleapis.com">
            <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
            <link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">
            <style>
                * {
                    margin: 0;
                    padding: 0;
                    box-sizing: border-box;
                }

                html, body {
                    margin: 0;
                    padding: 0;
                    font-family: "Roboto", Arial, sans-serif;
                    width: 100%;
                    height: 100%;
                }

                .full-page {
                    width: 207px;
                    height: 396px;
                    position: relative;
                }

                .template-img {
                    width: 207px;
                    height: 396px;
                    position: absolute;
                    top: 0;
                    left: 0;
                    z-index:1;
                }
                .container-img {
                    position: absolute;
                    top: 9.8;
                    left: 45.4;
                    z-index: 2;
                }
                .img-user {
                    width: 99px;
                    height: 99px;
                    object-fit: cover;
                    border-radius: 60px;
                }
                .container-text {
                    position: absolute;
                    top: 125px;
                    left: 0;
                    z-index: 2;
                    width: 100%;
                    text-align: center;
                }
                .reset_p {
                    margin:-2;
                    padding: -2;
                }
                .name {
                    font-weight: bold;
                    font-size: 12px;
                    text-transform: uppercase;
                }
                .last-name {
                    font-weight: 400;
                    font-size: 12px;
                }
                .area {
                    position: absolute;
                    top: 174px;
                    left: 0;
                    z-index: 2;
                    width: 100%;
                    text-align: center;
                }
                .area-text {
                    font-weight: 300;
                    font-size: 8px;
                    text-transform: capitalize;
                    line-height: 7px;
                }
                .type {
                    font-weight: 300;
                    font-size: 8px;
                    text-transform: capitalize;
                }
                .document {
                    font-weight: 300;
                    font-size: 8px;
                    text-transform: capitalize;
                }
                .logo {
                    position: absolute;
                    bottom: 8px;
                    left: 5px;
                    z-index: 2;
                }
                .img-logo {
                    width: auto;
                    height: 22px;
                }
                .qr-code {
                    position: absolute;
                    bottom: 85px;
                    left: 62px;
                    z-index: 2;
                }
                .img-qr {
                    width: 86px;
                    height: 86px;
                }
                .container-text-idrd {
                    position: absolute;
                    bottom: 70px;
                    left: 0;
                    z-index: 2;
                    width: 100%;
                    text-align: center;
                }
                .container-text-legend {
                    position: absolute;
                    bottom: 53px;
                    left: 0;
                    z-index: 2;
                    padding-left: 12px;
                    padding-right: 12px;
                    text-align: center;
                }
                .text-idrd {
                    font-size: 8px;
                    font-weight: bold;
                }
                .text-legend {
                    font-size: 4px;
                    font-weight: 700;
                    line-height: 4px;
                }
                .container-sign {
                    position: absolute;
                    bottom: 12px;
                    right: 10px;
                    z-index: 2;
                    text-align: center;
                }
                .img-sign {
                    width: auto;
                    height: 25px
                }
                .separator {
                    border-top: 1px solid black;
                    width: 100%;
                    padding-top: 3px;
                }
            </style>
        </head>
        <body>
            <div class="full-page">
                <div class="template-img">
                    <img  src="' . $src_carnet . '" width="207" height="396" />
                </div>
                <div class="container-img">
                    <img src="' . $src_img_user . '" class="img-user" />
                </div>
                <div class="container-text">
                    <p class="name">' . $name . '</p>
                    <p class="last-name">' .  $surname . '</p>
                </div>
                 <div class="area">
                    <p class="reset_p area-text">Área ' . $area .'</p>
                    <p class="reset_p type">Contratista</p>
                    <p class="reset_p document">CC: ' . $document . '</p>
                    <p class="reset_p document">RH: ' . $rh . '</p>
                </div>
                <div class="qr-code">
                    <img src="' . $qrCodeBase64 . '" alt="Código QR" class="img-qr" />
                </div>
                <div class="container-text-idrd">
                    <p class="reset_p text-idrd">Instituto Distrital de Recreación y Deporte</p>
                </div>
                <div class="container-text-legend">
                    <p class="reset_p text-legend">Este carnet lo identifica como funcionario del instituto Distrital de Recreación y Deporte. Debe portarse en un lugar visible durante las horas de trabajo. En el momento de retiro debe ser devuelto al Área de almacén.</p>
                </div>
                <div class="logo">
                    <img src="' . $src_logo . '" alt="logo" class="img-logo" />
                </div>
                <div class="container-sign">
                    <img src="' . $src_sign . '" alt="logo" class="img-sign" />
                    <div class="separator"/>
                    <p class="reset_p text-legend">Firma que autoriza</>
                </div>
            </div>

        </body>
        </html>';

        $options = new Options();
        $options->set('isHtml5ParserEnabled', true);
        $options->set('isRemoteEnabled', true);
        $options->set("isPhpEnabled", true);
        $dompdf = new Dompdf($options);

        $ancho = 55 * 2.835;  // ≈ 155.925 puntos
        $alto = 105 * 2.835;  // ≈ 297.675 puntos

        $dompdf->loadHtml($html);
        $dompdf->setPaper([0, 0, $ancho, $alto], 'portrait');
        $dompdf->render();
        $result = $dompdf->output();
        return $this->success_message([
            "file_name" => 'CARNET_INSTITUCIONAL.pdf',
            "file" => "data:application/pdf;base64," . base64_encode($result)
        ]);
    }
}
