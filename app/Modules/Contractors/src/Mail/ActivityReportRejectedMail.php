<?php

namespace App\Modules\Contractors\src\Mail;

use App\Modules\Contractors\src\Models\ActivityReport;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ActivityReportRejectedMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var ActivityReport
     */
    private $activityReport;

    /**
     * @var string
     */
    private $observation;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(
        ActivityReport $activityReport,
        string $observation
    ) {
        $this->activityReport = $activityReport;
        $this->observation = $observation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $contract = $this->activityReport->contract_number;
        $date = $this->activityReport->updated_at;
        $protocol = 'https';

        $path = config('app.env') != 'production'
            ? "/portal-contratista-dev"
            : "/";

        $subdomain = config('app.env') != 'production' ? "sim" : "portalcontratista";
        $domain = "{$subdomain}.idrd.gov.co";

        if (config('app.env') === 'local') {
            $protocol = 'http';
            $domain = 'localhost:3000';
        }

        $url = "{$protocol}://{$domain}{$path}";
        $id = $this->activityReport->id;
        $style = "font-family:'Open Sans',Arial,sans-serif;font-size:24px;color:#3b3b3b;font-weight:bold;letter-spacing:4px;";

        return $this->view('mail.mail')
            ->subject('Informe de Actividades - Rechazado')
            ->with([
                'slogan'    => 'Notificación Informe de Actividades IDRD',
                'header'    => 'IDRD',
                'title'     => 'Informe de Actividades - Portal Contratista',
                'content'   => "¡El informe de actividades generado para el contrato {$contract} ha sido rechazado! Puede realizar la revisión dando click en el siguiente botón.",
                'details'   => "
                    <div style='text-align: center;'>
                        <h2 style='$style'>Observaciones</h2>
                        <p>$this->observation</p>
                        <p>Fecha de Creación: $date</p>
                    </div>
                ",
                'url'       => "{$url}/es/activity-report-store/$id",
                'year'      => now()->year
            ]);
    }
}
