<?php

namespace App\Modules\Payroll\src\Exports;

// use App\Modules\Payroll\src\Models\CertificateCompliance;
use App\Modules\Payroll\src\Request\PayrollExcelRequest;
use Maatwebsite\Excel\Excel;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Color;

class PayrollExportTemplate
{
    /** @var array */
    private $collections;

    /**  @var array */
    private $collectionsSummary;

    /** @var object **/
    private $requestAux;

    /** @var array **/
    private $fundingSourceCollection;

    /** @var array **/
    private $entryCollection;

    /** @var array **/
    private $exspenseConceptCollection;

    /**
     * @var \PhpOffice\PhpSpreadsheet\Spreadsheet
     */
    private $file;

    /**
     * PayrollExportTemplateExportTemplate constructor.
     */
    public function __construct(PayrollExcelRequest $request)
    {
        $this->collections = collect($request->get('contractorsList'));
        $this->collectionsSummary = collect($request->get('contractorsSummaryList'));
        $this->requestAux = collect($request);

        $this->fundingSourceCollection = collect($request->get('fundingSourceList'));
        $this->entryCollection = collect($request->get('entryList'));
        $this->exspenseConceptCollection = collect($request->get('exspenseConceptList'));
    }

    public function create()
    {
        try {
            $this->file = IOFactory::load( storage_path('app/templates/PLANILLA_DE_PAGO_V1.3.xlsx') );
            $this->file->setActiveSheetIndex(0);
            $this->worksheet = $this->file->getActiveSheet();
            $this->worksheet->getProtection()->setSheet(true);

            $style = array(
                'alignment' => array(
                    'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
                    'vertical' => \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER,
                )
            );
            if(count($this->collections) > 1){
                $this->worksheet->insertNewRowBefore(11, count($this->collections) - 1);
            }
            $i = 10;
            $contador = 1;
            $total_pagar_aux = 0;

            $collectionAux = collect([]);
            $countByIdentification = $this->collections->countBy('contract_number');
            foreach ($this->collections as $key => $collection) {

                $this->worksheet->mergeCells("D$i:E$i");
                $this->worksheet->getStyle("C$i:U$i")->applyFromArray($style);

                //combinar celdas por identificación
                if(! $collectionAux->contains( $collection['contract_number'] ) ){
                    $y = $i + $countByIdentification["{$collection['contract_number']}"];
                    $y--;
                    $this->worksheet->getCell("C$i")->setValue($contador);
                    $this->worksheet->mergeCells("C$i:C$y");
                    $this->worksheet->mergeCells("G$i:G$y");
                    $this->worksheet->mergeCells("Q$i:Q$y");

                    $this->worksheet->mergeCells("U$i:U$y");
                    $this->worksheet->mergeCells("V$i:V$y");
                    $this->worksheet->mergeCells("W$i:W$y");
                    $this->worksheet->mergeCells("X$i:X$y");
                    $this->worksheet->mergeCells("Y$i:Y$y");
                    $this->worksheet->mergeCells("Z$i:Z$y");
                    $this->worksheet->mergeCells("AA$i:AA$y");
                    $this->worksheet->mergeCells("AB$i:AB$y");
                    $this->worksheet->mergeCells("AC$i:AC$y");
                    $this->worksheet->mergeCells("AD$i:AD$y");
                    $this->worksheet->mergeCells("AE$i:AE$y");
                    $this->worksheet->mergeCells("AF$i:AF$y");
                    $this->worksheet->mergeCells("AG$i:AG$y");
                    $this->worksheet->mergeCells("AH$i:AH$y");
                    $this->worksheet->mergeCells("AI$i:AI$y");
                    $this->worksheet->mergeCells("AJ$i:AJ$y");
                    $this->worksheet->mergeCells("AK$i:AK$y");
                    $this->worksheet->mergeCells("AL$i:AL$y");
                    $this->worksheet->mergeCells("AM$i:AM$y");
                    $this->worksheet->mergeCells("AN$i:AN$y");
                    $this->worksheet->mergeCells("AO$i:AO$y");
                    $this->worksheet->mergeCells("AP$i:AP$y");
                    $this->worksheet->mergeCells("AQ$i:AQ$y");
                    $this->worksheet->mergeCells("AR$i:AR$y");
                    $this->worksheet->mergeCells("AS$i:AS$y");
                    $this->worksheet->mergeCells("AT$i:AT$y");
                    $this->worksheet->mergeCells("AU$i:AU$y");

                    //Opción 3: Mostrar pago mes campo full_payment que viene desde SEVEN
                    $this->worksheet->getCell("Q$i")->setValue(isset($collection['full_payment']) ? (double) $collection['full_payment'] : null);

                    //payroll
                    $auxSummary = $this->collectionsSummary->firstWhere('contract_number', $collection['contract_number']);
                    $this->worksheet->getCell("U$i")->setValue(isset($auxSummary['base_ica']) ? (double) $auxSummary['base_ica'] : null);
                    $this->worksheet->getCell("V$i")->setValue(isset($auxSummary['balance']) ? (double) $auxSummary['balance'] : null);
                    $this->worksheet->getCell("W$i")->setValue(isset($auxSummary['account_type']) ? (string) $auxSummary['account_type'] : null);
                    $this->worksheet->getCell("X$i")->setValue(isset($auxSummary['account_number']) ? (string) $auxSummary['account_number'] : null);
                    $this->worksheet->getCell("Y$i")->setValue(isset($auxSummary['bank_name']) ? (string)$auxSummary['bank_name'] : null);
                    $this->worksheet->getCell("Z$i")->setValue(isset($auxSummary['bank_code']) ? (string) $auxSummary['bank_code'] : null);

                    $this->worksheet->getCell("AA$i")->setValue(isset($auxSummary['uvt']) ? (double) $auxSummary['uvt'] : null);
                    $this->worksheet->getCell("AB$i")->setValue(isset($auxSummary['total_eps']) ? (double) $auxSummary['total_eps'] : null);
                    $this->worksheet->getCell("AC$i")->setValue(isset($auxSummary['total_pension']) ? (double) $auxSummary['total_pension'] : null);
                    $this->worksheet->getCell("AD$i")->setValue(isset($auxSummary['total_arl']) ? (double) $auxSummary['total_arl'] : null);
                    $this->worksheet->getCell("AE$i")->setValue(isset($auxSummary['total_housing']) ? (double) $auxSummary['total_housing'] : null);
                    $this->worksheet->getCell("AF$i")->setValue(isset($auxSummary['total_children']) ? (double) $auxSummary['total_children'] : null);
                    $this->worksheet->getCell("AG$i")->setValue(isset($auxSummary['total_afc']) ? (double) $auxSummary['total_afc'] : null);
                    $this->worksheet->getCell("AH$i")->setValue(isset($auxSummary['ingreso_base_gravado']) ? (double) $auxSummary['ingreso_base_gravado'] : null);
                    $this->worksheet->getCell("AI$i")->setValue(isset($auxSummary['ingreso_base_gravado_25']) ? (double) $auxSummary['ingreso_base_gravado_25'] : null);
                    $this->worksheet->getCell("AJ$i")->setValue(isset($auxSummary['base_uvt']) ? (double) $auxSummary['base_uvt'] : null);
                    $this->worksheet->getCell("AK$i")->setValue(isset($auxSummary['base_ica']) ? (double) $auxSummary['base_ica'] : null);
                    $this->worksheet->getCell("AL$i")->setValue(isset($auxSummary['est_pcul']) ? (double) $auxSummary['est_pcul'] : null);
                    $this->worksheet->getCell("AM$i")->setValue(isset($auxSummary['est_ppm']) ? (double) $auxSummary['est_ppm'] : null);
                    $this->worksheet->getCell("AN$i")->setValue(isset($auxSummary['total_ica']) ? (double) $auxSummary['total_ica'] : null);
                    $this->worksheet->getCell("AO$i")->setValue(isset($auxSummary['retefuente']) ? (double) $auxSummary['retefuente'] : null);
                    $this->worksheet->getCell("AP$i")->setValue(isset($auxSummary['other_discounts']) ? (double) $auxSummary['other_discounts'] : null);
                    $this->worksheet->getCell("AQ$i")->setValue(isset($auxSummary['total_deductions']) ? (double) $auxSummary['total_deductions'] : null);
                    $this->worksheet->getCell("AR$i")->setValue(isset($auxSummary['net_pay']) ? (double) $auxSummary['net_pay'] : null);



                    $contador++;
                    $collectionAux->push($collection['contract_number']);
                }

                $this->worksheet->getCell("D$i")->setValue(isset($collection['person_name']) ? (string) $collection['person_name'] : null);
                $this->worksheet->getCell("F$i")->setValue(isset($collection['identification']) ? (string) $collection['identification'] : null);
                $this->worksheet->getCell("G$i")->setValue(isset($collection['contract_object']) ? (string) $collection['contract_object'] : null);
                $this->worksheet->getCell("H$i")->setValue(isset($collection['entry']) ? (string) $collection['entry'] : null);
                $this->worksheet->getCell("I$i")->setValue(isset($collection['contract_number']) ? (string) $collection['contract_number'] : null);
                $this->worksheet->getCell("J$i")->setValue(isset($collection['registry_number']) ? (string) $collection['registry_number'] : null);

                $this->worksheet->getCell("K$i")->setValue(isset($collection['source']) ? (string) $collection['source'] : null);
                $this->worksheet->getCell("L$i")->setValue(isset($collection['exspense_concept']) ? (string) $collection['exspense_concept'] : null);

                $this->worksheet->getCell("M$i")->setValue(isset($collection['position']) ? (string) $collection['position'] : null);
                $this->worksheet->getCell("N$i")->setValue(isset($collection['pmr']) ? (string) $collection['pmr'] : null);

                $this->worksheet->getCell("O$i")->setValue(date('d/m/Y',strtotime($collection['start_date'])) );
                $this->worksheet->getCell("P$i")->setValue(date('d/m/Y',strtotime($collection['final_date'])) );

                $this->worksheet->getCell("R$i")->setValue("{$collection['startPeriod']} AL {$collection['finalPeriod']}");
                $this->worksheet->getCell("S$i")->setValue(isset($collection['dias_trabajados']) ? (int) $collection['dias_trabajados'] : null);
                //$this->worksheet->getCell("T$i")->setValue(isset($collection['total_pagar']) ? (double) $collection['total_pagar'] : null);
                $aux_total = round( (int) $collection['dias_trabajados'] / 30 * (double) $collection['pago_mensual'] );
                $total_pagar_aux += $aux_total;
                $this->worksheet->getCell("T$i")->setValue($aux_total);

                // planilla de pago

                $i++;
            }
            $i++;
            // Notas u observaciones
            $this->worksheet->getCell("C$i")->setValue( isset($this->requestAux['observations']) ? "OBSERVACIONES: " . toUpper((string) $this->requestAux['observations']) : null );

            // datos supervisores
            //i es para la fila nombre
            $i += 4;
            $filaFirma = $i - 1;
            $filaIdentificacion = $i + 1;
            $filaCargo = $i + 2;

            $this->worksheet->getCell("E$i")->setValue(isset($this->requestAux['responsibleName']) ? toUpper((string) $this->requestAux['responsibleName']) : null );
            $this->worksheet->getCell("E$filaIdentificacion")->setValue(isset($this->requestAux['areaName']) ? 'ENCARGADO ' . toUpper((string) $this->requestAux['areaName']) : null );

            $this->worksheet->getCell("I$i")->setValue(isset($this->requestAux['diligenceName']) ? toUpper((string) $this->requestAux['diligenceName']) : null );
            $this->worksheet->getCell("I$filaIdentificacion")->setValue(isset($this->requestAux['diligenceProfesion']) ? toUpper((string) $this->requestAux['diligenceProfesion']) : null );

            //resume page
            $this->worksheet->getStyle("A1:Z$i")//A13:U$i
                            ->getProtection()
                            ->setLocked(\PhpOffice\PhpSpreadsheet\Style\Protection::PROTECTION_PROTECTED);

            $this->file->setActiveSheetIndex(1);
            $this->worksheet = $this->file->getActiveSheet();
            $this->worksheet->getProtection()->setSheet(true);
            $this->worksheet->getCell("B5")->setValue("PERIODO DE PAGO: {$this->requestAux['settlementPeriod']}");
            $this->worksheet->getCell("B8")->setValue(isset($this->requestAux['budgetCode']) ? (string) $this->requestAux['budgetCode'] : null );
            $this->worksheet->getCell("C8")->setValue(isset($this->requestAux['totalPayroll']) ? (double) $this->requestAux['totalPayroll'] : null );

            $indexResume = 10;
            // resumen fuentes de financiación
            if(count($this->fundingSourceCollection) > 1){
                $this->worksheet->insertNewRowBefore(11, count($this->fundingSourceCollection) - 1);
            }
            foreach ($this->fundingSourceCollection as $key => $collection) {
                $this->worksheet->getCell("B$indexResume")->setValue(isset($collection['fundingSource'])  ? (string) $collection['fundingSource'] : null);
                $this->worksheet->getCell("C$indexResume")->setValue(isset($collection['total'])  ? (double) $collection['total'] : null);
                $indexResume ++;
            }

            // resumen rubros
            $indexResume ++;
            if(count($this->entryCollection) > 1){
                $this->worksheet->insertNewRowBefore(($indexResume + 1), count($this->entryCollection) - 1);
            }
            foreach ($this->entryCollection as $key => $collection) {
                $this->worksheet->getCell("B$indexResume")->setValue(isset($collection['entry'])  ? (string) $collection['entry'] : null);
                $this->worksheet->getCell("C$indexResume")->setValue(isset($collection['total'])  ? (double) $collection['total'] : null);
                $indexResume ++;
            }

            $indexResume ++;
            // resumen componentes gasto
            if(count($this->exspenseConceptCollection) > 1){
                $this->worksheet->insertNewRowBefore(($indexResume + 1), count($this->exspenseConceptCollection) - 1);
            }
            foreach ($this->exspenseConceptCollection as $key => $collection) {
                $this->worksheet->getCell("B$indexResume")->setValue(isset($collection['exspenseConcept'])  ? (string) $collection['exspenseConcept'] : null);
                $this->worksheet->getCell("C$indexResume")->setValue(isset($collection['total'])  ? (double) $collection['total'] : null);
                $indexResume ++;
            }

            $this->worksheet->getCell("B4")->setValue(isset($this->requestAux['areaName']) ? 'CONTRATISTAS ' .toUpper( (string) $this->requestAux['areaName']) : 'CONTRATISTAS' );

            $indexResume ++;
            $this->worksheet->getCell("C$indexResume")->setValue(isset($this->requestAux['estPculSummary']) ? (double) $this->requestAux['estPculSummary'] : null );
            $indexResume ++;
            $this->worksheet->getCell("C$indexResume")->setValue(isset($this->requestAux['estPpmSummary']) ? (double) $this->requestAux['estPpmSummary'] : null );
            $indexResume ++;
            $this->worksheet->getCell("C$indexResume")->setValue(isset($this->requestAux['icaSummary']) ? (double) $this->requestAux['icaSummary'] : null );
            $indexResume ++;
            $this->worksheet->getCell("C$indexResume")->setValue(isset($this->requestAux['retefuenteSummary']) ? (double) $this->requestAux['retefuenteSummary'] : null );
            $indexResume ++;
            $this->worksheet->getCell("C$indexResume")->setValue(isset($this->requestAux['otherDiscountsSummary']) ? (double) $this->requestAux['otherDiscountsSummary'] : null );
            $indexResume ++;
            $this->worksheet->getCell("C$indexResume")->setValue(isset($this->requestAux['totalDiscounts']) ? (double) $this->requestAux['totalDiscounts'] : null );
            $indexResume += 2;
            $this->worksheet->getCell("C$indexResume")->setValue(isset($this->requestAux['totalBank']) ? (double) $this->requestAux['totalBank'] : null );

            return IOFactory::createWriter($this->file, Excel::XLSX);
        } catch (\Exception $exception) {

        }
    }
}
