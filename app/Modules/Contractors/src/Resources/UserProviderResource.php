<?php

namespace App\Modules\Contractors\src\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserProviderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'pvd_codi'  => $this->pvd_codi ?? null,
            'pvd_dive'  => $this->pvd_dive ?? null,
            'pvd_nomb'  => $this->pvd_nomb ?? null,
            'pvr_apel'  => $this->pvr_apel ?? null,
            'tip_codi'  => $this->tip_codi ?? null,
            'tip_codi_text'  => isset($this->tip_codi) ? $this->getDocumentType($this->tip_codi) : null,
            'pvr_esta'  => $this->pvr_esta ?? null,
            'pvr_estate_text'  => isset($this->pvr_esta) ? $this->getStateText($this->pvr_esta): null,
            'pvd_fcre'  => $this->pvd_fcre ?? null,
            'pvd_clas'  => $this->pvd_clas ?? null,
            'pvd_clas_text'  => isset($this->pvd_clas) ? $this->getClassificationText($this->pvd_clas): null,
            'pvr_noco'  => $this->pvr_noco ?? null,
            'pvr_auto'  => $this->pvr_auto ?? null,
            'pvr_riva'  => $this->pvd_fcre ?? null,
            'tpr_codi'  => $this->tpr_codi ?? null,
            'mon_codi'  => $this->mon_codi ?? null,
            'pvr_clad'  => $this->pvr_clad ?? null,
        ];
    }

    public static function headers(): array
    {
        return [
            [
                'text' => "Tipo de documento",
                'value'  =>  "tip_codi_text",
                'icon'  =>  'mdi-card-account-details',
            ],
            [
                'text' => "Documento",
                'value'  =>  "pvd_codi",
                'icon'  =>  'mdi-numeric',
            ],
            [
                'text' => "Dígito de verificación",
                'value'  =>  "pvd_dive",
                'icon'  =>  'mdi-numeric',
            ],
            [
                'text' => "Nombres",
                'value'  =>  "pvd_nomb",
                'icon'  =>  'mdi-face-man',
            ],
            [
                'text' => "Apellidos",
                'value'  =>  "pvr_apel",
                'icon'  =>  'mdi-face-man',
            ],
            [
                'text' => "Clasificación",
                'value'  =>  "pvd_clas_text",
                'icon'  =>  'mdi-account-tie',
            ],
            [
                'text' => "Estado del proveedor",
                'value'  =>  "pvr_estate_text",
                'icon'  =>  'mdi-account-badge',
            ],
        ];
    }

    public function getDocumentType($document_type_id): string
    {
        switch ($document_type_id) {
            case(2):
                $msg = "C.C";
                break;
            case(1):
                $msg = "N.I.T.";
                break;
            case(3):
                $msg = "C.E";
                break;
            case(5):
                $msg = "PA";
                break;
            case(6):
                $msg = "T.I";
                break;
            case(11):
                $msg = "R.C";
                break;
            case(21):
                $msg = "T.E";
                break;
            case(22):
                $msg = "NITE";
                break;
            case(23):
                $msg = "PEP";
                break;
            case(24):
                $msg = "PPT";
                break;
            default:
                $msg = 'C.C';
        }

        return $msg;
    }

    public function getStateText($state): string
    {
        switch ($state) {
            case('A'):
                $msg = "APROBADO";
                break;
            case('R'):
                $msg = "RECHAZADO";
                break;
            case('I'):
                $msg = "INACTIVO";
                break;
            case('E'):
                $msg = "EN ESTUDIO";
                break;
            default:
                $msg = 'APROBADO';
        }

        return $msg;
    }

    public function getClassificationText($classification): string
    {
        switch ($classification) {
            case('N'):
                $msg = "NATURAL";
                break;
            case('J'):
                $msg = "JURÍDICO";
                break;
            case('C'):
                $msg = "CONSORCIO";
                break;
            case('E'):
                $msg = "UNIÓN TEMPORAL";
                break;
            case('A'):
                $msg = "OTRO";
                break;
            default:
                $msg = 'NATURAL';
        }

        return $msg;
    }

}
