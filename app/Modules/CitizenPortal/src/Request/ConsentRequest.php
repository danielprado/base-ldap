<?php


namespace App\Modules\CitizenPortal\src\Request;



use App\Modules\CitizenPortal\src\Constants\Roles;
use App\Modules\CitizenPortal\src\Models\Activity;
use Illuminate\Foundation\Http\FormRequest;

class ConsentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('api')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'     =>  'required|string|min:3|max:191',
            'file'     =>  'required|file|mimes:pdf',
            'adult'      =>  'required|bool',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'title' =>  __('citizen.validations.title'),
            'file' =>  __('citizen.validations.file'),
            'adult' =>  __('citizen.validations.adult'),
        ];
    }
}
