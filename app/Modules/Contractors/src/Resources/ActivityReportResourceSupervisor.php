<?php

namespace App\Modules\Contractors\src\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ActivityReportResourceSupervisor extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id ?? null,
            'name' => $this->contractor->name .  ' '  . $this->contractor->surname ?? null,
            'contract' => $this->contract->contract ?? null,
            'contract_type' => $this->contract->contract_type->name ?? null,
            'contract_type_id' => $this->contract->contract_type->id ?? null,
            'state' => $this->state ?? null,
            'report_number' => $this->report_number ?? null,
            'report_date' => $this->report_date ?? null,
            'exp_virtual' => $this->exp_virtual ?? null,
            'document_contractor' => $this->contractor->document ?? null,
            'object_contract' => $this->contract->object_contract ?? null,
            'execution_period' => $this->contract->duration ?? null,
            'total' => $this->contract->total ?? null,
            'start_date'    => explode('T', $this->contract->start_date)[0]  ?? null,
            'final_date'    => explode('T', $this->contract->final_date)[0]  ?? null,
            'range_activities' => $this->activity_period ?? null,
            'eps_name'  => $this->eps_name ?? null,
            'eps_value' => $this->eps_value ?? null,
            'afp_name'  => $this->afp_name ?? null,
            'afp_value' => $this->afp_value ?? null,
            'risks_name'  => $this->risks_name ?? null,
            'risks_value' => $this->risks_value ?? null,
            'contributions_total'    => $this->contributions_total ?? null,
            'contractor_signature_url' => $this->contractor->signature ?? null,
            'supervisor_signature_url' => $this->supervisor_signature_path ?? null,
            'observations' => $this->observations ?? null,
            'updated_at' => explode('T', $this->updated_at)[0] ?? null,
            'evidences' => $this->whenLoaded('evidences'),
        ];
    }
}
