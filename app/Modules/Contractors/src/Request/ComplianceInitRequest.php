<?php


namespace App\Modules\Contractors\src\Request;


use App\Modules\Contractors\src\Models\Compliance;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ComplianceInitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'period_month'  => 'required|numeric',
            'contract_id'  => 'required|numeric',
            'document'  => 'nullable|numeric',
            'contractor_id' => 'required|numeric',
            'contract' => 'required',
            'period_year'  => [
                'required',
                Rule::unique('mysql_contractors.compliance')
                    ->where(function ($query) {
                        return $query->where([
                            'contract_id' => $this->get('contract_id'),
                            'contractor_id' => $this->get('contractor_id'),
                            'period_month' => $this->get('period_month'),
                            'period_year' => $this->get('period_year'),
                        ]);
                    }),
            ],
        ];
    }

    public function messages()
    {
        return [
            'period_year'   => 'Año del periodo a certificar',
            'period_month'   => 'Mes del periodo a certificar',
            'contract_id'   => 'Número de contrato',
            'document'   => 'número de documento',
            'period_year.unique' => 'Ya existe un certificado a liquidar para el periodo seleccionado.'
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'period_year'   => 'Año del periodo a certificar',
            'period_month'   => 'Mes del periodo a certificar',
            'contract_id'   => 'Número de contrato',
            'document'   => 'número de documento'
        ];
    }
}
