<?php

namespace App\Modules\Certifications\src\Exports;

use App\Models\Security\User;
use Illuminate\Contracts\Queue\ShouldQueue;
use Imtigger\LaravelJobStatus\JobStatus;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Events\BeforeWriting;

class CertificationDataExport implements WithMultipleSheets, ShouldQueue, WithEvents
{

    use Exportable;
    /**
     * @var array
     */
    private $request;

    /**
     * @var JobStatus
     */
    private $job;

    /**
     * @var User
     */
    private $user;

    /**
     * Excel constructor.
     *
     * @param array $request
     * @param int $job
     */
    public function __construct(array $request, int $job, User $user)
    {
        $this->user = $user;
        $this->request = $request;
        $this->job = $job;
        update_status_job($job, JobStatus::STATUS_EXECUTING, 'excel-certifications-module');
    }

    /**
     * @inheritDoc
     */
    public function sheets(): array
    {
        $sheets = [];

        if (array_key_exists('supports', $this->request)) {
            $sheets[0] = new SupportExport($this->request, $this->job, $this->user);
            return $sheets;
        }

        if (array_key_exists('contracts', $this->request)) {
            $sheets[0] = new ContractExport($this->request, $this->job, $this->user);
            $sheets[1] = new ObligationExport($this->request, $this->job, $this->user);
            $sheets[2] = new AdditionExport($this->request, $this->job, $this->user);
            $sheets[3] = new ExtensionExport($this->request, $this->job, $this->user);
            $sheets[4] = new SuspensionExport($this->request, $this->job, $this->user);
            $sheets[5] = new AssignmentExport($this->request, $this->job, $this->user);
            $sheets[6] = new MemberExport($this->request, $this->job, $this->user);
            $sheets[7] = new ExpeditionExport($this->request, $this->job, $this->user);

            return $sheets;
        }
    }

    public function registerEvents(): array
    {
        return [
            BeforeWriting::class => function(BeforeWriting $writer) {
                $writer->writer->getDelegate()->setActiveSheetIndex(0);
            }
        ];
    }
}
