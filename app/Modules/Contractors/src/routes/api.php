<?php

use App\Modules\Contractors\src\Controllers\AccountTypeController;
use App\Modules\Contractors\src\Controllers\AdminController;
use App\Modules\Contractors\src\Controllers\BankController;
use App\Modules\Contractors\src\Controllers\CareerController;
use App\Modules\Contractors\src\Controllers\ContractController;
use App\Modules\Contractors\src\Controllers\ContractorController;
use App\Modules\Contractors\src\Controllers\ContractTypeController;
use App\Modules\Contractors\src\Controllers\EconomicActivityController;
use App\Modules\Contractors\src\Controllers\FileController;
use App\Modules\Contractors\src\Controllers\FileTypeController;
use App\Modules\Contractors\src\Controllers\PaymentCertificatesController;
use App\Modules\Contractors\src\Controllers\PeaceAndSafeController;
use App\Modules\Contractors\src\Controllers\PlanController;
use App\Modules\Contractors\src\Controllers\ThirdPartyController;
use App\Modules\Contractors\src\Controllers\UserBankController;
use App\Modules\Contractors\src\Controllers\UserController;
use App\Modules\Contractors\src\Controllers\Certificados_TributariosController;
use App\Modules\Contractors\src\Controllers\CertificatesContractualController;
use App\Modules\Contractors\src\Controllers\ComplianceContributionController;
use App\Modules\Contractors\src\Controllers\ComplianceController;
use App\Modules\Contractors\src\Controllers\ComplianceFileController;
use App\Modules\Contractors\src\Controllers\PayrollController;
use App\Modules\Contractors\src\Controllers\SecopController;
use App\Modules\Contractors\src\Controllers\ActivityReportController;
use App\Modules\Contractors\src\Controllers\SupervisorController;
use App\Modules\Contractors\src\Controllers\ContributionController;
use App\Modules\Contractors\src\Controllers\ActivityEvidenceController;
use App\Modules\Contractors\src\Controllers\CardInstitutionalController;
use App\Modules\Contractors\src\Controllers\EmbargoesController;
use App\Modules\Contractors\src\Controllers\ManualsController;
use Illuminate\Support\Facades\Route;
use App\Modules\Contractors\src\Controllers\SurveyResponseController;



Route::prefix('contractors-portal')->group(function () {
    Route::get('/secop/user', [SecopController::class, 'userContract']);
    Route::get('/secop/contract', [SecopController::class, 'contract']);
    Route::get('/secop/counter', [SecopController::class, 'counter']);
    Route::get('/secop/stats', [SecopController::class, 'stats']);
    Route::get('/academic-level', [CareerController::class, 'levels']);
    Route::get('/academic-level/{level}/careers', [CareerController::class, 'index']);
    // Route::get('/sample', [PeaceAndSafeController::class, 'sample']);
    Route::get('/sample/hidden/{token}', [PeaceAndSafeController::class, 'custom']);
    Route::post('/peace-and-save', [PeaceAndSafeController::class, 'index']);
    Route::post('/peace-and-save-download', [PeaceAndSafeController::class, 'download']);
    Route::post('/warehouse-peace-and-save', [PeaceAndSafeController::class, 'wareHouse']);
    Route::post('/warehouse-peace-and-save-download', [PeaceAndSafeController::class, 'wareHouseDownload']);
    Route::post('/warehouse-peace-and-save/notification', [PeaceAndSafeController::class, 'sendWareHouseNotification']);
    Route::post('/warehouse-peace-and-save/validate-code/{code}', [PeaceAndSafeController::class, 'validateCode']);
    Route::post('/warehouse-peace-and-save/excel', [PeaceAndSafeController::class, 'excelWare']);
    Route::post('/warehouse-peace-and-save/more-data', [PeaceAndSafeController::class, 'paginateData']);
    Route::get('/peace-and-save/{token}', [PeaceAndSafeController::class, 'show']);
    Route::post('/generate-certificate', [PeaceAndSafeController::class, 'validation']);
    Route::get('/enable-ldap/{username}-{ous?}', [PeaceAndSafeController::class, 'enableLDAP']);
    Route::get('/disable-ldap/{username}', [PeaceAndSafeController::class, 'disableLDAPManual']);
    Route::post('/oracle', [UserController::class, 'oracle']);
    Route::post('/oracle-count', [UserController::class, 'countOracle']);
    Route::post('/oracle-excel', [UserController::class, 'excelOracle']);
    Route::post('/login', [UserController::class, 'login'])->name('contractors.login');
    Route::get('/users', [AdminController::class, 'index'])->middleware('auth:api');
    Route::get('/excel', [ContractorController::class, 'excel'])->middleware('auth:api');
    Route::get('/roles', [AdminController::class, 'roles'])->middleware('auth:api');
    Route::post('/roles/{user}/user', [AdminController::class, 'store'])->middleware('auth:api');
    Route::delete('/roles/{user}/user', [AdminController::class, 'destroy'])->middleware('auth:api');
    Route::get('/find-users', [AdminController::class, 'findUsers'])->middleware('auth:api');
    Route::get('/menu', [UserController::class, 'drawer'])->middleware('auth:api');
    Route::get('/permissions', [UserController::class, 'permissions'])->middleware('auth:api');
    Route::get('/counter', [ContractorController::class, 'counter'])->middleware('auth:api');
    Route::get('/stats', [ContractorController::class, 'stats']);//->middleware('auth:api');
    Route::get('/contract-types', [ContractTypeController::class, 'index']);
    Route::get('/file-types', [FileTypeController::class, 'index']);
    Route::get('/banks', [BankController::class, 'index']);
    Route::post('/banks-sync', [UserBankController::class, 'sync'])->middleware('auth:api');
    Route::post('/users-seven-banks', [UserBankController::class, 'UsersSevenBank']);
    Route::get('/account-types', [AccountTypeController::class, 'index']);
    Route::post('/find-contractor', [ContractorController::class, 'find'])->middleware('auth:api');
    Route::get('/subdirectorates', [SecopController::class, 'getAreasAndSubdirectorates']);
    Route::put('/subdirectorates', [ContractController::class, 'changeAreaAndSubdirectorate'])->middleware('auth:api');
    Route::get('/payment-status/{document}/{date}', [ContractorController::class, 'getStatusPayment']);
    Route::get('/test/{document}/{contract}', [ComplianceController::class, 'test']);
    Route::get('/security-social', [ComplianceController::class, 'getSecuritySocial']);
    Route::prefix('compliance')->group(function () {
        Route::get('/status', [ComplianceController::class, 'getTimeStatus']);
        Route::get('/contractor', [ComplianceController::class, 'indexContractor'])->middleware('auth:api');
        Route::post('/{contractor}/contractor', [ComplianceController::class, 'init'])->middleware('auth:api');
        Route::put('/reject', [ComplianceController::class, 'compliancesReject'])->middleware('auth:api');
        Route::put('/approved', [ComplianceController::class, 'compliancesApproved'])->middleware('auth:api');
        Route::put('/reviewed', [ComplianceController::class, 'compliancesReviewed'])->middleware('auth:api');
        Route::put('/reviewed-technical', [ComplianceController::class, 'compliancesReviewedTechnical'])->middleware('auth:api');
        Route::get('', [ComplianceController::class, 'index'])->middleware('auth:api');
        Route::get('/search-period', [ComplianceController::class, 'getCompliance'])->middleware('auth:api');
        Route::get('/search-period-status', [ComplianceController::class, 'getComplianceStatus']);
        Route::get('/get-by-payroll-review', [ComplianceController::class, 'getByPayrollReview'])->middleware('auth:api');
        Route::get('/{compliance}', [ComplianceController::class, 'show'])->middleware('auth:api');
        Route::get('/file/{period}/{name}', [ComplianceFileController::class, 'file'])->name('compliance.file.resource');
        Route::post('/search-contractor', [ComplianceController::class, 'searchByContractor']);
        Route::get('/search-contractor/payload', [ComplianceController::class, 'searchByContractorPayload']);
        Route::post('', [ComplianceController::class, 'store']);
        Route::prefix('/status')->group(function () {
            Route::put('', [ComplianceContributionController::class, 'changeRevisionStatus'])
                ->middleware('auth:api')
                ->name('compliance.status.change');
        });
        Route::post('/{compliance}/pdf', [ComplianceController::class, 'getPdf'])->name('compliance.pdf')->middleware('auth:api');
    });

    Route::prefix('payrolls')->group(function () {
        Route::post('/generate-binnacle', [PayrollController::class, 'generateBinnacle'])->middleware('auth:api');
        Route::post('/generate-invoicing', [PayrollController::class, 'generateInvoicing'])->middleware('auth:api');
        Route::post('/binnacle-report', [PayrollController::class, 'getBinnacleReport'])->middleware('auth:api');
        Route::post('/invoicing-report', [PayrollController::class, 'getInvoicingReport'])->middleware('auth:api');
        Route::post('/pac-report', [PayrollController::class, 'getPacReport'])->middleware('auth:api');
        Route::post('/{payroll}/verify', [PayrollController::class, 'verify'])->middleware('auth:api');
    });
    Route::resource('payrolls', PayrollController::class, [
        'only'     =>     ['index', 'store', 'show', 'update'],
        'parameters' =>     ['payrolls' => 'payroll']
    ])->middleware('auth:api');
    Route::prefix('payments-plans')->group(function () {
        Route::get('/approved', [PlanController::class, 'getApproved'])->middleware('auth:api');
        Route::get('/plans-count', [PlanController::class, 'getPlansCount'])->middleware('auth:api');
        Route::get('/plans-status', [PlanController::class, 'getPlansStatus'])->middleware('auth:api');
    });
    Route::get('/user/contract/{payload}', [ContractorController::class, 'user']);
    Route::put('contractors/{contractor}', [ContractorController::class, 'update']);
    Route::get('resend-notification/{contractor}', [ContractorController::class, 'resendNotification'])->middleware('auth:api');
    Route::get('account-bank-notification/{contractor}', [ContractorController::class, 'accountBankNotification'])->middleware('auth:api');
    Route::put('basic-data/{contractor}', [ContractorController::class, 'updateBasicData'])->middleware('auth:api');
    Route::resource('contractors', ContractorController::class, [
        'only'     =>     ['index', 'show', 'store'],
        'parameters' =>     ['contractors' => 'contractor']
    ])->middleware('auth:api');
    Route::prefix('activity-reports')->group(function (){
        Route::get('get-latest/{contract_id}', [ActivityReportController::class, 'getLatest'])->name('getLatest');
        Route::put('send-to-approve/{report}', [ActivityReportController::class, 'sendToApprove'])->middleware('auth:api');
        Route::get('search-period', [ActivityReportController::class, 'getReportByPeriodSupervisor'])->middleware('auth:api');
        Route::post('reject', [ActivityReportController::class, 'rejectReportActivity'])->middleware('auth:api');
        Route::put('approved', [ActivityReportController::class, 'approvedReportActivity'])->middleware('auth:api');
        Route::put('reviewed', [ActivityReportController::class, 'reviewedReportActivity'])->middleware('auth:api');
        Route::put('reviewed-technical', [ActivityReportController::class, 'reviewedTechnicalReportActivity'])->middleware('auth:api');
        Route::get('get-by-report-date/{contract_id}/{report_date}', [ActivityReportController::class, 'getByReportDate'])->middleware('auth:api');
        Route::get('range-dates', [ActivityReportController::class, 'getRangeDates']);
        Route::post('pdf/{report}', [ActivityReportController::class, 'createPDF'])->middleware('auth:api');
        Route::get('reminder', [ActivityReportController::class, 'reminder'])->middleware('auth:api');
        Route::get('reminder_final_report', [ActivityReportController::class, 'reminder_final_report'])->middleware('auth:api');
        Route::get('validate_reports_create', [ActivityReportController::class, 'validate_reports_create'])->middleware('auth:api');
    });
    Route::resource('activity-reports', ActivityReportController::class, [
        'parameters' =>     ['activity-reports' => 'report'],
    ])->middleware('auth:api');
    Route::resource('contributions', ContributionController::class, [
        'parameters' =>     ['contributions' => 'contribution'],
    ])->middleware('auth:api');
    Route::resource('activity-evidences', ActivityEvidenceController::class, [
        'parameters' =>     ['activity-evidences' => 'evidence'],
    ])->middleware('auth:api');
    Route::prefix('activity-evidences')->group(function (){
        Route::post('delete-file/{id}', [ActivityEvidenceController::class, 'deleteFile'])->middleware('auth:api');
    });
    Route::get('contractors/{contractor}/user', [ContractorController::class, 'userContractor'])->middleware('auth:api');
    Route::post('contractors/{contractor}/change-bank', [ContractorController::class, 'updateAccountBank'])->middleware('auth:api');
    Route::post('contractors/{contractor}/personal-data', [ContractorController::class, 'updatePersonalData'])->middleware('auth:api');
    Route::get('storage/file/{file}-{name?}', [FileController::class, 'file'])->name('file.resource');
    Route::get('contractors/rut/{contractor}-{name?}', [ContractorController::class, 'rut'])->name('file.contractors.rut');
    Route::get('contractors/bank/{contractor}-{name?}', [ContractorController::class, 'bank'])->name('file.contractors.bank');
    Route::get('contractors/photo/{contractor}-{name?}', [ContractorController::class, 'photo'])->name('file.contractors.photo');
    Route::put('contractors/third-party/{contractor}', [ContractorController::class, 'thirdParty'])->name('file.contractors.third.party')->middleware('auth:api');
    Route::post('contractors/payment-approved', [ContractorController::class, 'approvedToPayment'])->middleware('auth:api');
    Route::resource('contracts.files', FileController::class, [
        'only'     =>     ['index', 'store', 'destroy'],
        'parameters' =>     ['contracts' => 'contract', 'files' => 'file'],
    ])->middleware('auth:api');
    Route::resource('contractors.contracts', ContractController::class, [
        'only'     =>     ['index', 'store', 'update'],
        'parameters' =>     ['contractors' => 'contractor', 'contracts' => 'contract'],
    ])->middleware('auth:api');
    Route::post('certificado-tributario', [Certificados_TributariosController::class, "index"]);
    Route::post('certificado-tributario/token', [Certificados_TributariosController::class, "validarUsuario"]);
    Route::post('certificado-tributario/oracle', [Certificados_TributariosController::class, "consultaSV"]);
    Route::post('/arl-risk', [ContractController::class, 'arlRisk']);
    Route::get('certificate-contractual', [CertificatesContractualController::class, 'index']);
    Route::post('certificate-contractual/{id}/{check}', [CertificatesContractualController::class, 'generateCertificate']);
    Route::post('certificate-contractual', [CertificatesContractualController::class, 'store']);
    Route::get('economic-activities', [EconomicActivityController::class, 'index']);
    Route::get('third_party/{contractor}', [ThirdPartyController::class, 'show'])->middleware('auth:api');
    Route::post('third_party/{contractor}', [ThirdPartyController::class, 'store'])->middleware('auth:api');
    Route::post('payment-certificate', [PaymentCertificatesController::class, 'index']);
    Route::post('payment-certificate/token', [PaymentCertificatesController::class, 'validateUser']);
    Route::get('embargoes/{contractor}', [EmbargoesController::class, 'show'])->middleware('auth:api');
    Route::put('contractors/embargoes/{contractor}', [EmbargoesController::class, 'embargoed'])->name('contractors.embargoed')->middleware('auth:api');
    Route::post('embargoes/format-excel', [EmbargoesController::class, 'formatFile'])->name('file.embargoes')->middleware('auth:api');
    Route::post('embargoes/upload', [EmbargoesController::class, 'upload'])->middleware('auth:api');
    Route::post('embargoes/{contractor}', [EmbargoesController::class, 'store'])->middleware('auth:api');
    Route::put('embargoes/{contractor}', [EmbargoesController::class, 'update'])->middleware('auth:api');
    Route::delete('embargoes/{embargo}', [EmbargoesController::class, 'delete'])->middleware('auth:api');

    //routes seven - erp
    Route::post('/get-user-seven-list', [SecopController::class, 'getUserSevenBySecop']);
    Route::post('/user-seven-third-party', [ThirdPartyController::class, 'getUserThirdParty']);
    Route::post('/user-seven-third-party-financial', [ThirdPartyController::class, 'getUserThirdPartyFinancial']);
    Route::post('/user-seven-provider', [ThirdPartyController::class, 'getUserProvider']);
    Route::post('/user-seven-provider-detail', [ThirdPartyController::class, 'getUserProviderDetail']);
    Route::post('/activities-economic', [ThirdPartyController::class, 'getActivitiesEconomic']);
    Route::post('/user-seven-payment-certificates', [PaymentCertificatesController::class, 'getUserPayment']);
    Route::post('certificate-payment', [PaymentCertificatesController::class, 'getCertificateSeven']);

    Route::prefix('contracts')->group(function () {
        Route::get('contracts-active', [ContractController::class, 'getContractorForDocumentInit'])->middleware('auth:api');
        Route::post('create-document-init', [ContractController::class, 'createDocumentInit'])->middleware('auth:api');
        Route::get('contracts-with-plane', [ContractController::class, 'getContractWithPlane'])->middleware('auth:api');
        Route::get('plan-payment', [ContractController::class, 'getPlanePayment'])->middleware('auth:api');
        Route::get('get-document-init/{contract_id}', [ContractController::class, 'getDocumentInitByContract'])->middleware('auth:api');
        Route::get('{contract}/object', [ContractController::class, 'getObject'])->middleware('auth:api');
        Route::get('fees', [ContractController::class, 'getFees'])->middleware('auth:api');
        Route::get('sessions', [ContractController::class, 'getSessionContracts'])->middleware('auth:api');
    });

    //rutas supervisor
    Route::prefix('supervisor')->group(function () {
        Route::get('sign', [SupervisorController::class, 'sign'])->middleware('auth:api');
        Route::post('add-sign', [SupervisorController::class, 'addSign'])->middleware('auth:api');
    });

    Route::get('manuals/pdf', [ManualsController::class, 'getPdf']);
    Route::get('test/pdf', [ActivityReportController::class, 'createDomPdf']);
    Route::get('card-institutional/pdf', [CardInstitutionalController::class, 'getCardInst'])->middleware('auth:api');

    Route::prefix('survey')->group(function () {
        Route::get('validate_survey', [SurveyResponseController::class, 'validate_survey'])->middleware('auth:api');
        Route::post('submit_survey', [SurveyResponseController::class, 'store'])->middleware('auth:api');
    });

});
