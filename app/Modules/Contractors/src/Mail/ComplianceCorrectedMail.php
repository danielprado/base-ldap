<?php

namespace App\Modules\Contractors\src\Mail;

use App\Modules\Contractors\src\Models\Compliance;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ComplianceCorrectedMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Compliance
     */
    private $compliance;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Compliance $compliance) {
        $this->compliance = $compliance;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $contract = $this->compliance->contract_number;

        $protocol = 'https';

        $path = config('app.env') != 'production'
            ? "/portal-contratista-dev"
            : "/";

        $subdomain = config('app.env') != 'production' ? "sim" : "portalcontratista";
        $domain = "{$subdomain}.idrd.gov.co";

        if (config('app.env') === 'local') {
            $protocol = 'http';
            $domain = 'localhost:3000';
        }

        $url = "{$protocol}://{$domain}{$path}";

        return $this->view('mail.mail')
            ->subject('Certificado de Cumplimiento Tributario - Corregido')
            ->with([
                'slogan'    => 'Notificación Certificado de Cumplimiento Tributario IDRD',
                'header'    => 'IDRD',
                'title'     => 'Certificado de Cumplimiento Tributario - Portal Contratista',
                'content'   => "¡El certificado de cumplimiento tributario generado para el contrato {$contract} ha sido para corregido! Puede realizar la revisión dando click en el siguiente botón.",
                'details'   => "
                    <div style='text-align: center;'>
                        <p>Fecha de Actualización: {$this->compliance->updated_at}</p>
                    </div>
                ",
                'btn_text'  => 'Revisar',
                'url'  => "{$url}/es/tramites-pagos",
                'year'      => now()->year
            ]);
    }
}
