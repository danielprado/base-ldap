<?php

namespace App\Modules\Contractors\src\Resources;

use App\Modules\Contractors\src\Constants\Roles;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Crypt;
use Carbon\Carbon;
use NumberFormatter;

class ContributionResource extends JsonResource
{

    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => isset($this->id) ? (int) $this->id : null,
            'payroll_number' => isset($this->payroll_number) ? $this->payroll_number : null,
            'payment_date' => isset($this->payment_date) ? $this->payment_date : null,
            'report_id' => isset($this->report_id) ? (int) $this->report_id : null,
            'eps_name' => isset($this->eps_name) ? $this->eps_name : null,
            'eps_value' => isset($this->eps_value) ? $this->getCurrencyFormat((int)$this->eps_value) : null,
            'afp_name' => isset($this->afp_name) ? $this->afp_name : null,
            'afp_value' => isset($this->afp_value) ? $this->getCurrencyFormat((int)$this->afp_value) : null,
            'arl_name' => isset($this->arl_name) ? $this->arl_name : null,
            'arl_value' => isset($this->arl_value) ? $this->getCurrencyFormat((int)$this->arl_value) : null,
            'total' => isset($this->total) ? $this->getCurrencyFormat((int)$this->total) : null,
            'support_file' => isset($this->support_file) ? $this->support_file : null
        ];
    }

    public static function getCurrencyFormat($value) {
        $formatter = new NumberFormatter('es_CO', NumberFormatter::CURRENCY);
        return $formatter->formatCurrency($value, 'COP');
    }

    public static function headers()
    {
        return [
            [
                'text' => "Número Planilla",
                'value'  =>  "payroll_number"
            ],
            [
                'text' => "Fecha Pago",
                'value'  =>  "payment_date"
            ],
            [
                'text' => "Empresa EPS",
                'value'  =>  "eps_name"
            ],
            [
                'text' => "Empresa Pensión",
                'value'  =>  "afp_name"
            ],
            [
                'text' => "Empresa ARL",
                'value'  =>  "arl_name"
            ],
            [
                'text' => "Soporte",
                'value'  =>  "support_file"
            ],
            [
                'text' => "Acciones",
                'value'  =>  "actions"
            ]
        ];
    }

    public static function expanded()
    {
        return [
            [
                'text' => "Número Planilla",
                'value'  =>  "payroll_number"
            ],
            [
                'text' => "Fecha Pago",
                'value'  =>  "payment_date"
            ],
            [
                'text' => "Empresa EPS",
                'value'  =>  "eps_name"
            ],
            [
                'text' => "Aporte EPS",
                'value'  =>  "eps_value"
            ],
            [
                'text' => "Empresa Pensión",
                'value'  =>  "afp_name"
            ],
            [
                'text' => "Aporte Pensión",
                'value'  =>  "afp_value"
            ],
            [
                'text' => "Empresa ARL",
                'value'  =>  "arl_name"
            ],
            [
                'text' => "Aporte ARL",
                'value'  =>  "arl_value"
            ],
            [
                'text' => "Total",
                'value'  =>  "total"
            ]
        ];
    }
}
