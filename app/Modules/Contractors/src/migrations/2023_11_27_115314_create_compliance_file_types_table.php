<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplianceFileTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compliance_file_types', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name', 191);

            $table->string('support_field_name', 191)
                ->comment('Nombre del campo referente a la petición de cumplimiento');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compliance_file_types');
    }
}
