<?php

namespace App\Modules\Contractors\src\Controllers;

use App\Helpers\FPDF;
use App\Http\Controllers\Controller;
use App\Models\Security\User;
use App\Modules\Contractors\src\Constants\Roles;
use App\Modules\Contractors\src\Exceptions\InaccessibleComplianceException;
use App\Modules\Contractors\src\Exceptions\NotStoredFilesException;
use App\Modules\Contractors\src\Jobs\ComplianceCorrectedReminder;
use App\Modules\Contractors\src\Jobs\ComplianceRejectedReminder;
use App\Modules\Contractors\src\Models\ActivityReport;
use App\Modules\Contractors\src\Models\Compliance;
use App\Modules\Contractors\src\Models\ComplianceVerification;
use App\Modules\Contractors\src\Models\ComplianceView;
use App\Modules\Contractors\src\Models\Contract;
use App\Modules\Contractors\src\Models\Contractor;
use App\Modules\Contractors\src\Models\PlanPayment;
use App\Models\Security\Area;
use App\Models\Security\Subarea;
use App\Modules\Contractors\src\Notifications\ComplianceCorrectedNotification;
use App\Modules\Contractors\src\Request\ComplianceContractorRequest;
use App\Modules\Contractors\src\Request\ComplianceInitRequest;
use App\Modules\Contractors\src\Request\RejectCompliancesRequest;
use App\Modules\Contractors\src\Request\SecuritySocialRequest;
use App\Modules\Contractors\src\Request\StoreComplianceContractorRequest;
use App\Modules\Contractors\src\Resources\CompleteComplianceResource;
use App\Modules\Contractors\src\Resources\ComplianceResource;
use App\Modules\Contractors\src\Resources\ComplianceResourceSeven;
use App\Modules\Contractors\src\Resources\ContractResource;
use App\Modules\Contractors\src\Resources\ContractWithAllResource;
use App\Modules\Contractors\src\Resources\SocialSecurityResource;
use App\Modules\Contractors\src\Resources\ComplianceStatusResource;
use DOMDocument;
use GuzzleHttp\Cookie\CookieJar;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;


class ComplianceController extends Controller
{
    public function getTimeStatus(Request $request): JsonResponse
    {
        $time = ComplianceVerification::query()
            ->where('status', '=', 'TIME')
            ->where('code', '=', 'CCT')
            ->first();

        if ($request->has('code')) {
            $time = ComplianceVerification::query()
                ->where('status', '=', 'TIME')
                ->where('code', '=', $request->get('code'))
                ->first();
        }

        return $this->success_message(
            [
                'start_date' => (int) $time->start ?? null,
                'final_date' => (int) $time->end ?? null,
            ]
        );
    }

    public function index(Request $request)
    {
        $certificates = Compliance::select(['compliance.*'])
            ->join('contracts', 'compliance.contract_id', '=', 'contracts.id')
            ->whereIn('compliance.status', [Compliance::STATUS_PENDING, Compliance::STATUS_REJECTED])
            ->when($request->has('document'), function ($q) use ($request) {
                $q->whereHas('contractor', function ($q) use ($request) {
                    $q->where('document', 'like', "%{$request->query('document')}%");
                });
            })
            ->when(!$request->has('document'), function ($q) {
                $user = (object)auth('api')->user();
            })
            ->when($request->has('period'), function ($q) use ($request) {
                $period = explode('-', $request->query('period'), 2);
                $periodMonth = $period[1];
                $periodYear = $period[0];

                $q->where('period_month', $periodMonth)
                    ->where('period_year', $periodYear);
            })
            ->whereRaw('compliance.updated_at > compliance.created_at')
            ->paginate($request->query('per_page', 10));

        return CompleteComplianceResource::collection($certificates);
    }

    public function indexContractor(Request $request)
    {
        $compliance = Compliance::query()
            ->whereHas('contractor', function ($query) {
                $query->where('document', auth('api')->user()->document);
            })
            ->when($request->has('query'), function ($q) use ($request) {
                $data = strtolower($request->get('query'));
                return $q->whereHas('contract', function ($query) use ($data) {
                    return $query->where('contract', 'like', "%{$data}%");
                });
            })
            ->latest()
            ->paginate($this->per_page);

        return $this->success_response(
            CompleteComplianceResource::collection($compliance),
            Response::HTTP_OK,
            [
                'headers'   =>  CompleteComplianceResource::headers(),
                'expanded'  =>  CompleteComplianceResource::additionalData(),
            ]
        );
    }

    public function getByPayrollReview(Request $request)
    {
        try {
            $user = (object)auth('api')->user();
            $isSupervisor = $user->isA(Roles::ROLE_SUPERVISOR);
            $isAdmin = $user->isA(Roles::ROLE_ADMIN);
            $period = explode('-', $request->query('period'), 2);
            $periodMonth = $period[1];
            $periodYear = $period[0];
            // TODO: filtrar unicamente los registros que no tengan planilla se comenta por el momento
            $compliances = ComplianceView::where('status', Compliance::STATUS_APPROVED)
                ->where('period_month', $periodMonth)
                ->where('period_year', $periodYear)
                ->where('payroll_id', null)
                ->get();
            // TODO: devolver vacío para no ir a consultar en SEVEN
            // if($compliances->count() < 1){
            //     return  response()->json([]);
            // }
            $contractorDocumentsList = collect($compliances)->pluck('contractor_document');
            $contractorDocumentsList = $contractorDocumentsList->unique();
            // TODO: revisar como consumen actualmente apis del server
            // TODO: crear resource y devolverla si es necesario
            $http = new Client([
                'base_uri' => env('URL_DOCKER_SERVER'),
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                ],
            ]);
            $response = $http->post("/api/contractors-portal/payroll/get-payroll-seven-list", [
                'json' => [
                    'listDocuments' => $contractorDocumentsList,
                    'validity' => filter_var($request->query('validity'), FILTER_VALIDATE_BOOLEAN),
                ],
            ]);
            $responseData = json_decode($response->getBody()->getContents(), true);


            $array1 = json_decode($compliances, true);
            $result = [];
            foreach ($array1 as $item) {
                $identification = $item['contractor_document'];
                $matchingContract = collect($responseData['data'])->firstWhere('identification', $identification);
                $combinedData = [
                    'id' => $item['compliance_id'],
                    'document' => $item['contractor_document'],
                    'contractor_name' => $item['contractor_name'],
                    'contract_number' => $item['contract_number'],
                    "is_validity" => $matchingContract ? ($matchingContract['is_reserve'] === 'X' ? false : true) : null,
                    "is_seven" => $matchingContract ? true : false,
                    "created_at" => $item['created_at'],
                    "updated_at" => $item['updated_at'],
                ];
                $result[] = $combinedData;
            }

            $collection = collect($result)->all();
            $filteredCompliancesCollection = new Collection($collection);
            return $this->success_response(
                ComplianceResourceSeven::collection($filteredCompliancesCollection)
            );
        } catch (\Throwable $exception) {
            return $this->error_response(
                'No podemos realizar la consulta en este momento, por favor intente más tarde.',
                422,
                $exception->getMessage()
            );
        }
    }

    public function show(Compliance $compliance)
    {
        return CompleteComplianceResource::make($compliance);
    }

    public function searchByContractor(ComplianceContractorRequest $request)
    {
        $validated = $request->validated();
        $contractNumber = format_contract($validated['contract_number'], $validated['contract_year']);

        $compliance = Compliance::query()
            ->whereHas('contractor', function ($q) use ($validated) {
                $q->where('document', $validated['document']);
            })
            ->whereHas('contract', function ($q) use ($contractNumber) {
                $q->where('contract', $contractNumber)
                    ->whereIn('contract_type_id', [Contract::NEW_CONTRACT, Contract::CONTRACT_ASSIGNMENT]);
            })
            ->where('period_month', $validated['period_month'])
            ->where('period_year', $validated['period_year'])
            ->with(['contractor'])
            ->firstOr(function () use ($contractNumber, $validated) {
                if (!now()->betweenIncluded(Compliance::getPeriodStart(), Compliance::getPeriodEnd())) {
                    throw new InaccessibleComplianceException();
                }

                $contract = Contract::where('contract', $contractNumber)
                    ->whereIn('contract_type_id', [Contract::NEW_CONTRACT, Contract::CONTRACT_ASSIGNMENT])
                    ->with(['contractor'])
                    ->first();

                return Compliance::query()->create([
                    'contractor_id' => $contract->contractor->id,
                    'contract_id' => $contract->id,
                    'period_month' => $validated['period_month'],
                    'period_year' => $validated['period_year']
                ]);
            });

        //if (!$compliance->canBeModified()) {
          //  throw new InaccessibleComplianceException();
        //}

        return ComplianceResource::make($compliance);
    }

    public function searchByContractorPayload(Request $request)
    {
        if (!$request->has('payload')) {
            return $this->error_response(
                'No se pudo acceder al certificado tributario seleccionado',
                Response::HTTP_BAD_REQUEST,
                'Ingrese un payload válido.'
            );
        }

        try {
            $json = Crypt::decrypt($request->query('payload'));
            $payload = json_decode($json, true);
        } catch (\Throwable $th) {
            return $this->error_response(
                'Payload inválido',
                Response::HTTP_BAD_REQUEST,
                'El payload ingresado no es válido o no se encuentra en el formato correcto.'
            );
        }

        $compliance = Compliance::query()
            ->with('contract')
            ->whereHas('contractor', function ($q) use ($payload) {
                $q->where('document', $payload['document']);
            })
            ->whereHas('contract', function ($q) use ($payload) {
                $q->where('contract', $payload['contract']);
            })
            ->where('period_month', $payload['period_month'])
            ->where('period_year', $payload['period_year'])
            ->with(['contractor'])
            ->firstOrFail();

        $contract = $compliance->contract;

        return $this->success_response(
            ComplianceResource::make($compliance),
            Response::HTTP_OK,
            [
                'contract' => ContractResource::make($contract),
            ]
        );
    }

    public function init(ComplianceInitRequest $request, Contractor $contractor): JsonResponse
    {
        $contract = Contract::query()->find($request->get('contract_id'));
        $contractReference = extract_number_and_year_from_contract($request->get('contract'));
        $exists = Compliance::query()->where('contract_id', $request->get('contract_id'))->exists();
        $new_compliance = $contractor->compliances()
            ->create($request->all());

        try {
            DB::connection('mysql_contractors')->beginTransaction();
            // Check if month is April
            if (Carbon::now()->month === 4) {
                $new_compliance->modifiable = null;
            } else {
                $new_compliance->modifiable = $exists ? now()->format('Y-m-d H:i:s') : null;
            }
            $new_compliance->contract_number = isset($contract) ? $contract->contract : '';
            $new_compliance->status = 'PENDING';
            $new_compliance->saveOrFail();

            DB::connection('mysql_contractors')->commit();
            return  $this->success_response(
                ComplianceResource::make($new_compliance),
                Response::HTTP_OK,
                [
                    'contract_year'   => $contractReference['contract_year'],
                    'contract_number'   =>  $contractReference['contract_number'],
                    'contract' => ContractResource::make($contract),
                ]
            );
        } catch (\Throwable $e) {
            DB::connection('mysql_contractors')->rollBack();
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }

    public function store(StoreComplianceContractorRequest $request)
    {
        $validated = $request->validated();
        $data = array_filter($validated, function ($key) {
            return !in_array($key, ['contract', 'document']);
        }, ARRAY_FILTER_USE_KEY);

        $compliance = Compliance::query()
            ->whereHas('contractor', function ($q) use ($validated) {
                $q->where('document', $validated['document']);
            })
            ->whereHas('contract', function ($q) use ($validated) {
                $q->where('contract', $validated['contract']);
            })
            ->where('period_month', $validated['period_month'])
            ->where('period_year', $validated['period_year'])
            ->with(['contractor', 'contract'])
            ->firstOrFail();

        // $contract = extract_number_and_year_from_contract($compliance->contract_number);

        try {
            DB::connection('mysql_contractors')->beginTransaction();
            $compliance->files()->delete();
            $compliance->fill($request->validated());
            $compliance->modifiable = null;
            $compliance->dependent_value = $this->getValueDependent($request->get('children_number'));

            foreach ($data as $field => $value) {
                $compliance->$field = $value;
            }

            if ($compliance->isRejected()) {
                $this->dispatch(new ComplianceCorrectedReminder($compliance));

                Notification::send(
                    User::where('email', $compliance->reviewer)->first(),
                    new ComplianceCorrectedNotification($compliance)
                );
            }

            $compliance->status = 'PENDING';
            $compliance->saveOrFail();
            DB::connection('mysql_contractors')->commit();
        } catch (\Throwable $th) {
            DB::connection('mysql_contractors')->rollBack();

            if ($th instanceof InaccessibleComplianceException) throw $th;

            throw new NotStoredFilesException($th->getMessage());
        }

        return $this->success_message(
            __('validation.handler.success'),
            Response::HTTP_CREATED
        );
    }

    public function getValueDependent($value)
    {
        // Si el valor es null o no está definido, retornar 0
        if (is_null($value) || $value === '') {
            return 0;
        }

        switch ($value) {
            case 1:
                return 282390;
            case 2:
                return 564780;
            case 3:
                return 847170;
            default:
                if ($value >= 4) {
                    return 1129560;
                }
                return 0;
        }
    }

    public function getHttpClientSispro(): Client
    {
        return new Client([
            'base_uri' => env('URL_SICOPI_SISPRO'),
            'cookies' => new CookieJar(),
            'headers' => [
                'Accept' => 'application/json',
            ],
            'verify' => false,
        ]);
    }

    public function consultSecuritySocial($http, $request, $document_type)
    {
        $data = collect();
        $http->post("/APCONSULTAS_PILA/PILA/ValidarToken", [
            'multipart' => [
                [
                    'name' => 'Correo_electronico',
                    'contents' => env('URL_SICOPI_SISPRO_EMAIL'),
                ],
                [
                    'name' => 'Nit_entidad',
                    'contents' => env('URL_SICOPI_SISPRO_NIT'),
                ],
                [
                    'name' => 'Token',
                    'contents' => env('URL_SICOPI_SISPRO_TOKEN'),
                ],
            ],
        ]);

        $response = $http->post("/APCONSULTAS_PILA/PILA/ConsultaContratista", [
            'multipart' => [
                [
                    'name' => 'tipoDocumento',
                    'contents' => $document_type,
                ],
                [
                    'name' => 'numDocumento',
                    'contents' => $request->get('document'),
                ],
                [
                    'name' => 'numeroPlanilla',
                    'contents' => $request->get('payroll_number'),
                ],
                [
                    'name' => 'fechaPago',
                    'contents' => $request->get('payment_date'),
                ],
            ],
        ]);

        $html = $response->getBody()->getContents();
        $dom = new DOMDocument();
        libxml_use_internal_errors(true);
        $dom->loadHTML($html);
        libxml_clear_errors();
        $table = $dom->getElementsByTagName('table')->item(0);
        $keys = [
            "name",
            "payment_date",
            "type_contributor",
            "subtype_contributor",
            "operator",
            "period",
            "ibc",
            "eps_value",
            "afp_value",
            "arl_value",
            "contribution_solidarity_value",
            "contribution_subsistence_value"
        ];
        if (!is_null($table)) {
            $headerCells = $table->getElementsByTagName('th');
            foreach ($headerCells as $headerCell) {
                $keys[] = trim($headerCell->textContent);
            }
            foreach ($table->getElementsByTagName('tr') as $rowIndex => $row) {
                if ($rowIndex === 0) {
                    continue;
                }
                $rowData = [];
                $cellIndex = 0;
                foreach ($row->getElementsByTagName('td') as $cell) {
                    $rowData[$keys[$cellIndex]] = $cell->textContent;
                    $cellIndex++;
                }
                $data = $rowData;
            }
            $data['status'] = 'PAGADO';
        }

        return collect($data);
    }

    public function getSecuritySocial(SecuritySocialRequest $request): JsonResponse
    {
        $data = null;
        try {
            $document_type = $this->getDocumentTypeFormat($request->get('document_type_id'));
            $http = $this->getHttpClientSispro();
            $data = $this->consultSecuritySocial($http, $request, $document_type);
        } catch (\Throwable $exception) {
            return $this->error_response(
                'No podemos realizar la consulta en este momento, por favor intente más tarde.',
                500,
                $exception->getMessage()
            );
        }

        if ($request->has('compliance_id')) {
            $compliance = Compliance::query()->find($request->get('compliance_id'));
            if (is_null($compliance)) {
                abort(Response::HTTP_NOT_FOUND, 'No se encontró un certificado con los parámetros establecidos');
            }
            if ($data->isNotEmpty()) {
                $compliance->secop_status = $data['status'];
                $compliance->secop_payment = true;
                $compliance->save();
                return $this->success_message(
                    'El aporte con fecha de pago ha sido encontrado!',
                    Response::HTTP_OK,
                    null,
                    new SocialSecurityResource($data)
                );
            } else {
                $compliance->secop_status = 'NO PAGADO';
                $compliance->secop_payment = false;
                $compliance->save();
                return $this->error_response(
                    __('contractor.not_found_security_social'),
                    Response::HTTP_NOT_FOUND
                );
            }
        }

        if ($data->isNotEmpty()) {
            $report_date = $request->get('report_date');
            if (isset($report_date)) {
                $previous_date = $this->getPreviousMonth($report_date);
                if ($data['period'] !== $previous_date) {
                    abort(Response::HTTP_NOT_FOUND, "El periodo del certificado debe ser {$previous_date}");
                }
            }
            return $this->success_message(
                'El aporte con fecha de pago ha sido encontrado!',
                Response::HTTP_OK,
                null,
                new SocialSecurityResource($data)
            );
        }

        return $this->error_response(
            __('contractor.not_found_security_social'),
            Response::HTTP_NOT_FOUND
        );
    }

    private function getDocumentTypeFormat($document_type_id): ?string
    {
        $formats = [
            1 => 'CC',
            4 => 'CE',
            14 => 'PE',
            15 => 'PT',
            2 => 'TI',
            6 => 'PA',
        ];

        return $formats[$document_type_id] ?? null;
    }

    public function getCompliance(Request $request)
    {
        try {
            $user = auth('api')->user()->load('areas');
            $period = explode('-', $request->query('period'), 2);
            $periodMonth = (int) $period[1];
            $periodYear = (int) $period[0];
            $currentDate = Carbon::now();
            $gracePeriod = 100; // Tiempo adicional para que los supervisores puedan revisar el trámite una vez el contrato haya finalizado
            $extendedDate = $currentDate->copy()->subDays($gracePeriod);

            $contractsWithReportCompliancesQuery = Contract::with([
                'dependency',
                'contractor',
                'activityReports' => function ($query) use ($request) {
                    $query->whereIn('state', [
                        ActivityReport::STATUS_PENDING,
                        ActivityReport::STATUS_APPROVED,
                        ActivityReport::STATUS_REJECTED
                    ]);
                    $query->where('report_date', $request->query('period'));
                },
                'activityReports.evidences',
                'compliances' => function ($query) use ($periodMonth, $periodYear) {
                    $query->whereIn('status', [
                        Compliance::STATUS_PENDING,
                        Compliance::STATUS_APPROVED,
                        Compliance::STATUS_REJECTED
                    ]);
                    $query->where('period_month', $periodMonth);
                    $query->where('period_year', $periodYear);
                },
                'compliances.files',
                'compliances.files.fileType',
                'plans' => function ($query) use ($periodMonth, $periodYear) {
                    $query->whereNotIn('status', [PlanPayment::STATUS_CANCELLED]);
                    $query->where('month', $periodMonth);
                    $query->where('year', $periodYear);
                },
                'plans.pmrs',
                'contract_type',
            ])
            ->whereDate('final_date', '>', $extendedDate)
            ->whereHas('plans', function ($query) use ($periodMonth, $periodYear) {
                $query->whereNotIn('status', [PlanPayment::STATUS_CANCELLED])
                      ->where('month', $periodMonth)
                      ->where('year', $periodYear);
            });

            $contractsWithReportCompliances = collect([]);

            $area_ids = $user->areas()->pluck('areas.id');
            if (!$area_ids->isEmpty()) {
                $contractsWithReportCompliancesQuery->whereIn('dependency_id', $area_ids);
                $contractsWithReportCompliances = $contractsWithReportCompliancesQuery->get();
            }
            $resource = $contractsWithReportCompliances->map(function ($contract) {
                $document = $contract->contractor->document;
                $signature = User::includeExpiredAndTrashed()
                    ->where('document', $document)->value('signature');
                return new ContractWithAllResource($contract, $signature);
            });
            return $this->success_response(ContractWithAllResource::collection($resource));
        } catch (\Throwable $exception) {
            return $this->error_response(
                'No podemos realizar la consulta en este momento, por favor intente más tarde.',
                422,
                $exception->getMessage()
            );
        }
    }

    public function getComplianceStatus(Request $request)
    {
        try {
            $document = $request->query('document');
            $period = explode('-', $request->query('period'), 2);
            $periodMonth = $period[1];
            $periodYear = $period[0];
            $currentDate = Carbon::now();

            $contract = Contract::with([
                'contractor',
                'activityReports' => function ($query) use ($request) {
                    $query->whereIn('state', [ActivityReport::STATUS_PENDING, ActivityReport::STATUS_APPROVED, ActivityReport::STATUS_REJECTED]);
                    $query->where('report_date', $request->query('period'));
                },
                'compliances' => function ($query) use ($periodMonth, $periodYear) {
                    $query->whereIn('status', [Compliance::STATUS_PENDING, Compliance::STATUS_APPROVED, Compliance::STATUS_REJECTED]);
                    $query->where('period_month', $periodMonth);
                    $query->where('period_year', $periodYear);
                },
                'plans' => function ($query) use ($periodMonth, $periodYear) {
                    $query->where('month', $periodMonth);
                    $query->where('year', $periodYear);
                },
                'plans.payroll'
            ])
                //->whereDate('final_date', '>', $currentDate)
                ->whereHas('contractor', function ($query) use ($document) {
                    $query->where('document', $document);
                })
                ->whereHas('plans', function ($query) use ($periodMonth, $periodYear) {
                    $query->where('month', $periodMonth);
                    $query->where('year', $periodYear);
                })
                ->latest()
                ->first();

            if (is_null($contract)) {
                abort(Response::HTTP_NOT_FOUND, 'No se encontró un estado de pago con los parámetros establecidos');
            }

            return $this->success_response(new ComplianceStatusResource($contract));
        } catch (\Throwable $exception) {
            return $this->error_response(
                'No podemos realizar la consulta en este momento, por favor intente más tarde.',
                422,
                $exception->getMessage()
            );
        }
    }

    public function compliancesReject(RejectCompliancesRequest $request)
    {
        try {
            $user = Auth::guard('api')->user();
            $compliance = Compliance::with(['contractor'])->where('id', $request->id)->first();
            if (!$compliance) {
                return $this->error_response(
                    'No podemos realizar la transacción en este momento, por favor intente más tarde.',
                    422,
                    'No existe una certificación de cumplimiento con ese ID'
                );
            }
            $compliance->observations = $request->observations;
            $compliance->reviewer = $user->email;
            $compliance->status = Compliance::STATUS_REJECTED;
            $compliance->save();
            $contractor = $compliance->contractor;
            $this->dispatch(new ComplianceRejectedReminder($contractor, $compliance, $request->get('observations')));
            return $this->success_message(__('validation.handler.success'), Response::HTTP_OK);
        } catch (\Throwable $th) {
            return $this->error_response(
                'No podemos realizar la transacción en este momento, por favor intente más tarde.',
                422,
                $th->getMessage()
            );
        }
    }

    public function compliancesApproved(Request $request)
    {
        try {
            $user = Auth::guard('api')->user();
            $compliance = Compliance::where('id', $request->id)->first();

            if (!$compliance) {
                return $this->error_response(
                    'No podemos realizar la transacción en este momento, por favor intente más tarde.',
                    422,
                    'No existe una certificación de cumplimiento con ese ID'
                );
            }
            $compliance->observations = '';
            $compliance->reviewer = $user->email;
            $compliance->status = Compliance::STATUS_APPROVED;
            $compliance->save();
            return $this->success_message(__('validation.handler.success'), Response::HTTP_OK);
        } catch (\Throwable $th) {
            return $this->error_response(
                'No podemos realizar la transacción en este momento, por favor intente más tarde.',
                422,
                $th->getMessage()
            );
        }
    }

    public function compliancesReviewed(Request $request)
    {
        try {
            $user = Auth::guard('api')->user();
            $compliance = Compliance::where('id', $request->id)->first();

            if (!isset($compliance)) {
                return $this->error_response(
                    'No podemos realizar la transacción en este momento, por favor intente más tarde.',
                    422,
                    'No existe una certificación de cumplimiento con ese ID'
                );
            }
            $compliance->reviewer = $user->email;
            $compliance->reviewed = 1;
            $compliance->name_reviewer = $user->name . ' ' . $user->surname;
            $compliance->save();
            return $this->success_message(__('validation.handler.success'), Response::HTTP_OK);
        } catch (\Throwable $th) {
            return $this->error_response(
                'No podemos realizar la transacción en este momento, por favor intente más tarde.',
                422,
                $th->getMessage()
            );
        }
    }

    public function compliancesReviewedTechnical(Request $request)
    {
        try {
            $user = Auth::guard('api')->user();
            $compliance = Compliance::where('id', $request->id)->first();

            if (!isset($compliance)) {
                return $this->error_response(
                    'No podemos realizar la transacción en este momento, por favor intente más tarde.',
                    422,
                    'No existe una certificación de cumplimiento con ese ID'
                );
            }
            $compliance->reviewed_technical = 1;
            $compliance->name_reviewer_technical = $user->name . ' ' . $user->surname;
            $compliance->save();
            return $this->success_message(__('validation.handler.success'), Response::HTTP_OK);
        } catch (\Throwable $th) {
            return $this->error_response(
                'No podemos realizar la transacción en este momento, por favor intente más tarde.',
                422,
                $th->getMessage()
            );
        }
    }

    public function getPreviousMonth($date)
    {
        $parsedDate = Carbon::createFromFormat('Y-m', $date)->startOfMonth();
        $previousMonth = $parsedDate->subMonth();
        return $previousMonth->format('Y-m');
    }

    public function getPdf(Compliance $compliance)
    {
        $user = User::includeExpiredAndTrashed()
            ->where('document', $compliance->document)->first();
        $pdf = new FPDF('L', 'mm', 'Letter');

        $pdf->SetStyle("p", "Helvetica", "N", 10, "0,0,0", 0);
        $pdf->SetStyle("small", "Helvetica", "N", 8, "0,0,0", 0);
        $pdf->SetStyle("h1", "Helvetica", "B", 14, "0,0,0", 0);
        $pdf->SetStyle("a", "Helvetica", "BU", 9, "0,0,0", 15);
        $pdf->SetStyle("pers", "Helvetica", "I", 0, "0,0,0");
        $pdf->SetStyle("place", "Helvetica", "U", 0, "0,0,0");
        $pdf->SetStyle("b", "Helvetica", "B", 0, "0,0,0");
        $pdf->SetStyle("u", "Helvetica", "U", 0, "0,0,0");

        $pdf->AddPage();
        // set the source file
        $pdf->setSourceFile(storage_path("app/templates/CERTIFICADO_CUMPLIMIENTO_TRIBUTARIO.pdf"));
        $tplId = $pdf->importPage(1);
        $pdf->useTemplate($tplId, 0, 0, null, null, true);

        // Creation date and time
        $created_at = isset($compliance->updated_at) ? $compliance->updated_at->format('Y-m-d') : null;
        $pdf->SetFont('Helvetica', 'B');
        $pdf->SetFontSize(8);
        $pdf->SetTextColor(0, 0, 0);
        $pdf->SetXY(15, 38);
        $pdf->Cell(190, 6, utf8_decode('Ciudad y Fecha: ' . 'BOGOTÁ ' . $created_at), 0, 0, 'R');

        $this->createHeader($pdf, $compliance);

        list($textWidth) = $this->getLettersItems($compliance, $pdf);

        $pdf->SetTextColor(0, 0, 255);
        $pdf->SetFont("Helvetica", "B", 10);
        $pdf->MultiCell($textWidth, '4', utf8_decode('Responda las preguntas 1 al 10 solo si respondió NO a la pregunta A'), 0, 'J');
        $pdf->SetTextColor(0);
        $pdf->Ln(8);

        $compliance = $this->getNumbersItems($compliance, $pdf, $tplId);

        // Contractor Signature
        if ($user->signature !== null) {
            $signaturePath = storage_path('app/user/' . $user->signature);
            $jpgSignaturePath = storage_path('app/user/' . pathinfo($user->signature, PATHINFO_FILENAME) . '.jpg');
            if ($this->convertToJPG($signaturePath, $jpgSignaturePath)) {
                $pdf->Image($jpgSignaturePath, 30, 215, 40);
            }
        }

        $pdf->SetFont("Helvetica", "", 10);
        $pdf->SetXY(15, 190);
        $pdf->MultiCell(0, 6, utf8_decode('Nota: Me comprometo a informar cualquier novedad relacionada con lo aquí manifestado, presentando nuevamente el certificado, junto con los soportes a que haya lugar.'));

        $pdf->SetXY(15, 225);
        $pdf->Cell(0, 4, utf8_decode('Firma: ____________________________'));

        $name = $compliance->name . ' ' . $compliance->surname;
        $document = $compliance->document;
        $pdf->SetXY(15, 230);
        $pdf->Cell(0, 4, utf8_decode("Nombre: $name"));

        $pdf->SetXY(15, 235);
        $pdf->Cell(0, 4, utf8_decode("Cédula: $document"));

        $pdf->SetY(-45);
        $pdf->SetFont('Helvetica', '', 8);
        $pdf->SetXY(15, 240);
        $pdf->MultiCell(0, 4, utf8_decode('La información tributaria determinada con base en lo aquí certificado, surtirá efecto a partir del primer pago o abono en cuenta posterior a la fecha de entrega. '));

        $pdf->AliasNbPages();

        $pdf->SetY(-31); // Posiciona 15 mm desde el borde inferior
        $pdf->Cell(0, 10, utf8_decode("Página " . $pdf->PageNo() . ' de {nb}'), 0, 0, 'C');

        $month = Carbon::create()->month($compliance->period_month)->monthName;
        $period = toUpper($month . "_" . "$compliance->period_year");
        $file_name = "CERTIFICADO_CUMPLIMIENTO_TRIBUTARIO_" . $compliance->document . "_" . $period . ".pdf";
        $file = $pdf->Output('S', $file_name, true);

        return $this->success_message([
            'file' => 'data:application/pdf;base64,' . base64_encode($file),
            'file_name' => $file_name
        ]);
    }

    public function convertToJPG($inputFile, $outputFile) {
        $info = getimagesize($inputFile);
        $mime = $info['mime'];
        switch ($mime) {
            case 'image/jpeg':
                $image = imagecreatefromjpeg($inputFile);
                break;
            case 'image/png':
                $image = imagecreatefrompng($inputFile);
                break;
            case 'image/gif':
                $image = imagecreatefromgif($inputFile);
                break;
            default:
                return false;
        }
        if ($image === false) {
            return false;
        }
        imagejpeg($image, $outputFile, 90);
        imagedestroy($image);
        return true;
    }

    public function createHeaderText($compliance, $text = null)
    {
        $contract = extract_number_and_year_from_contract($compliance->contract_number);
        $number = $contract['contract_number'];
        $year = $contract['contract_year'];
        $contractor = $compliance->contractor;
        $document_type = $contractor->document_type->name;

        $text = "<p>De conformidad con las normas citadas en la referencia, y con el fin de suministrarla información necesaria para el ";
        $text .= "cálculo de mi base de retención en la fuente aplicable por los ingresos obtenidos en ejecución del contrato de <u>prestación de servicios</u> ";
        $text .= "<u>$number</u> de <u>$year</u>, suscrito entre el IDRD y <u>$contractor->full_name</u> identificado con <u>$document_type ";
        $text .= "$contractor->document</u>, <b>bajo la gravedad de juramento certifico que: </b></p>";

        return $text;
    }

    /**
     * @param FPDF $pdf
     * @param Compliance $compliance
     * @return void
     */
    public function createHeader(FPDF $pdf, Compliance $compliance): void
    {
        $pdf->SetFont('Helvetica', '', 10);
        $pdf->SetXY(15, 44);
        $pdf->Cell(160, 6, utf8_decode('Señores'), 0, 1, 'L');

        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(15, 49);
        $pdf->Cell(160, 6, utf8_decode('INSTITUTO DISTRITAL PARA LA RECREACION Y EL DEPORTE'), 0, 1, 'L');

        $pdf->SetFont('Helvetica', 'B', 10);
        $pdf->SetXY(15, 53);
        $pdf->Cell(160, 6, utf8_decode('Atn. Area Financiera'), 0, 1, 'L');

        $pdf->SetFont("Helvetica", "B", 10);
        $pdf->SetXY(15, 60);
        $pdf->Cell(30, 6, utf8_decode('Referencia: '), 0, 0, 'L');
        $pdf->SetFont("Helvetica", "", 10);
        $pdf->SetXY(35, 61);
        $pdf->MultiCell(0, 4, utf8_decode('Certificado de Información Tributaria para Depuración de la Base del cálculo Retención en La Fuente, Art. 383, 387, 388 Estatuto Tributario (Art. 1.2.4.1.6. y siguientes del Decreto 1625 De 2016), por rentas de trabajo que no provienen de una relación laboral o legal y reglamentaria. (ley 2277 de 2022 y Decreto 2231 de 2023).'));

        $textoPrincipal = $this->createHeaderText($compliance);
        $pdf->SetFont("Helvetica", "", 10);
        $pdf->SetXY(15, 80);
        $pdf->WriteTag(0, 4, utf8_decode($textoPrincipal), 0, 'J');
        $pdf->Ln(5);
    }

    /**
     * @param Compliance $compliance
     * @param FPDF $pdf
     * @return array
     */
    public function getLettersItems(Compliance $compliance, FPDF $pdf): array
    {
        $letters = array(
            array(
                'id' => 'A.',
                'name' => 'De conformidad con el numeral 6 del artículo 1.2.4.1.6. y con el párrafo 4 artículo 1.2.4.1.17. del Decreto 1625 de 2016, tomaré costos y/o deducciones asociados a las rentas percibidas por mi contrato.',
                'value' => $compliance['retention'],
                'subtitle' => 'Si marca (SI), se aplicará la tarifa de retención en la fuente del artículo 392 del E.T (entre el 4% y 11%).     Si marca (NO), se aplicará la tarifa de retención en la fuente del artículo 383.'
            ),
            array(
                'id' => 'B.',
                'name' => 'Soy Pensionado (a)',
                'value' => $compliance['pensioner'],
                'subtitle' => 'Requiere adjuntar certificación de pensión'
            ),
            array(
                'id' => 'C.',
                'name' => 'Soy declarante de renta',
                'value' => $compliance['income_tax_filer']
            ),
            array(
                'id' => 'D.',
                'name' => 'Soy responsable de Iva',
                'value' => $compliance['responsible_iva']
            ),
            array(
                'id' => 'E.',
                'name' => 'Solicito realizar una retención en la fuente adicional a la calculada, por valor de: ',
                'value' => $compliance['withholding_value'] != null ? cop_money_format($compliance['withholding_value'], '$', null, '0') : 0
            ),
        );

        $x = $pdf->GetX();
        $y = $pdf->GetY();
        $width = 195;
        $lineHeight = 6;

        foreach ($letters as $letter) {
            $pdf->SetXY($x + 6, $pdf->GetY() + 0.8);
            $pdf->SetFont("Helvetica", "B", 10);
            $pdf->Cell(7, 4, $letter['id'], 0, 0, 'L');

            $checkbox_y = $pdf->GetY();

            $textWidth = ($width * 0.72);
            $textLines = ceil($pdf->GetStringWidth($letter['name']) / $textWidth);
            $textHeight = $textLines * $lineHeight;

            $pdf->MultiCell($textWidth, '4', utf8_decode($letter['name']), 0, 'J');

            if (isset($letter['subtitle'])) {
                $pdf->SetFont('Helvetica', '', 9);
                $pdf->SetXY($x + 11, $pdf->GetY() + 1);
                $pdf->MultiCell($textWidth + 12, $lineHeight - 2, utf8_decode($letter['subtitle']), 0);
            }

            if ($letter['id'] === 'E.') {
                $pdf->SetFont('Helvetica', '', 10);
                $withholding_value = $letter['value'];
                $pdf->SetXY($textWidth + 22, $pdf->GetY() - 5); // Ajustar posición para el valor
                $pdf->Cell(22, 6, $withholding_value, 1, 0, 'R'); // Dibujar un rectángulo con el valor dinámico
            } else {
                // Casillas de selección para "SI" y "NO"
                $pdf->SetFont('Helvetica', 'B', 10);
                $checkbox_x_si = $x + ($width * 0.7) + 18; // Posición X para "SI"
                $checkbox_x_no = $x + ($width * 0.8) + 18; // Posición X para "NO"
                $checkbox_center_y = $checkbox_y + ($textHeight / 2); // Centrar verticalmente las casillas
                $pdf->SetXY($checkbox_x_si, $checkbox_center_y - 4);
                $pdf->MultiCell(($width * 0.1) - 5, $lineHeight, utf8_decode('SI'), 0, 'C');
                $pdf->SetXY($checkbox_x_no, $checkbox_center_y - 4);
                $pdf->MultiCell(($width * 0.1) - 5, $lineHeight, utf8_decode('NO'), 0, 'C');

                // Recuadros para "SI" y "NO"
                $pdf->SetXY($checkbox_x_si + 12, $checkbox_center_y - 4); // Ajuste de la coordenada X y Y para "SI"
                $pdf->Cell(5, 5, ($letter['value'] ? 'X' : ''), 1, 0, 'C'); // Cuadrito para "SI" o vacío
                $pdf->SetXY($checkbox_x_no + 12, $checkbox_center_y - 4); // Ajuste de la coordenada X y Y para "NO"
                $pdf->Cell(5, 5, ($letter['value'] ? '' : 'X'), 1, 0, 'C'); // Cuadrito para "NO" o vacío
            }

            $pdf->Ln($textHeight + 4);
        }
        return array($textWidth);
    }

    /**
     * @param Compliance $compliance
     * @param FPDF $pdf
     * @param string $tplId
     * @return Compliance
     */
    public function getNumbersItems(Compliance $compliance, FPDF $pdf, string $tplId): Compliance
    {
        $symbol = chr(149);
        $mapUvts = [
            2023 => '$11,027.120',
            2024 =>'$12,236.900',
            2025 =>'$12,947.740'
        ];
        $numbers = array(
            array(
                'id' => '1.',
                'name' => 'Durante el año ' . ($compliance->period_year - 1) . ' realicé pago de intereses por préstamos para adquisición de vivienda (incluido Leasing habitacional)',
                'value' => $compliance['housing_interests'],
            ),
            array(
                'id' => '2.',
                'name' => 'En caso de responder afirmativamente la pregunta 1, por favor adjuntar la certificación bancaria y completar la siguiente información:',
                'subitems' => [
                    array(
                        'id' => $symbol,
                        'number' => 1,
                        'name' => 'Terceras personas con las que adquirí el inmueble hacen uso del beneficio tributario',
                        'value' => $compliance['third_people'],
                    ),
                    array(
                        'id' => $symbol,
                        'number' => 2,
                        'name' => 'Mi conyugue hace USO del beneficio tributario',
                        'value' => $compliance['spouse_tax'],
                    ),
                    array(
                        'id' => $symbol,
                        'number' => 3,
                        'name' => 'Por lo anterior, Declaro que la deducción prevista se debe realizar proporcionalmente (Decreto 3750 de 1986 Articulo 8). A mi favor',
                        'value' => isset($compliance['percentage']) ? $compliance['percentage'] : 0,
                        'subtitle' => 'Bajo la gravedad de juramento declaro que el inmueble por el cual estoy pasando la disminución cumple con los requisitos establecidos en la ley para acceder a este beneficio'
                    ),
                ]
            ),
            array(
                'id' => '3.',
                'name' => 'Durante el año ' . ($compliance->period_year - 1) . ' realicé pagos por salud (medicina prepagada - seguros de salud)',
                'value' => $compliance['prepaid_medicine'],
                'subtitle' => 'Anexar certificado de la entidad donde se evidencie el valor pagado y el periodo.'
            ),
            array(
                'id' => '4.',
                'name' => 'Para el presente pago realicé aporte a pensiones voluntarios',
                'value' => $compliance['afp_voluntary'],
                'subtitle' => 'Anexar copia del pago.'
            ),
            array(
                'id' => '5.',
                'name' => 'Para el presente pago realicé aporte a AFC',
                'value' => $compliance['afc_voluntary'],
                'subtitle' => 'Anexar copia de la consiganción.'
            ),
            array(
                'id' => '6.',
                'name' => 'Certifico que tengo algunos de los siguientes dependientes',
                'value' => $compliance['has_children'],
            ),
            array(
                'id' => '7.',
                'name' => 'Número de dependientes',
                'value' => isset($compliance['children_number']) ? $compliance['children_number'] : 0,
                'subitems' => [
                    array(
                        'id' => $symbol,
                        'number' => 1,
                        'name' => 'Hijos menores de 18 años',
                        'value' => $compliance['children_minor'],
                        'subtitle' => 'Requiere registro civil.'
                    ),
                    array(
                        'id' => $symbol,
                        'number' => 2,
                        'name' => 'Hijos entre los 18 y 23 años a quienes se les esté dando educación',
                        'value' => $compliance['children_mayor_students'],
                        'subtitle' => 'Requiere anexar certificación semestral de pago de matricula expedida por la respectiva entidad educativa.'
                    ),
                    array(
                        'id' => $symbol,
                        'number' => 3,
                        'name' => 'Hijos de cualquier edad que se encuentren en situación de dependencia por discapacidad ',
                        'value' => $compliance['children_disabilities'],
                        'subtitle' => 'Requiere adjuntar certtificado de Medicina Legal o de EPS sobre situación de discapacidad o certificado de contador público si es dependiente por ingresos'
                    ),
                    array(
                        'id' => $symbol,
                        'number' => 4,
                        'name' => 'Cónyuge o compañero permanente en situación de dependencia por ausencia de ingresos o ingresos anuales inferiores a 260 UVT (' . ($compliance->period_year - 1) . ') ' . $mapUvts[$compliance->period_year - 1] . ' anuales, o por discapacidad',
                        'value' => $compliance['spouse'],
                        'subtitle' => 'Requiere adjuntar certtificado de Medicina Legal o de EPS sobre situación de discapacidad o certificado de contador público si es dependiente por ingresos'
                    ),
                    array(
                        'id' => $symbol,
                        'number' => 5,
                        'name' => 'Padres y hermanos en situación de dependencia por ausencia de ingresos o por ingresos inferiores a 260 UVT (' . ($compliance->period_year - 1) .') ' . $mapUvts[$compliance->period_year - 1] . ' anuales, o por discapacidad',
                        'value' => $compliance['parents_dependency'],
                        'subtitle' => 'Requiere adjuntar certtificado de Medicina Legal o de EPS sobre situación de discapacidad o certificado de contador público si es dependiente por ingresos'
                    ),
                ]
            ),
        );

        $x = $pdf->GetX();
        $y = $pdf->GetY() + 2;
        $width = 195;
        $lineHeight = 6;

        foreach ($numbers as $item) {
            $pdf->SetXY($x + 6, $pdf->GetY() - 2);
            $pdf->SetFont("Helvetica", 'B', 10);
            $pdf->Cell(7, 4, $item['id'], 0, 0, 'L');

            $checkbox_y = $pdf->GetY();

            $textWidth = ($width * 0.72);
            $textLines = ceil($pdf->GetStringWidth($item['name']) / $textWidth);
            $textHeight = $textLines * $lineHeight;

            $pdf->SetFont("Helvetica", '', 10);
            $pdf->MultiCell($textWidth, '4', utf8_decode($item['name']), 0, 'J');

            if (isset($item['subtitle'])) {
                $pdf->SetFont('Helvetica', 'B', 9);
                $pdf->SetXY($x + 15, $pdf->GetY() + 1);
                $pdf->MultiCell($textWidth + 28, $lineHeight - 1, utf8_decode($item['subtitle']), 0);
            }

            if ($item['id'] === '7.') {
                $pdf->SetFont('Helvetica', '', 10);
                $children_number = $item['value'];
                $pdf->SetXY($textWidth + 29, $pdf->GetY() - 4);
                $pdf->Cell(12, 6, $children_number, 1, 0, 'C');
                //$pdf->Ln($textHeight + 4);
                if ($item['id'] === '7.' && isset($item['subitems'])) {
                    foreach ($item['subitems'] as $subitem) {
                        // Sangría para el subítem
                        $pdf->SetFont("Helvetica", '', 10);
                        $pdf->SetXY($x + 10, $pdf->GetY() + 6);
                        $pdf->Cell(4, $lineHeight, $subitem['id'], 0, 0, 'L');

                        $checkbox_y = $pdf->GetY();

                        $textWidth = ($width * 0.72);
                        $textLines = ceil($pdf->GetStringWidth($subitem['name']) / $textWidth);
                        $textHeight = $textLines * $lineHeight;

                        $pdf->MultiCell($textWidth, $lineHeight - 2, utf8_decode($subitem['name']), 0, 'J');

                        if (isset($subitem['subtitle'])) {
                            $pdf->SetFont("Helvetica", 'B', 9);
                            $pdf->SetXY($x + 15, $pdf->GetY() + 0.5);
                            $pdf->MultiCell($textWidth + 5, $lineHeight - 2, utf8_decode($subitem['subtitle']), 0);
                            //$pdf->Ln(2);
                        }

                        // Casillas de selección para "SI" y "NO"
                        $pdf->SetFont('Helvetica', 'B', 10);
                        $checkbox_x_si = $x + ($width * 0.7) + 16;
                        $checkbox_x_no = $x + ($width * 0.8) + 16;
                        $checkbox_center_y = $checkbox_y + ($textHeight / 2);
                        $pdf->SetXY($checkbox_x_si, $checkbox_center_y - 1);
                        $pdf->MultiCell(($width * 0.1), $lineHeight, utf8_decode('SI'), 0, 'C');
                        $pdf->SetXY($checkbox_x_no, $checkbox_center_y - 1);
                        $pdf->MultiCell(($width * 0.1), $lineHeight, utf8_decode('NO'), 0, 'C');

                        // Recuadros para "SI" y "NO"
                        $pdf->SetXY($checkbox_x_si + 14, $checkbox_center_y);
                        $pdf->Cell(5, 4, ($subitem['value'] ? 'X' : ''), 1, 0, 'C');
                        $pdf->SetXY($checkbox_x_no + 14, $checkbox_center_y);
                        $pdf->Cell(5, 4, ($subitem['value'] ? '' : 'X'), 1, 0, 'C');
                        $pdf->Ln(4);
                    }
                }
            } elseif (isset($item['subitems'])) {
                foreach ($item['subitems'] as $subitem) {
                    // Sangría para el subítem
                    $pdf->SetFont("Helvetica", '', 10);
                    $pdf->SetXY($x + 10, $pdf->GetY() + 2);
                    $pdf->Cell(4, $lineHeight, $subitem['id'], 0, 0, 'L');

                    $checkbox_y = $pdf->GetY();

                    $textWidth = ($width * 0.72);
                    $textLines = ceil($pdf->GetStringWidth($item['name']) / $textWidth);
                    $textHeight = $textLines * $lineHeight;

                    //$pdf->SetFont("Helvetica", '', 10);
                    $pdf->MultiCell($textWidth, 4, utf8_decode($subitem['name']), 0, 'J');

                    if (isset($subitem['subtitle'])) {
                        $pdf->SetFont("Helvetica", 'B', 9);
                        $pdf->SetXY($x + 11, $pdf->GetY() + 2);
                        $pdf->MultiCell($textWidth + 28, $lineHeight - 1, utf8_decode($subitem['subtitle']), 0);
                    }

                    if ($subitem['number'] === 3) {
                        $pdf->SetFont('Helvetica', '', 10);
                        $percentage = $subitem['value'];
                        $pdf->SetXY($textWidth + 26, $pdf->GetY() - 20);
                        $pdf->Cell(12, 6, '% ' . $percentage . '       (% de 1 a 100)', 1, 0, 'L');
                        $pdf->Ln($textHeight + 4);
                    } elseif ($subitem['number'] === 3) {
                        $pdf->SetFont('Helvetica', '', 10);
                        $children_number = $subitem['value'];
                        $pdf->SetXY($textWidth + 26, $pdf->GetY() - 20);
                        $pdf->Cell(12, 6, $children_number . '     máximo 4', 1, 0, 'L');
                        $pdf->Ln($textHeight + 4);
                    } else {
                        // Casillas de selección para "SI" y "NO" con formato del ítem 1
                        $pdf->SetFont('Helvetica', 'B', 10);
                        $checkbox_x_si = $x + ($width * 0.7) + 16;
                        $checkbox_x_no = $x + ($width * 0.8) + 16;
                        $checkbox_center_y = $pdf->GetY() + ($lineHeight / 2);
                        $pdf->SetXY($checkbox_x_si, $checkbox_center_y - 8);
                        $pdf->MultiCell(($width * 0.1), $lineHeight, utf8_decode('SI'), 0, 'C');
                        $pdf->SetXY($checkbox_x_no, $checkbox_center_y - 8);
                        $pdf->MultiCell(($width * 0.1), $lineHeight, utf8_decode('NO'), 0, 'C');
                        // Recuadros para "SI" y "NO" con formato del ítem 1s
                        $pdf->SetXY($checkbox_x_si + 14, $checkbox_center_y - 7.5);
                        $pdf->Cell(5, 5, ($subitem['value'] ? 'X' : ''), 1, 0, 'C');
                        $pdf->SetXY($checkbox_x_no + 14, $checkbox_center_y - 7.5);
                        $pdf->Cell(5, 5, ($subitem['value'] ? '' : 'X'), 1, 0, 'C');
                        $pdf->Ln(8);
                    }
                    //$pdf->Ln(2);
                }

                $pdf->AddPage();
                $pdf->useTemplate($tplId, 0, 0, null, null, true);
                //$tplId = $pdf->importPage(2);
                $pdf->Ln(25);
            } else {
                // Casillas de selección para "SI" y "NO"
                $pdf->SetFont('Helvetica', 'B', 10);
                $checkbox_x_si = $x + ($width * 0.7) + 18;
                $checkbox_x_no = $x + ($width * 0.8) + 18;
                $checkbox_center_y = $checkbox_y + ($textHeight / 2);
                $pdf->SetXY($checkbox_x_si, $checkbox_center_y - 6);
                $pdf->MultiCell(($width * 0.1) - 5, $lineHeight + 1, utf8_decode('SI'), 0, 'C');
                $pdf->SetXY($checkbox_x_no, $checkbox_center_y - 6);
                $pdf->MultiCell(($width * 0.1) - 5, $lineHeight + 1, utf8_decode('NO'), 0, 'C');
                // Recuadros para "SI" y "NO"
                $pdf->SetXY($checkbox_x_si + 12, $checkbox_center_y - 5);
                $pdf->Cell(5, 4, ($item['value'] ? 'X' : ''), 1, 0, 'C');
                $pdf->SetXY($checkbox_x_no + 12, $checkbox_center_y - 5);
                $pdf->Cell(5, 4, ($item['value'] ? '' : 'X'), 1, 0, 'C');
                $pdf->Ln(4);
            }
            $pdf->Ln($textHeight + 4);
        }
        return $compliance;
    }
}
