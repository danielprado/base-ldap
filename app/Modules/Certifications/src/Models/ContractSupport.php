<?php

namespace App\Modules\Certifications\src\Models;

use App\Models\Security\DocumentType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class ContractSupport extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'mysql_certificacion_contratos';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'soporte';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Tipo_Documento_Solicitante_Id',
        'Nombre_Solicitante',
        'Documento_Solicitante',
        'Correo_Solicitante',
        'Descripcion_Solicitud',
        'Estado',
        'Solucion',
    ];

    /*
     * ---------------------------------------------------------
     * Query Scopes
     * ---------------------------------------------------------
    */
    public static function scopeUnProcessedByYear($query)
    {
        //$year = now()->year;
        return $query->where('Estado', '=', 1)
            //->whereYear('created_at', $year)
            ->count();
    }

    public function scopeProcessedByYear($query)
    {
        //$year = now()->year;
        return $query->where('Estado', '=', 2)
            //->whereYear('updated_at', $year)
            ->count();
    }

    /*
     * ---------------------------------------------------------
     * Eloquent Relations
     * ---------------------------------------------------------
    */

    /**
     * @return BelongsTo
     */
    public function document_type()
    {
        return $this->belongsTo(DocumentType::class, 'Tipo_Documento_Solicitante_Id');
    }

}
