<?php

namespace App\Modules\SportsPerformance\src\Models;

use Illuminate\Database\Eloquent\Model;

class AthleteClassification extends Model
{
     /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_sim_rendimiento_deportivo';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'clasificacion_deportista';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['Nombre_Clasificacion_Deportista'];

}
