<?php


namespace App\Modules\CitizenPortal\src\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\CitizenPortal\src\Constants\Roles;
use App\Modules\CitizenPortal\src\Jobs\ConfirmStatusFileCitizen;
use App\Modules\CitizenPortal\src\Models\CitizenSchedule;
use App\Modules\CitizenPortal\src\Models\File;
use App\Modules\CitizenPortal\src\Models\FileType;
use App\Modules\CitizenPortal\src\Models\Observation;
use App\Modules\CitizenPortal\src\Models\Profile;
use App\Modules\CitizenPortal\src\Models\ProfileView;
use App\Modules\CitizenPortal\src\Models\Status;
use App\Modules\CitizenPortal\src\Notifications\FileStatusNotification;
use App\Modules\CitizenPortal\src\Request\FileStatusRequest;
use App\Modules\CitizenPortal\src\Resources\FileResource;
use Exception;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class FileController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
        /*
        $this->middleware(Roles::actions(File::class, 'status'))
            ->only('update');
        $this->middleware(Roles::actions(File::class, 'destroy'))
            ->only('destroy');
        */
    }

    /**
     * @return JsonResponse
     */
    public function index(Profile $profile)
    {
        return $this->success_response(
            FileResource::collection(File::where('profile_id', $profile->getKey())->latest()->paginate($this->per_page))
        );
    }

    /**
     * @param $profile
     * @param File $file
     * @return JsonResponse
     * @throws FileNotFoundException
     */
    public function show(Profile $profile, File $file)
    {
        try {
            $data = [];
            if ($file->file_type_id == FileType::IDENTIFICATION_DOCUMENT) {
                $profile_type = isset($profile->profile_type_id) ? (int) $profile->profile_type_id : null;
                $user_id = isset($profile->user_id) ? (int) $profile->user_id : null;
                $principal = [];
                if ($profile_type != Profile::PROFILE_PERSONAL) {
                    $personal = Profile::query()
                        ->where("user_id", $user_id)
                        ->where("profile_type_id", Profile::PROFILE_PERSONAL)
                        ->first();
                    $principal = [
                        'id'        =>  isset($personal->id) ? (int) $personal->id : null,
                        "name" => $personal->full_name ?? null,
                        'document_type'      =>  isset($personal->document_type->name) ? (string) $personal->document_type->name : null,
                        'document'      =>  isset($personal->document) ? (int) $personal->document : null,
                        'sex'      =>  isset($personal->sex_name->name) ? (string) $personal->sex_name->name : null,
                    ];
                }
                $data = [
                    'id'        =>  isset($profile->id) ? (int) $profile->id : null,
                    'user_id'      =>  $user_id,
                    'relationship'      =>  isset($profile->relationship) ? (string) $profile->relationship : null,
                    'profile_type_id'      =>  $profile_type,
                    'profile_type'      =>  isset($profile->profile_type->name) ? (string) $profile->profile_type->name : null,
                    "name" => $profile->full_name ?? null,
                    'document_type'      =>  isset($profile->document_type->name) ? (string) $profile->document_type->name : null,
                    'document'      =>  isset($profile->document) ? (int) $profile->document : null,
                    'sex'      =>  isset($profile->sex_name->name) ? (string) $profile->sex_name->name : null,
                    'blood_type'      =>  isset($profile->blood_type_name->name) ? (string) $profile->blood_type_name->name : null,
                    'birthdate'      =>  isset($profile->birthdate) ? $profile->birthdate->format('Y-m-d') : null,
                    'country_birth'      =>  isset($profile->country_birth->name) ? (string) $profile->country_birth->name : null,
                    'state_birth'      =>  isset($profile->state_birth->name) ? (string) $profile->state_birth->name : null,
                    'city_birth'      =>  isset($profile->city_birth->name) ? (string) $profile->city_birth->name : null,
                    "parent" => $principal
                ];
            }
            if ( Storage::disk('citizen_portal')->exists($file->file) ) {
                $selected = Storage::disk('citizen_portal')->get($file->file);
                $mime = Storage::disk('citizen_portal')->mimeType($file->file);
                return $this->success_message([
                    "profile" => $profile,
                    "additional" => $data,
                    'name'  => $file->file,
                    'mime'  => $mime,
                    'file'   => 'data:'.$mime.';base64,'.base64_encode($selected)
                ]);
            } elseif (Storage::disk('local')->exists('templates/NOT_FOUND.pdf')) {
                $selected = Storage::disk('local')->get('templates/NOT_FOUND.pdf');
                $mime = Storage::disk('local')->mimeType('templates/NOT_FOUND.pdf');
                return $this->success_message([
                    "profile" => $profile,
                    "additional" => $data,
                    'name'  => $file->file,
                    'mime'  => $mime,
                    'file'   => 'data:'.$mime.';base64,'.base64_encode($selected)
                ]);
            } else {
                return $this->error_response(
                    __('validation.handler.resource_not_found'),
                    Response::HTTP_NOT_FOUND
                );
            }
        } catch (Exception $exception) {
            if (Storage::disk('local')->exists('templates/NOT_FOUND.pdf')) {
                $selected = Storage::disk('local')->get('templates/NOT_FOUND.pdf');
                $mime = Storage::disk('local')->mimeType('templates/NOT_FOUND.pdf');
                return $this->success_message([
                    "error" => $exception->getMessage(),
                    "additional" => [],
                    'name'  => $file->file,
                    'mime'  => $mime,
                    'file'   => 'data:'.$mime.';base64,'.base64_encode($selected)
                ]);
            } else {
                return $this->error_response(
                    __('validation.handler.resource_not_found'),
                    Response::HTTP_NOT_FOUND
                );
            }
        }
    }

    /**
     * @param FileStatusRequest $request
     * @param ProfileView $profile
     * @param File $file
     * @throws \Throwable
     */
    public function update(FileStatusRequest $request, Profile $profile, File $file)
    {
        DB::connection('mysql_citizen_portal')->transaction(function () use ($request, $profile, $file) {
            $is_verified = in_array($request->get('status_id'), [Status::FILE_VERIFIED, Status::VERIFIED]);
            $file->status_id = $request->get('status_id');
            $file->verified_at = $is_verified ? now() : null;
            $file->save();
            $status = Status::find($request->get('status_id'));
            $file_type = isset( $file->file_type->name ) ? (string) $file->file_type->name : null;
            $status_name = isset( $status->name ) ? (string) $status->name : null;

            $observation = toFirstUpper("El archivo $file_type tiene el estado $status_name. {$request->get('observation')}");

            $file->profile->observations()->create([
                'observation'   => $observation,
                'user_ldap_id'       =>  auth('api')->user()->id,
            ]);

            if ($is_verified && $file->file_type_id == FileType::IDENTIFICATION_DOCUMENT) {
                $profile->verified_at = now();
                $profile->status_id = Status::VERIFIED;
                $profile->save();
            }

            $this->dispatch(new ConfirmStatusFileCitizen(
                $profile,
                $status,
                $file,
                $observation,
                auth('api')->user()->email
            ));
            $profile->user->notify(new FileStatusNotification(
                $profile,
                $status,
                $file,
                $observation,
                auth('api')->user()->full_name
            ));
        });
        return $this->success_message(
            __('validation.handler.success')
        );
    }

    /**
     * @param $profile
     * @param File $file
     * @return JsonResponse
     */
    public function destroy($profile, File $file)
    {
        try {
            if (Storage::disk('citizen_portal')->exists($file->file) ) {
                if (config('app.env') == 'production') {
                    Storage::disk('citizen_portal')->delete($file->file);
                }
            }
            if (config('app.env') == 'production') {
                $file->delete();
            }
            return $this->success_message(
                __('validation.handler.deleted'),
                Response::HTTP_OK,
                Response::HTTP_NO_CONTENT
            );
        } catch (Exception $exception) {
            return $this->error_response(
                __('validation.handler.service_unavailable')
            );
        }
    }
}
