<?php


namespace App\Modules\Contractors\src\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\Contractors\src\Models\AccountType;
use App\Modules\Contractors\src\Models\Contractor;
use App\Modules\Contractors\src\Models\ContractType;
use App\Modules\Contractors\src\Resources\AccountTypeResource;
use App\Modules\Contractors\src\Resources\ContractorResource;
use App\Modules\Contractors\src\Resources\ContractTypeResource;
use Illuminate\Http\JsonResponse;

class ManualsController extends Controller
{
    public function getPdf()
    {
        $filePath = storage_path("app/public/manuals/portal-contratista.pdf");
        if (!file_exists($filePath)) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_NOT_FOUND,
                'Archivo no encontrado'
            );
        }
        $pdfContent = file_get_contents($filePath);
        $base64Pdf = base64_encode($pdfContent);
        return $this->success_message([
            "file_name" => 'Guia Paso a Paso Contratista V1_26072024.pdf',
            "file" => "data:application/pdf;base64," . $base64Pdf
        ]);
    }
    
}
