<?php

namespace App\Modules\Contractors\src\Jobs;

use App\Modules\Contractors\src\Mail\ComplianceVerificationMail;
use App\Modules\Contractors\src\Models\Compliance;
use App\Modules\Contractors\src\Models\Contractor;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use SendGrid\Mail\Mail;

class ComplianceVerificationCode implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Contractor
     */
    private $user;

    /**
     * @var Compliance
     */
    private $compliance;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Contractor $user, Compliance $compliance)
    {
        $this->user = $user;
        $this->compliance = $compliance;

        if ($this->compliance->verification_code) return;
        $this->setCode();
    }

    private function setCode()
    {
        $digits = 6;
        $code = rand(pow(10, $digits-1), pow(10, $digits)-1);
        $this->compliance->createNewVerification($code);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(Mail $mailer)
    {
        $email = config('app.env') == 'local'
            ? env('SAMPLE_EMAIL', 'andres.tellez@idrd.gov.co')
            : (isset($this->user->email) ? $this->user->email : null);

        if (isset($email) && filter_var($email, FILTER_VALIDATE_EMAIL)) {
            $mailer->setFrom(
                env('MAIL_CONTRACTS_FROM', 'portal.contratista@idrd.gov.co'),
                env('MAIL_CONTRACTS_FROM_NAME', 'Portal Contratista')
            );
            $mailer->setSubject('Certificado de Cumplimiento Tributario - Código de Verificación');
            $mailer->addTo($email);
    
            $render = (new ComplianceVerificationMail($this->user, $this->compliance))
                ->render();
    
            $mailer->addContent("text/html", $render);
            $sendgrid = new \SendGrid(config('mail.sendgrid.api'));
    
            try {
                $response = $sendgrid->send($mailer);
                sendgird_logger(
                    $email,
                    "Certificado de Cumplimiento Tributario - Código de Verificación",
                    $render,
                    [],
                    $response->statusCode(),
                    $response->body()
                );
            } catch (\Exception $e) {
                sendgird_logger(
                    $email,
                    "[EXCEPTION] Certificado de Cumplimiento Tributario - Código de Verificación",
                    $render,
                    [],
                    $e->getCode(),
                    $e->getMessage()
                );
            }
        }
    }
}
