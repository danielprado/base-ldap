<?php

namespace App\Modules\Certifications\src\Models;

use App\Models\Security\DocumentType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


class CertificationMember extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'mysql_certificacion_contratos';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'integrante';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Contrato_Id',
        'Nombre_Integrante',
        'Tipo_Documento_Integrante_Id',
        'Documento_Integrante',
        'Porcentaje_Integrante',
        'Conteo'
    ];

    /**
     * @return BelongsTo
     */
    public function document_type()
    {
        return $this->belongsTo(DocumentType::class, 'Tipo_Documento_Integrante_Id');
    }
}
