<?php


namespace App\Modules\CitizenPortal\src\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\CitizenPortal\src\Constants\Roles;
use App\Modules\CitizenPortal\src\Models\Activity;
use App\Modules\CitizenPortal\src\Models\CitizenSchedule;
use App\Modules\CitizenPortal\src\Models\Modality;
use App\Modules\CitizenPortal\src\Models\Schedule;
use App\Modules\CitizenPortal\src\Request\ActivityRequest;
use App\Modules\CitizenPortal\src\Request\ModalityRequest;
use App\Modules\CitizenPortal\src\Resources\ActivityResource;
use App\Modules\CitizenPortal\src\Resources\ModalityResource;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ModalityController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        $query = $this->setQuery(Modality::query(), (new Modality)->getSortableColumn($this->column))
            ->when(isset($this->query), function ($query) {
                return $query->where('name', 'like', "%$this->query%");
            })
            ->orderBy((new Modality)->getSortableColumn($this->column), $this->order);
        return $this->success_response(
            ModalityResource::collection(
                (int) $this->per_page > 0
                    ? $query->paginate( $this->per_page )
                    : $query->get()
            ),
            Response::HTTP_OK,
            [
                'headers'   => ModalityResource::headers()
            ]
        );
    }

    /**
     * @param ModalityRequest $request
     * @return JsonResponse
     */
    public function store(ModalityRequest $request)
    {
        $model = new Modality();
        $model->fill($request->validated());
        $model->save();
        return $this->success_message(
            __('validation.handler.success'),
            Response::HTTP_CREATED
        );
    }

    /**
     * @param ModalityRequest $request
     * @param Modality $modality
     * @return JsonResponse
     */
    public function update(ModalityRequest $request, Modality $modality)
    {
        $modality->fill($request->validated());
        $modality->save();
        return $this->success_message(
            __('validation.handler.updated')
        );
    }

    /**
     * @param Modality $modality
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(Modality $modality)
    {
        $modality->delete();
        return $this->success_message(
            __('validation.handler.deleted'),
            Response::HTTP_OK,
            Response::HTTP_NO_CONTENT
        );
    }
}
