<?php

namespace App\Modules\CitizenPortal\src\Models;

use App\Models\Security\BloodType;
use App\Models\Security\CityLDAP;
use App\Models\Security\CountryLDAP;
use App\Models\Security\Disability;
use App\Models\Security\DocumentType;
use App\Models\Security\Eps;
use App\Models\Security\EpsSim;
use App\Models\Security\EthnicGroup;
use App\Models\Security\GenderIdentity;
use App\Models\Security\PopulationGroup;
use App\Models\Security\Sex;
use App\Models\Security\SexualOrientation;
use App\Models\Security\StateLDAP;
use App\Models\Security\User;
use App\Modules\Parks\src\Models\Location;
use App\Modules\Parks\src\Models\Neighborhood;
use App\Modules\Parks\src\Models\Upz;
use App\Traits\FullTextSearch;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable;

class Profile extends Model implements Auditable
{
    use \OwenIt\Auditing\Auditable, FullTextSearch;

    // Relationship
    const RELATIONSHIP = [
        [
            'id'    =>  3,
            'name'  =>  'PADRE - MADRE',
        ],
        [
            'id'    =>  7,
            'name'  =>  'TUTOR LEGAL',
        ],
    ];

    const PROFILE_PERSONAL = 1; // Id de la tabla profile_types
    const PROFILE_BENEFICIARY = 2; // Id de la tabla profile_types

    // Ids de la tabla status "estados"

    // Validate Profile
    const PENDING = 1;
    const VALIDATING = 2;
    const VERIFIED = 3;
    const RETURNED = 4;

    // Validate Subscription
    const PENDING_SUBSCRIBE = 5;
    const SUBSCRIBED = 6;
    const UNSUBSCRIBED = 7;

    // Document
    const FILE_PENDING = 8;
    const FILE_VERIFIED = 9;
    const FILE_RETURNED = 10;

    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'mysql_citizen_portal';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'profiles';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id',
        'profile_type_id',
        'document_type_id',
        'document',
        'name',
        's_name',
        'surname',
        's_surname',
        'sex',
        'blood_type',
        'birthdate',
        'country_birth_id',
        'state_birth_id',
        'city_birth_id',
        'country_residence_id',
        'state_residence_id',
        'city_residence_id',
        'locality_id',
        'upz_id',
        'neighborhood_id',
        'other_neighborhood_name',
        'address',
        'stratum',
        'ethnic_group_id',
        'population_group_id',
        'gender_id',
        'sexual_orientation_id',
        'has_disability',
        'disability_id',
        'contact_name',
        'contact_phone',
        'contact_relationship',
    ];

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = [ 'verified_at', 'assigned_by_id', 'assigned_at', 'checker_id', 'status_id' ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [ 'verified_at', 'assigned_at', 'birthdate' ];

    /**
     * The columns of the full text index
     *
     * @var array
     */
    protected $searchable = [
        'document',
        'name',
        'surname',
    ];

    /*
    * ---------------------------------------------------------
    * Data Change Auditor
    * ---------------------------------------------------------
    */

    /**
     * Attributes to include in the Audit.
     *
     * @var array
     */
    protected $auditInclude = [
        'user_id',
        'profile_type_id',
        'document_type_id',
        'document',
        'name',
        'surname',
        'sex',
        'blood_type',
        'birthdate',
        'country_birth_id',
        'state_birth_id',
        'city_birth_id',
        'country_residence_id',
        'state_residence_id',
        'city_residence_id',
        'locality_id',
        'upz_id',
        'neighborhood_id',
        'other_neighborhood_name',
        'address',
        'stratum',
        'ethnic_group_id',
        'population_group_id',
        'gender_id',
        'sexual_orientation_id',
        'has_disability',
        'disability_id',
        'contact_name',
        'contact_phone',
        'contact_relationship',
        'verified_at',
        'assigned_by_id',
        'assigned_at',
        'checker_id',
        'status_id'
    ];

    /**
     * Generating tags for each model audited.
     *
     * @return array
     */
    public function generateTags(): array
    {
        return ['citizen_portal_profiles'];
    }

    /*
     * ---------------------------------------------------------
     * Accessors and Mutator
     * ---------------------------------------------------------
     */

    /**
     * @return mixed|string|null
     */
    public function getFullNameAttribute()
    {
        return toUpper( "{$this->name} {$this->s_name} {$this->surname} {$this->s_surname}" );
    }

    /**
     * @return mixed|string|null
     */
    public function getEmailFullNameAttribute()
    {
        return toTitle( $this->name )." ".toTitle($this->surname);
    }

    /**
     * @return string|null
     */
    public function getContactRelationshipNameAttribute()
    {
        $data = collect( Profile::RELATIONSHIP )->firstWhere('id', '=', (int) $this->contact_relationship);
        return isset($data->name) ? (string) $data->name : null;
    }

    /**
     * @param $column
     * @return string
     */
    public function getSortableColumn($column)
    {
        switch ($column) {
            case 'id':
            case 'created_at':
            case 'updated_at':
                return $column;
            case "status":
                return "status_id";
            case "profile_type":
                return "profile_type_id";
            case "document_type":
                return "document_type_id";
            default:
                return in_array($column, $this->fillable)
                    ? $column
                    : $this->primaryKey;
        }
    }

    /*
     * ---------------------------------------------------------
     * Eloquent Relationship
     * ---------------------------------------------------------
     */


    public function observations()
    {
        return $this->hasMany(Observation::class, 'profile_id', 'id')->latest();
    }

    public function files()
    {
        return $this->hasMany(File::class, 'profile_id', 'id')->latest();
    }

    public function user_schedules()
    {
        return $this->hasMany(CitizenSchedule::class, 'profile_id', 'id');
    }

    public function assigned()
    {
        return $this->hasOne( User::class, 'id', 'assigned_by_id' );
    }

    public function viewer()
    {
        return $this->hasOne( User::class, 'id', 'checker_id' );
    }

    public function status()
    {
        return $this->hasOne( Status::class, 'id', 'status_id' );
    }

    public function user()
    {
        return $this->belongsTo(Citizen::class, 'user_id', 'id');
    }

    public function profile_type()
    {
        return $this->belongsTo(ProfileType::class, 'profile_type_id', 'id');
    }

    public function document_type()
    {
        return $this->hasOne(DocumentType::class, 'Id_TipoDocumento', 'document_type_id');
    }

    public function sex_name()
    {
        return $this->hasOne(Sex::class, 'Id_Genero', 'sex');
    }

    public function blood_type_name()
    {
        return $this->hasOne(BloodType::class, 'Id_GrupoSanguineo', 'blood_type');
    }

    public function country_birth()
    {
        return $this->hasOne(CountryLDAP::class, 'id', 'country_birth_id');
    }

    public function state_birth()
    {
        return $this->hasOne(StateLDAP::class, 'id', 'state_birth_id');
    }

    public function city_birth()
    {
        return $this->hasOne(CityLDAP::class, 'id', 'city_birth_id');
    }

    public function country_residence()
    {
        return $this->hasOne(CountryLDAP::class, 'id', 'country_residence_id');
    }

    public function state_residence()
    {
        return $this->hasOne(StateLDAP::class, 'id', 'state_residence_id');
    }

    public function city_residence()
    {
        return $this->hasOne(CityLDAP::class, 'id', 'city_residence_id');
    }

    public function locality()
    {
        return $this->hasOne(Location::class, 'Id_Localidad', 'locality_id');
    }

    public function upz()
    {
        return $this->hasOne(Upz::class, 'cod_upz', 'upz_id');
    }

    public function neighborhood()
    {
        return $this->hasOne(Neighborhood::class, 'IdBarrio', 'neighborhood_id');
    }

    public function ethnic_group()
    {
        return $this->hasOne(EthnicGroup::class, 'Id_Etnia', 'ethnic_group_id');
    }

    public function population_group()
    {
        return $this->hasOne(PopulationGroup::class, 'id', 'population_group_id');
    }

    public function gender()
    {
        return $this->hasOne(GenderIdentity::class, 'id', 'gender_id');
    }

    public function sexual_orientation()
    {
        return $this->hasOne(SexualOrientation::class, 'id', 'sexual_orientation_id');
    }

    public function eps()
    {
        return $this->hasOne(EpsSim::class, 'Id_Eps', 'eps_id');
    }

    public function disability()
    {
        return $this->hasOne(Disability::class, 'id', 'disability_id');
    }
}
