<?php

namespace App\Helpers;

class Mapper 
{
  private $origin;
  private $destination;

  private function __construct($origin) {
    $this->origin = $origin;
  }

  public static function map($origin)
  {
    return new Mapper($origin);
  }

  public function to($destination)
  {
    $this->destination = $destination;
    return $this;
  }

  public function with(Mappable $mapper)
  {
    $this->destination = array_merge(
      $this->destination, 
      $mapper::map($this->origin)
    );

    return $this->destination;
  }
}

interface Mappable {
  public static function map($origin); 
}