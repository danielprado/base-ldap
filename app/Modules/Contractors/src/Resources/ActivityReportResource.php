<?php

namespace App\Modules\Contractors\src\Resources;

use App\Modules\Contractors\src\Constants\Roles;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Crypt;
use App\Models\Security\User;
use App\Modules\Contractors\src\Resources\ContributionResource;
use App\Modules\Contractors\src\Resources\ActivityReportEvidenceResource;
use Carbon\Carbon;

class ActivityReportResource extends JsonResource
{

    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $user = User::where('document', $this->contractor->document)->first();
        return [
            'id' => isset($this->id) ? (int) $this->id : null,
            'contractor_id' => isset($this->contractor_id) ? (int) $this->contractor_id : null,
            'contract_id' => isset($this->contract_id) ? (int) $this->contract_id : null,
            'contract_number' => isset($this->contract_number) ? $this->contract_number : null,
            'virtual_expedient_number' => isset($this->virtual_expedient_number) ? $this->virtual_expedient_number : null,
            'report_number' => isset($this->report_number) ? $this->report_number : null,
            'report_date' => isset($this->report_date) ? $this->report_date : null,
            'report_date_fmt' => isset($this->report_date) ? $this->getReportDateFormat($this->report_date) : null,
            'activity_period' => isset($this->activity_period) ? explode(',', $this->activity_period) : null,
            'payment_number' => isset($this->payment_number) ? $this->payment_number : null,
            'payment_date' => isset($this->payment_date) ? $this->payment_date : null,
            'eps_name' => isset($this->eps_name) ? $this->eps_name : null,
            'eps_value' => isset($this->eps_value) ? (int)$this->eps_value : null,
            'afp_name' => isset($this->afp_name) ? $this->afp_name : null,
            'afp_value' => isset($this->afp_value) ? (int)$this->afp_value : null,
            'risks_name' => isset($this->risks_name) ? $this->risks_name : null,
            'risks_value' => isset($this->risks_value) ? (int)$this->risks_value : null,
            'contributions_total' => isset($this->contributions_total) ? (int)$this->contributions_total : null,
            'state' => isset($this->state) ? $this->state : null,
            'evidences' => ActivityReportEvidenceResource::collection($this->whenLoaded('evidences')),
            'contributions' => ContributionResource::collection($this->whenLoaded('contributions')),
            'observations' => isset($this->observations) ? $this->observations : null,
            'attachments_path' => isset($this->attachments_path) ? $this->attachments_path : null,
            'contractor_signature_path' => isset($user->signature) ? $user->signature : null,
            'is_new_contractor' => isset($this->is_new_contractor) ? (bool)$this->is_new_contractor : null,
            'reviewed' => isset($this->reviewed) ? (bool)$this->reviewed : null,
            'reviewed_technical' => isset($this->reviewed_technical) ? (bool)$this->reviewed_technical : null,
            "created_at"  => isset($this->created_at) ? $this->created_at->format('Y-m-d H:i:s') : null,
            "updated_at"  => isset($this->updated_at) ? $this->updated_at->format('Y-m-d H:i:s') : null,
            'social_security_attachment_path' => isset($this->social_security_attachment_path) ? $this->social_security_attachment_path : null,
            'peaceandsafe_path' => $this->peaceandsafe_path ?? null,
            'peaceandsafe_warehouse_path' => $this->peaceandsafe_warehouse_path ?? null
        ];
    }

    public static function headers()
    {
        return [
            [
                'text' => "Informe No.",
                'value'  =>  "report_number"
            ],
            [
                'text' => "Mes del Informe",
                'value'  =>  "report_date_fmt"
            ],
            [
                'text' => "Contrato",
                'value'  =>  "contract_number"
            ],
            [
                'text' => "Estado",
                'value'  =>  "state"
            ],
            [
                'text' => "Acciones",
                'value'  =>  "actions"
            ]
        ];
    }

    public function getReportDateFormat($report_date) {
        $date = Carbon::createFromFormat('Y-m', $report_date);
        $month_name = $date->translatedFormat('F');
        return $date->format('Y') . '-' . $month_name;
    }
}
