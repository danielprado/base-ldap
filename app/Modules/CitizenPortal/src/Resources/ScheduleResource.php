<?php


namespace App\Modules\CitizenPortal\src\Resources;


use App\Modules\CitizenPortal\src\Constants\Roles;
use App\Modules\CitizenPortal\src\Models\Schedule;
use App\Modules\CitizenPortal\src\Models\TeamSchedule;
use App\Modules\CitizenPortal\src\Models\ParticipantRole;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\DB;

class ScheduleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        =>  isset($this->id) ? (int) $this->id : null,
            'program_id'      =>  isset($this->program_id) ? (int) $this->program_id : null,
            'program_name'      =>  isset($this->program_name) ? (string) $this->program_name : null,
            'activity_id'      =>  isset($this->activity_id) ? (int) $this->activity_id : null,
            'activity_name'      =>  isset($this->activity_name) ? (string) $this->activity_name : null,
            'stage_id'      =>  isset($this->stage_id) ? (int) $this->stage_id : null,
            'stage_name'      =>  isset($this->stage_name) ? (string) $this->stage_name : null,
            'park_id'      =>  isset($this->park_id) ? (int) $this->park_id : null,
            'park_code'      =>  isset($this->park_code) ? (string) $this->park_code : null,
            'park_name'      =>  isset($this->park_name) ? (string) $this->park_name : null,
            'park_address'      =>  isset($this->park_address) ? (string) $this->park_address : null,
            'weekday_id'      =>  isset($this->weekday_id) ? (int) $this->weekday_id : null,
            'weekday_name'      =>  isset($this->weekday_name) ? (string) $this->weekday_name : null,
            'daily_id'      =>  isset($this->daily_id) ? (int) $this->daily_id : null,
            'daily_name'      =>  isset($this->daily_name) ? (string) $this->daily_name : null,
            'min_age'      =>  isset($this->min_age) ? (int) $this->min_age : null,
            'max_age'      =>  isset($this->max_age) ? (int) $this->max_age : null,
            'quota'      =>  isset($this->quota) ? (int) $this->quota : null,
            'is_paid'      =>  isset($this->is_paid) ? (bool) $this->is_paid : null,
            'rate_id'      =>  isset($this->rate_id) ? (int) $this->rate_id : null,
            'rate_name'      =>  isset($this->rate_name) ? (string) $this->rate_name : null,
            'rate_value'      =>  isset($this->rate_value) ? (string) $this->rate_value : null,
            'is_initiate'      =>  isset($this->is_initiate) ? (bool) $this->is_initiate : null,
            'start_date'      =>  isset($this->start_date) ? $this->start_date->format('Y-m-d H:i:s') : null,
            'final_date'      =>  isset($this->final_date) ? $this->final_date->format('Y-m-d H:i:s') : null,
            'is_activated'      =>  isset($this->is_activated) ? (bool) $this->is_activated : null,
            'disability'      =>  isset($this->disability) ? (bool) $this->disability : null,
            'is_teams'      =>  isset($this->is_teams) ? (bool) $this->is_teams : null,
            'min_team_participants_quota'      =>  isset($this->min_team_participants_quota) ? (int) $this->min_team_participants_quota : 0,
            'max_team_participants_quota'      =>  isset($this->max_team_participants_quota) ? (int) $this->max_team_participants_quota : 0,
            'users_schedules_count'   => (bool) $this->is_teams ? $this->getTotalUserTeams() : (isset($this->users_schedules_count) ? (int) $this->users_schedules_count : 0),
            'teams_schedules_count'   => isset($this->teams_schedules_count) ? (int) $this->teams_schedules_count : 0,
            'created_at'    =>  isset($this->created_at) ? $this->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'    =>  isset($this->updated_at) ? $this->updated_at->format('Y-m-d H:i:s') : null,
            'audits'       => [],
            'project_id' => isset($this->project_id) ? (int) $this->project_id : null,
            'activity_type_id' => isset($this->activity_type_id) ? (int) $this->activity_type_id : null,
            'set_population_params' => isset($this->set_population_params) ? (bool) $this->set_population_params : null,
            'sex_id' => isset($this->sex_id) ? array_map('intval', explode(',', $this->sex_id)) : [],
            'orientation_id' => isset($this->orientation_id) ? array_map('intval', explode(',', $this->orientation_id)) : [],
            'gender_id' => isset($this->gender_id) ? array_map('intval', explode(',', $this->gender_id)) : [],
            'ethnic_group_id' => isset($this->ethnic_group_id) ? array_map('intval', explode(',', $this->ethnic_group_id)) : [],
            'population_group_id' => isset($this->population_group_id) ? array_map('intval', explode(',', $this->population_group_id)) : [],
            'disability_id' => isset($this->disability_id) ? array_map('intval', explode(',', $this->disability_id)) : [],
            'men_participants_num' => isset($this->men_participants_num) ? (int) $this->men_participants_num : null,
            'women_participants_num' => isset($this->women_participants_num) ? (int) $this->women_participants_num : null,
            'staff_participants_num' => isset($this->staff_participants_num) ? (int) $this->staff_participants_num : null,
            'project_name' => isset($this->project_name) ? $this->project_name : null,
            'activity_type_name' => isset($this->activity_type_name) ? $this->activity_type_name : null,
            'quota_info' => isset($this->quota) ? $this->taken . ' de ' . $this->quota : null
        ];
    }

    private function getTotalUserTeams() {
        $teamSchedules = TeamSchedule::where('schedule_id', $this->id)
            ->with('team.participants')
            ->get();
        $totalParticipants = 0;
        foreach ($teamSchedules as $teamSchedule) {
            if ($teamSchedule->team) {
                $totalParticipants += $teamSchedule->team->participants->where('role_id', ParticipantRole::PARTICIPANT)->count();
            }
        }
        return $totalParticipants;
    }    

    public static function headers()
    {
        return [
            'headers'   => [
                [
                    'align' => "right",
                    'text' => "#",
                    'value'  =>  "id",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.activity')),
                    'value'  =>  "activity_name",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.disability')),
                    'value'  =>  "disability",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.min_age')),
                    'value'  =>  "min_age",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.max_age')),
                    'value'  =>  "max_age",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.quota')),
                    'value'  =>  "quota_info",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.users_schedules_count')),
                    'value'  =>  "users_schedules_count",
                    'sortable' => false
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.is_paid')),
                    'value'  =>  "is_paid",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.is_activated')),
                    'value'  =>  "is_activated",
                    'sortable' => true
                ],
                [
                    'align' => "right",
                    'text' => Str::ucfirst(__('citizen.validations.actions')),
                    'value'  =>  "actions",
                    'sortable' => false
                ],
            ],
            'expanded'  => [
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.weekday')),
                    'value'  =>  "weekday_name",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.daily')),
                    'value'  =>  "daily_name",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.is_initiate')),
                    'value'  =>  "is_initiate",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.start_date')),
                    'value'  =>  "start_date",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.final_date')),
                    'value'  =>  "final_date",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.program')),
                    'value'  =>  "program_name",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.stage')),
                    'value'  =>  "stage_name",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.park_code')),
                    'value'  =>  "park_code",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.park')),
                    'value'  =>  "park_name",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.address')),
                    'value'  =>  "park_address",
                    'sortable' => true
                ],
                [
                    'align' => "right",
                    'text' => Str::ucfirst(__('citizen.validations.created_at')),
                    'value'  =>  "created_at",
                    'sortable' => true
                ],
                [
                    'align' => "right",
                    'text' => Str::ucfirst(__('citizen.validations.updated_at')),
                    'value'  =>  "updated_at",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.project_name')),
                    'value'  =>  "project_name",
                    'sortable' => true
                ],
                [
                    'align' => "left",
                    'text' => Str::ucfirst(__('citizen.validations.activity_type_name')),
                    'value'  =>  "activity_type_name",
                    'sortable' => true
                ]
            ],
        ];
    }
}
