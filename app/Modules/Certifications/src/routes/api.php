<?php

use App\Modules\Certifications\src\Controllers\AdditionController;
use App\Modules\Certifications\src\Controllers\AdminController;
use App\Modules\Certifications\src\Controllers\AssignmentController;
use App\Modules\Certifications\src\Controllers\CertificationController;
use App\Modules\Certifications\src\Controllers\ContractController;
use App\Modules\Certifications\src\Controllers\ContractTypeController;
use App\Modules\Certifications\src\Controllers\ExtensionController;
use App\Modules\Certifications\src\Controllers\MemberController;
use App\Modules\Certifications\src\Controllers\ObligationController;
use App\Modules\Certifications\src\Controllers\SupportController;
use App\Modules\Certifications\src\Controllers\SuspensionController;
use App\Modules\Certifications\src\Controllers\UserController;
use Illuminate\Support\Facades\Route;

Route::prefix('certifications')->group(function () {
    Route::get('/stats', [SupportController::class, 'stats']);
    Route::get('/excel', [ContractController::class, 'excel']);

    Route::middleware('auth:api')->group(function () {
        Route::get('/roles', [AdminController::class, 'roles']);
        Route::post('/roles/{user}/user', [AdminController::class, 'store']);
        Route::delete('/roles/{user}/user', [AdminController::class, 'destroy']);
        Route::get('/find-users', [AdminController::class, 'find']);
        Route::get('/permissions', [UserController::class, 'permissions']);
        Route::get('/menu', [UserController::class, 'menu']);
        Route::get('/counters', [CertificationController::class, 'count']);
        Route::get('/users', [AdminController::class, 'index']);
        Route::get('/contract-types', [ContractTypeController::class, 'index']);

        // Supports
        Route::resource('/supports', SupportController::class, [
            'only' => ['index', 'show', 'update'],
            'parameters' => ['supports' => 'support']
        ]);
        // Contract management
        Route::resource('/contracts', ContractController::class, [
            'only' => ['index', 'show', 'store', 'update', 'destroy'],
            'parameters' => ['contracts' => 'contract']
        ]);
        // Obligations management
        Route::resource('contracts.obligations', ObligationController::class, [
            'only' => ['store', 'update', 'destroy'],
            'parameters' => ['contracts' => 'contract', 'obligations' => 'obligation']
        ]);
        // Additions management
        Route::resource('contracts.additions', AdditionController::class, [
            'only' => ['store', 'update', 'destroy'],
            'parameters' => ['contracts' => 'contract', 'additions' => 'addition']
        ]);
        // Extensions management
        Route::resource('contracts.extensions', ExtensionController::class, [
            'only' => ['store', 'update', 'destroy'],
            'parameters' => ['contracts' => 'contract', 'extensions' => 'extension']
        ]);
        // Suspensions management
        Route::resource('contracts.suspensions', SuspensionController::class, [
            'only' => ['store', 'update', 'destroy'],
            'parameters' => ['contracts' => 'contract', 'suspensions' => 'suspension']
        ]);
        // Assignments management
        Route::resource('contracts.assignments', AssignmentController::class, [
            'only' => ['store', 'update', 'destroy'],
            'parameters' => ['contracts' => 'contract', 'assignments' => 'assignment']
        ]);
        // Members management
        Route::resource('contracts.members', MemberController::class, [
            'only' => ['store', 'update', 'destroy'],
            'parameters' => ['contracts' => 'contract', 'members' => 'member']
        ]);

    });
});
