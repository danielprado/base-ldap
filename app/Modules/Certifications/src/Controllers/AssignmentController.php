<?php

namespace App\Modules\Certifications\src\Controllers;
use App\Http\Controllers\Controller;
use App\Modules\Certifications\src\Models\CertificationContract;
use App\Modules\Certifications\src\Request\AssignmentRequest;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class AssignmentController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function store(AssignmentRequest $request, CertificationContract $contract): JsonResponse
    {
        $contract->assignments()
            ->create([
                'Numero_Cesion' => $request->get('number'),
                'Tipo_Documento_Cesionario' => $request->get('document_type_id'),
                'Cedula_Cesionario' => $request->get('document'),
                'Dv_Cesion' => $request->get('dv'),
                'Nombre_Cesionario' => toUpper($request->get('name')),
                'Valor_Cedido' => $request->get('value'),
                'Fecha_Cesion' => $request->get('date'),
            ]);

        return $this->success_message(
            __('validation.handler.success'),
            Response::HTTP_CREATED
        );
    }

    public function update(AssignmentRequest $request, CertificationContract $contract): JsonResponse
    {
        $assignment = $contract->assignments()
            ->where('Id', $request->get('id'))
            ->firstOrFail();

        $assignment->update([
            'Numero_Cesion' => $request->get('number'),
            'Tipo_Documento_Cesionario' => $request->get('document_type_id'),
            'Cedula_Cesionario' => $request->get('document'),
            'Dv_Cesion' => $request->get('dv'),
            'Nombre_Cesionario' => $request->get('name'),
            'Valor_Cedido' => $request->get('value'),
            'Fecha_Cesion' => $request->get('date'),
        ]);

        return $this->success_message(__('validation.handler.updated'));
    }

    /**
     * @throws Exception
     */
    public function destroy(CertificationContract $contract, $id): JsonResponse
    {
        $assignment = $contract->assignments()->find($id);
        if ($assignment) {
            $assignment->delete();
            $order = 1;
            foreach ($contract->assignments as $assignment) {
                $assignment->Numero_Cesion = $order;
                $assignment->update();
                $order++;
            }
            return $this->success_message(__('validation.handler.deleted'));
        }
        return $this->error_response(__('validation.handler.unexpected_failure'));
    }
}
