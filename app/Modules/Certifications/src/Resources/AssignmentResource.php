<?php

namespace App\Modules\Certifications\src\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class AssignmentResource extends JsonResource
{
    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    public function toArray($request): array
    {
        return [
            'id' => isset($this->Id) ? (int)$this->Id : null,
            'contract_id' => isset($this->Contrato_Id) ? (int)$this->Contrato_Id : null,
            'number' => isset($this->Numero_Cesion) ? (int) $this->Numero_Cesion : null,
            'document_type_id' => isset($this->Tipo_Documento_Cesionario) ? (int)$this->Tipo_Documento_Cesionario : null,
            'document_type'     => $this->document_type->name ?? null,
            'document' => isset($this->Cedula_Cesionario) ? (int)$this->Cedula_Cesionario : null,
            'name' => $this->Nombre_Cesionario ?? null,
            'dv' => isset($this->Dv_Cesion) ? (int) $this->Dv_Cesion : null,
            'value' => isset($this->Valor_Cedido) ? number_format($this->Valor_Cedido, 0, '.', '.') : null,
            'date' => $this->Fecha_Cesion ?? null,
            'created_at' => isset($this->created_at) ? $this->created_at->format('Y-m-d') : null,
            'updated_at' => isset($this->updated_at) ? $this->updated_at->format('Y-m-d') : null,
        ];
    }

    public static function headers(): array
    {
        return [
            [
                'text' => "#",
                'value'  =>  "id",
                'sortable' => false
            ],
            [
                'align' => "right",
                'text' => "Número",
                'value'  =>  "number",
                'sortable' => false
            ],
            [
                'align' => "right",
                'text' => "Tipo de documento",
                'value'  =>  "document_type",
                'sortable' => false
            ],
            [
                'align' => "right",
                'text' => "Documento",
                'value'  =>  "document",
                'sortable' => false
            ],
            [
                'align' => "right",
                'text' => "Nombre",
                'value'  =>  "name",
                'sortable' => false
            ],
            [
                'align' => "right",
                'text' => "Valor cedido",
                'value'  =>  "value",
                'sortable' => false
            ],
            [
                'align' => "right",
                'text' => "Fecha de cesión",
                'value'  =>  "date",
                'sortable' => false
            ],
            [
                'align'     => "center",
                'text'      => "Acciones",
                'value'     =>  "actions",
                'sortable'  => false
            ],
        ];
    }
}
