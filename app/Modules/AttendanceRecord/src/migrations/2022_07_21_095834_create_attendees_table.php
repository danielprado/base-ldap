<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAttendeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::connection('mysql_attendance')->create('attendees', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('type_document');
            $table->text('number_document');
            $table->text('complete_names');
            $table->date('date_birth');
            $table->text('sex');
            $table->text('sexual_orientation');
            $table->text('gender_identity');
            $table->text('phone');
            $table->bigInteger('country_birth_id')->unsigned();
            $table->text('civil_status');
            $table->text('rh');
            $table->bigInteger('eps_id')->unsigned();
            $table->bigInteger('state_birth_id')->unsigned();
            $table->bigInteger('city_birth_id')->unsigned();
            $table->bigInteger('country_home_id')->unsigned();
            $table->bigInteger('state_home_id')->unsigned();
            $table->bigInteger('city_home_id')->unsigned();
            $table->integer('locality_home_id')->unsigned();
            $table->integer('upz_home_id')->unsigned();
            $table->text('emergency_contac');
            $table->text('emergency_phone');
            $table->integer('stratum');
            $table->text('mail');
            $table->text('Adress');
            $table->text('has_computed');
            $table->text('has_conectivity');
            $table->text('type_socialsecurity');
            $table->text('group_etnic');
            $table->text('study_level');
            $table->text('initial_skill');
            $table->text('progress_made');
            $table->text('occupation');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('attendees');
    }
}
