<?php

namespace App\Modules\Certifications\src\Models;

use Illuminate\Database\Eloquent\Model;


class CertificationSuspension extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'mysql_certificacion_contratos';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'suspencion';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Contrato_Id',
        'Numero_Suspencion',
        'Meses',
        'Dias',
        'Fecha_Inicio',
        'Fecha_Fin',
        'Fecha_Reinicio',
        'Fecha_Fin_CTO'
    ];

    protected $dates = ['Fecha_Fin'];
}
