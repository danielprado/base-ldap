<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnUserAgent extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::connection('mysql_pse')->table('pago_pse', function (Blueprint $table) {
            //
            $table->text('user_agent')->nullable()->after('otp_expiration');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::connection('mysql_pse')->table('pago_pse', function (Blueprint $table) {
            $table->dropColumn('user_agent');
        });
    }
}
