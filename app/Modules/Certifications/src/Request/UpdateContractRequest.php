<?php

namespace App\Modules\Certifications\src\Request;

use Illuminate\Foundation\Http\FormRequest;

class UpdateContractRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            // Contractor
            'document_type_id' => 'required|numeric',
            'document' => 'required|numeric',
            'dv' => 'nullable|numeric',
            'name' => 'required|string',
            // Legal Representative
            'document_type_representative_id' => 'nullable|numeric',
            'document_representative' => 'nullable|numeric',
            'name_representative' => 'nullable|string',
            // Contract
            'contract_type_id' => 'required|numeric',
            'contract_number' => 'required|numeric',
            'contract_year' => 'nullable|numeric',
            'object' => 'required|min:3|max:1000',
            'reference' => 'nullable|string',
            'signature_date' => 'required|date|date_format:Y-m-d',
            'start_date' => 'required|date|date_format:Y-m-d|before:final_date',
            'final_date' => 'required|date|date_format:Y-m-d|after:start_date',
            'anticipated_date' => 'nullable|date',
            'duration_months' => 'required|numeric',
            'duration_days' => 'required|numeric',
            'duration_other' => 'nullable|string',
            'initial_value' => 'required|numeric',
            'monthly_value' => 'required|numeric',
        ];
    }

    public function attributes()
    {
        return [
            // Contractor
            'document_type_id' => 'tipo de documento',
            'document' => 'número de documento',
            'dv' => 'dígito de verificación',
            'name' => 'nombre',
            // Legal Representative
            'document_type_representative_id' => 'tipo de documento representante',
            'document_representative' => 'número de documento representante',
            'name_representative' => 'nombre representante',
            // Contract
            'contract_type_id' => 'tipo de contrato',
            'contract_number' => 'número del contrato',
            'contract_year' => 'año del contrato',
            'object' => 'objeto del contrato',
            'signature_date' => 'fecha de firma',
            'start_date' => 'fecha de inicio del contrato',
            'final_date' => 'fecha de terminación del contrato',
            'anticipated_date' => 'fecha de terminación anticipada',
            'duration_months' => 'duración del contrato en meses',
            'duration_days' => 'duración del contrato en días',
            'duration_other' => 'otra duración',
            'initial_value' => 'valor del contrato',
            'monthly_value' => 'valor mensual del contrato',
            'reference' => 'referencia del contrato',
        ];
    }
}
