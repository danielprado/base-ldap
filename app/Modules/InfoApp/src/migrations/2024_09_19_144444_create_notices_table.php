<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_infoapp')->create('notices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('image1');
            $table->text('image2');
            $table->text('nombre');
            $table->text('resumen');
            $table->text('descripcion');
            $table->unsignedBigInteger('categoria_id');
            $table->integer('estado');
            $table->date('fechaSubida');
            $table->integer('destacado');
            $table->timestamps();
            $table->softDeletes();
            // Foreing key
            $table->foreign('categoria_id')->references('id')->on('categories');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_infoapp')->dropIfExists('notices');
    }
}
