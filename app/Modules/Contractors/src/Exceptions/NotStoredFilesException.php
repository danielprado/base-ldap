<?php

namespace App\Modules\Contractors\src\Exceptions;

use Exception;
use Illuminate\Http\Response;

class NotStoredFilesException extends Exception
{
    private $internalMessage;

    public function __construct($message = '')
    {
        $this->internalMessage = $message;
    }
    /**
     * Render the exception into an HTTP response.
     */
    public function render($request)
    {
        return response()
            ->json([
                'message' => __('validation.handler.unexpected_failure'),
                'details' => 'Los archivos no pudieron ser procesados. ' . $this->internalMessage,
                'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                'requested_at' => now()->toDateTimeLocalString()
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
