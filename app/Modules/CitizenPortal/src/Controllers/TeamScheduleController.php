<?php


namespace App\Modules\CitizenPortal\src\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\CitizenPortal\src\Constants\Roles;
use App\Modules\CitizenPortal\src\Exports\CitizenScheduleExport;
use App\Modules\CitizenPortal\src\Jobs\ConfirmStatusSubscriptionCitizen;
use App\Modules\CitizenPortal\src\Models\CitizenSchedule;
use App\Modules\CitizenPortal\src\Models\Profile;
use App\Modules\CitizenPortal\src\Models\ProfileView;
use App\Modules\CitizenPortal\src\Models\Schedule;
use App\Modules\CitizenPortal\src\Models\ScheduleView;
use App\Modules\CitizenPortal\src\Models\Status;
use App\Modules\CitizenPortal\src\Models\Team;
use App\Modules\CitizenPortal\src\Models\TeamSchedule;
use App\Modules\CitizenPortal\src\Notifications\ProfileScheduleStatusNotification;
use App\Modules\CitizenPortal\src\Request\ProfileFilterRequest;
use App\Modules\CitizenPortal\src\Request\StatusProfileScheduleRequest;
use App\Modules\CitizenPortal\src\Resources\CitizenActivitiesResource;
use App\Modules\CitizenPortal\src\Resources\CitizenScheduleResource;
use App\Modules\CitizenPortal\src\Resources\ProfileResource;
use App\Modules\CitizenPortal\src\Resources\TeamScheduleResource;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;
use Maatwebsite\Excel\Excel;

class TeamScheduleController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @param ProfileFilterRequest $request
     * @param Schedule $schedule
     * @return JsonResponse
     */
    public function index(ProfileFilterRequest $request, Schedule $schedule)
    {
        $query = TeamSchedule::query()
            ->whereHas('team', function ($query) use ($request) {
                return $this->setQuery($query, (new Team)->getSortableColumn($this->column))
                    ->when($this->query, function ($query) {
                        $keys = Team::where("name", "like", "%{$this->query}%")->get(['id'])->pluck('id')->toArray();
                        return $query->whereKey($keys);
                    });
            })
            ->with("team")
            ->where('schedule_id', $schedule->id);
        return $this->success_response(
            TeamScheduleResource::collection($query->paginate( $this->per_page)),
            Response::HTTP_OK,
            TeamScheduleResource::headers()
        );
    }

    /**
     * @param $schedule
     * @param $team
     * @return JsonResponse
     */
    public function show($schedule, $team)
    {
        $query = TeamSchedule::query()
            ->with("team")->where('schedule_id', $schedule)
            ->where('team_id', $team)
            ->firstOrFail();
        return $this->success_response(
            new TeamScheduleResource(
                $query
            ),
            Response::HTTP_OK,
            TeamScheduleResource::headers()
        );
    }

    /**
     * @param $profile
     * @return JsonResponse
     */
    public function activities($profile)
    {
        $query = TeamSchedule::query()
            ->with('team')
            ->where('profile_id', $profile)
            ->latest()
            ->paginate($this->per_page);
        return $this->success_response(
            TeamScheduleResource::collection($query)
        );
    }
}
