<?php


namespace App\Modules\CitizenPortal\src\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\CitizenPortal\src\Constants\Roles;
use App\Modules\CitizenPortal\src\Models\ActivityType;
use App\Modules\CitizenPortal\src\Resources\ActivityTypeResource;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class ActivityTypeController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * @return JsonResponse
     */
    public function index()
    {
        $types = ActivityType::all();
        return $this->success_response(
            ActivityTypeResource::collection($types),
            Response::HTTP_OK
        );
    }
}
