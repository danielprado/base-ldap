<?php

namespace App\Modules\Contractors\src\Notifications;

use App\Modules\Contractors\src\Models\Compliance;
use App\Modules\Contractors\src\Models\Contract;
use App\Modules\Contractors\src\Models\Contractor;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;

class ComplianceCorrectedNotification extends Notification
{
    use Queueable;

    /**
     * @var Compliance
     */
    private $compliance;

    /**
     * Create a new notification instance.
     *
     * @param Contractor $contractor
     * @param Contract $contract
     */
    public function __construct(Compliance $compliance)
    {
        $this->compliance = $compliance;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $contract = $this->compliance->contract_number;

        return [
            'title'         => 'Corrección de Cumplimiento Tributario',
            'subject'       => "Corrección de certificado de cumplimiento tributario para el contrato $contract.",
            'user'          => 'SISTEMA',
            'created_at'    => now()->format('Y-m-d H:i:s'),
            'url'           => [
                'name'      =>  'review-activity-report'
            ]
        ];
    }
}
