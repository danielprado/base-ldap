<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddCyclePathGuardianColumns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plans', function (Blueprint $table) {
            $table->boolean('cycle_path_guardian')->default(0)->after('reserve');
            $table->integer('missing_sessions')->default(0)->after('cycle_path_guardian');
        });
        Schema::table('payrolls', function (Blueprint $table) {
            $table->boolean('cycle_path_guardian')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plans', function (Blueprint $table) {
            $table->dropColumn('cycle_path_guardian')->after('reserve');
            $table->dropColumn('missing_sessions');
        });
        Schema::table('payrolls', function (Blueprint $table) {
            $table->dropColumn('cycle_path_guardian');
        });
    }
}
