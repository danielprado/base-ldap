<?php

namespace App\Modules\AttendanceRecord\src\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Assist extends Model
{
      //
      use SoftDeletes;
      protected $connection = 'mysql_attendance';
      protected $table = 'assists';
      protected $dates = ['deleted_at'];
      protected $fillable = [
            'id',
            'attendee_id',
            'module_id',
            'date_assist',
            'session_id',
            'data',
      ];

      public function attendee()
      {
            return $this->belongsTo(Attendee::class,'attendee_id', 'id');
      }

      public function module()
      {
            return $this->belongsTo(Module::class,'module_id', 'id');
      }
}
