<?php

namespace App\Modules\PaymentGateway\src\Models;

use Illuminate\Database\Eloquent\Model;

class BookingView extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_pse';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'booking';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [];

    /*
     * ---------------------------------------------------------
     * Query Scopes
     * ---------------------------------------------------------
    */

    public function scopeBookingsApproved($query)
    {
        return $query->where('status_id', '=', 2);
    }
    public function scopeBookingsFailed($query)
    {
        return $query->where('status_id', '!=', 2);
    }
    public function scopeBookings($query)
    {
        return $query->whereIn('status_id', [1,2,3,4,5]);
    }

}
