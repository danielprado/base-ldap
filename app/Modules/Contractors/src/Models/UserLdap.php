<?php

namespace App\Modules\Contractors\src\Models;

use Illuminate\Database\Eloquent\Model;


//use Yajra\Oci8\Eloquent\OracleEloquent as Model;

class UserLdap extends Model
{
    protected $connection = 'mysql_ldap';
    protected $table = 'users';
    protected $hidden = ['created_at', 'updated_at'];

    protected $primaryKey = 'id';

    protected $fillable = [
        'signature'
    ];

    public function payroll()
    {
        return $this->belongsTo(Payroll::class);
    }
}

