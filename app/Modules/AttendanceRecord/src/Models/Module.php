<?php

namespace App\Modules\AttendanceRecord\src\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Module extends Model
{
       /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_attendance';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'modules';

}
