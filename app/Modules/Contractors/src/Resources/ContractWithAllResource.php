<?php

namespace App\Modules\Contractors\src\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Modules\Contractors\src\Models\Contract;
use App\Modules\Contractors\src\Models\DocumentInit;
use Carbon\CarbonPeriod;
use NumberFormatter;
use Carbon\Carbon;

class ContractWithAllResource extends JsonResource
{
    protected $signature;
    protected $numberContract;

    public function __construct($resource, $signature)
    {
        parent::__construct($resource);
        $this->signature = $signature;
        $numberArray = array_reverse(explode('-', $this->contract));
        $this->numberContract = $numberArray[1] . '-' . $numberArray[0];
    }


    public function toArray($request)
    {
        $document_init = DocumentInit::where('contract_id', $this->id)->first();
        $suspension_contract = Contract::where('contract', 'like', '%' . $this->numberContract . '%')->where('contract_type_id', Contract::CONTRACT_SUSPENSION)->get();
        $addition_contract = Contract::where('contract', 'like', '%' . $this->numberContract . '%')->whereIn('contract_type_id', [Contract::CONTRACT_ADD_AND_EXT, Contract::CONTRACT_EXTENSION])->first();
        $datesStringSuspension = $suspension_contract->map(function ($contract) {
            return substr($contract->start_suspension_date, 0, 10) . ' - ' . substr($contract->final_suspension_date, 0, 10);
        })->implode(', ');

        return [
            'contract' => [
                'id'  => $this->id ?? null,
                'contract'  => $this->contract ?? null,
                'contractor_id'  => $this->contractor_id ?? null,
                'name'  => $this->contractor->name . ' ' . $this->contractor->surname ?? null,
                'document'  => $this->contractor->document ?? null,
                'contract_type'  => $this->contract_type->name ?? null,
                'object_contract' => $this->object_contract ?? null,
                'execution_period'  => $this->durationText($document_init),
                'total'  => $this->total ?? null,
                'start_date'  => $this->getStartDate(),
                'contract_type_id'  => $this->contract_type_id ?? null,
                'final_date'  => $this->final_date ?? null,
                'signature' => $this->signature ?? null,
                'supervisor'  => [
                    'id'  => $this->getUser()->id ?? null,
                    'name'  => $this->getUser()->name . ' ' . $this->getUser()->surname,
                    'document'  => $this->getUser()->document ?? null,
                    'email'  => $this->getUser()->email ?? null,
                    'description'  => $this->getUser()->description ?? null,
                    'dependency'  => $this->getUser()->dependency ?? null,
                    'signature'  => $this->getUser()->signature ?? null,
                ],
                'dependency_id' => $this->dependency_id ?? null,
                'document_init'=>$this->document_init ?? null,
                'suspension' => $suspension_contract->isNotEmpty() ? $datesStringSuspension : 'NO',
                'addition' =>  isset($addition_contract) ? substr($addition_contract->start_date, 0, 10).'  -  '.substr($addition_contract->final_date, 0, 10) : 'NO'
            ],
            'compliances'  =>   CompleteComplianceResource::collection($this->whenLoaded('compliances')),
            'compliance_expanded'  =>  CompleteComplianceResource::additionalData(),
            'activity_reports'  => $this->activityReports ?? null,
            'plans'  => $this->plans ?? null
        ];
    }

    private function getUser()
    {
        $user = auth('api')->user();
        return $user;
    }

    private function durationText($document_init)
    {
        if (isset($document_init)){
            $months = $document_init->months_contract;
            $days = $document_init->days_contract;
            if ($days == 0) {
                return "$months meses";
            }
            return "$months meses $days días";
        }

        $start_date = Carbon::createFromFormat('Y-m-d', substr($this->start_date, 0, 10));
        $final_date = Carbon::createFromFormat('Y-m-d', substr($this->final_date, 0, 10));
        $difference = $start_date->diff($final_date);
        if ($difference->y > 0 || $difference->m > 0) {
            return "{$difference->m} meses" . ($difference->d > 0 ? " {$difference->d} días" : "");
        }
        return $difference->days > 0 ? "{$difference->days} días" : "";
    }

    private function getStartDate()
    {
        if (in_array($this->contract_type_id, [Contract::NEW_CONTRACT, Contract::CONTRACT_ASSIGNMENT])){
            return isset($this->start_date) ? $this->start_date->format('Y-m-d') : null;
        }
        $original_contract = Contract::where('contract', 'like', '%' . $this->numberContract . '%')->where('contract_type_id', Contract::NEW_CONTRACT)->first();
        if (isset($original_contract)){
            return isset($original_contract->start_date) ? $original_contract->start_date->format('Y-m-d') : null;
        }
        return null;
    }

    private function gestSuspensions(){

    }

}
