<?php


namespace App\Modules\CitizenPortal\src\Resources;


use App\Modules\CitizenPortal\src\Constants\Roles;
use App\Modules\CitizenPortal\src\Models\FileType;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class PaymentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                    => $this->id ?? null,
            'park_id'               => isset($this->parque_id) ? (int) $this->parque_id : null,
            'park'                  => $this->park->nombre_parque ?? null,
            'park_code'             => $this->park->codigo_parque ?? null,
            'service_id'            => isset($this->servicio_id) ? (int) $this->servicio_id : null,
            'service'               => toUpper($this->service->servicio_nombre ?? null),
            'document'              => $this->identificacion ?? null,
            'document_type_id'      => isset($this->tipo_identificacion) ? (int) $this->tipo_identificacion : null,
            'payment_code'          => $this->codigo_pago ?? null,
            'pse_transaction_id'    => $this->id_transaccion_pse ?? null,
            'email'                 => toLower($this->email ?? null),
            'name'                  => toUpper($this->nombre ?? null),
            'lastname'              => toUpper($this->apellido ?? null),
            'phone'                 => $this->telefono ?? null,
            'status_id'             => isset($this->estado_id) ? (int) $this->estado_id : null,
            'status'                => toUpper($this->status->descripcion ?? null),
            'status_color'          => $this->statusColor( toUpper($this->status->descripcion ?? null) ),
            'status_bank'           => toUpper($this->estado_banco ?? null),
            'concept'               => toUpper($this->concepto ?? null),
            'money'                 => $this->moneda ?? null,
            'total'                 => $this->total ?? null,
            'total_format'          => cop_money_format($this->total ?? null, '$', $this->moneda ?? null),
            'tax'                   => $this->iva ?? null,
            'permission'            => $this->permiso ?? null,
            'permission_type'       => $this->tipo_permiso ?? null,
            'booking_id'            => $this->id_reserva ?? null,
            'user_id_pse'           => $this->user_id_pse ?? null,
            'method_id'             => isset($this->medio_id) ? (int) $this->medio_id : null,
            'method'                => toUpper($this->method->Nombre ?? null),
            'payment_at'            => isset($this->fecha_pago) ? Carbon::parse($this->fecha_pago)->format('Y-m-d H:i:s') : null,
            'created_at'            => isset($this->created_at) ? $this->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'            => isset($this->updated_at) ? $this->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }

    public function statusColor($status = null)
    {
        switch ($status) {
            case toUpper('Pendiente') == toUpper($status);
                return 'warning';
            case toUpper('Aprobado') == toUpper($status);
                return 'success';
            case toUpper('Cancelado') == toUpper($status);
                return 'grey';
            case toUpper('Rechazado') == toUpper($status);
            case toUpper('Fallido') == toUpper($status);
                return 'error';
            default:
                return 'primary';
        }
    }

    public static function headers()
    {
        return [
            'headers'   => [
                [
                    'icon'  =>  'mdi-pound',
                    'align' => "right",
                    'text' => "#",
                    'value'  =>  "id",
                    'sortable' => false
                ],
                [
                    'icon'  =>  'mdi-text',
                    'align' => "right",
                    'text' => "Código del Pago",
                    'value'  =>  "payment_code",
                    'sortable' => false
                ],
                [
                    'icon'  =>  'mdi-text',
                    'align' => "right",
                    'text' => "Servicio",
                    'value'  =>  "service",
                    'sortable' => false
                ],
                [
                    'icon'  =>  'mdi-text',
                    'align' => "right",
                    'text' => "Estado",
                    'value'  =>  "status",
                    'sortable' => false
                ],
                [
                    'icon'  =>  'mdi-text',
                    'align' => "right",
                    'text' => "IVA",
                    'value'  =>  "tax",
                    'sortable' => false
                ],
                [
                    'icon'  =>  'mdi-text',
                    'align' => "right",
                    'text' => "Método de Pago",
                    'value'  =>  "method",
                    'sortable' => false
                ],
                [
                    'icon'  =>  'mdi-currency-usd',
                    'align' => "right",
                    'text' => "Total Pagado",
                    'value'  =>  "total_format",
                    'sortable' => false
                ],
                [
                    'icon'  =>  'mdi-calendar',
                    'align' => "right",
                    'text' => "Fecha de pago",
                    'value'  =>  "payment_at",
                    'sortable' => false
                ],
            ],
            'expanded'  => [
                [
                    'icon'  => 'mdi-text',
                    'align' => "left",
                    'text' => 'Nombres',
                    'value'  =>  "name",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-text',
                    'align' => "left",
                    'text' => 'Apellidos',
                    'value'  =>  "lastname",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-numeric',
                    'align' => "left",
                    'text' => 'Documento registrado',
                    'value'  =>  "document",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-numeric',
                    'align' => "left",
                    'text' => 'Correo electrónico',
                    'value'  =>  "email",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-phone',
                    'align' => "left",
                    'text' => 'Teléfono',
                    'value'  =>  "phone",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pound',
                    'align' => "left",
                    'text' => 'Código PSE de la transacción',
                    'value'  =>  "pse_transaction_id",
                    'sortable' => false
                ],
                [
                    'icon'  =>  'mdi-text',
                    'align' => "right",
                    'text' => "Estado del Banco",
                    'value'  =>  "status_bank",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pine-tree',
                    'align' => "left",
                    'text' => 'Parque',
                    'value'  =>  "park",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pine-tree',
                    'align' => "left",
                    'text' => 'Código PSE del Parque',
                    'value'  =>  "park_code",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pine-tree',
                    'align' => "left",
                    'text' => 'Permiso',
                    'value'  =>  "permission",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pine-tree',
                    'align' => "left",
                    'text' => 'Tipo de Permiso',
                    'value'  =>  "permission_type",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-pine-tree',
                    'align' => "left",
                    'text' => 'Concepto',
                    'value'  =>  "concept",
                    'sortable' => false
                ],
                [
                    'icon'  => 'mdi-calendar',
                    'align' => "left",
                    'text' => 'Fecha de registro',
                    'value'  =>  "created_at",
                    'sortable' => false
                ],
                [
                    'align' => "left",
                    'text' => 'Fecha de Actualización',
                    'value'  =>  "updated_at",
                    'sortable' => false
                ],
            ]
        ];
    }
}
