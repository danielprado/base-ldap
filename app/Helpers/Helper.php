<?php

use allejo\Socrata\Exceptions\InvalidResourceException;
use App\Helpers\SecopAPI;
use App\Models\Security\SendgridStatus;
use App\Modules\Contractors\src\Models\WebServiceStatus;
use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Arr;
use PhpOffice\PhpSpreadsheet\Shared\Date;

if ( !function_exists('sendgird_logger') ) {
    function sendgird_logger(
        $tos = null,
        $subject = null,
        $body = null,
        $attachment,
        $status = null,
        $response = null
    ) {
        try {
            $sendgrid = new SendgridStatus();
            // Log de envío
            $sendgrid->to = $tos;
            $sendgrid->subject = $subject;
            $sendgrid->body = $body;
            $sendgrid->attachment = $attachment;
            $sendgrid->status = $status;
            $sendgrid->response = $response;
            $sendgrid->save();
            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }
}

if ( !function_exists('search_in_string') ) {
    function search_in_string($string, $needle) {
        $string = iconv('UTF-8', 'ASCII//TRANSLIT', $string);
        $string = preg_replace('/[^a-zA-Z0-9]/', '_', $string);
        $string = preg_replace('!\s+!', ' ', mb_convert_case( strtolower( trim( strip_tags( $string ) ) ), MB_CASE_UPPER, 'UTF-8'));

        $needle = iconv('UTF-8', 'ASCII//TRANSLIT', $needle);
        $needle = preg_replace('/[^a-zA-Z0-9]/', '_', $needle);
        $needle = preg_replace('!\s+!', ' ', mb_convert_case( strtolower( trim( strip_tags( $needle ) ) ), MB_CASE_UPPER, 'UTF-8'));
        return str_contains($string, $needle);
    }
}

if ( !function_exists('percent_filled') ) {
    /**
     * Calculate how much a data is completed
     *
     * @param $array
     * @param $except
     * @return float|int
     */
    function percent_filled($array, $except, $only = false) {
        if ( ! $array || count($array) == 0 || !is_array($array)) {
            return 0;
        }
        $data = $only ? Arr::only($array, $except) : Arr::except($array, $except);
        $columns = array_keys($data);
        $per_column = 100 / count($data);
        $total      = 0;

        foreach ($data as $key => $value) {
            if ($value !== NULL && $value !== [] && in_array($key, $columns)) {
                $total += $per_column;
            }
        }

        return $total;
    }
}

if ( !function_exists('toUpper') ) {
    /**
     * The method to return upper string including spanish chars
     *
     * @param null $string
     * @return mixed|string
     */
    function toUpper( $string = null )
    {
        if ( is_string($string) || is_numeric( $string ) ) {
            return preg_replace('!\s+!', ' ', mb_convert_case( strtolower( trim( strip_tags( $string ) ) ), MB_CASE_UPPER, 'UTF-8'));
        }

        return null;
    }
}

if ( !function_exists('isAValidDate') ) {
    /**
     * The method to return upper string including spanish chars
     *
     * @param $date
     * @param string $format
     * @return bool
     */
    function isAValidDate( $date, $format = 'Y-m-d' )
    {
        $d = DateTime::createFromFormat($format, $date);
        return $d && $d->format($format) == $date;
    }
}

if ( !function_exists('validateDate') ) {
    /**
     * The method to return upper string including spanish chars
     *
     * @param $date
     * @param string $format
     * @return bool
     */
    function valiateDate( $date, string $format = 'Y-m-d' )
    {
        try {
            return Carbon::parse($date)->format($format);
        } catch (Exception $exception) {
            return false;
        }
    }
}

if ( ! function_exists('toLower') ) {
    /**
     * The method to return lower string including spanish chars
     *
     * @param null $string
     * @return mixed|string
     */
    function toLower( $string = null )
    {
        if ( is_string($string) || is_numeric( $string ) ) {
            return preg_replace('!\s+!', ' ', mb_convert_case( strtolower( trim( strip_tags( $string ) ) ), MB_CASE_LOWER, 'UTF-8'));
        }

        return null;
    }
}

if ( ! function_exists('toTitle') ) {
    /**
     * The method to return title string including spanish chars
     *
     * @param null $string
     * @return mixed|string
     */
    function toTitle( $string = null )
    {
        if ( is_string($string) || is_numeric( $string ) ) {
            return preg_replace('!\s+!', ' ', mb_convert_case( strtolower( trim( strip_tags( $string ) ) ), MB_CASE_TITLE, 'UTF-8'));
        }

        return null;
    }
}

if ( ! function_exists('toFirstUpper') ) {
    /**
     * The method to return title string including spanish chars
     *
     * @param null $string
     * @return mixed|string
     */
    function toFirstUpper( $string = null )
    {
        if ( is_string($string) || is_numeric( $string ) ) {
            $str = preg_replace('!\s+!', ' ', ucfirst( mb_convert_case( strtolower( trim( strip_tags( $string ) ) ), MB_CASE_LOWER, 'UTF-8') ));
            preg_match_all("/\.\s*\w/", $str, $matches);

            foreach($matches[0] as $match){
                $str = str_replace($match, strtoupper($match), $str);
            }
            return $str;
        }

        return null;
    }
}

if ( ! function_exists('ldapDateToCarbon') ) {
    /**
     * Convert LDAP dates to readable date
     *
     * @param $date
     * @return string
     */
    function ldapDateToCarbon($date) {
        if ( $date == "0" || $date == 0 ) {
            return now()->addYears(2)->format('Y-m-d H:i:s');
        } else {
            $winSecs       = (int)($date / 10000000); // divide by 10 000 000 to get seconds
            $unixTimestamp = ($winSecs - 11644473600); // 1.1.1600 -> 1.1.1970 difference in seconds
            $date = date(DateTime::RFC822, $unixTimestamp);
            return Carbon::parse( $date )->format('Y-m-d H:i:s');
        }
    }
}

if ( ! function_exists('ldapFormatDate') ) {
    /**
     * Convert LDAP dates to valid date
     *
     * @param $date
     * @return string|array
     */
    function ldapFormatDate($date) {
        if ( is_array($date) && count($date) > 1 ) {
            $dates = [];
            foreach ($date as $dt) {
                $new_date = explode('.', $dt);
                $new_date = isset($new_date[0]) ? $new_date[0] : '0000-00-00 00:00:00';
                $dates[] = Carbon::parse( $new_date )->format('Y-m-d H:i:s');
            }
            return $dates;
        } elseif (is_array($date) && count($date) == 1) {
            $new_date = explode('.', $date[0]);
            $new_date = isset($new_date[0]) ? $new_date[0] : '0000-00-00 00:00:00';
            return Carbon::parse( $new_date )->format('Y-m-d H:i:s');
        } else {
            $new_date = explode('.', $date);
            $new_date = isset($new_date[0]) ? $new_date[0] : '0000-00-00 00:00:00';
            return Carbon::parse( $new_date )->format('Y-m-d H:i:s');
        }
    }
}

if ( ! function_exists('isJson') ) {
    /**
     * @param $string
     * @return bool
     */
    function isJson($string) {
        try {
            if (is_null($string)) {
                return false;
            }
            // decode the JSON data
            $result = is_array($string)
                ? json_encode($string)
                : json_decode($string);

            // switch and check possible JSON errors
            switch (json_last_error()) {
                case JSON_ERROR_NONE:
                    $error = ''; // JSON is valid // No error has occurred
                    break;
                case JSON_ERROR_DEPTH:
                    $error = 'The maximum stack depth has been exceeded.';
                    break;
                case JSON_ERROR_STATE_MISMATCH:
                    $error = 'Invalid or malformed JSON.';
                    break;
                case JSON_ERROR_CTRL_CHAR:
                    $error = 'Control character error, possibly incorrectly encoded.';
                    break;
                case JSON_ERROR_SYNTAX:
                    $error = 'Syntax error, malformed JSON.';
                    break;
                // PHP >= 5.3.3
                case JSON_ERROR_UTF8:
                    $error = 'Malformed UTF-8 characters, possibly incorrectly encoded.';
                    break;
                // PHP >= 5.5.0
                case JSON_ERROR_RECURSION:
                    $error = 'One or more recursive references in the value to be encoded.';
                    break;
                // PHP >= 5.5.0
                case JSON_ERROR_INF_OR_NAN:
                    $error = 'One or more NAN or INF values in the value to be encoded.';
                    break;
                case JSON_ERROR_UNSUPPORTED_TYPE:
                    $error = 'A value of a type that cannot be encoded was given.';
                    break;
                default:
                    $error = 'Unknown JSON error occured.';
                    break;
            }
            return $error !== '';
        } catch (Exception $exception) {
            return false;
        }
    }
}

if ( ! function_exists('mask') ) {
    /**
     * @param $str
     * @param $first
     * @param $last
     * @return string
     */
    function mask($str, $first, $last) {
        $len = strlen($str);
        $toShow = $first + $last;
        return substr($str, 0, $len <= $toShow ? 0 : $first).str_repeat("*", $len - ($len <= $toShow ? 0 : $toShow)).substr($str, $len - $last, $len <= $toShow ? 0 : $last);
    }
}

if ( ! function_exists('mask_email') ) {
    /**
     * @param $email
     * @return string
     */
    function mask_email($email) {
        $mail_parts = explode("@", $email);
        $domain_parts = explode('.', $mail_parts[1]);

        $mail_parts[0] = mask($mail_parts[0], 2, 1); // show first 2 letters and last 1 letter
        $domain_parts[0] = mask($domain_parts[0], 2, 1); // same here
        $mail_parts[1] = implode('.', $domain_parts);

        return implode("@", $mail_parts);
    }
}

if ( ! function_exists('format_contract') ) {
    function format_contract($number, $year) {
        $contract_number = str_pad($number, 4, '0', STR_PAD_LEFT);
        return toUpper("IDRD-CTO-{$contract_number}-{$year}");
    }
}

if ( ! function_exists('random_img_name') ) {
    /**
     * @return string
     */
    function random_img_name() {
        $s = strtoupper(md5(uniqid(rand(),true)));
        return substr($s,0,8) . '-' .
            substr($s,8,4) . '-' .
            substr($s,12,4). '-' .
            substr($s,16,4). '-' .
            substr($s,20);
    }
}

if ( ! function_exists('template_exist') ) {
    /**
     * @param $file
     * @param string $disk
     * @param string $path
     * @return bool
     */
    function template_exist($file, string $disk = 'local', string $path = 'templates/') {
        return \Illuminate\Support\Facades\Storage::disk($disk)->exists("{$path}{$file}");
    }
}

if ( ! function_exists('get_template') ) {
    /**
     * @param $file
     * @param string $disk
     * @param string $path
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    function get_template($file, string $disk = 'local', string $path = 'templates/') {
        try {
            return \Illuminate\Support\Facades\Storage::disk($disk)->get("{$path}{$file}");
        } catch (\Illuminate\Contracts\Filesystem\FileNotFoundException $exception) {
            return false;
        }
    }
}

if ( ! function_exists('array_insert_in_position') ) {
    /**
     * @param array $array
     * @param array $insertedArray
     * @param int $position
     * @return array
     */
    function array_insert_in_position(array $array, array $insertedArray, int $position = 0) {
        $i = 0;
        $new_array = [];
        foreach ($array as $value) {
            if ($i == $position) {
                foreach ($insertedArray as $ivalue) {
                    $new_array[] = $ivalue;
                }
            }
            $new_array[] = $value;
            $i++;
        }
        return $new_array;
    }
}

if ( ! function_exists('str_starts_with') ) {
    function str_starts_with($haystack, $needle) {
        $length = strlen($needle);
        return substr($haystack, 0, $length) === $needle;
    }
}

if ( ! function_exists('str_ends_with') ) {
    function str_ends_with($haystack, $needle) {
        $length = strlen($needle);
        if (!$length) {
            return true;
        }
        return substr($haystack, -$length) === $needle;
    }
}

if ( ! function_exists('class_dash_name') ) {
    function class_dash_name($class) {
        return strtolower(preg_replace('/([a-zA-Z])(?=[A-Z])/', '$1-', class_basename($class)));
    }
}

if ( ! function_exists('verify_url') ) {
    /**
     * @param $url
     * @return bool
     */
    function verify_url($url) {
        try {
            if ($url == null) {
                return false;
            }
            $client = new Client();
            $data = $client->head( $url );
            $status = $data->getStatusCode();
            return $status >= 200 && $status < 300;
        } catch (ClientException $e) {
            return false;
        }
    }
}

if (! function_exists('date_time_to_excel')) {
    /**
     * @param null $date
     */
    function date_time_to_excel($date = null, $fallbackFormat = 'Y-m-d H:i:s') {
        try {
            if (is_null($date)) {
                return null;
            }
            return Date::dateTimeToExcel($date);
        } catch (\Exception $exception) {
            if ($date instanceof Carbon) {
                return $date->format($fallbackFormat);
            } else {
                $check = valiateDate($date, $fallbackFormat);
                return $check ?: $date;
            }
        }
    }
}

if (! function_exists('update_status_job')) {
    function update_status_job($id, $status, $queue) {
        try {
            $job = \Imtigger\LaravelJobStatus\JobStatus::query()->whereKey($id)->first();
            if (isset($job->id)) {
                $job->status = $status;
                $job->queue = $queue;
                $job->finished_at = null;
                $job->save();
            }
        } catch (Exception $exception) {

        }
    }
}

if (! function_exists('whatsapp_link')) {
    function whatsapp_link($phone, $message = null, $phone_code = '57') {
        if (isset($phone) && strlen($phone) > 9) {
            $url = "https://api.whatsapp.com/send?";
            $params = [
                'phone' => "{$phone_code}{$phone}",
                'text'  => $message,
            ];
            $queryString =  http_build_query($params);
            return "$url$queryString";
        }
        return null;
    }
}

if (! function_exists('cop_money_format')) {
    function cop_money_format(
        $number = null,
        $prefix = "$",
        $suffix = null,
        $decimal = 2,
        $decimal_separator = '.',
        $thousands_separator = ','
    ) {
        return $number
            ? $prefix.' '.number_format($number, $decimal, $decimal_separator, $thousands_separator).' '.$suffix
            : $number;
    }
}

if (! function_exists('extract_number_format_contract') ) {
    function extract_number_format_contract($format): ?string
    {
        if (preg_match('/IDRD-[A-Z]+-[A-Z]+-(\d{4})(\d+)/', $format, $matches)) {
            $contract_number = ltrim($matches[2], '0');
        }
        elseif (preg_match('/IDRD-[A-Z]+-[A-Z]+-(\d+)-(\d{4})/', $format, $matches)) {
            $contract_number = ltrim($matches[1], '0');
        }
        else {
            $contract_number = substr($format, 9, -5);
            if (!is_numeric($contract_number) || str_starts_with($contract_number, ' ') || str_ends_with($contract_number, ' ')) {
                $contract_number = ltrim($contract_number);
                $contract_number = preg_replace('/[^0-9\-]/', '', $contract_number);
                $contract_number = preg_replace('/-.*/', '', $contract_number);
            }
            $contract_number = ltrim($contract_number, '0');
        }

        return $contract_number ?: null;
    }
}

if (! function_exists('extract_reference_format_contract') ) {
    function extract_reference_format_contract($format): ?string
    {
        //"IDRD-STRD-CPS-20243090" o "IDRD-STRD-CPS-3090-2024"
        if (preg_match('/IDRD-([A-Z]+-[A-Z]+)-\d{4}\d+/', $format, $matches)) {
            $reference = $matches[1]; // Captura "STRD-CPS" en este ejemplo
        }
        // "IDRD-STRD-CPS-3090-2024"
        elseif (preg_match('/IDRD-([A-Z]+-[A-Z]+)-\d+-\d{4}/', $format, $matches)) {
            $reference = $matches[1]; // Captura "STRD-CPS" en este ejemplo
        }

        return $reference ?: null;
    }
}

if (!function_exists('extract_number_and_year_from_contract')) {
    function extract_number_and_year_from_contract($contract): ?array
    {
        $pattern = '/(\d+)-(\d{4})$/';
        if (preg_match($pattern, $contract, $matches)) {
            return [
                'contract_number' => $matches[1],
                'contract_year' => $matches[2]
            ];
        }
        return null;
    }
}

if (! function_exists('format_string_name') ) {
    function format_string_name($string): ?string
    {
        return remove_accents(preg_replace('/^\s+|\s+$|\s+(?=\s)/', '', $string)) ?? null;
    }
}

if (! function_exists('is_defined') ) {
    function is_defined($value): bool
    {
        return $value && $value != 'null' && $value != 'undefined';
    }
}

if (! function_exists('compliance_comparable_period') ) {
    function compliance_comparable_period(): array
    {
        $now = now();
        $baseComplianceDate = Carbon::create($now->year, $now->month, 20);

        if ($baseComplianceDate->diffInDays($now, false) <= 0) {
            return [
                'start' => $baseComplianceDate->copy()->subMonth()->toDateString(),
                'end' => $baseComplianceDate->toDateString()
            ];
        }

        return [
            'start' => $baseComplianceDate->toDateString(),
            'end' => $baseComplianceDate->copy()->addMonth()->toDateString()
        ];
    }
}

if (! function_exists('remove_accents')) {
    function remove_accents($string) {
        if ( !preg_match('/[\x80-\xff]/', $string) )
            return $string;

        $chars = array(
            // Decompositions for Latin-1 Supplement
            chr(195).chr(128) => 'A', chr(195).chr(129) => 'A',
            chr(195).chr(130) => 'A', chr(195).chr(131) => 'A',
            chr(195).chr(132) => 'A', chr(195).chr(133) => 'A',
            chr(195).chr(135) => 'C', chr(195).chr(136) => 'E',
            chr(195).chr(137) => 'E', chr(195).chr(138) => 'E',
            chr(195).chr(139) => 'E', chr(195).chr(140) => 'I',
            chr(195).chr(141) => 'I', chr(195).chr(142) => 'I',
            chr(195).chr(143) => 'I', chr(195).chr(145) => 'N',
            chr(195).chr(146) => 'O', chr(195).chr(147) => 'O',
            chr(195).chr(148) => 'O', chr(195).chr(149) => 'O',
            chr(195).chr(150) => 'O', chr(195).chr(153) => 'U',
            chr(195).chr(154) => 'U', chr(195).chr(155) => 'U',
            chr(195).chr(156) => 'U', chr(195).chr(157) => 'Y',
            chr(195).chr(159) => 's', chr(195).chr(160) => 'a',
            chr(195).chr(161) => 'a', chr(195).chr(162) => 'a',
            chr(195).chr(163) => 'a', chr(195).chr(164) => 'a',
            chr(195).chr(165) => 'a', chr(195).chr(167) => 'c',
            chr(195).chr(168) => 'e', chr(195).chr(169) => 'e',
            chr(195).chr(170) => 'e', chr(195).chr(171) => 'e',
            chr(195).chr(172) => 'i', chr(195).chr(173) => 'i',
            chr(195).chr(174) => 'i', chr(195).chr(175) => 'i',
            chr(195).chr(177) => 'n', chr(195).chr(178) => 'o',
            chr(195).chr(179) => 'o', chr(195).chr(180) => 'o',
            chr(195).chr(181) => 'o', chr(195).chr(182) => 'o',
            chr(195).chr(182) => 'o', chr(195).chr(185) => 'u',
            chr(195).chr(186) => 'u', chr(195).chr(187) => 'u',
            chr(195).chr(188) => 'u', chr(195).chr(189) => 'y',
            chr(195).chr(191) => 'y',
            // Decompositions for Latin Extended-A
            chr(196).chr(128) => 'A', chr(196).chr(129) => 'a',
            chr(196).chr(130) => 'A', chr(196).chr(131) => 'a',
            chr(196).chr(132) => 'A', chr(196).chr(133) => 'a',
            chr(196).chr(134) => 'C', chr(196).chr(135) => 'c',
            chr(196).chr(136) => 'C', chr(196).chr(137) => 'c',
            chr(196).chr(138) => 'C', chr(196).chr(139) => 'c',
            chr(196).chr(140) => 'C', chr(196).chr(141) => 'c',
            chr(196).chr(142) => 'D', chr(196).chr(143) => 'd',
            chr(196).chr(144) => 'D', chr(196).chr(145) => 'd',
            chr(196).chr(146) => 'E', chr(196).chr(147) => 'e',
            chr(196).chr(148) => 'E', chr(196).chr(149) => 'e',
            chr(196).chr(150) => 'E', chr(196).chr(151) => 'e',
            chr(196).chr(152) => 'E', chr(196).chr(153) => 'e',
            chr(196).chr(154) => 'E', chr(196).chr(155) => 'e',
            chr(196).chr(156) => 'G', chr(196).chr(157) => 'g',
            chr(196).chr(158) => 'G', chr(196).chr(159) => 'g',
            chr(196).chr(160) => 'G', chr(196).chr(161) => 'g',
            chr(196).chr(162) => 'G', chr(196).chr(163) => 'g',
            chr(196).chr(164) => 'H', chr(196).chr(165) => 'h',
            chr(196).chr(166) => 'H', chr(196).chr(167) => 'h',
            chr(196).chr(168) => 'I', chr(196).chr(169) => 'i',
            chr(196).chr(170) => 'I', chr(196).chr(171) => 'i',
            chr(196).chr(172) => 'I', chr(196).chr(173) => 'i',
            chr(196).chr(174) => 'I', chr(196).chr(175) => 'i',
            chr(196).chr(176) => 'I', chr(196).chr(177) => 'i',
            chr(196).chr(178) => 'IJ',chr(196).chr(179) => 'ij',
            chr(196).chr(180) => 'J', chr(196).chr(181) => 'j',
            chr(196).chr(182) => 'K', chr(196).chr(183) => 'k',
            chr(196).chr(184) => 'k', chr(196).chr(185) => 'L',
            chr(196).chr(186) => 'l', chr(196).chr(187) => 'L',
            chr(196).chr(188) => 'l', chr(196).chr(189) => 'L',
            chr(196).chr(190) => 'l', chr(196).chr(191) => 'L',
            chr(197).chr(128) => 'l', chr(197).chr(129) => 'L',
            chr(197).chr(130) => 'l', chr(197).chr(131) => 'N',
            chr(197).chr(132) => 'n', chr(197).chr(133) => 'N',
            chr(197).chr(134) => 'n', chr(197).chr(135) => 'N',
            chr(197).chr(136) => 'n', chr(197).chr(137) => 'N',
            chr(197).chr(138) => 'n', chr(197).chr(139) => 'N',
            chr(197).chr(140) => 'O', chr(197).chr(141) => 'o',
            chr(197).chr(142) => 'O', chr(197).chr(143) => 'o',
            chr(197).chr(144) => 'O', chr(197).chr(145) => 'o',
            chr(197).chr(146) => 'OE',chr(197).chr(147) => 'oe',
            chr(197).chr(148) => 'R',chr(197).chr(149) => 'r',
            chr(197).chr(150) => 'R',chr(197).chr(151) => 'r',
            chr(197).chr(152) => 'R',chr(197).chr(153) => 'r',
            chr(197).chr(154) => 'S',chr(197).chr(155) => 's',
            chr(197).chr(156) => 'S',chr(197).chr(157) => 's',
            chr(197).chr(158) => 'S',chr(197).chr(159) => 's',
            chr(197).chr(160) => 'S', chr(197).chr(161) => 's',
            chr(197).chr(162) => 'T', chr(197).chr(163) => 't',
            chr(197).chr(164) => 'T', chr(197).chr(165) => 't',
            chr(197).chr(166) => 'T', chr(197).chr(167) => 't',
            chr(197).chr(168) => 'U', chr(197).chr(169) => 'u',
            chr(197).chr(170) => 'U', chr(197).chr(171) => 'u',
            chr(197).chr(172) => 'U', chr(197).chr(173) => 'u',
            chr(197).chr(174) => 'U', chr(197).chr(175) => 'u',
            chr(197).chr(176) => 'U', chr(197).chr(177) => 'u',
            chr(197).chr(178) => 'U', chr(197).chr(179) => 'u',
            chr(197).chr(180) => 'W', chr(197).chr(181) => 'w',
            chr(197).chr(182) => 'Y', chr(197).chr(183) => 'y',
            chr(197).chr(184) => 'Y', chr(197).chr(185) => 'Z',
            chr(197).chr(186) => 'z', chr(197).chr(187) => 'Z',
            chr(197).chr(188) => 'z', chr(197).chr(189) => 'Z',
            chr(197).chr(190) => 'z', chr(197).chr(191) => 's'
        );

        return strtr($string, $chars);
    }
}

if (!function_exists('get_all_contracts_secop_api')) {
    function get_all_contracts_secop_api()
    {
        try {
            $secopAPI = new SecopAPI();
            $contracts = $secopAPI->getAllContracts();
            return $contracts ?? [];
        } catch (InvalidResourceException $exception) {
            return false;
        }
    }
}

if (!function_exists('get_all_active_contracts_secop_api')) {
    function get_all_active_contracts_secop_api()
    {
        try {
            $secopAPI = new SecopAPI();
            $contracts = $secopAPI->getAllActivesContracts();
            return $contracts ?? [];
        } catch (InvalidResourceException $exception) {
            return false;
        }
    }
}

if (!function_exists('get_all_active_process_contracts_secop_api')) {
    function get_all_active_process_contracts_secop_api()
    {
        try {
            $secopAPI = new SecopAPI();
            $process = $secopAPI->getAllProcessContracts();
            return $process ?? [];
        } catch (InvalidResourceException $exception) {
            return false;
        }
    }
}

if (!function_exists('get_active_contracts_secop_api')) {
    function get_active_contracts_secop_api($document)
    {
        try {
            $secopAPI = new SecopAPI();
            $contracts = $secopAPI->getActiveContracts($document);
            return $contracts ?? [];
        } catch (InvalidResourceException $exception) {
            return false;
        }
    }
}

if (!function_exists('get_active_contract_secop_api')) {
    function get_active_contract_secop_api($document, $contract)
    {
        try {
            $secopAPI = new SecopAPI();
            $contracts = $secopAPI->getActiveContracts($document) ?? [];
            $contract = $contracts->where('referencia_del_contrato', $contract)->first();
            return $contract;
        } catch (InvalidResourceException $exception) {
            return false;
        }
    }
}

if (!function_exists('get_invoice_secop_api')) {
    function get_invoice_secop_api($document, $number)
    {
        try {
            $secopAPI = new SecopAPI();
            return $secopAPI->getInvoce($document, $number);
        } catch (InvalidResourceException $exception) {
            return false;
        }
    }
}

if (!function_exists('get_contracts_secop_api')) {
    function get_contracts_secop_api($document)
    {
        try {
            $secopAPI = new SecopAPI();
            $contracts = $secopAPI->getContracts($document);
            return $contracts ?? [];
        } catch (InvalidResourceException $exception) {
            return false;
        }
    }
}

if (!function_exists('get_all_contracts_secop_api')) {
    function get_all_contracts_secop_api()
    {
        try {
            $secopAPI = new SecopAPI();
            $contracts = $secopAPI->getAllContracts();
            return $contracts ?? [];
        } catch (InvalidResourceException $exception) {
            return false;
        }
    }
}

if (!function_exists('getBankAccountDigits')) {
    function getBankAccountDigits(): array
    {
        return [
            40 => 12,
            52 => 9,
            59 => 16,
            7 => 11,
            13 => 10,
            1 => 9,
            32 => 11,
            9 => 10,
            19 => 10,
            67 => 12,
            292 => 9,
            61 => 12,
            558 => 13,
            51 => 12,
            62 => 12,
            12 => 11,
            6 => 9,
            23 => 9,
            60 => 9,
            2 => 12,
        ];
    }
}


if ( !function_exists('webservice_logger') ) {
    function webservice_logger(
        $user_id = null,
        $user = null,
        $url = null,
        $request_type = null,
        $request = null,
        $status = null,
        $response = null
    ) {
        try {
            $webservice = new WebServiceStatus();
            // Log de envío
            $webservice->user_id = $user_id;
            $webservice->user = $user;
            $webservice->url = $url;
            $webservice->request_type = $request_type;
            $webservice->request = $request;
            $webservice->status = $status;
            $webservice->response = $response;
            $webservice->save();
            return true;
        } catch (\Exception $exception) {
            return false;
        }
    }
}
