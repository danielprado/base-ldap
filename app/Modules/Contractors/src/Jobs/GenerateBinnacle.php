<?php


namespace App\Modules\Contractors\src\Jobs;

use App\Jobs\NotifyUserOfCompletedExport;
use App\Models\Security\Subdirectorate;
use App\Models\Security\User;
use App\Modules\Contractors\src\Exports\BinnacleExport;
use GuzzleHttp\Client;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Log;
use Imtigger\LaravelJobStatus\JobStatus;
use Imtigger\LaravelJobStatus\Trackable;
use Maatwebsite\Excel\Excel;


class GenerateBinnacle implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Trackable;

    /**
     * @var array
     */
    private $request;

    /**
     * @var array
     */
    private $params;

    /**
     * @var string
     */
    private $name;

    /**
     * @var User
     */
    private $user;

    /**
     * Create a new job instance.
     *
     * @param array $request
     * @param User $user
     * @param array $params
     */
    public function __construct(array $request, User $user, array $params = [])
    {
        $this->name = "PORTAL-CONTRATISTA-PROCESO-BITACORA-".random_img_name();
        $this->params = array_merge(['key' => $this->name], $params);
        $this->request = $request;
        $this->prepareStatus($this->params);
        $this->user = $user;
        $this->setInput([
            'request' => $this->request,
            'user' => $this->user
        ]);
    }

    /**
     * Execute the job.
     *
     *
     * @return void
     */
    public function handle()
    {
        ini_set('max_execution_time', 7200); // 2 horas
        ini_set('memory_limit', '1024M');

        $this->update([
            'queue' => 'post-generate-binnacle',
            'status' => JobStatus::STATUS_EXECUTING,
            'finished_at' => null,
        ]);

        $job = JobStatus::query()->where('key', $this->name)->first();

        try {
            $http = new Client(['timeout' => 7200]);
            $response = $http->put(env('URL_PLAN_PAYMENT') . "/payrolls/binnacle", [
                'json' => $this->request,
            ]);

            $data = json_decode($response->getBody()->getContents(), true);

            $results = [];

            foreach ($data as $item) {
                $result = [
                    'contract' => $item['contract'],
                    'message' => $item['message'],
                    'isSuccessfull' => $item['isSuccessfull'],
                ];
                $results[] = $result;
            }

            if ($job) {
                $job->status = JobStatus::STATUS_FINISHED;
                $job->output = json_encode($results);
                $job->finished_at = now();
                $job->save();
            }

        } catch (\Exception $e) {
            Log::error("Error in GenerateBinnacle job: " . $e->getMessage());
            if (isset($job)) {
                $job->status = JobStatus::STATUS_FAILED;
                $job->output = json_encode(['error' => $e->getMessage()]);
                $job->finished_at = now();
                $job->save();
            }
        }

        /*$payroll_id = $this->request['payrollId'];
        $responseKeys = $http->get(env('URL_PLAN_PAYMENT') . "/payrolls/report/$payroll_id");
        $keys = json_decode($responseKeys->getBody()->getContents(), true);
        $payroll = collect($keys);
        $sub_address = Subdirectorate::query()->find($payroll['subdirectionId']);*/

        /*(new BinnacleExport($payroll, $sub_address))
            ->queue("exports/$this->name", 'local', Excel::XLSX)
            ->chain([
                new NotifyUserOfCompletedExport($this->user, $this->name, $url)
            ]);

        $this->setOutput(['file' => $this->name]);*/
    }
}
