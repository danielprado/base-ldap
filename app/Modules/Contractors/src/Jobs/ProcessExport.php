<?php


namespace App\Modules\Contractors\src\Jobs;


use App\Jobs\NotifyUserOfCompletedExport;
use App\Models\Security\Subdirectorate;
use App\Models\Security\User;
use App\Modules\Contractors\src\Constants\GlobalQuery;
use App\Modules\Contractors\src\Exports\DataExport;
use App\Modules\Contractors\src\Models\ContractorView;
use Carbon\Carbon;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Imtigger\LaravelJobStatus\JobStatus;
use Imtigger\LaravelJobStatus\Trackable;
use Maatwebsite\Excel\Excel;

class ProcessExport implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, Trackable;

    /**
     * @var array
     */
    private $request;

    /**
     * @var array
     */
    private $params;

    /**
     * @var string
     */
    private $name;

    /**
     * @var User
     */
    private $user;

    /**
     * Create a new job instance.
     *
     * @param array $request
     * @param User $user
     * @param array $params
     */
    public function __construct(array $request, User $user, array $params = [])
    {
        $this->name = "PORTAL-CONTRATISTA-".random_img_name().".xlsx";
        $this->params = array_merge(['key' => $this->name], $params);
        $this->request = $request;
        $this->prepareStatus($this->params);
        $this->user = $user;
        $this->setInput([
            'request' => $this->request,
            'user' => $this->user
        ]);
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $this->update(
            [
                'queue' => 'excel-contractor-portal',
                'status' => JobStatus::STATUS_EXECUTING,
                'finished_at' => null,
            ]
        );
        $path = config('app.env') != 'production'
            ? "/portal-contratista-dev"
            : "/";
        $subdomain = config('app.env') != 'production' ? "sim" : "portalcontratista";
        $url = "https://{$subdomain}.idrd.gov.co{$path}/es/login";

        $request = collect($this->request);
        $contractors = [];
        ContractorView::with('contracts')
            ->when($request->has('doesnt_have_arl'), function ($q) {
                return $q->whereNull('modifiable')
                    ->whereHas('contracts', function ($query) {
                        $contracts = new GlobalQuery();
                        return $query->whereIn('id', $contracts->contracts());
                    });
            })
            ->when($request->has('doesnt_have_secop'), function ($q) {
                return $q->whereHas('contracts', function ($query) {
                    $contracts = new GlobalQuery();
                    return $query->whereIn('id', $contracts->contracts('other_files_count'));
                });
            })
            ->when(
                $request->has(['start_date', 'final_date']),
                function ($query) use ($request) {
                    return $query
                        ->whereHas('contracts', function ($query) use ($request) {
                            return $query->where('start_date', '>=', $request->get('start_date'))
                                ->where('final_date', '<=', $request->get('final_date'));
                        });
                }
            )
            ->when($request->has('document'), function ($q) use ($request) {
                return $q->where('document', $request->get('document'));
            })
            ->when($request->has('contract'), function ($q) use ($request) {
                $data = $request->get('contract');
                return $q->whereHas('contracts', function ($query) use ($data) {
                    return $query->where('contract', 'like', "%{$data}%");
                });
            })
            ->when($request->has('doesnt_have_data'), function ($q) use ($request) {
                return $q->whereNotNull('modifiable');
            })
            ->when($request->has('actives'), function ($q) use ($request) {
                return $q->whereHas('contracts', function ($query) {
                    return $query->whereDate('final_date', '>=', now()->format('Y-m-d'))
                        ->orderBy('created_at', 'desc');
                });
            })
            ->when($request->has('recreation'), function ($q) use ($request) {
                return $q->whereHas('contracts', function ($query) {
                    return $query->where('subdirectorate_id', Subdirectorate::RECREATION)
                        ->whereDate('final_date', '>=', now()->format('Y-m-d'))
                        ->orderBy('updated_at', 'desc');
                });
            })
            ->when($request->has(['birthdate_start', 'birthdate_final']), function ($q) use ($request) {
                $from = Carbon::parse($request->get('birthdate_start'))->format('m-d');
                $till = Carbon::parse($request->get('birthdate_final'))->format('m-d');
                return $q->whereRaw("DATE_FORMAT(birthdate, '%m-%d') BETWEEN '$from' AND '$till'")
                    ->whereHas('contracts', function ($query) {
                        return $query->whereDate('final_date', '>=', now());
                    });
            })
            ->orderBy('document', 'desc')
            ->chunk(100, function ($models) use (&$contractors) {
                foreach ($models as $model) {
                    $contractors[] = [
                        'id' => $model->id,
                        'contracts' => $model->contracts->pluck('id')->toArray()
                    ];
                }
            });
        $keys =  [
            'contractors' => collect($contractors)->pluck('id')->toArray(),
            'contracts' => collect($contractors)->pluck('contracts')->flatten()->toArray(),
        ];

        (new DataExport($keys, $this->getJobStatusId(), $this->user))
            ->queue("exports/$this->name", 'local', Excel::XLSX)
            ->chain([
                new NotifyUserOfCompletedExport($this->user, $this->name, $url)
            ]);
        $this->setOutput(['file' => $this->name]);
    }
}
