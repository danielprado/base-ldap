<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePayrollsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('payrolls', function (Blueprint $table) {
            $table->boolean('reserve')->default(false)->after('verified')->comment('Identifica si es Reserva o Vigencia');
        });

        Schema::table('compliance', function (Blueprint $table) {
            $table->unsignedInteger('dependent_value')
                ->default(null)
                ->nullable()
                ->after('has_children');
        });

        Schema::table('destinity_code', function (Blueprint $table) {
            $table->unsignedBigInteger('subdirectorate_id')->nullable()->after('code')->comment('Subdirección a la que pertenece');

            $ldap_database = env('DB_LDAP_DATABASE');
            $table->foreign('subdirectorate_id')
                ->references('id')
                ->on("{$ldap_database}.subdirectorates")
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('payrolls', function (Blueprint $table) {
            $table->dropColumn('reserve');
        });

        Schema::table('compliance', function (Blueprint $table) {
            $table->dropColumn('dependent_value');
        });

        Schema::table('destinity_code', function (Blueprint $table) {
            $table->dropColumn('subdirectorate_id');
            $table->dropForeign('subdirectorate_id_foreign');
        });
    }
}
