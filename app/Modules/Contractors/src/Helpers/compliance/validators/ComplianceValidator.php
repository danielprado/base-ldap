<?php

namespace App\Modules\Contractors\src\Helpers\compliance\validators;

use App\Modules\Contractors\src\Helpers\compliance\traits\HasValidators;

abstract class ComplianceValidator implements Validator
{
  use HasValidators;
}