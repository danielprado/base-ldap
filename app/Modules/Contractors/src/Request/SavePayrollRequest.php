<?php


namespace App\Modules\Contractors\src\Request;


use Illuminate\Foundation\Http\FormRequest;

class SavePayrollRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    public function rules()
    {
        return [
            "area_id" => "required|numeric",
            "validity" => "required|boolean",
            "status" => "required|string",
            "datePayroll" => "required|string",
            "compliances" => "required|array",
        ];
    }
}
