<?php

namespace App\Modules\PaymentGateway\src\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class BookingResource extends JsonResource
{
    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'                    =>  (int) isset($this->id) ? (int) $this->id : null,
            'date'                  =>  $this->date ?? null,
            'start_hour'            =>  isset($this->start_hour) ? Carbon::parse($this->start_hour)->format('g:i A') : null,
            'final_hour'            =>  isset($this->final_hour) ? Carbon::parse($this->final_hour)->format('g:i A') : null,
            'hour_range_text'       =>  $this->setRangeHoursForHumans($this->start_hour, $this->final_hour),
            'hour_range'            =>  $this->hour_range ?? null,
            'name'                  =>  isset($this->name) ? toUpper($this->name) : null,
            'park'                  =>  isset($this->park) ? toUpper($this->park) : null,
            'address'               =>  isset($this->address) ? toUpper($this->address) : null,
            'stratum'               =>  $this->stratum ?? null,
            'endowment_type'        =>  isset($this->endowment_type) ? toUpper($this->endowment_type) : null,
            'equipment'             =>  isset($this->equipment) ? toUpper($this->equipment) : null,
            'total'                 =>  str_replace(' ', '', cop_money_format($this->total, '$', null, '0', null, '.')) ?? null,
            'service_name'          =>  isset($this->service_name) ? toUpper($this->service_name) : null,
            'document_type'         =>  $this->document_type ?? null,
            'document'              =>  $this->document ?? null,
            'payment_code'          =>  $this->payment_code ?? null,
            'pse_transaction_id'    =>  $this->pse_transaction_id ?? null,
            'payer_email'           =>  isset($this->payer_email) ? toUpper($this->payer_email) : null,
            'payer_name'            =>  isset($this->payer_name) ? toUpper($this->payer_name) : null,
            'payer_surname'         =>  isset($this->payer_surname) ? toUpper($this->payer_surname) : null,
            'payer_phone'           =>  $this->payer_phone ?? null,
            'status_id'             =>  $this->status_id ?? null,
            'status_bank'           =>  $this->setStatus($this->status_id, $this->status_bank),
            'concept'               =>  isset($this->concept) ? toUpper($this->concept) : null,
            'permission_type'       =>  isset($this->permission_type) ? toUpper($this->permission_type) : null,
            'payer_total'           =>  $this->payer_total ?? null,
            'pse_user_id'           =>  $this->pse_user_id ?? null,
            'created_at'            =>  isset($this->created_at) ? Carbon::parse($this->created_at)->format('d-m-Y H:i') : null,
            'pse_park_id'           =>  $this->pse_park_id ?? null,
        ];
    }
    public function setRangeHoursForHumans($start_hour, $final_hour): ? string
    {
        $start = Carbon::createFromTimeString($start_hour);
        $end = Carbon::createFromTimeString($final_hour);
        $diff = $end->longAbsoluteDiffForHumans($start);
        return $diff ?? null;
    }

    public function setStatus($status_id, $status_bank)
    {
        $status = $status_id == 2 ? 'Aprobado' : 'Denegado';
        return $status ?? $status_bank;
    }
}
