<?php

namespace App\Http\Middleware;

use Illuminate\Http\Request;
use Silber\Bouncer\Bouncer;

use Closure;

class ScopeBouncer
{
    /**
     * The Bouncer instance.
     *
     * @var \Silber\Bouncer\Bouncer
     */
    protected $bouncer;

    /**
     * Constructor.
     *
     * @param \Silber\Bouncer\Bouncer  $bouncer
     */
    public function __construct(Bouncer $bouncer)
    {
        $this->bouncer = $bouncer;
    }

    /**
     * Set the proper Bouncer scope for the incoming request.
     *
     * @param Request $request
     * @param Closure $next
     * @param $identifier
     * @return mixed
     */
    public function handle($request, Closure $next, $identifier = null)
    {
        // Here you may use whatever mechanism you use in your app
        // to determine the current tenant. To demonstrate, the
        // $tenantId is set here from the user's account_id.
        // $tenantId = $request->user()->account_id;

        if ($identifier) {
            $this->bouncer->scope()->to($identifier);
        }

        return $next($request);
    }
}
