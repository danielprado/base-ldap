<?php


namespace App\Modules\Contractors\src\Request;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'roles'  => 'required|array',
            'subdirectorate_id' => 'nullable|numeric|exists:mysql_ldap.subdirectorates,id',
            'areas' => 'nullable|array|exists:mysql_ldap.areas,id',
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     * @return void
     */
    public function withValidator($validator)
    {
        if ($this->method() !== 'DELETE') {
            $validator->sometimes('subdirectorate_id', 'required', function ($input) {
                return in_array('contractors-portal-authorizing-officer', $input->roles);
            });

            $validator->sometimes('areas', 'required|array', function ($input) {
                return in_array('contractors-portal-supervisor', $input->roles) ||
                    in_array('contractors-portal-supervisor-support', $input->roles) ||
                    in_array('contractors-portal-supervisor-technical-support', $input->roles);
            });
        }
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'subdirectorate_id.required' => 'El campo subdirección es obligatorio cuando se selecciona el rol correspondiente.',
            'areas.required' => 'El campo dependencia es obligatorio cuando se selecciona el rol correspondiente.',
        ];
    }

}
