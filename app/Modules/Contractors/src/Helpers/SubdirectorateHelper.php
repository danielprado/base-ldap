<?php

namespace App\Modules\Contractors\src\Helpers;

use App\Modules\Contractors\src\Helpers\clients\OrfeoClient;

class SubdirectorateHelper 
{
  private $originalSubdirectorates;
  private $groupedSubdirectorates;

  private $client;

  private function __construct(OrfeoClient $client) {
    $this->client = $client;
  }

  public static function fetch(): self {
    $instance = new self(resolve(OrfeoClient::class));
    $instance->fetchSubdirectorates();

    return $instance;
  }

  private function fetchSubdirectorates() {
    $this->originalSubdirectorates = $this->getOriginalSubdirectorates();
    $this->groupedSubdirectorates = $this->getGroupedSubdirectorates();
  }

  private function getOriginalSubdirectorates(): array {
    return [
      [
        'ID' => 0,
        'NOMBRE_AREA' => 'DIRECCIÓN GENERAL',
        'CODIGO_AREA' => '100',
        'NOMBRE_ENCARGADO' => 'BLANCA INES DURAN HERNANDEZ',
        'DOCUMENTO_ENCARGADO' => '51987429',
        'CARGO_ENCARGADO' => 'JEFE',
        'CODIGO_PADRE' => '100',
        'ES_SUBDIRECCION' => '0'
      ],
      [
        'ID' => 1,
        'NOMBRE_AREA' => 'OFICINA ASESORA DE DIRECCIÓN',
        'CODIGO_AREA' => '101',
        'NOMBRE_ENCARGADO' => 'ANA ADELA GELACIO RODRIGUEZ',
        'DOCUMENTO_ENCARGADO' => '51593601',
        'CARGO_ENCARGADO' => 'JEFE',
        'CODIGO_PADRE' => '100',
        'ES_SUBDIRECCION' => '0'
      ],
      [
        'ID' => 2,
        'NOMBRE_AREA' => 'AREA SISTEMAS',
        'CODIGO_AREA' => '102',
        'NOMBRE_ENCARGADO' => 'ANA ADELA GELACIO RODRIGUEZ',
        'DOCUMENTO_ENCARGADO' => '51593601',
        'CARGO_ENCARGADO' => 'JEFE',
        'CODIGO_PADRE' => '101',
        'ES_SUBDIRECCION' => '0'
      ],
    ];
  }

  private function getGroupedSubdirectorates(): array {
    return collect($this->originalSubdirectorates)->reduce(function($grouped, $subdirectorate) {
      if ($subdirectorate['CODIGO_PADRE'] != $subdirectorate['CODIGO_AREA']) {
        $parent = $this->getSubdirectorateRecursive($subdirectorate['CODIGO_PADRE']);

        array_push($grouped[$parent['CODIGO_AREA']]['areas'], [
          'id' => $subdirectorate['ID'],
          'code' => $subdirectorate['CODIGO_AREA'],
          'name' => $subdirectorate['NOMBRE_AREA']
        ]);

        return $grouped;
      }

      $grouped[$subdirectorate['CODIGO_AREA']] = [
        'id' => $subdirectorate['ID'],
        'code' => $subdirectorate['CODIGO_AREA'],
        'name' => $subdirectorate['NOMBRE_AREA'],
        'areas' => []
      ];

      return $grouped;
    }, []);
  }

  public function getAllSubdirectorates(): array {
    return $this->groupedSubdirectorates;
  }

  public function getSubdirectorate($code): array {
    return $this->groupedSubdirectorates[$code];
  }

  public function getArea($subdirectorateCode, $areaCode) {
    $subdirectorate = $this->getSubdirectorate($subdirectorateCode);
    return $subdirectorate['areas'][$areaCode];
  }

  private function getSubdirectorateRecursive($parent): array {
    $subdirectorateParent = collect($this->originalSubdirectorates)
      ->firstWhere('CODIGO_AREA', $parent);

    if ($subdirectorateParent['CODIGO_AREA'] === $subdirectorateParent['CODIGO_PADRE']) {
      return $subdirectorateParent;
    }
    
    return $this->getSubdirectorateRecursive($subdirectorateParent['CODIGO_PADRE']);
  }
}