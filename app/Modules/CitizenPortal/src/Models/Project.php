<?php

namespace App\Modules\CitizenPortal\src\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Arr;
use OwenIt\Auditing\Contracts\Auditable;

class Project extends Model implements Auditable
{
    use SoftDeletes, \OwenIt\Auditing\Auditable;

    protected $connection = "mysql_citizen_portal";

    protected $table = "projects";

    protected $primaryKey = "id";

    protected $fillable = [
        'name'
    ];

    protected $appends = [ 'name' ];

    protected $auditInclude = [
        'name'
    ];
}
