<?php

namespace App\Modules\Parks\src\Help;

use App\Modules\Parks\src\Constants\Utils;
use App\Modules\Parks\src\Exports\ExportEndowmentAll;
use App\Modules\Parks\src\Exports\ExportFurnitureAll;
use Maatwebsite\Excel\Facades\Excel;
use ZipArchive;

class ReportExporter
{
    public function exportFurnitureReport($allFeatures)
    {
        $currentPage = 1;
        $recordsPerPage = Utils::REGISTERS;
        $tempFiles = [];
        $remainingRecords = count($allFeatures);
        while ($remainingRecords > 0) {
            $export = new ExportFurnitureAll(collect($allFeatures), $currentPage);
            $exportedRecords = min($recordsPerPage, $remainingRecords);
            $fileName = 'REPORTE_PAGE_' . $currentPage . '.xlsx';
            Excel::store($export, 'temp/' . $fileName);
            $tempFiles[] = 'temp/' . $fileName;
            $remainingRecords -= $exportedRecords;
            $currentPage++;
        }

        $zipFileName = 'REPORTE_MOBILIARIO.zip';
        $zip = new ZipArchive();
        $zip->open($zipFileName, ZipArchive::CREATE | ZipArchive::OVERWRITE);

        foreach ($tempFiles as $tempFile) {
            $zip->addFile(storage_path('app/' . $tempFile), basename($tempFile));
        }
        $zip->close();

        return $zipFileName;
    }

    public function exportEndowmentReport($allFeatures)
    {
        $currentPage = 1;
        $recordsPerPage = Utils::REGISTERS;
        $tempFiles = [];
        $remainingRecords = count($allFeatures);
        while ($remainingRecords > 0) {
            $export = new ExportEndowmentAll(collect($allFeatures), $currentPage);
            $exportedRecords = min($recordsPerPage, $remainingRecords);
            $fileName = 'REPORTE_PAGE_' . $currentPage . '.xlsx';
            Excel::store($export, 'temp/' . $fileName);
            $tempFiles[] = 'temp/' . $fileName;
            $remainingRecords -= $exportedRecords;
            $currentPage++;
        }

        $zipFileName = 'REPORTE_DOTACIONES.zip';
        $zip = new ZipArchive();
        $zip->open($zipFileName, ZipArchive::CREATE | ZipArchive::OVERWRITE);

        foreach ($tempFiles as $tempFile) {
            $zip->addFile(storage_path('app/' . $tempFile), basename($tempFile));
        }
        $zip->close();

        return $zipFileName;
    }
}
