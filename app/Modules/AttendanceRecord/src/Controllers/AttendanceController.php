<?php

namespace App\Modules\AttendanceRecord\src\Controllers;


use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Modules\AttendanceRecord\src\Help\Helpers;
use App\Modules\AttendanceRecord\src\Models\Attendee;
use App\Modules\AttendanceRecord\src\Models\Disability;
use App\Modules\AttendanceRecord\src\Models\Module;
use App\Modules\AttendanceRecord\src\Models\Paidcare;
use App\Modules\AttendanceRecord\src\Models\Population;
use App\Modules\AttendanceRecord\src\Models\Unpaidcare;
use Illuminate\Http\Request;

/**
 * @group Pasarela de pagos - Parques
 *
 * API para la gestión y consulta de datos de Parques Pse
 */
class AttendanceController extends Controller
{
      /**
       * Initialise common request params
       */
      public function __construct()
      {
            parent::__construct();
      }

      /**
       * @group Pasarela de pagos - Parques
       *
       * Parques
       *
       * Muestra un listado del recurso.
       *
       *
       * @return JsonResponse
       */
      public function getuser(Request $request)
      {
            $help = new Helpers();
            $Authorization = $request->header('Authorization');
            try {
                  $module = Module::where('token', '=', $Authorization)->first();
                  if (!$module) {
                        return $help->messageerror('No tienes autorizacion para esta funcionalidad', null, 403);
                  }
                  if ($module->state == 0) {
                        return $help->messageerror('No tienes autorizacion para esta funcionalidad, modulo en estado suspendido', null, 403);
                  }

                  $attendee = Attendee::with('disabilities', 'paidcares', 'populations', 'unpaidcares')
                        ->where('number_document', '=', $request->number_document)
                        ->get();
                  return $help->messagesuccess(count($attendee) > 0  ? 'Beneficiario Encontrado' : 'Beneficiario Inexistente', $attendee, 200);
            } catch (\Exception $ex) {
                  return $help->messagesuccess('Error', $ex->getMessage(), 500);
            }
      }

      public function getuserrecreovia(Request $request)
      {
            $help = new Helpers();
            $Authorization = $request->header('Authorization');
            try {
                  $module = Module::where('token', '=', $Authorization)->first();
                  if (!$module) {
                        return $help->messageerror('No tienes autorizacion para esta funcionalidad', null, 403);
                  }
                  if ($module->state == 0) {
                        return $help->messageerror('No tienes autorizacion para esta funcionalidad, modulo en estado suspendido', null, 403);
                  }

                  $attendee = Attendee::with(['disabilities', 'paidcares', 'populations', 'unpaidcares', 'assists' => function($q){
                        $q->where('assists.module_id', '=', 2);
                        $q->where('assists.date_assist', '=', date('Y-m-d'));
                        }])
                        ->where('number_document', '=', $request->number_document)
                        ->get();
                  return $help->messagesuccess(count($attendee) > 0  ? 'Beneficiario Encontrado' : 'Beneficiario Inexistente', $attendee, 200);
            } catch (\Exception $ex) {
                  return $help->messagesuccess('Error', $ex->getMessage(), 500);
            }
      }


      public function create(Request $request)
      {
            $help = new Helpers();
            $Authorization = $request->header('Authorization');
            try {
                  $module = Module::where('token', '=', $Authorization)->first();
                  if (!$module) {
                        return $help->messageerror('No tienes autorización para esta funcionalidad', null, 403);
                  }
                  if ($module->state == 0) {
                        return $help->messageerror('No tienes autorización para esta funcionalidad, modulo en estado suspendido', null, 403);
                  }
                  $message = 'Beneficiario Actualizado';
                  $module_id = (int)explode(":", base64_decode($Authorization))[1];
                  $attendee = Attendee::where('number_document', $request->number_document)->first();
                  if (!$attendee) {
                        $attendee = new Attendee;
                        $message = 'Beneficiario Creado';
                  }
                  $attendee->type_document = strtoupper($request->type_document);
                  $attendee->number_document = strtoupper($request->number_document);
                  $attendee->complete_names = strtoupper($request->complete_names);
                  $attendee->date_birth = strtoupper($request->date_birth);
                  $attendee->sex = strtoupper($request->sex);
                  $attendee->sexual_orientation = strtoupper($request->sexual_orientation);
                  $attendee->gender_identity = strtoupper($request->gender_identity);
                  $attendee->rh = strtoupper($request->rh);
                  $attendee->eps_id = $request->eps;
                  $attendee->phone = strtoupper($request->phone);
                  $attendee->country_birth_id = $request->country_birth;
                  $attendee->state_birth_id = $request->state_birth;
                  $attendee->city_birth_id = $request->city_birth;
                  $attendee->country_home_id = $request->country_home;
                  $attendee->state_home_id = $request->state_home;
                  $attendee->city_home_id = $request->city_home;
                  $attendee->civil_status = strtoupper($request->civil_status);
                  $attendee->stratum = $request->stratum;
                  $attendee->locality_home_id = $request->locality;
                  $attendee->upz_home_id = $request->upz_home;
                  $attendee->emergency_contac = strtoupper($request->emergency_contac);
                  $attendee->emergency_phone = strtoupper($request->emergency_phone);
                  $attendee->mail = strtoupper($request->mail);
                  $attendee->Adress = strtoupper($request->Adress);
                  $attendee->has_computed = strtoupper($request->has_computed);
                  $attendee->has_conectivity = strtoupper($request->has_conectivity);
                  $attendee->type_socialsecurity = strtoupper($request->type_socialsecurity);
                  $attendee->group_etnic = strtoupper($request->group_etnic);
                  $attendee->study_level = strtoupper($request->study_level);
                  $attendee->occupation = strtoupper($request->occupation);
                  $attendee->initial_skill = $request->initial_skill;
                  $attendee->progress_made = $request->progress_made;
                  $attendee->save();
                  foreach ($request->assists as $assist) {
                        $attendee->assists()->create([
                              'module_id' =>  $module_id,
                              'date_assist' => $assist['date_assist'],
                              'session_id' => $assist['session_id'] ? $assist['session_id'] : null,
                              'data' => $assist['data'],
                        ]);
                  }
                  $disabilities = Disability::find($request->disabilities);
                  $populations = Population::find($request->populations);
                  $paidcares = Paidcare::find($request->paidcares);
                  $unpaidcares = Unpaidcare::find($request->unpaidcares);

                  $attendee->disabilities()->sync($disabilities);
                  $attendee->populations()->sync($populations);
                  $attendee->paidcares()->sync($paidcares);
                  $attendee->unpaidcares()->sync($unpaidcares);

                  return $help->messagesuccess($message, null, 200);
            } catch (\Exception $ex) {
                  return $help->messageerror($ex->getMessage(), null, 500);
            }
      }
      
}
