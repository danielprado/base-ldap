<?php

namespace App\Modules\Certifications\src\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ContractTypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request): array
    {
        return [
            'id'    =>  isset($this->Id) ? (int) $this->Id : null,
            'name'  => $this->Nombre_Tipo_Contrato ?? null,
        ];
    }
}
