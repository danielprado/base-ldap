<?php

namespace App\Modules\Certifications\src\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Certifications\src\Models\CertificationContract;
use App\Modules\Certifications\src\Request\MemberRequest;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class MemberController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function store(MemberRequest $request, CertificationContract $contract): JsonResponse
    {
        $contract->members()
            ->create([
                'Tipo_Documento_Integrante_Id' => $request->get('document_type_id'),
                'Documento_Integrante' => $request->get('document'),
                'Nombre_Integrante' => toUpper($request->get('name')),
                'Porcentaje_Integrante' => toUpper($request->get('percent')),
            ]);

        return $this->success_message(
            __('validation.handler.success'),
            Response::HTTP_CREATED
        );
    }

    public function update(MemberRequest $request, CertificationContract $contract): JsonResponse
    {
        $member = $contract->members()
            ->where('Id', $request->get('id'))
            ->firstOrFail();

        $member->update([
            'Tipo_Documento_Integrante_Id' => $request->get('document_type_id'),
            'Documento_Integrante' => $request->get('document'),
            'Nombre_Integrante' => toUpper($request->get('name')),
            'Porcentaje_Integrante' => toUpper($request->get('percent')),
        ]);

        return $this->success_message(__('validation.handler.updated'));
    }

    /**
     * @throws Exception
     */
    public function destroy(CertificationContract $contract, $id): JsonResponse
    {
        $member = $contract->members()->find($id);
        if ($member) {
            $member->delete();
            return $this->success_message(__('validation.handler.deleted'));
        }
        return $this->error_response(__('validation.handler.unexpected_failure'));
    }
}
