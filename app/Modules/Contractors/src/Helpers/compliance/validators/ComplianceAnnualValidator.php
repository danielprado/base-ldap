<?php

namespace App\Modules\Contractors\src\Helpers\compliance\validators;

use Carbon\Carbon;

class ComplianceAnnualValidator extends ComplianceDateValidator
{
  private $startMonth;
  private $endMonth;

  private function __construct($startMonth, $endMonth)
  {
    $this->startMonth = $startMonth ?? 1;
    $this->endMonth = $endMonth;
    $this->useValidator(ComplianceMonthlyValidator::create());
  }

  public static function create($startMonth = null, $endMonth = null)
  {
    return new self($startMonth, $endMonth);
  }

  public function name(): string
  {
    return 'annual';
  }

  public function period(Carbon $date = null)
  {
    $date = $date ?? now();

    $monthlyPeriod = $this->getValidator('monthly')
      ->period(Carbon::create($date->year, $this->startMonth));

    $start = $monthlyPeriod['start'];

    $end = $this->endMonth 
      ? $monthlyPeriod['end']->setUnitNoOverflow('month', $this->endMonth, 'month')
      : $monthlyPeriod['end'];

    return compact('start', 'end');
  }
}