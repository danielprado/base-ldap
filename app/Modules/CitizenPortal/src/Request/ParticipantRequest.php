<?php


namespace App\Modules\CitizenPortal\src\Request;



use App\Modules\CitizenPortal\src\Constants\Roles;
use App\Modules\CitizenPortal\src\Models\Activity;
use App\Modules\CitizenPortal\src\Models\Modality;
use App\Modules\CitizenPortal\src\Models\ParticipantRole;
use App\Modules\CitizenPortal\src\Models\Profile;
use App\Modules\CitizenPortal\src\Models\Schedule;
use Illuminate\Foundation\Http\FormRequest;

class ParticipantRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /*
        $permission = toLower($this->getMethod()) == 'post'
            ? Roles::can(Activity::class,'create_or_manage', true)
            : Roles::can(Activity::class,'update_or_manage', true);
        */
        return auth('api')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $profile = new Profile();
        $roles = new ParticipantRole();
        return [
            'profile_id' => "required|numeric|exists:{$profile->getConnectionName()}.{$profile->getTable()},{$profile->getKeyName()}",
            'role_id'   =>  "required|numeric|exists:{$roles->getConnectionName()}.{$roles->getTable()},{$roles->getKeyName()}",
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name' =>  __('citizen.validations.name'),
        ];
    }
}
