<?php

namespace App\Modules\AttendanceRecord\src\Models;

use Illuminate\Database\Eloquent\Model;

class Cronograma extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_recreovia';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Cronogramas';
     protected $primaryKey = 'Id';

    public function sessions()
    {
        return $this->hasMany(Session::class, 'Id_Cronograma');
    }

    public function punto()
    {
    	return $this->belongsTo(Punto::class, 'Id_Punto');
    }

    public function jornada()
    {
    	return $this->belongsTo(Jornada::class, 'Id_Jornada');
    }


}
