<?php

namespace App\Modules\AttendanceRecord\src\Models;

use Illuminate\Database\Eloquent\Model;

class Jornada extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_recreovia';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'Jornadas';
    protected $primaryKey = 'Id_Jornada';


    public function cronogramas()
    {
        return $this->hasMany(Cronograma::class, 'Id_Jornada');
    }
}
