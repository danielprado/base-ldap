<?php

namespace App\Modules\PaymentGateway\src\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class ScheduleResource extends JsonResource
{
    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'day'                   =>  $this->Dia ?? null,
            'status'                =>  $this->setStatus($this->FechaHabilitacion, $this->FechaInhabilitacion),
            'schedule'              =>  $this->Horario ?? null,
            'id_park'               =>  $this->IdParque ?? null,
            'start_time'            =>  isset($this->HoraInicio) ? Carbon::parse($this->HoraInicio)->format('g:i A') : null,
            'end_time'              =>  isset($this->HoraFin) ? Carbon::parse($this->HoraFin)->format('g:i A') : null,
            'range_text'            =>  $this->setRangeHoursForHumans($this->HoraInicio, $this->HoraFin),
            'park_name'             =>  isset($this->NombreParque) ? toUpper($this->NombreParque) : null,
            'park_data'             =>  isset($this->DatosParque) ? toUpper($this->DatosParque) : null,
            'park_email'            =>  $this->Email_Parque ?? null,
            'id_endowment'          =>  $this->IdDotacion ?? null,
            'description_endowment' =>  isset($this->DescripcionDotacion) ? toUpper($this->DescripcionDotacion) : null,
            'date_enable'           =>  isset($this->FechaHabilitacion) ? Carbon::parse($this->FechaHabilitacion)->format('d-m-Y H:i') : null,
            'date_disable'          =>  isset($this->FechaInhabilitacion) ? Carbon::parse($this->FechaInhabilitacion)->format('d-m-Y H:i') : null,
        ];
    }
    public function setRangeHoursForHumans($start_time, $end_time): ? string
    {
        $start = Carbon::createFromTimeString($start_time);
        $end = Carbon::createFromTimeString($end_time);
        $diff = $end->longAbsoluteDiffForHumans($start);
        return $diff ?? null;
    }

    public function setStatus($enable, $disable): ? string
    {
        if (!is_null($enable) && is_null($disable)) {
            $status = 'enable';
        } elseif (!is_null($enable) && !is_null($disable)) {
            $status = 'disable';
        }
        return $status ?? null;
    }
}
