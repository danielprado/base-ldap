<?php

namespace App\Modules\Certifications\src\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ExtensionResource extends JsonResource
{
    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    public function toArray($request): array
    {
        return [
            'id' => isset($this->Id) ? (int)$this->Id : null,
            'contract_id' => isset($this->Contrato_Id) ? (int)$this->Contrato_Id : null,
            'number' => isset($this->Numero_Prorroga) ? (int) $this->Numero_Prorroga : null,
            'months' => isset($this->Meses) ? (int)$this->Meses : null,
            'days' => isset($this->Dias) ? (int)$this->Dias : null,
            'final_date' => isset($this->Fecha_Fin) ? $this->Fecha_Fin->format('Y-m-d') : null,
            'created_at' => isset($this->created_at) ? $this->created_at->format('Y-m-d') : null,
            'updated_at' => isset($this->updated_at) ? $this->updated_at->format('Y-m-d') : null,
        ];
    }

    public static function headers(): array
    {
        return [
            [
                'text' => "#",
                'value'  =>  "id",
                'sortable' => false
            ],
            [
                'align' => "center",
                'text' => "Número",
                'value'  =>  "number",
                'sortable' => false
            ],
            [
                'align' => "center",
                'text' => "Meses",
                'value'  =>  "months",
                'sortable' => false
            ],
            [
                'align' => "center",
                'text' => "Días",
                'value'  =>  "days",
                'sortable' => false
            ],
            [
                'align' => "center",
                'text' => "Fecha de finalización",
                'value'  =>  "final_date",
                'sortable' => false
            ],
            [
                'align' => "center",
                'text' => "Fecha de registro",
                'value'  =>  "created_at",
                'sortable' => false
            ],
            [
                'align'     => "center",
                'text'      => "Acciones",
                'value'     =>  "actions",
                'sortable'  => false
            ],
        ];
    }
}
