<?php

namespace App\Modules\Certifications\src\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class MemberResource extends JsonResource
{
    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    public function toArray($request): array
    {
        return [
            'id' => isset($this->Id) ? (int)$this->Id : null,
            'contract_id' => isset($this->Contrato_Id) ? (int)$this->Contrato_Id : null,
            'document_type_id' => isset($this->Tipo_Documento_Integrante_Id) ? (int)$this->Tipo_Documento_Integrante_Id : null,
            'document_type' => $this->document_type->name ?? null,
            'document' => isset($this->Documento_Integrante) ? (int)$this->Documento_Integrante : null,
            'name' => $this->Nombre_Integrante ?? null,
            'percent' => isset($this->Porcentaje_Integrante) ? (int)$this->Porcentaje_Integrante : null,
            'created_at' => isset($this->created_at) ? $this->created_at->format('Y-m-d') : null,
            'updated_at' => isset($this->updated_at) ? $this->updated_at->format('Y-m-d') : null,
        ];
    }

    public static function headers(): array
    {
        return [
            [
                'text' => "#",
                'value'  =>  "id",
                'sortable' => false
            ],
            [
                'align' => "right",
                'text' => "Tipo de documento",
                'value'  =>  "document_type",
                'sortable' => false
            ],
            [
                'align' => "right",
                'text' => "Documento",
                'value'  =>  "document",
                'sortable' => false
            ],
            [
                'align' => "right",
                'text' => "Nombre Integrante",
                'value'  =>  "name",
                'sortable' => false
            ],
            [
                'align' => "right",
                'text' => "Porcentaje",
                'value'  =>  "percent",
                'sortable' => false
            ],
            [
                'align' => "right",
                'text' => "Fecha de registro",
                'value'  =>  "created_at",
                'sortable' => false
            ],
            [
                'align'     => "center",
                'text'      => "Acciones",
                'value'     =>  "actions",
                'sortable'  => false
            ],
        ];
    }
}
