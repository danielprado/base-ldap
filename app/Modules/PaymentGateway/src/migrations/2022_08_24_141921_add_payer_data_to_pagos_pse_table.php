<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPayerDataToPagosPseTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_pse')->table('pago_pse', function (Blueprint $table) {
            //
            $table->text('email_pagador')->nullable()->after('telefono');
            $table->text('nombre_pagador')->nullable()->after('email_pagador');
            $table->text('apellido_pagador')->nullable()->after('nombre_pagador');
            $table->text('telefono_pagador')->nullable()->after('apellido_pagador');
            $table->bigInteger('identificacion_pagador')->nullable()->after('telefono_pagador');
            $table->integer('tipo_identificacion_pagador')->nullable()->after('identificacion_pagador');
            $table->string('codigo_banco_seleccionado')->nullable()->after('estado_banco');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_pse')->table('pago_pse', function (Blueprint $table) {
            $table->dropColumn('email_pagador');
            $table->dropColumn('nombre_pagador');
            $table->dropColumn('apellido_pagador');
            $table->dropColumn('telefono_pagador');
            $table->dropColumn('identificacion_pagador');
            $table->dropColumn('tipo_identificacion_pagador');
            $table->dropColumn('codigo_banco_seleccionado');
        });
    }
}
