<?php

namespace App\Modules\Contractors\src\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Contractors\src\Models\Contractor;
use App\Modules\Contractors\src\Models\ContractorBank;
use App\Modules\Contractors\src\Models\UserSevenBank;
use GuzzleHttp\Client;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Throwable;

class UserBankController extends Controller
{

    public function sync(): JsonResponse
    {
        try {
            DB::connection('mysql_contractors')->beginTransaction();
            $listContractors = Contractor::query()->pluck('document')->all();

            $http = new Client([
                'base_uri' => env('URL_DOCKER_SERVER'),
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                ],
            ]);
            //$response = $http->post("/api/contractors-portal/users-seven-banks", [
            $response = $http->post("/api/contractors-portal/users-seven-banks", [
                'json' => [
                    'listDocuments' => $listContractors,
                ],
            ]);

            $usersSeven = json_decode($response->getBody()->getContents(), false);

            if (!is_null($usersSeven)) {
                foreach ($usersSeven as $user) {
                    Contractor::query()
                        ->select('id', 'document', 'name', 'bank_id')
                        ->where('document', $user->ter_codi)
                        ->chunk(100, function ($contractors) use ($user) {
                            foreach ($contractors as $contractor) {

                                $contractor['seven'] = $user;
                                $contractor['seven']->tip_cuen = $contractor['seven']->tip_cuen == 'A' ? 2 : 1;

                                $bank = ContractorBank::query()->updateOrCreate(
                                    ['id' => $contractor->bank_id],
                                    [
                                        'contractor_id' => $contractor->id,
                                        'bank_id' => $contractor->seven->ban_codi,
                                        'account_type_id' => $contractor->seven->tip_cuen,
                                        'number' => $contractor->seven->cue_nume
                                    ]
                                );
                                $contractor->bank_id = $bank->id;
                                Contractor::query()->where('id', $contractor->id)->update(['bank_id' => $bank->id]);
                            }
                        });
                }
            }
            DB::connection('mysql_contractors')->commit();
            return $this->success_message(__('validation.handler.success'));

        } catch (Throwable $e) {
            DB::connection('mysql_contractors')->rollBack();
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                422,
                $e->getMessage()
            );
        }

    }

    public function UsersSevenBank(Request $request)
    {
        return UserSevenBank::query()->select('ter_codi', 'tip_cuen', 'cue_nume', 'ban_codi', 'ban_nomb')
            ->whereIn('ter_codi', $request->get('listDocuments'))
            ->distinct()
            ->get();
    }
}
