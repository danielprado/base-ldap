<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contracts', function (Blueprint $table) {
            $table->unsignedBigInteger('subarea_id')->nullable()->after('dependency_id')->comment('Identificador de la subarea si aplica');

            $ldap_database = env('DB_LDAP_DATABASE');
            $table->foreign('subarea_id')
                ->references('id')
                ->on("{$ldap_database}.subareas")
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contracts', function (Blueprint $table) {
            $table->dropColumn('subarea_id');
            $table->dropForeign('subarea_id_foreign');
        });
    }
}
