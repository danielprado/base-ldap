<?php

namespace App\Modules\Contractors\src\Helpers\compliance\validators;

use App\Modules\Contractors\src\Helpers\compliance\validators\Validator;

class ComplianceParentValidator implements Validator
{
  private $parent;

  public function __construct(string $parent) {
    $this->parent = $parent;
  }

  public function name(): string
  {
    return 'parent';
  }

  public function validate($compliance): bool
  {
    return $compliance->contribution($this->parent)->isValid();
  }
}