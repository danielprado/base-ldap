<?php

namespace App\Http\Middleware;

use Closure;

class DisableContractorsPortal
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->is('api/contractors-portal*')) {
            abort(503, 'El sitio está en mantenimiento pronto serán restablecidos los servicios, Agradecemos tu comprensión, Gracias.');
        }
        return $next($request);
    }
}
