<?php

namespace App\Modules\AttendanceRecord\src\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Disability extends Model
{
      //
      use SoftDeletes;
      protected $connection = 'mysql_attendance';
      protected $table = 'disabilities';
      protected $dates = ['deleted_at'];
      protected $hidden = ['pivot', 'created_at', 'updated_at', 'deleted_at'];
  
      public function attendees()
      {
          return $this->belongsToMany(Attendee::class);
      }
}
