<?php

namespace App\Modules\Parks\src\Request;

use App\Modules\Parks\src\Constants\Roles;
use App\Modules\Parks\src\Models\Rupi;
use App\Modules\Parks\src\Rules\ParkFinderRule;
use Illuminate\Foundation\Http\FormRequest;

/**
 * @bodyParam name string required Código rupi. Example: 9-123
 */
class RupiRequest extends FormRequest
{
    public $forUpdate = false;
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $method = toLower($this->getMethod());
        $action = in_array($method, ['put', 'patch']) ? 'update' : 'create';
        $this->forUpdate = $action == 'update';
        /*
        return auth('api')->user()->can(Roles::can(Rupi::class, $action), Rupi::class) ||
            auth('api')->user()->can(Roles::can(Rupi::class, 'manage'), Rupi::class);
        */
        return auth('api')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rupi = new Rupi();
        $add = $this->forUpdate ? ",{$this->route('rupi')->id},{$rupi->getKeyName()}" : "";
        return [
            'name'  => "required|string|unique:{$rupi->getConnectionName()}.{$rupi->getTable()},Rupi{$add}",
        ];
    }

    public function attributes()
    {
        return [
            "name" => "rupi"
        ];
    }
}
