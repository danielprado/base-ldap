<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdatePlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('plans', function (Blueprint $table) {
            $table->string('financial_type')->default('TRANSFER')->after('total_payment')->comment('Tipo de financiación');
            $table->boolean('reserve')->default(false)->after('financial_type')->comment('Es reserva');
        });

        Schema::table('pmr_configurations', function (Blueprint $table) {
            $table->unsignedBigInteger('budget_chain_id')->nullable()->after('payment_plan_id')->comment('Identificador de la cadena presupuestal');
            $table->foreign('budget_chain_id')
                ->references('id')
                ->on('budget_chains')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('plans', function (Blueprint $table) {
            $table->dropColumn(['financial_type','reserve']);
        });

        Schema::table('pmr_configurations', function (Blueprint $table) {
            $table->dropColumn('budget_chain_id');
            $table->dropForeign('budget_chain_id_foreign');
        });
    }
}
