<?php

namespace App\Modules\PaymentGateway\src\Exports;

use App\Modules\CitizenPortal\src\Models\CitizenSchedule;
use App\Modules\CitizenPortal\src\Models\File;
use App\Modules\PaymentGateway\src\Models\Pago;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;

class PaymentzExport implements FromCollection, WithHeadings, WithMapping
{
      use Exportable;

      protected $paymentz;

      public function __construct($paymentz = null)
      {
            $this->paymentz = $paymentz;
      }

      /**
       * @return \Illuminate\Support\Collection
       */
      public function collection()
      {
            return $this->paymentz;
      }

      public function map($paymentz): array
      {
          $payment = File::query()
              ->where('pse', $paymentz->codigo_pago)
              ->first();
          $file = null;
          if ( isset($payment->file) && Storage::disk('citizen_portal')->exists($payment->file) ) {
              $file = "https://sim.idrd.gov.co/sim-ldap/storage/app/portal/{$payment->file}";
          }
            return [
                  $paymentz->id,
                  $paymentz->created_at,
                  $paymentz->total,
                  $paymentz->codigo_parque . $paymentz->codigo_servicio,
                  $paymentz->nombre_parque,
                  $paymentz->servicio_nombre,
                  $paymentz->identificacion,
                  $paymentz->email,
                  $paymentz->nombre,
                  $paymentz->apellido,
                  $paymentz->telefono,
                  $paymentz->concepto,
                  $paymentz->medio_pago,
                  $paymentz->id_transaccion_pse,
                  $paymentz->codigo_pago,
                  $paymentz->identificacion_pagador,
                  $paymentz->nombre_pagador . ' ' . $paymentz->apellido_pagador,
                  $paymentz->email_pagador,
                  $paymentz->telefono_pagador,
                  $file
            ];
      }

      public function headings(): array
      {
            return ['ID', 'FECHA', 'TOTAL', 'CODIGO', 'PARQUE', 'SERVICIO',  'IDENTIFICACION', 'EMAIL', 'NOMBRE', 'APELLIDO', 'TELEFONO', 'CONCEPTO', 'METODO PAGO', 'TRANSACCION ID PSE', 'CODIGO PAGO', 'IDENTIFICACION PAGADOR', 'NOMBRE PAGADOR', 'CORREO PAGADOR', 'TELEFONO PAGADOR', "EVIDENCIA CONSIGNACIÒN"];
      }
}
