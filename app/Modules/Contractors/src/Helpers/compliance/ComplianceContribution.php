<?php

namespace App\Modules\Contractors\src\Helpers\compliance;

use App\Modules\Contractors\src\Exceptions\InaccessibleComplianceException;
use App\Modules\Contractors\src\Helpers\compliance\traits\HasValidators;
use App\Modules\Contractors\src\Helpers\compliance\validators\ComplianceAnnualValidator;
use App\Modules\Contractors\src\Helpers\compliance\validators\ComplianceMonthlyValidator;
use App\Modules\Contractors\src\Helpers\compliance\validators\ComplianceParentValidator;
use App\Modules\Contractors\src\Models\Compliance;
use App\Modules\Contractors\src\Models\ComplianceFile;
use App\Modules\Contractors\src\Models\ComplianceFileType;

class ComplianceContribution
{
    use HasValidators;

    private $compliance;
    public $name;

    public function __construct(string $name, Compliance &$compliance)
    {
        $this->name = $name;
        $this->compliance = $compliance;
    }

    public static function from(Compliance &$compliance)
    {
        return function (string $name) use ($compliance) {
            return new self($name, $compliance);
        };
    }

    public function monthly(int $day = null, int $endDay = null)
    {
        return $this->useValidator(ComplianceMonthlyValidator::create($day, $endDay));
    }

    public function annual(int $startMonth = null, int $endMonth = null)
    {
        return $this->useValidator(
            ComplianceAnnualValidator::create($startMonth, $endMonth)
                ->useValidator($this->getValidator('monthly'))
        );
    }

    public function valid()
    {
        if (!$this->isValid()) {
            throw new InaccessibleComplianceException();
        }

        return $this;
    }

    public function isValid()
    {
        return collect($this->validators)
            ->every(function ($validator) {
                return $validator->validate($this->compliance);
            });
    }

    public function getFile(): ?ComplianceFile
    {
        return $this->compliance->getFieldFile($this->name);
    }

    public function setFile($file)
    {
        $fileType = ComplianceFileType::query()
            ->name($this->name)
            ->first(['id', 'name']);

        $fileName = ComplianceFile::getFileName($fileType, $this->compliance->contract_number);

        $complianceFile = $this->compliance->files()
            ->firstOrCreate(
                [ 'file_type' => $fileType->id ],
                [ 'file_type' =>  $fileType->id, 'file_name' =>  $fileName ]
            );

        $file ? $complianceFile->store($file) : $complianceFile->delete();
    }
}
