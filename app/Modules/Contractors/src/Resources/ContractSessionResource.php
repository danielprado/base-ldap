<?php

namespace App\Modules\Contractors\src\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use NumberFormatter;


class ContractSessionResource extends JsonResource
{

    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    public function toArray($request)
    {
        $total_sessions_completed = $this->getTotalSessionsCompleted($this->plans ?? []);
        $total_sessions_pending = $this->document_init->total_sessions - $total_sessions_completed;
        $available_value = $total_sessions_pending * $this->document_init->value_month;
        $execution_percentage = ($this->document_init->total_sessions > 0) ? ($total_sessions_completed / $this->document_init->total_sessions) * 100 : 0;
        
        return [
            'contractor_name' => $this->contractor->name.' '.$this->contractor->surname,
            'identification_number' => $this->contractor->document,
            'contract_number' => $this->contract,
            'sessions_contract' => $this->document_init->total_sessions,
            'sessions_completed' => $total_sessions_completed,
            'sessions_pending' => $total_sessions_pending, 
            'execution_percentage' => round($execution_percentage, 2) . '%',
            'value' => $this->getCurrencyFormat($available_value)
        ];
    }
    
    public function getTotalSessionsCompleted($plans) {
        $total_sessions = 0;
        foreach ($plans as $plan) {
            $total_sessions += (int)$plan['amount_day_pay'];
        }
        return $total_sessions;
    }
    
    public function getCurrencyFormat($value)
    {
        $formatter = new NumberFormatter('es_CO', NumberFormatter::CURRENCY);
        return $formatter->formatCurrency($value, 'COP');
    }

    public static function headers()
    {
        return [
            [
                "text" => "Nombre del Contratista",
                "value" => "contractor_name",
                "icon" => "mdi-file"
            ],
            [
                "text" => "CC",
                "value" => "identification_number",
                "icon" => "mdi-file"
            ],
            [
                "text" => "Contrato",
                "value" => "contract_number",
                "icon" => "mdi-file"
            ],
            [
                "text" => "Jornadas Contrato",
                "value" => "sessions_contract",
                "icon" => "mdi-file"
            ],
            [
                "text" => "Jornadas Ejecutadas",
                "value" => "sessions_completed",
                "icon" => "mdi-file"
            ],
            [
                "text" => "Jornadas Pendientes",
                "value" => "sessions_pending",
                "icon" => "mdi-file"
            ],
            [
                "text" => "% Ejecución",
                "value" => "execution_percentage",
                "icon" => "mdi-file"
            ],
            [
                "text" => "Saldo",
                "value" => "value",
                "icon" => "mdi-file"
            ],
            // [
            //     "text" => "Acciones",
            //     "value" => "actions",
            //     "icon" => "mdi-file"
            // ]
        ];
    }
}
