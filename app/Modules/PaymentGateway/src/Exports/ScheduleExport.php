<?php

namespace App\Modules\PaymentGateway\src\Exports;

use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;

class ScheduleExport implements FromCollection, WithHeadings, WithMapping, WithTitle
{
    use Exportable;

    /**
     * @var array
     */
    private $array;
    /**
     * @var int
     */
    private $counter;

    public function __construct($collection)
    {
        $this->array = $collection;
        $this->counter = 1;
    }

    public function collection()
    {
        return $this->array;
    }

    public function headings(): array
    {
        return [
            'ID',
            'DIA',
            'ESTADO',
            'HORARIO',
            'HORA INICIO',
            'HORA FIN',
            'DURACIÓN',
            'ID PARQUE',
            'NOMBRE DEL PARQUE',
            'DATOS DEL PARQUE',
            'EMAIL DEL PARQUE',
            'ID EQUIPAMIENTO',
            'DESCRIPCION EQUIPAMIENTO',
            'FECHA HABILITACIÓN',
            'FECHA DESHABILITACIÓN',
        ];
    }

    public function map($row): array
    {
        return [
            'Identificador'             =>  $this->counter++,
            'dia'                       =>  $row['Dia'] ?? null,
            'estado'                    =>  $this->setStatus($row['FechaHabilitacion'], $row['FechaInhabilitacion']),
            'horario'                   =>  $row['Horario'] ?? null,
            'hora_inicio'               =>  isset($row['HoraInicio']) ? Carbon::parse($row['HoraInicio'])->format('g:i A') : null,
            'hora_fin'                  =>  isset($row['HoraFin']) ? Carbon::parse($row['HoraFin'])->format('g:i A') : null,
            'duracion'                  =>  $this->setRangeHoursForHumans($row['HoraInicio'], $row['HoraFin']),
            'id_parque'                 =>  $row['IdParque'] ?? null,
            'nombre_parque'             =>  isset($row['NombreParque']) ? toUpper($row['NombreParque']) : null,
            'datos_parque'              =>  isset($row['DatosParque']) ? toUpper($row['DatosParque']) : null,
            'email_parque'              =>  $row['Email_Parque'] ?? null,
            'id_dotacion'               =>  $row['IdDotacion'] ?? null,
            'descripcion_dotacion'      =>  isset($row['DescripcionDotacion']) ? toUpper($row['DescripcionDotacion']) : null,
            'fecha_habilitacion'        =>  isset($row['FechaHabilitacion']) ? Carbon::parse($row['FechaHabilitacion'])->format('d-m-Y H:i') : null,
            'fecha_inhabilitacion'      =>  isset($row['FechaInhabilitacion']) ? Carbon::parse($row['FechaInhabilitacion'])->format('d-m-Y H:i') : null,
        ];
    }

    public function title(): string
    {
        return 'Horarios';
    }
    public function setStatus($enable, $disable): ? string
    {
        if (!is_null($enable) && is_null($disable)) {
            $status = 'habilitado';
        } elseif (!is_null($enable) && !is_null($disable)) {
            $status = 'deshabilitado';
        }
        return $status ?? null;
    }
    public function setRangeHoursForHumans($start_time, $end_time): ? string
    {
        $start = Carbon::createFromTimeString($start_time);
        $end = Carbon::createFromTimeString($end_time);
        $diff = $end->longAbsoluteDiffForHumans($start);
        return $diff ?? null;
    }
}
