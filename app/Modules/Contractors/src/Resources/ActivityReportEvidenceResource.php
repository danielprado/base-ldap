<?php


namespace App\Modules\Contractors\src\Resources;


use App\Modules\Contractors\src\Constants\Roles;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Crypt;

class ActivityReportEvidenceResource extends JsonResource
{

    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => isset($this->id) ? (int) $this->id : null,
            'activity_report_id' => isset($this->activity_report_id) ? (int) $this->activity_report_id : null,
            'sequence' => isset($this->sequence) ? (int) $this->sequence : null,
            'obligation_text' => isset($this->obligation_text) ? $this->obligation_text : null,
            'evidence_text' => isset($this->evidence_text) ? $this->evidence_text : null,
            'evidence_link' => isset($this->evidence_link) ? $this->evidence_link : null,
            'file_path' => isset($this->file_path) ? $this->file_path : null
        ];
    }
}
