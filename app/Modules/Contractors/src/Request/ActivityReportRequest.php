<?php


namespace App\Modules\Contractors\src\Request;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class ActivityReportRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contractor_id'  =>  'required|numeric|exists:mysql_contractors.contractors,id',
            'contract_id'  =>  'required|numeric|exists:mysql_contractors.contracts,id',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'contractor_id'  =>  'id contratista',
            'contract_id'  =>  'id contrato',
            'report_date'  =>  'mes informe',
            'activity_period'   =>  'periodo de actividades',
            'eps_name' =>  'nombre empresa eps',
            'eps_value' =>  'contribción eps',
            'afp_name' =>  'nombre empresa pensión',
            'afp_value' =>  'contribución pensión',
            'risks_name' =>  'nombre empresa riesgos laborales',
            'risks_value' =>  'contribución riesgos laborales',
            'contributions_total' =>  'total contribuciones',
            'contractor_signature' =>  'firma contratista',
            'attachments' =>  'firma contratista',
            'supervisor_signature' =>  'firma supervisor',
            'social_security_attachment' =>  'anexo de seguridad social',
            'evidences' => 'evidencias',
            'is_new_contractor' => 'es nuevo contratista',
        ];
    }
}
