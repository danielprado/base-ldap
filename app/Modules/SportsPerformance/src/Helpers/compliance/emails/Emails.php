<?php

namespace App\Modules\SportsPerformance\src\Helpers\compliance\emails;

use \SendGrid\Mail\Mail as mailer;


class Emails
{

    public function sendEmailRejectCompliances($email, $email2, $observations)
      {
          $sendgrid = new \SendGrid(env('SENDGRID_API_KEY'));
          $mail = new mailer();
          $mail->setFrom('mails@idrd.gov.co', 'Notificaciones Portal contratista');
          $mail->setSubject('Observaciones certificación Tributaria');
          $mail->setFrom('mails@idrd.gov.co', 'Notificaciones Portal contratista');
          $mail->addTo($email);
          $mail->addTo($email2);
          $mail->addContent(
              'text/html',
              '<p>Hola, estas son las observaciones de tu certificado de cumplimiento tributario </p>' .
              '<br/>' . '<p>'. $observations .' </p>'. '<br/>' .
              '<p>Corrija el documento y vuelva a enviar</p>'
          );
          $sendgrid->send($mail);
      }

      public function sendEmailApprovedReportActivities($email, $email2)
      {
          $sendgrid = new \SendGrid(env('SENDGRID_API_KEY'));
          $mail = new mailer();
          $mail->setFrom('mails@idrd.gov.co', 'Notificaciones Portal contratista');
          $mail->setSubject('Informes de actividades Aprobado');
          $mail->setFrom('mails@idrd.gov.co', 'Notificaciones Portal contratista');
          $mail->addTo($email);
          $mail->addTo($email2);
          $mail->addContent(
              'text/html',
              '<p>Buenas tardes, su informe de actividades ha sido aprobado </p>'
          );
          $sendgrid->send($mail);
      }

      public function sendEmailRejectReportActivities($email, $email2, $observations)
      {
          $sendgrid = new \SendGrid(env('SENDGRID_API_KEY'));
          $mail = new mailer();
          $mail->setFrom('mails@idrd.gov.co', 'Notificaciones Portal contratista');
          $mail->setSubject('Informes de actividades Rechazado');
          $mail->setFrom('mails@idrd.gov.co', 'Notificaciones Portal contratista');
          $mail->addTo($email);
          $mail->addTo($email2);
          $mail->addContent(
            'text/html',
            '<p>Hola, estas son las observaciones de tu Informe de actividades </p>' .
            '<br/>' . '<p>'. $observations .' </p>'. '<br/>' .
            '<p>Corrija el documento y vuelva a enviar</p>'
        );
          $sendgrid->send($mail);
      }
}
