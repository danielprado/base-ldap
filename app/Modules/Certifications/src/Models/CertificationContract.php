<?php

namespace App\Modules\Certifications\src\Models;

use App\Models\Security\DocumentType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class CertificationContract extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'mysql_certificacion_contratos';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'contrato';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Tipo_Documento',
        'Cedula',
        'Dv',
        'Nombre_Contratista',
        'Numero_Contrato',
        'Tipo_Contrato_Id',
        'Nombre_Representante',
        'Tipo_Documento_Representante',
        'Cedula_Representante',
        'Objeto',
        'Fecha_Firma',
        'Fecha_Inicio',
        'Fecha_Fin',
        'Fecha_Terminacion_Anticipada',
        'Meses_Duracion',
        'Dias_Duracion',
        'Otra_Duracion',
        'Valor_Inicial',
        'Valor_Mensual',
        'Referencia'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'Fecha_Firma',
        'Fecha_Inicio',
        'Fecha_Fin',
        'Fecha_Terminacion_Anticipada'
    ];

    /**
     * Set value in uppercase
     *
     * @param $value
     */
    public function setNombreContratistaAttribute($value)
    {
        $this->attributes['Nombre_Contratista'] = toUpper($value);
    }

    /**
     * Set value in uppercase
     *
     * @param $value
     */
    public function setNombreRepresentanteAttribute($value)
    {
        $this->attributes['Nombre_Representante'] = toUpper($value);
    }

    /**
     * Set value in uppercase
     *
     * @param $value
     */
    public function setObjetoAttribute($value)
    {
        $this->attributes['Objeto'] = toUpper($value);
    }

    public function additions()
    {
        return $this->hasMany(CertificationAddition::class, 'Contrato_Id', 'Id');
    }

    public function assignments()
    {
        return $this->hasMany(CertificationAssignment::class, 'Contrato_Id', 'Id');
    }

    public function expeditions()
    {
        return $this->hasMany(CertificationExpedition::class, 'Contrato_Id', 'Id');
    }

    public function extensions()
    {
        return $this->hasMany(CertificationExtension::class, 'Contrato_Id', 'Id');
    }

    public function members()
    {
        return $this->hasMany(CertificationMember::class, 'Contrato_Id', 'Id');
    }

    public function suspensions()
    {
        return $this->hasMany(CertificationSuspension::class, 'Contrato_Id', 'Id');
    }

    public function obligations()
    {
        return $this->hasMany(CertificationObligation::class, 'Contrato_Id', 'Id');
    }

    public function contractTypes()
    {
        return $this->belongsTo(CertificationContractType::class, 'Tipo_Contrato_Id', 'Id');
    }

    /**
     * @return BelongsTo
     */
    public function document_type()
    {
        return $this->belongsTo(DocumentType::class, 'Tipo_Documento');
    }

    /**
     * @return BelongsTo
     */
    public function document_type_representative()
    {
        return $this->belongsTo(DocumentType::class, 'Tipo_Documento_Representante');
    }

}
