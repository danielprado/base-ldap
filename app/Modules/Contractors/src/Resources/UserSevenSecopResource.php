<?php

namespace App\Modules\Contractors\src\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserSevenSecopResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'identification'      =>  isset($this->ter_codi) ? $this->ter_codi : null,
            'contract_number'  =>  isset($this->con_ncon) ? $this->con_ncon : null,
            'ano'  =>  isset($this->con_anop) ? $this->con_anop : null,
            'counter' => isset($this->con_nume) ? $this->con_nume : null
        ];
    }

}
