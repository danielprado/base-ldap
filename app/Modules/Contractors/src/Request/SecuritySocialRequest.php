<?php


namespace App\Modules\Contractors\src\Request;


use App\Modules\Contractors\src\Models\Compliance;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SecuritySocialRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'document_type_id'  => 'required|numeric',
            'document'  => 'required|numeric',
            'payroll_number' => 'required|numeric',
            'payment_date' => 'required',
            'compliance_id'  => 'nullable',
            'report_date'  => 'nullable',
        ];
    }

    public function messages()
    {
        return [
            'document_type_id'   => 'tipo de documento',
            'document'   => 'número de documento',
            'payroll_number.required'   => 'el campo número de planilla es obligatorio',
            'payment_date.required'   => 'el campo fecha de pago es obligatorio',
            'compliance_id.required' => 'el campo identificador del cumplimiento es obligatorio'
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'period_year'   => 'Año del periodo a certificar',
            'period_month'   => 'Mes del periodo a certificar',
            'contract_id'   => 'Número de contrato',
            'document'   => 'número de documento'
        ];
    }
}
