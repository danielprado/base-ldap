<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComplianceFilesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('compliance_files', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->unsignedBigInteger('compliance_id')
                ->comment('Identificador de cumplimiento');

            $table->string('file_name', 191)
                ->comment('Nombre del archivo');

            $table->unsignedBigInteger('file_type')
                ->comment('Tipo de archivo');

            $table->foreign('compliance_id')
                ->references('id')
                ->on('compliance')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');

            $table->foreign('file_type')
                ->references('id')
                ->on('compliance_file_types')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('compliance_files');
    }
}
