<?php

namespace App\Modules\Contractors\src\Mail;

use App\Modules\Contractors\src\Models\Contract;
use App\Modules\Contractors\src\Models\Compliance;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Crypt;

class ComplianceRejectedMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Compliance
     */
    private $compliance;

    /**
     * @var string
     */
    private $observation;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(
        Compliance $compliance,
        string $observation
    ) {
        $this->compliance = $compliance;
        $this->observation = $observation;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $contract = $this->compliance->contract_number;
        $monthlyUpdatedAt = $this->compliance->updated_at;

        $protocol = 'https';

        $path = config('app.env') != 'production'
            ? "/portal-contratista-dev"
            : "/";

        $subdomain = config('app.env') != 'production' ? "sim" : "portalcontratista";
        $domain = "{$subdomain}.idrd.gov.co";

        if (config('app.env') === 'local') {
            $protocol = 'http';
            $domain = 'localhost:3000';
        }

        $url = "{$protocol}://{$domain}{$path}";

        $contractNumberAndYear = extract_number_and_year_from_contract($contract);

        $payload = Crypt::encrypt(json_encode([
            'document' => $this->compliance->document,
            'contract_year' => $contractNumberAndYear['contract_year'],
            'contract_number' => $contractNumberAndYear['contract_number'],
            'period_month' => $this->compliance->period_month,
            'period_year' => $this->compliance->period_year
        ]));

        $style = "font-family:'Open Sans',Arial,sans-serif;font-size:24px;color:#3b3b3b;font-weight:bold;letter-spacing:4px;";

        return $this->view('mail.mail')
            ->subject('Certificado de Cumplimiento Tributario - Rechazado')
            ->with([
                'slogan'    => 'Notificación Certificado de Cumplimiento Tributario IDRD',
                'header'    => 'IDRD',
                'title'     => 'Certificado de Cumplimiento Tributario - Portal Contratista',
                'content'   => "¡El certificado de cumplimiento tributario generado para el contrato {$contract} ha sido rechazado! Puede realizar la revisión dando click en el siguiente botón.",
                'details'   => "
                    <div style='text-align: center;'>
                        <h2 style='$style'>Observaciones</h2>
                        <p>$this->observation</p>
                        <p>Fecha de Creación: $monthlyUpdatedAt</p>
                    </div>
                ",
                'url'       => "{$url}/es/iniciar-pago?search=$payload",
                'date'      => now()->year
            ]);
    }
}
