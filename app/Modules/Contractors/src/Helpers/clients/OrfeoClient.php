<?php

namespace App\Modules\Contractors\src\Helpers\clients;

use App\Modules\Contractors\src\Exceptions\BadResponseException;
use App\Modules\Contractors\src\Exceptions\IncorrectResponseFormatException;
use GuzzleHttp\Client;

class OrfeoClient 
{
  /**
  * @var Client
  */
  private $httpClient;
  
  public function __construct(Client $client) {
    $this->httpClient = $client;
  }

  private function get(array $headers) {
    try {
      $response = $this->httpClient->get(env('ORFEO_API_ENDPOINT'), [
        'auth' => [env('ORFEO_API_USER'), env('ORFEO_API_PASSWORD')],
        'headers' => $headers,
        'decode-content' => false
      ]);
    } catch (\Throwable $th) {
      throw new BadResponseException($th->getMessage());
    }

    return json_decode($response->getBody()->getContents(), true);
  }

  public function getAreasAndSubdirectorates()
  {
    $data = $this->get([
      'action' => 'consultarDependencias'
    ]);

    if (!is_array($data)) {
      throw new IncorrectResponseFormatException($data);
    }
    
    return $data;
  }
}