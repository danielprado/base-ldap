<?php


namespace App\Modules\Contractors\src\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\Contractors\src\Constants\GlobalQuery;
use App\Modules\Contractors\src\Helpers\compliance\emails\Emails;
use App\Models\Security\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Throwable;
use App\Modules\Contractors\src\Models\ActivityReport;
use App\Modules\Contractors\src\Models\ContractorContribution;
use App\Modules\Contractors\src\Request\ContributionRequest;
use App\Modules\Contractors\src\Resources\ContributionResource;
use Illuminate\Support\Facades\Log;

class ContributionController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index(Request $request)
    {
        $contributions = ContractorContribution::where('report_id', $request->report_id)->oldest()->paginate($this->per_page);
        return $this->success_response(
            ContributionResource::collection($contributions),
            Response::HTTP_OK,
            [
                'headers'   => ContributionResource::headers(),
                'expanded'   => ContributionResource::expanded()
            ]
        );
    }

    public function show(ContractorContribution $contribution)
    {
        return $this->success_response(
            new ContributionResource($contribution),
            Response::HTTP_OK
        );
    }

    public function store(ContributionRequest $request)
    {
        try {
            DB::connection('mysql_contractors')->beginTransaction();
            
            $form = ContractorContribution::create($request->validated());

            $report = ActivityReport::with('contributions')->find($request->report_id);

            $support_path = '';
            if ($request->has('support') && $report){
                $file = $request->file('support')[0];
                $ext = $file->getClientOriginalExtension();
                $now = now()->format('YmdHis');
                $support_path = "SECURITY_SOCIAL_{$report->report_date}_{$form->id}_{$report->contract_number}_{$now}.$ext";
                $file->storeAs('activity-reports', $report->contract_number . '/' . $support_path, [ 'disk' => 'local' ]);
                $form->update(['support_file' => $support_path]);
            }

            $report->update([
                'eps_value' => $report->contributions->max('eps_value'),
                'afp_value' => $report->contributions->max('afp_value'),
                'risks_value' => $report->contributions->max('arl_value'),
                'contributions_total' => $report->contributions->max('total'),
                'social_security_attachment_path' => $support_path
            ]);
            
            DB::connection('mysql_contractors')->commit();

            return $this->success_message(
                __('validation.handler.success'),
                Response::HTTP_CREATED,
                Response::HTTP_CREATED,
                ['id' => $form->id]
            );
        } catch (Throwable $e) {
            DB::connection('mysql_contractors')->rollBack();
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }

    public function update(ContributionRequest $request, ContractorContribution $contribution)
    {
        try {
            DB::connection('mysql_contractors')->beginTransaction();
            $contribution->update($request->validated());
            
            $report = ActivityReport::with('contributions')->find($request->report_id);

            $support_path = '';
            if ($request->has('support') && $report){
                $original_path = $report->contract_number . '/' . $contribution->getOriginal('support_file');
                if ($contribution->getOriginal('support_file') && Storage::disk('activity-reports')->exists($original_path)) {
                    Storage::disk('activity-reports')->delete($original_path);
                }
                $file = $request->file('support')[0];
                $ext = $file->getClientOriginalExtension();
                $now = now()->format('YmdHis');
                $support_path = "SECURITY_SOCIAL_{$report->report_date}_{$contribution->id}_{$report->contract_number}_{$now}.$ext";
                $file->storeAs('activity-reports', $report->contract_number . '/' . $support_path, [ 'disk' => 'local' ]);
                $contribution->update(['support_file' => $support_path]);
            }

            $report->update([
                'eps_value' => $report->contributions->max('eps_value'),
                'afp_value' => $report->contributions->max('afp_value'),
                'risks_value' => $report->contributions->max('arl_value'),
                'contributions_total' => $report->contributions->max('total'),
                'social_security_attachment_path' => $support_path
            ]);
            
            DB::connection('mysql_contractors')->commit();

            return $this->success_message(
                __('validation.handler.success'),
                Response::HTTP_OK,
                Response::HTTP_OK,
                [
                    'contribution' => $contribution
                ]
            );
        } catch (\Exception $e) {
            DB::connection('mysql_contractors')->rollBack();
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }

    public function destroy(ContractorContribution $contribution)
    {
        try {
            DB::connection('mysql_contractors')->beginTransaction();
            $report = ActivityReport::find($contribution->report_id);
            if ($report){
                $original_path = $report->contract_number . '/' . $contribution->support_file;
                if ($contribution->support_file && Storage::disk('activity-reports')->exists($original_path)) {
                    Storage::disk('activity-reports')->delete($original_path);
                }
            }
    
            $eps_value = 0;
            $afp_value = 0;
            $arl_value = 0;
            $total = 0;
    
            $remaining = ContractorContribution::where('report_id', $report->id)->where('id', '!=', $contribution->id)->first();
            if ($remaining){
                if ($remaining->total === $report->contributions_total){
                    $eps_value = $report->eps_value - $contribution->eps_value;
                    $afp_value = $report->afp_value - $contribution->afp_value;
                    $arl_value = $report->risks_value - $contribution->arl_value;
                    $total = $report->contributions_total - $contribution->total;
                } else {
                    $eps_value = $remaining->eps_value;
                    $afp_value = $remaining->afp_value;
                    $arl_value = $remaining->arl_value;
                    $total = $remaining->total;
                }
            }
            $report->update([
                'eps_value' => $eps_value,
                'afp_value' => $afp_value,
                'risks_value' => $arl_value,
                'contributions_total' => $total
            ]);
    
            $contribution->delete();
            DB::connection('mysql_contractors')->commit();

            return $this->success_message(
                __('validation.handler.deleted'),
                Response::HTTP_OK,
                Response::HTTP_OK,
                [
                    'eps_value' => $eps_value,
                    'afp_value' => $afp_value,
                    'arl_value' => $arl_value,
                    'total' => $total
                ]
            );
        }catch (\Exception $e) {
            DB::connection('mysql_contractors')->rollBack();
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }
}
