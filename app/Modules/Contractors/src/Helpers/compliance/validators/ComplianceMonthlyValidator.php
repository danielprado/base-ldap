<?php

namespace App\Modules\Contractors\src\Helpers\compliance\validators;

use App\Modules\Contractors\src\Models\Compliance;
use Carbon\Carbon;

class ComplianceMonthlyValidator extends ComplianceDateValidator
{
  private $startDay;
  private $endDay;

  const DEFAULT_START_DAY = Compliance::COMPLIANCE_PERIOD_START_DAY;
  const DEFAULT_END_DAY = Compliance::COMPLIANCE_PERIOD_END_DAY;

  private function __construct($startDay, $endDay)
  {
    $this->startDay = $startDay ?? 5;
    $this->endDay = $endDay ?? ($startDay ? $startDay : 30);
  }

  public static function create($startDay = null, $endDay = null)
  {
    return new self(5, 30);
  }

  public function name(): string
  {
    return 'monthly';
  }

  public function period(Carbon $date = null)
  {
    $date = $date ?? now();

    $start = Carbon::create($date->year, $date->month, $this->startDay);

    $end = Carbon::create($date->year, $date->month, 1, 23, 59, 59)
      ->setUnitNoOverflow('day', $this->endDay, 'month');

    return compact('start', 'end');
  }
}
