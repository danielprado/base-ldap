<?php

namespace App\Modules\Parks\src\Help;

class ResultsProcessor
{
    public function processResults($resultsEndTypesFields)
    {
        $allFields = [];
        $allTypes = [];

        foreach ($resultsEndTypesFields as $index => $result) {
            if ($result['state'] === 'fulfilled') {
                $response = $result['value'];
                $content = json_decode($response->getBody()->getContents(), true);
                $fields = array_map(function ($field) {
                    return ['name' => $field['name'], 'domain' => $field['domain']];
                }, $content['fields']);

                $types = array_map(function ($field) {
                    return ['id' => $field['id'], 'name' => $field['name'], 'domains' => $field['domains']];
                }, $content['types']);

                $allFields[] = ['fields' => $fields];
                $allTypes[] = ['types' => $types];
            } else {
                // Maneja el error, por ejemplo, $result['reason']
            }
        }

        return ['allFields' => $allFields, 'allTypes' => $allTypes];
    }

    public function processResultsFeatures($results, $allFields, $allTypes, $condition, $modulo)
    {
        $indexTypesFields = 0;
        $allFeatures = [];

        foreach ($results as $index => $result) {
            if ($result['state'] === 'fulfilled') {
                $response = $result['value'];
                $content = json_decode($response->getBody()->getContents(), true)['features'];

                $indexTypesFields = ($index === 0) ? 0 : ($index % $modulo === 0 ? $indexTypesFields + 1 : $indexTypesFields);

                foreach ($content as $feature) {
                    $condition == 'endowment'
                        ? $allFeatures[] = $this->processFeatureEndowment($feature, $allFields[$indexTypesFields]['fields'], $allTypes[$indexTypesFields]['types'])
                        : $allFeatures[] = $this->processFeatureFurniture($feature, $allFields[$indexTypesFields]['fields'], $allTypes[$indexTypesFields]['types']);
                }
            } else {
                // Maneja el error, por ejemplo, $result['reason']
            }
        }

        return $allFeatures;
    }

    private function processFeatureEndowment($feature, $fields, $types)
    {
        return [
            'UB_REFERENCIA' => isset($feature['attributes']['UB_REFERENCIA']) ? $feature['attributes']['UB_REFERENCIA'] : null,
            'NOMBRE_PARQ' => isset($feature['attributes']['NOMBRE_PARQ']) ? $feature['attributes']['NOMBRE_PARQ'] : null,
            'ID_DOTACION' => isset($feature['attributes']['ID_DOTACION']) ? $feature['attributes']['ID_DOTACION'] : null,
            'NOM_DOTACION' => isset($feature['attributes']['NOM_DOTACION']) ? $this->getInfo($fields, $types, $feature['attributes']['NOM_DOTACION'], 'NOM_DOTACION', $feature['attributes']['TIPO_DOTACION']) : null,
            'NOM_DOTACION_ORIGINAL' => isset($feature['attributes']['NOM_DOTACION']) ? $feature['attributes']['NOM_DOTACION'] : null,
            'TIPO_DOTACION' => $this->getType($types, $feature['attributes']['TIPO_DOTACION']),
            'DESCR' => isset($feature['attributes']['DESCR']) ? $feature['attributes']['DESCR'] : null,
            'COOR_X' => isset($feature['attributes']['COOR_X']) ? $feature['attributes']['COOR_X'] : null,
            'COOR_Y' => isset($feature['attributes']['COOR_Y']) ? $feature['attributes']['COOR_Y'] : null,
            'RIESGO_1' => isset($feature['attributes']['RIESGO_1']) ? $this->getInfo($fields, $types, $feature['attributes']['RIESGO_1'], 'RIESGO_1', $feature['attributes']['TIPO_DOTACION']) : null,
            'RIESGO_2' => isset($feature['attributes']['RIESGO_2']) ? $this->getInfo($fields, $types, $feature['attributes']['RIESGO_2'], 'RIESGO_2', $feature['attributes']['TIPO_DOTACION']) : null,
            'RIESGO_3' => isset($feature['attributes']['RIESGO_3']) ? $this->getInfo($fields, $types, $feature['attributes']['RIESGO_3'], 'RIESGO_3', $feature['attributes']['TIPO_DOTACION']) : null,
            'RIESGO_4' => isset($feature['attributes']['RIESGO_4']) ? $this->getInfo($fields, $types, $feature['attributes']['RIESGO_4'], 'RIESGO_4', $feature['attributes']['TIPO_DOTACION']) : null,
            'MAT_PISO' => isset($feature['attributes']['MAT_PISO']) ? $this->getInfo($fields, $types, $feature['attributes']['MAT_PISO'], 'MAT_PISO', $feature['attributes']['TIPO_DOTACION']) : null,
            'AREA_M2' => isset($feature['attributes']['AREA_M2']) ? $feature['attributes']['AREA_M2'] : null,
            'AREA_JUEGO' => isset($feature['attributes']['AREA_JUEGO']) ? $feature['attributes']['AREA_JUEGO'] : null,
            'CAPACIDAD' => isset($feature['attributes']['CAPACIDAD']) ? $feature['attributes']['CAPACIDAD'] : null,
            'ILUMINACION' => isset($feature['attributes']['ILUMINACION']) ? $this->getInfo($fields, $types, $feature['attributes']['ILUMINACION'], 'ILUMINACION', $feature['attributes']['TIPO_DOTACION']) : null,
            'CUBIERTO' => isset($feature['attributes']['CUBIERTO']) ? $this->getInfo($fields, $types, $feature['attributes']['CUBIERTO'], 'CUBIERTO', $feature['attributes']['TIPO_DOTACION']) : null,
            'CERRAMIENTO' => isset($feature['attributes']['CERRAMIENTO']) ? $this->getInfo($fields, $types, $feature['attributes']['CERRAMIENTO'], 'CERRAMIENTO', $feature['attributes']['TIPO_DOTACION']) : null,
            'TIPO_CERRAMIENTO_LAT' => isset($feature['attributes']['TIPO_CERRAMIENTO_LAT']) ? $this->getInfo($fields, $types, $feature['attributes']['TIPO_CERRAMIENTO_LAT'], 'TIPO_CERRAMIENTO_LAT', $feature['attributes']['TIPO_DOTACION']) : null,
            'TIPO_CERRAMIENTO_FRO' => isset($feature['attributes']['TIPO_CERRAMIENTO_FRO']) ? $this->getInfo($fields, $types, $feature['attributes']['TIPO_CERRAMIENTO_FRO'], 'TIPO_CERRAMIENTO_FRO', $feature['attributes']['TIPO_DOTACION']) : null,
            'CAMERINO' => isset($feature['attributes']['CAMERINO']) ? $this->getInfo($fields,  $types, $feature['attributes']['CAMERINO'], 'CAMERINO', $feature['attributes']['TIPO_DOTACION']) : null,
            'SISTEMA_RIEGO' => isset($feature['attributes']['SISTEMA_RIEGO']) ? $this->getInfo($fields, $types, $feature['attributes']['SISTEMA_RIEGO'], 'SISTEMA_RIEGO', $feature['attributes']['TIPO_DOTACION']) : null,
            'USO_INCLUYENTE' => isset($feature['attributes']['USO_INCLUYENTE']) ? $this->getInfo($fields, $types, $feature['attributes']['USO_INCLUYENTE'], 'USO_INCLUYENTE', $feature['attributes']['TIPO_DOTACION']) : null,
            'FECHA_CREACION' =>  isset($feature['attributes']['FECHA_CREACION']) ? $feature['attributes']['FECHA_CREACION'] : null,
            'FECHA_ACTUALIZACION' => isset($feature['attributes']['FECHA_ACTUALIZACION']) ? $feature['attributes']['FECHA_ACTUALIZACION'] : null,
            'NRO_PISO' => isset($feature['attributes']['NRO_PISO']) ? $feature['attributes']['NRO_PISO'] : null,
            'ESTADO_DIAG' => isset($feature['attributes']['ESTADO_DIAG']) ? $this->getInfo($fields, $types, $feature['attributes']['ESTADO_DIAG'], 'ESTADO_DIAG', $feature['attributes']['TIPO_DOTACION']) : null,
            'ESTADO_DIAG_ORIGINAL' => isset($feature['attributes']['ESTADO_DIAG']) ? $feature['attributes']['ESTADO_DIAG'] : null,
        ];
    }

    private function processFeatureFurniture($feature, $fields, $types){
        return [
            'UB_REFERENCIA' => isset($feature['attributes']['UB_REFERENCIA']) ? $feature['attributes']['UB_REFERENCIA'] : null,
            'NOMBRE_PARQ' => isset($feature['attributes']['NOMBRE_PARQ']) ? $feature['attributes']['NOMBRE_PARQ'] : null,
            'ID_MOBILIARIO' => isset($feature['attributes']['ID_MOBILIARIO']) ? $feature['attributes']['ID_MOBILIARIO'] : null,
            'NOM_MOBILIARIO' => isset($feature['attributes']['NOM_MOBILIARIO']) ? $this->getInfo($fields,  $types, $feature['attributes']['NOM_MOBILIARIO'], 'NOM_MOBILIARIO', $feature['attributes']['TIPO_MOBILIARIO']) : null,
            'NOM_MOBILIARIO_ORIGINAL' => isset($feature['attributes']['NOM_MOBILIARIO']) ? $feature['attributes']['NOM_MOBILIARIO'] : null,
            'TIPO_MOBILIARIO' => $this->getType($types, $feature['attributes']['TIPO_MOBILIARIO']),
            'DESCR' => isset($feature['attributes']['DESCR']) ? $feature['attributes']['DESCR'] : null,
            'COOR_X' => isset($feature['attributes']['COOR_X']) ? $feature['attributes']['COOR_X'] : null,
            'COOR_Y' => isset($feature['attributes']['COOR_Y']) ? $feature['attributes']['COOR_Y'] : null,
            'MAT_ELEMENTO' => isset($feature['attributes']['MAT_ELEMENTO']) ? $this->getInfo($fields, $types, $feature['attributes']['MAT_ELEMENTO'], 'MAT_ELEMENTO', $feature['attributes']['TIPO_MOBILIARIO']) : null,
            'CAPACIDAD' => isset($feature['attributes']['CAPACIDAD']) ? $feature['attributes']['CAPACIDAD'] : null,
            'GRUPO_ETAREO' => isset($feature['attributes']['GRUPO_ETAREO']) ? $this->getInfo($fields, $types, $feature['attributes']['GRUPO_ETAREO'], 'GRUPO_ETAREO', $feature['attributes']['TIPO_MOBILIARIO']) : null,
            'USO_INCLUYENTE' => isset($feature['attributes']['USO_INCLUYENTE']) ? $this->getInfo($fields, $types, $feature['attributes']['USO_INCLUYENTE'], 'USO_INCLUYENTE', $feature['attributes']['TIPO_MOBILIARIO']) : null,
            'RIESGO_1' => isset($feature['attributes']['RIESGO_1']) ? $this->getInfo($fields, $types, $feature['attributes']['RIESGO_1'], 'RIESGO_1', $feature['attributes']['TIPO_MOBILIARIO']) : null,
            'RIESGO_2' => isset($feature['attributes']['RIESGO_2']) ? $this->getInfo($fields, $types, $feature['attributes']['RIESGO_2'], 'RIESGO_2', $feature['attributes']['TIPO_MOBILIARIO']) : null,
            'RIESGO_3' => isset($feature['attributes']['RIESGO_3']) ? $this->getInfo($fields, $types, $feature['attributes']['RIESGO_3'], 'RIESGO_3', $feature['attributes']['TIPO_MOBILIARIO']) : null,
            'RIESGO_4' => isset($feature['attributes']['RIESGO_4']) ? $this->getInfo($fields, $types, $feature['attributes']['RIESGO_4'], 'RIESGO_4', $feature['attributes']['TIPO_MOBILIARIO']) : null,
            'FECHA_CREACION' =>  isset($feature['attributes']['FECHA_CREACION']) ? $feature['attributes']['FECHA_CREACION'] : null,
            'FECHA_ACTUALIZACION' => isset($feature['attributes']['FECHA_ACTUALIZACION']) ? $feature['attributes']['FECHA_ACTUALIZACION'] : null,
            'NRO_PISO' => isset($feature['attributes']['NRO_PISO']) ? $feature['attributes']['NRO_PISO'] : null,
            'ESTADO_DIAG' => isset($feature['attributes']['ESTADO_DIAG']) ? $this->getInfo($fields, $types, $feature['attributes']['ESTADO_DIAG'], 'ESTADO_DIAG', $feature['attributes']['TIPO_MOBILIARIO']) : null,
            'ESTADO_DIAG_ORIGINAL' => isset($feature['attributes']['ESTADO_DIAG']) ? $feature['attributes']['ESTADO_DIAG'] : null,
        ];
    }

    private function getType($allTypes, $id): string
    {
        $matchType = ArrayHelper::find($allTypes, 'id', $id);
        return $matchType['name'];
    }

    private function getInfo($arrayFields, $arrayTypes, $value, $propName, $type): ?string
    {
        $matchTypes = ArrayHelper::find($arrayTypes, 'id', $type);
        if ($matchTypes['domains'][$propName]['type'] === 'codedValue') {
            $matchTypeDomains = ArrayHelper::find($matchTypes['domains'][$propName]['codedValues'], 'code', $value);
            return $matchTypeDomains['name'];
        } else {
            $matchFields = ArrayHelper::find($arrayFields, 'name', $propName);
            $object = ArrayHelper::find($matchFields['domain']['codedValues'], 'code', $value);
            return $object['name'];
        }
    }
}


// foreach ($resultsEndTypesFields as $index => $result) {
                //     if ($result['state'] === 'fulfilled') {
                //         $response = $result['value'];
                //         $content = json_decode($response->getBody()->getContents(), true);
                //         $allFields[] = [
                //             'fields' => array_map(function ($field) {
                //                 return ['name' => $field['name'], 'domain' => $field['domain']];
                //             }, $content['fields'])
                //         ];
                //         $allTypes[] = [
                //             'types' => array_map(function ($field) {
                //                 return ['id' => $field['id'], 'name' => $field['name'], 'domains' => $field['domains']];
                //             }, $content['types'])
                //         ];
                //     } else {
                //         // Maneja el error, por ejemplo, $result['reason']
                //     }
                // }


                // $indexTypesFields = 0;
                // foreach ($results as $index => $result) {
                //     if ($result['state'] === 'fulfilled') {
                //         $response = $result['value'];
                //         $content = json_decode($response->getBody()->getContents(), true)['features'];
                //         $indexTypesFields = ($index === 0) ? 0 : ($index % 2 === 0 ? $indexTypesFields + 1 : $indexTypesFields);
                //         foreach ($content as $feature) {
                //             $allFeatures[] = [
                //                 'UB_REFERENCIA' => isset($feature['attributes']['UB_REFERENCIA']) ? $feature['attributes']['UB_REFERENCIA'] : null,
                //                 'NOMBRE_PARQ' => isset($feature['attributes']['NOMBRE_PARQ']) ? $feature['attributes']['NOMBRE_PARQ'] : null,
                //                 'ID_DOTACION' => isset($feature['attributes']['ID_DOTACION']) ? $feature['attributes']['ID_DOTACION'] : null,
                //                 'NOM_DOTACION' => isset($feature['attributes']['NOM_DOTACION']) ? $this->getInfo($allFields[$indexTypesFields]['fields'], $allTypes[$indexTypesFields]['types'], $feature['attributes']['NOM_DOTACION'], 'NOM_DOTACION', $feature['attributes']['TIPO_DOTACION']) : null,
                //                 'NOM_DOTACION_ORIGINAL' => isset($feature['attributes']['NOM_DOTACION']) ? $feature['attributes']['NOM_DOTACION'] : null,
                //                 'TIPO_DOTACION' => $this->getType($allTypes[$indexTypesFields]['types'], $feature['attributes']['TIPO_DOTACION']),
                //                 'DESCR' => isset($feature['attributes']['DESCR']) ? $feature['attributes']['DESCR'] : null,
                //                 'COOR_X' => isset($feature['attributes']['COOR_X']) ? $feature['attributes']['COOR_X'] : null,
                //                 'COOR_Y' => isset($feature['attributes']['COOR_Y']) ? $feature['attributes']['COOR_Y'] : null,
                //                 'RIESGO_1' => isset($feature['attributes']['RIESGO_1']) ? $this->getInfo($allFields[$indexTypesFields]['fields'], $allTypes[$indexTypesFields]['types'], $feature['attributes']['RIESGO_1'], 'RIESGO_1', $feature['attributes']['TIPO_DOTACION']) : null,
                //                 'RIESGO_2' => isset($feature['attributes']['RIESGO_2']) ? $this->getInfo($allFields[$indexTypesFields]['fields'], $allTypes[$indexTypesFields]['types'], $feature['attributes']['RIESGO_2'], 'RIESGO_2', $feature['attributes']['TIPO_DOTACION']) : null,
                //                 'RIESGO_3' => isset($feature['attributes']['RIESGO_3']) ? $this->getInfo($allFields[$indexTypesFields]['fields'], $allTypes[$indexTypesFields]['types'], $feature['attributes']['RIESGO_3'], 'RIESGO_3', $feature['attributes']['TIPO_DOTACION']) : null,
                //                 'RIESGO_4' => isset($feature['attributes']['RIESGO_4']) ? $this->getInfo($allFields[$indexTypesFields]['fields'], $allTypes[$indexTypesFields]['types'], $feature['attributes']['RIESGO_4'], 'RIESGO_4', $feature['attributes']['TIPO_DOTACION']) : null,
                //                 'MAT_PISO' => isset($feature['attributes']['MAT_PISO']) ? $this->getInfo($allFields[$indexTypesFields]['fields'], $allTypes[$indexTypesFields]['types'], $feature['attributes']['MAT_PISO'], 'MAT_PISO', $feature['attributes']['TIPO_DOTACION']) : null,
                //                 'AREA_M2' => isset($feature['attributes']['AREA_M2']) ? $feature['attributes']['AREA_M2'] : null,
                //                 'AREA_JUEGO' => isset($feature['attributes']['AREA_JUEGO']) ? $feature['attributes']['AREA_JUEGO'] : null,
                //                 'CAPACIDAD' => isset($feature['attributes']['CAPACIDAD']) ? $feature['attributes']['CAPACIDAD'] : null,
                //                 'ILUMINACION' => isset($feature['attributes']['ILUMINACION']) ? $this->getInfo($allFields[$indexTypesFields]['fields'], $allTypes[$indexTypesFields]['types'], $feature['attributes']['ILUMINACION'], 'ILUMINACION', $feature['attributes']['TIPO_DOTACION']) : null,
                //                 'CUBIERTO' => isset($feature['attributes']['CUBIERTO']) ? $this->getInfo($allFields[$indexTypesFields]['fields'], $allTypes[$indexTypesFields]['types'], $feature['attributes']['CUBIERTO'], 'CUBIERTO', $feature['attributes']['TIPO_DOTACION']) : null,
                //                 'CERRAMIENTO' => isset($feature['attributes']['CERRAMIENTO']) ? $this->getInfo($allFields[$indexTypesFields]['fields'], $allTypes[$indexTypesFields]['types'], $feature['attributes']['CERRAMIENTO'], 'CERRAMIENTO', $feature['attributes']['TIPO_DOTACION']) : null,
                //                 'TIPO_CERRAMIENTO_LAT' => isset($feature['attributes']['TIPO_CERRAMIENTO_LAT']) ? $this->getInfo($allFields[$indexTypesFields]['fields'], $allTypes[$indexTypesFields]['types'], $feature['attributes']['TIPO_CERRAMIENTO_LAT'], 'TIPO_CERRAMIENTO_LAT', $feature['attributes']['TIPO_DOTACION']) : null,
                //                 'TIPO_CERRAMIENTO_FRO' => isset($feature['attributes']['TIPO_CERRAMIENTO_FRO']) ? $this->getInfo($allFields[$indexTypesFields]['fields'], $allTypes[$indexTypesFields]['types'], $feature['attributes']['TIPO_CERRAMIENTO_FRO'], 'TIPO_CERRAMIENTO_FRO', $feature['attributes']['TIPO_DOTACION']) : null,
                //                 'CAMERINO' => isset($feature['attributes']['CAMERINO']) ? $this->getInfo($allFields[$indexTypesFields]['fields'],  $allTypes[$indexTypesFields]['types'], $feature['attributes']['CAMERINO'], 'CAMERINO', $feature['attributes']['TIPO_DOTACION']) : null,
                //                 'SISTEMA_RIEGO' => isset($feature['attributes']['SISTEMA_RIEGO']) ? $this->getInfo($allFields[$indexTypesFields]['fields'], $allTypes[$indexTypesFields]['types'], $feature['attributes']['SISTEMA_RIEGO'], 'SISTEMA_RIEGO', $feature['attributes']['TIPO_DOTACION']) : null,
                //                 'USO_INCLUYENTE' => isset($feature['attributes']['USO_INCLUYENTE']) ? $this->getInfo($allFields[$indexTypesFields]['fields'], $allTypes[$indexTypesFields]['types'], $feature['attributes']['USO_INCLUYENTE'], 'USO_INCLUYENTE', $feature['attributes']['TIPO_DOTACION']) : null,
                //                 'FECHA_CREACION' =>  isset($feature['attributes']['FECHA_CREACION']) ? $feature['attributes']['FECHA_CREACION'] : null,
                //                 'FECHA_ACTUALIZACION' => isset($feature['attributes']['FECHA_ACTUALIZACION']) ? $feature['attributes']['FECHA_ACTUALIZACION'] : null,
                //                 'NRO_PISO' => isset($feature['attributes']['NRO_PISO']) ? $feature['attributes']['NRO_PISO'] : null,
                //                 'ESTADO_DIAG' => isset($feature['attributes']['ESTADO_DIAG']) ? $this->getInfo($allFields[$indexTypesFields]['fields'], $allTypes[$indexTypesFields]['types'], $feature['attributes']['ESTADO_DIAG'], 'ESTADO_DIAG', $feature['attributes']['TIPO_DOTACION']) : null,
                //                 'ESTADO_DIAG_ORIGINAL' => isset($feature['attributes']['ESTADO_DIAG']) ? $feature['attributes']['ESTADO_DIAG'] : null,
                //             ];
                //         }
                //     } else {
                //         // Maneja el error, por ejemplo, $result['reason']
                //     }
                // }
