<?php


namespace App\Modules\Contractors\src\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Contractors\src\Models\Bank;
use App\Modules\Contractors\src\Resources\BankResource;
use Illuminate\Http\JsonResponse;

class BankController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index()
    {
        $data = $this->setQuery(Bank::query()->orderBy('name')
            ->whereNotIn('id', [6, 8, 14, 25, 28, 35, 36, 50, 55, 57, 58, 70, 81, 100, 110, 180, 283, 292, 558, 701, 1000, 1367, 1507]),
            (new Bank())->getKeyName())
            ->get();
        return $this->success_response(
            BankResource::collection($data)
        );
    }

}
