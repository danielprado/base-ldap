<?php

namespace App\Modules\Certifications\src\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class SupportResource extends JsonResource
{
    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    public function toArray($request): array
    {
        return [
            'id'                => isset($this->Id) ? (int) $this->Id : null,
            'document_type_id'  =>  isset($this->Tipo_Documento_Solicitante_Id) ? (int) $this->Tipo_Documento_Solicitante_Id : null,
            'document_type'     => $this->document_type->name ?? null,
            'document'          => $this->Documento_Solicitante ?? null,
            'name'              => $this->Nombre_Solicitante ?? null,
            'email'             => $this->Correo_Solicitante ?? null,
            'phone'             => $this->Telefono_Solicitante ?? null,
            'contract_number'   => $this->Numero_Contrato ?? null,
            'contract_year'     => $this->Anio_Contrato ?? null,
            'description'       => $this->Descripcion_Solicitud ?? null,
            'state'             => $this->setState(),
            'solution'          => $this->Solucion ?? null,
            'created_at'        =>  isset($this->created_at) ? $this->created_at->format('Y-m-d') : null,
            'updated_at'        =>  isset($this->updated_at) ? $this->updated_at->format('Y-m-d') : null,
        ];
    }

    public static function headers(): array
    {
        return [
            [
                'text' => "#",
                'value'  =>  "id",
                'sortable' => false
            ],
            [
                'align' => "right",
                'text' => "Detalles",
                'value'  =>  "view",
                'sortable' => false
            ],
            [
                'align' => "center",
                'text' => "Estado",
                'value'  =>  "state",
                'sortable' => false
            ],
            [
                'align' => "center",
                'text' => "Nombres",
                'value'  =>  "name",
                'sortable' => false
            ],
            [
                'align' => "right",
                'text' => "Documento",
                'value'  =>  "document",
                'sortable' => false
            ],
            [
                'align' => "left",
                'text' => "E-mail",
                'value'  =>  "email",
                'sortable' => false
            ],
            [
                'align' => "left",
                'text' => "Teléfono",
                'value'  =>  "phone",
                'sortable' => false
            ],
            [
                'align' => "left",
                'text' => "Número contrato",
                'value'  =>  "contract_number",
                'sortable' => false
            ],
            [
                'align' => "left",
                'text' => "Año de contrato",
                'value'  =>  "contract_year",
                'sortable' => false
            ],
            [
                'align' => "right",
                'text' => "Fecha solicitud",
                'value'  =>  "created_at",
                'sortable' => false
            ],
        ];
    }

    public function setState(): string
    {
        if ($this->Estado == '1') {
            return 'Pendiente';
        }
        return 'Solucionado';
    }
}
