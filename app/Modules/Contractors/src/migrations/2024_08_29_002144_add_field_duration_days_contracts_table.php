<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldDurationDaysContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contracts', function (Blueprint $table) {
            $table->integer('duration_days')->after('duration')->nullable()->default(0)->comment('Duration del contrato en días');
            $table->string('total_addition', 191)->after('total')->nullable()->default(0)->comment('Valor total de la adición');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contracts', function (Blueprint $table) {
            $table->dropColumn('duration_days', 'total_addition');
        });
    }
}
