<?php


namespace App\Modules\Contractors\src\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Contractors\src\Models\EconomicActivity;
use App\Modules\Contractors\src\Resources\ActivityEconomicResource;
use Illuminate\Http\JsonResponse;

class EconomicActivityController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        $data = $this->setQuery(EconomicActivity::query()->orderBy('code'), (new EconomicActivity())->getKeyName())->get();
        return $this->success_response(
            ActivityEconomicResource::collection($data)
        );
    }

}
