<?php


namespace App\Modules\CitizenPortal\src\Resources;


use App\Modules\CitizenPortal\src\Constants\Roles;
use App\Modules\CitizenPortal\src\Models\Activity;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class PreRegisterResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'        =>  isset($this->id) ? (int) $this->id : null,
            'document_type_id'      =>  isset($this->document_type_id) ? (int) $this->document_type_id : null,
            'document_type'      =>  isset($this->document_type->name) ? (string) $this->document_type->name : null,
            'document'      =>  isset($this->document) ? (string) $this->document : null,
            'name'      =>  isset($this->name) ?  toUpper($this->name) : null,
            's_name'      =>  isset($this->s_name) ?  toUpper($this->s_name) : null,
            'surname'      =>  isset($this->surname) ?  toUpper($this->surname) : null,
            's_surname'      =>  isset($this->s_surname) ?  toUpper($this->s_surname) : null,
            'email'      =>  isset($this->email) ? toLower($this->email) : null,
            'code'      =>  isset($this->code) ? (string) $this->code : null,
            'created_at'    =>  isset($this->created_at) ? $this->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'    =>  isset($this->updated_at) ? $this->updated_at->format('Y-m-d H:i:s') : null,
            'audits'       => []
        ];
    }

    public static function headers()
    {
        return [
            [
                'align' => "right",
                'text' => "#",
                'value'  =>  "id",
                'sortable' => false
            ],
            [
                'align' => "left",
                'text' => Str::ucfirst(__('citizen.validations.document_type')),
                'value'  =>  "document_type",
                'sortable' => false
            ],
            [
                'align' => "left",
                'text' => Str::ucfirst(__('citizen.validations.document')),
                'value'  =>  "document",
                'sortable' => false
            ],
            [
                'align' => "left",
                'text' => Str::ucfirst(__('citizen.validations.name')),
                'value'  =>  "name",
                'sortable' => false
            ],
            [
                'align' => "left",
                'text' => Str::ucfirst(__('citizen.validations.s_name')),
                'value'  =>  "s_name",
                'sortable' => false
            ],
            [
                'align' => "left",
                'text' => Str::ucfirst(__('citizen.validations.surname')),
                'value'  =>  "surname",
                'sortable' => false
            ],
            [
                'align' => "left",
                'text' => Str::ucfirst(__('citizen.validations.s_surname')),
                'value'  =>  "s_surname",
                'sortable' => false
            ],
            [
                'align' => "left",
                'text' => Str::ucfirst(__('citizen.validations.email')),
                'value'  =>  "email",
                'sortable' => false
            ],
            [
                'align' => "left",
                'text' => Str::ucfirst(__('citizen.validations.code')),
                'value'  =>  "code",
                'sortable' => false
            ],
            [
                'align' => "right",
                'text' => Str::ucfirst(__('citizen.validations.created_at')),
                'value'  =>  "created_at",
                'sortable' => false
            ],
            [
                'align' => "right",
                'text' => Str::ucfirst(__('citizen.validations.updated_at')),
                'value'  =>  "updated_at",
                'sortable' => false
            ],
            [
                'align' => "right",
                'text' => Str::ucfirst(__('citizen.validations.actions')),
                'value'  =>  "actions",
                'sortable' => false
            ],
        ];
    }

    public static function expanded()
    {
        return [];
    }
}
