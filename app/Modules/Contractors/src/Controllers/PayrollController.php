<?php


namespace App\Modules\Contractors\src\Controllers;


use App\Http\Controllers\Controller;
use App\Models\Security\Subdirectorate;
use App\Models\Security\User;
use App\Modules\Contractors\src\Constants\PayrollGlobalQuery;
use App\Modules\Contractors\src\Constants\Roles;
use App\Modules\Contractors\src\Exports\BinnacleExport;
use App\Modules\Contractors\src\Exports\InvoicingExport;
use App\Modules\Contractors\src\Exports\MultiSheetInvoicingExport;
use App\Modules\Contractors\src\Exports\TreasuryExport;
use App\Modules\Contractors\src\Jobs\ConfirmContractor;
use App\Modules\Contractors\src\Jobs\ConfirmPayroll;
use App\Modules\Contractors\src\Jobs\GenerateBinnacle;
use App\Modules\Contractors\src\Jobs\GenerateInvoicing;
use App\Modules\Contractors\src\Jobs\PaymentOrderCreation;
use App\Modules\Contractors\src\Models\Payroll;
use App\Modules\Contractors\src\Models\PlanPayment;
use App\Modules\Contractors\src\Request\PayrollReportRequest;
use App\Modules\Contractors\src\Request\StorePayrollRequest;
use App\Modules\Contractors\src\Resources\PayrollResource;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Imtigger\LaravelJobStatus\JobStatus;
use Maatwebsite\Excel\Facades\Excel as ExcelFacade;
use App\Modules\Parks\src\Exports\Excel as ExcelRaw;
use Maatwebsite\Excel\Excel;
use Throwable;

class PayrollController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request): JsonResponse
    {
        $baseQueryStates = PayrollGlobalQuery::queryStates($request, Payroll::query());
        $baseQuery = PayrollGlobalQuery::query($request, Payroll::query());
        // Aplica filtros según el rol del usuario
        $filter = function ($query) {
            if (!auth()->user()->isA(Roles::ROLE_ACCOUNTING, Roles::ROLE_TREASURY, Roles::ROLE_OBSERVER_PAC)) {
                $query->where('subdirection_id', auth()->user()->subdirectorate_id);
            }
        };
        $countState = [
            'invoicing' => $this->countWithStatus(clone $baseQueryStates, $filter, 'INVOICING'),
            'binnacle' => $this->countWithStatus(clone $baseQueryStates, $filter, 'BINNACLE'),
            'pending' => $this->countWithStatus(clone $baseQueryStates, $filter, 'PENDING'),
            'payment' => $this->countWithStatus(clone $baseQueryStates, $filter, 'PAYMENT')
        ];
        // Consulta para los registros paginados
        $payrollsQuery = clone $baseQuery;
        $filter($payrollsQuery);
        $payrolls = $payrollsQuery->with('plans')
            ->latest()
            ->paginate($this->per_page);
        return $this->success_response(
            PayrollResource::collection($payrolls),
            Response::HTTP_OK,
            [
                'headers' => PayrollResource::headers(),
                'count_state' => $countState
            ]
        );
    }

    /**
     * Display the specified resource.
     *
     * @param Payroll $payroll
     * @return JsonResponse
     */
    public function show(Payroll $payroll): JsonResponse
    {
        $payroll->load([
            'plans' => function ($query) {
                $query->orderBy('approval_date');
            },
            'subdirectorate',
            'plans.contract',
            'plans.supervisor',
            'plans.activityReport',
            'plans.compliance',
        ]);

        return $this->success_response(
            new PayrollResource($payroll),
            Response::HTTP_OK,
            [
                'keys'  => array_merge(PayrollResource::headers(), PayrollResource::additionalData())
            ]
        );
    }

    /**
     * Display a store Payroll.
     *
     * @param StorePayrollRequest $request
     * @return JsonResponse
     */
    public function store(StorePayrollRequest $request)
    {
        try {
            $http = new Client();
            $response = $http->post(env('URL_PLAN_PAYMENT') . '/payrolls/', [
                'json' => [
                    'year' => $request->get('year'),
                    'month' => $request->get('month'),
                    'subdirectorId' => $request->get('subdirector_id'),
                    'subdirectionId' => $request->get('subdirection_id'),
                    'isReserve' => $request->get('reserve'),
                    'paymentPlanIdList' => $request->get('plans'),
                    'isCyclePathGuardian' => $request->get('cycle_path_guardian')
                ]
            ]);
            $payroll = json_decode($response->getBody()->getContents(), true);

            $this->dispatch(new PaymentOrderCreation($payroll));

            return $this->success_message(
                __('validation.handler.success'),
                Response::HTTP_CREATED,
                Response::HTTP_CREATED,
                [
                    'id'    => (int) $payroll['id']
                ]
            );
        } catch (RequestException $e) {
            $response = $e->getResponse();
            $responseBody = json_decode($response->getBody()->getContents(), true);
            if ($responseBody['statusCode'] == 400) {
                return $this->error_response(
                    $responseBody['message'],
                    Response::HTTP_BAD_REQUEST,
                    $responseBody['error']
                );
            }
        } catch (\Throwable $e) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param StorePayrollRequest $request
     * @param Payroll $payroll
     * @return JsonResponse
     */
    public function update(StorePayrollRequest $request, Payroll $payroll): JsonResponse
    {
        DB::connection('mysql_contractors')->beginTransaction();
        try {
            $approvedPlans = PlanPayment::query()->whereIn('id', $request->plans)
                ->where('status', 'APPROVED')
                ->where('year', $payroll->year)
                ->where('month', $payroll->month)
                ->get();

            foreach ($approvedPlans as $plan) {
                $plan->update([
                    'payroll_id' => $payroll->id,
                    'status' => PlanPayment::STATUS_PAYROLL,
                ]);
            }
            DB::connection('mysql_contractors')->commit();
            return $this->success_message(__('validation.handler.updated'));
        } catch (Throwable $e) {
            DB::connection('mysql_contractors')->rollBack();
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }

    public function verify(Payroll $payroll): JsonResponse
    {
        try {
            $payroll->verified = true;
            $payroll->saveOrFail();
            $this->dispatch(new ConfirmPayroll($payroll));
            return $this->success_message(__('validation.handler.updated'));
        } catch (Throwable $e) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }
    }

    /**
     * Generate binnacle in job.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function generateBinnacle(Request $request): JsonResponse
    {
        try {
            $http = new Client();
            $response = $http->put(env('URL_PLAN_PAYMENT') . "/payrolls/binnacle", [
                'json' => $request->all(),
            ]);

            $data = json_decode($response->getBody()->getContents(), true);
            return $this->success_message(
                'Se ha generado el proceso de bitácoras, te notificaremos una vez esté listo.',
                Response::HTTP_OK,
                $data
            );

        } catch (\Exception $e) {
            return $this->error_response(
                'Ocurrió un error durante el proceso de bitácoras. Por favor, intenta de nuevo.',
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }

    }

    /**
     * Generate Invoicing in job.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function generateInvoicing(Request $request): JsonResponse
    {
        try {
            $http = new Client();
            $response = $http->put(env('URL_PLAN_PAYMENT') . "/payrolls/invoicing", [
                'json' => $request->all(),
            ]);

            $data = json_decode($response->getBody()->getContents(), true);
            return $this->success_message(
                'Se ha generado el proceso de causación, te notificaremos una vez esté listo.',
                Response::HTTP_OK,
                $data
            );

        } catch (\Exception $e) {
            return $this->error_response(
                'Ocurrió un error durante el proceso de causación. Por favor, intenta de nuevo.',
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $e->getMessage()
            );
        }

    }

    /**
     * Export file binnacle in XLSX
     *
     * @param PayrollReportRequest $request
     * @return JsonResponse
     */
    public function getBinnacleReport(PayrollReportRequest $request): JsonResponse
    {
        $http = new Client();
        $payroll_id = $request->get('payroll_id');
        $response_keys = $http->get(env('URL_PLAN_PAYMENT') . "/payrolls/report/$payroll_id");
        $keys = json_decode($response_keys->getBody()->getContents(), true);
        $payroll = collect($keys);
        $sub_address = Subdirectorate::query()->find($payroll['subdirectorateId']);
        $file = ExcelRaw::raw(new BinnacleExport($payroll, $sub_address), Excel::XLSX);
        $file_name = 'REPORTE_BITACORA_'. $payroll['name'] . "_" . now()->format('Y-m-d') . '.xlsx';
        ExcelFacade::store($file, $file_name, 'public');
        if (Storage::disk('public')->exists($file_name)) {
            return $this->success_message([
                'file' => 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,' . base64_encode($file),
                'file_name' => $file_name
            ]);
        } else {
            return $this->error_response(
                'No se pudo generar el archivo, por favor intente más tarde.',
                422
            );
        }
    }

    /**
     * Export file invoicing in XLSX
     *
     * @param PayrollReportRequest $request
     * @return JsonResponse
     */
    public function getInvoicingReport(PayrollReportRequest $request): JsonResponse
    {
        $http = new Client();
        $payroll_id = $request->get('payroll_id');
        $response_keys = $http->get(env('URL_PLAN_PAYMENT') . "/payrolls/report/$payroll_id");
        $keys = json_decode($response_keys->getBody()->getContents(), true);
        $payroll = collect($keys);
        $sub_address = Subdirectorate::query()->find($payroll['subdirectorateId']);
        $supervisor = User::includeExpiredAndTrashed()->find($payroll['subdirectorId']);
        $file = ExcelRaw::raw(new MultiSheetInvoicingExport($payroll, $sub_address, $supervisor), Excel::XLSX);

        $file_name = 'REPORTE_CAUSACION_'. $payroll['name'] . "_" . now()->format('Y-m-d') . '.xlsx';
        ExcelFacade::store($file, $file_name, 'public');
        if (Storage::disk('public')->exists($file_name)) {
            return $this->success_message([
                'file' => 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,' . base64_encode($file),
                'file_name' => $file_name
            ]);
        } else {
            return $this->error_response(
                'No se pudo generar el archivo, por favor intente más tarde.',
                422
            );
        }
    }

    /**
     * Export file invoicing in XLSX
     *
     * @param PayrollReportRequest $request
     * @return JsonResponse
     */
    public function getPacReport(PayrollReportRequest $request): JsonResponse
    {
        $http = new Client();
        $payroll_id = $request->get('payroll_id');
        $response_keys = $http->get(env('URL_PLAN_PAYMENT') . "/payrolls/report/$payroll_id");
        $keys = json_decode($response_keys->getBody()->getContents(), true);
        $payroll = collect($keys);
        $sub_address = Subdirectorate::query()->find($payroll['subdirectorateId']);
        $supervisor = User::includeExpiredAndTrashed()->find($payroll['subdirectorId']);
        $file = ExcelRaw::raw(new TreasuryExport($payroll, $sub_address, $supervisor), Excel::XLSX);
        $file_name = 'REPORTE_PAC_'. $payroll['name'] . "_" . now()->format('Y-m-d') . '.xlsx';
        ExcelFacade::store($file, $file_name, 'public');
        if (Storage::disk('public')->exists($file_name)) {
            return $this->success_message([
                'file' => 'data:application/vnd.openxmlformats-officedocument.spreadsheetml.sheet;base64,' . base64_encode($file),
                'file_name' => $file_name
            ]);
        } else {
            return $this->error_response(
                'No se pudo generar el archivo, por favor intente más tarde.',
                422
            );
        }
    }

    private function countWithStatus($query, callable $filter, string $status): int
    {
        $filter($query);
        return $query->where('status', $status)->count();
    }
}
