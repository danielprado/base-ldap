<?php

namespace App\Modules\Parks\src\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\Parks\src\Constants\Roles;
use App\Modules\Parks\src\Models\Feature;
use App\Modules\Parks\src\Models\Park;
use App\Modules\Parks\src\Resources\FeaturesResource;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use App\Modules\Parks\src\Exports\parksExportTable;
use App\Modules\Parks\src\Models\ParkEndowment;

class FeatureController extends Controller
{
    //
    public function __construct()
	{
		parent::__construct();
		$this->middleware('auth:api')->only(['remove', 'add']);
		// $this->middleware(Roles::actions(Feature::class, 'create_or_manage'))->only('create');
		// $this->middleware(Roles::actions(Feature::class, 'update_or_manage'))->only('update');
		// $this->middleware(Roles::actions(Feature::class, 'destroy_or_manage'))->only('destroy');
        
	}

    public function features()
    {
        $features = Feature::all();
        return $this->success_response(
            FeaturesResource::collection($features)
        );
    }

    public function remove(Request $request)
    {
        $parkId = $request->parkId;
        $featureId = $request->featureId;
        $park = Park::find($parkId);
        $park->features()->detach($featureId);
        return $this->success_message(__('validation.handler.success'));
    }

    public function add(Request $request)
    {
        $parkId = $request->parkId;
        $featureIds = $request->featureIds;
        $park = Park::find($parkId);
        foreach ($featureIds as $featureId) {
            $park->features()->attach($featureId);
        }
        return $this->success_message(__('validation.handler.success'));
    }

    public function reportEndowmentDotacion(Request $request){
        $parksEndoments = ParkEndowment::with('park', 'endowment', 'endowment.equipment')
            ->where('Id_Dotacion', $request->endowmentId)
            ->groupBy('Id_Parque')
            ->get();
        $filtered_collection = $parksEndoments->filter(function ($item) {
            return $item->park !== null;
        })->values();
        //return $filtered_collection;
        return Excel::download(new parksExportTable($filtered_collection), 'reporte_dotaciones_parques.xlsx');
    }
}
