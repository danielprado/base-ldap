<?php

namespace App\Modules\Contractors\src\Models;

use Illuminate\Database\Eloquent\Model;


class PmrPlanPayment extends Model
{

    protected $connection = 'mysql_contractors';
    protected $table = 'pmr_configurations';

    protected $primaryKey = 'id';

    protected $fillable = [
        'payment_plan_id',
        'description',
        'rp_nume',
        'position`',
        'balance',
        'month',
        'total_pay_month',
        'adjusted_units',
        'funding_source',
        'reserve',
        'pmr_seven_id',
        'rp_seven_id',
        'entry',
        'destinity_code',
        'financial_type',
        'binnacle_date',
        'binnacle_bir_numo',
        'binnacle_bir_cont',
        'invoicing_date',
        'invoicing_fac_nume',
        'expense_component'
    ];

    protected $dates = ['binnacle_date', 'invoicing_date'];

    public function compliance()
    {
        return $this->belongsTo(Compliance::class);
    }

    public function planPayment()
    {
        return $this->belongsTo(PlanPayment::class, 'payment_plan_id', 'id');
    }
}
