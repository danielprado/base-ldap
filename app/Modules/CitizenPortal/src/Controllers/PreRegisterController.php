<?php


namespace App\Modules\CitizenPortal\src\Controllers;


use App\Http\Controllers\Controller;
use App\Modules\CitizenPortal\src\Models\Consent;
use App\Modules\CitizenPortal\src\Models\PreRegister;
use App\Modules\CitizenPortal\src\Request\ActivityRequest;
use App\Modules\CitizenPortal\src\Request\ConsentRequest;
use App\Modules\CitizenPortal\src\Request\StorePreRegisterRequest;
use App\Modules\CitizenPortal\src\Request\UpdatePreRegisterRequest;
use App\Modules\CitizenPortal\src\Resources\ConsentResource;
use App\Modules\CitizenPortal\src\Resources\PreRegisterResource;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

class PreRegisterController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        $query = $this->setQuery(PreRegister::query(), (new PreRegister)->getSortableColumn($this->column))
            ->when(isset($this->query), function ($query) {
                return $query
                    ->where('document', 'like', "%$this->query%")
                    ->orWhere('name', 'like', "%$this->query%")
                    ->orWhere('s_name', 'like', "%$this->query%")
                    ->orWhere('surname', 'like', "%$this->query%")
                    ->orWhere('s_surname', 'like', "%$this->query%")
                    ->orWhere('email', 'like', "%$this->query%")
                    ->orWhere('code', 'like', "%$this->query%");
            })
            ->orderBy((new Consent)->getSortableColumn($this->column), $this->order);
        return $this->success_response(
            PreRegisterResource::collection($query->paginate( $this->per_page)),
            Response::HTTP_OK,
            [
                'headers'   => PreRegisterResource::headers(),
            ]
        );
    }

    /**
     * @param StorePreRegisterRequest $request
     * @return JsonResponse
     */
    public function store(StorePreRegisterRequest $request)
    {
        $model = new PreRegister();
        $model->fill($request->validated());
        $digits = 6;
        $code = rand(pow(10, $digits-1), pow(10, $digits)-1);
        $model->code = $code;
        $model->save();
        return $this->success_message(
            __('validation.handler.success'),
            Response::HTTP_CREATED
        );
    }

    /**
     * @param UpdatePreRegisterRequest $request
     * @param PreRegister $register
     * @return JsonResponse
     */
    public function update(UpdatePreRegisterRequest $request, PreRegister $register)
    {
        $register->fill($request->validated());
        $digits = 6;
        $code = rand(pow(10, $digits-1), pow(10, $digits)-1);
        $register->code = $code;
        $register->save();
        return $this->success_message(
            __('validation.handler.updated')
        );
    }


    /**
     * @param PreRegister $register
     * @return JsonResponse
     * @throws Exception
     */
    public function destroy(PreRegister $register)
    {
        $register->delete();
        return $this->success_message(
            __('validation.handler.deleted'),
            Response::HTTP_OK,
            Response::HTTP_NO_CONTENT
        );
    }
}
