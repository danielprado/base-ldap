<?php

namespace App\Modules\Parks\src\Constants;

class ArgisUrls
{

    /**
     * @return string[]
     */
    public function urls() {
        return [
            env('URL_BASE_ARCGIS') . '/juegos/MapServer/0?f=pjson',
            env('URL_BASE_ARCGIS') . '/juegos/MapServer/1?f=pjson',
            env('URL_BASE_ARCGIS') . '/juegos/MapServer/2?f=pjson',
            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/20?f=pjson',
            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/21?f=pjson',
            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/22?f=pjson',
            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/23?f=pjson',
            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/24?f=pjson',
            env('URL_BASE_ARCGIS') . '/Ecologico/MapServer/0?f=pjson',
            env('URL_BASE_ARCGIS') . '/Ecologico/MapServer/1?f=pjson',
            env('URL_BASE_ARCGIS') . '/Ecologico/MapServer/2?f=pjson',
            env('URL_BASE_ARCGIS') . '/Multifuncional/MapServer/0?f=pjson',
            env('URL_BASE_ARCGIS') . '/Plazoletas/MapServer/0?f=pjson',
            env('URL_BASE_ARCGIS') . '/Plazoletas/MapServer/1?f=pjson',
            env('URL_BASE_ARCGIS') . '/ServiciosAuxiliares/MapServer/0?f=pjson',
            env('URL_BASE_ARCGIS') . '/ServiciosAuxiliares/MapServer/1?f=pjson',
            env('URL_BASE_ARCGIS') . '/ServiciosAuxiliares/MapServer/2?f=pjson',
        ];
    }

     /**
     * @return string[]
     */
    public function urlsAllEndowment() {
        return [
            env('URL_BASE_ARCGIS') . '/juegos/MapServer/0/query?f=pjson&where=1=1&resultOffset=0&returnGeometry=false&outFields=*',
            env('URL_BASE_ARCGIS') . '/juegos/MapServer/0/query?f=pjson&where=1=1&resultOffset=2000&returnGeometry=false&outFields=*',

            env('URL_BASE_ARCGIS') . '/juegos/MapServer/1/query?f=pjson&where=1=1&resultOffset=0&returnGeometry=false&outFields=*',
            env('URL_BASE_ARCGIS') . '/juegos/MapServer/1/query?f=pjson&where=1=1&resultOffset=2000&returnGeometry=false&outFields=*',

            env('URL_BASE_ARCGIS') . '/juegos/MapServer/2/query?f=pjson&where=1=1&resultOffset=0&returnGeometry=false&outFields=*',
            env('URL_BASE_ARCGIS') . '/juegos/MapServer/2/query?f=pjson&where=1=1&resultOffset=2000&returnGeometry=false&outFields=*',

            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/20/query?f=pjson&where=1=1&resultOffset=0&returnGeometry=false&outFields=*',
            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/20/query?f=pjson&where=1=1&resultOffset=2000&returnGeometry=false&outFields=*',

            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/21/query?f=pjson&where=1=1&resultOffset=0&returnGeometry=false&outFields=*',
            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/21/query?f=pjson&where=1=1&resultOffset=2000&returnGeometry=false&outFields=*',

            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/22/query?f=pjson&where=1=1&resultOffset=0&returnGeometry=false&outFields=*',
            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/22/query?f=pjson&where=1=1&resultOffset=2000&returnGeometry=false&outFields=*',

            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/23/query?f=pjson&where=1=1&resultOffset=0&returnGeometry=false&outFields=*',
            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/23/query?f=pjson&where=1=1&resultOffset=2000&returnGeometry=false&outFields=*',

            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/24/query?f=pjson&where=1=1&resultOffset=0&returnGeometry=false&outFields=*',
            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/24/query?f=pjson&where=1=1&resultOffset=2000&returnGeometry=false&outFields=*',

            env('URL_BASE_ARCGIS') . '/Ecologico/MapServer/0/query?f=pjson&where=1=1&resultOffset=0&returnGeometry=false&outFields=*',
            env('URL_BASE_ARCGIS') . '/Ecologico/MapServer/0/query?f=pjson&where=1=1&resultOffset=2000&returnGeometry=false&outFields=*',

            env('URL_BASE_ARCGIS') . '/Ecologico/MapServer/1/query?f=pjson&where=1=1&resultOffset=0&returnGeometry=false&outFields=*',
            env('URL_BASE_ARCGIS') . '/Ecologico/MapServer/1/query?f=pjson&where=1=1&resultOffset=2000&returnGeometry=false&outFields=*',

            env('URL_BASE_ARCGIS') . '/Ecologico/MapServer/2/query?f=pjson&where=1=1&resultOffset=0&returnGeometry=false&outFields=*',
            env('URL_BASE_ARCGIS') . '/Ecologico/MapServer/2/query?f=pjson&where=1=1&resultOffset=2000&returnGeometry=false&outFields=*',

            env('URL_BASE_ARCGIS') . '/Multifuncional/MapServer/0/query?f=pjson&where=1=1&resultOffset=0&returnGeometry=false&outFields=*',
            env('URL_BASE_ARCGIS') . '/Multifuncional/MapServer/0/query?f=pjson&where=1=1&resultOffset=2000&returnGeometry=false&outFields=*',

            env('URL_BASE_ARCGIS') . '/Plazoletas/MapServer/0/query?f=pjson&where=1=1&resultOffset=0&returnGeometry=false&outFields=*',
            env('URL_BASE_ARCGIS') . '/Plazoletas/MapServer/0/query?f=pjson&where=1=1&resultOffset=2000&returnGeometry=false&outFields=*',

            env('URL_BASE_ARCGIS') . '/Plazoletas/MapServer/1/query?f=pjson&where=1=1&resultOffset=0&returnGeometry=false&outFields=*',
            env('URL_BASE_ARCGIS') . '/Plazoletas/MapServer/1/query?f=pjson&where=1=1&resultOffset=2000&returnGeometry=false&outFields=*',

            env('URL_BASE_ARCGIS') . '/ServiciosAuxiliares/MapServer/0/query?f=pjson&where=1=1&resultOffset=0&returnGeometry=false&outFields=*',
            env('URL_BASE_ARCGIS') . '/ServiciosAuxiliares/MapServer/0/query?f=pjson&where=1=1&resultOffset=2000&returnGeometry=false&outFields=*',

            env('URL_BASE_ARCGIS') . '/ServiciosAuxiliares/MapServer/1/query?f=pjson&where=1=1&resultOffset=0&returnGeometry=false&outFields=*',
            env('URL_BASE_ARCGIS') . '/ServiciosAuxiliares/MapServer/1/query?f=pjson&where=1=1&resultOffset=2000&returnGeometry=false&outFields=*',

            env('URL_BASE_ARCGIS') . '/ServiciosAuxiliares/MapServer/2/query?f=pjson&where=1=1&resultOffset=0&returnGeometry=false&outFields=*',
            env('URL_BASE_ARCGIS') . '/ServiciosAuxiliares/MapServer/2/query?f=pjson&where=1=1&resultOffset=2000&returnGeometry=false&outFields=*',

        ];
    }

         /**
     * @return string[]
     */
    public function urlsAllFurniture() {
        return [
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=0',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=2000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=4000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=6000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=8000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=10000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=12000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=14000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=16000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=18000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=20000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=22000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=24000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=26000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=28000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=30000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=32000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=34000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=36000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=38000',

            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=0',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=2000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=4000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=6000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=8000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=10000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=12000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=14000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=16000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=18000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=20000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=22000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=24000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=26000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=28000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=30000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=32000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=34000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=36000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=38000',

            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=0',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=2000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=4000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=6000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=8000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=10000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=12000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=14000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=16000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=18000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=20000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=22000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=24000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=26000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=28000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=30000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=32000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=34000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=36000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=38000',

            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=0',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=2000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=4000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=6000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=8000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=10000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=12000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=14000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=16000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=18000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=20000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=22000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=24000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=26000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=28000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=30000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=32000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=34000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=36000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=38000',

            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=0',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=2000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=4000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=6000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=8000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=10000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=12000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=14000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=16000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=18000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=20000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=22000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=24000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=26000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=28000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=30000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=32000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=34000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=36000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=38000',

            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=0',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=2000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=4000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=6000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=8000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=10000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=12000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=14000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=16000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=18000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=20000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=22000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=24000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=26000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=28000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=30000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=32000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=34000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=36000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=38000',

            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=0',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=2000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=4000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=6000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=8000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=10000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=12000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=14000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=16000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=18000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=20000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=22000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=24000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=26000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=28000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=30000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=32000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=34000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=36000',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?f=pjson&where=1=1&outFields=*&returnGeometry=false&resultOffset=38000',
        ];
    }

     /**
     * @return string[]
     */
    public function urlsTypesFurniture() {
        return [
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0?f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1?f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2?f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31?f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32?f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33?f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34?f=pjson',
        ];
    }

      /**
     * @return string[]
     */
    public function urlsEndowmentLocalities($result) {
        return [
            env('URL_BASE_ARCGIS') . '/juegos/MapServer/0/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/juegos/MapServer/0/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/juegos/MapServer/1/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/juegos/MapServer/1/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/juegos/MapServer/2/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/juegos/MapServer/2/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/20/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/20/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/21/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/21/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/22/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/22/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/23/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/23/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/24/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/24/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Ecologico/MapServer/0/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Ecologico/MapServer/0/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Ecologico/MapServer/1/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Ecologico/MapServer/1/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Ecologico/MapServer/2/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Ecologico/MapServer/2/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Multifuncional/MapServer/0/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Multifuncional/MapServer/0/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Plazoletas/MapServer/0/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Plazoletas/MapServer/0/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Plazoletas/MapServer/1/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Plazoletas/MapServer/1/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/ServiciosAuxiliares/MapServer/0/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/ServiciosAuxiliares/MapServer/0/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/ServiciosAuxiliares/MapServer/1/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/ServiciosAuxiliares/MapServer/1/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/ServiciosAuxiliares/MapServer/2/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/ServiciosAuxiliares/MapServer/2/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',
        ];
    }

       /**
     * @return string[]
     */
    public function urlsEndowmentRisk($risk, $id) {
        return [
            env('URL_BASE_ARCGIS') . '/juegos/MapServer/0/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/juegos/MapServer/0/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/juegos/MapServer/1/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/juegos/MapServer/1/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/juegos/MapServer/2/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/juegos/MapServer/2/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/20/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/20/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/21/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/21/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/22/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/22/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/23/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/23/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/24/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/24/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Ecologico/MapServer/0/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Ecologico/MapServer/0/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Ecologico/MapServer/1/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Ecologico/MapServer/1/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Ecologico/MapServer/2/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Ecologico/MapServer/2/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Multifuncional/MapServer/0/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Multifuncional/MapServer/0/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Plazoletas/MapServer/0/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Plazoletas/MapServer/0/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Plazoletas/MapServer/1/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Plazoletas/MapServer/1/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/ServiciosAuxiliares/MapServer/0/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/ServiciosAuxiliares/MapServer/0/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/ServiciosAuxiliares/MapServer/1/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/ServiciosAuxiliares/MapServer/1/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/ServiciosAuxiliares/MapServer/2/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/ServiciosAuxiliares/MapServer/2/query?where='. $risk .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',
        ];
    }

       /**
     * @return string[]
     */
    public function urlsFurnitureLocalities($result) {
        return [
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=4000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=6000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=8000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=10000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=12000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=14000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=16000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=18000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=20000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=22000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=24000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=4000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=6000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=8000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=10000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=12000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=14000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=16000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=18000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=20000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=22000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=24000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=4000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=6000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=8000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=10000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=12000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=14000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=16000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=18000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=20000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=22000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=24000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=4000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=6000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=8000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=10000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=12000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=14000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=16000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=18000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=20000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=22000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=24000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=4000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=6000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=8000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=10000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=12000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=14000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=16000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=18000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=20000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=22000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=24000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=4000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=6000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=8000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=10000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=12000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=14000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=16000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=18000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=20000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=22000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=24000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=4000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=6000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=8000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=10000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=12000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=14000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=16000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=18000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=20000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=22000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=UB_REFERENCIA%20IN' . $result . '&outFields=*&returnGeometry=false&resultOffset=24000&f=pjson',
        ];
    }

        /**
     * @return string[]
     */
    public function urlsFurnitureRisk($risk, $id) {
        return [
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=4000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=6000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=8000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=10000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=12000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=14000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=16000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=18000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=20000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=22000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=24000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=26000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=28000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=30000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=32000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=34000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=36000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=38000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=40000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=42000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=44000&f=pjson',


            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=4000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=6000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=8000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=10000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=12000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=14000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=16000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=18000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=20000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=22000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=24000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=26000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=28000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=30000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=32000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=34000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=36000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=38000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=40000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=42000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=44000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=4000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=6000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=8000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=10000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=12000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=14000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=16000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=18000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=20000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=22000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=24000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=26000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=28000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=30000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=32000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=34000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=36000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=38000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=40000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=42000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=44000&f=pjson',


            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=4000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=6000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=8000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=10000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=12000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=14000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=16000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=18000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=20000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=22000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=24000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=26000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=28000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=30000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=32000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=34000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=36000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=38000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=40000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=42000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=44000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=4000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=6000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=8000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=10000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=12000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=14000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=16000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=18000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=20000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=22000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=24000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=26000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=28000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=30000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=32000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=34000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=36000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=38000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=40000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=42000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=44000&f=pjson',


            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=4000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=6000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=8000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=10000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=12000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=14000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=16000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=18000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=20000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=22000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=24000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=26000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=28000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=30000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=32000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=34000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=36000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=38000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=40000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=42000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=44000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=4000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=6000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=8000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=10000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=12000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=14000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=16000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=18000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=20000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=22000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=24000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=26000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=28000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=30000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=32000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=34000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=36000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=38000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=40000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=42000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=' . $risk  .'%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=44000&f=pjson',
        ];
    }

       /**
     * @return string[]
     */
    public function urlsEndowmentStatus($id) {
        return [
            env('URL_BASE_ARCGIS') . '/juegos/MapServer/0/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&f=pjson',
            env('URL_BASE_ARCGIS') . '/juegos/MapServer/0/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/juegos/MapServer/1/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&f=pjson',
            env('URL_BASE_ARCGIS') . '/juegos/MapServer/1/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/juegos/MapServer/2/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&f=pjson',
            env('URL_BASE_ARCGIS') . '/juegos/MapServer/2/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/20/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&f=pjson',
            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/20/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/21/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&f=pjson',
            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/21/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/22/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&f=pjson',
            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/22/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/23/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&f=pjson',
            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/23/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/24/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&f=pjson',
            env('URL_BASE_ARCGIS') . '/Deportivo/MapServer/24/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Ecologico/MapServer/0/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&f=pjson',
            env('URL_BASE_ARCGIS') . '/Ecologico/MapServer/0/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Ecologico/MapServer/1/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&f=pjson',
            env('URL_BASE_ARCGIS') . '/Ecologico/MapServer/1/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Ecologico/MapServer/2/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&f=pjson',
            env('URL_BASE_ARCGIS') . '/Ecologico/MapServer/2/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Multifuncional/MapServer/0/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&f=pjson',
            env('URL_BASE_ARCGIS') . '/Multifuncional/MapServer/0/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Plazoletas/MapServer/0/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&f=pjson',
            env('URL_BASE_ARCGIS') . '/Plazoletas/MapServer/0/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Plazoletas/MapServer/1/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&f=pjson',
            env('URL_BASE_ARCGIS') . '/Plazoletas/MapServer/1/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/ServiciosAuxiliares/MapServer/0/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&f=pjson',
            env('URL_BASE_ARCGIS') . '/ServiciosAuxiliares/MapServer/0/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/ServiciosAuxiliares/MapServer/1/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&f=pjson',
            env('URL_BASE_ARCGIS') . '/ServiciosAuxiliares/MapServer/1/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',

            env('URL_BASE_ARCGIS') . '/ServiciosAuxiliares/MapServer/2/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&f=pjson',
            env('URL_BASE_ARCGIS') . '/ServiciosAuxiliares/MapServer/2/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',
        ];
    }

       /**
     * @return string[]
     */
    public function urlsFurnitureStatus($id) {
        return [
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=4000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=6000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=8000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=10000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=12000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=14000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=16000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=18000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=20000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=22000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/0/query?where=ESTADO_DIAG%3D' . $id . '&outFields=*&returnGeometry=false&resultOffset=24000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=4000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=6000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=8000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=10000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=12000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=14000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=16000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=18000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=20000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=22000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/1/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=24000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=4000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=6000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=8000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=10000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=12000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=14000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=16000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=18000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=20000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=22000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/2/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=24000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=4000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=6000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=8000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=10000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=12000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=14000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=16000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=18000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=20000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=22000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/31/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=24000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=4000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=6000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=8000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=10000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=12000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=14000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=16000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=18000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=20000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=22000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/32/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=24000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=4000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=6000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=8000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=10000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=12000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=14000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=16000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=18000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=20000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=22000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/33/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=24000&f=pjson',

            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=0&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=2000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=4000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=6000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=8000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=10000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=12000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=14000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=16000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=18000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=20000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=22000&f=pjson',
            env('URL_BASE_ARCGIS') . '/Mobiliario/MapServer/34/query?where=ESTADO_DIAG%3D' . $id .'&outFields=*&returnGeometry=false&resultOffset=24000&f=pjson',
        ];
    }

}
