<?php

namespace App\Modules\Contractors\src\Mail;

use App\Modules\Contractors\src\Models\Compliance;
use App\Modules\Contractors\src\Models\Contract;
use App\Modules\Contractors\src\Models\Contractor;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ComplianceReminderMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Contractor
     */
    private $user;

    /**
     * @var Contract
     */
    private $contract;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Contractor $user, Contract $contract)
    {
        $this->user = $user;
        $this->contract = $contract;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $full_name = $this->user->full_name ?? '';
        $contractNumber = $this->contract->contract;
        
        $path = config('app.env') != 'production'
            ? "/portal-contratista-dev"
            : "/";

        $subdomain = config('app.env') != 'production' ? "sim" : "portalcontratista";

        $url = "https://{$subdomain}.idrd.gov.co{$path}";

        return $this->view('mail.mail')
            ->subject('Recordatorio de Certificado de Cumplimiento - ' . $contractNumber)
            ->with([
                'header'    => 'IDRD',
                'title'     => 'Recordatorio de Certificado de Cumplimiento',
                'content'   => "¡Hola {$full_name}! Recuerda completar tu certificado de cumplimiento mensual. A partir del día de mañana, podrás realizar esta acción a través del siguiente botón.",
                'url'       => "{$url}/es/cumplimiento",
                'remember'  =>  'Recuerde que el plazo para completar este formato es limitado. Contacte con su técnico para validar la disponibilidad.',
                'year'      => now()->year
            ]);
    }
}
