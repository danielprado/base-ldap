<?php
namespace App\Modules\InfoApp\src\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Modules\InfoApp\src\Models\Notice;

class NoticeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $notices = Notice::all();
        return response()->json($notices);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $notice = new Notice();
        $notice->fill($request->only([
            'image1',
            'image2',
            'nombre',
            'resumen',
            'descripcion',
            'categoria_id',
            'estado',
            'fechaSubida',
            'destacado',
        ]));
        $notice->save();
        return response()->json($notice, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $notice = Notice::find($id);
        if (!$notice) {
            return response()->json(['error' => 'Noticia no encontrado'], 404);
        }
        return response()->json($notice);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $notice = Notice::find($id);
        if (!$notice) {
            return response()->json(['error' => 'Noticia no encontrado'], 404);
        }
        $notice->fill($request->only([
            'image1',
            'image2',
            'nombre',
            'resumen',
            'descripcion',
            'categoria_id',
            'estado',
            'fechaSubida',
            'destacado',
        ]));
        $notice->save();
        return response()->json($notice);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $notice = Notice::find($id);
        if (!$notice) {
            return response()->json(['error' => 'Noticia no encontrado'], 404);
        }
        $notice->delete();
        return response()->json(['message' => 'Noticia borrado correctamente']);
    }
}