<?php


namespace App\Modules\Contractors\src\Resources;


use App\Models\Security\Subdirectorate;
use App\Models\Security\User;
use App\Modules\Contractors\src\Constants\Roles;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;


class PayrollResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $address = Subdirectorate::query()->find($this->subdirection_id);
        $number = str_pad($this->id, 4, '0', STR_PAD_LEFT);
        $user_order = User::query()->find($this->subdirector_id);
        return [
            'id'                => $this->id ?? null,
            'sub_address_id'    => $this->subdirection_id ?? null,
            'sub_address'       => isset($address) ? $address->name : null,
            'name'              => isset($address) ? "$number " . $address->name : null,
            'order_name'        => isset($user_order) ? $user_order->full_name : null,
            'state'             => $this->getStatus() ?? null,
            'total_payment'     => $this->applyMoneyFormat($this->total_plans_payment) ?? null,
            'order_date'        => isset($this->created_at) ? Carbon::parse($this->created_at)->format('Y-m-d')  : null,
            'plans_number'      => (int) $this->plans->count(),
            'period'            => $this->getPeriod() ?? null,
            'year'              => isset($this->year) ? (int) $this->year : null,
            'month'             => isset($this->month) ? (int) $this->month : null,
            'plans'             => PaymentPlanResource::collection($this->plans) ?? [],
            'plans_headers'     => PaymentPlanResource::headers(),
            'binnacle'          => $this->getBinnacleReport() ?? null,
            'invoicing'         => $this->getInvoicingReport() ?? null,
            'pac'               => $this->getPacReport() ?? null,
            'verified'          => (boolean) $this->verified,
            'reserve'           => (boolean) $this->reserve,
            'reserve_text'      => $this->reserve ? 'SI' : 'NO',
        ];
    }

    public function getStatus(): string
    {
        switch ($this->status) {
            case 'BINNACLE':
                return 'CON BITÁCORA';
            case 'INVOICING':
                return 'CAUSADA';
            case 'PAYMENT':
                return 'PAGADA';
            default:
                return 'PENDIENTE';
        }
    }

    public function getPeriod(): string
    {
        $date = Carbon::create(null, $this->month);
        Carbon::setLocale('es');
        return toUpper($date->isoFormat('MMMM')) . ' - ' . $this->year;
    }

    public function getBinnacleReport(): int
    {
        return $this->status !== 'PENDING' && $this->verified == true ? 1 : 0;
    }

    public function getInvoicingReport(): int
    {
        if($this->status === 'PENDING' || $this->status === 'BINNACLE' || $this->verified == false) {
            return 0;
        }
        return 1;
    }

    public function getPacReport(): int
    {
        if($this->status === 'PENDING' || $this->status === 'BINNACLE' || $this->verified == false) {
            return 0;
        }
        return 1;
    }

    private function applyMoneyFormat($value)
    {
        return $value
            ? preg_replace('/\s+/', '', cop_money_format($value, '$', null, 0, null, '.'))
            : null;
    }

    public static function headers()
    {
        return auth()->user()->isA(Roles::ROLE_ADMIN, Roles::ROLE_ACCOUNTING, Roles::ROLE_TREASURY)
            ? [
                [
                    'text' => "#",
                    'value'  =>  "id",
                ],
                [
                    'align' => "center",
                    'text' => "Detalles",
                    'value'  =>  "view",
                ],
                [
                    'text' => "Consecutivo de la planilla",
                    'value'  =>  "name",
                    'icon'  =>  'mdi-ab-testing',
                ],
                [
                    'align' => "center",
                    'text' => "Periodo de pago",
                    'value'  =>  "period",
                    'icon'  =>  'mdi-calendar-text',
                ],
                [
                    'align' => "center",
                    'text' => "Estado",
                    'value'  =>  "state",
                    'icon'  =>  'mdi-face-man',
                ],
                [
                    'align' => "center",
                    'text' => "Es reserva?",
                    'value'  =>  "reserve_text",
                    'icon'  =>  'mdi-face-man',
                ],
                [
                    'align' => "center",
                    'text' => "Bitácora",
                    'value'  =>  "binnacle",
                ],
                [
                    'align' => "center",
                    'text' => "Causación",
                    'value'  =>  "invoicing",
                ],
                [
                    'align' => "center",
                    'text' => "PAC",
                    'value'  =>  "pac",
                ],
                [
                    'text' => "Valor total de la planilla",
                    'value'  =>  "total_payment",
                    'icon'  =>  'mdi-face-man',
                ],
                [
                    'align' => "center",
                    'text' => "Total planes agregados",
                    'value'  =>  "plans_number",
                    'icon'  =>  'mdi-face-man',
                ],
                [
                    'text' => "Nombre del ordenador",
                    'value'  =>  "order_name",
                    'icon'  =>  'mdi-face-man',
                ],
                [
                    'text' => "Fecha de ordenación",
                    'value'  =>  "order_date",
                    'icon'  =>  'mdi-calendar-account-outline',
                ],
                [
                    'align' => "center",
                    'text' => "Validar",
                    'value'  =>  "verified",
                    'icon'  =>  'mdi-text-box-check',
                ],
            ] : [
                [
                    'text' => "#",
                    'value'  =>  "id",
                ],
                [
                    'align' => "right",
                    'text' => "Detalles",
                    'value'  =>  "view",
                ],
                [
                    'text' => "Consecutivo de la planilla",
                    'value'  =>  "name",
                    'icon'  =>  'mdi-ab-testing',
                ],
                [
                    'align' => "center",
                    'text' => "Periodo de pago",
                    'value'  =>  "period",
                    'icon'  =>  'mdi-calendar-text',
                ],
                [
                    'text' => "Estado",
                    'value'  =>  "state",
                    'icon'  =>  'mdi-face-man',
                ],
                [
                    'align' => "center",
                    'text' => "Es reserva?",
                    'value'  =>  "reserve_text",
                    'icon'  =>  'mdi-face-man',
                ],
                /*[
                    'align' => "center",
                    'text' => "Causación",
                    'value'  =>  "invoicing",
                ],*/
                [
                    'align' => "center",
                    'text' => "PAC",
                    'value'  =>  "pac",
                ],
                [
                    'text' => "Valor total de la planilla",
                    'value'  =>  "total_payment",
                    'icon'  =>  'mdi-face-man',
                ],
                [
                    'text' => "Total planes agregados",
                    'value'  =>  "plans_number",
                ],
                [
                    'text' => "Nombre del ordenador",
                    'value'  =>  "order_name",
                    'icon'  =>  'mdi-face-man',
                ],
                [
                    'text' => "Fecha de ordenación",
                    'value'  =>  "order_date",
                    'icon'  =>  'mdi-calendar-account-outline',
                ],
            ];
    }

    public static function additionalData()
    {
        /*return auth()->user()->isA(Roles::ROLE_ADMIN, Roles::ROLE_ACCOUNTING)
            ? [
                [
                    'text' => "Nombre de la planilla",
                    'value'  =>  "name",
                ],
                [
                    'align' => "center",
                    'text' => "Periodo PAC",
                    'value'  =>  "period",
                ],
                [
                    'text' => "Estado",
                    'value'  =>  "state",
                ],
                [
                    'text' => "Valor total de la planilla",
                    'value'  =>  "total_payment",
                ],
                [
                    'text' => "Total planes agregados",
                    'value'  =>  "plans_number",
                ],
                [
                    'text' => "Fecha de ordenación",
                    'value'  =>  "order_date",
                ],
                [
                    'text' => "Nombre del ordenador",
                    'value'  =>  "order_name",
                ],
                [
                    'align' => "right",
                    'label' => "Total planes agregados",
                    'field'  =>  "plans_number",
                    'icon'  =>  'mdi-calendar',
                ],
                [
                    'icon'   => 'mdi-map-marker',
                    'text' => "Fecha de ordenación",
                    'value'  =>  "order_date",
                ],
                [
                    'icon'   => 'mdi-map-marker',
                    'text' => "Nombre del ordenador",
                    'value'  =>  "order_name",
                ],
            ]
            : [
                [
                    'text' => "Nombre de la planilla",
                    'value'  =>  "name",
                ],
                [
                    'align' => "center",
                    'text' => "Periodo PAC",
                    'value'  =>  "period",
                ],
                [
                    'text' => "Estado",
                    'value'  =>  "state",
                ],
                [
                    'text' => "Valor total de la planilla",
                    'value'  =>  "total_payment",
                ],
                [
                    'text' => "Total planes agregados",
                    'value'  =>  "plans_number",
                ],
                [
                    'text' => "Fecha de ordenación",
                    'value'  =>  "order_date",
                ],
                [
                    'text' => "Nombre del ordenador",
                    'value'  =>  "order_name",
                ],
            ];*/
        return [];
    }
}
