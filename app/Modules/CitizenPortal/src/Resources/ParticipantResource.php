<?php


namespace App\Modules\CitizenPortal\src\Resources;


use App\Modules\CitizenPortal\src\Constants\Roles;
use App\Modules\CitizenPortal\src\Models\Activity;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Str;

class ParticipantResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'            => isset($this->id) ? (int) $this->id : null,
            'name'          => toUpper($this->name ?? null),
            'full_name'     => toUpper($this->fullName ?? null),
            'age'          =>  isset($this->profile->birthdate->age) ? (int) $this->profile->birthdate->age : null,
            'document'          => toUpper($this->document ?? null),
            'team_id'   => isset($this->team_id) ? (int) $this->team_id : null,
            'team'   =>  isset($this->team->name) ? (string) $this->team->name :  null,
            'role_id'   => isset($this->role_id) ? (int) $this->role_id : null,
            'role'   => isset($this->role->name) ? (string) $this->role->name : null,
            "accepted_at" => isset($this->accepted_at) ? $this->accepted_at->format('Y-m-d H:i:s') : null,
            'created_at'    => isset($this->created_at) ? $this->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'    => isset($this->updated_at) ? $this->updated_at->format('Y-m-d H:i:s') : null,
        ];
    }
}
