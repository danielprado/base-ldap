<?php

namespace App\Modules\Contractors\src\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserThirdPartyResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'ter_codi'  => $this->ter_codi ?? null,
            'ter_dire'  => $this->ter_dire ?? null,
            'ter_mail'  => $this->ter_mail ?? null,
            'ter_celu'  => $this->ter_celu ?? null,
            'ter_ntel'  => $this->ter_ntel ?? null,
            'ter_acti'  => $this->ter_acti ?? null,
            'ter_acti_text'  => isset($this->ter_acti) ? $this->getStateText($this->ter_acti): null,
        ];
    }

    public static function headers(): array
    {
        return [
            [
                'text' => "Dirección",
                'value'  =>  "ter_dire",
                'icon'  =>  'mdi-routes',
            ],
            [
                'text' => "E-mail",
                'value'  =>  "ter_mail",
                'icon'  =>  'mdi-mail',
            ],
            [
                'text' => "Teléfono",
                'value'  =>  "ter_celu",
                'icon'  =>  'mdi-phone',
            ],
            [
                'text' => "Teléfono 2",
                'value'  =>  "ter_ntel",
                'icon'  =>  'mdi-phone',
            ],
            [
                'text' => "Estado del tercero",
                'value'  =>  "ter_acti_text",
                'icon'  =>  'mdi-account-check',
            ],
        ];
    }

    public function getStateText($state): string
    {
        switch ($state) {
            case('A'):
                $msg = "APROBADO";
                break;
            case('R'):
                $msg = "RECHAZADO";
                break;
            case('I'):
                $msg = "INACTIVO";
                break;
            case('E'):
                $msg = "EN ESTUDIO";
                break;
            default:
                $msg = 'APROBADO';
        }

        return $msg;
    }

}
