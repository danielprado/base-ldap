<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->unsignedBigInteger('subdirectorate_id')->nullable()->after('signature')->comment('Subdirección a la que pertenece');
            $table->unsignedBigInteger('area_id')->nullable()->after('subdirectorate_id')->comment('Dependencia a la que pertenece');
            $table->foreign('subdirectorate_id')
                ->references('id')
                ->on('subdirectorates')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
            $table->foreign('area_id')
                ->references('id')
                ->on('areas')
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign(['subdirectorate_id']);
            $table->dropForeign(['area_id']);
            $table->dropColumn(['subdirectorate_id', 'area_id']);
        });
    }
}
