<?php

namespace App\Console\Commands;

use App\Modules\Contractors\src\Exports\UserSevenExport;
use App\Modules\Contractors\src\Jobs\GenerateReportSecop;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Maatwebsite\Excel\Facades\Excel;

class SecopContractsReport extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'secop:report';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate report contracts api secop';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $contracts_secop = get_all_active_contracts_secop_api();
        $contracts = $contracts_secop->whereNotIn('documento_proveedor', [
            'No Definido', '9007498203', '901382034', '9017356138', '900343856', '800240740', '900364710', '830050698',
            '900529375', '7001001157', '800159771', '800103052', '900466596', '830122983', '830062674']);

        $http = new Client([
            'base_uri' => env('URL_DOCKER_SERVER'),
            'headers' => [
                'Accept' => 'application/json',
                'Content-Type' => 'application/json',
            ],
        ]);

        $response = $http->post("/api/contractors-portal/get-user-seven-list");
        $data = json_decode($response->getBody()->getContents(), true);
        $max = collect($data['data'])->max('counter');

        $contractsSecop = collect($contracts)->map(function ($contract) {
            return [
                'number' => extract_number_format_contract($contract['referencia_del_contrato']),
                'document' => $contract['documento_proveedor']
            ];
        });

        $contractsSeven = collect($data['data'])->map(function ($item) {
            return [
                'contract_number' => $item['contract_number'],
                'identification' => $item['identification']
            ];
        });

        $diff_contracts = $contractsSecop->reject(function ($contract) use ($contractsSeven) {
            return $contractsSeven->contains(function ($sevenContract) use ($contract) {
                return $sevenContract['contract_number'] === $contract['number'] && $sevenContract['identification'] === $contract['document'];
            });
        })->unique(function ($contract) {
            return $contract['number'] . '-' . $contract['document'];
        })->values();

        $new_contracts = collect($contracts)->filter(function ($contract) use ($diff_contracts) {
            return $diff_contracts->contains('document', $contract['documento_proveedor']);
        })->unique('documento_proveedor')->values();

        if ($new_contracts->isNotEmpty()) {
            $file_name = "contratos-" . now()->format('Y-m-d') . ".xlsx";
            Excel::store(new UserSevenExport(collect($new_contracts), $max), "exports/$file_name", 'local', \Maatwebsite\Excel\Excel::XLSX);
            $emails = [
                env('SECOP_REPORT_EMAIL1'),
                env('SECOP_REPORT_EMAIL2'),
                env('SECOP_REPORT_EMAIL3'),
                env('SECOP_REPORT_EMAIL4'),
            ];

            foreach ($emails as $email) {
                if ( isset( $email )  && filter_var( $email, FILTER_VALIDATE_EMAIL) ) {
                    dispatch(new GenerateReportSecop($email, $file_name));
                }
            }

            $this->info("Report contracts generated.");
        } else {
            $this->info("Finished report without contracts news");
        }
    }
}
