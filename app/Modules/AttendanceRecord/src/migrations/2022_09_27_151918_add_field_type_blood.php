<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldTypeBlood extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::connection('mysql_attendance')->table('attendees', (Blueprint $table)) {
            $table->text('type_blood')->nullable()->after('civil_status');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('attendees', function (Blueprint $table) {
            $table->dropColumn('type_blood');
        });
    }
}
