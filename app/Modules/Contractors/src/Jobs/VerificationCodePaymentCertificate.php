<?php


namespace App\Modules\Contractors\src\Jobs;


use App\Modules\Contractors\src\Mail\PaymentCertificateMail;
use App\Modules\Contractors\src\Models\Certification;
use App\Modules\Contractors\src\Models\Contractor;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Mail\Mailer;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use SendGrid\Mail\Mail;

class VerificationCodePaymentCertificate implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * @var Contractor
     */
    private $user;

    /**
     * @var Certification
     */
    private $certification;

    /**
     * Create a new job instance.
     *
     * @param Contractor $user
     * @param Certification $certification
     */
    public function __construct(Contractor $user, Certification $certification)
    {
        $this->user = $user;
        $this->certification = $certification;
        if (!isset($this->certification->code)) {
            $digits = 6;
            $code = rand(pow(10, $digits-1), pow(10, $digits)-1);
            $this->certification->code = $code;
            $this->certification->save();
        }
    }

    /**
     * Execute the job.
     *
     * @param Mailer $mailer
     * @return void
     */
    public function handle(Mail $mailer)
    {
        $email = (config('app.env') == 'local'
            ? env('SAMPLE_EMAIL', 'brayan.aldana@idrd.gov.co')
            : isset($this->user->email)) ? $this->user->email : null;

        if ( isset($email)  && filter_var( $email, FILTER_VALIDATE_EMAIL) ) {
            $mailer->setFrom(
                env('MAIL_CONTRACTS_FROM', 'portal.contratista@idrd.gov.co'),
                env('MAIL_CONTRACTS_FROM_NAME', 'Portal Contratista')
            );
            $mailer->setSubject('Código de Verificación Certificado de Pagos');
            $mailer->addTo($email);
            $render = (new PaymentCertificateMail( $this->user, $this->certification ))->render();
            $mailer->addContent(
                "text/html", $render
            );
            $sendgrid = new \SendGrid(config('mail.sendgrid.api'));
            try {
                $response = $sendgrid->send($mailer);
                sendgird_logger(
                    $email,
                    "Código de Verificación Certificado de Pagos",
                    $render,
                    [],
                    $response->statusCode(),
                    $response->body()
                );
            } catch (\Exception $e) {
                sendgird_logger(
                    $email,
                    "[EXCEPTION] Código de Certificado de Pagos",
                    $render,
                    [],
                    $e->getCode(),
                    $e->getMessage()
                );
            }
        }
    }
}
