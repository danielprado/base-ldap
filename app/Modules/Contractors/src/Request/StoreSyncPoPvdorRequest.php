<?php


namespace App\Modules\Contractors\src\Request;


use App\Models\Security\Area;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreSyncPoPvdorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'emp_codi' =>  'required|numeric',
            'tip_codi' =>  'required|numeric',
            'pvd_coda' =>  'required|numeric',
            'pvd_dive' =>  'required|numeric',
            'pvd_nomb' =>  'required|string',
            'pvd_apel' =>  'required|string',
            'pvd_noco' =>  'required|string',
            'pai_codi' =>  'required|numeric',
            'dep_codi' =>  'required|numeric',
            'mun_codi' =>  'required|numeric',
            'tpr_codi' =>  'required|numeric',
            'pvd_clad' =>  'required|string',
            'arb_csuc' =>  'required|string',
            'cal_codi' =>  'required|numeric',
            'coc_codi' =>  'required|numeric',
            'cim_codi' =>  'required|numeric',
            'act_codi' =>  'required|numeric',
            'mon_codi' =>  'required|numeric',
            'cup_nume' =>  'required|string',
            'cup_idba' =>  'required|string',
            'ban_codi' =>  'required|numeric',
            'sub_codi' =>  'required|numeric',
            'cup_tipo' =>  'required|string',
            'pvr_riva' =>  'required|string',
            'pvr_auto' =>  'required|string',
            'pvr_reau' =>  'required|string',
            'cup_tdba' =>  'required|string',
            'cup_tcta' =>  'required|string',
            'cup_ib04' =>  'required|string',
            'cup_ib05' =>  'required|string',
            'cup_ib06' =>  'required|string',
            'cup_ib07' =>  'required|string',
            'cup_ib08' =>  'required|string',
            'cup_ib09' =>  'required|string',
            'cup_ib10' =>  'required|string',
            'dep_dire' =>  'required|string',
            'dep_ntel' =>  'required|string',
            'dep_mail' =>  'required|string|email',
            'dep_codd' =>  'required|numeric',
            'cup_numi' =>  'nullable|string',
            'cup_nomi' =>  'nullable|string',
            'ban_coin' =>  'required|numeric',
            'ban_dire' =>  'nullable|string',
            'cup_coab' =>  'nullable|string',
            'cup_coib' =>  'nullable|string',
            'cup_coas' =>  'nullable|string',
            'cup_cobi' =>  'nullable|string',
            'cup_clas' =>  'required|string',
            'dep_apar' =>  'nullable|string',
            'dep_nfax' =>  'nullable|string',
            'dep_cloc' =>  'nullable|string',
            'dep_nomb' =>  'required|string',
            'cup_esta' =>  'required|string',
            'pvd_audp' =>  'required|string',
            'pvd_moin' =>  'required|string',
            'pvd_mrpu' =>  'required|string',
            'pvd_serp' =>  'required|string',
            'ite_codi_cope' =>  'required|numeric',
            // Data
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'emp_codi' =>  'Código de la empresa a la cual pertenece el proveedor',
            'tip_codi' =>  'Código del tipo de documento de identificación del proveedor',
            'pvd_coda' =>  'Número de documento de identificación del proveedor',
            'pvd_dive' =>  'Dígito de verficación',
            'pvd_nomb' =>  'Nombre del proveedor',
            'pvd_apel' =>  'Apellidos del proveedor',
            'pvd_noco' =>  'Nombre comercial del proveedor',
            'pai_codi' =>  'Código del país a que pertenece el proveedor',
            'dep_codi' =>  'Código del departamento a que pertenece el proveedor',
            'mun_codi' =>  'Código del municipio a que pertenece el proveedor',
            'tpr_codi' =>  'Tipo de Proveedor',
            'pvd_clad' =>  'Código de la clasificación DIAN',
            'arb_csuc' =>  'Código de la sucursal del proveedor',
            'cal_codi' =>  'Código de calificación del proveedor',
            'coc_codi' =>  'Código de condición comercial',
            'cim_codi' =>  'Código de la configuración de impuestos',
            'act_codi' =>  'Código de la actividad económica',
            'mon_codi' =>  'Código de la moneda',
            'cup_nume' =>  'Número de cuenta bancaria para pago en consignación',
            'cup_idba' =>  'Código identificador bancario',
            'ban_codi' =>  'Código de banco',
            'sub_codi' =>  'Código de sucursal bancaria',
            'cup_tipo' =>  'Tipo de Cuenta Bancaria',
            'pvr_riva' =>  'Régimen IVA',
            'pvr_auto' =>  'Autorretenedor',
            'pvr_reau' =>  'Resolución autorretenedora',
            'cup_tdba' =>  'Tipo de documento bancario',
            'cup_tcta' =>  'Otros identificadores',
            'cup_ib04' =>  'Identificador bancario 4',
            'cup_ib05' =>  'Identificador bancario 5',
            'cup_ib06' =>  'Identificador bancario 6',
            'cup_ib07' =>  'Identificador bancario 7',
            'cup_ib08' =>  'Identificador bancario 8',
            'cup_ib09' =>  'Identificador bancario 9',
            'cup_ib10' =>  'Identificador bancario 10',
            'dep_dire' =>  'Dirección, detalle 1 proveedor',
            'dep_ntel' =>  'Número de teléfono detalle 1 proveedor',
            'dep_mail' =>  'Correo electrónico detalle 1 proveedor',
            'dep_codd' =>  'Código, detalle de proveedor',
            'cup_numi' =>  'Número cuenta bancaria - datos internacionales',
            'cup_nomi' =>  'Nombre cuenta bancaria – datos internacionales',
            'ban_coin' =>  'Código banco intermediario',
            'ban_dire' =>  'Dirección - datos internacionales',
            'cup_coab' =>  'Código Aba/Swift – datos internacionales',
            'cup_coib' =>  'Código Aba/Swift – indicadores bancarios',
            'cup_coas' =>  'Código Iban – indicadores bancarios',
            'cup_cobi' =>  'Código Iban – datos internacionales.',
            'cup_clas' =>  'cuenta nacional.',
            'dep_apar' =>  'Apartado aéreo',
            'dep_nfax' =>  'Número de fax',
            'dep_cloc' =>  'Código de localización',
            'dep_nomb' =>  'Nombre completo detalle de proveedor',
            'cup_esta' =>  'Estado cuenta bancaria del proveedor',
            'pvd_audp' =>  'Indica si autoriza el tratamiento de datos personales',
            'pvd_moin' =>  'Realiza Operaciones Internacionales',
            'pvd_mrpu' =>  'Maneja Recursos Públicos',
            'pvd_serp' =>  'Servidor Público',
            'ite_codi_cope' =>  'Clase de Operación',
        ];
    }
}
