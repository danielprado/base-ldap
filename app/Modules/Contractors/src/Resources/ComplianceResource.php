<?php

namespace App\Modules\Contractors\src\Resources;

use App\Modules\Contractors\src\Constants\Contributions;
use App\Modules\Contractors\src\Models\Compliance;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Crypt;

class ComplianceResource extends JsonResource
{

    public $number;
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {

        $contract = extract_number_and_year_from_contract($this->contract_number);
        $number = $contract['contract_number'] ?? null;
        $this->number = $number;
        $year = $contract['contract_year'] ?? null;

        return [
            'id'                    => $this->id,
            'contractor_id'         => $this->contractor_id,
            'contract_id'           => $this->contract_id,
            'document_type_id'      => $this->document_type_id,
            'period_month'          => $this->period_month,
            'period_year'           => $this->period_year,
            'document'              => $this->document,
            'name'                  => $this->name,
            'surname'               => $this->surname,
            'contract_type_id'      => $this->contract_type_id,
            'contract'              => $this->contract_number,
            'contract_number'       => $number,
            'contract_year'         => $year,
            'status'                => $this->status,
            'afc_value'             => $this->isRejected() ? intval($this->afc_value) : null,
            'afp_voluntary'         => $this->isRejected() ? $this->afp_voluntary : false,
            'afp_voluntary_value'   => $this->isRejected() ? intval($this->afp_voluntary_value) : null,
            'afc_voluntary'         => $this->isRejected() ? $this->afc_voluntary : false,
            'payload'               => $this->isRejected() ? $this->getPayload() : null,
            'modifiable'            => $this->modifiable ?? null,

            // files
            'afp_voluntary_file'    => $this->showFile($this->afp_voluntary_file),
            'afc_voluntary_file'    => $this->showFile($this->afc_voluntary_file),
            'reviewed'              => isset($this->reviewed) ? (bool)$this->reviewed : null,

            $this->merge($this->showFirstTimeField('subdirectorate_id')),
            $this->merge($this->showFirstTimeField('area_id')),
            $this->merge($this->showContribution(Contributions::PENSIONER, false)),
            $this->merge($this->showContributionFile(Contributions::PENSIONER, 'pensioner_file')),
            $this->merge($this->showContribution(Contributions::HAS_CHILDREN, false)),
            $this->merge($this->showContributionDependent('children_number', Contributions::HAS_CHILDREN, 0)),
            $this->merge($this->showContribution(Contributions::CHILDREN_MINOR, false)),
            $this->merge($this->showContributionFile(Contributions::CHILDREN_MINOR, 'children_minor_files')),
            $this->merge($this->showContribution(Contributions::CHILDREN_MAYOR_STUDENTS, false)),
            $this->merge($this->showContributionFile(Contributions::CHILDREN_MAYOR_STUDENTS, 'children_mayor_students_files')),
            $this->merge($this->showContribution(Contributions::CHILDREN_DISABILITIES, false)),
            $this->merge($this->showContributionFile(Contributions::CHILDREN_DISABILITIES, 'children_disabilities_files')),
            $this->merge($this->showContribution(Contributions::SPOUSE, false)),
            $this->merge($this->showContributionFile(Contributions::SPOUSE, 'spouse_files')),
            $this->merge($this->showContribution(Contributions::PARENTS_DEPENDENCY, false)),
            $this->merge($this->showContributionFile(Contributions::PARENTS_DEPENDENCY, 'parents_dependency_files')),
            $this->merge($this->showContribution(Contributions::INCOME_TAX_FILER, false)),
            $this->merge($this->showContributionDependent('withholding_value', Contributions::WITHHOLDING, 0)),
            //$this->merge($this->showContribution(Contributions::AFC_VOLUNTARY, false)),
            //$this->merge($this->showContributionFile(Contributions::AFC_VOLUNTARY, 'afc_voluntary_file')),
            //$this->merge($this->showContribution(Contributions::AFC_VALUE)),
            //$this->merge($this->showContribution(Contributions::AFP_VOLUNTARY, false)),
            //$this->merge($this->showContributionFile(Contributions::AFP_VOLUNTARY, 'afp_voluntary_file')),
            //$this->merge($this->showContribution(Contributions::AFP_VOLUNTARY_VALUE, 0)),
            $this->merge($this->showContribution(Contributions::HOUSING_INTERESTS, false)),
            $this->merge($this->showContributionFile(Contributions::HOUSING_INTERESTS, 'housing_interests_files')),
            $this->merge($this->showContribution(Contributions::PREPAID_MEDICINE, false)),
            $this->merge($this->showContributionFile(Contributions::PREPAID_MEDICINE, 'prepaid_medicine_file')),
            $this->merge($this->showContribution(Contributions::SPOUSE_TAX, false)),
            $this->merge($this->showContribution(Contributions::THIRD_PEOPLE, false)),
            $this->merge($this->showContribution(Contributions::RESPONSIBLE_IVA, false)),
            $this->merge($this->showContribution(Contributions::RETENTION, false)),
            $this->merge($this->showContribution(Contributions::WITHHOLDING, false)),
            $this->merge($this->showContribution(Contributions::PERCENTAGE, 0)),
        ];
    }

    private function showContribution($name, $default = null, $remember = true)
    {
        if ($this->isFirstTime() && !$this->isRejected()) {
            return collect([ $name => $default ]);
        }

        $contribution = $this->contribution($name);
        if (!$contribution->isValid() && !$this->isFirstTime()) return collect([]);

        return collect(
            $remember || $this->isRejected() ? [ $name => $this->$name ] : []
        );
    }

    private function showContributionDependent($name, $parent, $default = null, $remember = true)
    {
        if ($this->isFirstTime() && !$this->isRejected()) {
            return collect([ $name => $default ]);
        }

        $contribution = $this->contribution($parent);
        if (!$contribution->isValid() && !$this->isFirstTime()) return collect([]);

        return collect(
            $remember || $this->isRejected() ? [ $name => $this->$name ] : []
        );
    }

    private function showContributionFile($name, $label = null, $remember = true)
    {
        if ($this->isFirstTime() && !$this->isRejected()) {
            return collect([ $label ?? $name => null ]);
        }

        $contribution = $this->contribution($name);
        if (!$contribution->isValid() && !$this->isFirstTime()) return collect([]);

        $file = $contribution->getFile($name);

        return collect(
            $remember || $this->isRejected() ? [ $label ?? $name => $file ? $file->asUrl() : null ] : []
        );
    }

    private function showFirstTimeField(string $name)
    {
        return $this->isFirstTime()
            ? collect([ $name => $this->$name ])
            : collect([]);
    }

    private function showFile($file) {
        return $file && $this->isRejected() ? $file->asUrl() : null;
    }

    public function getPayload()
    {
        $contract = extract_number_and_year_from_contract($this->contract_number);
        $payload = Crypt::encrypt(json_encode([
            'document' => $this->document,
            'contract_year' => $contract['contract_year'],
            'contract_number' => $contract['contract_number'],
            'period_month' => $this->period_month,
            'period_year' => $this->period_year
        ]));

        return $payload ?? null;
    }
}
