<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateContractorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('contractors', function (Blueprint $table) {
            $table->integer('blood_type_id')->after('address')->nullable()->comment('Tipo de sangre');
            $table->string('contact_name', 191)->after('blood_type_id')->nullable()->comment('Nombre del contacto de emergencia');
            $table->string('contact_phone', 20)->after('contact_name')->nullable()->comment('Teléfono del contacto de emergencia');
            $table->string('contact_relationship', 191)->after('contact_phone')->nullable()->comment('Parentesco del contacto de emergencia');
            $table->text('photo')->after('contact_relationship')->nullable()->comment('Foto');

            $sim_database = env('DB_SIM_DATABASE');
            $table->foreign('blood_type_id')
                ->references('Id_GrupoSanguineo')
                ->on("{$sim_database}.grupo_sanguineo")
                ->onDelete('CASCADE')
                ->onUpdate('CASCADE');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('contractors', function (Blueprint $table) {
            $table->dropColumn([
                'blood_type_id',
                'contact_name',
                'contact_phone',
                'contact_relationship',
                'photo'
            ]);
            $table->dropForeign('blood_type_id_foreign');
        });
    }
}
