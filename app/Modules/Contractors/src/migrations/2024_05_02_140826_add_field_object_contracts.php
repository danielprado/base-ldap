<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldObjectContracts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection('mysql_contractors')->table('contracts', function (Blueprint $table) {
            $table->text('object_contract')->nullable()->before('lawyer_id');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection('mysql_contractors')->table('contracts', function (Blueprint $table) {
            $table->dropColumn('object_contract');

        });
    }
}
