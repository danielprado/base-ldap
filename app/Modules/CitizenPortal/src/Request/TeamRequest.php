<?php


namespace App\Modules\CitizenPortal\src\Request;



use App\Modules\CitizenPortal\src\Constants\Roles;
use App\Modules\CitizenPortal\src\Models\Activity;
use App\Modules\CitizenPortal\src\Models\Modality;
use App\Modules\CitizenPortal\src\Models\Schedule;
use Illuminate\Foundation\Http\FormRequest;

class TeamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /*
        $permission = toLower($this->getMethod()) == 'post'
            ? Roles::can(Activity::class,'create_or_manage', true)
            : Roles::can(Activity::class,'update_or_manage', true);
        */
        return auth('api')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $modality = new Modality();
        $activity = new Schedule();
        return [
            'name' => 'required|max:191|min:3',
            'modality_id' => "required|numeric|exists:{$modality->getConnectionName()}.{$modality->getTable()},{$modality->getKeyName()}",
            'activity_id' => "required|numeric|exists:{$activity->getConnectionName()}.{$activity->getTable()},{$activity->getKeyName()}"
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'name' =>  __('citizen.validations.name'),
        ];
    }
}
