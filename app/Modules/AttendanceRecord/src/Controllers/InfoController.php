<?php

namespace App\Modules\AttendanceRecord\src\Controllers;

use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use App\Modules\AttendanceRecord\src\Models\City;
use App\Modules\AttendanceRecord\src\Models\Country;
use App\Modules\AttendanceRecord\src\Models\Eps;
use App\Modules\AttendanceRecord\src\Models\Locality;
use App\Modules\AttendanceRecord\src\Models\State;
use App\Modules\AttendanceRecord\src\Models\Upz;
use Illuminate\Http\Request;

/**
 * @group Pasarela de pagos - Parques
 *
 * API para la gestión y consulta de datos de Parques Pse
 */
class InfoController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * @group registro asistencia
     *
     * Parques
     *
     * Muestra un listado del recurso.
     *
     *
     * @return JsonResponse
     */
    public function geteps()
    {
          $eps = Eps::where('active', '1')->orderBy('name','ASC')->get(['id', 'name']);
          return response()->json([
                'status' => 'OK',
                'message' => 'Eps obtenidas satisfactoriamente',
                'data' => $eps,
                'Error' => false,
          ], 200);
    }

    public function getcountries()
    {
          $countries = Country::orderBy('name','ASC')->get(['id', 'name']);
          return response()->json([
                'status' => 'OK',
                'message' => 'Paises obtenidos satisfactoriamente',
                'data' => $countries,
                'Error' => false,
          ], 200);
    }

    public function getstates(Request $request)
    {
          $states = State::where('country_id', $request->country_id)->get(['id', 'name']);
          return response()->json([
                'status' => 'OK',
                'message' => 'Departamentos obtenidos satisfactoriamente',
                'data' => $states,
                'Error' => false,
          ], 200);
    }

    public function getcities(Request $request)
    {
          $cities = City::where('state_id', $request->state_id)->get(['id', 'name']);
          return response()->json([
                'status' => 'OK',
                'message' => 'Ciudades obtenidos satisfactoriamente',
                'data' => $cities,
                'Error' => false,
          ], 200);
    }

    public function getlocalities()
    {
          $localities = Locality::get();
          return response()->json([
                'status' => 'OK',
                'message' => 'localidades obtenidos satisfactoriamente',
                'data' => $localities,
                'Error' => false,
          ], 200);
    }

    public function getupz(Request $request)
    {
          $upz = Upz::where('IdLocalidad', $request->locality_id)->get();
          return response()->json([
                'status' => 'OK',
                'message' => 'Upz obtenidos satisfactoriamente',
                'data' => $upz,
                'Error' => false,
          ], 200);
    }

}
