<?php

namespace App\Modules\CitizenPortal\src\Notifications;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use SendGrid\Mail\Mail;
use SendGrid\Mail\To;

class SendgridTemplates
{

    const EMAIL_SENT_OK = 202;

    const SUBSCRIPTION_TEMPLATE = "d-d65c4b7933724d7a80674df521ee64df";
    const NOTIFICATION_TEMPLATE = "d-f6e82457e5f14520a08846a4557767a9";
    const DELETE_ACCOUNT_TEMPLATE = "d-3721ef5dafe94ffa96d725051260179b";
    const SPORT_CLUBS_TEMPLATE = "d-a1669232a461435dac420fa15bd47aa5";
    const VERIFICATION_CODE_TEMPLATE = "d-f013f5efc27a475b8e501824320f19a3";
    const NOTIFICATION_COLORS = [
        "info" => [
            "color" => "#648fec",
            "soft_color" => "#dee5ff",
        ],
        "warning" => [
            "color" => "#eccc64",
            "soft_color" => "#fff6de",
        ],
        "error" => [
            "color" => "#ec6d64",
            "soft_color" => "#ffe0de",
        ],
        "success" => [
            "color" => "#9eb23b",
            "soft_color" => "#fcf9c6",
        ],
    ];
    /**
     * @var false
     */
    private $setSubscriptionTemplate;

    public function __construct($setSubscriptionTemplate = false)
    {
        $this->setSubscriptionTemplate = $setSubscriptionTemplate;
    }

    /**
     * @param array|string $toEmails
     * @param string $templateId
     * @param string|null $subject
     * @param array $templateData
     * @return bool
     */
    private function send($toEmails, string $templateId, string $subject = null, array $templateData = [])
    {
        $tos = is_array($toEmails) ? $toEmails : [["email" =>  $toEmails]];
        $tos = collect($tos)->filter(function ($email) {
            return filter_var(Arr::get($email, "email"), FILTER_VALIDATE_EMAIL);
        })->unique("email")->values()->toArray();
        $emails = collect($tos)->implode("email", ", ");
        $now = now()->format("Y-m-d H:i:s");
        if (count($tos) < 1) {
            Log::error(
                "[SENDGRID] [CODE: 400] [EMAILS: $emails] ERROR: La cantidad de correos es 0"
            );
            return "[$now] - [SENDGRID] [CODE: 400] [EMAILS: $emails] ERROR: La cantidad de correos es 0";
        }
        try {
            $mailer = new Mail();
            $mailer->addTos(
                collect($tos)->map(function ($to) {
                    return new To(
                        Arr::get($to, "email"),
                        Arr::get($to, "name")
                    );
                })->toArray()
            );
            $mailer->setFrom(
                "portal.ciudadano@idrd.gov.co",
                "Portal Ciudadano"
            );
            if ($subject) {
                $mailer->setSubject("$subject");
            }
            $mailer->setTemplateId("$templateId");
            if (count($templateData) > 0) {
                $mailer->addDynamicTemplateDatas($templateData);
            }
            $sendgrid = new \SendGrid(config("mail.sendgrid.api"));
            $response = $sendgrid->send($mailer);
            $status = $response->statusCode();
            if ($status != SendgridTemplates::EMAIL_SENT_OK) {
                sendgird_logger(
                    $emails,
                    $subject,
                    $templateId." [".json_encode($templateData)."]",
                    "",
                    $response->statusCode(),
                    $response->body()
                );
                return false;
            }
            return true;
        } catch (\Exception $e) {
            sendgird_logger(
                $emails,
                "[EXCEPTION] {$subject}",
                $templateId." [".json_encode($templateData)."]",
                "",
                500,
                $e->getMessage()
            );
            return false;
        }
    }

    /**
     * @param $email
     * @param string $subject
     * @return bool
     */
    public function sendDeleteAccountMessage($email, string $subject = "Eliminación de cuenta")
    {
        return $this->send($email, SendgridTemplates::DELETE_ACCOUNT_TEMPLATE, $subject);
    }

    /**
     * @param $email
     * @param $code
     * @param string $subject
     * @return bool
     */
    public function sendVerificationCodeMessage($email, $code, string $subject = "Verificación Correo electrónico")
    {
        return $this->send(
            $email,
            SendgridTemplates::VERIFICATION_CODE_TEMPLATE,
            $subject,
            [
                "code" => $code,
                "email" => $email,
            ]
        );
    }

    /**
     * @param string $email
     * @param array $params
     * $params = [
     *  'club'      =>  string,
     *  'email'     =>  string,
     *  'president' =>  string,
     *  'phone'     =>  string,
     *  'request_type'  =>  string,
     *  'request_type_description'  =>  string|null,
     *  'club_type'     =>  string,
     *  'accept_media'     =>  string,
     * ]
     * @param string $subject
     * @return bool
     */
    public function sendClubesMessage(string $email, array $params, string $subject)
    {
        return $this->send(
            $email,
            SendgridTemplates::SPORT_CLUBS_TEMPLATE,
            $subject,
            $params
        );
    }

    /**
     * @param $email
     * @param array $params
     * $params = [
     *  'color'      =>  string,
     *  'soft_color'     =>  string,
     *  'name' =>  string,
     *  'sub_user' =>  string,
     *  'status'     =>  string,
     *  'observation'  =>  string,
     *  'info'  =>  string,
     * ]
     * @param string $subject
     * @param string["info", "success", "warning", "error"] $type
     * @param bool $customColor
     * @return bool|string
     */
    public function sendNotificationMessage($email, array $params, string $subject, string $type = "info", $customColor = false)
    {
        $colors = $customColor
            ? []
            : Arr::get(SendgridTemplates::NOTIFICATION_COLORS, $type, [
                "color" => "#648fec",
                "soft_color" => "#dee5ff",
            ]);
        return $this->send(
            $email,
            $this->setSubscriptionTemplate ? SendgridTemplates::SUBSCRIPTION_TEMPLATE : SendgridTemplates::NOTIFICATION_TEMPLATE,
            $subject,
            array_merge(
                $params,
                $colors,
                [
                    "subject" => $subject
                ]
            )
        );
    }

    public function sendWarningMessage($email, array $params, string $subject)
    {
        return $this->sendNotificationMessage($email, $params, $subject, "warning");
    }

    public function sendSuccessMessage($email, array $params, string $subject)
    {
        return $this->sendNotificationMessage($email, $params, $subject, "success");
    }

    public function sendErrorMessage($email, array $params, string $subject)
    {
        return $this->sendNotificationMessage($email, $params, $subject, "error");
    }

    public function sendInfoMessage($email, array $params, string $subject)
    {
        return $this->sendNotificationMessage($email, $params, $subject);
    }

    /**
     * @param $email
     * @param array $params
     * @param string $subject
     * @param string $color Hexadecimal color
     * @param string $soft_color Hexadecimal color
     * @return bool|string
     */
    public function sendCustomMessage($email, array $params, string $subject, string $color, string $soft_color)
    {
        $newParams = array_merge(
            $params,
            [
                "color" => "$color",
                "soft_color" => "$soft_color",
            ]
        );
        return $this->sendNotificationMessage($email, $newParams, $subject, "none", $customColor = true);
    }
}
