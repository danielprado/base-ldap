<?php

namespace App\Modules\Parks\src\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NamesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'idType' => $this['idType'],
            'nameType' => $this['nameType'],
            'category' => $this['category'],
            'name' => $this['name'],
            'id' => $this['id'],
        ];
    }
}
