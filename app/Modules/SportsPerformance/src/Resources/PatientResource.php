<?php

namespace App\Modules\SportsPerformance\src\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Modules\SportsPerformance\src\Models\Athlete;
use App\Modules\SportsPerformance\src\Models\ParalympicAthlete;
use App\Modules\SportsPerformance\src\Models\ModalityClassification;
use App\Modules\SportsPerformance\src\Models\PopulationCharacterizationPeople;

class PatientResource extends JsonResource
{
    protected $person;
    protected $athlete;
    protected $characterization;
    protected $modality_classification;

    public function __construct($resource, $person)
    {
        parent::__construct($resource);
        $this->person = $person;
        $this->athlete = Athlete::where('Persona_Id', $this->persona_id)->first();
        $this->characterization = PopulationCharacterizationPeople::with(['sex','gender_identity','sexual_orientation'])
                                    ->where('persona_id', $this->persona_id)->first();
        $paralympic = ParalympicAthlete::where('Deportista_Id', $this->athlete->Id)->first();
        if (isset($paralympic)){
            $this->modality_classification = ModalityClassification::with(['disability', 'modality'])
                                             ->where('Id_Deporte', $this->athlete->Id_Deporte)->where('Id_Discapacidad', $paralympic->Discapacidad_Id)->latest('Id')->first();   
        }
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'uid'                     => $this->id,
            'first_name'              => $this->person->Primer_Nombre,
            'middle_name'             => $this->person->Segundo_Nombre,
            'last_name'               => $this->person->Primer_Apellido,
            'second_last_name'        => $this->person->Segundo_Apellido,
            'sex'                     => optional(optional($this->characterization)->sex)->nombre,
            'gender_identity'         => optional(optional($this->characterization)->gender_identity)->nombre,
            'sexual_orientation'      => optional(optional($this->characterization)->sexual_orientation)->nombre,
            'sector'                  => optional($this->athlete->classification)->Nombre_Clasificacion_Deportista,
            'sport'                   => optional($this->athlete->sport)->V_NOMBRE_DEPORTE,
            'modality'                => optional(optional($this->modality_classification)->modality)->Nombre,
            'limitation_type'         => optional(optional($this->modality_classification)->disability)->Nombre_Discapacidad,
            'birth_date'              => $this->person->Fecha_Nacimiento,
            'birth_municipality'      => optional($this->athlete->birth_city)->nombre,
            'birth_department'        => optional($this->athlete->birth_department)->Nombre_Departamento,
            'birth_country'           => 'Colombia',
            'document_type'           => optional($this->person->document_type)->Descripcion_TipoDocumento,
            'document'                => $this->person->Cedula,
            'expedition_municipality' => optional($this->expedition_city)->nombre,
            'expedition_department'   => optional($this->expedition_department)->Nombre_Departamento,
            'address'                 => $this->athlete->Direccion_Localiza,
            'neighborhood'            => $this->athlete->Barrio_Localiza,
            'locality'                => optional($this->athlete->locality)->Nombre_Localidad,
            'landline'                => $this->athlete->Fijo_Localiza,
            'mobile'                  => $this->athlete->Celular_Localiza
        ];
    }
}
