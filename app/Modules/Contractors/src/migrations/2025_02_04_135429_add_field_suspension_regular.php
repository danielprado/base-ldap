<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddFieldSuspensionRegular extends Migration
{

    protected $connection = 'mysql_contractors';
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::connection($this->connection)->table('contracts', function (Blueprint $table) {
            $table->tinyInteger('suspension_regular')->nullable()->after('consecutive');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::connection($this->connection)->table('contracts', function (Blueprint $table) {
            $table->dropColumn('suspension_regular');
        });
    }
}
