<?php

namespace App\Modules\Contractors\src\Exports;

use App\Models\Security\Subdirectorate;
use App\Models\Security\User;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Imtigger\LaravelJobStatus\JobStatus;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class BinnacleExport implements FromCollection, WithMapping, WithHeadings, WithColumnFormatting, ShouldAutoSize, WithEvents, WithTitle
{
    use Exportable;

    /**
     * @var int
     */
    private $counter;

    /**
     * @var Collection
     */
    private $payroll;

    /**
     * @var Subdirectorate|null
     */
    private $subdirectorate;

    public function __construct(Collection $payroll, ?Subdirectorate $subdirectorate)
    {
        $this->payroll = $payroll;
        $this->subdirectorate = $subdirectorate;
    }

    public function collection()
    {
        $data = collect($this->payroll['fullPaymentPlanList']);
        $expandedData = [];
        $contractorCounter = 1;
        $currentContractor = null;
        foreach ($data as $item) {
            foreach ($item['pmrList'] as $pmr) {
                if ($currentContractor !== $item['contractorDocument']) {
                    $currentContractor = $item['contractorDocument'];
                    $counter = $contractorCounter++;
                }
                $expandedData[] = array_merge($item, $pmr, ['counter' => $counter]);
            }
        }

        return collect($expandedData);
    }

    public function headings(): array
    {
        return [
            'No.',
            'Nº BITÁCORA',
            'FUENTE FINANCIAMIENTO',
            'FECHA',
            'NOMBRE CONTRATISTA',
            'DOCUMENTO CONTRATISTA',
            'NO. DE CONTRATO',
            'AÑO DEL CONTRATO',
            'OBJETO DEL CONTRATO',
            'VALOR DE PAGO',
        ];
    }

    public function map($row): array
    {
        return [
            $row['counter'],
            $row['binnacleBirNumo'],
            $row['fundingSource'],
            date('d/m/Y', strtotime($this->payroll['createdAt'])),
            $row['contractorName'],
            $row['contractorDocument'],
            $row['contractNumber'],
            date('Y', strtotime($row['contractStartDate'])),
            $row['contractObject'],
            isset($row['planTotalPayment']) ? (string) $row['planTotalPayment'] : '0',
        ];
    }

    public function columnFormats(): array
    {
        return [
            'J' => NumberFormat::FORMAT_CURRENCY_USD
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $sheet = $event->sheet->getDelegate();
                $spreadsheet = $event->sheet->getParent();

                // Agregar el autor al archivo
                $spreadsheet->getProperties()
                    ->setCreator('Brayan Sebastian Aldana Casas');

                // Proteger el archivo para que sea de solo lectura
                $sheet->getProtection()->setSheet(true);
                $sheet->getProtection()->setSort(true);
                $sheet->getProtection()->setInsertRows(true);
                $sheet->getProtection()->setFormatCells(true);
                $sheet->getProtection()->setPassword('');

                // Insertar filas vacías para ajustar el encabezado
                $sheet->insertNewRowBefore(1, 8);

                // Insertar imagen en el encabezado
                $drawing = new Drawing();
                $drawing->setName('Header Image');
                $drawing->setDescription('Header Image');
                $drawing->setPath(public_path('images/img.png')); // Ruta a tu imagen
                $drawing->setHeight(50);
                $drawing->setCoordinates('B3');
                $drawing->setWorksheet($sheet);

                // Combinar celdas para el encabezado de la imagen
                $sheet->mergeCells('A1:J5');

                $sheet->setCellValue('A6', "CONSECUTIVO PLANILLA: " . $this->payroll['name']);
                $sheet->mergeCells('A6:J6');
                $sheet->getStyle('A6')->getFont()->setBold(true);

                $sheet->setCellValue('A7', "FECHA ELABORACIÓN: " . Carbon::parse($this->payroll['createdAt'])->format('Y-m-d'));
                $sheet->mergeCells('A7:J7');
                $sheet->getStyle('A7')->getFont()->setBold(true);

                $periodPac = Carbon::parse($this->payroll['createdAt'])->format('Y-m');
                $sheet->setCellValue('A8', "PERIODO PAC: " . $periodPac);
                $sheet->mergeCells('A8:J8');
                $sheet->getStyle('A8')->getFont()->setBold(true);

                // Encabezado en negrita y centrado
                $sheet->getStyle('A9:J9')->getFont()->setBold(true);
                $sheet->getStyle('A9:J9')->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                $sheet->getStyle('A9:J9')->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                $sheet->getStyle('A9:J9')->getAlignment()->setWrapText(true);

                // Ajuste de ancho de columna automático
                foreach (range('A', 'J') as $columnID) {
                    $sheet->getColumnDimension($columnID)->setAutoSize(true);
                }

                $sheet->getColumnDimension('I')->setAutoSize(false)->setWidth(20);

                $columnWidths = [
                    'A' => 4,
                    'B' => 12,
                    'C' => 12,
                    'D' => 12,
                    'E' => 25,
                    'F' => 15,
                    'G' => 12,
                    'H' => 12,
                    'I' => 20,
                    'J' => 12,
                ];

                foreach ($columnWidths as $columnID => $width) {
                    $sheet->getColumnDimension($columnID)->setWidth($width);
                }

                // Columnas a combinar y centrar
                $columnsToMerge = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J'];

                // Combinación y centrado de celdas
                $rows = $sheet->toArray();
                $startRow = 10;
                $highestRow = count($rows);

                foreach ($columnsToMerge as $column) {
                    $previousCounter = null;
                    $previousValue = null;
                    $mergeStartRow = $startRow;

                    for ($i = $startRow; $i <= $highestRow; $i++) {
                        $currentCounter = $rows[$i - 1][0]; // Ajustar para la nueva fila de inicio
                        $currentValue = $rows[$i - 1][ord($column) - 65]; // 65 es el valor ASCII de 'A'

                        if ($currentCounter !== $previousCounter || $currentValue !== $previousValue) {
                            if ($previousValue !== null && $mergeStartRow < $i - 1) {
                                $mergeEndRow = $i - 1;
                                $sheet->mergeCells("$column{$mergeStartRow}:$column{$mergeEndRow}");
                            }
                            // Aplicar alineación a todas las celdas, incluso si solo una fila está presente
                            $sheet->getStyle("$column{$mergeStartRow}:$column{$i}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                            $sheet->getStyle("$column{$mergeStartRow}:$column{$i}")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                            $sheet->getStyle("$column{$mergeStartRow}:$column{$i}")->getAlignment()->setWrapText(true);
                            $mergeStartRow = $i;
                        }

                        if ($currentCounter !== $previousCounter) {
                            $previousCounter = $currentCounter;
                        }
                        $previousValue = $currentValue;
                    }

                    // Último grupo de celdas
                    if ($mergeStartRow <= $highestRow) {
                        $mergeEndRow = $highestRow;
                        $sheet->mergeCells("$column{$mergeStartRow}:$column{$mergeEndRow}");
                        $sheet->getStyle("$column{$mergeStartRow}:$column{$mergeEndRow}")->getAlignment()->setHorizontal(Alignment::HORIZONTAL_CENTER);
                        $sheet->getStyle("$column{$mergeStartRow}:$column{$mergeEndRow}")->getAlignment()->setVertical(Alignment::VERTICAL_CENTER);
                        $sheet->getStyle("$column{$mergeStartRow}:$column{$mergeEndRow}")->getAlignment()->setWrapText(true);
                    }
                }

                // Aplicar bordes a todas las celdas
                $highestColumn = $sheet->getHighestColumn();
                $sheet->getStyle("A9:{$highestColumn}{$highestRow}")
                    ->getBorders()->getAllBorders()->setBorderStyle(Border::BORDER_THIN);
            }
        ];
    }

    public function title(): string
    {
        return "REPORTE";
    }
}
