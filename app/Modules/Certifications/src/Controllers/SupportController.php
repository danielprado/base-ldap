<?php

namespace App\Modules\Certifications\src\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Certifications\src\Jobs\ReplySupport;
use App\Modules\Certifications\src\Models\CertificationContract;
use App\Modules\Certifications\src\Models\ContractSupport;
use App\Modules\Certifications\src\Request\SupportRequest;
use App\Modules\Certifications\src\Resources\SupportResource;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use GuzzleHttp\Client;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Throwable;

class SupportController extends Controller
{
    /**
     * Initialise common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    public function stats(): JsonResponse
    {
        $supports = ContractSupport::query()
            ->selectRaw("
                count(id) as total,
                date_format(created_at, '%b %Y') as period")
            ->where('Estado', '=', 1)
            ->whereYear('created_at', now()->year)
            ->groupBy((array)'period')
            ->orderBy('created_at')
            ->get()
            ->keyBy('period');

        $periods = collect([]);
        foreach (CarbonPeriod::create(Carbon::now()->startOfYear(), '1 month', now()) as $period) {
            $periods->push($period->format('M Y'));
        }

        $totals = $periods->map(function ($period) use ($supports) {
            return $supports->get($period)->total ?? '0';
        });

        return $this->success_message([
            'processed' => ContractSupport::ProcessedByYear(),
            'unprocessed' => ContractSupport::UnProcessedByYear(),
            'totals' => $totals->toArray(),
            'periods' => $periods->toArray()
        ]);
    }

    public function index(Request $request): JsonResponse
    {
        $supports = ContractSupport::query()
            ->when($request->has('solved'), function ($q) {
                return $q->where('Estado', 2)
                    ->orderBy('created_at', 'desc');
            })
            ->when(!$request->has('solved'), function ($q) {
                return $q->where('Estado', 1);
            })
            ->when($request->has('query'), function ($q) use ($request) {
                $data = toLower($request->get('query'));
                return $q->where('Documento_Solicitante', 'like', "%$data%")
                    ->orWhere('Nombre_Solicitante', 'like', "%$data%");
            })
            ->paginate($this->per_page);

        return $this->success_response(
            SupportResource::collection($supports),
            Response::HTTP_OK,
            [
                'headers' => SupportResource::headers()
            ]
        );
    }

    public function show(ContractSupport $support): JsonResponse
    {
        $history = ContractSupport::query()
            ->where('Documento_Solicitante', $support->Documento_Solicitante)
            ->whereKeyNot($support->Id)
            ->latest()
            ->get();

        return $this->success_response(
            new SupportResource($support),
            Response::HTTP_OK,
            [
                'history' => SupportResource::collection($history),
                'headers' => SupportResource::headers()
            ]
        );
    }

    public function update(SupportRequest $request, ContractSupport $support): JsonResponse
    {
        try {
            $support->fill($request->validated());
            $support->Estado = 2;
            $support->Solucion = $request->get('solution');
            $file = null;
            if ($request->get('contract_document')) {
                $contract = CertificationContract::query()
                    ->where('Cedula', $request->get('contract_document'))
                    ->where('Numero_Contrato', $request->get('contract_number'))
                    ->whereYear('Fecha_Firma', $request->get('contract_year'))
                    ->first();

                if ($contract) {
                    $http = new Client();
                    $response = $http->get("https://sim1.idrd.gov.co/SIM/CertificacionContratos/descargarContrato/$contract->Id/1");
                    $file = base64_encode($response->getBody()->getContents());
                }
            }

            $support->saveOrFail();
            $this->dispatch(new ReplySupport($support, $file));
            return $this->success_message(__('validation.handler.success'));
        } catch (Throwable $exception) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $exception->getMessage()
            );
        }
    }
}
