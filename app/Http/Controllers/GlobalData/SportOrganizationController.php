<?php

namespace App\Http\Controllers\GlobalData;

use App\Http\Resources\GlobalData\SportOrganizationResource;
use App\Models\Security\SportOrganization;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use function Clue\StreamFilter\fun;
use function foo\func;

class SportOrganizationController extends Controller
{
    /**
     * Initialize common request params
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $data = $this->setQuery(SportOrganization::query()->orderBy('id'), (new SportOrganization())->getKeyName())->get();
        return $this->success_response(
            SportOrganizationResource::collection($data)
        );
    }
}
