<?php

namespace App\Modules\Contractors\src\Resources;

use App\Modules\Contractors\src\Models\Contract;
use App\Modules\Contractors\src\Models\Contractor;
use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use function foo\func;

class CertificateContractualResource extends JsonResource
{
    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request): array
    {
        $document = $this->document ?? $this->Cedula;
        $contractor = $this->getContractor($document);

        return [
            // Contractor
            'document_type' => isset($this->Tipo_Documento) ? (int)$this->Tipo_Documento : null,
            'document' => isset($this->Cedula) ? (int)$this->Cedula : null,
            'Dv' => isset($this->Dv) ? (int)$this->Dv : null,
            'name' => $this->Nombre_Contratista ?? null,
            //'email' => isset($contractor->email) ? $this->hideEmail($contractor->email) : null,
            'email' => isset($contractor->email) ? $contractor->email : null,
            'phone' => isset($contractor->phone) ? $contractor->phone : null,
            // Legal Representative
            'document_type_representative' => isset($this->Tipo_Documento_Representante) ? (int)$this->Tipo_Documento_Representante : null,
            'document_representative' => isset($this->Cedula_Representante) ? (int)$this->Cedula_Representante : null,
            'name_representative' => $this->Nombre_Representante ?? null,
            // Contract
            'contract_id' => isset($this->Id) ? (int)$this->Id : null,
            'contract_type' => isset($this->Tipo_Contrato_Id) ? (int)$this->Tipo_Contrato_Id : null,
            'reference' => $this->Referencia ?? null,
            'contract_number' => isset($this->Numero_Contrato) ? (int)$this->Numero_Contrato : null,
            'object' => $this->Objeto ?? null,
            'signature_date' => isset($this->Fecha_Firma) ? $this->Fecha_Firma->format('Y-m-d') : null,
            'start_date' => isset($this->Fecha_Inicio) ? $this->Fecha_Inicio->format('Y-m-d') : null,
            'final_date' => isset($this->Fecha_Fin) ? $this->Fecha_Fin->format('Y-m-d') : null,
            'anticipated_date' => isset($this->Fecha_Terminacion_Anticipada) ? $this->Fecha_Terminacion_Anticipada->format('Y-m-d') : null,
            'duration_months' => $this->Meses_Duracion ?? null,
            'duration_days' => $this->Dias_Duracion ?? null,
            'duration_other' => $this->Otra_Duracion ?? null,
            'initial_value' => isset($this->Valor_Inicial) ? (int)$this->Valor_Inicial : null,
            'monthly_value' => isset($this->Valor_Mensual) ? (int)$this->Valor_Mensual : null,
            'created_at' => isset($this->created_at) ? $this->created_at->format('Y-m-d HH:mm:ss') : null,
            'updated_at' => isset($this->updated_at) ? $this->updated_at->format('Y-m-d HH:mm:ss') : null,
            'obligations' => $this->obligations ?? [],
        ];
    }
    public function getContractor($document) {
        return Contractor::query()
            ->with('contracts')
            ->where('document', $document)
            ->first();
    }

    public function duration($duration, $unit, $start_date)
    {
        if ($unit == 'Meses') {
            return $duration;
        } elseif ($unit == 'Dias') {
            $date = Carbon::create($start_date);
            return $date->diffInMonths();
        }
        return $duration;
    }
    function hideEmail($email)
    {
        $minFill = 4;
        return preg_replace_callback(
            '/^(.)(.*?)([^@]?)(?=@[^@]+$)/u',
            function ($m) use ($minFill) {
                return $m[1]
                    . str_repeat("*", max($minFill, mb_strlen($m[2], 'UTF-8')))
                    . ($m[3] ?: $m[1]);
            },
            $email
        );
    }
}
