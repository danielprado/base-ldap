<?php

namespace App\Modules\Contractors\src\Resources;

use Illuminate\Http\Resources\Json\JsonResource;


class ContractWithPlaneResource extends JsonResource
{

    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id ?? null,
            'name' => $this->contractor->name .  ' '  . $this->contractor->surname ?? null,
            'document' => $this->contractor->document ?? null,
            'contract' => $this->getNumberContract($this->contract) ?? null,
            'date_init' => substr($this->start_date, 0, 10) ?? null,
            'date_end' => substr($this->final_date, 0, 10) ?? null,
            'value_month' => $this->document_init->value_month ?? null,
            'value_total' => $this->total ?? null,
        ];
    }

    private function getNumberContract($contract){
        $numberArray = explode('-', $contract);
        $numberContract = $numberArray[2] . '-' . $numberArray[3];
        return $numberContract;
    }
}
