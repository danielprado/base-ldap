<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateSupportTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('soporte', function (Blueprint $table) {
            $table->string('Telefono_Solicitante')->after('Correo_Solicitante')->nullable();
            $table->string('Numero_Contrato')->after('Telefono_Solicitante')->nullable();
            $table->string('Anio_Contrato')->after('Numero_Contrato')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('soporte', function (Blueprint $table) {
            $table->dropColumn('Telefono_Solicitante');
            $table->dropColumn('Numero_Contrato');
            $table->dropColumn('Anio_Contrato');
        });
    }
}
