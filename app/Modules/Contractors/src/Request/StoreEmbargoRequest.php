<?php


namespace App\Modules\Contractors\src\Request;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreEmbargoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' =>  'nullable|numeric',
            'type'  =>  'required|string|min:3|max:191',
            'filed'  =>  'required|string|min:3|max:191',
            'date'    =>  'required|date|date_format:Y-m-d',
            'job'  =>  'required|string|min:3|max:191',
            'judged'   =>  'required|string|min:3|max:191',
            'value' =>  'required|numeric',
            'judicial_account' =>  'required|numeric',
            'description' =>  'required|string|min:3|max:191',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'type'  =>  'tipo de embargo',
            'filed'  =>  'radicado (expediente)',
            'date'    =>  'fecha',
            'job'  =>  'oficio',
            'judged'   =>  'juzgado',
            'value' =>  'valor y/o cuantía',
            'judicial_account' =>  'cuenta judicial',
            'description' =>  'descripción',
        ];
    }
}
