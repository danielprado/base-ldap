<?php

namespace App\Modules\SportsPerformance\src\Exceptions;

use Exception;
use Illuminate\Http\Response;

class InaccessibleComplianceException extends Exception
{
    /**
     * Render the exception into an HTTP response.
     */
    public function render($request)
    {
        return response()
            ->json([
                'message' => 'No puede acceder a este formato por el momento. Verifique la disponibilidad.',
                'details' => 'Verifique la disponibilidad de este formato con el correspondiente técnico en su subdirección.',
                'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                'requested_at' => now()->toDateTimeLocalString()
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}
