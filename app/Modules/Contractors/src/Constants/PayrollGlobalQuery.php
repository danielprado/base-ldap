<?php

namespace App\Modules\Contractors\src\Constants;

use App\Models\Security\Subdirectorate;
use Illuminate\Database\Concerns\BuildsQueries;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class PayrollGlobalQuery
{
    /**
     * @param $request
     * @param Builder $builder
     * @return BuildsQueries|Builder|mixed
     */
    public static function query($request, Builder $builder)
    {
        return $builder->when($request->has(['year', 'month']), function ($q) use ($request) {
            return $q->where('year', $request->get('year'))
                ->where('month', $request->get('month'));
        })->when($request->has('state'), function ($q) use ($request) {
            return $q->where('status', $request->get('state'));
        })->when($request->has('query'), function ($q) use ($request) {
            $data = toLower($request->get('query'));
            return $q->where(function ($query) use ($data) {
                $query->whereHas('plans.contract.contractor', function ($q) use ($data) {
                    $q->where('document', 'like', "%{$data}%");
                })->orWhereHas('plans', function ($query) use ($data) {
                    $query->where('contract', 'like', "%{$data}%");
                });
            });
        })->orderBy('updated_at', 'DESC');
    }

    public static function queryStates($request, Builder $builder)
    {
        return $builder->when($request->has(['year', 'month']), function ($q) use ($request) {
            return $q->where('year', $request->get('year'))
                ->where('month', $request->get('month'));
        });
    }

    public function paymentPlan($files = 'arl_files_count')
    {
        //
    }
}
