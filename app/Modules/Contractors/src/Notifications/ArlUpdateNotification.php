<?php


namespace App\Modules\Contractors\src\Notifications;


use App\Modules\Contractors\src\Models\Contract;
use App\Modules\Contractors\src\Models\Contractor;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ArlUpdateNotification extends Notification
{
    use Queueable;

    /**
     * @var Contractor
     */
    private $contractor;

    /**
     * @var Contract
     */
    private $contract;

    /**
     * Create a new notification instance.
     *
     * @param Contractor $contractor
     * @param Contract $contract
     */
    public function __construct(Contractor $contractor, Contract $contract)
    {
        $this->contractor = $contractor;
        $this->contract = $contract;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        $id = $this->contractor->id ?? '';
        $first = $this->contractor->name ?? '';
        $second = $this->contractor->surname ?? '';
        $number = $this->contract->contract ?? '000';
        $type = $this->contract->contract_type->name ?? '';
        $name = "$first $second";
        $subject = "{$name} / {$type} / {$number}";
        return [
            'title'         => $type,
            'subject'       => $subject,
            'user'          => "SISTEMA",
            'created_at'    => now()->format('Y-m-d H:i:s'),
            'url'           => [
                'name'      =>  'user-id-contractor',
                'params'    =>  [ 'id'  => $id ]
            ]
        ];
    }
}
