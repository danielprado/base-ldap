<?php

namespace App\Modules\Contractors\src\Models;

use Illuminate\Database\Eloquent\Model;

class UserSevenThirdParty extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'oracle_ws';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'seven.gn_terce';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'ter_codi';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'ter_dire',
        'ter_mail',
        'ter_celu',
        'ter_acti',
    ];
}
