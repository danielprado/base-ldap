<?php

namespace App\Modules\Certifications\src\Models;

use App\Models\Security\DocumentType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;


class CertificationAssignment extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'mysql_certificacion_contratos';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'cesion';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Contrato_Id',
        'Numero_Cesion',
        'Nombre_Cesionario',
        'Tipo_Documento_Cesionario',
        'Cedula_Cesionario',
        'Dv_Cesion',
        'Valor_Cedido',
        'Fecha_Cesion',
    ];

    protected $dates = ['Fecha_Cesion'];

    public function getFechaCesionAttribute($value)
    {
        if ($value === '-0001-11-30' || $value === '0000-00-00') {
            return null;
        }
        return $value;
    }

    /**
     * @return BelongsTo
     */
    public function document_type()
    {
        return $this->belongsTo(DocumentType::class, 'Tipo_Documento_Cesionario');
    }
}
