<?php

namespace App\Modules\Contractors\src\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ComplianceResourceSeven extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'compliance_id'       => $this['id'] ?? null,
            'contractor_document' => $this['document'] ?? null,
            'contractor_name' =>  $this['contractor_name'] ?? null,
            'contract_number' =>  $this['contract_number'] ?? null,
            'is_validity'         => $this['is_validity'] ?? null,
            "is_seven" => $this['is_seven'] ?? null,
            "created_at" => $this['created_at'] ?? null,
            "updated_at" => $this['updated_at'] ?? null,
        ];
    }
}
