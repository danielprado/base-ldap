<?php

namespace App\Modules\SportsPerformance\src\Exceptions;

use Exception;
use Illuminate\Http\Response;

class BadResponseException extends Exception
{
    public function __construct($message = 'La petición falló') {
        $this->message = $message;
    }
    /**
     * Render the exception into an HTTP response.
     */
    public function render($request)
    {
        return response()
            ->json([
                'message' => $this->message,
                'details' => 'La petición falló.',
                'code' => Response::HTTP_UNPROCESSABLE_ENTITY,
                'requested_at' => now()->toDateTimeLocalString()
            ], Response::HTTP_UNPROCESSABLE_ENTITY);
    }
}