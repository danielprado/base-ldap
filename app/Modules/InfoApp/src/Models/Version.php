<?php

namespace App\Modules\InfoApp\src\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Version extends Model
{
      //
      use SoftDeletes;
      protected $connection = 'mysql_infoapp';
      protected $table = 'versionApp';
      protected $dates = ['deleted_at'];
      protected $fillable = [];

}
