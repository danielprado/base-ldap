<?php

namespace App\Modules\Contractors\src\Helpers\compliance\validators;

interface Validator
{
  function name(): string;
  function validate($object): bool;
}