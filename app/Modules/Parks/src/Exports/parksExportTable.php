<?php

namespace App\Modules\Parks\src\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Concerns\Exportable;

class parksExportTable implements FromCollection, WithHeadings, WithMapping
{
    
    use Exportable;

    protected $parksEndoments;

    public function __construct($parksEndoments = null)
    {
          $this->parksEndoments = $parksEndoments;
    }

    public function collection()
    {
        return $this->parksEndoments;
    }

    public function map($parksEndoments): array
    {
        return [
            $parksEndoments->park ? $parksEndoments->park->Id : '',
            $parksEndoments->park ? $parksEndoments->park->Nombre : '',
            $parksEndoments->park ? $parksEndoments->park->Direccion : '',
            $parksEndoments->endowment->Dotacion,
            $parksEndoments->endowment->equipment->Equipamento,
            $parksEndoments->park ? $parksEndoments->park->Id_IDRD : '',
        ];
    }
    
    public function headings(): array
    {
          return ['ID','NOMBRE','DIRECCION', 'DOTACION', 'EQUIPAMENTO', 'CODIGO_IDRD'];
    }

}
