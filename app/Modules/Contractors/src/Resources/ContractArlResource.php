<?php

namespace App\Modules\Contractors\src\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class ContractArlResource extends JsonResource
{
    public function __construct($resource)
    {
        parent::__construct($resource);
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return  [
            'contract'      => $this->formatContract($this->contract) ?? null,
            'risk'          => $this->risk ?? null,
        ];
    }

    public function formatContract($contract)
    {
        return substr($contract, 9);
    }
}
