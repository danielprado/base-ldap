<?php

namespace App\Modules\Certifications\src\Exports;

use App\Models\Security\User;
use App\Modules\Certifications\src\Models\CertificationObligation;
use App\Traits\AppendHeaderToExcel;
use Carbon\Carbon;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Database\Eloquent\Builder;
use Imtigger\LaravelJobStatus\JobStatus;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromQuery;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class ObligationExport implements FromQuery, WithTitle, WithEvents, WithHeadings, WithMapping, WithColumnFormatting, ShouldQueue, ShouldAutoSize
{
    use Exportable, AppendHeaderToExcel;

    /**
     * @var array
     */
    private $request;

    /**
     * @var int
     */
    private $rowNumb = 2;

    /**
     * @var User
     */
    private $user;

    public function __construct(array $request, $job, $user)
    {
        $this->user = $user;
        $this->request = $request;
        update_status_job($job, JobStatus::STATUS_EXECUTING, 'excel-certifications-module');
    }

    /**
     * @return Builder
     */
    public function query()
    {
        return CertificationObligation::query()
            ->whereKey($this->request['obligations'])
            ->orderBy('created_at', 'desc');
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $sheet) {
                $this->setHeader($sheet->sheet, 'OBLIGACIONES - CERTIFICACION DE CONTRATOS', 'A1:F1', 'F');
                $high = $sheet->getSheet()->getDelegate()->getHighestRow();
                $sheet->getSheet()->getDelegate()
                    ->getStyle("D8:D$high")->getAlignment()
                    ->setWrapText(true)
                    ->setVertical(Alignment::VERTICAL_CENTER)
                    ->setHorizontal(Alignment::HORIZONTAL_LEFT);
            }
        ];
    }

    public function headings(): array
    {
        return [
            'ID OBLIGACIÓN',
            'ID DEL CONTRATO',
            'NÚMERO OBLIGACIÓN',
            'NOMBRE',
            'FECHA DE CREACIÓN',
            'FECHA DE MODIFICACIÓN',
        ];
    }

    public function title(): string
    {
        return 'OBLIGACIONES';
    }

    public function columnFormats(): array
    {
        return [
            'E' => NumberFormat::FORMAT_DATE_YYYYMMDD2.' h'.NumberFormat::FORMAT_DATE_TIME4,
            'F' => NumberFormat::FORMAT_DATE_YYYYMMDD2.' h'.NumberFormat::FORMAT_DATE_TIME4,
        ];
    }

    public function map($row): array
    {
        return [
            'id'            => $row['Id'] ?? null,
            'contract_id'   => $row['Contrato_Id'] ?? null,
            'number'        => $row['Numero_Obligacion'] ?? null,
            'name'          => $row['Objeto_Obligacion'] ?? null,
            'created_at'    => isset($row['created_at']) ? date_time_to_excel(Carbon::parse($row['created_at'])) : null,
            'updated_at'    => isset($row['updated_at']) ? date_time_to_excel(Carbon::parse($row['updated_at'])) : null,
        ];
    }
}
