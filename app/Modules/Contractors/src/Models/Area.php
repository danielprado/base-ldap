<?php

namespace App\Modules\Contractors\src\Models;

use Illuminate\Database\Eloquent\Model;


class Area extends Model
{

    protected $connection = 'mysql_ldap';
    protected $table = 'areas';
    protected $hidden = ['created_at', 'updated_at'];

    protected $primaryKey = 'id';

    public function payroll()
    {
        return $this->belongsTo(Payroll::class);
    }


}
