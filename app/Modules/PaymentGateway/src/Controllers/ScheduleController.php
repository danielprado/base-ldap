<?php

namespace App\Modules\PaymentGateway\src\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\PaymentGateway\src\Exports\ScheduleExport;
use App\Modules\PaymentGateway\src\Models\ScheduleView;
use App\Modules\PaymentGateway\src\Resources\ScheduleResource;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ScheduleController extends Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function search(Request $request): \Illuminate\Http\JsonResponse
    {
        $data = toUpper($request->get('query'));
        $schedules = ScheduleView::query()
            ->where('IdParque', 'like', "%$data%")
            ->orWhere('NombreParque', 'like', "%$data%")
            ->orderBy('FechaHabilitacion', 'desc')
            ->get();

        return $this->success_response(ScheduleResource::collection($schedules));
    }

    public function index(): \Illuminate\Http\JsonResponse
    {
        $schedules = ScheduleView::query()
            ->orderBy('FechaHabilitacion', 'desc')
            ->get();

        return $this->success_response(ScheduleResource::collection($schedules));
    }

    public function export(Request $request): \Symfony\Component\HttpFoundation\BinaryFileResponse
    {
        $data = collect($request);
        $file_name = "reporte-horarios-" . now()->format('Y-m-d') . ".xlsx";
        $schedules = ScheduleView::query()->get();
        if (!is_null($data->get('query'))) {
            $search = $data->get('query');
            $schedules = ScheduleView::query()->where('IdParque', 'like', "%$search%")
                ->orWhere('NombreParque', 'like', "%$search%")
                ->get();
        }
        return Excel::download(new ScheduleExport($schedules), $file_name);
    }
}
