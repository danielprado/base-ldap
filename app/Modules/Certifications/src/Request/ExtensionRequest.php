<?php

namespace App\Modules\Certifications\src\Request;

use Illuminate\Foundation\Http\FormRequest;

class ExtensionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number'        => 'required|numeric',
            'months'        => 'nullable|numeric',
            'days'          => 'nullable|numeric',
            'final_date'    => 'required|date',
        ];
    }

    public function attributes()
    {
        return [
            'number'        => 'Número de la prórroga',
            'months'        => 'Meses',
            'days'          => 'Días',
            'final_date'    => 'Fecha de finalización',
        ];
    }
}
