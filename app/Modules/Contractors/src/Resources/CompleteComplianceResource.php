<?php

namespace App\Modules\Contractors\src\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\Crypt;

class CompleteComplianceResource extends JsonResource
{
    private $compliance;

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $payload = $this->generatePayload();
        return [
            'id'                        => $this->id,
            'status'                    => $this->status,
            'status_text'               => $this->statusText($this->status),
            'period_month'              => $this->period_month,
            'period_month_text'         => toUpper(Carbon::create()->month($this->period_month)->monthName) ?? null,
            'period_year'               => $this->period_year,
            'contractor_id'             => $this->contractor_id,
            'document_type'             => $this->document_type,
            'document'                  => $this->document,
            'name'                      => $this->name,
            'surname'                   => $this->surname,
            'contract_type'             => $this->contract_type,
            'contract'                  => $this->contract_number,
            'subdirectorate'            => $this->subdirectorate,
            'area'                      => $this->area,
            'has_children'              => $this->has_children,
            'has_children_text'         => $this->has_children  ? 'SI' : 'NO',
            'children_number'           => $this->children_number ?? 0,
            'children_minor'            => $this->children_minor,
            'children_minor_text'       => $this->children_minor ? 'SI' : 'NO',
            'children_mayor_students'   => $this->children_mayor_students,
            'children_mayor_students_text'  => $this->children_mayor_students ? 'SI' : 'NO',
            'children_disabilities'     => $this->children_disabilities,
            'children_disabilities_text'    => $this->children_disabilities ? 'SI' : 'NO',
            'spouse'                    => $this->spouse,
            'spouse_text'               => $this->spouse ? 'SI' : 'NO',
            'parents_dependency'        => $this->parents_dependency,
            'parents_dependency_text'   => $this->parents_dependency ? 'SI' : 'NO',
            'income_tax_filer'          => $this->income_tax_filer,
            'income_tax_filer_text'     => $this->income_tax_filer ? 'SI' : 'NO',
            'withholding_value'         => isset($this->withholding_value) ? $this->applyMoneyFormat($this->withholding_value) : 0,
            'payment_date_text'         => isset($this->payment_date) ? toUpper(Carbon::createFromFormat('Y-m-d', $this->payment_date)->translatedFormat('l j \\d\\e F \\d\\e Y')) : null,
            'pensioner'                 => $this->pensioner,
            'pensioner_text'            => $this->pensioner ? 'SI' : 'NO',
            'afc_voluntary'             => $this->afc_voluntary,
            'afc_voluntary_text'        => $this->afc_voluntary ? 'SI' : 'NO',
            'afp_voluntary'             => $this->afp_voluntary,
            'afp_voluntary_text'        => $this->afp_voluntary ? 'SI' : 'NO',
            'responsible_iva'           => $this->responsible_iva,
            'responsible_iva_text'      => $this->responsible_iva ? 'SI' : 'NO',
            'withholding'               => $this->withholding,
            'withholding_text'          => $this->withholding ? 'SI' : 'NO',
            'third_people'              => $this->third_people,
            'spouse_tax'                => $this->spouse_tax,
            'percentage'                => $this->percentage,
            'retention'                 => $this->retention,
            'afc_value'                 => isset($this->afc_value) ? $this->applyMoneyFormat($this->afc_value) : '$0',
            'afp_voluntary_value'       => isset($this->afp_voluntary_value) ? $this->applyMoneyFormat($this->afp_voluntary_value) : '$0',
            'housing_interests'         => $this->housing_interests,
            'housing_interests_text'    => $this->housing_interests ? 'SI' : 'NO',
            'prepaid_medicine'          => $this->prepaid_medicine,
            'prepaid_medicine_text'     => $this->prepaid_medicine ? 'SI' : 'NO',
            'payload'                   => $this->isRejected() ? $payload : null,
            'observations'              => $this->observations ?? null,
            'reviewed'                  => $this->reviewed,
            'name_reviewer'             => $this->name_reviewer,
            'reviewed_technical'        => $this->reviewed_technical,
            'name_reviewer_technical'   => $this->name_reviewer_technical,
            'files'                     => [
                'children_minor_files'          => $this->showContributionFile('children_minor'),
                'children_mayor_students_files' => $this->showContributionFile('children_mayor_students'),
                'children_disabilities_files'   => $this->showContributionFile('children_disabilities'),
                'spouse_files'                  => $this->showContributionFile('spouse'),
                'parents_dependency_files'      => $this->showContributionFile('parents_dependency'),
                'housing_interests_files'       => $this->showContributionFile('housing_interests'),
                'prepaid_medicine_files'        => $this->showContributionFile('prepaid_medicine'),
                'pensioner_file'                => $this->showContributionFile('pensioner'),
                'afc_voluntary_file'            => $this->showContributionFile('afc_voluntary'),
                'afp_voluntary_file'            => $this->showContributionFile('afp_voluntary'),
            ],
        ];
    }

    private function showContributionFile($name)
    {
        $file = $this->contribution($name)->getFile();
        return $file ? $file->asUrl() : null;
    }

    private function applyMoneyFormat($value)
    {
        return $value
            ? preg_replace('/\s+/', '', cop_money_format($value, '$', null, 0, null, '.'))
            : null;
    }

    private function generatePayload()
    {
        //$contractNumberAndYear = extract_number_and_year_from_contract($this->contract_number);
        return Crypt::encrypt(json_encode([
            'document' => $this->document,
            'contract' => $this->contract_number,
            'period_month' => $this->period_month,
            'period_year' => $this->period_year
        ]));
    }

    public function statusText($status) {
        if ($status == 'PENDING') {
            return 'PENDIENTE';
        } elseif ($status == 'REJECTED') {
            return 'RECHAZADO';
        } else {
            return 'APROBADO';
        }
    }

    public static function headers()
    {
        return [
            [
                'text' => "#",
                'value'  =>  "id",
            ],
            [
                'align' => "center",
                'text' => "Detalles",
                'value'  =>  "view",
            ],
            [
                'text' => "Archivos",
                'value'  =>  "files",
            ],
            [
                'text' => "Estado",
                'value'  =>  "status_text",
            ],
            [
                'text' => 'Número de contrato',
                'value'  =>  'contract',
            ],
            [
                'text' => "Mes de certificado",
                'value'  =>  "period_month_text",
            ],
            [
                'text' => "Año de certificado",
                'value'  =>  "period_year",
            ],
            [
                'text'      => "Acciones",
                'value'     =>  "actions",
            ],
        ];
    }

    public static function additionalData()
    {
        return [
            [
                'label' => "Pensionado",
                'field'  =>  "pensioner_text",
                "icon"  => "mdi-account-tie",
                "file"  => true,
            ],
            [
                'label' => "Soy declarante de renta",
                'field'  =>  "income_tax_filer_text",
                "icon"  => "mdi-printer-pos",
            ],
            [
                'label' => "Soy responsable de IVA",
                'field'  =>  "responsible_iva_text",
                "icon"  => "mdi-sale",
            ],
            [
                'label'  => 'Solicito una retención en la fuente adicional a la calculada',
                'field'  => 'withholding_text',
                "icon"   => "mdi-hand-coin",
            ],
            [
                'label'  => 'valor de la retención en la fuente',
                'field'  => 'withholding_value',
                "icon"   => "mdi-currency-usd",
            ],
            [
                'label'  => 'realicé pago de intereses por préstamos para adquisión de vivienda',
                'field'  => 'housing_interests_text',
                "icon"   => "mdi-home-percent",
                "file"  => true,
            ],
            [
                'label'  => 'realicé pago por salud (medicina prepagada - seguros de salud)',
                'field'  => 'prepaid_medicine_text',
                "icon"   => "mdi-hospital-box",
                "file"   => true,
            ],
            [
                'label'  => 'Para el presente pago realicé aporte a pensiones voluntarias (AFP)',
                'field'  => 'afp_voluntary_text',
                "icon"   => "mdi-hand-heart",
                "file"  => true,
            ],
            [
                'label'  => 'valor del aporte a pensiones voluntarias',
                'field'  => 'afp_voluntary_value',
                "icon"   => "mdi-currency-usd",
            ],
            [
                'label'  => 'Para el presente pago realicé aporte voluntario a cuenta de ahorro fomento a la construcción (AFC)',
                'field'  => 'afc_voluntary_text',
                "icon"   => "mdi-home-percent",
                "file"  => true,
            ],
            [
                'label'  => 'valor del aporte a cuenta de ahorro fomento a la construcción',
                'field'  => 'afc_value',
                "icon"   => "mdi-currency-usd",
            ],
            [
                'label'  => 'Tengo dependientes',
                'field'  => 'has_children_text',
                "icon"   => "mdi-account-group",
            ],
            [
                'label'  => 'Número de dependientes',
                'field'  => 'children_number',
                "icon"   => "mdi-numeric",
            ],
            [
                'label'  => 'Hijos menores de 18 años',
                'field'  => 'children_minor_text',
                "icon"   => "mdi-baby-face",
                "file"  => true,
            ],
            [
                'label'  => 'Hijos entre los 18 y 23 años a quienes se les esté dando educación',
                'field'  => 'children_mayor_students_text',
                "icon"   => "mdi-school",
                "file"  => true,
            ],
            [
                'label'  => 'Hijos en situación de dependencia por discapacidad',
                'field'  => 'children_disabilities_text',
                "icon"   => "mdi-wheelchair-accessibility",
                "file"  => true,
            ],
            [
                'label'  => 'Cónyuge o compañero permanente en situación de dependencia',
                'field'  => 'spouse_text',
                "icon"   => "mdi-heart-plus-outline",
                "file"  => true,
            ],
            [
                'label'  => 'Padres y hermanos en situación de dependencia',
                'field'  => 'parents_dependency_text',
                "icon"   => "mdi-account-group-outline",
                "file"  => true,
            ],
        ];
    }
}
