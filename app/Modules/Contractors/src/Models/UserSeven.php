<?php

namespace App\Modules\Contractors\src\Models;

use Illuminate\Database\Eloquent\Model;
use Yajra\Oci8\Query\OracleBuilder;

//use Yajra\Oci8\Eloquent\OracleEloquent as Model;

class UserSeven extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'oracle_ws';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'SEVEN.CT_CONTR';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'TER_CODI';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'TER_CODI',
        'CON_NCON',
        'CON_ANOP',
        'CON_NUME'
    ];

    /**
     * Get a new query builder instance for the connection.
     *
     * @return OracleBuilder
     */
    protected function newBaseQueryBuilder()
    {
        $conn = $this->getConnection();

        $grammar = $conn->getQueryGrammar();

        return new OracleBuilder($conn, $grammar, $conn->getPostProcessor());
    }
}

