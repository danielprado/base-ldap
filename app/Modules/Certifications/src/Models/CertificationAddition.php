<?php

namespace App\Modules\Certifications\src\Models;

use Illuminate\Database\Eloquent\Model;


class CertificationAddition extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string|null
     */
    protected $connection = 'mysql_certificacion_contratos';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'adicion';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'Contrato_Id',
        'Numero_Adicion',
        'Valor_Adicion',
    ];
}
