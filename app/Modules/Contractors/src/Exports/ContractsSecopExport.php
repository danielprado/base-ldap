<?php

namespace App\Modules\Contractors\src\Exports;

use App\Traits\AppendHeaderToExcel;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithColumnFormatting;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\WithMapping;
use Maatwebsite\Excel\Concerns\WithTitle;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;

class ContractsSecopExport implements FromCollection, WithMapping, WithColumnFormatting, ShouldAutoSize, WithTitle, WithHeadings, WithEvents
{
    use Exportable, AppendHeaderToExcel;

    /**
     * @var array
     */
    private $array;

    /**
     * DataSevenExport constructor.
     */
    public function __construct($collection)
    {
        $this->array = $collection;
    }

    public function collection()
    {
        return $this->array;
    }

    public function headings(): array
    {
        return [
            'nombre_entidad',
            'nit_entidad',
            'departamento',
            'ciudad',
            'localización',
            'orden',
            'sector',
            'rama',
            'entidad_centralizada',
            'proceso_de_compra',
            'id_contrato',
            'referencia_del_contrato',
            'estado_contrato',
            'codigo_de_categoria_principal',
            'descripcion_del_proceso',
            'tipo_de_contrato',
            'modalidad_de_contratacion',
            'justificacion_modalidad_de',
            'fecha_de_firma',
            'fecha_de_inicio_del_contrato',
            'fecha_de_fin_del_contrato',
            'condiciones_de_entrega',
            'tipodocproveedor',
            'documento_proveedor',
            'proveedor_adjudicado',
            'es_grupo',
            'es_pyme',
            'habilita_pago_adelantado',
            'liquidación',
            'obligaci_n_ambiental',
            'obligaciones_postconsumo',
            'reversion',
            'origen_de_los_recursos',
            'destino_gasto',
            'valor_del_contrato',
            'valor_de_pago_adelantado',
            'valor_facturado',
            'valor_pendiente_de_pago',
            'valor_pagado',
            'valor_amortizado',
            'valor_pendiente_de',
            'valor_pendiente_de_ejecucion',
            'estado_bpin',
            'c_digo_bpin',
            'anno_bpin',
            'saldo_cdp',
            'saldo_vigencia',
            'espostconflicto',
            'dias_adicionados',
            'puntos_del_acuerdo',
            'pilares_del_acuerdo',
            'urlproceso',
            'nombre_representante_legal',
            'nacionalidad_representante_legal',
            'domicilio_representante_legal',
            'tipo_de_identificaci_n_representante_legal',
            'identificaci_n_representante_legal',
            'g_nero_representante_legal',
            'presupuesto_general_de_la_nacion_pgn',
            'sistema_general_de_participaciones',
            'sistema_general_de_regal_as',
            'recursos_propios_alcald_as_gobernaciones_y_resguardos_ind_genas_',
            'recursos_de_credito',
            'recursos_propios',
            'codigo_entidad',
            'codigo_proveedor',
            'objeto_del_contrato',
            'duraci_n_del_contrato',
            'nombre_del_banco',
            'tipo_de_cuenta',
            'n_mero_de_cuenta',
            'el_contrato_puede_ser_prorrogado',
            'nombre_ordenador_del_gasto',
            'tipo_de_documento_ordenador_del_gasto',
            'n_mero_de_documento_ordenador_del_gasto',
            'nombre_supervisor',
            'tipo_de_documento_supervisor',
            'n_mero_de_documento_supervisor',
            'nombre_ordenador_de_pago',
            'tipo_de_documento_ordenador_de_pago',
            'n_mero_de_documento_ordenador_de_pago',
            'duracion',
            'unidad_de_duracion',
            'precio_base',
        ];
    }

    public function columnFormats(): array
    {
        return [
            'B' => NumberFormat::FORMAT_NUMBER,
            'S' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'T' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'U' => NumberFormat::FORMAT_DATE_DDMMYYYY,
            'X' => NumberFormat::FORMAT_NUMBER,
            'AI' => NumberFormat::FORMAT_CURRENCY_USD,
            'AJ' => NumberFormat::FORMAT_CURRENCY_USD,
            'AK' => NumberFormat::FORMAT_CURRENCY_USD,
            'AL' => NumberFormat::FORMAT_CURRENCY_USD,
            'AM' => NumberFormat::FORMAT_CURRENCY_USD,
            'AN' => NumberFormat::FORMAT_CURRENCY_USD,
            'AO' => NumberFormat::FORMAT_CURRENCY_USD,
            'AP' => NumberFormat::FORMAT_CURRENCY_USD,
            'AT' => NumberFormat::FORMAT_CURRENCY_USD,
            'BE' => NumberFormat::FORMAT_NUMBER,
            'BG' => NumberFormat::FORMAT_CURRENCY_USD,
            'BH' => NumberFormat::FORMAT_CURRENCY_USD,
            'BI' => NumberFormat::FORMAT_CURRENCY_USD,
            'BJ' => NumberFormat::FORMAT_CURRENCY_USD,
            'BK' => NumberFormat::FORMAT_CURRENCY_USD,
            'BL' => NumberFormat::FORMAT_CURRENCY_USD,
            'BM' => NumberFormat::FORMAT_NUMBER,
            'BN' => NumberFormat::FORMAT_NUMBER,
            'BS' => NumberFormat::FORMAT_NUMBER,
            'BW' => NumberFormat::FORMAT_NUMBER,
            'BZ' => NumberFormat::FORMAT_NUMBER,
            'CC' => NumberFormat::FORMAT_NUMBER,
            'CF' => NumberFormat::FORMAT_CURRENCY_USD,
        ];
    }

    public function map($row): array
    {
        return [
            $row['nombre_entidad'] ?? null,
            isset($row['nit_entidad']) ? (int) $row['nit_entidad'] : null,
            $row['departamento'] ?? null,
            $row['ciudad'] ?? null,
            $row['localizaci_n'] ?? null,
            $row['orden'] ?? null,
            $row['sector'] ?? null,
            $row['rama'] ?? null,
            $row['entidad_centralizada'] ?? null,
            $row['proceso_de_compra'] ?? null,
            $row['id_contrato'] ?? null,
            $row['referencia_del_contrato'] ?? null,
            $row['estado_contrato'] ?? null,
            $row['codigo_de_categoria_principal'] ?? null,
            $row['descripcion_del_proceso'] ?? null,
            $row['tipo_de_contrato'] ?? null,
            $row['modalidad_de_contratacion'] ?? null,
            $row['justificacion_modalidad_de'] ?? null,
            isset($row['fecha_de_firma']) ? date_time_to_excel(Carbon::parse($row['fecha_de_firma'])) : null,
            isset($row['fecha_de_inicio_del_contrato']) ? date_time_to_excel(Carbon::parse($row['fecha_de_inicio_del_contrato'])) : null,
            isset($row['fecha_de_fin_del_contrato']) ? date_time_to_excel(Carbon::parse($row['fecha_de_fin_del_contrato'])) : null,
            $row['condiciones_de_entrega'] ?? null,
            $row['tipodocproveedor'] ?? null,
            isset($row['documento_proveedor']) ? (int) $row['documento_proveedor'] : null,
            isset($row['proveedor_adjudicado']) ? toUpper($row['proveedor_adjudicado']) : null,
            $row['es_grupo'] ?? null,
            $row['es_pyme'] ?? null,
            $row['habilita_pago_adelantado'] ?? null,
            $row['liquidación'] ?? null,
            $row['obligaci_n_ambiental'] ?? null,
            $row['obligaciones_postconsumo'] ?? null,
            $row['reversion'] ?? null,
            $row['origen_de_los_recursos'] ?? null,
            $row['destino_gasto'] ?? null,
            isset($row['valor_del_contrato']) ? (int) $row['valor_del_contrato'] : null,
            $row['valor_de_pago_adelantado'] ?? null,
            $row['valor_facturado'] ?? null,
            isset($row['valor_pendiente_de_pago']) ? (int) $row['valor_pendiente_de_pago'] : null,
            $row['valor_pagado'] ?? null,
            $row['valor_amortizado'] ?? null,
            $row['valor_pendiente_de'] ?? null,
            isset($row['valor_pendiente_de_ejecucion']) ? (int) $row['valor_pendiente_de_ejecucion'] : null,
            $row['estado_bpin'] ?? null,
            $row['c_digo_bpin'] ?? null,
            $row['anno_bpin'] ?? null,
            $row['saldo_cdp'] ?? null,
            $row['saldo_vigencia'] ?? null,
            $row['espostconflicto'] ?? null,
            $row['dias_adicionados'] ?? null,
            $row['puntos_del_acuerdo'] ?? null,
            $row['pilares_del_acuerdo'] ?? null,
            $row['urlproceso']['url'] ?? null,
            isset($row['nombre_representante_legal']) ? toUpper($row['nombre_representante_legal']) : null,
            $row['nacionalidad_representante_legal'] ?? null,
            $row['domicilio_representante_legal'] ?? null,
            $row['tipo_de_identificaci_n_representante_legal'] ?? null,
            isset($row['identificaci_n_representante_legal']) ? (int) $row['identificaci_n_representante_legal'] : null,
            $row['g_nero_representante_legal'] ?? null,
            $row['presupuesto_general_de_la_nacion_pgn'] ?? null,
            $row['sistema_general_de_participaciones'] ?? null,
            $row['sistema_general_de_regal_as'] ?? null,
            isset($row['recursos_propios_alcald_as_gobernaciones_y_resguardos_ind_genas_']) ? (int) $row['recursos_propios_alcald_as_gobernaciones_y_resguardos_ind_genas_'] : null,
            $row['recursos_de_credito'] ?? null,
            $row['recursos_propios'] ?? null,
            isset($row['codigo_entidad']) ? (int) $row['codigo_entidad'] : null,
            isset($row['codigo_proveedor']) ? (int) $row['codigo_proveedor'] : null,
            isset($row['objeto_del_contrato']) ? $this->setObject($row['objeto_del_contrato']) : null,
            $row['duraci_n_del_contrato'] ?? null,
            $row['nombre_del_banco'] ?? null,
            $row['tipo_de_cuenta'] ?? null,
            $row['n_mero_de_cuenta'] ?? null,
            $row['el_contrato_puede_ser_prorrogado'] ?? null,
            isset($row['nombre_ordenador_del_gasto']) ? toUpper($row['nombre_representante_legal']) : null,
            $row['tipo_de_documento_ordenador_del_gasto'] ?? null,
            isset($row['n_mero_de_documento_ordenador_del_gasto']) ? (int) $row['n_mero_de_documento_ordenador_del_gasto'] : null,
            isset($row['nombre_supervisor']) ? toUpper($row['nombre_supervisor']) : null,
            $row['tipo_de_documento_supervisor'] ?? null,
            isset($row['n_mero_de_documento_supervisor']) ? (int) $row['n_mero_de_documento_supervisor'] : null,
            $row['nombre_ordenador_de_pago'] ?? null,
            $row['tipo_de_documento_ordenador_de_pago'] ?? null,
            $row['n_mero_de_documento_ordenador_de_pago'] ?? null,
            $row['duracion'] ?? null,
            $row['unidad_de_duracion'] ?? null,
            isset($row['precio_base']) ? (int) $row['precio_base'] : null,
        ];
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class => function(AfterSheet $sheet) {
                $this->setHeader($sheet->sheet, 'REPORTE SECOP - PORTAL CONTRATISTA', 'A1:CF1', 'CF');
            },
        ];
    }

    public function title(): string
    {
        return 'Contratos ' . now()->year;
    }

    public function setObject($object)
    {
        return mb_strtoupper(preg_replace('/[\r\n"]/', '', $object), 'UTF-8');
    }
}
