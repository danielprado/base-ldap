<?php

namespace App\Helpers;

use allejo\Socrata\Exceptions\InvalidResourceException;
use allejo\Socrata\SodaClient;
use allejo\Socrata\SodaDataset;
use allejo\Socrata\SoqlQuery;
use Carbon\Carbon;
use Illuminate\Support\Collection;

class SecopAPI
{
    /**
     * Contratos Electronicos SECOP II - Datos abiertos - https://dev.socrata.com/foundry/www.datos.gov.co/jbjy-vk9h
     * @throws InvalidResourceException
     */
    protected function getSodaDatasetContracts(): SodaDataset
    {
        return new SodaDataset(new SodaClient('www.datos.gov.co'), 'jbjy-vk9h');
    }

    /**
     * Procesos de contratación SECOP II - Datos abiertos - https://dev.socrata.com/foundry/www.datos.gov.co/p6dx-8zbt
     * @return SodaDataset
     * @throws InvalidResourceException
     */
    protected function getSodaDatasetProcessContracts(): SodaDataset
    {
        return new SodaDataset(new SodaClient('www.datos.gov.co'), 'p6dx-8zbt');
    }

    /**
     * Facturas Pagadas SECOP II - Datos abiertos - https://dev.socrata.com/foundry/www.datos.gov.co/ibyt-yi2f
     * @return SodaDataset
     * @throws InvalidResourceException
     */
    protected function getSodaDatasetInvoice(): SodaDataset
    {
        return new SodaDataset(new SodaClient('www.datos.gov.co'), 'ibyt-yi2f');
    }

    /**
     * @param string $document
     * @return Collection
     * @throws InvalidResourceException
     */
    public function getActiveContracts(string $document): Collection
    {
        $date = now()->format('Y-m-d');
        $ds = $this->getSodaDatasetContracts();
        $soqlQuery = new SoqlQuery();
        $soqlQuery
            ->select('id_contrato', 'tipodocproveedor', 'documento_proveedor', 'proveedor_adjudicado', 'referencia_del_contrato', 'tipo_de_contrato',
                'fecha_de_firma', 'fecha_de_inicio_del_contrato', 'fecha_de_fin_del_contrato', 'valor_del_contrato',
                'objeto_del_contrato', 'proceso_de_compra', 'estado_contrato', 'dias_adicionados')
            ->where("nit_entidad = '860061099'
                AND documento_proveedor = '$document'
                AND estado_contrato in('En ejecución', 'Modificado', 'cedido')
                AND fecha_de_fin_del_contrato >= '$date'")
            ->order('fecha_de_firma', 'desc')
            ->limit(50);

        return $this->getData($ds, $soqlQuery);
    }

    /**
     * @throws InvalidResourceException
     * @return Collection
     */
    public function getAllActivesContracts(): Collection
    {
        $date = now()->format('Y-m-d');
        $year = now()->year;
        $subYear = now()->subYear()->year;
        $ds = $this->getSodaDatasetContracts();
        $soqlQuery = new SoqlQuery();
        $soqlQuery
            ->select('tipodocproveedor', 'documento_proveedor', 'referencia_del_contrato', 'tipo_de_contrato',
                'fecha_de_firma', 'fecha_de_inicio_del_contrato', 'fecha_de_fin_del_contrato', 'valor_del_contrato',
                'objeto_del_contrato', 'proceso_de_compra', 'estado_contrato', 'dias_adicionados')
            ->where("nit_entidad = '860061099'
                AND estado_contrato in('En ejecución', 'Modificado', 'cedido', 'Activo')
                AND (referencia_del_contrato LIKE 'IDRD-CTO_%$year'
                    OR referencia_del_contrato LIKE 'IDRD-CTO_%$subYear'
                    OR referencia_del_contrato LIKE 'IDRD-STRD-CPS-%$year%'
                    OR referencia_del_contrato LIKE 'IDRD-STRD-CPS-%-%$year'
                    OR referencia_del_contrato LIKE 'IDRD-SAF-CPS-%$year%'
                    OR referencia_del_contrato LIKE 'IDRD-SAF-CPS-%-%$year'
                    OR referencia_del_contrato LIKE 'IDRD-STP-CPS-%$year%'
                    OR referencia_del_contrato LIKE 'IDRD-STP-CPS-%-%$year'
                    OR referencia_del_contrato LIKE 'IDRD-STC-CPS-%$year%'
                    OR referencia_del_contrato LIKE 'IDRD-STC-CPS-%-%$year')
                AND fecha_de_fin_del_contrato >= '$date'
                AND tipo_de_contrato in('Prestación de servicios', 'No Especificado')")
            ->order('fecha_de_firma', 'desc')
            ->limit(10000);

        $contracts_secop = $ds->getData($soqlQuery);
        $process_contracts = $this->getAllProcessContracts();

        $contracts = $this->getFormatContracts($contracts_secop, $process_contracts);

        return !empty($contracts) ? collect($contracts) : collect();
    }

    /**
     * @throws InvalidResourceException
     * @return Collection
     */
    public function getAllContracts(): Collection
    {
        $ds = $this->getSodaDatasetContracts();
        $soqlQuery = new SoqlQuery();
        $soqlQuery
            ->where("nit_entidad = '860061099'
            AND fecha_de_firma >= '2023-01-01'")
            ->order('fecha_de_firma', 'desc')
            ->limit(10000);

        $contracts_secop = $ds->getData($soqlQuery);
        $process_contracts = $this->getAllProcessContracts();

        $contracts = $this->getFormatContracts($contracts_secop, $process_contracts);

        return !empty($contracts) ? collect($contracts) : collect();

        /* $contracts = collect();
        $offset = 0;

        do {
            $soqlQuery->offset($offset);
            $contracts_secop = $ds->getData($soqlQuery);

            if (!empty($contracts_secop)) {
                $process_contracts = $this->getAllProcessContracts();
                $formattedContracts = $this->getFormatContracts($contracts_secop, $process_contracts);
                $contracts = $contracts->merge(collect($formattedContracts));
            }

            $offset += 1000;
        } while (!empty($contracts_secop));

        return $contracts; */
    }

    /**
     * @param string $document
     * @return Collection
     * @throws InvalidResourceException
     */
    public function getContracts(string $document): Collection
    {
        $ds = $this->getSodaDatasetContracts();
        $soqlQuery = new SoqlQuery();
        $soqlQuery
            ->select('id_contrato', 'tipodocproveedor', 'documento_proveedor', 'proveedor_adjudicado', 'referencia_del_contrato', 'tipo_de_contrato',
                'fecha_de_firma', 'fecha_de_inicio_del_contrato', 'fecha_de_fin_del_contrato', 'valor_del_contrato',
                'objeto_del_contrato', 'proceso_de_compra', 'estado_contrato', 'dias_adicionados')
            ->where("nit_entidad = '860061099'
                AND documento_proveedor = '$document'")
            ->order('fecha_de_firma', 'desc')
            ->limit(50);

        return $this->getData($ds, $soqlQuery);
    }

    /**
     * @param string $contracts
     * @return array
     * @throws InvalidResourceException
     */
    public function getProcessContracts(string $contracts): array
    {
        $ds = $this->getSodaDatasetProcessContracts();
        $soqlQuery = new SoqlQuery();
        $soqlQuery
            ->select('precio_base', 'duracion', 'unidad_de_duracion', 'id_del_portafolio')
            ->where("nit_entidad = '860061099' AND id_del_portafolio in($contracts)")
            ->limit(1000);
        $data = $ds->getData($soqlQuery);

        return collect($data)->unique('id_del_portafolio')->toArray();
    }

    /**
     * @throws InvalidResourceException
     */
    public function getAllProcessContracts(): array
    {
        $year = now()->year;
        $subYear = now()->subYear()->year;
        $ds = $this->getSodaDatasetProcessContracts();
        $soqlQuery = new SoqlQuery();
        $soqlQuery
            ->select('precio_base', 'duracion', 'unidad_de_duracion', 'id_del_portafolio')
            ->where("nit_entidad = '860061099'
                AND (referencia_del_proceso LIKE 'IDRD-_%$year'
                OR referencia_del_proceso LIKE 'IDRD-_%$subYear'
                OR referencia_del_proceso LIKE 'IDRD-STRD-CPS-%$year%'
                OR referencia_del_proceso LIKE 'IDRD-STRD-CPS-%-%$year'
                OR referencia_del_proceso LIKE 'IDRD-SAF-CPS-%$year%'
                OR referencia_del_proceso LIKE 'IDRD-SAF-CPS-%-%$year'
                OR referencia_del_proceso LIKE 'IDRD-STP-CPS-%$year%'
                OR referencia_del_proceso LIKE 'IDRD-STP-CPS-%-%$year'
                OR referencia_del_proceso LIKE 'IDRD-STC-CPS-%$year%'
                OR referencia_del_proceso LIKE 'IDRD-STC-CPS-%-%$year')")
            ->limit(10000);
        return $ds->getData($soqlQuery);
    }

    /**
     * @param $document
     * @param $number
     * @return Collection
     * @throws InvalidResourceException
     */
    public function getInvoce($document, $number): Collection
    {
        $ds = $this->getSodaDatasetInvoice();
        $contracts = get_active_contracts_secop_api($document);
        $contract = collect($contracts)->first(function ($contract) use ($number) {
            $num = extract_number_format_contract($contract['referencia_del_contrato']);
            $int = intval($number);
            return $num == $int;
        });

        if ($contract) {
            $id_contract = $contract['id_contrato'];
            $soqlQuery = new SoqlQuery();
            $soqlQuery
                ->select('id_contrato', 'id_pago', 'numero_de_factura', 'fecha_factura', 'valor_neto',
                    'valor_total', 'notas', 'fecha_de_entrega', 'radicado', 'fecha_estiamda_de_pago', 'valor_a_pagar', 'estado', 'pago_confirmado', 'usuario_pago')
                ->where("id_contrato = '$id_contract'")
                ->order('fecha_factura', 'desc');

            return collect($ds->getData($soqlQuery));
        } else {
            return collect();
        }
    }

    /**
     * @param SodaDataset $ds
     * @param SoqlQuery $soqlQuery
     * @return Collection
     * @throws InvalidResourceException
     */
    public function getData(SodaDataset $ds, SoqlQuery $soqlQuery): Collection
    {
        $contracts_secop = $ds->getData($soqlQuery);

        if (!empty($contracts_secop)) {
            $keys = collect($contracts_secop)
                ->pluck('proceso_de_compra')
                ->map(function ($item) {
                    return "'$item'";
                })
                ->implode(", ");

            $process_contracts = $this->getProcessContracts($keys);

            $contracts = $this->getFormatContracts($contracts_secop, $process_contracts);

            return $contracts->unique('referencia_del_contrato');
        } else {
            return collect();
        }
    }

    /**
     * @param array $contracts_secop
     * @param array $process_contracts
     * @return Collection
     */
    public function getFormatContracts(array $contracts_secop, array $process_contracts): Collection
    {
        $contracts = collect();
        foreach ($contracts_secop as $contract) {
            $hasDuration = isset($contract['duracion']) && isset($contract['unidad_de_duracion']) && isset($contract['precio_base']);
            if (!$hasDuration) {
                if (isset($contract['proceso_de_compra'])) {
                    $process_contract = collect($process_contracts)->first(function ($process) use ($contract) {
                        return $process['id_del_portafolio'] === $contract['proceso_de_compra'];
                    });

                    if ($process_contract) {
                        $contract['duracion'] = $process_contract['duracion'];
                        $contract['unidad_de_duracion'] = $process_contract['unidad_de_duracion'];
                        $contract['precio_base'] = $process_contract['precio_base'];
                    } else {
                        $start = isset($contract['fecha_de_inicio_del_contrato']) ? Carbon::parse($contract['fecha_de_inicio_del_contrato']) : null;
                        $end = isset($contract['fecha_de_fin_del_contrato']) ? Carbon::parse($contract['fecha_de_fin_del_contrato']) : null;
                        $contract['duracion'] = isset($start) ? $start->diffInMonths($end) : null;
                        $contract['unidad_de_duracion'] = 'Mes(es)' ?? null;
                        $contract['precio_base'] = $contract['valor_del_contrato'] ?? null;
                    }
                } elseif (in_array($contract['estado_contrato'], ['En ejecución', 'Modificado', 'cedido', 'terminado', 'Suspendido', 'Cerrado'])) {
                    $start = Carbon::parse($contract['fecha_de_inicio_del_contrato']);
                    $end = Carbon::parse($contract['fecha_de_fin_del_contrato']);
                    $contract['duracion'] = $start->diffInMonths($end);
                    $contract['unidad_de_duracion'] = 'Mes(es)';
                    $contract['precio_base'] = $contract['valor_del_contrato'] ?? null;
                }
            }

            $contract['documento_proveedor'] = str_replace('.', '', $contract['documento_proveedor']);
            $contract['objeto_del_contrato'] = mb_strtoupper(preg_replace('/[\r\n"]/', '', $contract['objeto_del_contrato']), 'UTF-8');
            $contracts[] = $contract;
        }
        return $contracts;
    }
}
