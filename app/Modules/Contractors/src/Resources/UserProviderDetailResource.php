<?php

namespace App\Modules\Contractors\src\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UserProviderDetailResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'pvd_codi'  => $this->pvd_codi ?? null,
            'dep_nomb'  => $this->dep_nomb ?? null,
            'coc_codi'  => $this->coc_codi ?? null,
            'cim_codi'  => $this->cim_codi ?? null,
            'pai_codi'  => $this->pai_codi ?? null,
            'dep_codi'  => $this->dep_codi ?? null,
            'mun_codi'  => $this->mun_codi ?? null,
            'act_codi'  => $this->act_codi ?? null,
            'dep_dire'  => $this->dep_dire ?? null,
            'dep_ntel'  => $this->dep_ntel ?? null,
            'dep_mail'  => $this->dep_mail ?? null,
        ];
    }

    public static function headers(): array
    {
        return [
            [
                'text' => "Actividad económica",
                'value'  =>  "act_codi",
                'icon'  =>  'mdi-numeric',
            ],
        ];
    }

}
