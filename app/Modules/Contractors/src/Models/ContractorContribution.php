<?php

namespace App\Modules\Contractors\src\Models;

use App\Models\Security\Area;
use App\Models\Security\Subdirectorate;
use App\Modules\Contractors\src\Constants\Contributions;
use App\Modules\Contractors\src\Helpers\compliance\validators\ComplianceDateValidator;
use App\Modules\Contractors\src\Helpers\compliance\validators\ComplianceMonthlyValidator;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class ContractorContribution extends Model
{
    protected $connection = 'mysql_contractors';

    protected $table = 'contractor_contributions';

    protected $fillable = [
        'report_id',
        'payroll_number',
        'payment_date',
        'eps_name',
        'eps_value',
        'afp_name',
        'afp_value',
        'arl_name',
        'arl_value',
        'total',
        'support_file'
    ];

    public function report()
    {
        return $this->belongsTo(ActivityReport::class, 'report_id', 'id');
    }
}
