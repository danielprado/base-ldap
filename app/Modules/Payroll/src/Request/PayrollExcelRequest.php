<?php


namespace App\Modules\Payroll\src\Request;


use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PayrollExcelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
/**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'contractorsList' => 'required',
            'contractorsSummaryList' => 'required',
            'budgetCode' => 'required',
            'estPculSummary' => 'required',
            'estPpmSummary' => 'required',
            'icaSummary' => 'required',
            'otherDiscountsSummary' => 'required',
            'retefuenteSummary' => 'required',
            'settlementPeriod' => 'required',
            'totalBank' => 'required',
            'totalDiscounts' => 'required',
            'totalPayroll' => 'required',
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'contractorsList' => 'listado de contratos',
            'contractorsSummaryList' => 'listado de contratos resumen',
            'budgetCode'  =>  'codigo presupuestal',
            'estPculSummary' => 'estimado pro. cultura',
            'estPpmSummary' => 'estimado pro. personas mayores',
            'icaSummary' => 'ica',
            'otherDiscountsSummary' => 'otros descuentos',
            'retefuenteSummary' => 'retefuente',
            'settlementPeriod' => 'periodo a pagar',
            'totalBank' => 'total banco',
            'totalDiscounts' => 'total descuentos',
            'totalPayroll' => 'total pagar',
        ];
    }
}
