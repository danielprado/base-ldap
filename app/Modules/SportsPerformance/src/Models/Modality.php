<?php

namespace App\Modules\SportsPerformance\src\Models;

use Illuminate\Database\Eloquent\Model;

class Modality extends Model
{
    /**
     * The connection name for the model.
     *
     * @var string
     */
    protected $connection = 'mysql_sim_rendimiento_deportivo';

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'modalidad_deporte';

    /**
     * The primary key for the model.
     *
     * @var string
     */
    protected $primaryKey = 'Id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['Id_Deporte' , 'Id_Clasificacion' , 'Nombre'];
    
    public $timestamps = false;
}
