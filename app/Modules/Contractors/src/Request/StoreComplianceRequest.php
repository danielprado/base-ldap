<?php

namespace App\Modules\Contractors\src\Request;

use App\Modules\Contractors\src\Constants\Contributions;
use App\Modules\Contractors\src\Models\Compliance;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class StoreComplianceRequest extends FormRequest
{
    private $compliance;

    /**
     * Indicates if the validator should stop on the first rule failure.
     *
     * @var bool
     */
    protected $stopOnFirstFailure = true;

    protected function prepareForValidation()
    {
        $compliance = Compliance::query()
            ->whereHas('contract', function ($query) {
                $query->where('contract', $this->input('contract', '0000'));
            })
            ->where('period_month', $this->input('period_month', 0))
            ->where('period_year', $this->input('period_year', 0))
            ->firstOrFail();

        $this->compliance = $compliance;

        $dependentOrOptionalFields = [
            'income_tax_filer'          => 'withholding_value',
            'afc_value'                 => 'afc_value',
            'afp_voluntary_value'       => 'afp_voluntary_value',
            'children_minor'            => 'children_minor_files',
            'children_mayor_students'   => 'children_mayor_students_files',
            'children_disabilities'     => 'children_disabilities_files',
            'spouse'                    => 'spouse_files',
            'parents_dependency'        => 'parents_dependency_files',
            'housing_interests'         => 'housing_interests_file',
            'prepaid_medicine'          => 'prepaid_medicine_file'
        ];

        foreach ($dependentOrOptionalFields as $parent => $dependent) {
            if ($this->shouldRequireContribution($parent)) {
                $this->request->set($dependent, $this->input($dependent));
            }
        }

        if ($this->input('pensioner')) $this->request->set('afp_value', null);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'document'                      => 'required|numeric|digits_between:3,12',
            'contract'                      => 'required|string|max:100',
            'period_month'                  => 'required|numeric',
            'period_year'                   => 'required|numeric|digits:4|date_format:Y',
            'subdirectorate_id'             => [ Rule::requiredIf($this->compliance->isFirstTime()) ],
            'area_id'                       => [ Rule::requiredIf($this->compliance->isFirstTime()) ],
            'has_children'                  => [ Rule::requiredIf($this->shouldRequireContribution(Contributions::HAS_CHILDREN)), 'nullable', 'boolean'],
            'children_number'               => 'required_if:has_children,1,0|prohibited_if_not:has_children|nullable|numeric|digits_between:1,12',
            'children_minor'                => [ Rule::requiredIf($this->shouldRequireContribution(Contributions::CHILDREN_MINOR)), 'nullable', 'boolean'],
            'children_minor_files'          => 'required_if:children_minor,1|prohibited_if_not:children_minor|nullable|file|mimes:pdf',
            'children_mayor_students'       => [ Rule::requiredIf($this->shouldRequireContribution(Contributions::CHILDREN_MAYOR_STUDENTS)), 'nullable', 'boolean'],
            'children_mayor_students_files' => 'required_if:children_mayor_students,1|prohibited_if_not:children_mayor_students|nullable|file|mimes:pdf',
            'children_disabilities'         => [ Rule::requiredIf($this->shouldRequireContribution(Contributions::CHILDREN_DISABILITIES)), 'nullable', 'boolean'],
            'children_disabilities_files'   => 'required_if:children_disabilities,1|prohibited_if_not:children_disabilities|nullable|file|mimes:pdf',
            'spouse'                        => [ Rule::requiredIf($this->shouldRequireContribution(Contributions::SPOUSE)), 'nullable', 'boolean'],
            'spouse_files'                  => 'required_if:spouse,1|prohibited_if_not:spouse|nullable|file|mimes:pdf',
            'parents_dependency'            => [ Rule::requiredIf($this->shouldRequireContribution(Contributions::PARENTS_DEPENDENCY)), 'nullable', 'boolean'],
            'parents_dependency_files'      => 'required_if:parents_dependency,1|prohibited_if_not:parents_dependency|nullable|file|mimes:pdf',
            'income_tax_filer'              => [ Rule::requiredIf($this->shouldRequireContribution(Contributions::INCOME_TAX_FILER)), 'nullable', 'boolean'],
            'withholding_value'             => 'required_if:income_tax_filer,1|prohibited_if_not:income_tax_filer|nullable|numeric|digits_between:1,12',
            'pensioner'                     => 'required|boolean',
            'pensioner_file'                => 'required_if:pensioner,1|prohibited_if_not:pensioner|nullable|file|mimes:pdf',
            'afc_value'                     => 'nullable|numeric|digits_between:1,12',
            'afp_voluntary_value'           => 'nullable|numeric|digits_between:1,12',
            'housing_interests'             => [ Rule::requiredIf($this->shouldRequireContribution(Contributions::HOUSING_INTERESTS)), 'nullable', 'boolean'],
            'housing_interests_file'       => 'required_if:housing_interests,1|prohibited_if_not:housing_interests|nullable|file|mimes:pdf',
            'prepaid_medicine'              => [ Rule::requiredIf($this->shouldRequireContribution(Contributions::PREPAID_MEDICINE)), 'nullable', 'boolean'],
            'prepaid_medicine_file'         => 'required_if:prepaid_medicine,1|prohibited_if_not:prepaid_medicine|nullable|file|mimes:pdf',
        ];
    }

    private function shouldRequireContribution($name)
    {
        if ($this->compliance->isFirstTime()) return true;
        return $this->compliance->contribution($name)->isValid();
    }

    private function loadComplianceModel()
    {
        $compliance = Compliance::query()
            ->whereHas('contract', function ($query) {
                $query->where('contract', $this->input('contract', '0000'));
            })
            ->where('period_month', $this->input('period_month', 0))
            ->where('period_year', $this->input('period_year', 0))
            ->firstOrFail();

        $this->compliance = $compliance;
    }

    private function prepareDependentOrOptionalFields()
    {
        $dependentOrOptionalFields = [
            'income_tax_filer'          => 'withholding_value',
            'afc_value'                 => 'afc_value',
            'afp_voluntary_value'       => 'afp_voluntary_value',
            'children_minor'            => 'children_minor_files',
            'children_mayor_students'   => 'children_mayor_students_files',
            'children_disabilities'     => 'children_disabilities_files',
            'spouse'                    => 'spouse_files',
            'parents_dependency'        => 'parents_dependency_files',
            'housing_interests'         => 'housing_interests_file',
            'prepaid_medicine'          => 'prepaid_medicine_file'
        ];

        foreach ($dependentOrOptionalFields as $parent => $dependent) {
            if ($this->shouldRequireContribution($parent)) {
                $this->request->set($dependent, $this->input($dependent));
            }
        }
    }
}
