<?php


namespace App\Modules\Contractors\src\Constants;


class Roles
{
    const ROLE_ADMIN = 'contractors-portal-super-admin';
    const ROLE_ARL = 'contractors-portal-arl';
    const ROLE_HIRING = 'contractors-portal-hiring';
    const ROLE_LEGAL = 'contractors-portal-legal';
    const ROLE_RP = 'contractors-portal-rp';
    const ROLE_THIRD_PARTY = 'contractors-portal-third-party';
    const ROLE_OBSERVER = 'contractors-portal-observer';
    const ROLE_CONSULTATION = 'contractors-portal-consultation';
    const ROLE_SUPERVISOR = 'contractors-portal-supervisor';
    const ROLE_CONTRACTOR = 'contractors-portal-contractor';
    const ROLE_AUTHORIZING_OFFICER = 'contractors-portal-authorizing-officer';
    const ROLE_SUPERVISOR_SUPPORT = 'contractors-portal-supervisor-support';
    const ROLE_ACCOUNTING = 'contractors-accounting';
    const ROLE_TREASURY = 'contractors-treasury';
    const ROLE_BINNACLE = 'contractors-binnacle';
    const ROLE_SUPERVISOR_TECHNICAL_SUPPORT = 'contractors-portal-supervisor-technical-support';
    const ROLE_OBSERVER_PAC = 'contractors-portal-observer-pac';
    const ROLE_CYCLE_PATH_GUARDIAN = 'contractors-portal-cycle-path-guardian';

    /**
     * @return string[]
     */
    public static function all()
    {
        return [
            self::ROLE_ADMIN,
            self::ROLE_ARL,
            self::ROLE_HIRING,
            self::ROLE_LEGAL,
            self::ROLE_RP,
            self::ROLE_OBSERVER,
            self::ROLE_THIRD_PARTY,
            self::ROLE_CONSULTATION,
            self::ROLE_SUPERVISOR,
            self::ROLE_CONTRACTOR,
            self::ROLE_AUTHORIZING_OFFICER,
            self::ROLE_SUPERVISOR_SUPPORT,
            self::ROLE_ACCOUNTING,
            self::ROLE_TREASURY,
            self::ROLE_BINNACLE,
            self::ROLE_SUPERVISOR_TECHNICAL_SUPPORT,
            self::ROLE_CYCLE_PATH_GUARDIAN,
            self::ROLE_OBSERVER_PAC
        ];
    }

    /**
     * @return string[]
     */
    public static function keyed()
    {
        return [
            self::ROLE_ADMIN    => self::ROLE_ADMIN,
            self::ROLE_ARL      => self::ROLE_ARL,
            self::ROLE_HIRING   => self::ROLE_HIRING,
            self::ROLE_LEGAL    => self::ROLE_LEGAL,
            self::ROLE_RP       => self::ROLE_RP,
            self::ROLE_OBSERVER => self::ROLE_OBSERVER,
            self::ROLE_THIRD_PARTY => self::ROLE_THIRD_PARTY,
            self::ROLE_CONSULTATION => self::ROLE_CONSULTATION,
            self::ROLE_SUPERVISOR => self::ROLE_SUPERVISOR,
            self::ROLE_CONTRACTOR => self::ROLE_CONTRACTOR,
            self::ROLE_AUTHORIZING_OFFICER => self::ROLE_AUTHORIZING_OFFICER,
            self::ROLE_SUPERVISOR_SUPPORT=> self::ROLE_SUPERVISOR_SUPPORT,
            self::ROLE_ACCOUNTING => self::ROLE_ACCOUNTING,
            self::ROLE_TREASURY => self::ROLE_TREASURY,
            self::ROLE_BINNACLE => self::ROLE_BINNACLE,
            self::ROLE_SUPERVISOR_TECHNICAL_SUPPORT => self::ROLE_SUPERVISOR_TECHNICAL_SUPPORT,
            self::ROLE_CYCLE_PATH_GUARDIAN => self::ROLE_CYCLE_PATH_GUARDIAN,
            self::ROLE_OBSERVER_PAC => self::ROLE_OBSERVER_PAC
        ];
    }

    public static function find($role)
    {
        return isset(self::keyed()[$role]) ? self::keyed()[$role] : null;
    }

    public static function adminAnd($role)
    {
        $find = isset(self::keyed()[$role]) ? self::keyed()[$role] : null;
        $roles = [
            Roles::ROLE_ADMIN
        ];
        return $find ? array_push($roles, [$find]) : $roles;
    }
    public static function withoutConsultingAndContractor()
    {
        return [
            Roles::ROLE_ADMIN,
            Roles::ROLE_ARL,
            Roles::ROLE_HIRING,
            Roles::ROLE_LEGAL,
            Roles::ROLE_RP,
            Roles::ROLE_OBSERVER,
            Roles::ROLE_THIRD_PARTY,
            Roles::ROLE_CYCLE_PATH_GUARDIAN,
            Roles::ROLE_CONSULTATION,
        ];
    }

    public static function contractorAndOther()
    {
        return [
            Roles::ROLE_ADMIN,
            Roles::ROLE_ARL,
            Roles::ROLE_HIRING,
            Roles::ROLE_LEGAL,
            Roles::ROLE_RP,
            Roles::ROLE_OBSERVER,
            Roles::ROLE_THIRD_PARTY,
            Roles::ROLE_SUPERVISOR,
            Roles::ROLE_AUTHORIZING_OFFICER,
            Roles::ROLE_SUPERVISOR_SUPPORT,
            Roles::ROLE_ACCOUNTING,
            Roles::ROLE_TREASURY,
            Roles::ROLE_BINNACLE,
            Roles::ROLE_SUPERVISOR_TECHNICAL_SUPPORT,
            Roles::ROLE_CYCLE_PATH_GUARDIAN,
            Roles::ROLE_OBSERVER_PAC
        ];
    }
}
