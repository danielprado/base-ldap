<?php

namespace App\Modules\Contractors\src\imports;

use App\Modules\Contractors\src\Models\Contractor;
use App\Modules\Contractors\src\Models\Embargo;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class EmbargoImport implements ToModel, WithHeadingRow
{
    /**
     * @param array $row
     *
     * @return Model|null
     * @throws \Exception
     * @throws \Throwable
     */
    public function model(array $row)
    {
        if (empty($row['contratista'])) {
            return null;
        }
        $contractor = Contractor::query()->with('embargoes')->where('document', $row['contratista'])->first();
        if (!isset($contractor)) {
            throw new \Exception("No se encontró un contratista con el documento: " . $row['contratista']);
        }
        $contractor->embargoed = true;
        $contractor->saveOrFail();
        $date = isset($row['fecha']) ? Carbon::parse($row['fecha'])->format('Y-m-d') : null;
        return new Embargo([
            'contractor_id'     => $contractor->id,
            'document'          => $contractor->document,
            'name'              => $contractor->full_name,
            'type'              => $row['tipo'] ?? null,
            'filed'             => $row['radicado'] ?? null,
            'date'              => $date ?? null,
            'job'               => $row['oficio'] ?? null,
            'judged'            => $row['juzgado'] ?? null,
            'description'       => $row['descripcion'] ?? null,
            'judicial_account'  => $row['cuenta'] ?? null,
            'value'             => $row['valor'] ?? null,
        ]);
        /*return $contractor->embargoes()->create([
            'document'          => $contractor->document,
            'name'              => $contractor->full_name,
            'type'              => $row['tipo'] ?? null,
            'filed'             => $row['radicado'] ?? null,
            'date'              => $date ?? null,
            'job'               => $row['oficio'] ?? null,
            'judged'            => $row['juzgado'] ?? null,
            'description'       => $row['descripcion'] ?? null,
            'judicial_account'  => $row['cuenta'] ?? null,
            'value'             => $row['valor'] ?? null,
        ]);*/
    }
}
