<?php

namespace App\Modules\Certifications\src\Request;

use Illuminate\Foundation\Http\FormRequest;

class SuspensionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'number'            => 'required|numeric',
            'months'            => 'required|numeric',
            'days'              => 'required|numeric',
            'start_date'        => 'required|date',
            'final_date'        => 'required|date',
            'restart_date'      => 'required|date',
            'final_date_cto'    => 'required|date',
        ];
    }

    public function attributes()
    {
        return [
            'number'            => 'Número de la suspensión',
            'months'            => 'Meses',
            'days'              => 'Días',
            'start_date'        => 'Fecha de incio',
            'final_date'        => 'Fecha de finalización',
            'restart_date'      => 'Fecha de reinicio',
            'final_date_cto'    => 'Fecha de finalización del contrato',
        ];
    }
}
