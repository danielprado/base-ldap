<?php

namespace App\Modules\Contractors\src\Exports;

use App\Models\Security\Subdirectorate;
use App\Models\Security\User;
use App\Traits\AppendHeaderToExcel;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Support\Collection;
use Imtigger\LaravelJobStatus\JobStatus;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\WithMultipleSheets;
use Maatwebsite\Excel\Events\AfterSheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\NumberFormat;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class MultiSheetInvoicingExport implements WithMultipleSheets
{
    use Exportable;

    /**
     * @var Collection
     */
    private $payroll;

    /**
     * @var Subdirectorate|null
     */
    private $subdirectorate;

    /**
     * @var User|null
     */
    private $supervisor;

    public function __construct(Collection $payroll, ?Subdirectorate $subdirectorate, ?User $supervisor)
    {

        $this->payroll = $payroll;
        $this->subdirectorate = $subdirectorate;
        $this->supervisor = $supervisor;
    }

    public function sheets(): array
    {
        /* return [
            new InvoicingExport($this->filterByFundingSource('1'), $this->subdirectorate, $this->supervisor, 'TRANSFERENCIA', $this->payroll),
            new InvoicingExport($this->filterByFundingSource('3'), $this->subdirectorate, $this->supervisor, 'ADMINISTRADOS', $this->payroll),
        ]; */
        $sheets = [];
        $transfer = $this->filterByFundingSource('1');
        if ($transfer->isNotEmpty()) {
            $sheets[] = new InvoicingExport($transfer, $this->subdirectorate, $this->supervisor, 'TRANSFERENCIA', $this->payroll);
        }

        $managed = $this->filterByFundingSource('3');
        if ($managed->isNotEmpty()) {
            $sheets[] = new InvoicingExport($managed, $this->subdirectorate, $this->supervisor, 'ADMINISTRADOS', $this->payroll);
        }

        return $sheets;
    }

    /**
     * Filtrar los datos por fundingSource.
     *
     * @param string $prefix
     * @return Collection
     */
    private function filterByFundingSource(string $prefix): Collection
    {
        return collect($this->payroll['fullPaymentPlanList'])
            ->map(function ($item) use ($prefix) {
                if (isset($item['pmrList']) && is_array($item['pmrList'])) {
                    $filteredPmrList = collect($item['pmrList'])->filter(function ($pmr) use ($prefix) {
                        return substr($pmr['fundingSource'], 0, strlen($prefix)) === $prefix;
                    });

                    if ($filteredPmrList->isNotEmpty()) {
                        $item['pmrList'] = $filteredPmrList->toArray();
                        return $item;
                    }
                }
                return null;
            })
            ->filter();
    }
}
