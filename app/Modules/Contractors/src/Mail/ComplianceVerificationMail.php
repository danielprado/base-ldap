<?php

namespace App\Modules\Contractors\src\Mail;

use App\Modules\Contractors\src\Models\Compliance;
use App\Modules\Contractors\src\Models\Contractor;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ComplianceVerificationMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Contractor
     */
    private $user;

    /**
     * @var Compliance
     */
    private $compliance;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Contractor $user, Compliance $compliance)
    {
        $this->user = $user;
        $this->compliance = $compliance;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $full_name = $this->user->full_name ?? '';
        $code = $this->compliance->verification_code;

        $style = "font-family:'Open Sans',Arial,sans-serif;font-size:42px;color:#3b3b3b;font-weight:bold;letter-spacing:4px;text-align:center";

        return $this->view('mail.mail')
            ->subject('Certificado de Cumplimiento Tributario - Código de Verificación')
            ->with([
                'slogan'    => 'Notificación Certificado de Cumplimiento Tributario IDRD', 
                'header'    => 'IDRD',
                'title'     => 'Código de Verificación',
                'content'   =>  "¡Hola {$full_name}! para continuar con la creación de tu certificado de cumplimiento tributario, usa el siguiente código de verificación.",
                'details'   =>  "<h1 style=\"$style\">{$code}</h1>",
                'hide_btn'  => true,
                'info'      =>  "Si no solicitó un código de verificación, no se requiere ninguna acción.",
                'year'      =>  now()->year
            ]);
    }
}
