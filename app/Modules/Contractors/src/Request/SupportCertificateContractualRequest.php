<?php

namespace App\Modules\Contractors\src\Request;

use App\Modules\Contractors\src\Models\Contractor;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SupportCertificateContractualRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'documentType'          => 'required|numeric',
            'document'              => 'required|numeric',
            'name'                  => 'required|string',
            'email'                 => 'required|email|',
            'email_confirmation'    => 'required|email|same:email',
            'phone'                 => 'required|numeric',
            'year'                  => 'required|numeric|min:1900|max:'.now()->year,
            'description'           => 'nullable|string',
            'contract_number'       => [
                'nullable',
                'numeric',
                Rule::unique('mysql_certificacion_contratos.soporte', 'Numero_Contrato')->where(function ($query) {
                    return $query
                        ->where('Documento_Solicitante', $this->get('document'))
                        ->where('Anio_Contrato', $this->get('year'))
                        ->where('Numero_Contrato', $this->get('contract_number'))
                        ->where('Estado', 1);
                }),
            ],
        ];
    }

    /**
     * Get custom attributes for validator errors.
     *
     * @return array
     */
    public function attributes()
    {
        return [
            'documentType'          =>  'tipo de documento',
            'document'              =>  'número de documento',
            'name'                  =>  'nombre de la contratista',
            'email'                 =>  'correo electrónico personal',
            'email_confirmation'    =>  'confirmar correo',
            'phone'                 =>  'número de teléfono',
            'year'                  =>  'año de contrato',
            'contract_number'       =>  'número de contrato',
            'description'           =>  'información adicional',
        ];
    }

    public function messages()
    {
        return [
            'contract_number.unique'    =>  'Ya se realizó una solicitud con el número y año de contrato ingresados, espere una solución a través de correo electrónico.',
        ];
    }
}
