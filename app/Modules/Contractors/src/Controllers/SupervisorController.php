<?php


namespace App\Modules\Contractors\src\Controllers;

use App\Http\Controllers\Controller;
use App\Modules\Contractors\src\Models\UserLdap;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Storage;

class SupervisorController extends Controller
{

    public function sign(){
        try {
            $userLogin = auth('api')->user();
            $user = UserLdap::where('document', $userLogin->document)->first();
            return $this->success_message(
                __('validation.handler.success'),
                Response::HTTP_OK,
                Response::HTTP_OK,
                ['signature' => $user->signature]
            );

        } catch (\Throwable $exception) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $exception->getMessage()
            );
        }


    }


    public function addSign(Request $request)
    {
        try {
            if (($request->hasFile('file') && $request->file('file')->isValid())) {
                $userLogin = auth('api')->user();
                $user = UserLdap::where('document', $userLogin->document)->first();
                $signature = $user->signature;
                $file = $request->file('file');
                $extension = $file->getClientOriginalExtension();
                if ($extension != 'png' && $extension != 'jpg') {
                    return $this->error_response(
                        __('validation.handler.unexpected_failure'),
                        Response::HTTP_UNPROCESSABLE_ENTITY,
                        'Extension del archivo invalida'
                    );
                }

                if (!$user) {
                    return $this->error_response(
                        __('validation.handler.unexpected_failure'),
                        Response::HTTP_UNPROCESSABLE_ENTITY,
                        'Usuario no encontrado'
                    );
                }
                if ($signature && Storage::disk('user')->exists($signature)) {
                    Storage::disk('user')->delete($signature);
                }

                $now = now()->format('YmdHis');
                $signature = "SIGNATURE_{$user->document}_{$now}.$extension";
                $file->storeAs('user', $signature, [ 'disk' => 'local' ]);
                $user->update(['signature' => $signature]);
                return $this->success_message(
                    __('validation.handler.success'),
                    Response::HTTP_OK,
                    Response::HTTP_OK
                );
            }
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                'Adjunte un archivo png o jpg'
            );
        } catch (\Throwable $exception) {
            return $this->error_response(
                __('validation.handler.unexpected_failure'),
                Response::HTTP_UNPROCESSABLE_ENTITY,
                $exception->getMessage()
            );
        }
    }
}
